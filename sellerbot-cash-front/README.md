<div align="right"><i>Updated at 2020-09-07 by kenny(sonmini03@gmail.com)</i></div>

Overview
========
### What ?

SellerBotCash 사용자 사이트

### HA Config.
<img src="./doc/sellerbot_ha.png"/>


Development
===========
Setting-up tools
----------------
### Visual Studio Code
- https://code.visualstudio.com/Download#
- Extensions
    + Java Extension Pack
    + Language Support for Java(TM) by Red Hat (0.64.1)
    + Maven for Java
    + Spring Initializr java Support
    + JavaScript(ES6) code snippets
    + Marp for VS Code
    + lombok Annotations Support for VS Code

### Git Client 
- https://github.com/git-for-windows/git/releases/download/v2.28.0.windows.1/Git-2.28.0-64-bit.exe
- Gitlab에 SSH key 등록

### Apache maven 
- http://mirror.apache-kr.org/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip
- setting.xml 설정  
    ${maven 설치 디렉토리}/conf/setting.xml 에 다음을 추가
    ```xml
    <servers>
        <server>
            <id>maven-snapshots</id>
            <username>admin</username>
            <password>dhsfldnjs@$)2018</password>
        </server>
    </servers>
    ```
- VS Code 터미널에서 Maven 경로를 못찾을 경우, system reboot.

### Setting https \w NGINX on local machine 
+ NGINX 다운로드 & unzip : https://nginx.org/download/nginx-1.18.0.zip
+ Copy doc/nginx/dev/`*.*` to $NGINX_INSTALL_DIR/conf/`*.*`
+ Starting/stopping NGINX 
    @ Powershell Terminal
    #> cd $NGINX_INSTALL_DIR
    #> Start-process .\nginx.exe        ;; Starting nginx           
    #> taskkill /f /im nginx.exe        ;; Stopping nginx       

+ 또는 host 파일 활용(Optional)

    - Edit C:\\windows\System32\\drivers\\etc\\hosts
    ```
        ...
        ${local IP}	sellerbot.co.kr
        ${local IP}	www.sellerbot.co.kr
        ...
    ```
    - 설정 파일 활성 및 재시작
    ```
        #> mv conf/sites/local.conf conf/sites/local.conf.unavailable
        #> mv conf/sites/sellerbot.co.kr.unavailable conf/sites/sellerbot.co.kr
        ;;; restart nginx
    ```

Deployment (Jenkins)
==========
+ smp-commons 배포(smp-commons-dto 변경 時)
+ verify (http://192.168.111.240:8080)
    - verify-smp-commons : SMP 공통 모듈
    - verify-sellerbot-front
+ production (http://183.111.105.141:8080)
    - prod-smp-commons : 공통 모듈
    - prod-sellerbotcash-01(183.111.105.136)
    - prod-sellerbotcash-02(183.111.105.138)

Operation
=========
NGINX
-----
### Staring/Stopping nginx
@ SSH terminal  
```
    #> sudo systemctl [start|stop|restart|status] nginx    
```     
+ ref. /etc/nginx 
+ TIP 1 : How to verify nginx config.
```
    #> sudo nginx -t
```

### 사이트 '운용' <-> '공사 중' 상태 변경 방법
+ "공사 중" 상태로 변경
```
    #> ssh svccash2@183.111.105.136(& 138)
    #> cd /etc/nginx/sites-enabled
    #> sudo rm sellerbot.co.kr.conf
    #> sudo ln -s ../sites/uc.conf .
    #> sudo systemctl restart nginx 
```
+ 다시 "운용" 상태로 변경
```
    #> cd /etc/nginx/sites-enabled
    #> sudo rm uc.conf
    #> sudo ln -s ../sites/sellerbot.co.kr.conf .
    #> sudo systemctl restart nginx 
```

Tomcat
------
### Staring/Stopping tomcat
```
    #> sudo systemctl [start|stop|restart|status] tomcat
```

Okname
------
### at kcb.module.v3.OkCert.callOkCert 관련 오류시
```
    Open JDK 1.8이 설치되있는 폴더로 이동해서 security 폴더 내에 있는 java.security 파일에서
    jdk.tls.disabledAlgorithms=SSLv3, TLSv1, TLSv1.1, RC4, DES, MD5withRSA, ＼
    DH keySize < 1024, EC keySize < 224, 3DES_EDE_CBC, anon, NULL, ＼
    include jdk.disabled.namedCurves

    여기서 TLSv1, TLSv1.1을 제거하고 사용 중인 WAS인 tomcat을 재시작하면 KCB 휴대폰 본인인증 모듈이 정상적으로
    실행되는 것을 확인할 수 있다.
```