-- cash_pay_free_int_004 FEBN 체험종료결제예정_004
UPDATE tc_kkont_tpl SET KKONT_TPL_ID='cash_pay_free_int_004', TPL_TYP_CD='FEBN', TPL_TITLE='체험종료결제예정_004'
, TPL_CONT='${bizName} 고객님의 정산예정금 비서 ${goodsName}이에요.

무료체험기간이 ${remainingDays}일 남았어요!
체험 종료 후에는 등록하신 결제수단으로 매월 정기결제가 진행될 거예요~

▶ 무료체험 신청정보
- 이용권 : ${goodsName}
- 무료체험기간 : ${startDate} ~ ${endDate}
- 첫 결제일 : ${nextBillDate}
- 결제예정금 : ${price}원 (VAT별도)

※ 은행, 카드사 결제내역에는 ‘(주)온리원’으로 나타나는 점 참고해주세요~

※ 무료체험종료 후 등록된 결제수단으로 매월 ${billingDay}일 정기결제 되며,
무료체험 종료일까지 서비스 해지 시 결제예정은 취소처리 됩니다~

※ 해지는 아래 경로 또는 아래 버튼 ‘이용권 해지 바로가기‘ 에서 가능해요~

[셀러봇캐시→마이페이지→결제정보→이용권해지]', BTN_CONT='[{"name":"셀러봇캐시 바로가기", "type":"WL", "url_mobile":"https://sellerbot.co.kr", "url_pc":"https://sellerbot.co.kr" },{"name":"이용권 해지 바로가기", "type":"WL", "url_mobile":"https://www.sellerbot.co.kr/sub/my/paidCancel", "url_pc":"https://www.sellerbot.co.kr/sub/my/paidCancel" }]', MOD_ID='admin', MOD_TS=now() WHERE TPL_TYP_CD = 'FEBN';

-- cash_pay_int_004  BILN 결제예정_004
UPDATE tc_kkont_tpl SET KKONT_TPL_ID='cash_pay_int_004', TPL_TYP_CD='BILN', TPL_TITLE='결제예정_004', TPL_CONT='${bizName} 고객님의 정산예정금 비서 ${goodsName}이에요(꺄아)

고객님의 무료체험기간이 ${remainingDays}일 남았어요!
체험 종료 후에는 등록하신 결제수단으로 매월 정기결제가 진행됩니다!

▶ 무료체험 신청정보
- 이용권 : ${goodsName}
- 무료체험기간 : ${startDate} ~ ${endDate}
- 첫 결제일 : ${nextBillDate}
- 결제예정금 : ${price}원 (VAT별도)

※ 은행, 카드사 결제내역에는 ‘(주)온리원’으로 나타나는 점 참고해주세요.
※ 무료체험종료 후 등록된 결제수단으로 매월 ${billingDay}일 정기결제 되며, 무료체험 종료일까지 서비스 해지 시 결제예정은 취소처리 됩니다.
※ 해지는 아래 경로 또는 아래 버튼 ‘이용권 해지 바로가기‘ 에서 가능합니다.
[셀러봇캐시→마이페이지→결제정보→이용권해지]', BTN_CONT='[{"name":"이용권 해지 바로가기", "type":"WL", "url_mobile":"https://www.sellerbot.co.kr/sub/my/paidCancel", "url_pc":"https://www.sellerbot.co.kr/sub/my/paidCancel" }]', MOD_ID='admin', MOD_TS=now() WHERE TPL_TYP_CD = 'BILN';

-- cash_err_pay_004  BIER 오류_정기결제_004
UPDATE tc_kkont_tpl SET KKONT_TPL_ID='cash_err_pay_004', TPL_TYP_CD='BIER', TPL_TITLE='오류_정기결제_004', TPL_CONT='${bizName} 고객님의 정산예정금 비서 ${goodsName}이에요.

카드의 정기결제가 실패하여 ${goodsName} 이용권 연장에 실패하였어요.

▶ 이용권 정기결제 실패
- 이용권 : ${goodsName}
- 이용기간 : ${useSttDt} ~ ${useEndDt}
- 월 요금 : ${price}원 (VAT별도)
- 실패사유 : ${message}

※ 결제 실패로 유료 서비스 이용이 중단될 예정이에요. 아래 버튼을 클릭하여 등록된 결제수단을 확인해주세요!

${note}', BTN_CONT='[{"name":"셀러봇캐시 바로가기", "type":"WL", "url_mobile":"https://sellerbot.co.kr", "url_pc":"https://sellerbot.co.kr" },{"name":"결제수단확인하러 가기", "type":"WL", "url_mobile":"https://www.sellerbot.co.kr/sub/my/paidInfo", "url_pc":"https://www.sellerbot.co.kr/sub/my/paidInfo" }]', MOD_ID='admin', MOD_TS=now() WHERE TPL_TYP_CD = 'BIER';

-- join_003        JOIN 가입_003
UPDATE tc_kkont_tpl SET KKONT_TPL_ID='join_003', TPL_TYP_CD='JOIN', TPL_TITLE='가입_003', TPL_CONT='${bizName} 고객님,
쉬운 정산관리, 셀러봇캐시에
가입해주셔서 감사합니다(뿌듯)

셀러봇캐시에서는 온라인쇼핑몰 판매자님을 위한 다양한 기능을 제공해드리고 있어요!

■ 고객님의 가입 정보!
#가입일자 : ${regDate}
#아이디(이메일) : ${custId}

■ 이럴 때 유용한 ‘셀러봇캐시’
#팔긴 팔았는데, 언제 얼마나 받지?
→ 정산예정금 확인!
#판매를 잘하고 있는건가?
→ 판매통계 분석 (매출/정산금/반품/매출추이)
#정산주기가 너무 길다면?
→ 제휴 금융서비스!

■ 알아서 챙겨주는 알림톡리포트!
#정산예정금을 매일 보고해드려요 (주말 제외)
#가입단계 또는 마이페이지→판매몰 관리에서 신청하세요

${note}', BTN_CONT='[{"name":"셀러봇캐시 바로가기", "type":"WL", "url_mobile":"https://sellerbot.co.kr", "url_pc":"https://sellerbot.co.kr" }]', MOD_ID='admin', MOD_TS=now() WHERE TPL_TYP_CD = 'JOIN';

-- pay_fail_inv_003    PBFC      결제실패_프리봇전환_003
UPDATE tc_kkont_tpl SET KKONT_TPL_ID='pay_fail_inv_003', TPL_TYP_CD='PBFC', TPL_TITLE='결제실패_프리봇전환_003', TPL_CONT='${bizName} 님의 정산예정금 비서봇이 알려드려요~

${goodsName}의 정기결제일 ${billDate} 이후, 재결제가 되지 않아서 내일부터 ''프리봇'' 회원으로 전환될 예정이예요.

▶ 이용권 결제를 원하실 경우 아래 경로에서 재결제를 해주세요.
① 셀러봇캐시 로그인
② (좌측메뉴) 이용요금 안내 → 이용권 소개
③ 이용권 선택 후 다음을 눌러 결제 진행

※ 프리봇 전환 시 변동되는 내용은?
‘사이트 내 상세데이터’ 확인이 안돼요.
(정산일자별 정산예정금 등)

${note}', BTN_CONT='[{"name":"셀러봇캐시 바로가기", "type":"WL", "url_mobile":"https://sellerbot.co.kr", "url_pc":"https://sellerbot.co.kr" },{"name":"결제 페이지 이동", "type":"WL", "url_mobile":"https://www.sellerbot.co.kr/sub/payment/product", "url_pc":"https://www.sellerbot.co.kr/sub/payment/product" }]
', MOD_ID='admin', MOD_TS=now() WHERE TPL_TYP_CD = 'PBFC';

-- cash_pay_inc_int_001   BFIN 요금인상_결제예정_001
INSERT INTO tc_kkont_tpl (KKONT_TPL_ID, TPL_TYP_CD, TPL_TITLE, TPL_CONT, BTN_CONT, REG_ID, REG_TS, MOD_ID, MOD_TS) VALUES('cash_pay_inc_int_001', 'BFIN', '요금인상_결제예정_001', '${bizName} 고객님의 정산예정금 비서 ${beforeGoodsName}이에요(꺄아)

셀러봇캐시의 이용권요금 인상으로 고객님의 기존 고객 유예기간이 ${remainingDays}일 남았어요.

유예기간 종료 시, 인상된 요금으로 등록하신 결제수단을 통해 매월 정기결제가 진행됩니다!

▶ 이용권 정보
- 이용권 : ${afterGoodsName}
- 결제일 : ${nextBillDate}
- 결제예정금 : ${afterPrice}원(VAT별도)

셀러봇캐시는 앞으로 더 고도화된 서비스로 보답하도록 노력하겠습니다 :)

※ 은행, 카드사 결제내역에는‘(주)온리원’으로 나타나는점 참고해주세요.
※ 기존 고객 유예기간 종료 후 인상된 요금으로 등록된 결제수단을 통해 매월 ${billingDay}일 정기결제 되며,유예기간 종료일까지 서비스 해지 시 결제예정은 취소처리 됩니다.
※ 해지는 아래 경로 또는 아래 버튼 ‘이용권 해지 바로가기‘ 에서 가능합니다.
[셀러봇캐시→마이페이지→결제정보→이용권해지]', '[{"name":"이용권 해지 바로가기", "type":"WL", "url_mobile":"https://www.sellerbot.co.kr/sub/my/paidCancel", "url_pc":"https://www.sellerbot.co.kr/sub/my/paidCancel" }]', 'admin', now(), 'admin', now());

-- cash_join_canc_001     ESTN 전용가입_해지예정_001
INSERT INTO tc_kkont_tpl (KKONT_TPL_ID, TPL_TYP_CD, TPL_TITLE, TPL_CONT, BTN_CONT, REG_ID, REG_TS, MOD_ID, MOD_TS) VALUES('cash_join_canc_001', 'ESTN', '전용가입_해지예정_001', '${bizName} 고객님의 정산예정금 비서 ${goodsName}이에요(꺄아)

고객님의 무료체험기간이${remainingDays}일 남았어요!
가입 시, 해지 예약을 신청하신 것으로 확인 되어 체험기간 종료 후 셀러봇캐시 이용이 해지 됩니다!

셀러봇캐시 서비스를 더 이용하고 싶으시다면 아래 버튼을 클릭하여 해지 취소를 진행해주세요(뿌듯)

▶ 무료체험 신청 정보
- 이용권 : ${goodsName}
- 무료체험기간 : ${startDate}~ ${endDate}
-해지예정일: ${cancleDate}

해지예약을 철회 하실 경우,
${goodsName} -${price}원 (VAT별도) 이 등록하신 결제 수단으로 매월 정기결제가 진행됩니다.

${note}', '[{"name":"해지예약 철회하기", "type":"WL", "url_mobile":"https://www.sellerbot.co.kr${param}", "url_pc":"https://www.sellerbot.co.kr${param}" }]', 'admin', now(), 'admin', now());