INSERT INTO tp_code_m (CD_GRP_ID, COM_CD, CD_NM, SCR_SHOW_CD, SORT_ORD, USE_YN, CD_DESCP, TOMM_SCRA_YN, TOD_RETRY_YN, INS_SHOW_YN, USR_ERR_YN) 
VALUES('A065', 'PASB', '메가봇', NULL, 7, 'Y', NULL, 'N', 'N', 'N', 'N');
INSERT INTO tp_code_m (CD_GRP_ID, COM_CD, CD_NM, SCR_SHOW_CD, SORT_ORD, USE_YN, CD_DESCP, TOMM_SCRA_YN, TOD_RETRY_YN, INS_SHOW_YN, USR_ERR_YN) 
VALUES('A065', 'PASP', '기가봇', NULL, 8, 'Y', NULL, 'N', 'N', 'N', 'N');
INSERT INTO tp_code_m (CD_GRP_ID, COM_CD, CD_NM, SCR_SHOW_CD, SORT_ORD, USE_YN, CD_DESCP, TOMM_SCRA_YN, TOD_RETRY_YN, INS_SHOW_YN, USR_ERR_YN) 
VALUES('A065', 'PAFP', '테라봇', NULL, 9, 'Y', NULL, 'N', 'N', 'N', 'N');

INSERT INTO ts_goods (GOODS_TYP_CD, GOODS_NM, SALE_MALL_REG_ID_PSB_CNT, SETT_ACC_ACCT_REG_PSB_CNT, BIZ_REG_PSB_CNT, ORD, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS
)VALUES('PASB', '메가봇', 5, 5, 10, 8, 'N', 'system', now(), 'system', now());

INSERT INTO ts_goods (GOODS_TYP_CD, GOODS_NM, SALE_MALL_REG_ID_PSB_CNT, SETT_ACC_ACCT_REG_PSB_CNT, BIZ_REG_PSB_CNT, ORD, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS
)VALUES('PASP', '기가봇', 0, 0, 0, 9, 'N', 'system', now(), 'system', now());

INSERT INTO ts_goods (GOODS_TYP_CD, GOODS_NM, SALE_MALL_REG_ID_PSB_CNT, SETT_ACC_ACCT_REG_PSB_CNT, BIZ_REG_PSB_CNT, ORD, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS
)VALUES('PAFP', '테라봇', 0, 0, 0, 10, 'N', 'system', now(), 'system', now());


-- GOODS_SEQ_NO 입력해야 함.
-- 메가봇
INSERT INTO ts_goods_opt (GOODS_SEQ_NO, PAY_TYP_CD, FREE_DUR, VLD_DUR, GOODS_BAS_PRC, FNA_PRC, PERIOD, BILL_DUR, ORD, ADDN_YN, FREE_TRIAL_YN, DEF_YN, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS) 
VALUES(, 'BPAY', '14d', '1m', 50000, 50000, 1, NULL, 5, 'N', 'Y', 'Y', 'N', 'system', now(), 'system', now());

-- 기가봇
INSERT INTO ts_goods_opt (GOODS_SEQ_NO, PAY_TYP_CD, FREE_DUR, VLD_DUR, GOODS_BAS_PRC, FNA_PRC, PERIOD, BILL_DUR, ORD, ADDN_YN, FREE_TRIAL_YN, DEF_YN, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS) 
VALUES(, 'BPAY', '14d', '1m', 100000, 100000, 1, NULL, 2, 'N', 'Y', 'Y', 'N', 'system', now(), 'system', now());

-- 테라봇
INSERT INTO ts_goods_opt (GOODS_SEQ_NO, PAY_TYP_CD, FREE_DUR, VLD_DUR, GOODS_BAS_PRC, FNA_PRC, PERIOD, BILL_DUR, ORD, ADDN_YN, FREE_TRIAL_YN, DEF_YN, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS) 
VALUES(, 'BPAY', '14d', '1m', 100000, 50000, 1, NULL, 2, 'Y', 'Y', 'Y', 'N', 'system', now(), 'system', now());

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- DCFP 사용 안함.
-- INSERT INTO ts_goods (GOODS_TYP_CD, GOODS_NM, SALE_MALL_REG_ID_PSB_CNT, SETT_ACC_ACCT_REG_PSB_CNT, BIZ_REG_PSB_CNT, ORD, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS
-- )VALUES('DCFP', '금융할인상품', 0, 0, 0, 11, 'N', 'system', now(), 'system', now());
-- INSERT INTO ts_goods_opt (GOODS_SEQ_NO, PAY_TYP_CD, FREE_DUR, VLD_DUR, GOODS_BAS_PRC, FNA_PRC, PERIOD, BILL_DUR, ORD, ADDN_YN, FREE_TRIAL_YN, DEF_YN, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS) VALUES(, 'BPAY', '14d', '1m', 100000, 50000, 1, NULL, 2, 'Y', 'Y', 'Y', 'N', 'system', now(), 'system', now());

-- 테라봇 무료 상품 생성
INSERT INTO ts_goods_opt (GOODS_SEQ_NO, PAY_TYP_CD, FREE_DUR, VLD_DUR, GOODS_BAS_PRC, FNA_PRC, PERIOD, BILL_DUR, ORD, ADDN_YN, FREE_TRIAL_YN, DEF_YN, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS) 
  VALUES(, 'FREE', NULL, NULL, 0, 0, 0, NULL, 3, 'N', 'Y', 'N', 'Y', 'system', now(), 'system', now());

-- SMP에서 이용권 발급을 위함.
INSERT INTO ts_goods_opt (GOODS_SEQ_NO, PAY_TYP_CD, FREE_DUR, VLD_DUR, GOODS_BAS_PRC, FNA_PRC, PERIOD, BILL_DUR, ORD, ADDN_YN, FREE_TRIAL_YN, DEF_YN, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS) 
VALUES(, 'BCHR', NULL, NULL, 0, 0, 0, NULL, 1, 'N', 'N', 'N', 'Y', 'system', now(), 'system', now());

INSERT INTO ts_goods_opt (GOODS_SEQ_NO, PAY_TYP_CD, FREE_DUR, VLD_DUR, GOODS_BAS_PRC, FNA_PRC, PERIOD, BILL_DUR, ORD, ADDN_YN, FREE_TRIAL_YN, DEF_YN, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS) 
VALUES(, 'BCHR', NULL, NULL, 0, 0, 0, NULL, 1, 'N', 'N', 'N', 'Y', 'system', now(), 'system', now());

INSERT INTO ts_goods_opt (GOODS_SEQ_NO, PAY_TYP_CD, FREE_DUR, VLD_DUR, GOODS_BAS_PRC, FNA_PRC, PERIOD, BILL_DUR, ORD, ADDN_YN, FREE_TRIAL_YN, DEF_YN, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS) 
VALUES(, 'BCHR', NULL, NULL, 0, 0, 0, NULL, 1, 'N', 'N', 'N', 'Y', 'system', now(), 'system', now());




INSERT INTO ts_goods_promotion (GOODS_OPT_SEQ_NO, PROMOTION_CD, PROMOTION_NM, PROMOTION_DUR, DUR_CD, INF_SITE_CD, USE_STT_DT, USE_END_DT, URL, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS, CANCLE_PRODUCT_YN) 
VALUES(, 'LOANJOFR', '신한은행전용가입프로모션', '14', 'd', 'SHIN', '20230516', '20991231', '', 'Y', 'system', now(), 'system', , now(), 'Y');
INSERT INTO ts_goods_promotion (GOODS_OPT_SEQ_NO, PROMOTION_CD, PROMOTION_NM, PROMOTION_DUR, DUR_CD, INF_SITE_CD, USE_STT_DT, USE_END_DT, URL, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS, CANCLE_PRODUCT_YN) 
VALUES(, 'LOANJOFR', 'KB국민은행전용가입프로모션', '14', 'd', 'KBB', '20230516', '20991231', '', 'Y', 'system', , now(), 'system', , now(), 'Y');
INSERT INTO ts_goods_promotion (GOODS_OPT_SEQ_NO, PROMOTION_CD, PROMOTION_NM, PROMOTION_DUR, DUR_CD, INF_SITE_CD, USE_STT_DT, USE_END_DT, URL, USE_YN, REG_ID, REG_TS, MOD_ID, MOD_TS, CANCLE_PRODUCT_YN) 
VALUES(, 'LOANJOFR', '키움Yes저축은행전용가입프로모션', '14', 'd', 'KWY', '20230516', '20991231', '', 'Y', 'system', , now(), 'system', , now(), 'Y');
