CREATE TABLE `ts_selected_goods_history` (
  `GOODS_SEQ` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '시퀀스',
  `CUST_SEQ_NO` bigint(20) DEFAULT NULL COMMENT '고객 ID SEQ',
  `GOODS_SEQ_NO` bigint(20) DEFAULT NULL COMMENT '상품 일련번호',
  `GOODS_OPT_SEQ_NO` bigint(20) DEFAULT NULL COMMENT '상품 옵션 일련번호',
  `REG_TS` datetime NOT NULL DEFAULT current_timestamp() COMMENT '등록일시',
  `GOODS_TYP_CD` varchar(4) DEFAULT NULL COMMENT '상품 유형 코드',
  PRIMARY KEY (`GOODS_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='상품 선택 임시저장 테이블';