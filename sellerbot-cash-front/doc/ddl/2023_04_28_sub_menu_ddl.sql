UPDATE ts_sub_menu SET
	MENU_NM = '셀러봇캐시 정기구독'
	, MENU_PATH = '/sub/payment/step1'
	, ORD = 1
	, MENU_SEQ_NO = 7
	, SHOW_MENU_BAR_YN = 'Y'
	, MENU_BAR_TITLE = '셀러봇캐시 정기구독'
	, SHOW_ONLY_CF24_YN = 'N'
WHERE SUB_MENU_SEQ_NO = 17;