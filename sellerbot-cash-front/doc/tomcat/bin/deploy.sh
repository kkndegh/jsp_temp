#!/bin/bash
 
# TOMCAT VARIABLE
 
# SERVER 이름 변경시 폴더명도 변경필요함
SERVER_NAME=sellerbot
SETUSER=svccash2
UNAME=`id -u -n`

PACKAGE_NAME="sellerbot-cash-front.war.original"
PACKAGE_DIR="/home/svccash2/deploy"
WAS_ROOT="/home/svccash2/was"
CATALINA_BASE=$WAS_ROOT/apps/$SERVER_NAME


WAS_WEBAPPS="$CATALINA_BASE/webapps/"



# Check user
RUNNER=`whoami`

if [ $RUNNER != $SETUSER ] ;
   then echo "Deny Access : [ $RUNNER ]. Not $SETUSER" ;
   exit 0 ;
fi


# Check Tomcat running

tomcat_pid() {
    echo `ps aux | grep "$CATALINA_BASE[ ]" | grep -v grep | awk '{ print $2 }'`
}


pid=$(tomcat_pid)

if [ -n "$pid" ]
then
	echo "Tomcat is already running (pid: $pid)"
	exit 1
fi

rm -rf $WAS_WEBAPPS/ROOT*

cp ${PACKAGE_DIR}/${PACKAGE_NAME} $WAS_WEBAPPS/ROOT.war
