#!/bin/bash
# kenneth 
LOGDIR='/home/svccash2/was/apps/sellerbot/logs'
last_month=`date +%Y-%m -d 'last month'`
last_month_catalina=`date +%Y%m -d 'last month'`
month_last=`date +%Y-%m -d '-2 month'`

cd $LOGDIR
mkdir -p $last_month

mv broker.$last_month*.log $last_month &>/dev/null
mv ais.$last_month*.log $last_month &>/dev/null
mv catalina.$last_month*.log $last_month &>/dev/null
mv host-manager.$last_month*.log $last_month &>/dev/null
mv localhost.$last_month*.log $last_month &>/dev/null
mv localhost_access.$last_month*.log $last_month &>/dev/null
mv manager.$last_month*.log $last_month &>/dev/null
mv catalina.out-$last_month_catalina*.gz $last_month &>/dev/null

rm -f $month_last.tar
if [ -d "$month_last" ]; then
	tar -cf $month_last.tar $month_last
	gzip -9 $month_last.tar
	rm -rf $month_last
fi

find . -name '*.gz' -mtime +365 -exec rm -f {} \;
