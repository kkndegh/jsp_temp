#!/bin/bash
 
# TOMCAT VARIABLE
export JAVA_HOME=/usr
 
# SERVER 이름 변경시 폴더명도 변경필요함
SERVER_NAME=sellerbot
TOMCAT_USER=svccash2
SHUTDOWN_WAIT=10
UNAME=`id -u -n`

# http 및 ajp 포트 설정
export JAVA_OPTS="-server"
export JAVA_OPTS="$JAVA_OPTS -Dtomcat.port.http=8300"
export JAVA_OPTS="$JAVA_OPTS -Dtomcat.port.ajp=8301"
export JAVA_OPTS="$JAVA_OPTS -Dtomcat.port.shutdown=8302"
export JAVA_OPTS="$JAVA_OPTS -Dtomcat.port.redirect=8303"
export JAVA_OPTS="$JAVA_OPTS -Djava.net.preferIPv4Stack=true"

#TOMCAT CATALINA Setting


export WAS_ROOT="/home/svccash2/was"
export CATALINA_HOME=$WAS_ROOT/tomcat
export CATALINA_BASE=$WAS_ROOT/apps/$SERVER_NAME
#export CATALINA_OPTS="$CATALINA_OPTS -Denv=dev -Dlogback.configurationFile=logger/dev-logback.xml"
export CATALINA_PID="$CATALINA_BASE/work/catlina.pid"

#JVM setting

export JAVA_OPTS="$JAVA_OPTS -Xms2024m"
export JAVA_OPTS="$JAVA_OPTS -Xmx2024m"
export JAVA_OPTS="$JAVA_OPTS -XX:NewSize=512m"
export JAVA_OPTS="$JAVA_OPTS -XX:MaxNewSize=512m"
export JAVA_OPTS="$JAVA_OPTS -XX:+DisableExplicitGC"
export JAVA_OPTS="$JAVA_OPTS -XX:+UseParallelGC"
export JAVA_OPTS="$JAVA_OPTS -XX:+UseParallelOldGC"
export JAVA_OPTS="$JAVA_OPTS -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=50M -Xloggc:$CATALINA_BASE/logs/gc.log"

# JMX
#export JAVA_OPTS="-Dcom.sun.management.jmxremote
#-Dcom.sun.management.jmxremote.port=9099
#-Dcom.sun.management.jmxremote.rmi.port=9199
#-Dcom.sun.management.jmxremote.ssl=false
#-Dcom.sun.management.jmxremote.authenticate=false $JAVA_OPTS "

# PINPOINT AGENT

#export VERSION=1.6.2
#export AGENT_PATH=/skt/semo/agent/
#export AGENT_ID=
#export APPLICATION_NAME=

#export CATALINA_OPTS="$CATALINA_OPTS -javaagent:$AGENT_PATH/pinpoint-bootstrap-$VERSION.jar"
#export CATALINA_OPTS="$CATALINA_OPTS -Dpinpoint.agentId=$AGENT_ID"
#export CATALINA_OPTS="$CATALINA_OPTS -Dpinpoint.applicationName=$APPLICATION_NAME"

# Check user
SETUSER="$TOMCAT_USER"
RUNNER=`whoami`

if [ $RUNNER != $SETUSER ] ;
   then echo "Deny Access : [ $RUNNER ]. Not $SETUSER" ;
   exit 0 ;
fi


# TOMCAT 실행 구문

 
tomcat_pid() {
    echo `ps aux | grep "$CATALINA_BASE[ ]" | grep -v grep | awk '{ print $2 }'`
}
 
start() {
    pid=$(tomcat_pid)
 
    if [ -n "$pid" ]
    then
        echo "Tomcat is already running (pid: $pid)"
    else
        echo "Starting tomcat (pid: $pid)"
 
        if [ e$UNAME = "eroot" ]
        then
            ulimit -n 100000
            umask 007
            /bin/su -p -s /bin/sh $TOMCAT_USER $CATALINA_HOME/bin/startup.sh
        else
            $CATALINA_HOME/bin/startup.sh
        fi
    fi
 
    return 0
}
 
stop() {
    pid=$(tomcat_pid)
    echo "Running tomcat PID : $pid" 
    if [ -n "$pid" ]
    then
        echo "Stoping Tomcat"
 
        if [ e$UNAME = "eroot" ]
        then
            /bin/su -p -s /bin/sh $TOMCAT_USER $CATALINA_HOME/bin/shutdown.sh
        else
            $CATALINA_HOME/bin/shutdown.sh -force
        fi
 
        let kwait=$SHUTDOWN_WAIT
        count=0;
        until [ `ps -p $pid | grep -c $pid` = '0' ] || [ $count -gt $kwait ]
        do
            echo -n -e "\nwaiting for processes to exit (pid: $pid)\n";
            sleep 1
            let count=$count+1;
        done
 
        if [ $count -gt $kwait ]; then
            echo -n -e "\nkilling processes which didn't stop after $SHUTDOWN_WAIT seconds (pid: $pid)"
            kill -9 $pid
        fi
    else
        echo "Tomcat is not running"
    fi
 
    return 0
}
 
case $1 in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start
        ;;
    status)
        pid=$(tomcat_pid)
        if [ -n "$pid" ]
        then
            echo "Tomcat is running with pid: $pid"
        else
            echo "Tomcat is not running"
        fi
        ;;
    *)
        echo $"Usage : $0 {start|stop|restart}"
        exit 1
esac
exit 0


