//gnb
$(function(){
	$(".btn_toggle").click(function(){
		$(".gnb").toggleClass("on");
		$(".gnb ul li").mouseover(function(){
			$(this).addClass("on");
		});
		$(".gnb ul li").mouseout(function(){
			$(this).removeClass("on");
		});
	});

	//user, language
	$(".btn_user").click(function(event){
		event.stopPropagation();
		$(".user .list").slideToggle('fast');
	});
	$(".language .active").click(function(event){
		event.stopPropagation();
		$(".language ul").slideToggle('fast');
	});
	$(document).click(function(){
		$('.language ul, .user .list').slideUp('fast');
	})

	//Quick menu
	$(".btn_quick").click(function(){
		$(".quick_menu").toggleClass("on");
	})

	//accordion
	$('.btn_dropdown').click(function() {
		$('.btn_dropdown').removeClass('on');
	 	$('.gnb ul li dl dd ul').slideUp('fast');
		if($(this).parent().next().is(':hidden') == true) {
			$(this).addClass('on');
			$(this).parent().next().slideDown('fast');
		 }
	 });
	$('.btn_dropdown').mouseover(function() {
		$(this).addClass('over');
	}).mouseout(function() {
		$(this).removeClass('over');
	});
	$('.gnb ul li dl dd ul').hide();

	//input animation
	$( ".inp_default input" ).focusin(function() {
        $(this).parent().addClass("on");
    });
    $( ".inp_default input" ).focusout(function() {
        $(this).parent().removeClass("on");
	});

	//main link button
	$( ".pc_state_btn .btn" ).click(function() {
        $(this).parent().toggleClass("on");
	});

	var height =  $(".notice").height();
	var num = $(".rolling li").length;
	var max = height * num;
	var move = 0;
	function noticeRolling(){
		move += height;
		$(".rolling").animate({"top":-move},600,function(){
			if( move >= max ){
				$(this).css("top",0);
				move = 0;
			};
		});
	};

	// notice Rolling
	noticeRollingOff = setInterval(noticeRolling,3000);
	$(".rolling").append($(".rolling li").first().clone());

	$(".rolling_stop").click(function(){
		clearInterval(noticeRollingOff);
	});
	$(".rolling_start").click(function(){
		noticeRollingOff = setInterval(noticeRolling,3000);
	});

	//tab_nav
	$('.tab_nav a').click(function(){
		$(this).closest('.tab_nav').find('li').removeClass('on');
		$(this).parent('li').addClass('on');
		var tCon = $(this).attr("href");
		$(tCon).parent().find('.tab_con').removeClass('on');
		$(tCon).addClass('on');

		return false;
	});


	//contentClose
	$('.contents .contents_box').wrap('<div class="closeWrap">').parents('.closeWrap');
	$('.con_tit').append('<a href="/" class="closeBtn"><span><span></a>')




});
