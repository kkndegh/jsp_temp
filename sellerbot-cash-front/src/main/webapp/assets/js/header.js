import modalShow from '/assets/new_join/js/import/modalShow.js';

// document.querySelector('header .top .text .notiWrap ul li');

function headerSlider() {
    let i = 0;
    setInterval(() => {
        i++;
        if (i > document.querySelectorAll('header .top .text .notiWrap ul li').length - 1) {
            i = 0;
        }
        document.querySelector('header .top .text .notiWrap ul').style.top = i * -20 + 'px';
    }, 5000);
}
headerSlider();

document.querySelector('#closeHeaderNoti').addEventListener('click', () => {
    document.querySelector('header').classList.add('notiClose');
    if (document.querySelector('main section')) {
        document.querySelector('main section').style.paddingTop = '140px';
    }
});

document.querySelector('.m_menu_wra')?.addEventListener('click', () => {
    document.querySelector('.m_menu_wra').classList.toggle('active');
    document.querySelector('.m_sideMenu_wra').classList.toggle('active');
    if(document.querySelector('.m_sideMenu_cover')){
        document.querySelector('.m_sideMenu_cover').classList.toggle('active');
    }

    if (document.querySelector('.m_menu_wra').classList.contains('active')) {
        document.querySelector('body').style.overflow = 'hidden';
    } else {
        document.querySelector('body').style.overflow = '';
    }
});

// footer 각 약관 클릭 했을 때
document.querySelector('#footer_term_1').addEventListener('click', () => {
    let terms;

    if( isNull(sessionStorage.getItem("term")) ){
        // 약관 팝업
        $.get("/pub/common/terms", null, function(res){
            sessionStorage.setItem("term", JSON.stringify(res));
            terms = sessionStorage.getItem("term");
        });	
    } else {
        let jsonRes = sessionStorage.getItem("term");
        terms = JSON.parse(jsonRes);
    }

    $.each(terms, function(){
        if(this.terms_typ_cd == 'UST'){
            let popupHtml = `
                <div class="head">
                    <h5>`+ this.terms_title + `</h5>
                    <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
                </div>
                <div class="body">
                    <div class="textDiv">
                <pre>`
                    + this.terms_cont +
                `</pre>
                </div>
                <button class="cancelModal">확인</button>
                </div>
            `;

            modalShow({
                id: 'footer_term_1_modal',
                content: popupHtml,
                function: () => {
                    document
                        .querySelector('#footer_term_1_modal button.cancelModal')
                        .addEventListener('click', () => {
                            document.querySelector('#footer_term_1_modal').remove();
                            document.querySelector('body').style.overflow = '';
                        });
                },
            });
        }
    });
});

document.querySelector('#footer_term_2').addEventListener('click', () => {
    let terms;

    if( isNull(sessionStorage.getItem("term")) ){
        // 약관 팝업
        $.get("/pub/common/terms", null, function(res){
            sessionStorage.setItem("term", JSON.stringify(res));
            terms = sessionStorage.getItem("term");
        });	
    } else {
        let jsonRes = sessionStorage.getItem("term");
        terms = JSON.parse(jsonRes);
    }

    $.each(terms, function(){
        if(this.terms_typ_cd == 'PPT'){
            let popupHtml = `
                <div class="head">
                    <h5>`+ this.terms_title + `</h5>
                    <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
                </div>
                <div class="body">
                    <div class="textDiv">
                <pre>`
                    + this.terms_cont +
                `</pre>
                </div>
                <button class="cancelModal">확인</button>
                </div>
            `;

            modalShow({
                id: 'footer_term_2_modal',
                content: popupHtml,
                function: () => {
                    document
                        .querySelector('#footer_term_2_modal button.cancelModal')
                        .addEventListener('click', () => {
                            document.querySelector('#footer_term_2_modal').remove();
                            document.querySelector('body').style.overflow = '';
                        });
                },
            });
        }
    });
});

document.querySelector('#footer_term_3').addEventListener('click', () => {
    let popupHtml = `
        <div class="head">
            <h5>`+ "이메일무단수집 거부" + `</h5>
            <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
        </div>
        <div class="body">
            <div class="textDiv">
        <pre>`
            + "영리목적 셀러봇캐시에서는 본 웹사이트에 게시된 이메일 주소가 전자우편 수집<br> 프로그램이나 그 밖의 기술적 장치를 이용하여 무단으로 수집되는 것을 거부하며,<br> 이를 위반시 정보통신망법에 의해 형사처벌됨을 유념하시기 바랍니다." +
        `</pre>
        </div>
        <button class="cancelModal">확인</button>
        </div>
    `;

    modalShow({
        id: 'footer_term_3_modal',
        content: popupHtml,
        function: () => {
            document
                .querySelector('#footer_term_3_modal button.cancelModal')
                .addEventListener('click', () => {
                    document.querySelector('#footer_term_3_modal').remove();
                    document.querySelector('body').style.overflow = '';
                });
        },
    });
});