<div class="side_menu_area">
    <span class="icon"><img src="/assets/images/notice/sideMenu.png" alt=""></span>
    <div class="side_menu_section">
        <p class="title_menu">셀러봇캐시 통합관리
            <img src="/assets/images/member/close.png" alt=""></p>
        <ul class="gnb_side">
            <li class="menu">
                <p class="depth1">정산예정금 통합관리</p>
                <ul class="sub_gnb_side">
                    <li class="sub_menu sub2depth focus">
                        <a href="">정산예정금 확인</a>
                        <ul class="sub_sub">
                            <li class="sub_sub_menu focus"><a href="/sub/settAcc/all.php">- 정산예정금 한눈에 보기</a></li>
                            <li class="sub_sub_menu"><a href="/sub/settAcc/detail.php">- 정산예정금 상세보기</a></li>
                            <li class="sub_sub_menu"><a href="/sub/settAcc/calendar.php">- 정산예정금 스케줄보기</a></li>
                        </ul>
                    </li>
                    <li class="sub_menu"><a href="/sub/sale/sale.php">매출통계</a></li>
                    <li class="sub_menu"><a href="/sub/past/status.php">과거 정산금 통계</a></li>
                    <li class="sub_menu"><a href="">반품통계</a></li>
                </ul>
            </li>
            <li class="menu">
                <p class="depth1">정산계좌 통합관리</p>
                <ul class="sub_gnb_side">
                        <li class="sub_menu"><a href="">정산계좌 통합조회</a></li>
                        <li class="sub_menu"><a href="">정산계좌 상세내역</a></li>
                    </ul>
            </li>
            <li class="menu">
                <p class="depth1">동종 업계 매출 추이</p>
                <ul class="sub_gnb_side">
                    <li class="sub_menu"><a href="">동종 업계 매출 추이 현황</a></li>
                </ul>
            </li>
            <li class="menu">
                <p class="depth1">금융 서비스</p>
                <ul class="sub_gnb_side">
                    <li class="sub_menu"><a href="">금융 서비스</a></li>
                    <li class="sub_menu"><a href="">서비스소개</a></li>
                </ul>
            </li>
            <li class="menu last">
                <p class="depth1">공지사항</p>
                <ul class="sub_gnb_side">
                    <li class="sub_menu"><a href="/sub/notice/notice.php">공지사항</a></li>
                    <li class="sub_menu"><a href="/sub/notice/event.php">이벤트</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<script>

</script>