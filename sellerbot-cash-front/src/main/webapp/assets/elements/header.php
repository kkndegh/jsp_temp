<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="index,follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="canonical" href="URL입력">
    <title>Sellerbot</title>
    <!--font-->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/moonspam/NanumSquare@1.0/nanumsquare.css">
    <!--swiper-->
    <link rel="stylesheet" href="/assets/js/swiper-master/dist/css/swiper.min.css">
    <!--fullcalendar-->
    <link rel="stylesheet" href="/assets/js/fullcalendar/daygrid/main.css">
    <link rel="stylesheet" href="/assets/js/fullcalendar/core/main.css">
    <link rel="stylesheet" href="/assets/js/monthpicker/monthPicker.css">
    <!--style-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/assets/js/select2/select2.css">

    <link rel="stylesheet" href="/assets/css/style.css">
   <!--js-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>
    <script src="/assets/js/moment.min.js"></script>

</head>
<body>
<div class="wra">

    <div class="header_wra">
        <div class="header_area">
            <a href="/index.php" class="logo"><img src="/assets/images/common/sellerbot_Logo.png" alt=""></a>
            <!--로그인 전-->
            <div class="right_menu before" style="display: none;">
                <a href="/sub/member/login.php" class="mb_btn">Login</a>
                <span class="bar">|</span>
                <a href="/sub/member/join_step1.php" class="mb_btn">Join</a>
            </div>
            <!--로그인 후-->
            <div class="right_menu after">
                <ul class="clearfix">
                    <li class="new alarm_m"><a href="/sub/notice/notice.php" class="m_alarm"><img src="/assets/images/member/alram_icon.png" alt=""></a>
                    <span class="num_new">22</span>
                    </li>
                    <li class="new pservice_m"><a href="" class="m_premium"><img src="/assets/images/member/premium_icon.png" alt=""></a>
                        <span class="num_new">6M</span></li>
                    <li class="my_m"><a href="/sub/my/edit_info.php" class="m_my"><img src="/assets/images/member/my_icon.png" alt=""> My</a></li>
                </ul>
                <span class="nick">Only1 님</span>
                <span class="bar">|</span>
                <a href="" class="mb_logout">Logout</a>
                <a href="/sub/cs/faq.php" class="m_cs"><img src="/assets/images/member/customerCenter_icon.png" alt="">고객센터</a>

                <!--alarm-->
                <div class="modal_header modal_alarm">
                    <p class="close_modal"><img src="/assets/images/main/btn_X.png" alt=""></p>
                    <div class="modal_menu">
                        <p class="focus">미확인알림 <span>5</span></p>
                        <p>전체알림 <span>22</span></p>
                    </div>
                    <div class="list_alarm active" data-alarm="list1">
                        <ul>
                            <li class="item_yet">
                                <a href="/sub/notice/notice.php">
                                    <p class="title">필독! 정산서비스 관련 안내공지</p>
                                    <p class="desc">현재 내부플랫폼 안정화 작업을 위해 다음을 알려드립니다</p>
                                    <p class="date">2019-04-26</p>
                                </a>
                            </li>
                            <li class="item_yet">
                                <a href="/sub/notice/notice.php">
                                    <p class="title">긴급 점검사항 안내공지</p>
                                    <p class="desc">현재 내부플랫폼 안정화 작업을 위해 다음을 알려드립니다</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                            <li class="item_yet">
                                <a href="/sub/notice/notice.php">
                                    <p class="title">[4월 이벤트] 깜짝 이벤트를 들고왔4~샤샤~</p>
                                    <p class="desc">현재 내부플랫폼 안정화 작업을 위해 다음을 알려드립니다</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                            <li class="item_yet">
                                <a href="/sub/notice/notice.php">
                                    <p class="title">접속이 자꾸 끊기는 현상에 대해...</p>
                                    <p class="desc">최근 자주 접속 끊기는 사례가 많아 대처 방안을 알려드..</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                            <li class="item_yet">
                                <a href="/sub/notice/notice.php">
                                    <p class="title">오늘은 셀러봇캐시데이~여러분을 위해...</p>
                                    <p class="desc">지치고 힘든 셀러님들을 위해 준비했어요!</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                            <li class="item_yet">
                                <a href="/sub/notice/notice.php">
                                    <p class="title">오늘은 셀러봇캐시데이~여러분을 위해...</p>
                                    <p class="desc">지치고 힘든 셀러님들을 위해 준비했어요!</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="list_alarm" data-alarm="list2">
                        <ul>
                            <li>
                                <a href="/sub/notice/notice.php">
                                    <p class="title">[4월 이벤트] 깜짝 이벤트를 들고왔4~샤샤~</p>
                                    <p class="desc">현재 내부플랫폼 안정화 작업을 위해 다음을 알려드립니다</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                            <li>
                                <a href="/sub/notice/notice.php">
                                    <p class="title">접속이 자꾸 끊기는 현상에 대해...</p>
                                    <p class="desc">최근 자주 접속 끊기는 사례가 많아 대처 방안을 알려드..</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                            <li>
                                <a href="/sub/notice/notice.php">
                                    <p class="title">오늘은 셀러봇캐시데이~여러분을 위해...</p>
                                    <p class="desc">지치고 힘든 셀러님들을 위해 준비했어요!</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                            <li>
                                <a href="/sub/notice/notice.php">
                                    <p class="title">필독! 정산서비스 관련 안내공지</p>
                                    <p class="desc">현재 내부플랫폼 안정화 작업을 위해 다음을 알려드립니다</p>
                                    <p class="date">2019-04-26</p>
                                </a>
                            </li>
                            <li>
                                <a href="/sub/notice/notice.php">
                                    <p class="title">긴급 점검사항 안내공지</p>
                                    <p class="desc">현재 내부플랫폼 안정화 작업을 위해 다음을 알려드립니다</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                            <li>
                                <a href="/sub/notice/notice.php">
                                    <p class="title">오늘은 셀러봇캐시데이~여러분을 위해...</p>
                                    <p class="desc">지치고 힘든 셀러님들을 위해 준비했어요!</p>
                                    <p class="date">2019-04-25</p>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!--service 준비중-->
                <div class="modal_header modal_service_prepare">
                    <p class="close_modal"><img src="/assets/images/main/btn_X.png" alt=""></p>
                    <p class="msg_prepare">서비스 준비중입니다.<br>
                        조금만 기다려주세요~</p>
                </div>
                <!--service type1-->
                <div class="modal_header modal_service type1">
                    <p class="close_modal"><img src="/assets/images/main/btn_X.png" alt=""></p>
                    <div class="modal_title">
                        <p>프리미엄 서비스</p>
                    </div>
                    <div class="modal_content">
                        <div class="item_p_service_modal">
                            <p class="desc">
                                한 곳에서 한 눈에!<br>
                                매출, 정산금, 반품, 동종업계 매출추이 등<br>
                                보다 고도화된 셀러봇캐시를 만나보려면,<br>
                                <span class="color">프리미엄 서비스</span>를 신청하세요!<br>
                            </p>
                            <div class="link_group">
                                <a href="" class="link_p_service">신청하기</a>
                            </div>
                        </div>
                        <div class="item_p_service_modal">
                            <p class="title">이용기간</p>
                            <p class="etc">프리미엄 서비스를 신청하지 않으셨습니다.</p>
                            <div class="link_group">
                                <a href="" class="link_p_service">신청하기</a>
                            </div>
                        </div>
                        <div class="item_p_service_modal">
                            <p class="title">자동이체일자</p>
                            <p class="etc">자동이체 서비스를 신청하지 않으셨습니다.</p>
                            <div class="link_group">
                                <a href="" class="link_p_service">신청하기</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--service type2-->
                <div class="modal_header modal_service type2">
                    <p class="close_modal"><img src="/assets/images/main/btn_X.png" alt=""></p>
                    <div class="modal_title">
                        <p>프리미엄 서비스</p>
                    </div>
                    <div class="modal_content">
                        <div class="item_p_service_modal">
                            <p class="desc">고객님께서는 셀러봇캐시<br>
                                <span class="color">프리미엄 서비스</span>를 이용하고계십니다 .</p>
                            <div class="link_group">
                                <a href="" class="link_p_service done">자세히보기</a>
                            </div>
                        </div>
                        <div class="item_p_service_modal">
                            <p class="title">이용기간</p>
                            <p class="etc">2019년 11월 7일~2019년 12월 7일</p>
                            <div class="link_group">
                                <a href="" class="link_p_service done">자세히보기</a>
                            </div>
                        </div>
                        <div class="item_p_service_modal">
                            <p class="title">자동이체일자</p>
                            <p class="etc">매월 21일</p>
                            <div class="link_group">
                                <a href="" class="link_p_service history">이용내역</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--service type3-->
                <div class="modal_header modal_service type3">
                    <p class="close_modal"><img src="/assets/images/main/btn_X.png" alt=""></p>
                    <div class="modal_title">
                        <p>프리미엄 서비스</p>
                    </div>
                    <div class="modal_content">
                        <div class="item_p_service_modal">
                            <p class="desc">고객님께서는 셀러봇캐시<br>
                                <span class="color">프리미엄 서비스</span>를 이용하고계십니다 .</p>
                            <div class="link_group">
                                <a href="" class="link_p_service done">자세히보기</a>
                            </div>
                        </div>
                        <div class="item_p_service_modal">
                            <p class="title">이용기간</p>
                            <p class="etc">2019년 11월 7일~2019년 12월 7일</p>
                            <div class="link_group">
                                <a href="" class="link_p_service done">자세히보기</a>
                            </div>
                        </div>
                        <div class="item_p_service_modal">
                            <p class="title">프리미엄 서비스</p>
                            <div class="link_group">
                                <a href="" class="link_p_service">결제하기</a>
                                <a href="" class="link_p_service history">이용내역</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <script>
            $(".modal_menu p").click(function () {
                $(".modal_menu p").removeClass("focus");
                $(this).addClass("focus");
                var numAlarm = $(this).index() + 1;
                $(".list_alarm").removeClass("active");
                $(".list_alarm[data-alarm=list"+numAlarm+"]").addClass("active");
            });

            $(".alarm_m").click(function () {
                $(".modal_alarm").show();
                $(".modal_service_prepare").hide();
                $(".modal_alarm .close_modal").click(function () {
                    $(".modal_alarm").hide();
                });
                return false;
            });
            $(".pservice_m").click(function () {
                $(".modal_service_prepare").show();
                $(".modal_alarm").hide();
                $(".modal_service_prepare .close_modal").click(function () {
                    $(".modal_service_prepare").hide();
                });
                return false;
            });

        </script>
