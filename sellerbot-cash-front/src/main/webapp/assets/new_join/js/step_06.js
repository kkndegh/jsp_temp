import modalShow from './import/modalShow.js';
import numberWithCommas from '/assets/new_join/js/import/numberWithCommas.js';

$(document).ready(function(){
    initTicketSelect();
});

// document.querySelector('#prevStep').addEventListener('click', () => {
//     location.href = '/sub/member/step5';
// });

let errMsg = document.getElementById('errMsg').value;
let successYN = document.getElementById('successYN').value;

if(successYN == 'N'){
    var msg = '';
    if(nonNull(errMsg)){
        showAlert("결제 요청이 실패하였습니다.<br>다시 시도해주세요.<br/>(" + errMsg + ")");
    }else{
        showAlert("결제 요청이 실패하였습니다.");
    }
}

document.querySelector('#nextStep').addEventListener('click', () => {
    if(!document.querySelector('#terms_01').checked){
        $("#terms").attr("tabindex", -1).focus();
        var msg = '셀러봇캐시 유료결제 약관에 동의해주세요.';
        document.querySelector('#alertText').textContent = msg;
        document.querySelector('.popup_alarm').style.display = "block";
    }else{
        inicisModule();
    }
});

let price = "";
let goodsName = "";
let typeCd = "";
let optNo = "";
let goodsList = [];

//step1에서 선택한 이용권 선택
function initTicketSelect() {
    var selGoodsSeqNo = document.querySelector('#selGoodsSeqNo').value; 
    var selGoodsOptSeqNo = document.querySelector('#selGoodsOptSeqNo').value; 
    var cnt=0;

    let galleryItem = document.querySelectorAll('.ticket_gallery ul li');

    for(var i=0; i < galleryItem.length; i++){
        if(galleryItem[i].dataset.seq == selGoodsSeqNo && galleryItem[i].dataset.opt == selGoodsOptSeqNo){
            let index = galleryItem[i].dataset.idx;
            let prc = galleryItem[index].dataset.price;
            let fna_prc = galleryItem[index].dataset.fnaprc;

            if(index != 0){
                for(let j=0; j<index; j++){
                    document.querySelector('#nextTicket').click();
                }
            }else{
                document.querySelector('.ticket_gallery ul').style.left = 0 + '%';
            }

            if(fna_prc == '' || fna_prc == null || fna_prc == '0'){
                price =  prc;
            }else{
                price =  fna_prc;
            }

            document.querySelector('#ticketPrice').innerHTML = `월 ${numberWithCommas(price)}원`;
            document.querySelector('#totalPrice').innerHTML =  numberWithCommas(price) + `원`;
            document.querySelector('#ticketName').innerHTML = galleryItem[index].dataset.name;
            
            goodsName = galleryItem[index].dataset.name;
            typeCd = galleryItem[index].dataset.typ;
            optNo = galleryItem[index].dataset.opt;
            inicisDataSetting(price, goodsName, typeCd, optNo);

            cnt++;
        }

        if(i == galleryItem.length-1 && cnt == 0){
            let prc = galleryItem[0].dataset.price;
            let fna_prc = galleryItem[0].dataset.fnaprc;

            document.querySelector('.ticket_gallery ul').style.left = 0 + '%';

            if(fna_prc == '' || fna_prc == null || fna_prc == '0'){
                price =  prc;
            }else{
                price =  fna_prc;
            }
            document.querySelector('#ticketPrice').innerHTML = `월 ${numberWithCommas(price)}원`;
            document.querySelector('#totalPrice').innerHTML = numberWithCommas(price) + `원`;
            document.querySelector('#ticketName').innerHTML = galleryItem[0].dataset.name;

            goodsName = galleryItem[0].dataset.name;
            typeCd = galleryItem[0].dataset.typ;
            optNo = galleryItem[0].dataset.opt;
            inicisDataSetting(price, goodsName, typeCd, optNo);
        }
    }
}

// 이용권 좌 우 클릭
function ticketSelect() {
    let index = 0;
    let galleryItem = document.querySelectorAll('.ticket_gallery ul li');

    if (document.querySelector('#prevTicket').clickHandler) {
        document
            .querySelector('#prevTicket')
            .removeEventListener('click', document.querySelector('#prevTicket').clickHandler);
    }
    if (document.querySelector('#nextTicket').clickHandler) {
        document
            .querySelector('#nextTicket')
            .removeEventListener('click', document.querySelector('#nextTicket').clickHandler);
    }

    document.querySelector('#prevTicket').clickHandler = () => {
        index - 1 < 0 ? (index = galleryItem.length - 1) : index--;
        document.querySelector('.ticket_gallery ul').style.left = index * -100 + '%';

        let prc = galleryItem[index].dataset.price;
        let fna_prc = galleryItem[index].dataset.fnaprc;

        if(fna_prc == '' || fna_prc == null || fna_prc == '0'){
            price =  prc;
        }else{
            price =  fna_prc;
        }
        document.querySelector('#ticketPrice').innerHTML = `월 ${numberWithCommas(price)}원`;
        document.querySelector('#totalPrice').innerHTML =  numberWithCommas(price) + '원';
        document.querySelector('#ticketName').innerHTML = galleryItem[index].dataset.name;

        goodsName = galleryItem[index].dataset.name;
        typeCd = galleryItem[index].dataset.typ;
        optNo = galleryItem[index].dataset.opt;
        inicisDataSetting(price, goodsName, typeCd, optNo);

        document.querySelector('#goodsList').value= JSON.stringify(goodsList);
        document.querySelector('#SendPayForm_id input[name=price]').value = Number(parseInt(price)* 1.1);
        document.querySelector('#SendPayForm_id input[name=goodname]').value = goodsName;
    };
    document
        .querySelector('#prevTicket')
        .addEventListener('click', document.querySelector('#prevTicket').clickHandler);
        
    document.querySelector('#nextTicket').clickHandler = () => {
        index + 1 > galleryItem.length - 1 ? (index = 0) : index++;
        document.querySelector('.ticket_gallery ul').style.left = index * -100 + '%';

        let prc = galleryItem[index].dataset.price;
        let fna_prc = galleryItem[index].dataset.fnaprc;
        
        if(fna_prc == '' || fna_prc == null || fna_prc == '0'){
            price =  prc;
        }else{
            price =  fna_prc;
        }
        document.querySelector('#ticketPrice').innerHTML = `월 ${numberWithCommas(price)}원`;
        document.querySelector('#totalPrice').innerHTML = numberWithCommas(price) + '원';
        document.querySelector('#ticketName').innerHTML = galleryItem[index].dataset.name;

        goodsName = galleryItem[index].dataset.name;
        typeCd = galleryItem[index].dataset.typ;
        optNo = galleryItem[index].dataset.opt;
        inicisDataSetting(price, goodsName, typeCd, optNo);
    };
    document
        .querySelector('#nextTicket')
        .addEventListener('click', document.querySelector('#nextTicket').clickHandler);
}
ticketSelect();

// 갤러리 좌 우 클릭
function slideGallery() {
    let index = 0;
    let galleryItem = document.querySelectorAll('.gallery ul li');
    document.querySelector('#galleryLeft').addEventListener('click', () => {
        index - 1 < 0 ? (index = galleryItem.length - 1) : index--;
        document.querySelector('.gallery ul').style.left = index * -100 + '%';
        document.querySelectorAll('ul.bottomUl li').forEach((el) => el.classList.remove('active'));
        document.querySelectorAll('ul.bottomUl li')[index].classList.add('active');
    });
    document.querySelector('#galleryRight').addEventListener('click', () => {
        index + 1 > galleryItem.length - 1 ? (index = 0) : index++;
        document.querySelector('.gallery ul').style.left = index * -100 + '%';
        document.querySelectorAll('ul.bottomUl li').forEach((el) => el.classList.remove('active'));
        document.querySelectorAll('ul.bottomUl li')[index].classList.add('active');
    });
}
slideGallery();

// 아코디언 🎵
let acc = document.querySelectorAll('.accordion');
acc.forEach((el) => {
    el.querySelector('.title span').addEventListener('click', () => {
        el.classList.toggle('active');

        /* Toggle between hiding and showing the active panel */
        let panel = el.querySelector('.panel');
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            if (panel.scrollHeight < 200) {
                panel.style.maxHeight = panel.scrollHeight + 'px';
            } else {
                panel.style.maxHeight = '200px';
            }
        }
    });
});

// 약관 텍스트 클릭 시 체크박스 클릭됨
document.querySelectorAll('.text_checkBox').forEach((el) => {
    el.addEventListener('click', () => {
        el.parentNode.querySelector('input.chkBox').click();
    });
});

// 약관 동의 시 결제수단 활성화
document.querySelector('#terms_01').addEventListener('change', () => {
    if (document.querySelector('#terms_01').checked) {
        document.querySelector('#creditCard').classList.add('active');
        document.querySelector('.fixed').classList.add('active');
    } else {
        document.querySelector('#creditCard').classList.remove('active');
        document.querySelector('.fixed').classList.remove('active');
    }
});

var inicisDataSetting = function(price, goodsName, typeCd, optNo) {
    price = Number(parseInt(price));
    goodsList = [];
        goodsList.push({
            p: price + (price * 0.1),
            n: encodeURI(goodsName),
            t: typeCd,
            o: optNo
        });

    document.querySelector('#goodsList').value= JSON.stringify(goodsList);
    document.querySelector('#SendPayForm_id input[name=price]').value = price + (price * 0.1);
    document.querySelector('#SendPayForm_id input[name=goodname]').value = "셀러봇이용권(" + goodsName + ")";
        
};


//결제수단 등록 완료 시
document.querySelector('#creditCard').addEventListener('click', () => {
    inicisModule();
});

var inicisModule = function() {
    var price = $('#SendPayForm_id [name="price"]').val();
    var mid = $("#SendPayForm_id [name=mid]").val();

    $.ajax({
        url: '/pub/payment/pay_getSignature',
        data: { mid: mid, price: price },
        async: false,
        type: 'POST',
        dataType: 'json',
        success: function (r) {
            if (r.signature) {
                if ($("#DeviceType").val() == 'PC') {
                    $("#SendPayForm_id [name=oid]").val(r.oid);
                    $("#SendPayForm_id [name=timestamp]").val(r.timestamp);
                    $("#SendPayForm_id [name=signature]").val(r.signature);
    
                    var data = new Object();
                    data.goodsList = JSON.parse($("#goodsList").val());
                    data.freeTrialYn = $("#freeTrialYn").val();
                    data.eventNo = $('#eventNo').val();
                    data.mode = 'register';
                    $("#SendPayForm_id [name=merchantData]").val(JSON.stringify(data));
                    INIStdPay.pay('SendPayForm_id');
                } else {
                    $("#SendPayForm_id [name=orderid]").val(r.oid);
                    $("#SendPayForm_id [name=timestamp]").val(r.timestamp);
                    $("#SendPayForm_id [name=hashdata]").val(r.hash);

                    var data = new Object();
                    data.goodsList = JSON.parse($("#goodsList").val());
                    data.freeTrialYn = $("#freeTrialYn").val();
                    data.eventNo = $('#eventNo').val();
                    data.mode = 'register';
                    data.buyerName = $('#SendPayForm_id [name="buyername"]').val();
                    data.buyerTel = $('#SendPayForm_id [name="buyertel"]').val();
                    data.buyerEmail = $('#SendPayForm_id [name="buyeremail"]').val();
                    
                    $("#SendPayForm_id [name=p_noti]").val(JSON.stringify(data));
                    $("#SendPayForm_id").submit();
               }
            }
        },
        error: function (response, status, error) {
            showAlert("서버 에러.");
        }
    });
        
};

// 유료 결제 약관
fetch('/assets/new_join/pay_term.txt')
    .then((res) => res.text())
    .then((data) => {
        document.querySelector('#termContent').innerHTML = data;
    });

// 이전단게에서 로니봇 선택했는데 몰을 5개 초과로 등록한 경우 20230323 수정
function loanyBotAlert() {
    let popupHtml = `
    <div class="head">
        <h5>이용권 변경 안내</h5>
        <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <p>등록 가능한 판매몰(5개) 초과로<br>선택한 이용권이 '기가봇'으로 변경되었어요.</p>
        <span class="line"></span>
        <p><b>- 메가봇 이용을 원하시면</b></p>
        <p>현재 페이지에서 파이봇으로 결제한 후, <b>무료체험 2주 내에</b></p>
        <p>마이페이지-결제정보-이용권 변경을 통해 이용권을 변경해주세요.</p>
        <br>
        <p><b>- 기가봇 이용을 원하시는 경우</b></p>
        <p>'확인'버튼을 눌러 결제를 진행해주세요.</p>
        <div class="btnFlex">
            <button type="button" class="blue" id="modalConfirm">확인</button>
        </div>
    </div>
    `;
    modalShow({
        id: 'modal_01',
        content: popupHtml,
        function: () => {
            document.querySelector('#modalConfirm').addEventListener('click', () => {
                document.querySelector('#modal_01').remove();
                document.querySelector('body').style.overflow = '';
                // convertToFiBot();
            });
            document.querySelector('#cancelModal').addEventListener('click', () => {
            });
        },
    });
}

// 금융사전용 결제수단 선택 시
function fiBotDCAlert() {
    let popupHtml = `
    <div class="head">
        <h5>무료체험 안내 (결제수단 등록 안내)</h5>
        <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <h4>금융사 전용 고객이시군요!</h4>
        <h4>선택한 이용권은 '테라봇'입니다.</h4>
        <span class="line"></span>
        <div class="row">
        <span class="check"></span>
        <p>가입을 위해서는 결제수단 등록이 필수이며, <br>결제수단 등록 시 무료체험 14일이 제공됩니다.</p>
        </div>
        <br>
        <div class="row">
        <span class="check"></span>
        <p>체험 종료 후 매월 자동 정기결제가 되며, <br>해지는 언제든지 가능합니다.</p>
        </div>
        <div class="btnFlex">
            <button class="blue" id="modalConfirm">확인</button>
        </div>
    </div>
    `;
    modalShow({
        id: 'modal_02',
        content: popupHtml,
        function: () => {
            const closeModal = () => {
                document.querySelector('#modal_02').remove();
                document.querySelector('body').style.overflow = '';
            };

            document.querySelector('#modal_02 #modalConfirm').addEventListener('click', closeModal);
        },
    });
}

if(document.querySelector('#mallCnt').value > 5){
    loanyBotAlert();
}else{
    if(document.querySelector('#selGoodsTypCd').value == 'PAFP'){
        fiBotDCAlert();
    }
}

if(document.querySelector('#skipStep')){
    document.querySelector('#skipStep').addEventListener('click', freeTicketPopup);
}

// 무료체험지급안내팝업
function freeTicketPopup() {
	let popupHtml = `
    <div class="head">
        <h5>셀러봇캐시 이용권 무료체험지급 안내</h5>
        <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <p>셀러봇캐시의 회원이 되어주신 셀러님께 감사의 의미로 테라봇 무료체험 14일을 제공해드립니다</p>
		<p class="red">체험종료 후 결제를 원하시는 경우 아래의 경로를 이용해주세요.</p>
		<span class="line"></span>
		<p>*결제 경로 : [셀러봇캐시 → 이용요금 안내 → 셀러봇캐시 정기구독]</p>
        <div class="btnFlex">
            <button class="blue" id="modalConfirm">대시보드 이동</button>
        </div>
    </div>
    `;
	modalShow({
		id: 'modal_01',
		content: popupHtml,
		function: () => {
			const goDashBoard = () => {
                $.post('/sub/member/step6/skipStep', function (data, status) {
                    location.href = "/";
                })
                .fail(function (response) {
                    if (response.status == 409) {
                        if ("COMM-4300" == response.responseText) {
                            alert("카드 등록하지 않고 이용 대상자가 아닙니다.");
                        } else {
                            alert("기타 오류");
                        }
                    } else {
                        location.reload();
                    }
                }); 

			};

			document
				.querySelector('#modal_01 #modalConfirm')
				.addEventListener('click', goDashBoard);
		},
	});
}