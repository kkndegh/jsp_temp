document.querySelector('#prevStep').addEventListener('click', () => {
    location.href = '/pub/member/step1';
});

document.querySelector('#nextStep').addEventListener('click', () => {
    let checkVail = true;
    var focusName = '';

    if (!document.querySelector('#email').value) {
        document.querySelector('#emailMsg').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'email';
        }
    }
    if (!document.querySelector('#verificationCode').value) {
        document.querySelector('#verificationCodeMsg').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'verificationCode';
        }
    }
    if (!document.querySelector('#confirmVerificationCode').classList.contains('blue')) {
        document.querySelector('#verificationCodeMsg').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'verificationCode';
        }
    }
    if (!document.querySelector('#passwd').value) {
        document.querySelector('#passwdMsg').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'passwd';
        }
    }
    if (!document.querySelector('#passwd_same').value) {
        document.querySelector('#passwd_sameMsg').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'passwd_same';
        }
    } else {
        if (document.querySelector('#passwd').value != document.querySelector('#passwd_same').value) {
            document.querySelector('#passwd_sameMsg').style.display = 'block';
            checkVail = false;
            if (isNull(focusName)) {
                focusName = 'passwd_same';
            }
        }
    }

    if (!checkVail) {
        document.querySelector('#'+ focusName).focus();
        return false;
    }

    // 필수 선택 중 선택하지 않은  약관이 있는지 확인
	var checkEsseYn = true;
	document.querySelectorAll('.checkBoxDiv .chkBox').forEach((el) => {
		var esseYn = el.getAttribute('term_esse_yn');
		if (esseYn == 'Y' && !el.checked) {
			checkEsseYn = false;
		}
	});

	if (!checkEsseYn) {
        var msg = '필수 약관에 동의해주세요';
        document.querySelector('#alertText').textContent = msg;
        document.querySelector('.popup_alarm').style.display = "block";

        $("#termsAgree").attr("tabindex", -1).focus();
        return false;
	}
   
    if (document.querySelector('#nextStep').classList.contains('active')) {
        document.getElementById('form').submit();
    }
});

// 아코디언 🎵
let acc = document.querySelectorAll('.accordion');
acc.forEach((el) => {
    el.querySelector('.title span').addEventListener('click', () => {
        el.classList.toggle('active');

        /* Toggle between hiding and showing the active panel */
        let panel = el.querySelector('.panel');
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            if (panel.scrollHeight < 300) {
                panel.style.maxHeight = panel.scrollHeight + 'px';
            } else {
                panel.style.maxHeight = '300px';
            }
        }
    });
});

document.querySelector('#domain').addEventListener('change', () => {
    if (document.querySelector('#domain').value == 'manual') {
        document.querySelector('#manualDomain').removeAttribute('disabled');
    } else {
        document.querySelector('#manualDomain').setAttribute('disabled', true);
        document.querySelector('#manualDomain').value = '';
    }
});

// 다음 버튼 활성화용 플래그
let flagObj = {
    verifFlag: false,
    pwFlag: false,
    pwCheckFlag: false,
    terms1Flag: false,
    terms2Flag: false,
};
function flagChecker(flag, bool) {
    flagObj[flag] = bool;

    let notChecked = Object.keys(flagObj).find((key) => flagObj[key] === false);

    if (!notChecked) {
        document.querySelector('#nextStep').classList.add('active');
    } else {
        document.querySelector('#nextStep').classList.remove('active');
    }

}

function showMsgArea(id, msg) {
    document.querySelector('#' + id).innerHTML = msg;
    document.querySelector('#' + id).style.display = "block";
}

//인증번호 요청 클릭 시
document.querySelector('#reqVerificationCode').addEventListener('click', (e) => {
    let checkVail = true;
    var focusName = '';

    var checktypCd = true;
    document.querySelectorAll('input[name=sellerbotTerm]').forEach((el) => {
        let termTypeCd = el.getAttribute('term_type_cd');
        if (termTypeCd == 'PPT') {
            if (!el.checked) {
                checktypCd = false;
            }
        }
    });
    if (!checktypCd) {
        var msg = '셀러봇캐시 개인정보 처리 방침을 동의하셔야 인증번호 발송이 가능합니다.';
        document.querySelector('#alertText').textContent = msg;
        document.querySelector('.popup_alarm').style.display = "block";
        checkVail = false;
    }

    if (!document.querySelector('#email').value) {
        document.querySelector('#email').focus();
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'email';
        }
    }

    if (document.querySelector('#domain').value == '') {
        document.querySelector('#domain').focus();
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'domain';
        }
    }

    if (
        document.querySelector('#domain').value == 'manual' &&
        !document.querySelector('#manualDomain').value
    ) {
        document.querySelector('#manualDomain').focus();
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'manualDomain';
        }
    }

    if (!checkVail) {
        document.querySelector('#emailMsg').style.display = 'block';
        document.querySelector('#'+ focusName).focus();
        return false;
    }

    let domain;
    if (document.querySelector('#domain').value == 'manual') {
        domain = document.querySelector('#manualDomain').value;
    } else {
        domain = document.querySelector('#domain').value;
    }
    const emailAddress = document.querySelector('#email').value + '@' + domain;
    
    // 메일로 인증코드 발송
    if(isEmail(emailAddress)){
        var param = {
            "custId": emailAddress
        };
        $.ajax({
            url: '/pub/api/member/join/confirmCust',
            data: param,
            type: 'post',
            async: false,
            success: function (response) {
                var msg = `입력하신 이메일<b>(${emailAddress})</b>으로 인증번호가 발송되었습니다.`;
                showMsgArea('emailMsg', msg);

                document.querySelector('#sendEmailHidInfo').value = emailAddress;
                document.querySelector('#reqVerificationCode').innerHTML = '재발송';
                document.querySelector('#reqVerificationCode').classList.add('blue');
                document.querySelector('#verificationCode').value = null;

            },
            error: function (response) {
                if (response.status == 410) { // 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
                    location.reload();
                } else if (response.status == 409) { // 등록된 아이디가 있음
                    var msg = `<b style="color:blue;">이미 등록된 이메일 입니다.</b>`;
                    showMsgArea('emailMsg', msg);
                } else if (response.status == 4490) { // 이미 ID에 인증번호가 발송된 상태
                    var msg = `인증 번호가 발송된 상태 입니다.`;
                    showMsgArea('emailMsg', msg);
                } else {
                    const jsonObj = JSON.parse(response.responseText);
                    showAlert(jsonObj.comment);
                }
            }
        });
    }else{
        var msg = `입력하신 이메일 <b style="color:red;">(${emailAddress})</b> 을 다시 확인해 주세요.`;
        showMsgArea('emailMsg', msg);
        return false;
    }
});

document.querySelector('#email').addEventListener('keyup', (e) => {

    let domain;
    if (document.querySelector('#domain').value == 'manual') {
        domain = document.querySelector('#manualDomain').value;
    } else {
        domain = document.querySelector('#domain').value;
    }

    const chkEmail = document.querySelector('#email').value + '@' + domain;
    const emailAddress = document.querySelector('#sendEmailHidInfo').value;

    if (emailAddress != "") {
        if (chkEmail != emailAddress) {
            var msg = `<b style="color:red;">변경하신 이메일로 인증번호 재발송이 필요합니다.</b>`;
            showMsgArea('emailMsg', msg);
            return false;
        }    
    }
    
});

//인증번호 확인 클릭 시
document.querySelector('#confirmVerificationCode').addEventListener('click', (e) => {
    let sendEmailHidInfo = document.querySelector('#sendEmailHidInfo').value;

    let codeVal = document.querySelector('#verificationCode').value;

    let domain;
    if (document.querySelector('#domain').value == 'manual') {
        domain = document.querySelector('#manualDomain').value;
    } else {
        domain = document.querySelector('#domain').value;
    }

    const emailAddress = document.querySelector('#email').value + '@' + domain;



    // 인증번호가 입력되지 않았거나 일치하지 않을 경우
    if (!codeVal) {
        document.querySelector('#verificationCodeMsg').style.display = 'block';
    } else {
        $.post("/pub/api/member/join/confirmAuthNo", { authNo: codeVal }, function (data, status) {
            document.querySelector('#confirmVerificationCode').innerHTML = '인증완료';
            document.querySelector('#confirmVerificationCode').classList.add('blue');
            document.querySelector('#confirmVerificationCode').style.pointerEvents = 'none';
            document.querySelector('#verificationCode').setAttribute('disabled', true);
            document.querySelector('#reqVerificationCode').setAttribute('disabled', true);
            document.querySelector('#email').setAttribute('disabled', true);
            document.querySelector('#domain').setAttribute('disabled', true);
            document.querySelector('#manualDomain').setAttribute('disabled', true);

            document.querySelector('#verificationCodeMsg').style.display = 'none'
            flagChecker('verifFlag', true);
            document.querySelector('#cust_id').value = emailAddress;
           
        })
        .fail(function (response) {
            if (response.status == 410) { // 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
                location.reload();
            } else if (response.status == 400) { // 인증번호 오류
                document.querySelector('#verificationCodeMsg').style.display = 'block';
            } else if (response.status == 4490) { // 인증 번호 발송을 요청 하지 않은 상태
                document.querySelector('#verificationCodeMsg').style.display = 'block';
            } else {
                document.querySelector('#verificationCodeMsg').style.display = 'block';
            }
        });
    }
});

// 비밀번호 유효성 체크
document.querySelector('#passwd').addEventListener('keyup', (e) => {
    let regexp = new RegExp(/^(?=.*?[a-zA-Z])(?=.*?\d)(?=.*?[!@#$%^&*+=\-[\]]).{8,20}$/);
    if (!regexp.test(e.target.value)) {
        document.querySelector('#passwdMsg').style.display = 'block';
        flagChecker('pwFlag', false);
    } else {
        document.querySelector('#passwdMsg').style.display = 'none';
        flagChecker('pwFlag', true);
    }
});
// 비밀번호 확인 체크
document.querySelector('#passwd_same').addEventListener('keyup', (e) => {
    if (e.target.value !== document.querySelector('#passwd').value) {
        document.querySelector('#passwd_sameMsg').style.display = 'block';
        flagChecker('pwCheckFlag', false);
    } else {
        document.querySelector('#passwd_sameMsg').style.display = 'none';
        flagChecker('pwCheckFlag', true);
    }
});

// 필수 약관 체크
document.querySelector('#terms_01').addEventListener('change', () => {
    if (document.querySelector('#terms_01').checked) {
        flagChecker('terms1Flag', true);
    } else {
        flagChecker('terms1Flag', false);
    }
});

document.querySelector('#terms_02').addEventListener('change', () => {
    if (document.querySelector('#terms_02').checked) {
        flagChecker('terms2Flag', true);
    } else {
        flagChecker('terms2Flag', false);
    }
});

// 전체 동의 클릭
document.querySelector('#checkAll').addEventListener('change', () => {
    const termsArr = ['#terms_01', '#terms_02', '#terms_03', '#terms_04', '#terms_05'];
    if (document.querySelector('#checkAll').checked) {
        termsArr.forEach((el) => {
            document.querySelector(el).checked !== true ? document.querySelector(el).click() : null;
        });
    } else {
        termsArr.forEach((el) => {
            document.querySelector(el).checked !== false
                ? document.querySelector(el).click()
                : null;
        });
    }
});

// 마켓동의 여부 체크 건수 확인
document.querySelectorAll('input[name=smtTerm]').forEach((el)=>{
    el.addEventListener('click',()=>{
        let true_cnt = 0;
        document.querySelectorAll('input[name=smtTerm]').forEach((el2)=>{
            if(el2.checked == true)
                true_cnt++;
        });
        
        if(true_cnt > 0)
            document.querySelector('#terms_03').checked = true;
        else
            document.querySelector('#terms_03').checked = false;    

        sellerbotTermAllFlag();    
    });
})

document.querySelector('#terms_03').addEventListener('change', () => {
    if (document.querySelector('#terms_03').checked) {
        document.querySelector('#SMTDM').checked = true;
        document.querySelector('#SMTKKT').checked = true;
        document.querySelector('#SMTMES').checked = true;
        document.querySelector('#SMTPHO').checked = true;
    } else {
        document.querySelector('#SMTDM').checked = false;
        document.querySelector('#SMTKKT').checked = false;
        document.querySelector('#SMTMES').checked = false;
        document.querySelector('#SMTPHO').checked = false;
    }
});

// 약관 텍스트 클릭 시 체크박스 클릭됨
document.querySelectorAll('.text_checkBox').forEach((el) => {
    el.addEventListener('click', () => {
        el.parentNode.querySelector('input.chkBox').click();
    });
});

// 숫자만 입력 가능 🔢
document.querySelectorAll('.onlyNumber').forEach((el) => {
    el.addEventListener('keyup', () => {
        el.value = el.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    });
});
// 셀로브캐시 약관동의
const sellerbotTerm = document.querySelectorAll('input[name="sellerbotTerm"]');
// 셀로브캐시 마켓동의
const smtTerm = document.querySelectorAll('input[name="smtTerm"]');

// 약관동의 라벨 클릭
document.querySelectorAll('.label').forEach((el) => {
    el.addEventListener('click',() =>{
        el.parentNode.querySelector('input[type="checkbox"]').click();
        sellerbotTermAllFlag();
    });
    
});

// 약관 동의 클릭
sellerbotTerm.forEach((el) =>{
    el.addEventListener('click',()=>{
        sellerbotTermAllFlag();
    });
})    

var sellerbotTermAllFlag = function(){
    let sellerbotTermAll = true;
    sellerbotTerm.forEach((el) =>{
        var term_type_cd = el.attributes['term_type_cd'].value;
        if(!el.checked){
            sellerbotTermAll = false;
        }
        if(term_type_cd == 'SMT' ){
            smtTerm.forEach((el2)=>{
                if(!el2.checked){
                    sellerbotTermAll = false;
                }
    
            })
                
        }
    })
    
    document.querySelector('#checkAll').checked = sellerbotTermAll;
   
}