//모달 알럿창 none 처리
document.querySelector('.popup_alarm').style.display = "none";

//모달 알럿창 닫기
document.querySelectorAll('.popup_alarm .pop_footer .close_btn, .popup_alarm .close_pop').forEach((el) => {
    el.addEventListener('click', () => {
        var _target = el.getAttribute('target');
        document.querySelector('#alertText').textContent = "";
        document.querySelector('.'+ _target).style.display = "none";
    });
});

// 메서지 영역 keyup 이벤트 발생 시 영역 none 처리
document.querySelectorAll('input[type=text], input[type=password]').forEach((el) => {
	el.addEventListener('keyup', (el) => {
        var _targetId = el.target.getAttribute('targetId');
        if (_targetId != null) {
            if(document.querySelector('#' + _targetId + 'Msg')){
                document.querySelector('#' + _targetId + 'Msg').style.display = "none";
            }
        }
	});
});


document.querySelectorAll('input[type=text], input[type=password]').forEach((el) => {
	el.addEventListener('keyup', (el) => {
        var _targetId = el.target.getAttribute('targetId');
        if (_targetId != null) {
            if(document.querySelector('#' + _targetId + 'Msg')){
                document.querySelector('#' + _targetId + 'Msg').style.display = "none";
            }
        }
	});
});