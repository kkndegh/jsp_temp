import modalShow from '/assets/new_join/js/import/modalShow.js';

document.querySelector('#prevStep').addEventListener('click', () => {
    location.href = '/pub/member/step2';
});

document.addEventListener('DOMContentLoaded', function() {
    //cafe24 or shopby siteCd 가 있는경우 유입경로 change 이벤트 동작
    if(document.querySelector('#spCode').value){
        let element = document.querySelector('#inf_path_cd');
        let event = new Event('change');
        element.dispatchEvent(event);

        document.querySelector('#inf_path_cd').disabled = true;
    }
});

const bizName = document.querySelector('#biz_nm');
const bizNum = document.querySelector('#biz_no');
const userName = document.querySelector('#ceo_nm');
const userPhone_1 = document.querySelector('#ceo_no1');
const userPhone_2 = document.querySelector('#ceo_no2');
const talkTime_1 = document.querySelector('#talkTime_1');
const managerName = document.querySelector('#chrg_nm');
const managerPhone_1 = document.querySelector('#chrg_no1');
const managerPhone_2 = document.querySelector('#chrg_no2');
const talkTime_2 = document.querySelector('#talkTime_2');
const funnel = document.querySelector('#inf_path_cd');
const userTalkTime_1 = document.querySelector('#trs_hop_hour1');

// 다음 버튼 활성화용 플래그
let flagObj = {
    bizNameFlag: false,
    bizNumFlag: false,
    userNameFlag: false,
    userPhone_2Flag: false,
    funnelFlag: false,
};
function flagChecker(flag, bool) {
    flagObj[flag] = bool;

    let notChecked = Object.keys(flagObj).find((key) => flagObj[key] === false);

    if (!notChecked) {
        document.querySelector('#nextStep').classList.add('active');
    } else {
        document.querySelector('#nextStep').classList.remove('active');
    }
}

bizName.addEventListener('keyup', () => {
    bizName.value = bizName.value.split(' ').join('');
    if (bizName.value) {
        document.querySelector('#biz_nmMsg').style.display = 'none';
        flagChecker('bizNameFlag', true);
    } else {
        document.querySelector('#biz_nmMsg').style.display = 'block';
        flagChecker('bizNameFlag', false);
    }
});

bizNum.addEventListener('keyup', () => {
    if (bizNum.value) {
        document.querySelector('#biz_noMsg').style.display = 'none';
        flagChecker('bizNumFlag', true);
    } else {
        document.querySelector('#biz_noMsg').innerHTML = '사업자번호를 입력해주세요';
        document.querySelector('#biz_noMsg').style.display = 'block';
        flagChecker('bizNumFlag', false);
    }
});

userName.addEventListener('keyup', () => {
    userName.value = userName.value.split(' ').join('');
    if (userName.value) {
        document.querySelector('#ceo_nmMsg').style.display = 'none';
        flagChecker('userNameFlag', true);
    } else {
        document.querySelector('#ceo_nmMsg').style.display = 'block';
        flagChecker('userNameFlag', false);
    }
});

userPhone_2.addEventListener('keyup', () => {
    if (userPhone_2.value) {
        document.querySelector('#msg4').style.display = 'none';
        flagChecker('userPhone_2Flag', true);
    } else {
        document.querySelector('#msg4').style.display = 'block';
        flagChecker('userPhone_2Flag', false);
    }
});

userTalkTime_1.addEventListener('change', () => {
    if (isNull(userTalkTime_1.value)) {
        document.querySelector('#trs_hop_hour2').setAttribute('disabled', true);
    } else {
        document.querySelector('#trs_hop_hour2').setAttribute('disabled', false);
    }
});

managerName.addEventListener('keyup',()=>{
    managerName.value = managerName.value.split(' ').join('');

    var trsHopHour2 = document.querySelector('#trs_hop_hour2').value;
    if (isNull(managerName.value) && isNull(trsHopHour2)) {
        document.querySelector('#chrg_no2Msg').style.display = 'none';
    }
});

funnel.addEventListener('change', () => {
    if (funnel.value) {
        document.querySelector('#inf_path_cdMsg').style.display = 'none';
        flagChecker('funnelFlag', true);
    } else {
        document.querySelector('#inf_path_cdMsg').style.display = 'block';
        flagChecker('funnelFlag', false);
    }
});

document.querySelector('#trs_hop_hour2').addEventListener('change', (el) => {
    if (isNull(el.target.value)) {
        document.querySelector('#chrg_nmMsg').style.display = 'none';
        document.querySelector('#chrg_no2Msg').style.display = 'none';
    } else {
        document.querySelector('#chrg_nmMsg').style.display = 'block';
        document.querySelector('#chrg_no2Msg').style.display = 'block';
    }
});

document.querySelector('#nextStep').addEventListener('click', () => {
    let checkVail = true;
    var focusName = '';

    if (!bizName.value) {
        document.querySelector('#biz_nmMsg').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'biz_nm';
        }
    }

    if (!bizNum.value) {
        document.querySelector('#biz_noMsg').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'biz_no';
        }
    } else {
        if (document.querySelector('#bizNumCheck').dataset.verified !== 'true') {
            document.querySelector('#biz_noMsg').innerHTML = '사업자번호 중복확인을 해주세요.';
            document.querySelector('#biz_noMsg').style.display = 'block';
            checkVail = false;
            if (isNull(focusName)) {
                focusName = 'bizNumCheck';
            }
        }
    }

    if (!userName.value) {
        document.querySelector('#ceo_nmMsg').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'ceo_nm';
        }
    }

    if (!userPhone_2.value) {
        document.querySelector('#msg4').textContent = '대표자 연락처를 입력해주세요.';
        document.querySelector('#msg4').style.display = 'block';
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'ceo_no2';
        }
    } else {
        let ceo_no1 = document.querySelector("#ceo_no1").value;
        let ceo_no2 = document.querySelector("#ceo_no2").value;
        
        var ceo_no = ceo_no1+ceo_no2
        if (!isMobile(ceo_no)) {
            document.querySelector('#msg4').textContent = '휴대폰 번호를 정확히 입력해주세요.';
            document.querySelector('#msg4').style.display = 'block';
            checkVail = false;
            if (isNull(focusName)) {
                focusName = 'ceo_no2';
            }
        } else {
            document.querySelector('#ceo_no').value = ceo_no;
        }
    }

    if (!funnel.value) {
        document.querySelector('#inf_path_cdMsg').style.display = 'block';
        flagChecker('funnelFlag', false);
        checkVail = false;
        if (isNull(focusName)) {
            focusName = 'inf_path_cd';
        }
    } else {
        document.querySelector('#inf_path_cdMsg').style.display = 'none';
        flagChecker('funnelFlag', true);
    }
    
    
    let chrg_no1 = document.querySelector("#chrg_no1").value;
    let chrg_no2 = document.querySelector("#chrg_no2").value;

    var chrgNm = document.querySelector('#chrg_nm').value;
    var trsHopHour2 = document.querySelector('#trs_hop_hour2').value;

    if (nonNull(trsHopHour2)) {
        if (isNull(chrgNm)) {
            document.querySelector('#chrg_nmMsg').style.display = 'block';
            checkVail = false;
            if (isNull(focusName)) {
                focusName = 'chrg_nm';
            }
        }
        
        if (isNull(chrg_no2)) {
            document.querySelector('#chrg_no2Msg').textContent = '담당자 연락처를 입력해주세요.';
            document.querySelector('#chrg_no2Msg').style.display = 'block';
            checkVail = false;
            if (isNull(focusName)) {
                focusName = 'chrg_no2';
            }
        } else {
            var chrg_no = chrg_no1+chrg_no2
            if (!isMobile(chrg_no)) {
                document.querySelector('#chrg_no2Msg').textContent = '휴대폰 번호를 정확히 입력해주세요.';
                document.querySelector('#chrg_no2Msg').style.display = 'block';
                checkVail = false;
                if (isNull(focusName)) {
                    focusName = 'chrg_no2';
                }
            } else {
                document.querySelector('#chrg_no').value = chrg_no;
            }
        }
    } else {
        if (isNull(chrgNm) && nonNull(chrg_no2)) {
            document.querySelector('#chrg_nmMsg').style.display = 'block';
            checkVail = false;
            if (isNull(focusName)) {
                focusName = 'chrg_nm';
            }
        }
        
        if (isNull(chrg_no2)) {
            if (nonNull(chrgNm)) {
                document.querySelector('#chrg_no2Msg').textContent = '담당자 연락처를 입력해주세요.';
                document.querySelector('#chrg_no2Msg').style.display = 'block';
                checkVail = false;
                if (isNull(focusName)) {
                    focusName = 'chrg_no2';
                }
            }
        } else {
            var chrg_no = chrg_no1+chrg_no2
            if (!isMobile(chrg_no)) {
                document.querySelector('#chrg_no2Msg').textContent = '휴대폰 번호를 정확히 입력해주세요.';
                document.querySelector('#chrg_no2Msg').style.display = 'block';
                checkVail = false;
                if (isNull(focusName)) {
                    focusName = 'chrg_no2';
                }
            } else {
                document.querySelector('#chrg_no').value = chrg_no;
            }
        }
    }

    if (!checkVail) {
        document.querySelector('#'+ focusName).focus();
        return false;
    }

    document.querySelector('#req_dt').value = fnGetToDay("YYYYMMDD");

    if (document.querySelector('#nextStep').classList.contains('active')) {
        showLoadingModal();
        document.querySelector('#inf_path_cd').disabled = false;
        document.getElementById('form').submit();
    }
});

// 사업자번호 중복체크
document.querySelector('#bizNumCheck').addEventListener('click', () => {
    if (!bizNum.value) {
        // 미입력
        document.querySelector('#biz_noMsg').innerHTML = '사업자번호를 입력해주세요';
        document.querySelector('#biz_noMsg').style.display = 'block';
    } else if (bizNum.value.length > 10) {
        // 오입력 (10자리 미만)
        document.querySelector('#biz_noMsg').innerHTML = '사업자 번호가 정확하지 않습니다';
        document.querySelector('#biz_noMsg').style.display = 'block';
    }
    else {
        // 유효함
        if(validateBizNo(bizNum.value)) {
            $.post("/pub/api/member/join/checkBizNo", { bizNo: bizNum.value }, function (data, status) {
                document.querySelector('#biz_noMsg').innerHTML = '<b style="color:#3D78FF;">사용 가능한 사업자번호입니다.</b>';
                document.querySelector('#biz_noMsg').classList.add('blue');
                document.querySelector('#biz_noMsg').style.display = 'block';

                document.querySelector('#bizNumCheck').dataset.verified = 'true';
                document.querySelector('#bizNumCheck').style.pointerEvents = 'none';
                bizNum.setAttribute('disabled', true);
            }).fail(function (response) {
                if (response.status == 410) { // 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
                    location.reload();
                } else if (response.status == 409) { // 사업자 번호 확인 오류
                    if ("AUTH-4003" == response.responseText) {
                        document.querySelector('#biz_noMsg').innerHTML = '이미 등록된 사업자 번호 입니다.';
                        document.querySelector('#biz_noMsg').style.display = 'block';
                    } else if ("AUTH-4005" == response.responseText) {
                        document.querySelector('#biz_noMsg').innerHTML = '입력하신 사업자번호가 유효 하지 않습니다.';
                        document.querySelector('#biz_noMsg').style.display = 'block';
                    } else if ("AUTH-4307" == response.responseText) {
                        document.querySelector('#biz_noMsg').innerHTML = "탈퇴한 회원입니다. <a href='https://9p9j3.channel.io/lounge' target='_blank' style='color: blue'>챗봇으로 문의하기</a>";
                        document.querySelector('#biz_noMsg').style.display = 'block';
                    } else {
                        document.querySelector('#biz_noMsg').innerHTML = response.responseText;
                        document.querySelector('#biz_noMsg').style.display = 'block';
                    }
                } else {
                    alert("Error: " + response.status);
                }
            });
        }else{
            document.querySelector('#biz_noMsg').innerHTML = '사업자 번호가 정확하지 않습니다';
            document.querySelector('#biz_noMsg').style.display = 'block';
        }    
    }
});

document.querySelectorAll('#talkInfo').forEach((el) => {
    let talkInfoHtml = `
    <div class="wrapper">
    <div class="left">
        <img src="/assets/new_join/img/talkInfoModal.png" alt="talkExample">
    </div>
    <div class="right">
        <div class="row flexRight">
            <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot">
        </div>
        <div class="flexRow">
            <h2>셀러봇캐시 알림톡</h2>
            <span class="line"></span>
        </div>
        <div class="row">
            <p>정산예정금을 판매몰별, 배송상태별, 일자별로 꼼꼼히 계산해서 지정하신 시간에 리포트를 보내드립니다.</p>
        </div>
        <div class="grayBox">
            <h3>셀러봇캐시 알림톡 체험</h3>
            <div class="flexRow">
                <select id="talk_userPhone_1">
                    <option value="010" selected>010</option>
                </select>
                <input class="onlyNumber" type="text" id="talk_userPhone_2" maxLength = "8">
                <button type="button" id="submit_ph">발송</button>
                <span class="underMsg" id ="underMsg">입력하신 연락처로 알림톡 SAMPLE 메세지가 발송됩니다.<br>
                    ( 샘플 발송 외 다른목적 사용 X )</span>
            </div>
        </div>
    </div>
</div>`;
    el.addEventListener('click', () => {
        modalShow({
            id: 'talkInfo',
            content: talkInfoHtml,
            function: () => {
                document.querySelector('.popupBody').style.overflowY = "auto";
                // console.log(document.querySelector('.popupBody').outerHTML);
                document.querySelectorAll('.onlyNumber').forEach((el) => {
                    el.addEventListener('keyup', () => {
                        el.value = el.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
                    });
                });
                document.querySelector("#submit_ph").addEventListener('click',() => {
                    var talk_userPhone_1 = $("#talk_userPhone_1").val();
                    var talk_userPhone_2 = $("#talk_userPhone_2").val();
                    let tgt_cust_no = talk_userPhone_1 + talk_userPhone_2;
                    
                    if (!isMobile(tgt_cust_no)) {
                        document.querySelector("#underMsg").innerHTML = "휴대폰 번호를 정확히 입력해주세요.";
                        document.querySelector("#underMsg").style.color ="red";
                        return false;
                    }

                    $.ajax({
                        url: "/pub/trial",
                        data: {
                            'tgt_cust_no': tgt_cust_no
                        },
                        type: 'POST',
                        success: function (data) {
                            if ("OK" == data) {
                                document.querySelector("#underMsg").innerHTML = "알림톡 체험 신청 되었습니다.";
                                document.querySelector("#underMsg").style.color ="blue";
                                return false;
                            } else {
                                document.querySelector("#underMsg").innerHTML ="알림톡 체험을 이미 이용한 번호입니다.";
                                document.querySelector("#underMsg").style.color ="red";
                                return false;
                            }
                        },
                        error: function (error) {
                            document.querySelector("#underMsg").innerHTML ="알림톡 체험 신청에 실패하였습니다.";
                            document.querySelector("#underMsg").style.color ="red";
                            return false;
                        }
                    });
                });
            },
        });
    });

    
});

// 숫자만 입력 가능 🔢
document.querySelectorAll('.onlyNumber').forEach((el) => {
    el.addEventListener('keyup', () => {
        el.value = el.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    });
});

function validateBizNo(bizID) {
    bizID = bizID + "";
    // bizID는 숫자만 10자리로 해서 문자열로 넘긴다. 
    var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
    var tmpBizID, i, chkSum = 0, c2, remander;
    bizID = bizID.replace(/-/gi, '');

    for (i = 0; i <= 7; i++) chkSum += checkID[i] * bizID.charAt(i);
    c2 = "0" + (checkID[8] * bizID.charAt(8));
    c2 = c2.substring(c2.length - 2, c2.length);
    chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
    remander = (10 - (chkSum % 10)) % 10;

    if (Math.floor(bizID.charAt(9)) == remander) return true; // OK! 
    return false;
}