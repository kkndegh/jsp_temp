
import modalShow from './import/modalShow.js';
import createElement from './import/createElement_old.js';
import {
    modalHide_old,
    modalInit_old,
    modalLoad_old,
    modalShow_old,
} from './import/modalShow_old.js';

(async () => {
    $(document).ready(function(){
        // 한번에 업데이트    
        $(document).on('click' ,'#updateAll',function(){
            var formData = new Array();
            var emptyNoData = new Array();
            var modifyLength = $('.modifyArea').children('div').length;
            var registerLength = $('.registerArea').children('div').length;
            var divLength = $('.modifyArea , .registerArea').children('div').length;
            var overLapBoolean = true;
            var sumLength = modifyLength + registerLength;
			
            if(divLength > 0 ){	
				$('#updateAll').attr('disabled',true);
                $.each($('.modifyArea , .registerArea').children('div') , function(indxe , item ){
                    var valBoolean = true;
                    const $this = $(this);
                    var div_id = $(this).attr('id');
                    var div_type = $(this).data('div_type');
                    var cust_mall_seq_no = $this.data('seq_no');
                    var cert_step = $this.data('cert_step');
                    var passwd_step = $this.data('passwd_step');
                    var mall_cd = $this.data('mall_cd');
                    var sub_mall_cert_1st_nm = $this.data('sub_mall_cert_1st_nm');
                    var sub_mall_cert_2nd_nm = $this.data('sub_mall_cert_2nd_nm');
                    var mall_cert_2nd_passwd_nm = $this.data('mall_cert_2nd_passwd_nm');
                    var sub_mall_auth_1st_esse_yn = $this.data('sub_mall_auth_1st_esse_yn');
                    var sub_mall_auth_2nd_esse_yn = $this.data('sub_mall_auth_2nd_esse_yn');
                    var data_biz_no_auth_esse_yn = $this.data('data_biz_no_auth_esse_yn');
                    var otp_auth_typ_cd  = $this.data('data-otp_auth_typ_cd');
                    var otp_auth_esse_yn = $this.data('data-otp_auth_esse_yn');
                    var mall_cert_1st_id = $this.children().find('.mall_cert_1st_id').val();    
                    var mall_cert_1st_passwd = $this.children().find('.mall_cert_1st_passwd').val();
                    var sub_mall_cert_1st = $this.children().find('.sub_mall_cert_1st').val();
                    var sub_mall_cert_2nd = $this.children().find('.sub_mall_cert_2nd').val();
                    var mall_cert_2nd_passwd = $this.children().find('.mall_cert_2nd_passwd').val();
                    var goods_cate_seq_no = $this.children().find("select[name='goods_cate_seq_no']").val();
                    var mall_cert_typ_cd = $this.children().find("select[name='mall_cert_typ_cd']").val();
                    var overlapDataChk = mall_cd+mall_cert_1st_id+sub_mall_cert_1st+sub_mall_cert_2nd;
                    var mall_auth_otp_key = $this.children().find('.mall_auth_otp_key').val();
                    var otp_auth_use_yn = $this.data('otp_auth_use_yn');
                    var regexBoolean = true;
                    var overlapDataChk = "";
                    //var regex = /^[ㄱ-ㅎ|가-힣|a-z|A-Z|0-9|]+$/;
                    // if(!regex.test(mall_cert_1st_id)){
                    //     valBoolean = false;
                    //     regexBoolean = false;
                    // }


                    if(mall_cert_1st_id != null && mall_cert_1st_id !=''){
                        mall_cert_1st_id = mall_cert_1st_id.split(' ').join('');
                    }
        
                    if(mall_cert_1st_passwd != null && mall_cert_1st_passwd !=''){
                        mall_cert_1st_passwd = mall_cert_1st_passwd.split(' ').join('');
                    }
        
                    if(sub_mall_cert_1st != null && sub_mall_cert_1st !=''){
                        sub_mall_cert_1st = sub_mall_cert_1st.split(' ').join('');
                    }
                    
                    if(sub_mall_cert_2nd != null && sub_mall_cert_2nd !=''){
                        sub_mall_cert_2nd = sub_mall_cert_2nd.split(' ').join('');
                    }
        
                    if(mall_cert_2nd_passwd != null && mall_cert_2nd_passwd !='' ){
                        mall_cert_2nd_passwd = mall_cert_2nd_passwd.split(' ').join('');
                    }

                    overlapDataChk = mall_cd+mall_cert_1st_id+sub_mall_cert_1st+sub_mall_cert_2nd;
                
                    if(mall_cert_1st_id == null || mall_cert_1st_id ==''){
                        valBoolean = false;
                    }        
        
                    if(mall_cert_1st_passwd == null || mall_cert_1st_passwd ==''){
                        valBoolean = false;
                    }    
                    
                    if(otp_auth_use_yn == 'Y'){
                        if(mall_auth_otp_key == null || mall_auth_otp_key == ''){
                            valBoolean = false;
                        }
                    }

                    if(sub_mall_auth_1st_esse_yn == 'Y'){
                        if(sub_mall_cert_1st_nm != null && sub_mall_cert_1st_nm != ''){
                            if(sub_mall_cert_1st == null || sub_mall_cert_1st ==''){
                                valBoolean = false;
                            }    
                        }
                    }

                    if(sub_mall_auth_2nd_esse_yn == 'Y'){
                        if(sub_mall_cert_2nd_nm != null && sub_mall_cert_2nd_nm != ''){
                            if(sub_mall_cert_2nd == null || sub_mall_cert_2nd ==''){
                                valBoolean = false;
                            }    
                        }
                    }
        
                    if(mall_cert_2nd_passwd_nm != null && mall_cert_2nd_passwd_nm != ''){
                        if(mall_cert_2nd_passwd == null || mall_cert_2nd_passwd ==''){
                            valBoolean = false;
                        }    
                    }
        
                    if(goods_cate_seq_no == null || goods_cate_seq_no == ''){
                        valBoolean = false;
                    }
                    
                    if ((mall_cd == "005" || mall_cd == "006") && valBoolean == true) {
                        if (sub_mall_cert_1st == "" && sub_mall_cert_2nd == "") {
                            valBoolean = true;
                        } else {
                            // 20210420 요청 
                            // 업체 번호 및 공급계약번호 정보 중 하나의 정보만 입력되여 있다면 2개의 정보 다 입력 하도록.
                            if (sub_mall_cert_1st == "") {
                                valBoolean = false;
                            } 

                            if (sub_mall_cert_2nd == "") {
                                valBoolean = false;
                            } 
                        }
                    } 
                    
                    var pushData  = {
                        div_id : div_id,
                        div_type : div_type,
                        cust_mall_seq_no : cust_mall_seq_no,
                        cert_step : cert_step,
                        passwd_step : passwd_step,
                        mall_cd : mall_cd,
                        mall_cert_1st_id : mall_cert_1st_id,
                        mall_cert_1st_passwd : mall_cert_1st_passwd,
                        sub_mall_cert_1st : sub_mall_cert_1st,
                        sub_mall_cert_2nd : sub_mall_cert_2nd,
                        mall_cert_2nd_passwd : mall_cert_2nd_passwd,
                        goods_cate_seq_no : goods_cate_seq_no,
                        sub_mall_cert_1st_nm : sub_mall_cert_1st_nm,
                        sub_mall_cert_2nd_nm : sub_mall_cert_2nd_nm,
                        mall_cert_2nd_passwd_nm : mall_cert_2nd_passwd_nm,
                        mall_cert_typ_cd : mall_cert_typ_cd,
                        overlapDataChk : overlapDataChk,
                        sub_mall_auth_1st_esse_yn : sub_mall_auth_1st_esse_yn,
                        sub_mall_auth_2nd_esse_yn : sub_mall_auth_2nd_esse_yn,
                        data_biz_no_auth_esse_yn : data_biz_no_auth_esse_yn,
                        otp_auth_use_yn : otp_auth_use_yn,
                        mall_auth_otp_key : mall_auth_otp_key,
                    };
                    
                    $.each(formData,function(i,v){    
                        overLapBoolean = true;
                        if(v.overlapDataChk == overlapDataChk){
                            $('#'+div_id+'').find("#deleteItem").trigger("click");    
                            overLapBoolean = false;
                        }
                    });

                    if(overLapBoolean){
                        if(valBoolean){
                            formData.push(pushData);
                        }else {
                            emptyNoData.push(pushData);
                        }
                    }
                });
                
                var currRegCnt =  $("#currentRegCount").val();
                currRegCnt = (typeof(currRegCnt) == "number") ? currRegCnt : parseInt(currRegCnt);
                var currentRegCount = currRegCnt + $('.registerArea').children('div').length - 1;
                var maxRegCount = $("#maxRegCount").val();
				
                // if(!fnIsRegData(currentRegCount, maxRegCount)) {
                //     showAlert("현재 이용 중이신 서비스는<br>판매몰 " + $("#maxRegCount").val() + "개까지만 등록이 가능합니다.");
                //     return;
                // }

                var url = "";
                var message = "";
                var errorMessage = "";
                url = '/sub/my/join/custMallSave';
                message = "저장 되었습니다.";
                errorMessage = "저장 실패하였습니다.";

                if(formData.length > 0){
                    $.ajax({
                        url: '/sub/my/join/custMallSave'
                        , type: 'post'
                        , async: true
                        , dataType: 'json'
                        , contentType: 'application/json'
                        , data: JSON.stringify(formData)
                        , success: function (data) {
                            var successCnt = 0;
                            var failCnt = 0;
                            var failMessage = '';
                            $.each(data,function(i,v){
                                if(v.result_code == 'fail'){
                                    $('#'+v.mall_id+'').children().children('.errorMsg').text(v.result_message);
                                    failMessage = v.result_message;
                                    failCnt++;
                                }else{
                                    successCnt++;
                                    $('#'+v.mall_id+'').find("#deleteItem").trigger("click");    
                                }
    
                            });

                            if(successCnt > 0){
                                let ceo_no = $("#ceo_no").val();
                                let full_no = ceo_no.substr(0, 3) + "-" + "****" + "-" + ceo_no.substr(7, 4);

                                let html = 
                                    "<h2>판매몰 등록이 완료되었습니다</h2>"+
                                    "<br>"+
                                    "<p>등록 결과를 확인하는 데는 최대 1시간까지 소요될 수 있습니다.<br>"+
                                    "결과는 회원가입 시 등록된 휴대전화 번호로 알림톡을 발송해 드립니다."+
                                    "</p>"+
                                    "<br>"+
                                    "<h3>휴대전화 번호 :" + full_no + "</h3>";

                                modalShow_old(html, {
                                    width: modalSize['largeSize'],
                                    button: [
                                        {
                                            html: '확인',
                                            class: 'blue',
                                            click: () => {
                                                userMallListMaker();
                                                fn_delLi();
                                                fn_modLi();    
                                                modalHide_old();
                                                if(emptyNoData.length > 0){
                                                    $.each(emptyNoData, function(i,val){
                                                        var validation = fn_validation(val);
                                                        return validation;
                                                    });
                                                }
                                            },
                                        },
                                    ],
                                });
                            }else {
                                modalShow_old(`저장에 실패 하였습니다.`, {
                                    width: modalSize['smallSize'],
                                    button: [
                                        {
                                            html: '확인',
                                            class: 'blue',
                                            click: () => {
                                                modalHide_old();
                                            },
                                        },
                                    ],
                                });
                            }
							$('#updateAll').attr('disabled',false);
                        }
                        , error: function () {
    
                            modalShow_old(`저장에 실패 하였습니다.`, {
                                width: modalSize['smallSize'],
                                button: [
                                    {
                                        html: '확인',
                                        class: 'blue',
                                        click: () => {
                                            modalHide_old();
                                        },
                                    },
                                ],
                            });     
							
							$('#updateAll').attr('disabled',false);
                        }
                    });
                }else {
                    if(emptyNoData.length > 0){
                        $.each(emptyNoData, function(i,val){
                            var validation = fn_validation(val);
                            return validation;
                        });
                    }

					$('#updateAll').attr('disabled',false);
                }

            }else{    
                modalShow_old(`현재 선택한 판매몰은 0건 입니다.`, {
                    width: modalSize['smallSize'],
                    button: [
                        {
                            html: '확인',
                            class: 'blue',
                            click: () => {
                                modalHide_old();
                            },
                        },
                    ],
                });
            }        

		
        });

        $(document).on('click','.main',function(){
            var $this = $(this);
            var help_use_yn = $this.parent('.registerBox, .modifyBox').data('help_use_yn');
            var help_info = $this.parent('.registerBox, .modifyBox').data('help_info');
            var video_use_yn = $this.parent('.registerBox, .modifyBox').data('video_use_yn');
            var video_url = $this.parent('.registerBox, .modifyBox').data('video_url');
            var detail_use_yn = $this.parent('.registerBox, .modifyBox').data('detail_use_yn');
            var detail_url = $this.parent('.registerBox, .modifyBox').data('detail_url');
            var cust_mall_sts_cd = $this.parent('.registerBox, .modifyBox').data('cust_mall_sts_cd');
            var err_msg = $this.parent('.registerBox, .modifyBox').data('err_msg');
            var mall_cd = $this.parent('.registerBox, .modifyBox').data('mall_cd');
            var mall_nm = $this.children('.top').children('#mall_nm, #mallName').text();
            var err_msg = $this.parent('.registerBox, .modifyBox').data('err_msg');
            var measure = $this.parent('.registerBox, .modifyBox').data('measure');
            var post_yn = $this.parent('.registerBox, .modifyBox').data('post_yn');
            var err_msg_use_yn = $this.parent('.registerBox, .modifyBox').data('err_msg_use_yn');
            var measure_yn = $this.parent('.registerBox, .modifyBox').data('measure_yn');
            
            var manualBtn = $('#article_2 .flexArea .right .topTitle button'); 
            manualBtn.attr("data-mall_nm",mall_nm);
            manualBtn.attr("data-help_info",help_info);
            manualBtn.attr("data-err_msg",err_msg);
            manualBtn.attr("data-measure",measure);
            manualBtn.attr("data-post_yn",post_yn);
            manualBtn.attr("data-err_msg_use_yn",err_msg_use_yn);
            manualBtn.attr("data-measure_yn",measure_yn);

            fn_helpInfo(mall_cd , mall_nm 
                    , help_use_yn , help_info  
                    , video_use_yn , video_url
                    , detail_use_yn , detail_url
                    , cust_mall_sts_cd , err_msg
                    , measure    , post_yn
                    , err_msg_use_yn , measure_yn
            );

        });

        // 확인 버튼
        $(document).on('click','#registerOkBtn',function(e){
            var formData = new Array();
            var $this = $(this).parent('.registerBox');
            var div_id = $this.attr('id');
            var div_type = $this.data('div_type');
            var cust_mall_seq_no = $this.data('seq_no');
            var cert_step = $this.data('cert_step');
            var passwd_step = $this.data('passwd_step');
            var mall_cd = $this.data('mall_cd');
            var sub_mall_cert_1st_nm = $this.data('sub_mall_cert_1st_nm');
            var sub_mall_cert_2nd_nm = $this.data('sub_mall_cert_2nd_nm');
            var mall_cert_2nd_passwd_nm = $this.data('mall_cert_2nd_passwd_nm');
            var sub_mall_auth_1st_esse_yn = $this.data('sub_mall_auth_1st_esse_yn');
            var sub_mall_auth_2nd_esse_yn = $this.data('sub_mall_auth_2nd_esse_yn');
            var data_biz_no_auth_esse_yn = $this.data('data_biz_no_auth_esse_yn');

            var mall_cert_1st_id = $this.children().find('.mall_cert_1st_id').val();    
            var mall_cert_1st_passwd = $this.children().find('.mall_cert_1st_passwd').val();

            var sub_mall_cert_1st = $this.children().find('.sub_mall_cert_1st').val();
            var sub_mall_cert_2nd = $this.children().find('.sub_mall_cert_2nd').val();
            var mall_cert_2nd_passwd = $this.children().find('.mall_cert_2nd_passwd').val();
            var goods_cate_seq_no = $this.children().find("select[name='goods_cate_seq_no']").val();
            var mall_cert_typ_cd = $this.children().find("select[name='mall_cert_typ_cd']").val();
            var mall_auth_otp_key = $this.children().find('.mall_auth_otp_key').val();
            var otp_auth_use_yn = $this.data('otp_auth_use_yn');

            if(mall_cert_1st_id != null && mall_cert_1st_id !=''){
                mall_cert_1st_id = mall_cert_1st_id.split(' ').join('');
            }

            if(mall_cert_1st_passwd != null && mall_cert_1st_passwd !=''){
                mall_cert_1st_passwd = mall_cert_1st_passwd.split(' ').join('');
            }

            if(sub_mall_cert_1st != null && sub_mall_cert_1st !=''){
                sub_mall_cert_1st = sub_mall_cert_1st.split(' ').join('');
            }
            
            if(sub_mall_cert_2nd != null && sub_mall_cert_2nd !=''){
                sub_mall_cert_2nd = sub_mall_cert_2nd.split(' ').join('');
            }

            if(mall_cert_2nd_passwd != null && mall_cert_2nd_passwd !='' ){
                mall_cert_2nd_passwd = mall_cert_2nd_passwd.split(' ').join('');
            }
            
            var pushData  = {
                div_id : div_id,
                div_type : div_type,
                cust_mall_seq_no : cust_mall_seq_no,
                cert_step : cert_step,
                passwd_step : passwd_step,
                mall_cd : mall_cd,
                mall_cert_1st_id : mall_cert_1st_id,
                mall_cert_1st_passwd : mall_cert_1st_passwd,
                sub_mall_cert_1st : sub_mall_cert_1st,
                sub_mall_cert_2nd : sub_mall_cert_2nd,
                mall_cert_2nd_passwd : mall_cert_2nd_passwd,
                goods_cate_seq_no : goods_cate_seq_no,
                sub_mall_cert_1st_nm : sub_mall_cert_1st_nm,
                sub_mall_cert_2nd_nm : sub_mall_cert_2nd_nm,
                mall_cert_2nd_passwd_nm : mall_cert_2nd_passwd_nm,
                mall_cert_typ_cd : mall_cert_typ_cd,
                sub_mall_auth_1st_esse_yn : sub_mall_auth_1st_esse_yn,
                sub_mall_auth_2nd_esse_yn : sub_mall_auth_2nd_esse_yn,
                data_biz_no_auth_esse_yn : data_biz_no_auth_esse_yn,
                mall_auth_otp_key : mall_auth_otp_key,
                otp_auth_use_yn    : otp_auth_use_yn
            };

            formData.push(pushData);

            var validation = fn_validation(formData[0]);

            if(validation){
                $.ajax({
                    url: '/sub/my/join/custMallSave'
                    , type: 'post'
                    , async: false
                    , dataType: 'json'
                    , contentType: 'application/json'
                    , data: JSON.stringify(formData)
                    , success: function (data) {
                        var successCnt = 0;
                        var failCnt = 0;
                        var failMessage = '';
                        var result = true;
                        $.each(data,function(i,v){
                            result = false;
                            if(v.result_code == 'fail'){
                                $('#'+v.mall_id+'').children().children('.errorMsg').text(v.result_message);
                                failMessage = v.result_message;
                                failCnt++;
                            }else{
                                successCnt++;
                                $('#'+v.mall_id+'').find("#deleteItem").trigger("click");    
                            }
                        });    
    
                        if(successCnt > 0){
                            let ceo_no = $("#ceo_no").val();
                            let full_no = ceo_no.substr(0, 3) + "-" + "****" + "-" + ceo_no.substr(7, 4);

                            let html = 
                                "<h2>판매몰 등록이 완료되었습니다</h2>"+
                                "<br>"+
                                "<p>등록 결과를 확인하는 데는 최대 1시간까지 소요될 수 있습니다.<br>"+
                                "결과는 회원가입 시 등록된 휴대전화 번호로 알림톡을 발송해 드립니다."+
                                "</p>"+
                                "<br>"+
                                "<h3>휴대전화 번호 :" + full_no + "</h3>";

                            modalShow_old(html, {
                                width: modalSize['largeSize'],
                                button: [
                                    {
                                        html: '확인',
                                        class: 'blue',
                                        click: () => {
                                            userMallListMaker();    
                                            fn_delLi();
                                            fn_modLi();
                                            modalHide_old();
                                        },
                                    },
                                ],
                            });
                        }else {
                            modalShow_old(failMessage, {
                                width: modalSize['smallSize'],
                                button: [
                                    {
                                        html: '확인',
                                        class: 'blue',
                                        click: () => {
                                            modalHide_old();
                                        },
                                    },
                                ],
                            });
                        }    
                    }
                    , error: function () {
                        modalShow_old(`저장에 실패 하였습니다..`, {
                            width: modalSize['smallSize'],
                            button: [
                                {
                                    html: '확인',
                                    class: 'silver',
                                    click: () => {
                                        modalHide_old();
                                    },
                                },
                            ],
                        });                
                    }
                });
            }    
                // ajax end    
        });

        $(document).on('click','#modifyOkBtn',function(e){
            var formData = new Array();
            var nullChk = true;
            var $this = $(this).parent('.modifyBox');
            var div_id = $this.attr('id');
            var div_type = $this.data('div_type');
            var cust_mall_seq_no = $this.data('seq_no');
            var cert_step = $this.data('cert_step');
            var passwd_step = $this.data('passwd_step');
            var mall_cd = $this.data('mall_cd');
            var sub_mall_cert_1st_nm = $this.data('sub_mall_cert_1st_nm');
            var sub_mall_cert_2nd_nm = $this.data('sub_mall_cert_2nd_nm');
            var mall_cert_2nd_passwd_nm = $this.data('mall_cert_2nd_passwd_nm');
            var sub_mall_auth_1st_esse_yn = $this.data('sub_mall_auth_1st_esse_yn');
            var sub_mall_auth_2nd_esse_yn = $this.data('sub_mall_auth_2nd_esse_yn');
            var data_biz_no_auth_esse_yn = $this.data('data_biz_no_auth_esse_yn');

            var mall_cert_1st_id = $this.children().find('.mall_cert_1st_id').val();    
            var mall_cert_1st_passwd = $this.children().find('.mall_cert_1st_passwd').val();

            var sub_mall_cert_1st = $this.children().find('.sub_mall_cert_1st').val();
            var sub_mall_cert_2nd = $this.children().find('.sub_mall_cert_2nd').val();
            var mall_cert_2nd_passwd = $this.children().find('.mall_cert_2nd_passwd').val();
            var goods_cate_seq_no = $this.children().find("select[name='goods_cate_seq_no']").val();
            var mall_cert_typ_cd = $this.children().find("select[name='mall_cert_typ_cd']").val();
            var mall_auth_otp_key = $this.children().find('.mall_auth_otp_key').val();
            var otp_auth_use_yn = $this.data('otp_auth_use_yn');    

            if(mall_cert_1st_id != null && mall_cert_1st_id !=''){
                mall_cert_1st_id = mall_cert_1st_id.split(' ').join('');
            }

            if(mall_cert_1st_passwd != null && mall_cert_1st_passwd !=''){
                mall_cert_1st_passwd = mall_cert_1st_passwd.split(' ').join('');
            }

            if(sub_mall_cert_1st != null && sub_mall_cert_1st !=''){
                sub_mall_cert_1st = sub_mall_cert_1st.split(' ').join('');
            }
            
            if(sub_mall_cert_2nd != null && sub_mall_cert_2nd !=''){
                sub_mall_cert_2nd = sub_mall_cert_2nd.split(' ').join('');
            }

            if(mall_cert_2nd_passwd != null && mall_cert_2nd_passwd !='' ){
                mall_cert_2nd_passwd = mall_cert_2nd_passwd.split(' ').join('');
            }

            var pushData  = {
                div_id : div_id,
                div_type : div_type,
                cust_mall_seq_no : cust_mall_seq_no,
                cert_step : cert_step,
                passwd_step : passwd_step,
                mall_cd : mall_cd,
                mall_cert_1st_id : mall_cert_1st_id,
                mall_cert_1st_passwd : mall_cert_1st_passwd,
                sub_mall_cert_1st : sub_mall_cert_1st,
                sub_mall_cert_2nd : sub_mall_cert_2nd,
                mall_cert_2nd_passwd : mall_cert_2nd_passwd,
                goods_cate_seq_no : goods_cate_seq_no,
                mall_cert_typ_cd : mall_cert_typ_cd,
                sub_mall_cert_1st_nm : sub_mall_cert_1st_nm,
                sub_mall_cert_2nd_nm : sub_mall_cert_2nd_nm,
                mall_cert_2nd_passwd_nm : mall_cert_2nd_passwd_nm,
                sub_mall_auth_1st_esse_yn : sub_mall_auth_1st_esse_yn,
                sub_mall_auth_2nd_esse_yn : sub_mall_auth_2nd_esse_yn,
                data_biz_no_auth_esse_yn : data_biz_no_auth_esse_yn,
                mall_auth_otp_key : mall_auth_otp_key,
                otp_auth_use_yn    : otp_auth_use_yn

            };

            formData.push(pushData);

            var validation = fn_validation(formData[0]);

            if(validation){
            $.ajax({
                url: '/sub/my/join/custMallSave'
                , type: 'post'
                , async: false
                , dataType: 'json'
                , contentType: 'application/json'
                , data: JSON.stringify(formData)
                , success: function (data) {
                    var successCnt = 0;
                    var failCnt = 0;
                    $.each(data,function(i,v){
                        if(v.result_code == 'fail'){
                            $('#'+v.mall_id+'').children().children('.errorMsg').text(v.result_message);
                            failCnt++;
                        }else{
                            successCnt++;
                            $('#'+v.mall_id+'').find("#deleteItem").trigger("click");    
                        }
                    });

                        if(successCnt > 0){
                            modalShow_old(`수정 되었습니다.`, {
                                width: modalSize['smallSize'],
                                button: [
                                    {
                                        html: '확인',
                                        class: 'blue',
                                        click: () => {
                                            userMallListMaker();    
                                            fn_delLi();
                                            fn_modLi();
                                            modalHide_old();
                                        },
                                    },
                                ],
                            });
                        }else {
                            modalShow_old(`수정에 실패 하였습니다.`, {
                                width: modalSize['smallSize'],
                                button: [
                                    {
                                        html: '확인',
                                        class: 'blue',
                                        click: () => {
                                            modalHide_old();
                                        },
                                    },
                                ],
                            });
                        }
                        
                    }
                    , error: function () {
                        modalShow_old(`수정에 실패 하였습니다..`, {
                            width: modalSize['smallSize'],
                            button: [
                                {
                                    html: '확인',
                                    class: 'silver',
                                    click: () => {
                                        modalHide_old();
                                    },
                                },
                            ],
                        });                
                    }
                });    // ajax end    
            }
        });

        //1분마다 리스트 조회
        setInterval(function() { 
            userMallListMaker();    
            fn_delLi();
            fn_modLi();
        }, 60000);


        $(document).on('change','.hidden_file',function(){
            var id = $(this).attr("id");
            var item = $(this).parent('.item');
            
            var filename = $('#'+id)[0].files[0].name.split(".");         
            var fileExtension = filename[filename.length - 1];
            var fileSize =  $('#'+id)[0].files[0].size;
        
            if (fileSize < 10485760 && isAllowImage(fileExtension)) {
                
            } else {
                
                return false;
            }
            
            var formData = new FormData();

            if ($('#'+id).val()) {
                var filename = $('#'+id)[0].files[0].name.split(".");         
                var fileExtension = filename[filename.length - 1];
                var fileSize =  $('#'+id)[0].files[0].size;

                if (fileSize < 10485760 && isAllowImage(fileExtension)) {
                    $("#qr_code_file_err").hide();
                } else {
                    $("#qr_code_file_err").show();
                    return false;
                }

                formData.append("qrcode_image_file", $('#'+id)[0].files[0]);
            }
    
            $.ajax({
                url: '/qrcode/image/key/extraction',
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                type: 'POST',
                success: function (data) {
                    item.children('.mall_auth_otp_key').val(data);
                },
                error: function (request, status, error) {
                    $(".input_otp .hidden_file").val("");
                    showAlert("형식에 맞지 않은 QR코드입니다.");
                }
            });
    
        });

        $(document).on('click','.add_pooup_close, .button_row .close',function(){
            $(".new_popup_wrap").css("display", "none");
        });

        $(document).on('click','.chg_ticket',function(){
            var cust_mall_seq_no = $(this).data("cust_mall_seq_no");

            var params = {
                cust_mall_seq_no: cust_mall_seq_no
            };

            var url = '/sub/member/send_sub_auth_mail';
            var errorMessage = "처리 실패하였습니다.";

            $.ajax({
                url: url
                , type: 'post'
                , async: true
                , dataType: 'json'
                , contentType: 'application/json'
                , data: JSON.stringify(params)
                , success: function (response) {
                    $(".new_popup_wrap").css("display", "none");
                    
                    if (response.result == "OK") {
                        showAlert("업데이트 요청 완료<br>(나중에 다시 확인해주세요~)", function () {
                            userMallListMaker();    
                            fn_delLi();
                            fn_modLi();
                        });
                    } else {
                        showAlert(errorMessage);
                    }
                }
            });
        });
    });
        
        
    // 기본은 데스크탑
    const modalSize = {
        smallSize: '280px',
        mediumSize: '365px',
        largeSize: '465px',
    };

    if (window.innerWidth < 860) {
        modalSize['smallSize'] = '90%';
        modalSize['mediumSize'] = '90%';
        modalSize['largeSize'] = '90%';
    }

    window.addEventListener('resize', () => {
        if (window.innerWidth < 860) {
            // 모바일 사이즈
            modalSize['smallSize'] = '90%';
            modalSize['mediumSize'] = '90%';
            modalSize['largeSize'] = '90%';
        } else {
            // 데스크탑 사이즈
            modalSize['smallSize'] = '280px';
            modalSize['mediumSize'] = '365px';
            modalSize['largeSize'] = '465px';
        }
    });

    modalInit_old();
    let total = document.querySelector('.bottomBar #total');
    let reg = document.querySelector('.bottomBar #reg');
    let mod = document.querySelector('.bottomBar #mod');
        
    const openedMallData = await fetch('/sub/my/join/mallList?mallOpenYn=Y')
        .then((res) => res.json())
        .then((data) => {
            data.forEach((el) => {
                el['count'] = 0;
            });
            return data;
        })
        .catch((error) => {
            console.error(error);
        });

    const beOpenedMallData = await fetch('/sub/my/join/mallList?mallOpenYn=N')
        .then((res) => res.json())
        .then((data) => {
            data.forEach((el) => {
                el['count'] = 0;
            });
            return data;
        })
        .catch((error) => {
            console.error(error);
        });
    
    const openedTopTenMallData = await fetch('/sub/my/join/mallList?mallOpenYn=Y')
        .then((res) => res.json())
        .then((data) => {
            data.forEach((el) => {
                el['count'] = 0;
            });
            return data;
        })
        .catch((error) => {
            console.error(error);
        });

    const categoryData = await fetch('/sub/my/join/categoryList')
        .then((res) => res.json())
        .then((data) => {
            data.forEach((el) => {
                el['count'] = 0;
            });
            return data;
        })
        .catch((error) => {
            console.error(error);
        });

    const mallModRegNoDataText = document.querySelector(
        '#article_2 .flexArea .left .contents h3.noDataText'
    );
    const mallModRegObj = {
        num: {
            reg: 0,
            mod: 0,
            checkMallRegCnt: 0,
        },
    };

    let timeout;
    const bottomBar = document.querySelector('.bottomBar');
    bottomBar.addEventListener('mouseenter', () => {
        if (timeout) {
            clearTimeout(timeout);
        }
    });
    bottomBar.addEventListener('mouseleave', () => {
        timeout = setTimeout(() => {
            bottomBar.classList.remove('show');
        }, 3000);
    });
    bottomBar.querySelector('#hideBottomBar').addEventListener('click', () => {
        if (timeout) {
            clearTimeout(timeout);
        }
        bottomBar.classList.remove('show');
    });
    bottomBar.querySelector('#closeBottomBar').addEventListener('click', () => {
        if (timeout) {
            clearTimeout(timeout);
        }
        bottomBar.classList.remove('show');
    });
    function bottomBarShow(action) {
        let mallObj = mallModRegObj['num'];
        switch (action) {
            case 'reg':
                mallObj['reg']++;
                reg.innerHTML = mallObj['reg'];
                mod.innerHTML = mallObj['mod'];
                total.innerHTML = mallObj['reg'] + mallObj['mod'];
                mallModRegNoDataText.classList.add('hide');
                break;

            case 'mod':
                mallObj['mod']++;
                reg.innerHTML = mallObj['reg'];
                mod.innerHTML = mallObj['mod'];
                total.innerHTML = mallObj['reg'] + mallObj['mod'];
                mallModRegNoDataText.classList.add('hide');
                break;

            case 'del':
                mallObj['reg'] = 0;
                mallObj['mod'] = 0;
                reg.innerHTML = mallObj['reg'];
                mod.innerHTML = mallObj['mod'];
                total.innerHTML = mallObj['reg'] + mallObj['mod'];
                mallModRegNoDataText.classList.remove('hide');
            default:
                break;
        }
        bottomBar.classList.add('show');
        if (timeout) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(() => {
            bottomBar.classList.remove('show');
        }, 3000);
    }
    
    document.querySelector('#mallGuide').addEventListener('click', () => {
        let popupHtml = `
        <div class="head">
            <h5>판매몰 등록 가이드</h5>
            <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
        </div>
        <div class="body">
            <p>현재 판매하고 있는 쇼핑몰을 선택 후 등록가이드에 따라 등록해주세요.<br>등록이 어려우신 경우, 우측 하단의 '1:1상담톡'에서 문의주세요.</p>
            <span class="line"></span>
            <p>등록할 판매몰을 하단의 리스트에서 선택하여 추가해주세요.</p>
            <img src="/assets/new_join/img/step_05_modal.png" alt="mallGuide">
            <span class="line"></span>
            <div class="flex">
            <p>1. 하단의 TOP10/이름순 항목에서 판매몰 리스트를 추가</p>
            <img class="arrow" src="/assets/new_join/img/step_05_rightArrow.svg" alt="arrow">
            <p>2. 판매몰 등록/수정에서 업데이트</p>
            </div>
        </div>`;
        modalShow({
            id: 'modal_01',
            content: popupHtml,
            function: () => {},
        });
    });

    document.querySelector('#nextStep').addEventListener('click', () => {
        location.href = '/sub/member/step6';
    });

    // 등록 판매몰 리스트 생성
    function userMallListMaker() {
        let article = document.querySelector('article#article_0');
        let mallUl = article.querySelector('.listWrap ul');
        let mallLi = article.querySelector('.listWrap ul li');
        // ul은 데스크탑, ol은 모바일전용
        let mallOl = article.querySelector('.listWrap ol');
        
        while (mallUl.firstChild) { 
            mallUl.removeChild(mallUl.firstChild);
        }

        while (mallOl.firstChild) { 
            mallOl.removeChild(mallOl.firstChild);
        }

        $.ajax({
            url: '/sub/my/join/custMallList',
            type: 'GET',
            dataType: 'json',
            async : false,
            success: function (userMallData) {
                if (userMallData.mallList !== null && userMallData.mallList.length > 0) {
                    //데스크탑
                    let cnt = userMallData.extReCustMallCntDTO;
                    // 카운트
                    document.getElementById('all_cnt').innerText = cnt.all_cnt - cnt.del_cnt - cnt.dup_cnt;
                    document.getElementById('nr_cnt').innerText = cnt.nr_cnt;
                    document.getElementById('run_cnt').innerText = cnt.run_cnt;
                    document.getElementById('rdy_cnt').innerText = cnt.rdy_cnt;
                    document.getElementById('err_cnt').innerText = cnt.err_cnt;
                    document.getElementById('ins_cnt').innerText = cnt.ins_cnt;
                    document.getElementById('ocm_cnt').innerText = cnt.ocm_cnt;
                    document.getElementById('currentRegCount').value = cnt.all_cnt - cnt.del_cnt - cnt.dup_cnt;
                    
                    let listBottom  = article.querySelector('.listBottom');
                    if(userMallData.mallList.length  < 10){
                        listBottom.classList.add('under_10');
                    }    
                    userMallData.mallList.forEach((el) => {

                        let li = createElement('li', {
                            data: {
                                seq_no: el['cust_mall_seq_no'],
                                mall_cd : el['mall_cd'],
                                mall_cert_1st_id : el['mall_cert_1st_id'],
                                mall_cert_1st_passwd : el['mall_cert_1st_passwd'],
                                sub_mall_cert_1st_nm : el['sub_mall_cert_1st_nm'],
                                sub_mall_cert_2nd_nm : el['sub_mall_cert_2nd_nm'],
                                scb_loan_cnt : el['scb_loan_cnt'],
                                
                            }, 
                        });

                        //오픈예정몰
                        if(el['mall_typ_cd'] == 'OCM'){
                            li.classList.add(el['mall_typ_cd']);
                        }else{
                            li.classList.add(el['cust_mall_sts_cd']);
                        }
                        
                        let dd_0 = createElement('dd', {
                            class: 'status',
                        });
                        let span = createElement('span');
                        dd_0.append(span);
                        let dd_1 = createElement('dd', {
                            html: el['mall_nm'],
                        });
                        let dd_2 = createElement('dd', {
                            html: el['mall_cert_1st_id'],
                        });
                        let dd_3 = createElement('dd');
                        if (el['cert_step'] > 0) {
                            dd_3.classList.add('extraInfo');
                            if (el['cert_step'] == 1) {
                                
                                dd_3.dataset.infoKey = el['sub_mall_cert_1st_nm'];
                                dd_3.innerHTML = el['sub_mall_cert_1st'];
                                
                            } else if (el['cert_step'] == 2) {
                                if((el['sub_mall_cert_1st'] !== null && el['sub_mall_cert_2nd'] !== null)
                                && (el['sub_mall_cert_1st'] !== '' && el['sub_mall_cert_2nd'] !== '')
                                ){
                                    dd_3.dataset.infoKey = el['sub_mall_cert_1st_nm']  + '/ ' + el['sub_mall_cert_2nd_nm'];
                                    dd_3.innerHTML = el['sub_mall_cert_1st'] + '/' + el['sub_mall_cert_2nd'];
                                }
                            }
                        }
                        let dd_4 = createElement('dd', {
                            class: 'logos',
                        });
        
                        if(el['loanMallList'] != null && el['loanMallList'].length > 0){
                            let moreLogo = createElement('div', {
                                class: 'moreLogo',
                            });
                            if (el['loanMallList'].length > 3) {
                                dd_4.classList.add('more');
                                dd_4.append(moreLogo);
                            }
                        
                            el['loanMallList'].forEach((el2,idx2) => {
                                if(el['cust_mall_seq_no'] == el2['cust_mall_seq_no']){
                                    let imgDiv = createElement('div', {
                                        class: 'imgDiv',
                                    });
                                    if(el2['svc_id'] == 'kcbkbbank'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoKB.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }else if(el2['svc_id'] == 'scbank'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoSC.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }else if(el2['svc_id'] == 'kcbshinhan'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoShinhan.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }else if(el2['svc_id'] == 'sodeOne'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoSodeone.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }else if(el2['svc_id'] == 'sodeTwo'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoSodetwo.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }
                                    switch(el2['svc_id']){
                                        case 'kcbkbbank':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                        break;

                                        case 'scbank':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                        break;

                                        case 'sodeOne':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                            break;
                                            
                                        case 'sodeTwo':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                            break;

                                        case 'kcbshinhan':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                            break;

                                        default:
                                        break;
                                    }
                                    
                                    if (idx2 > 2) {
                                        moreLogo.append(imgDiv);
                                    } else {
                                        dd_4.append(imgDiv);
                                    }
                                    
                                }
                            });
                        }

                        // 기타 스크래핑 오류 - 점검
                        if(el['cust_mall_sts_cd'] == 'INS'){
                            el['cd_nm'] = "점검";
                            el['cd_descp'] = "점검";
                        }

                        let dd_5 = createElement('dd', {
                            class: 'tip',
                            html: el['cd_nm'] ?? '',
                        });

                        if(el['cust_mall_sts_cd'] == 'ERR' && el['scra_err_cd'] == 'E013' 
                            && (el['mall_cd'] == '046' || el['mall_cd'] == '047' )){
                                
                                dd_5.onclick = () => {
                                    var cust_mall_seq_no = el['cust_mall_seq_no'];

                                    var params = {
                                        cust_mall_seq_no: cust_mall_seq_no
                                    };

                                    var url = '/sub/member/get_cust_sub_auth_mail';
                                    var errorMessage = "처리 실패하였습니다.";

                                    $.ajax({
                                        url: url
                                        , type: 'post'
                                        , async: true
                                        , dataType: 'json'
                                        , contentType: 'application/json'
                                        , data: JSON.stringify(params)
                                        , success: function (response) {
                                            console.log(response);
                                            if (response.result == "OK") {
                                                $(".new_popup_wrap").css("display", "flex");
                                                $(".pop_up_1").show();
                                                $(".display_none_back").show();
                                                $("#user_mail").text(response.data);
                                                $(".chg_ticket").attr('data-cust_mall_seq_no',cust_mall_seq_no);
                                            }else {
                                                showAlert(errorMessage);
                                            }
                                        }
                                    });
                                }
                            }else{
                                if (el['cd_descp'] != null) {
                                    dd_5.classList.add('msg');
                                    dd_5.dataset.msg = el['cd_descp'];
                                }
                            }

                        
                        let dd_6 = createElement('dd', {
                            class: 'btns',
                        });
                        let modLi = createElement('button', {
                            id: 'modLi',
                            html: '수정',
                        });
                        let delLi = createElement('button', {
                            id: 'delLi',
                            html: '삭제',
                        });
                        dd_6.append(modLi, delLi);
                        li.append(dd_0, dd_1, dd_2, dd_3, dd_4, dd_5, dd_6);
                        mallUl.append(li);
                    });
        
                    //모바일
                    userMallData.mallList.forEach((el) => {
                        let li = createElement('li', {
                            class: el['cust_mall_sts_cd'],
                            data: {
                                seq_no: el['cust_mall_seq_no'],
                                mall_cd : el['mall_cd'],
                                mall_cert_1st_id : el['mall_cert_1st_id'],
                                mall_cert_1st_passwd : el['mall_cert_1st_passwd'],
                                sub_mall_cert_1st_nm : el['sub_mall_cert_1st_nm'],
                                sub_mall_cert_2nd_nm : el['sub_mall_cert_2nd_nm'],
                                scb_loan_cnt : el['scb_loan_cnt'],
                                
                            },
                        });
        
                        let top = createElement('div', {
                            class: 'top',
                        });
                        li.append(top);
                        let p = createElement('p', {
                            html: el['mall_nm'],
                        });
                        let span = createElement('span');
                        top.append(p, span);
        
                        let mid = createElement('div', {
                            class: 'mid',
                        });
                        li.append(mid);
        
                        let row_0 = createElement('div', {
                            class: 'row',
                        });
                        let row_0_dt = createElement('dt', {
                            html: '아이디',
                        });
                        let row_0_dd = createElement('dd', {
                            html: el['mall_cert_1st_id'],
                        });
                        row_0.append(row_0_dt, row_0_dd);
                        mid.append(row_0);
        
                        let row_1 = createElement('div', {
                            class: 'row',
                        });
                        mid.append(row_1);
                        let row_1_dt = createElement('dt', {
                            html: '부가정보',
                        });
                        let row_1_dd = createElement('dd');
                        row_1.append(row_1_dt, row_1_dd);
                        if (el['cert_step'] > 0) {
                            let text = '';
                            if (el['cert_step'] == 1) {
                                text +=  ` ${el['sub_mall_cert_1st_nm']}  ${el['sub_mall_cert_1st']}`;
                            } else if (el['cert_step'] == 2) {
                                text += `${el['sub_mall_cert_1st_nm']}  ${el['sub_mall_cert_1st']} <br> ${el['sub_mall_cert_2nd_nm']} ${el['sub_mall_cert_2nd']}`;
                            }
                            row_1_dd.innerHTML = text;
                        }
        
                        let row_2 = createElement('div', {
                            class: 'row logos',
                        });
                        mid.append(row_2);
                        let imgDt = createElement('dt', {
                            html: '제휴서비스',
                        });
                        let imgDd = createElement('dd', {
                            html: '',
                        });
                        row_2.append(imgDt, imgDd);


                        if(el['loanMallList'] != null && el['loanMallList'].length > 0){
                            el['loanMallList'].forEach((el2,idx2) => {
                                if(el['cust_mall_seq_no'] == el2['cust_mall_seq_no']){
                                    let imgDiv = createElement('div', {
                                        class: 'imgDiv',
                                    });
                                    if(el2['svc_id'] == 'kcbkbbank'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoKB.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }else if(el2['svc_id'] == 'scbank'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoSC.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }else if(el2['svc_id'] == 'kcbshinhan'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoShinhan.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }else if(el2['svc_id'] == 'sodeOne'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoSodeone.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }else if(el2['svc_id'] == 'sodeTwo'){
                                        let img = createElement('img', {
                                            src: '/assets/pubMarket/img/logoSodetwo.png',
                                            alt: el2['svc_id'],
                                        });    
                                        imgDiv.append(img);
                                    }
                                    switch(el2['svc_id']){
                                        case 'kcbkbbank':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                        break;
                                        case 'scbank':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                        break;
                                        case 'sodeOne':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                            break;
                                            
                                        case 'sodeTwo':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                            break;

                                        case 'kcbshinhan':
                                            imgDiv.dataset.logoName = el2['svc_nm'];
                                            break;

                                        default:
                                        break;
                                    }
                                    imgDd.append(imgDiv);
                                }
                            });
                        }

                        let row_3 = createElement('div', {
                            class: 'row',
                        });
                        mid.append(row_3);
                        let row_3_dt = createElement('dt', {
                            html: 'TIP',
                        });

                        // 기타 스크래핑 오류 - 점검
                        if(el['cust_mall_sts_cd'] == 'INS'){
                            el['cd_nm'] = "점검";
                        }

                        let row_3_dd = createElement('dd', {
                            html: el['cd_nm'] ?? '',
                        });

                        if(el['cust_mall_sts_cd'] == 'ERR' && el['scra_err_cd'] == 'E013' 
                            && (el['mall_cd'] == '046' || el['mall_cd'] == '047' )){
                                row_3_dd.style = "color:red;text-decoration: underline;";
                                row_3_dd.onclick = () => {
                                    var cust_mall_seq_no = el['cust_mall_seq_no'];

                                    var params = {
                                        cust_mall_seq_no: cust_mall_seq_no
                                    };

                                    var url = '/sub/member/get_cust_sub_auth_mail';
                                    var errorMessage = "처리 실패하였습니다.";

                                    $.ajax({
                                        url: url
                                        , type: 'post'
                                        , async: true
                                        , dataType: 'json'
                                        , contentType: 'application/json'
                                        , data: JSON.stringify(params)
                                        , success: function (response) {
                                            if (response.result == "OK") {
                                                $(".new_popup_wrap").css("display", "flex");
                                                $(".pop_up_1").show();
                                                $(".display_none_back").show();
                                                $("#user_mail").text(response.data);
                                                $(".chg_ticket").attr('data-cust_mall_seq_no',cust_mall_seq_no);
                                            }else {
                                                showAlert(errorMessage);
                                            }
                                        }
                                    });
                            }
                        }

                    
                        row_3.append(row_3_dt, row_3_dd);
        
                        let bot = createElement('div', {
                            class: 'bot',
                        });
                        li.append(bot);
                        let b = createElement('b', {
                            html: '관리',
                        });
                        let btns = createElement('div', {
                            class: 'btns',
                        });
                        bot.append(b, btns);
                        let modLi = createElement('button', {
                            id: 'modLi',
                            html: '수정',
                        });
                        let delLi = createElement('button', {
                            id: 'delLi',
                            html: '삭제',
                        });
                        btns.append(modLi, delLi);
        
                        mallOl.append(li);
                    });
                } else {
                    // 데이터가 없을경우
                    let listBottom  = article.querySelector('.listBottom');
                    listBottom.classList.add('under_10');
                    let h3 = createElement('h3', {
                        class: 'noDataText',
                        html: '등록한 판매몰이 없습니다.<br>등록할 판매몰을 [ 지원 판매몰 리스트 ] 에서 선택해주세요.' ,
                    });
                    let h3_copy = h3.cloneNode(true);
                    mallUl.append(h3);
                    mallOl.append(h3_copy);
                }
            }
        });    
    }
    userMallListMaker();

    // 지원 판매몰 리스트 생성
    function mallListMaker() {
        let article = document.querySelector('article#article_1');
        let top10 = article.querySelector('#top10 .gridArea');
        let allMalls = article.querySelector('#allMalls .gridArea');
        
        openedMallData.forEach((el) => {
            if (el['use_yn'] == 'N') {
            let item = createElement('div', {
                    class: 'item',
                    data: {
                        mall_cd: el['mall_cd'],
                        mall_nm: el['mall_nm'],
                        mall_sts_cd: el['mall_sts_cd'],
                    },
                });
                let img = createElement('img', {
                    src: `/imagefile/${el['stor_path']}${el['stor_file_nm']}`,
                    alt: '몰 로고',
                });
                let b = createElement('b', {
                    html: el['mall_nm'],
                });
                item.append(img, b);
                allMalls.append(item);
            }
        });
        openedTopTenMallData.forEach((el) => {
            if (el['use_yn'] == 'Y') {
            
                let item = createElement('div', {
                    class: 'item',
                    data: {
                        mall_cd: el['mall_cd'],
                        mall_nm: el['mall_nm'],
                        mall_sts_cd: el['mall_sts_cd'],
                    },
                });
                let img = createElement('img', {
                    src: `/imagefile/${el['stor_path']}${el['stor_file_nm']}`,
                    alt: '몰 로고',
                });
                let b = createElement('b', {
                    html: el['mall_nm'],
                });

                item.append(img, b);
                top10.append(item);
                
            }
        });
    }
    mallListMaker();

    // statusBtns 클릭
    document.querySelectorAll('.statusBtns button').forEach((el, idx, arg) => {
        el.addEventListener('click', () => {
        
            var all = document.querySelectorAll('.listWrap ul li , .listWrap ol li');
            let article = document.querySelector('article#article_0');
            let listBottom  = article.querySelector('.listBottom');

            //오픈예정몰 값 없음
            if(el.id == null || el.id == ""){
                el.id = "test";
            }

            all.forEach(el => {
                el.style.display = "none";
            });

            if(el.id =='ALL'){
                
                var statusCnt = document.getElementById(el.id).querySelector('btn-num').innerText; 
                var showLi = document.querySelectorAll('.'+el.id);
                
                //접기 버튼
                if(statusCnt < 10){
                    listBottom.classList.add('under_10');
                }else{
                    listBottom.classList.remove('under_10');
                }

                all.forEach(el => {
                    el.style.display = "";
                });

            }else{
            
                var showLi = document.querySelectorAll('.'+el.id);
                var statusCnt = document.getElementById(el.id).querySelector('btn-num').innerText; 
                
                //접기 버튼
                if(statusCnt < 10){
                    listBottom.classList.add('under_10');
                }else {
                    listBottom.classList.remove('under_10');
                }

                showLi.forEach(el2 => {
                    el2.style.display = "";
                });
            }
        
            arg.forEach((el2) => el2.classList.remove('on'));
            el.classList.add('on');
        });
    });

    // 판매몰 제휴서비스 더보기 버튼
    if (document.querySelectorAll('dd.logos.more')) {
        document.querySelectorAll('dd.logos.more').forEach((el) => {
            el.addEventListener('click', () => {
                if (!el.classList.contains('show')) {
                    el.classList.add('show');
                } else {
                    el.classList.remove('show');
                }
            });
        });
    }

    //등록한 판매몰 전체보기 버튼
    document.querySelector('#showListBtn').addEventListener('click', (e) => {
        let target = e.target;
        const listWrap = document.querySelector('#article_0 .listWrap');
        if (listWrap.classList.contains('open')) {
            // 전체보기 상태일경우
            listWrap.classList.remove('open');
            target.innerHTML = '<b>전체보기</b>';
            target.classList.remove('open');
            window.scrollTo({
                top: document.querySelector('#article_0').offsetTop,
            });
        } else {
            // 접혀있는 상태일경우
            listWrap.classList.add('open');
            target.innerHTML = '<b>접기</b>';
            target.classList.add('open');
        }
    });

    // 판매몰 별 관리 -> 삭제 버튼 클릭
    function fn_delLi() {
        document.querySelectorAll('#delLi').forEach((el) => {
        
            el.addEventListener('click', () => {
                
                modalShow_old('<p>계정을 삭제하시겠습니까?</p>', {
                    width: modalSize['smallSize'],
                    button: [
                        {
                            html: '취소',
                            click: modalHide_old(),
                            class: 'silver',
                        },
                        {
                            html: '확인',
                            click: () => {
                                // 삭제확인을 눌렀을 때 콜백
                                modalHide_old();
                                let targetLi = el.parentNode.parentNode;
                                if (targetLi.classList.contains('bot')) {
                                    // 모바일 예외처리
                                    targetLi = el.parentNode.parentNode.parentNode;
                                }
                                let cust_mall_seq_no = targetLi.dataset.seq_no;
                                let scb_loan_cnt = targetLi.dataset.scb_loan_cnt;
                                var modifyChk = true;

                                $.each($('.modifyArea , .registerArea').children('div') , function(indxe , item ){ 
                                    if(cust_mall_seq_no == item.dataset.cust_mall_seq_no ){
                                        modalShow_old(`수정중에는 삭제가 불가 합니다.`, {
                                            width: modalSize['smallSize'],
                                            button: [
                                                {
                                                    html: '확인',
                                                    class: 'blue',
                                                    click: () => {
                                                        modalHide_old();
                                                        
                                                    },
                                                },
                                            ],
                                        });
                                        modifyChk = false;
                                    }
                                });
                                
                                if(!modifyChk){
                                    return false;    
                                }

                                if(scb_loan_cnt > 0){
                                    modalShow_old(`SC제일은행 대출을 이용 중인 고객님은 <br>대출을 사용하시는 기간 동안 쇼핑몰의 삭제가 불가능합니다.`, {
                                        width: modalSize['smallSize'],
                                        button: [
                                            {
                                                html: '확인',
                                                class: 'blue',
                                                click: () => {
                                                    modalHide_old();
                                                },
                                            },
                                        ],
                                    });
                                    
                                    return false;
                                    
                                }
                                
                                // 2021.03.11 대출 신청된 판매몰 삭제 막기 추가
                                var checkCnt = 0;
                                var svcDescp = '';
                                $("input:checkbox[name=loan_chk]:checked").each(function() {
                                    if (cust_mall_seq_no == $(this).data("cust-mall-seq-no")) {
                                        checkCnt++;
                                        svcDescp = $(this).data("loan-svc-descp");
                                        return false;
                                    }
                                });

                                if(checkCnt > 0){
                                    modalShow_old(svcDescp + " 대출을 이용 중인 고객님은 <br>대출을 사용하시는 기간 동안 쇼핑몰의 삭제가 불가능합니다.", {
                                        width: modalSize['smallSize'],
                                        button: [
                                            {
                                                html: '확인',
                                                class: 'blue',
                                                click: () => {
                                                    modalHide_old();
                                                },
                                            },
                                        ],
                                    });
                                    
                                    return false;
                                    
                                }

                                $.ajax({
                                    url: '/sub/my/join/custMallDelete',
                                    data: { cust_mall_seq_no: cust_mall_seq_no },
                                    type: 'POST',
                                    dataType: 'json',
                                    async : true,
                                    cache : true,
                                    success: function (data) {
                                
                                        userMallListMaker();
                                        fn_delLi();
                                        fn_modLi();
                                    }
                                    , error: function () {
                                        alert("2222222222");                    
                                    }
                                });

                            },
                            class: 'blue',
                        },
                    ],
                });
            });
        });
    }
    fn_delLi();

    // 판매몰 별 관리 -> 수정 버튼 클릭
    function fn_modLi() {
    
        document.querySelectorAll('#modLi').forEach((modLi) => {
            modLi.addEventListener('click', () => {
                let targetLi = modLi.parentNode.parentNode;
                let regCount = mallModRegObj.num.reg;
                if (targetLi.classList.contains('bot')) {
                    // 모바일 예외처리
                    targetLi = modLi.parentNode.parentNode.parentNode;
                }
                let seqNo = targetLi.dataset.seq_no;

                // 이미 클릭한 아이템일 경우
                if (document.querySelectorAll('.modifyArea .modifyBox')) {
                    let stopFlag = false;
                    document.querySelectorAll('.modifyArea .modifyBox').forEach((el) => {
                        if (seqNo == el.dataset.seq_no) {
                            modalShow_old('이미 리스트에 추가되어 있습니다', {
                                width: modalSize['smallSize'],
                                button: [
                                    {
                                        html: '확인',
                                        click: modalHide_old(),
                                        class: 'silver',
                                    },
                                ],
                            });
                            stopFlag = true;
                        }
                    });

                    if (stopFlag == true) {
                        return false;
                    }
                }
                
                $.ajax({
                    url: '/sub/my/join/getMallInfo',
                    data: { cust_mall_seq_no: seqNo },
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        if (data.cust_mall_seq_no == seqNo) {
                            let modifyBox = createElement('div', {
                                class: 'modifyBox',
                                id : 'update'+mallModRegObj.num.mod,
                                data: {
                                    seq_no: seqNo,
                                    div_type: 'mod',
                                    cust_mall_seq_no : data.cust_mall_seq_no,
                                    cert_step : data.sub_mall_cert_step,
                                    passwd_step : data.passwd_step,
                                    mall_cd : data.mall_cd,
                                    mall_cert_1st_id : data.mall_cert_1st_id,
                                    mall_cert_1st_passwd : data.mall_cert_1st_passwd,
                                    mall_cert_2nd_passwd_nm : data.mall_cert_2nd_passwd_nm,
                                    sub_mall_cert_1st_nm : data.sub_mall_cert_1st_nm,
                                    sub_mall_cert_2nd_nm : data.sub_mall_cert_2nd_nm,
                                    help_use_yn : data.help_use_yn,
                                    help_info : data.help_info,
                                    video_use_yn : data.video_use_yn,
                                    video_url : data.video_url,
                                    detail_use_yn : data.detail_use_yn,
                                    detail_url : data.detail_url,
                                    cust_mall_sts_cd : data.cust_mall_sts_cd,
                                    post_yn : data.post_yn,
                                    err_msg : data.err_msg,
                                    measure : data.measure,
                                    err_msg_use_yn : data.err_msg_use_yn,
                                    measure_yn : data.measure_yn,
                                    sub_mall_auth_1st_esse_yn : data.sub_mall_auth_1st_esse_yn,
                                    sub_mall_auth_2nd_esse_yn : data.sub_mall_auth_2nd_esse_yn,
                                    data_biz_no_auth_esse_yn : data.data_biz_no_auth_esse_yn,
                                    otp_auth_esse_yn : data.otp_auth_esse_yn,
                                    otp_auth_typ_cd : data.otp_auth_typ_cd,
                                    otp_auth_use_yn : data.otp_auth_use_yn,
                                    mall_auth_otp_key : data.mall_auth_otp_key
                                },

                            });
                            document.querySelector('.modifyArea').append(modifyBox);
                            let main = createElement('div', {
                                class: 'main',
                                style: 'cursor:pointer;'
                            });
                            let modifyOkBtn = createElement('button', {
                                id: 'modifyOkBtn',
                                html: '확인',
                            });
                            modifyBox.append(main, modifyOkBtn);
                            let top = createElement('div', {
                                class: 'top',
                                
                            });
                            let smallLine = createElement('span', {
                                class: 'smallLine',
                            });
                            let middle = createElement('div', {
                                class: 'middle',
                            });
                            let errorMsg = createElement('p', {
                                class: 'errorMsg',
                                html: data.cd_descp  ?? '<br>',
                            });
                            main.append(top, smallLine, middle, errorMsg);
                            let b = createElement('b', {
                                id: 'mallName',
                                html: data.mall_nm,
                            });
                            let img = createElement('img', {
                                class: 'mallLogo',
                                src: `/imagefile/${data.stor_path}${data.stor_file_nm}`,
                                alt: '몰 로고',
                            });
                            top.append(b, img);
                            
                            // if(data.cust_mall_sts_cd == 'INS'){
                            //     data.cd_nm = '점검';
                            // }

                            if (data.cust_mall_sts_cd == 'ERR') {
                                let span = createElement('span', {
                                    class: `msg ${data.cust_mall_sts_cd}`,
                                    html: data.cd_nm,
                                });
                                top.append(span);
                            } else {
                                let span = createElement('span', {
                                    class: `msg`,
                                    html: '',
                                });
                                top.append(span);
                            }
                            let button = createElement('button', {
                                id: 'deleteItem',
                                
                            });
                            top.append(button);
                            let btnImg = createElement('img', {
                                src: '/assets/pubMarket/img/xmark.svg',
                                alt: 'xmark',
                            });

                            // 입력 태그 생성
                            button.append(btnImg);


                            if(data.mall_cert_typ.length>0){
                                let itemA = createElement('div', {
                                    class: 'item',
                                });
                                middle.append(itemA);
                                let pA = createElement('p', {
                                    html: '몰 인증 유형',
                                });
                                itemA.append(pA);
                                let selectA = createElement('select', {
                                    id: 'mall_cert_typ_cd',
                                    class :'mall_cert_typ_cd',
                                    name : 'mall_cert_typ_cd',
                                });
                    
                                data.mall_cert_typ.forEach((el2) => {
                                    let optionA = createElement('option', {
                                        html: el2.mall_cert_typ_nm,
                                        value: el2.mall_cert_typ_cd,
                                    });
                                    selectA.append(optionA);
                                });
                    
                                itemA.classList.add('row_100');
                                itemA.append(selectA);
                                
                                const el3 = document.getElementById('mall_cert_typ_cd');
                                const len = el3.options.length;

                                for(let i=0;i<len;i++){
                                    if(el3.options[i].value == data.mall_cert_typ_cd){
                                        el3.options[i].selected=true;
                                    }    
                                }
                            }else{
                                let itemA = createElement('div', {
                                    class: 'item',
                                    style : 'display:none'
                                });
                                middle.append(itemA);
                                let pA = createElement('p', {
                                    html: '몰 인증 유형',
                                });

                                itemA.append(pA);
                                let selectA = createElement('select', {
                                    id: 'mall_cert_typ_cd',
                                    class :'mall_cert_typ_cd',
                                    name : 'mall_cert_typ_cd',
                                });
                    
                                let optionA = createElement('option', {
                                    value: 'A00',
                                });
                                selectA.append(optionA);
                                itemA.append(selectA);
                                
                            }

                            let item = createElement('div', {
                                class: 'item',
                            });
                            middle.append(item);
                            let p = createElement('p', {
                                html: '아이디',
                            });
                            item.append(p);
                            let input = createElement('input', {
                                type: 'text',
                                class: 'mall_cert_1st_id',
                                value : data.mall_cert_1st_id,
                                readOnly : true
                            });
                            item.append(input);

                            item = createElement('div', {
                                class: 'item',
                            });

                            middle.append(item);
                            p = createElement('p', {
                                html: '패스워드',
                            });
                            item.append(p);
                                input = createElement('input', {
                                type: 'password',
                                class: 'mall_cert_1st_passwd',
                            });
                            item.append(input);

                            if(data.otp_auth_esse_yn == 'Y'){
                                item = createElement('div', {
                                    class: 'item',
                                });
        
                                middle.append(item);

                                input = createElement('input', {
                                    type: 'file',
                                    class: 'hidden_file',
                                    name : 'qr_code_file',
                                    id : 'fileUpload' + regCount,
                                    style : 'display:none'
                                });
                                item.append(input);

                                p = createElement('p', {
                                    id : 'fileUpload'  + regCount,
                                    name : 'qr_code_file'
                                });
                                
                                p.classList.add('checkNeed');
                                p.innerHTML = 'QR등록';
                                p.onclick = () => {
                                    document.getElementById('fileUpload' + regCount).click();
                                }
                            
                                item.append(p);

                                input = createElement('input', {
                                    type: 'password',
                                    class: 'mall_auth_otp_key',
                                    name : 'mall_auth_otp_key',
                                    value : data.mall_auth_otp_key,
                                    readOnly : true
                                });
                                item.append(input);
                            }

                            if(data.sub_mall_cert_1st_nm != null && data.sub_mall_cert_1st_nm != ""){
                                item = createElement('div', {
                                    class: 'item',
                                });
                                
                                middle.append(item);
                                p = createElement('p', {
                                    html: data.sub_mall_cert_1st_nm,
                                });
                                if(data.mall_cd == '008'){
                                    p.classList.add('checkNeed');
                                    p.innerHTML = data.sub_mall_cert_1st_nm;
                                    p.onclick = () => {
                                        window.open("https://sell.smartstore.naver.com/#/sellers/store/detail/api", "_blank");
                                    }
                                }
                                if(data.sub_mall_cert_1st == null || data.sub_mall_cert_1st == ''){
                                    data.sub_mall_cert_1st = '';
                                }
                                item.append(p);
                                    input = createElement('input', {
                                    type: 'text',
                                    class: 'sub_mall_cert_1st',
                                    value : data.sub_mall_cert_1st, 
                                });
                                item.append(input);
                            }
                            if(data.sub_mall_cert_2nd_nm != null && data.sub_mall_cert_2nd_nm != ""){
                                item = createElement('div', {
                                    class: 'item',
                                });
        
                                middle.append(item);
                                p = createElement('p', {
                                    html: data.sub_mall_cert_2nd_nm,
                                });

                                if(data.sub_mall_cert_2nd == null || data.sub_mall_cert_2nd == ''){
                                    data.sub_mall_cert_2nd = '';
                                }
                                item.append(p);
                                    input = createElement('input', {
                                    type: 'text',
                                    class: 'sub_mall_cert_2nd',
                                    value : data.sub_mall_cert_2nd,
                                });
                                item.append(input);
                            }

                            if(data.mall_cert_2nd_passwd_nm != null && data.mall_cert_2nd_passwd_nm != "" ){
                                item = createElement('div', {
                                    class: 'item',
                                });
        
                                middle.append(item);
                                p = createElement('p', {
                                    html: data.mall_cert_2nd_passwd_nm,
                                });
                                item.append(p);
                                    input = createElement('input', {
                                    type: 'password',
                                    class: 'mall_cert_2nd_passwd',
                                });
                                item.append(input);
                            }

                            item = createElement('div', {
                                class: 'item',
                            });
                            middle.append(item);
                            p = createElement('p', {
                                html: '주요 판매 품목',
                            });
                            item.append(p);
                            let select = createElement('select', {
                                id: 'goods_cate_seq_no',
                                class :'goods_cate_seq_no',
                                name :'goods_cate_seq_no',
                            });
                            categoryData.forEach((el2) =>{
                                let option = createElement('option', {
                                    html: el2.cate_nm,
                                    value: el2.goods_cate_seq_no,
                                });
                                select.append(option);
                            });
                            item.append(select);

                            const el3 = document.getElementById('goods_cate_seq_no');
                            const len = el3.options.length;

                            for(let i=0;i<len;i++){
                                if(el3.options[i].value == data.goods_cate_seq_no){
                                    el3.options[i].selected=true;
                                }    
                            }

                            // 버튼에 도움말 , 오류 안내 데이터 
                            var manualBtn = document.querySelector('#article_2 .flexArea .right .topTitle button'); 
                            manualBtn.dataset.mall_nm = data.mall_nm;
                            manualBtn.dataset.help_info = data.help_info;
                            manualBtn.dataset.err_msg = data.err_msg;
                            manualBtn.dataset.measure = data.measure;
                            manualBtn.dataset.post_yn = data.post_yn; 
                            manualBtn.dataset.err_msg_use_yn = data.err_msg_use_yn; 
                            manualBtn.dataset.measure_yn = data.measure_yn;  

                            // 쇼핑몰 도움말 설정
                            fn_helpInfo(data.mall_cd , data.mall_nm 
                                    , data.help_use_yn , data.help_info
                                    , data.video_use_yn , data.video_url
                                    , data.detail_use_yn , data.detail_url
                                    , data.cust_mall_sts_cd , data.err_msg
                                    , data.measure    ,data.post_yn
                                    , data.err_msg_use_yn    , data.measure_yn
                            );
                            
                            // 202212161513 수정
                            modifyBox.addEventListener('click', () => {
                                if (document.querySelector('.registerArea .registerBox.active')) {
                                    document
                                        .querySelector('.registerArea .registerBox.active')
                                        .classList.remove('active');
                                }
                                if (document.querySelector('.modifyArea .modifyBox.active')) {
                                    document.querySelector('.modifyArea .modifyBox.active').classList.remove('active');
                                }
                                modifyBox.classList.add('active');
                            });
                            
                            modifyBox.querySelector('#deleteItem').addEventListener('click', (e) => {
                                e.stopPropagation();

                                //삭제시 도움만 초기화
                                var topTitle = document.querySelector('.topTitle p');
                                var helpInfo = document.getElementById('helpInfo');
                                var manualBtn = document.querySelector('#article_2 .flexArea .right .topTitle button'); 
                                let topTitle_mall_cd ="";

                                topTitle.innerHTML = "";
                                // helpInfo.innerHTML = "";
                                new toastui.Editor({
                                    el: document.querySelector('#viewer'),
                                    initialValue: ""
                                });
                                topTitle.dataset.mall_cd = data.mall_cd;
                                topTitle.innerHTML = "판매몰 도움말이 없습니다.";
                                manualBtn.style.display = "none";
                                manualBtn.dataset.mall_nm = "";
                                manualBtn.dataset.help_info = "";
                                manualBtn.dataset.err_msg = "";
                                manualBtn.dataset.measure = "";
                                manualBtn.dataset.post_yn = ""; 
                                manualBtn.dataset.err_msg_use_yn = ""; 
                                manualBtn.dataset.measure_yn = "";  
                                mallModRegObj['num']['mod']--;
                                modifyBox.remove();

                                if (mallModRegObj['num']['mod'] == 0 && mallModRegObj['num']['reg'] == 0) {
                                    mallModRegNoDataText.classList.remove('hide');
                                }
                            });
        
                            // 수정 버튼 클릭 시
                            var modifyChk = true;
                        }
                    }
                });        

                mallModRegObj['num']['mod'] = mallModRegObj['num']['mod']++;
                bottomBarShow('mod');
            });
        }); // 수정버튼 끝
    }

    fn_modLi();
    // 판매몰 등록 추가 함수
    function addMallCard(data) {
        let regCount = mallModRegObj.num.reg;
        let registerBox = createElement('div', 
            { class: 'registerBox' ,
                id : 'reg' + regCount,
                  data: {
                    div_type: 'reg',
                    mall_cd : data['mall_cd'],
                    cert_step : data['sub_mall_cert_step'],
                    passwd_step : data['passwd_step'],
                    sub_mall_cert_1st_nm : data['sub_mall_cert_1st_nm'],
                    sub_mall_cert_2nd_nm : data['sub_mall_cert_2nd_nm'],
                    mall_cert_2nd_passwd_nm : data['mall_cert_2nd_passwd_nm'],
                    mall_auth_otp_key : data['mall_auth_otp_key'],
                    stor_path : data['stor_path'],
                    org_file_nm : data['org_file_nm'],
                    stor_file_nm : data['stor_file_nm'],
                    help_use_yn : data['help_use_yn'],
                    help_info : data['help_info'],
                    video_use_yn : data['video_use_yn'],
                    video_url : data['video_url'],
                    detail_use_yn : data['detail_use_yn'],
                    detail_url : data['detail_url'],
                    sub_mall_auth_1st_esse_yn : data['sub_mall_auth_1st_esse_yn'],
                    sub_mall_auth_2nd_esse_yn : data['sub_mall_auth_2nd_esse_yn'],
                    data_biz_no_auth_esse_yn : data['data_biz_no_auth_esse_yn'],
                    otp_auth_esse_yn : data['otp_auth_esse_yn'],
                    otp_auth_typ_cd : data['otp_auth_typ_cd'],
                    otp_auth_use_yn : data['otp_auth_use_yn']
                },
            });
        document.querySelector('.registerArea').append(registerBox);
        let main = createElement('div', { 
                class: 'main',
                style: 'cursor:pointer;',
            });
        let registerOkBtn = createElement('button', {
            id: 'registerOkBtn',
            html: '확인',
        });
        registerBox.append(main, registerOkBtn);
        let top = createElement('div', { class: 'top'  });
        let smallLine = createElement('span', { class: 'smallLine' });
        let middle = createElement('div', { class: 'middle' });
        let errorMsg = createElement('p', {
            class: 'errorMsg',
            html: data['scra_err_cd_desc'] ?? '<br>',
        });
        
        main.append(top, smallLine, middle, errorMsg);
        let top_b = createElement('b', {
            id: 'mall_nm',
            html: data['mall_nm'],
        });
        let top_img = createElement('img', {
            class: 'mallLogo',
            src: `/imagefile/${data['stor_path']}${data['stor_file_nm']}`,
            alt: '몰 로고',
        });
        let top_msg = createElement('span', {
            class: 'msg',
        });
        if (data['scra_err_cd_nm'] !== null) {
            top_msg.classList.add(data['cust_mall_sts_cd']);
            top_msg.innerHTML = data['scra_err_cd_nm'] ?? '';
        } else {
            top_msg.innerHTML = '';
        }
        let delBtn = createElement('button', {
            id: 'deleteItem',
            html: '<img src="/assets/pubMarket/img/xmark.svg" alt="xmark">',
        });
        top.append(top_b, top_img, top_msg, delBtn);

        if(data['mall_cert_typ'].length>0){
            let itemA = createElement('div', {
                class: 'item',
            });
            middle.append(itemA);
            let pA = createElement('p', {
                html: '몰 인증 유형',
            });
            itemA.append(pA);
            let selectA = createElement('select', {
                id: 'mall_cert_typ_cd',
                class :'mall_cert_typ_cd',
                name : 'mall_cert_typ_cd',
            });

            data['mall_cert_typ'].forEach((el2) => {
                let optionA = createElement('option', {
                    html: el2.mall_cert_typ_nm,
                    value: el2.mall_cert_typ_cd,
                });
                selectA.append(optionA);
            });

            itemA.classList.add('row_100');
            itemA.append(selectA);
            
        }else{
            let itemA = createElement('div', {
                class: 'item',
                style : 'display:none'
            });
            middle.append(itemA);
            let pA = createElement('p', {
                html: '몰 인증 유형',
            });

            itemA.append(pA);
            let selectA = createElement('select', {
                id: 'mall_cert_typ_cd',
                class :'mall_cert_typ_cd',
                name : 'mall_cert_typ_cd',
            });

            let optionA = createElement('option', {
                value: 'A00',
            });
            selectA.append(optionA);
            itemA.append(selectA);
            
        }
        
        let item = createElement('div', {
            class: 'item',
        });
        middle.append(item);
        
        let p = createElement('p', {
            html: '아이디',
        });
        item.append(p);
        let input = createElement('input', {
            type: 'text',
            class: 'mall_cert_1st_id',
        });
        item.append(input);

        item = createElement('div', {
            class: 'item',
        });

        middle.append(item);
        p = createElement('p', {
            html: '패스워드',
        });
        item.append(p);
            input = createElement('input', {
            type: 'password',
            class: 'mall_cert_1st_passwd',
        });
        item.append(input);

        if(data.otp_auth_esse_yn == 'Y'){
            item = createElement('div', {
                class: 'item',
            });

            middle.append(item);

            input = createElement('input', {
                type: 'file',
                class: 'hidden_file',
                name : 'qr_code_file',
                id : 'fileUpload' + regCount,
                style : 'display:none'
            });
            item.append(input);
            
            p = createElement('p', {
                name : 'qr_code_file'
            });
            
            p.classList.add('checkNeed');
            p.innerHTML = 'QR등록';
            p.onclick = () => {
                document.getElementById('fileUpload' + regCount).click();
            }
        
            item.append(p);

            input = createElement('input', {
                type: 'password',
                class: 'mall_auth_otp_key',
                name : 'mall_auth_otp_key',
                readOnly : true,
                
            });
            item.append(input);
        }

        if(data.sub_mall_cert_1st_nm != null && data.sub_mall_cert_1st_nm != ""){
            item = createElement('div', {
                class: 'item',
            });

            middle.append(item);
            p = createElement('p', {
                html: data.sub_mall_cert_1st_nm,
            });

            if(data['mall_cd'] == '008'){
                p.classList.add('checkNeed');
                p.innerHTML = data.sub_mall_cert_1st_nm;
                p.onclick = () => {
                    window.open("https://sell.smartstore.naver.com/#/sellers/store/detail/api", "_blank");
                }
            }
            
            item.append(p);
                input = createElement('input', {
                type: 'text',
                class: 'sub_mall_cert_1st',
            });
            item.append(input);
        }
        if(data.sub_mall_cert_2nd_nm != null && data.sub_mall_cert_2nd_nm != ""){
            item = createElement('div', {
                class: 'item',
            });

            middle.append(item);
            p = createElement('p', {
                html: data.sub_mall_cert_2nd_nm,
            });
            item.append(p);
                input = createElement('input', {
                type: 'text',
                class: 'sub_mall_cert_2nd',
            });
            item.append(input);
        }

        if(data.mall_cert_2nd_passwd_nm != null && data.mall_cert_2nd_passwd_nm != "" ){
            item = createElement('div', {
                class: 'item',
            });

            middle.append(item);
            p = createElement('p', {
                html: data.mall_cert_2nd_passwd_nm,
            });
            item.append(p);
                input = createElement('input', {
                type: 'password',
                class: 'mall_cert_2nd_passwd',
            });
            item.append(input);
        }

        item = createElement('div', {
            class: 'item',
        });
        middle.append(item);
        p = createElement('p', {
            html: '주요 판매 품목',
        });
        item.append(p);
        let select = createElement('select', {
            id: 'goods_cate_seq_no',
            class :'goods_cate_seq_no',
            name : 'goods_cate_seq_no',
        });
        let option = createElement('option', {
            html: '선택',
            value: '',
        });
        select.append(option);
        categoryData.forEach((el2) =>{
            let option = createElement('option', {
                html: el2.cate_nm,
                value: el2.goods_cate_seq_no,
            });
            select.append(option);
        });
        item.append(select);

        //도움말 부분
        
        var manualBtn = document.querySelector('#article_2 .flexArea .right .topTitle button'); 
        manualBtn.dataset.mall_nm = "";
        manualBtn.dataset.help_info = "";
        manualBtn.dataset.err_msg = "";
        manualBtn.dataset.measure = "";
        manualBtn.dataset.post_yn = ""; 
        manualBtn.dataset.err_msg_use_yn = ""; 
        manualBtn.dataset.measure_yn = "";  
        
        // 쇼핑몰 도움말 설정
        fn_helpInfo(data['mall_cd'] , data['mall_nm'] 
            , data['help_use_yn'] , data['help_info'] 
            , data['video_use_yn'] , data['video_url'] 
            , data['detail_use_yn'] , data['detail_url'] 
        );


        registerBox.addEventListener('click', () => {
            // 클릭시 active class추가
            if (document.querySelector('.registerArea .registerBox.active')) {
                document
                    .querySelector('.registerArea .registerBox.active')
                    .classList.remove('active');
            }
            if (document.querySelector('.modifyArea .modifyBox.active')) {
                document.querySelector('.modifyArea .modifyBox.active').classList.remove('active');
            }
            registerBox.classList.add('active');

            if (window.innerWidth < 860) {
                const thisTop = registerBox.offsetTop;
                const parentTop = document.querySelector('#article_2 .left .contents').offsetTop;

                document.querySelector('#article_2 .left .contents').scrollTo({
                    top: thisTop - parentTop,
                });
                window.scrollTo({
                    top: parentTop,
                });
                const viewportTop = registerBox.getBoundingClientRect().top;
                window.scrollBy({
                    top: viewportTop,
                });
            }
        });

        registerBox.querySelector('#deleteItem').addEventListener('click', (e) => {
            e.stopPropagation();
            mallModRegObj['num']['reg']--;
            beOpenedMallData.forEach((el) => {
                if (el['mall_cd'] == data['mall_cd']) {
                    el['count']--;
                }
            });
            openedMallData.forEach((el) => {
                if(el['use_yn'] == 'N'){
                    if (el['mall_cd'] == data['mall_cd']) {
                        if (data['mall_cd'] != '008') {
                            mallModRegObj['num']['checkMallRegCnt']--;
                        }
                        el['count']--;
                        syncCount();
                    }
                }
            });
            registerBox.remove();

            if (mallModRegObj['num']['mod'] == 0 && mallModRegObj['num']['reg'] == 0) {
                mallModRegNoDataText.classList.remove('hide');
            }

            var topTitle = document.querySelector('.topTitle p');
            var helpInfo = document.getElementById('helpInfo');
            var manualBtn = document.querySelector('#article_2 .flexArea .right .topTitle button'); 
            let topTitle_mall_cd ="";
            
            topTitle.innerHTML = "";
            // helpInfo.innerHTML = "";
            new toastui.Editor({
                el: document.querySelector('#viewer'),
                initialValue: ""
            });
            topTitle.dataset.mall_cd = data['mall_cd'];
            topTitle.innerHTML = "판매몰 도움말이 없습니다.";
            manualBtn.style.display = "none";
            manualBtn.dataset.mall_nm = "";
            manualBtn.dataset.help_info = "";
            manualBtn.dataset.err_msg = "";
            manualBtn.dataset.measure = "";
            manualBtn.dataset.post_yn = ""; 
            manualBtn.dataset.err_msg_use_yn = ""; 
            manualBtn.dataset.measure_yn = "";  

        });
    }

    $(function () {
        var all_mall = new Array();

        $("#allMalls .gridArea .item").each(function(){
            if ($(this).data("mall_sts_cd") == 'NOR') {
                all_mall.push($(this).data("mall_nm"));
            }
        });

        $(".market_box_find_text_area").autocomplete({
            source: all_mall,
            select: function (event, ui) {
                for (var i = 0; i < $('#allMalls .gridArea .item').length; i++) {
                    if (ui.item.value == $('#allMalls .gridArea .item').eq(i).data('mall_nm')) {
                        $('#allMalls .gridArea .item').eq(i).trigger("click")
                    }
                }
            },
            focus: function (event, ui) {
                return false;
            }
        });
    });

    // 지원 판매몰 리스트 -> 오픈예정몰 버튼 클릭
    document.querySelector('#beOpenedBtn').addEventListener('click', () => {
        let html = `
            <h2>오픈예정몰 사전등록</h2>
            <br>
            <h3>[아래 리스트에서 판매몰을 선택해주세요]</h3>
            <br>
            <h4>오픈 준비 중인 판매몰을 미리 등록하시면<br>
            추후 오픈 시 정산예정금 정보를 바로 확인하실 수 있습니다.<br>
            (등록한 오픈예정몰 계정 확인 → 등록한 판매몰)</h4>
            <br>
            <div id="beOpenedMallList" class="scrollable">`;
        beOpenedMallData.forEach((el) => {
            let string;
            if (el['count'] > 0) {
                string = `<div class="item on" data-mall_cd="${el['mall_cd']}" data-num="${el['count']}">`;
            } else {
                string = `<div class="item" data-mall_cd="${el['mall_cd']}">`;
            }
            string += `    
                        <img src="/imagefile/${el['stor_path']}${el['stor_file_nm']}" alt="몰 로고">
                        <b>${el['mall_nm']}</b>
                    </div>
                    `;
            html += string;
        });

        html += '</div>';

        modalShow_old(html, {
            width: modalSize['largeSize'],
            class: 'beOpenedModal',
            saveId: 'openMall',
            button: [
                {
                    html: '닫기',
                    class: 'dark',
                    click: modalHide_old,
                },
            ],
        });

        // 오픈예정몰 각 판매몰 클릭시
        document.querySelectorAll('#beOpenedMallList .item').forEach((el, idx) => {
            el.addEventListener('click', () => {
                let count = el.dataset.num ?? 0;
                el.classList.add('on');
                el.dataset.num = parseInt(count) + 1;
                beOpenedMallData.forEach((el2) => {
                    if (el2['mall_cd'] == el.dataset.mall_cd) {
                        el2['count']++;
                        bottomBarShow('reg');
                        addMallCard(el2);
                    }
                });
            });
        });
    });

    // top10 및 전체 몰 클릭 시
    let allMalls = document.querySelectorAll('#allMalls .gridArea .item');
    let top10 = document.querySelectorAll('#top10 .gridArea .item');
    let mallsArr = [...allMalls, ...top10];
    mallsArr.forEach((el) => {
        el.addEventListener('click', () => {
            openedMallData.forEach((el2) => {
                if(el2['use_yn'] == 'N'){

                    if (el2['mall_cd'] == el.dataset.mall_cd) {
                        var currRegCnt =  $("#currentRegCount").val();
                        currRegCnt = (typeof(currRegCnt) == "number") ? currRegCnt : parseInt(currRegCnt);
                        var currentRegCount = currRegCnt + mallModRegObj['num']['checkMallRegCnt'];
                        var maxRegCount = $("#maxRegCount").val();

                        if (el.dataset.mall_cd != '008') {
                            if(!fnIsRegData(currentRegCount, maxRegCount)) {
                                showAlert("현재 이용 중이신 서비스는<br>판매몰 " + $("#maxRegCount").val() + "개까지만 등록이 가능합니다.");
                                return;
                            }
                            mallModRegObj['num']['checkMallRegCnt']++;
                        }

                        el2['count']++;
                        syncCount();
                        bottomBarShow('reg');
                        addMallCard(el2);
                    }
                }
            });
        });
    });

    // 클릭 카운트 싱크 맞추는 함수
    function syncCount() {
        let allMalls = document.querySelectorAll('#allMalls .gridArea .item');
        let top10 = document.querySelectorAll('#top10 .gridArea .item');
        let mallsArr = [...allMalls, ...top10];
        openedMallData.forEach((el) => {
            mallsArr.forEach((el2) => {
                if (el['mall_cd'] == el2.dataset.mall_cd) {
                    if (el['count'] !== el2.dataset.num) {
                        el2.dataset.num = el['count'];
                        if (el['count'] > 0) {
                            el2.classList.add('on');
                        } else {
                            el2.classList.remove('on');
                        }
                    }
                }
            });
        });
    }

    function fn_helpInfo(mall_cd , mall_nm 
                , help_use_yn , help_info 
                , video_use_yn , video_url
                , detail_use_yn , detail_url
                , mall_sts_cd , err_msg
                , measure    ,post_yn
                , err_msg_use_yn    ,measure_yn
    ){
        var topTitle = document.querySelector('.topTitle p');
        var helpInfo = document.getElementById('helpInfo');
        var videoGuide = document.getElementById('videoGuide');
        var showDetail = document.getElementById('showDetail');
        var manualBtn = document.querySelector('#article_2 .flexArea .right .topTitle button');
        let topTitle_mall_cd ="";
        
        // 오류 쇼핑몰
        if(mall_sts_cd == 'ERR' && post_yn =='Y'){
            // 오류 메세지 
            if (err_msg != null && err_msg !=''){
                topTitle.innerHTML = "";
                // helpInfo.innerHTML = "";
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: ""
                });
                topTitle.dataset.mall_cd = mall_cd;
                topTitle.innerHTML += " [ " + mall_nm + " ] ";
                if(err_msg_use_yn == 'Y'){
                    topTitle.innerHTML +=" " + err_msg + "  "; 
                }
                if(measure_yn == 'Y'){
                    // helpInfo.innerHTML = measure;
                    new toastui.Editor({
                        el: document.querySelector('#viewer'),
                        initialValue: measure
                    });
                }
                manualBtn.className = 'reg'
                manualBtn.innerHTML = '등록 방법';
                manualBtn.style.display = '';
            }else{
                topTitle.innerHTML = "";
                // helpInfo.innerHTML = "";
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: ""
                });
                //topTitle.innerHTML = " [ " + mall_nm + " ] 오류 안내";
                topTitle.dataset.mall_cd = mall_cd;
                topTitle.innerHTML = "판매몰 오류안내가 없습니다.";
                manualBtn.className = 'reg'
                manualBtn.innerHTML = '등록 방법';
                manualBtn.style.display = '';
            }
        }else {
            if( help_info != null &&  help_info != ''){
                topTitle.innerHTML = "";
                // helpInfo.innerHTML = "";
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: ""
                });
                topTitle.dataset.mall_cd = mall_cd;
                topTitle.innerHTML = " [ " + mall_nm + " ] 등록방법";
                // helpInfo.innerHTML = help_info;
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: help_info
                });
                manualBtn.style.display = 'none';
            
            }else{
                topTitle.innerHTML = "";
                // helpInfo.innerHTML = "";
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: ""
                });
                topTitle.dataset.mall_cd = mall_cd;
                topTitle.innerHTML = "판매몰 도움말이 없습니다.";
                manualBtn.style.display = 'none';
            }

        }

        if (video_use_yn == "Y") {
            videoGuide.style.display = "";
            // TODO 동영상은 레이어 팝업? 새창?
            videoGuide.onclick = () => {
                window.open(video_url, "_blank");
            }
        } else {
            videoGuide.style.display = "none"
        }

        if (detail_use_yn == "Y") {
            showDetail.style.display = "";
            showDetail.onclick = () => {
                window.open(detail_url, "_blank");
            };
        } else {
            showDetail.style.display = "none"
        }
    }
    
    function fn_valModal(text , item, val){
        var val_text = text + " 을/를 입력해주세요.";
        if(val == 'goods_cate_seq_no'){
            val_text = text + "을 선택해주세요."
        }

        var registerOkBtn = document.getElementById("registerOkBtn");

        modalShow_old(`${val_text}`, {
            width: modalSize['smallSize'],
            button: [
                {
                    html: '확인',
                    class: 'silver',
                    click: () => {
                        if(val != 'goods_cate_seq_no'){
                            $('#'+item.div_id).children().find('.'+val).val("");
                        }
                        $('#'+item.div_id).children().find('.'+val).focus();
                        //$('.').children().find('.'+val).focus();
                        modalHide_old();
                        //registerOkBtn.style.readOnly = false;
                        return false;
                    },
                },
            ],
        });
        
    }

    function fn_validation(val){
        var validation = true;
        var div_id = val.div_id;
        var div_type = val.div_type;
        var cust_mall_seq_no = val.cust_mall_seq_no;
        var cert_step = val.cert_step;
        var passwd_step = val.passwd_step;
        var mall_cd = val.mall_cd;
        var sub_mall_cert_1st_nm = val.sub_mall_cert_1st_nm;
        var sub_mall_cert_2nd_nm = val.sub_mall_cert_2nd_nm;
        var mall_cert_2nd_passwd_nm = val.mall_cert_2nd_passwd_nm;
        var mall_cert_1st_id = val.mall_cert_1st_id;    
        var mall_cert_1st_passwd = val.mall_cert_1st_passwd;

        var sub_mall_cert_1st = val.sub_mall_cert_1st;
        var sub_mall_cert_2nd = val.sub_mall_cert_2nd;
        var mall_cert_2nd_passwd = val.mall_cert_2nd_passwd;
        var goods_cate_seq_no = val.goods_cate_seq_no;
        var overlapDataChk = val.overlapDataChk; 
        var mall_cert_typ_cd = val.mall_cert_typ_cd;

        var sub_mall_auth_1st_esse_yn = val.sub_mall_auth_1st_esse_yn;
        var    sub_mall_auth_2nd_esse_yn = val.sub_mall_auth_2nd_esse_yn;
        var    data_biz_no_auth_esse_yn = val.data_biz_no_auth_esse_yn;
        var mall_auth_otp_key = val.mall_auth_otp_key;
        var otp_auth_use_yn = val.otp_auth_use_yn;


        // 20210420
        // 인터파크 OM 및 MD 관련 중복 체크 여부
        // 업체 번호 , 공급계약번호 정보가 없다면 쇼핑몰 아이디 중복 체크
        // 업체 번호 , 공급계약번호 정보가 있다면 중복 체크는 SMP에서 
        // => (기존 등록 및 수정 로직이 같이 있게 개발이 되여 있어서 이렇게 처리 할수 밖에 없었음.)
        var checkFlag = true;
        if (mall_cd == "005" || mall_cd == "006") {
            if (sub_mall_cert_1st == "" && sub_mall_cert_2nd == "") {
                checkFlag = true;
            } else {
                // 20210420 요청 
                // 업체 번호 및 공급계약번호 정보 중 하나의 정보만 입력되여 있다면 2개의 정보 다 입력 하도록.
                if (sub_mall_cert_1st == "") {
                    var text = sub_mall_cert_1st_nm;
                    fn_valModal(text , val, 'sub_mall_cert_1st');
                    checkFlag = false;
                    return false;
                } 

                if (sub_mall_cert_2nd == "") {
                    var text = sub_mall_cert_2nd_nm;
                    fn_valModal(text , val, 'sub_mall_cert_2nd');
                    checkFlag = false;
                    return false;
                } 
            }
        } else {
            checkFlag = true;
        }

        
        if(mall_cert_1st_id == null || mall_cert_1st_id ==''){
            var text = '아이디';
            fn_valModal(text , val, 'mall_cert_1st_id');
            validation = false;
            return false;
        }        

        if(mall_cert_1st_passwd == null || mall_cert_1st_passwd ==''){
            var text = '패스워드';
            fn_valModal(text , val, 'mall_cert_1st_passwd');
            validation = false;
            return false;
        }    
        
        if(otp_auth_use_yn == 'Y'){
            if(mall_auth_otp_key == null || mall_auth_otp_key == '' ){
                var text = 'QR 등록';
                fn_valModal(text,val,'mall_auth_otp_key');
                validation = false;
                return false;
            }
            
        }

        if(sub_mall_auth_1st_esse_yn == 'Y'){
            if(sub_mall_cert_1st_nm != null && sub_mall_cert_1st_nm != ''){
                if(sub_mall_cert_1st == null || sub_mall_cert_1st ==''){
                    var text = sub_mall_cert_1st_nm;
                    fn_valModal(text , val, 'sub_mall_cert_1st');
                    validation = false;
                    return false;
                }    
            }
        }

        if(sub_mall_auth_2nd_esse_yn == 'Y'){
            if(sub_mall_cert_2nd_nm != null && sub_mall_cert_2nd_nm != ''){
                if(sub_mall_cert_2nd == null || sub_mall_cert_2nd ==''){
                    var text = sub_mall_cert_2nd_nm;
                    fn_valModal(text , val, 'sub_mall_cert_2nd');
                    validation = false;
                    return false;
                }    
            }
        }
        
        if(mall_cert_2nd_passwd_nm != null && mall_cert_2nd_passwd_nm != ''){
            if(mall_cert_2nd_passwd == null || mall_cert_2nd_passwd ==''){
                var text = mall_cert_2nd_passwd_nm;
                fn_valModal(text , val, 'mall_cert_2nd_passwd');
                validation = false;
                return false;
            }    
        }

        if(goods_cate_seq_no == null || goods_cate_seq_no == ''){
            var text = '주요 판매 품목';
            fn_valModal(text , val, 'goods_cate_seq_no');
            validation = false;
            return false;
        }
        return validation;
    }

    // 판매몰 등록/수정 초기화 버튼 클릭 시
    document.querySelector('#article_2 #initialize').addEventListener('click', () => {
        if (
            document.querySelector('#article_2 .flexArea .left .contents .modifyArea .modifyBox') ||
            document.querySelector(
                '#article_2 .flexArea .left .contents .registerArea .registerBox'
            )
        ) {
            function initialize() {
                document
                    .querySelectorAll('#article_2 .flexArea .left #deleteItem')
                    .forEach((el) => {
                        el.click();
                    });
                modalHide_old();
                mallModRegNoDataText.classList.remove('hide');
            }
            modalShow_old('정말 초기화 하시겠습니까?', {
                width: modalSize['smallSize'],
                button: [
                    {
                        html: '취소',
                        class: 'silver',
                        click: modalHide_old,
                    },
                    {
                        html: '초기화',
                        class: 'dark',
                        click: initialize,
                    },
                ],
            });
        } else {
            modalShow_old('초기화할 데이터가 없습니다!', {
                width: modalSize['smallSize'],
                button: [
                    {
                        html: '확인',
                        class: 'silver',
                        click: modalHide_old(),
                    },
                ],
            });
        }
    });

    // bottomBar의 바로가기 버튼 클릭 시
    document.querySelector('#goto').addEventListener('click', () => {
        window.scrollTo({
            top: document.querySelector('#article_2').offsetTop,
            behavior: 'smooth',
        });
        bottomBar.classList.remove('show');
    });

    // Top 버튼 클릭 시
    document.querySelector('#goTop').addEventListener('click', () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    });

    // 20221226 수정
    // 판매몰 도움말 버튼 클릭시 "등록 방법" <-> "오류 안내" 전환 기능
    const manualBtn = document.querySelector('#article_2 .flexArea .right .topTitle button');
    manualBtn.addEventListener('click', (e) => {
        var topTitle = document.querySelector('.topTitle p');
        var helpInfo = document.getElementById('helpInfo');
        var btn_mall_nm = e.target.getAttribute('data-mall_nm');
        var btn_help_info = e.target.getAttribute('data-help_info');
        var btn_err_msg = e.target.getAttribute('data-err_msg');
        var btn_measure = e.target.getAttribute('data-measure');
        var btn_post_yn = e.target.getAttribute('data-post_yn');
        var btn_err_msg_use_yn = e.target.getAttribute('data-err_msg_use_yn');
        var measure_yn = e.target.getAttribute('data-measure_yn');
        
        if(e.target.classList.contains('err') && btn_post_yn == 'Y'){
            e.target.className = 'reg';
            e.target.innerHTML = '등록 방법';

            // if ((btn_err_msg == null || btn_err_msg =='' || btn_err_msg == 'null')){
            topTitle.innerHTML = "";
            // helpInfo.innerHTML = "";
            new toastui.Editor({
                el: document.querySelector('#viewer'),
                initialValue: ""
            });
            topTitle.innerHTML += " [ " + btn_mall_nm + " ] ";
            
            if(btn_err_msg_use_yn == 'Y'){
                topTitle.innerHTML += " " + btn_err_msg + "  ";
            }
            
            if(measure_yn == 'Y'){
                // helpInfo.innerHTML = btn_measure;
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: btn_measure
                });
            }
        
        }else{
            e.target.className = 'err';
            e.target.innerHTML = '오류 안내';

            if(btn_help_info != null && btn_help_info != ''){
                topTitle.innerHTML = "";
                // helpInfo.innerHTML = "";
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: ""
                });
                topTitle.innerHTML = " [ " + btn_mall_nm + " ] 등록방법";
                // helpInfo.innerHTML = btn_help_info;
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: btn_help_info
                });
            }else {
                topTitle.innerHTML = "";
                // helpInfo.innerHTML = "";
                new toastui.Editor({
                    el: document.querySelector('#viewer'),
                    initialValue: ""
                });
                topTitle.innerHTML = "판매몰 도움말이 없습니다.";
            }
        }

    });

})();