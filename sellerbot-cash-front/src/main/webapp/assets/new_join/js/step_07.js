import createElement from './import/createElement.js';
import modalShow from './import/modalShow.js';
import numberWithCommas from '/assets/new_join/js/import/numberWithCommas.js';


let bankList;

( async () => {
    bankList = await fetch('/pub/member/bankList')
        .then((res) => res.json())
        .then((data) => {
            data.forEach((el) => {
                el['count'] = 0;
            });s
            return data;
        })
        .catch((error) => {
            console.error(error);
        });

    });


    let currentRegCount;

    // 완료 버튼 
    document.querySelector('#nextStep').addEventListener('click',()=>{
        location.href = "/";
    });
  
    // 등록 판매몰 리스트 생성
    function accountList() {
        let tbody = document.querySelector('#accountListTbody');
        $('tbody tr').remove();
        $.ajax({
            url: '/pub/membe/accountList',
            type: 'GET',
            async : false,
            success: function (data) {
                var all_acct_cnt = 0;
                var nor_acct_cnt = 0;
                var err_acct_cnt = 0;
                var del_acct_cnt = 0;
                var ins_acct_cnt = 0; 
                currentRegCount = 0;
                if (data !== null && data.length > 0) {
                    data.forEach((el) => {
                        currentRegCount++;
                        all_acct_cnt++;     //전체
                        switch(el['cust_acct_sts_cd']){
                            case "NOR":     //정상
                                nor_acct_cnt++;
                                break;
                            case "INS" :    //점검
                                ins_acct_cnt++;
                                break;
                            case "ERR" :    //에러
                                err_acct_cnt++;
                                break
                            default :
                                break;        
                        }

                        let tr = createElement('tr',{
                            data: {
                                cust_acct_seq_no: el['cust_acct_seq_no'],
                            },
                        });

                        let td1 = createElement('td',{
                            innerHTML: el['bank_nm']
                        });    
                        tr.append(td1);

                        let td2 = createElement('td',{
                            innerHTML: el['acct_no']
                        });
                        tr.append(td2);

                        let td3 = createElement('td',{
                            innerHTML:   numberWithCommas(el['acct_prc']) + " 원"
                        });    
                        tr.append(td3);

                        let td4; 
                        if(el['acct_prc_mod_ts'] == null || el['acct_prc_mod_ts'] == ""){
                            td4 = createElement('td',{
                                innerHTML: "없음"
                            }); 
                        }else{
                            td4 = createElement('td',{
                                innerHTML: el['acct_prc_mod_ts']
                            }); 
                        }
                          
                        tr.append(td4);

                        var mallList = "";
                        el['mall_list'].forEach((el2,idx) => {
                            if(idx > 0){
                                mallList += " ," + el2['mall_nm'];
                            }else{
                                mallList += el2['mall_nm'];
                            }
                        });
                        let td5 ;
                        if(mallList == ""){
                            td5 = createElement('td',{
                                innerHTML: "없음"
                            });    
                            tr.append(td5);
                        }else{
                            td5 = createElement('td',{
                                innerHTML: mallList
                            }); 
                           
                        }

                        tr.append(td5);
                      
                        let td6 = createElement('td',{
                            innerHTML: "업데이트 일자 : " + moment(el['mod_ts']).format("YYYY-MM-DD") 
                        });    
                        tr.append(td6);
                        
                        tbody.append(tr);

                    });

                } else {
                    let tr = createElement('tr' ,{
                        className :'noData'
                    });    
                    
                    let td = createElement('td',{
                        colSpan : 6,
                        innerHTML : '등록한 계좌가 없습니다'
                    });
                    
                    tr.append(td);
                    tbody.append(tr);
                } // else end
              
                document.getElementById('all').innerHTML = "전체 " + all_acct_cnt;
                document.getElementById('err').innerHTML = "오류 " + err_acct_cnt;
                document.getElementById('ins').innerHTML = "점검 " + ins_acct_cnt;
                document.getElementById('nor').innerHTML = "정상 " + nor_acct_cnt;

            }
        });    
    }        

    
    accountList();
  
// 계좌등록 모달 닫기
document.querySelectorAll('.closeModal').forEach((el) => {
    el.addEventListener('click',()=>{
        $("#modal_01").css('display', 'none');
    document.querySelector('body').style.overflow = '';
    });
});




// 계좌등록 도움말 모달 닫기
document.querySelectorAll('.closeModal2').forEach((el) => {
    el.addEventListener('click',()=>{
        $("#modal_02").css('display', 'none');
        document.querySelector('body').style.overflow = '';
    });
});

// 계좌등록 모달 오픈
document.querySelector('#regAcc').addEventListener('click', () => {
    var maxRegCount = document.getElementById('acctRegPsbCnt').value;
   
    if(!fnIsRegData(currentRegCount, maxRegCount)) {
        showAlert("현재 이용 중이신 서비스는<br>계좌 " + maxRegCount + "개까지만 등록이 가능합니다.");
        return;
    }else{
        initModal('reg');
        $("#reg_bank_cert_m_seq_no").val(18);
        changeInputFields($("#reg_bank_cert_m_seq_no").val(), "reg");
        $("#modal_01").css('display', 'flex');
    }
});

//계좌등록 도움말 모달 오픈
document.querySelector('#regAccManual').addEventListener('click', () => {
    $("#modal_02").css('display', 'flex');
});

// 은행 셀렉트 박스 선택
document.querySelector('#reg_bank_cert_m_seq_no').addEventListener('change',(el) => {
    var value = document.querySelector('#reg_bank_cert_m_seq_no').value;
    initModal('reg');
    changeInputFields(value, 'reg');
});

//계정 테스트
document.querySelector('#accTest').addEventListener('click',() => {
    btnPressedTestAccount('reg');
});

//계좌번호 중복 확인
document.querySelector('#confirmAccNum').addEventListener('click',() => {
    var acctNo = $("#reg_acct_no").val();
    checkDupl(acctNo, "reg");
});

// 입력값 변경(change) 확인 
const inputText = document.querySelectorAll('.inputText').forEach((el) =>{
    el.addEventListener("change",function(e){
        //입력값 변경시 계정테스트 값 'N'
        document.getElementById('regAcctTestSuccYN').value = '';

    });
});

// 입력값 변경(keyup) 확인 
document.querySelectorAll('.inputText').forEach((el) =>{
    el.addEventListener("keyup",function(e){
        //입력값 변경시 계정테스트 값 'N'
        document.getElementById(el.id+'_error').style.display='none';
        document.getElementById('regAcctTestSuccYN').value = ''; 
        if(el.id == 'reg_acct_no'){
            document.getElementById('chkAcctDuplYN').value = '';
        }
        //if(el.id == '')     
    });
});

//계좌번호 변경 확인
const reg_acct_no = document.getElementById('reg_acct_no');
reg_acct_no.addEventListener("change",function(e){
    //계좌번호 변경시 중복체크 다시
    document.getElementById('chkAcctDuplYN').value = '';
    //계좌번호 변경시 계정테스트 다시
    document.getElementById('regAcctTestSuccYN').value = '';
});

//계좌 등록
document.querySelector('#submit').addEventListener('click',()=>{

    if($("#regAcctTestSuccYN").val() != "Y") {
        showAlert("계정 테스트 성공 후 등록 가능합니다.");
        return;
    }

    if (!isVali()) {
        showAlert('계정 테스트 성공 후 등록 가능합니다.');
        return; 
    }
    
    // 기본 공통 값
    var param = {
        "bank_cert_m_seq_no": $("#reg_bank_cert_m_seq_no").val()
    };
    var input = $("#modal_01").find(".form_container").find("[required]");
    var jqThis;
    $.each(input, function () {
        jqThis = $(this);
        if (nonNull(jqThis.val())) {
            var id = jqThis.attr("id").replace("reg_", "");
            param[id] = jqThis.val();
        }
    });

    // 통화
    var crc_use_cd = $("#reg_bank_cert_m_seq_no").find("option:selected").data("crccd");
    if (crc_use_cd == "S") {
        param.crc = $("#reg_crc").val();
    }

    // 입금 확인용 계좌 여부
    if($("#cafe24_usr_yn").val() == 'Y' && $("input:checkbox[id='reg_reco_yn']").is(":checked"))
        param.reco_yn = "Y";
    else
        param.reco_yn = "N";

    $.ajax({
        url: '/sub/account/regAccount'
        , type: 'post'
        , async: false
        // , dataType: 'json'
        , contentType: 'application/json'
        , data: JSON.stringify([param])
        , success: function (res) {
            $("#modal_01").css('display', 'none');
            document.querySelector('body').style.overflow = '';
            showAlert("계좌가 등록되었습니다.");
            accountList();
        }
        , error: function (error) {
            showAlert("이미 동일한 계좌가 등록되어 있습니다. <br>입력하신 정보를 다시 확인해주세요.");
        }
    });
});

document.querySelectorAll('.onlyNumber').forEach((el) => {
    el.addEventListener('keyup', () => {
        el.value = el.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    });
});

//선택은행 바로 가기
document.querySelector('#btnPressedOpenBank').addEventListener('click',() => {
    btnPressedOpenBank('reg');
});

// 계좌 중복체크
var checkDupl = function(acctNo, area) {
    if (acctNo.length == 0) {
        $("#" + area + "_checkDupl_info").text("*계좌 번호를 입력하세요.").css({color: "Red", fontSize: "12px"});
        $("#reg_acct_no_error").show();
        $("#chkAcctDuplYN").val("N");
    } else if (isAcctNo_numberNLetter(acctNo)) {
        $.ajax({
            url : '/sub/account/checkDupl',
            type: 'post',
            data: {"acct_no" : acctNo},
            success: function(data) {
                if (data >= 1) {
                    if (area == "mdfy") {
                        $("reg_acct_no_error").text("*수정 가능한 계좌번호입니다.").css({color: "blue", fontSize: "12px"});
                        $("#reg_acct_no_error").show();
                        $("#chkAcctDuplYN").val("Y");
                    } else {
                        $("reg_acct_no_error").text("*이미 등록된 계좌번호입니다.").css({color: "Red", fontSize: "12px"});
                        $("#reg_acct_no_error").show();
                        $("#chkAcctDuplYN").val("N");
                    }
                } else if(data == 0) {
                    $("#reg_acct_no_error").text("*사용 가능한 계좌번호입니다.").css({color: "blue", fontSize: "12px"});
                    $("#reg_acct_no_error").show();
                    $("#chkAcctDuplYN").val("Y");
                }
            }, error: function (error) {
                showAlert("요청이 실패하였습니다.", type);
            }
        });
    } else {
        $("#reg_acct_no_error").text("*계좌 번호가 정확하지 않습니다.").css({color: "Red", fontSize: "12px"});
        $("#chkAcctDuplYN").val("N");
    }
};

var btnPressedTestAccount = function(type) {
    if (!isVali()) {
        return false; 
    }

     if($("#chkAcctDuplYN").val() == "") {
        showAlert("계좌 중복 확인 후 사용 가능합니다.", type);
     }else if($("#chkAcctDuplYN").val() == "N"){
        showAlert("중복된 계좌번호입니다.<br> 계좌번호 확인 후 사용 가능합니다.", type);
     }else {
        var bankCd = $("#" + type + "_bank_cert_m_seq_no").find("option:selected").data("bankcd");
        var param = {
            "bankCd": bankCd
        };

        var input = $(".form_container").find("[required]");
        var jqThis;
        $.each(input, function () {
            jqThis = $(this);
            if (nonNull(jqThis.val())) {
                var id = jqThis.attr("id").replace(type + "_", "");
                param[id] = jqThis.val();
            }
        });
        
        showLoadingModal();
        // beforeSend, complete 함수들로 로딩바 처리 안됨
        setTimeout(function () {
            testAccount(type, param);
        }, 100);
     }
        
};
// 계정 테스트 
var testAccount = function(type, param) {
    $.ajax({
        url: '/sub/account/verifyAccount'
        , type: 'post'
        , async: false
        , data: param
        , success: function (res) {
            hideLoadingModal();

            if(res.errCode == "00000000") {
                $("#" + type + "AcctTestSuccYN").val("Y");
                showAlert("입력하신 정보로 계정 테스트를 성공하였습니다.", type);
            }
            else {
                showAlert("요청이 실패하였습니다. <br>(" + res.errMsg + ")", type);
            }
        }
        , error: function (error) {
            hideLoadingModal();
            showAlert("요청이 실패하였습니다. <br>입력하신 정보를 다시 확인해주세요.", type);
        }
    }); // ajax end
};

// 은행별 입력 필드들 설정
var changeInputFields = function(value, type) {
    changeBankInfo('reg');
    $("[required]").each(function(){
        $(this).prop("required", false);
    });

    var dataKeyList = [{"dataKey": "bankidyn", "targetId": "bank_id"}, // 빠른조회 은행아이디
                        {"dataKey": "bankpwyn", "targetId": "bank_passwd"}, // 빠른조회 은행비밀번호
                        {"dataKey": "acctnoyn", "targetId": "acct_no"}, // 계좌번호
                        {"dataKey": "acctpwyn", "targetId": "acct_passwd"}, // 계좌 비밀번호
                        {"dataKey": "ctznoyn", "targetId": "ctz_biz_no"}, // 주민등록 (사업자)번호
                        {"dataKey": "crccd", "targetId": "crc"}]; // 통화

    for(var i=0; i<dataKeyList.length; i++) {
        var id = "#" + type + "_" + dataKeyList[i].targetId;
        var data = $("#" + type + "_bank_cert_m_seq_no").find("option:selected").data(dataKeyList[i].dataKey);
        if(data == "Y") {
            $(id + "_area").show();
            $(id).prop("required", true);
        }
        else if(data == "S") {
            $(id + "_area").show();
        }
        else {
            $(id + "_area").hide();
            $(id).removeAttr('required');
        }
        
        $(id).val("");
    }

    // 예금주 데이터 초기화
    $("#" + type + "_" + "dpsi_nm").val("");
    $("#" + type + "_" + "dpsi_nm").prop("required", true);

    // 입금 확인용 계좌 여부 초기화
    if($("#cafe24_usr_yn").val() == 'Y')
        $("input:checkbox[id='" + type + "_reco_yn']").prop("checked", false);

    // 대구은행인 경우만 계좌번호와 비밀번호 타이틀 변경
    if (value == 10) {
        $("#" + type + "_acctNoTitle").text("안전계좌번호");
        $("#" + type + "_acctPwdTitle").text("안전계좌비밀번호");
        $("#" + type + "_acct_no").attr("data-valitype", "acctNo_numberNLetter");
    }
    else {
        $("#" + type + "_acctNoTitle").text("계좌번호");
        $("#" + type + "_acctPwdTitle").text("계좌비밀번호");
        $("#" + type + "_acct_no").attr("data-valitype", "acctNo");
    }
};



var initModal = function(type) {
    $("#" + type + "_bank_id_error").css('display', 'none');
    $("#" + type + "_bank_passwd_error").css('display', 'none');
    $("#" + type + "_dpsi_nm_error").css('display', 'none');
    $("#" + type + "_acct_no_error").css('display', 'none');
    $("#" + type + "_acct_no_error").text("*계좌번호를 바르게 입력해주세요.").css({color: "red", fontSize: "12"});;
    $("#" + type + "_acct_passwd_error").css('display', 'none');
    $("#" + type + "_ctz_biz_no_error").css('display', 'none');
    $("#" + type + "_crc_error").css('display', 'none');
};


var changeBankInfo = function(type) {
    var html = [];
    var seqNo = $("#" + type + "_bank_cert_m_seq_no").val();
    if(typeof(seqNo) == 'string')
        seqNo = parseInt(seqNo);
    switch (seqNo) {
        case 1: // 산업은행
            html.push('<span class="line"></span>');
            html.push('<p>산업은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 산업은행 홈페이지(<a href="http://www.kdb.co.kr" target = "_brank">http://www.kdb.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [뱅킹관리 → 계좌관리 → 계좌관리] 에서 등록');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 산업은행 홈페이지(<a href="http://www.kdb.co.kr" target = "_brank">http://www.kdb.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ A. 기업뱅킹 (USB 1개 사용) : [뱅킹관리 → 계좌관리 → 빠른조회대상계좌설정]에서 등록<br />B. 기업뱅킹 (USB 2개 이상) : [대표관리자 인터넷뱅킹 접속 → 뱅킹관리 → 계좌관리 → 빠른조회대상계좌설정] 에서 등록');
            html.push('</pre>');
            break;
        case 2: // 기업은행
            html.push('<span class="line"></span>');
            html.push('<p>기업은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹/기업뱅킹 공통');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 기업은행 홈페이지(<a href="http://www.ibk.co.kr" target = "_brank">http://www.ibk.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ 뱅킹관리 → 계좌관리 → 빠른계좌조회서비스 신청/해지】에서 신청');
            html.push('</pre>');
            break;
        case 3: // 국민은행
            html.push('<span class="line"></span>');
            html.push('<p>국민은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 국민은행 홈페이지(<a href="http://www.kbstar.com" target = "_brank">http://www.kbstar.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ 【뱅킹관리 → 계좌관리 → 빠른조회서비스】에서 신청');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 국민은행 홈페이지(<a href="http://www.kbstar.com" target = "_brank">http://www.kbstar.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③【뱅킹관리 → MASTER 설정 → 빠른조회서비스 등록/해제】에서 신청');
            html.push('</pre>');
            break;
        case 4: // 수협은행
            html.push('<span class="line"></span>');
            html.push('<p>수협은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 수협은행 홈페이지(<a href="http://www.suhyup-bank.com" target = "_brank">http://www.suhyup-bank.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [MY정보 > 계좌정보관리 > 간편조회서비스]에서 등록');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 수협은행 홈페이지(<a href="http://www.suhyup-bank.com" target = "_brank">http://www.suhyup-bank.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③【[MY정보 > 계좌정보관리 > 간편조회서비스]에서 등록');
            html.push('</pre>');
            break;
        case 5: // 농협
            html.push('<span class="line"></span>');
            html.push('<p>농협은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 농협 홈페이지(<a href="http://banking.nonghyup.com" target = "_brank">http://banking.nonghyup.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③【MY뱅크 → 뱅킹서비스관리 → 빠른조회】에서 등록/해지');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 농협 홈페이지(<a href="http://banking.nonghyup.com" target = "_brank">http://banking.nonghyup.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③【기업인터넷뱅킹 → 이용자관리 → 계좌관리 → 빠른 조회 계좌관리】에서 등록');
            html.push('</pre>');
            break;
        case 6: // 우리
            html.push('<span class="line"></span>');
            html.push('<p>우리은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 우리은행 홈페이지(<a href="http://wooribank.com" target = "_brank">http://wooribank.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③【뱅킹관리 → 뱅킹계좌관리 → 스피드조회계좌등록/해지】에서 신청');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 우리은행 홈페이지(<a href="http://wooribank.com" target = "_brank">http://wooribank.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③【뱅킹관리 → 뱅킹계좌관리 → 스피드조회계좌등록/해지】에서 신청');
            html.push('</pre>');
            break;
        case 7: // sc은행
            html.push('<span class="line"></span>');
            html.push('<p>SC은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 스탠다드차타드은행 홈페이지(<a href="https://www.standardchartered.co.kr/" target = "_brank">https://www.standardchartered.co.kr/</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③【인터넷뱅킹 가입고객 : 【서비스 및 설정 → 통장관리 → 스피드조회등록】에서 신청');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 스탠다드차타드은행 홈페이지(<a href="https://www.standardchartered.co.kr/" target = "_brank">https://www.standardchartered.co.kr/</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통하여 인터넷뱅킹 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ A. First Biz 가입고객 : 【이용자관리 → 관리자 업무 → 계좌정보관리 → 스피드계좌관리】에서 등록<br />B. Straight2Bank 가입고객 : 【관리자화면 → 계좌정보관리 → 스피드계좌등록/해지】에서 등록');
            html.push('</pre>');
            break;
        case 8: // 신한은행
            html.push('<span class="line"></span>');
            html.push('<p>신한은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 신한은행 홈페이지(<a href="http://www.shinhan.com" target = "_brank">http://www.shinhan.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 좌측중앙 [간편조회서비스] > [계좌조회] > 로그인(홈페이지 회원 아이디와 비밀번호 필요)');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ 로그인후 좌측상단 [계좌조회] > [간편계좌관리] > [간편계좌조회 추가하기]');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 신한은행 홈페이지(<a href="http://www.shinhan.com" target = "_brank">http://www.shinhan.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 좌측중앙 [간편조회서비스] > [계좌조회] > 로그인(홈페이지 회원 아이디와 비밀번호 필요) ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ 로그인후 좌측상단 [계좌조회] > [간편계좌관리] > [간편계좌조회 추가하기]');
            html.push('</pre>');
            break;
        case 9: // 씨티은행
            html.push('<span class="line"></span>');
            html.push('<p>씨티은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 씨티은행 홈페이지(<a href="http://www.citibank.co.kr" target = "_brank">http://www.citibank.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 빠른조회 서비스 신청 - [개인 로그인 > 웹회원가입 > 웹회원등록]');
            html.push('<br/>');
            html.push('<br/>');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 씨티은행 홈페이지(<a href="http://www.citibank.co.kr" target = "_brank">http://www.citibank.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② [법인 로그인 > 웹회원가입 > 기업인터넷뱅킹 > 인증서 로그인> CAT-i 메뉴 > 이용자관리 > 빠른조회서비스]에서 등록 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('</pre>');
            break;
        case 10: // 대구은행
            html.push('<span class="line"></span>');
            html.push('<p>대구은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 대구은행 홈페이지(<a href="https://www.dgb.co.kr" target = "_brank">https://www.dgb.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [안전계좌조회 > 안전계좌신청]에서 등록');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 대구은행 홈페이지(<a href="https://www.dgb.co.kr" target = "_brank">https://www.dgb.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [안전계좌조회 > 안전계좌신청]에서 등록');
            html.push('</pre>');
            break;
        case 11: // 부산은행
            html.push('<span class="line"></span>');
            html.push('<p>부산은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 부산은행 홈페이지(<a href="http://www.busanbank.co.kr" target = "_brank">http://www.busanbank.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [메인화면중앙 > 빠른조회서비스 > 개인빠른조회 계좌등록]에서 등록');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 부산은행 홈페이지(<a href="http://www.busanbank.co.kr" target = "_brank">http://www.busanbank.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [메인화면중앙 > 빠른조회서비스 > 기업빠른조회 계좌등록]에서 등록');
            html.push('</pre>');
            break;
        case 12: // 광주은행
            html.push('<span class="line"></span>');
            html.push('<p>광주은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 광주은행 홈페이지(<a href="http://www.kjbank.com" target = "_brank">http://www.kjbank.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [마이뱅킹관리 > 계좌통합관리 > 빠른서비스 사용계좌관리]에서 등록');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 광주은행 홈페이지(<a href="http://www.kjbank.com" target = "_brank">http://www.kjbank.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인 ');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [사용자관리 > 계좌통합관리 > 빠른서비스 계좌등록]에서 등록');
            html.push('</pre>');
            break;
        case 13: // 제주은행
            html.push('<span class="line"></span>');
            html.push('<p>제주은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 제주은행 홈페이지(<a href="http://www.e-jejubank.com" target = "_brank">http://www.e-jejubank.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [사용자관리 > 바로바로서비스 신청]에서 신청');
            html.push('</pre>');
            break;
        case 14: // 전북은행
            html.push('<span class="line"></span>');
            html.push('<p>전북은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 전북은행 홈페이지(<a href="http://www.jbbank.co.kr" target = "_brank">http://www.jbbank.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [뱅킹관리 > 계좌관리 > 바로바로서비스 계좌관리]에서 등록');
            html.push('</pre>');
            break;
        case 15: // 경남은행
            html.push('<span class="line"></span>');
            html.push('<p>경남은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 경남은행 홈페이지(<a href="http://www.knbank.co.kr" target = "_brank">http://www.knbank.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ [왼쪽상단 My Banking > 계좌관리> 빠른서비스 계좌관리]에서 등록');
            html.push('</pre>');
            break;
        case 16: // 새마을금고
            html.push('<span class="line"></span>');
            html.push('<p>새마을금고</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 새마을금고 홈페이지(<a href="http://www.kfcc.co.kr" target = "_brank">http://www.kfcc.co.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 개인뱅킹 클릭');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('④ [뱅킹관리 > 계좌관리 > 즉시조회 계좌관리] 클릭');
            html.push('<br/>');
            html.push('<br/>');
            html.push('⑤ 보안매체 (OTP 또는 보안카드) 인증 및 공인인증서 인증');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① [새마을금고 영업점]에서 신청');
            html.push('</pre>');
            break;
        case 17: // 우체국
            html.push('<span class="line"></span>');
            html.push('<p>우체국</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 우체국 홈페이지(<a href="http://www.epostbank.go.kr" target = "_brank">http://www.epostbank.go.kr</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② [예금간편서비스 > 간편조회계좌등록]에서 등록');
            html.push('</pre>');
            break;
        case 18: // 하나은행
               html.push('<span class="line"></span>');
            html.push('<p>KEB하나은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① KEB하나은행 홈페이지(<a href="http://www.kebhana.com">http://www.kebhana.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ 마이하나 &gt; 계좌정보관리 &gt; 빠른조회관리에서 신청');
            html.push('</pre>');
            html.push('<span class="line dot"></span>');
            html.push('<pre>▣ 기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① KEB하나은행 홈페이지(<a href="http://www.kebhana.com">http://www.kebhana.com</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인 - 기업/CMS 로 ');
            html.push('분류되며, ');
            html.push('CMS인 경우 지점방문 후 신청해야 합니다.');
            html.push('<br/>');
            html.push('<br/>');
            html.push('③ 뱅킹관리 &gt; 계좌관리 &gt; 빠른조회계좌관리에서 신청');
            html.push('</pre>');
            break;            
        case 19: // 신협
            html.push('<span class="line"></span>');
            html.push('<p>신협은행</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인/기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 신협 홈페이지(<a href="http://openbank.cu.co.kr" target ="_brank">http://openbank.cu.co.kr)</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 좌측중앙[조회전용 > 조회전용서비스 > 신규 > 계좌등록/해지]에서 등록');
            html.push('</pre>');
            break;
        case 20: // 산림조합
            html.push('<span class="line"></span>');
            html.push('<p>산림조합</p>');
            html.push('<span class="line"></span>');
            html.push('<pre>▣ 개인/기업뱅킹');
            html.push('<br/>');
            html.push('<br/>');
            html.push('① 산림조합 홈페이지(<a href="http://banking.nfcf.or.kr/index.jsp" target ="_brank">http://banking.nfcf.or.kr/index.jsp)</a>)');
            html.push('에 접속');
            html.push('<br/>');
            html.push('<br/>');
            html.push('② 공인인증서를 통한 인터넷뱅킹에 로그인');
            html.push('<br/>');
            html.push('<br/>');
            html.push('⑤ [계좌관리> 빠른조회계좌관리 > 이용계좌등록]에서 등록');
            html.push('</pre>');
            break;    
        default:
            break;
    }

    var guide = $("#modal_02").find(".bank_info");
    guide.empty();
    guide.append(html.join(""));
};


var btnPressedOpenBank = function(type) {
    var bankUrl = "";
    var seqNo = $("#" + type + "_bank_cert_m_seq_no").val();
    if(typeof(seqNo) == 'string')
        seqNo = parseInt(seqNo);
    
    switch (seqNo) {
        case 1: // 산업은행
            bankUrl = "http://www.kdb.co.kr";
            break;
        case 2: // 기업은행
            bankUrl = "http://www.ibk.co.kr";
            break;
        case 3: // 국민은행
            bankUrl = "http://www.kbstar.com";
            break;
        case 4: // 수협은행
            bankUrl = "http://www.suhyup-bank.com";
            break;
        case 5: // 농협
            bankUrl = "http://banking.nonghyup.com";
            break;
        case 6: // 우리
            bankUrl = "http://wooribank.com";
            break;
        case 7: // sc은행
            bankUrl = "https://www.standardchartered.co.kr/";
            break;
        case 8: // 신한은행
            bankUrl = "http://www.shinhan.com";
            break;
        case 9: // 씨티은행
            bankUrl = "http://www.citibank.co.kr";
            break;
        case 10: // 대구은행
            bankUrl = "https://www.dgb.co.kr";
            break;
        case 11: // 부산은행
            bankUrl = "http://www.busanbank.co.kr";
            break;
        case 12: // 광주은행
            bankUrl = "http://www.kjbank.com";
            break;
        case 13: // 제주은행
            bankUrl = "http://www.e-jejubank.com";
            break;
        case 14: // 전북은행
            bankUrl = "http://www.jbbank.co.kr";
            break;
        case 15: // 경남은행
            bankUrl = "http://www.knbank.co.kr";
            break;
        case 16: // 새마을금고
            bankUrl = "http://www.kfcc.co.kr";
            break;
        case 17: // 우체국
            bankUrl = "http://www.epostbank.go.kr";
            break;
        case 18: // 하나은행
            bankUrl = "http://www.kebhana.com";
            break;            
        case 19: // 신협
            bankUrl = "http://openbank.cu.co.kr";
            break;
        case 20: // 산림조합
            bankUrl = "http://banking.nfcf.or.kr/index.jsp";
            break;    
        default:
            break;
    }

    window.open(bankUrl);
};

(() => {

    // 결제 완료 팝업
    let goodsTypCd = document.getElementById('goodsTypCd').value;
    let goodsTypNm = "";
    let price = document.getElementById('price').value; 
    let nextBillYMD = document.getElementById('nextBillYMD').value; 
    let nextBillDate = document.getElementById('nextBillDate').value; 
    let goodName = document.getElementById('goodName').value; 
    let cardNum = document.getElementById('cardNum').value;
    let successYN = document.getElementById('successYN').value; 
    let acctRegPsbCnt = document.getElementById('acctRegPsbCnt').value;
    let mallRegIdPsbCnt = document.getElementById('mallRegIdPsbCnt').value;

    if(successYN == 'Y'){
        let nextBillYMDFmt = document.getElementById('nextBillYMdFmt').innerHTML;
      
        switch(goodsTypCd){
            case "PASB":     
                goodsTypNm = "메가봇";
                break;
            case "PASP" :    
                goodsTypNm = "기가봇";
                break;
            case "PAFP" :    
                goodsTypNm = "테라봇";
                break;    
            default :
                break;        
        }    
    
        let popupHtml = "";
        popupHtml += `
        <div class="head">
            <h5>결제수단 등록 완료</h5>
            <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
        </div>
        <div class="body" style="background-color: white;">
            <p>셀러봇캐시를 이용해주셔서 감사합니다.</p>
            <p>셀러봇캐시 요금제( `+goodName+` ) 이용이 시작되었습니다.</p>
            <p><b>다음 정기결제일은 `+nextBillYMDFmt+` 입니다.</b></p>
            <h2>최종 결제금액</h2>
            <div class="flexBox">
                <div class="left">
                    <h5>`+ goodsTypNm +`</h5>
                    <!-- 20230323 수정 -->
                    <h4 id="bankBotPrice">`+numberWithCommas(price)+`원</h4>
                    <small>(VAT 포함)</small>
                </div>
                <div class="right">
                    <div class="content">
                        <div>
                        <div class="box">
                            <span class="check"></span>`;
                            if(mallRegIdPsbCnt > 0){
                                popupHtml += `<p>판매몰 ID : `+ mallRegIdPsbCnt +` 개 등록</p>`;
                            }else{
                                popupHtml += `<p>판매몰 ID : 무제한 등록</p>`    ;
                            }
                        popupHtml +=`    
                        </div>
                        <div class="box">
                            <span class="check"></span>
                            <p>데이터 조회 : 일자별 상세 정산내역, 판매통계분석 및 그래프</p>
                        </div>
                        </div>
                        <div>
                            <div class="box">
                                <span class="check"></span>`;
                                if(acctRegPsbCnt > 0){
                                    popupHtml +=`<p>판매몰 계좌 : `+acctRegPsbCnt+` 개 등록</p>`;
                                }else{
                                    popupHtml +=`<p>판매몰 계좌 : 무제한 등록</p>`;
                                }
                            popupHtml +=`    
                            </div>
                            <div class="box">
                                <span class="check"></span>
                                <p>리포팅 서비스 : 매영업일 정산예정금 알림톡 리포트 발송</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h2>결제정보</h2>
            <div class="flexBox payInfo">
                <div class="left">
                    <h5>신용카드</h5>
                </div>
                <div class="right">
                    <p>`+cardNum+`</p>
                </div>
            </div>
            <button type="button" class="blue_outline" id="closeModal3">닫기</button>
            </div>
        </div>`;
        modalShow({
            id: 'modal_00',
            content: popupHtml,
            function: () => {
                document.querySelector('#cancelModal').addEventListener('click', () => {
                    document.querySelector('body').style.overflow = '';
                }); 

                document.querySelector('#closeModal3 ').addEventListener('click', () => {
                    document.querySelector('#modal_00').remove();
                    document.querySelector('body').style.overflow = '';
                }); 
            },
        });
    } // if end
})();

// 갤러리 슬라이더

function gallerySlider() {
    let i = 0;
    setInterval(() => {
        if (i == 0) {
            i = 1;
        } else {
            i = 0;
        }
        document.querySelector('#extraInfo .gallery ul').style.left = i * -100 + '%';
        document
            .querySelectorAll('#extraInfo ul.bottomUl li')
            .forEach((el) => el.classList.remove('active'));
        document.querySelectorAll('#extraInfo ul.bottomUl li')[i].classList.add('active');
    }, 5000);
}
gallerySlider();

// 20230323 추가
document.querySelector('#vidGuide').addEventListener('click', () => {
    let width = window.innerWidth > 800 ? 800 : window.innerWidth - 20;
    let height = width * 0.6;
    modalShow({
        id: 'youtube_00',
        content: `
        <iframe width="${width}" height="${height}" src="https://www.youtube.com/embed/OzaIAVrgQEo" title="[셀러봇캐시 등록방법] 정산계좌통합관리" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
        `,
    });
});
