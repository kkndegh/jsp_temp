import modalShow from '/assets/new_join/js/import/modalShow.js';

document.querySelector('#skipStep').addEventListener('click', () => {
    skipStepAcc();
});
document.querySelector('#nextStep').addEventListener('click', () => {
    if (document.querySelector('#nextStep').classList.contains('active')) {
        // 소드원 계정이 아닐 경우
        // notSodeOneAcc();
        // 소드원 계정일 경우
        // existSodeOneAcc();
        
        let domain;
        if (document.querySelector('#domain').value == 'manual') {
            domain = document.querySelector('#manualDomain').value;
        } else {
            domain = document.querySelector('#domain').value;
        }

        const emailAddress = document.querySelector('#email').value + '@' + domain;
        if(isEmail(emailAddress)){
            $.ajax({
                url: '/pub/member_only1/join_check',
                data: { password: $("#pw").val(), svc_cust_id: emailAddress},
                async: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    $('#req_cust_id').val(emailAddress);
                    $('#password').val($("#pw").val());

                    if (data.sodeone_member) {
                        existSodeOneAcc();
                    } else if (!data.sodeone_member) {
                        notSodeOneAcc();
                    }
                },
                error: function (response, status, error) {
                    var error = response.responseJSON.error;
                    if (response.status == 403) {
                        if (error.code == "AUTH-4302") {
                            noIdAcc();
                        } else if (error.code == "AUTH-4303") {
                            noIdAcc();
                        }
    
                    } else {
                        showAlert(error.comment);
                    }
                }
            }); // 
        }else{
            document.querySelector('#emailMsg').style.display ='block';
            document.querySelector('#emailMsg').innerHTML = '입력하신 이메일 <b style="color:red;">('+emailAddress+')</b> 을 다시 확인해 주세요.';
        }
    }
});

// 다음 버튼 활성화용 플래그
let flagObj = {
    email_1: true,
    email_2: true,
    pwFlag: true,
};
function flagChecker(flag, bool) {
    flagObj[flag] = bool;

    let notChecked = Object.keys(flagObj).find((key) => flagObj[key] === false);

    if (!notChecked) {
        document.querySelector('#nextStep').classList.add('active');
    } else {
        document.querySelector('#nextStep').classList.remove('active');
    }
}

document.querySelector('#email').addEventListener('keyup', () => {
    if (document.querySelector('#email').value == '') {
        flagChecker('email_1', false);
    } else {
        flagChecker('email_1', true);
    }
	emailEmptyChk();
});
document.querySelector('#domain').addEventListener('change', () => {
    if (document.querySelector('#domain').value == 'manual') {
        document.querySelector('#manualDomain').removeAttribute('disabled');
        if (document.querySelector('#manualDomain').value == '') {
            flagChecker('email_2', false);
        } else {
            flagChecker('email_2', true);
        }
    } else {
        document.querySelector('#manualDomain').value = "";
        document.querySelector('#manualDomain').setAttribute('disabled', true);
        flagChecker('email_2', true);
    }
	emailEmptyChk();
});
document.querySelector('#manualDomain').addEventListener('keyup', () => {
    if (document.querySelector('#manualDomain').value == '') {
        flagChecker('email_2', false);
    } else {
        flagChecker('email_2', true);
    }
	emailEmptyChk();
});
// 비밀번호 유효성 체크
document.querySelector('#pw').addEventListener('keyup', (e) => {
    let regexp = new RegExp(/^(?=.*?[a-zA-Z])(?=.*?\d)(?=.*?[!@#$%^&*+=\-[\]]).{8,20}$/);
    if (!regexp.test(e.target.value)) {
        document.querySelector('#passwdMsg').style.display ='block';
        flagChecker('pwFlag', false);
    } else {
        document.querySelector('#passwdMsg').style.display ='none';
        flagChecker('pwFlag', true);
    }
});

function notSodeOneAcc() {
    let popupHtml = `
    <div class="head">
        <h5></h5>
        <img src="/assets/images/header/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <p>해당 이메일 아이디는 소드원 계정이 아닙니다.</p>
        <p>소드원 계정이 없으시다면 건너뛰기를 눌러주세요.</p>
        <div class="btnFlex">
            <button class="blue_outline" id="modalCancel">확인</button>
            <button class="blue" id="modalConfirm">건너뛰기</button>
        </div>
    </div>
    `;
    modalShow({ 
        id: 'modal_01',
        content: popupHtml,
        function: () => {
            document.querySelector('#modalCancel').addEventListener('click', () => {
                document.querySelector('#modal_01').remove();
                document.querySelector('body').style.overflow = '';
            });
            document.querySelector('#modalConfirm').addEventListener('click', () => {
                $("#form").attr("action", "/pub/member_only1/step2");
                document.form.submit();
            });
        },
    });
}

function existSodeOneAcc() {
    let popupHtml = `
    <div class="head">
        <h5></h5>
        <img src="/assets/images/header/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <p>소드원 회원정보가 존재합니다.</p>
        <p>셀러봇캐시 계정으로 통합하시겠습니까?</p>
        <div class="btnFlex">
            <button class="blue_outline" id="modalCancel">취소</button>
            <button class="blue" id="modalConfirm">통합하기</button>
        </div>
    </div>
    `;
    modalShow({
        id: 'modal_02',
        content: popupHtml,
        function: () => {
            document.querySelector('#modalCancel').addEventListener('click', () => {
                document.querySelector('#modal_02').remove();
                document.querySelector('body').style.overflow = '';
            });
            document.querySelector('#modalConfirm').addEventListener('click', () => {
                $("#form").attr("action", "/pub/member_only1/join_cust_confirm");  
                document.form.submit();
            });
        },
    });
}

function skipStepAcc() {
    let popupHtml = `
    <div class="head">
        <h5></h5>
        <img src="/assets/images/header/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <p>소드원 회원정보가 확인되지않습니다.</p>
        <p>셀러봇캐시 통합회원 가입을 하시겠습니까?</p>
        <div class="btnFlex">
            <button class="blue_outline" id="modalCancel">취소</button>
            <button class="blue" id="modalConfirm">가입하기</button>
        </div>
    </div>
    `;
    modalShow({
        id: 'modal_03',
        content: popupHtml,
        function: () => {
            document.querySelector('#modalCancel').addEventListener('click', () => {
                document.querySelector('#modal_03').remove();
                document.querySelector('body').style.overflow = '';
            });
            document.querySelector('#modalConfirm').addEventListener('click', () => {
                $("#form").attr("action", "/pub/member_only1/step2");
                document.form.submit();
            });
        },
    });
}

function noIdAcc() {
    let popupHtml = `
    <div class="head">
        <h5></h5>
        <img src="/assets/images/header/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <p>회원정보가 존재하지 않습니다.</p>
        <div class="btnFlex">
            <button class="blue_outline" id="modalCancel">확인</button>
            <button class="blue" id="modalConfirm">건너뛰기</button>
        </div>
    </div>
    `;
    modalShow({
        id: 'modal_04',
        content: popupHtml,
        function: () => {
            document.querySelector('#modalCancel').addEventListener('click', () => {
                document.querySelector('#modal_04').remove();
                document.querySelector('body').style.overflow = '';
            });
            document.querySelector('#modalConfirm').addEventListener('click', () => {
                $("#form").attr("action", "/pub/member_only1/step2");
                document.form.submit();
            });
        },
    });
}

function emailEmptyChk(){
	if (document.querySelector('#domain').value == 'manual') {
		if(document.querySelector('#email').value == '' || document.querySelector('#manualDomain').value == ''){
			document.querySelector('#emailMsg').style.display = 'block';
			document.querySelector('#emailMsg').innerHTML ='이메일을 입력해주세요';
		}else{
			document.querySelector('#emailMsg').style.display = 'none';
		}
	} else {
		if(document.querySelector('#email').value == ''){
			document.querySelector('#emailMsg').style.display = 'block';
			document.querySelector('#emailMsg').innerHTML ='이메일을 입력해주세요';
		}else{
			document.querySelector('#emailMsg').style.display = 'none';
		}
	}
}