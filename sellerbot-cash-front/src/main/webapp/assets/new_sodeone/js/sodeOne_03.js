import modalShow from '/assets/new_join/js/import/modalShow.js';

document.querySelector('#nextStep').addEventListener('click', () => {
    if (document.querySelector('#nextStep').classList.contains('active')) {
        /** 사업자 정보입력 */
        $("#form1").attr("action", "/pub/member_only1/step3");
        document.form1.submit();
    }
});

// 다음 버튼 활성화용 플래그
let flagObj = {
    email_1: false,
    email_2: false,
    verifFlag: false,
};
function flagChecker(flag, bool) {
    flagObj[flag] = bool;

    let notChecked = Object.keys(flagObj).find((key) => flagObj[key] === false);

    if (!notChecked) {
        document.querySelector('#nextStep').classList.add('active');
    } else {
        document.querySelector('#nextStep').classList.remove('active');
    }
}

document.querySelector('#email').addEventListener('keyup', () => {
    if (document.querySelector('#email').value == '') {
        flagChecker('email_1', false);
    } else {
        flagChecker('email_1', true);
    }
    emailEmptyAndSendChk();
});

document.querySelector('#domain').addEventListener('change', () => {
    if (document.querySelector('#domain').value == 'manual') {
        document.querySelector('#manualDomain').removeAttribute('disabled');
        if (document.querySelector('#manualDomain').value == '') {
            flagChecker('email_2', false);
        } else {
            flagChecker('email_2', true);
        }
    } else {
        document.querySelector('#manualDomain').setAttribute('disabled', true);
        document.querySelector('#manualDomain').value='';
        flagChecker('email_2', true);
    }
    emailEmptyAndSendChk();
});
document.querySelector('#manualDomain').addEventListener('keyup', () => {
    if (document.querySelector('#manualDomain').value == '') {
        flagChecker('email_2', false);
    } else {
        flagChecker('email_2', true);
    }
    emailEmptyAndSendChk();
});

//인증번호 요청 클릭 시
document.querySelector('#reqVerificationCode').addEventListener('click', (e) => {
    if (!document.querySelector('#email').value) {
        document.querySelector('#email').focus();
        return false;
    } else if (document.querySelector('#domain').value == '') {
        document.querySelector('#domain').focus();
        return false;
    } else if (
        document.querySelector('#domain').value == 'manual' &&
        !document.querySelector('#manualDomain').value
    ) {
        document.querySelector('#manualDomain').focus();
        return false;
    } else {
        let domain;
        if (document.querySelector('#domain').value == 'manual') {
            domain = document.querySelector('#manualDomain').value;
        } else {
            domain = document.querySelector('#domain').value;
        }
        const emailAddress = document.querySelector('#email').value + '@' + domain;
        document.querySelector('#cust_id').value = emailAddress;
        // 메일로 인증코드 발송
        // ...
        var ajaxParam = { 
            cust_id : emailAddress, 
            svc_id : "sodeone" 
        };

        $.ajax({
            url: '/pub/member_only1/id_check',
            data: ajaxParam,
            async: false,
            type: 'POST',
            success: function (data) {
                document.querySelector('#auth_token').value = data;
                //document.querySelector('#auth_no').focus();

                if (document.querySelector('#reqVerificationCode').classList.contains('blue')) {
                    document.querySelector('#emailMsg').style.display ='block';
                    document.querySelector(
                        '#emailMsg'
                    ).innerHTML = `입력하신 이메일<b>(${emailAddress})</b>으로 인증번호가 재발송되었습니다.`;
                 
                } else {
                    document.querySelector('#emailMsg').style.display ='block';
                    document.querySelector(
                        '#emailMsg'
                    ).innerHTML = `입력하신 이메일<b>(${emailAddress})</b>으로 인증번호가 발송되었습니다.`;
                }

                document.querySelector('#sendEmailHidInfo').value = emailAddress;
                document.querySelector('#reqVerificationCode').innerHTML = '재발송';
                document.querySelector('#reqVerificationCode').classList.add('blue');
            },
            error: function (response, status, error) {
                if (response.status == 400) {
                    uniteAcc($.param(ajaxParam));
                } else if (isNull(document.querySelector('#svc_cust_id').value) && response.status == 409) {
                    // svc_cust_id not null and cust_id duplication
                    // 아이디가 중복되었습니다. (아이디 재입력, 셀러봇캐시 가입확인)
                    document.querySelector('#cust_id').value = emailAddress;
                    notUniteAcc();
                } else if (nonNull(document.querySelector('#svc_cust_id').value) && response.status == 409) {
                    // svc_cust_id not null and cust_id duplication
                    // 통합가입 대상
                    showAlert("해당 이메일(ID)는 소드원과 셀러봇캐시에<br>이미 가입이 되어있습니다.<br>셀러봇캐시 회원 통합을 진행합니다.", function () {
                        $("#form1").attr("action", "/pub/member_only1/cash_confirm");
                        document.form1.submit();
                    });
                } else {
                    showAlert("Error: " + response.status);
                }
            }
        });
    }
});

//인증번호 확인 클릭 시
document.querySelector('#confirmVerificationCode').addEventListener('click', (e) => {
    let codeVal = document.querySelector('#verificationCode').value;

    let domain;
    if (document.querySelector('#domain').value == 'manual') {
        domain = document.querySelector('#manualDomain').value;
    } else {
        domain = document.querySelector('#domain').value;
    }
    const emailAddress = document.querySelector('#email').value + '@' + domain;
    const sendEmailHidInfo = document.querySelector('#sendEmailHidInfo').value;
    
    if(emailAddress != sendEmailHidInfo){
        showAlert("인증번호를 다시 요청해 주세요.");
        return false;
    }
    
    // 인증번호가 입력되지 않았거나 일치하지 않을 경우
    if (!codeVal) {
        document.querySelector('#msg2').style.display = 'block';
    } else {
        $.ajax({
            url: '/pub/member_only1/confirm_cust',
            data: {
                cust_id: emailAddress,
                authNo: $("#verificationCode").val(),
                auth_token: $("#auth_token").val(),
                password: $("#password").val(),
                auth_type: $("#auth_type").val()
            },
            async: true,
            type: 'POST',
            success: function (data) {
                document.querySelector('#confirmVerificationCode').innerHTML = '인증완료';
                document.querySelector('#confirmVerificationCode').classList.add('blue');
                document.querySelector('#confirmVerificationCode').style.pointerEvents = 'none';
                document.querySelector('#verificationCode').setAttribute('disabled', true);
                document.querySelector('#email').setAttribute('disabled', true);
                document.querySelector('#domain').setAttribute('disabled', true);
                document.querySelector('#manualDomain').setAttribute('disabled', true);
                document.querySelector('#reqVerificationCode').setAttribute('disabled', true);
                document.querySelector('#msg2').style.display = 'none';
                document.querySelector('#cust_id').value=emailAddress;
                flagChecker('verifFlag', true);
            },
            error: function (response, status, error) {
                // 인증 번호 오류
                if (response.status == 400) {
                    showAlert("인증번호가 틀립니다.");
                    return false;
                } else if (response.status == 4490) {
                    // 인증 번호 발송을 요청 하지 않은 상태 
                    showAlert("인증번호 발송을 먼저 하세요.", function () {
                        $("#auth_no").focus();
                    });
                    return false;
                }
                showAlert("Error: " + response.status);
            }
        }); 
    }
});

// 숫자만 입력 가능 🔢
document.querySelectorAll('.onlyNumber').forEach((el) => {
    el.addEventListener('keyup', () => {
        el.value = el.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    });
});

function uniteAcc(param) {
    let popupHtml = `
    <div class="head">
        <h5></h5>
        <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <p>통합계정입니다.</p>
        <p>비밀번호 찾기를 진행하시겠습니까?</p>
        <div class="btnFlex">
            <button class="blue_outline" id="modalCancel">취소</button>
            <button class="blue" id="modalConfirm">확인</button>
        </div>
    </div>
    `;
    modalShow({
        id: 'modal_01',
        content: popupHtml,
        function: () => {
            document.querySelector('#modalCancel').addEventListener('click', () => {
                //비밀번호 찾기 프로세스
                document.querySelector('#modal_01').remove();
                document.querySelector('body').style.overflow = '';
            });
            document.querySelector('#modalConfirm').addEventListener('click', () => {
                location.href = "/pub/member/find_pw?" + param;
            });
        },
    });
}

function notUniteAcc() {
    let popupHtml = `
    <div class="head">
        <h5></h5>
        <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body">
        <p>해당 이메일아이디는 셀러봇캐시에 가입되어 있어요.</p>
        <p>통합 가입을 진행하시겠습니까?</p>
        <div class="btnFlex">
            <button class="blue_outline" id="modalCancel">취소</button>
            <button class="blue" id="modalConfirm">확인</button>
        </div>
    </div>
    `;
    modalShow({
        id: 'modal_02',
        content: popupHtml,
        function: () => {
            document.querySelector('#modalCancel').addEventListener('click', () => {
                document.querySelector('#modal_02').remove();
                document.querySelector('body').style.overflow = '';
            });
            document.querySelector('#modalConfirm').addEventListener('click', () => {
                var fm = document.form1;
                fm.action = "/pub/member_only1/cash_confirm";
                fm.submit();
            });
        },
    });
}
// validation
function emailEmptyAndSendChk(){

    let domain;
    if (document.querySelector('#domain').value == 'manual') {
        domain = document.querySelector('#manualDomain').value;
    } else {
        domain = document.querySelector('#domain').value;
    }

    const chkEmail = document.querySelector('#email').value + '@' + domain;
    const sendEmail = document.querySelector('#sendEmailHidInfo').value;

    if (sendEmail != "") {
        if (chkEmail != sendEmail) {
            document.querySelector('#emailMsg').style.display = 'block';
			document.querySelector('#emailMsg').innerHTML ='<b style="color:red;">변경하신 이메일로 인증번호 재발송이 필요합니다.</b>';
            document.querySelector('#reqVerificationCode').innerHTML = '인증번호 요청';
            document.querySelector('#reqVerificationCode').classList.remove('blue');
        }else{
            document.querySelector('#emailMsg').style.display ='block';
            document.querySelector('#reqVerificationCode').innerHTML = '재발송';
            document.querySelector(
                '#emailMsg'
            ).innerHTML = `입력하신 이메일<b>(${sendEmail})</b>으로 인증번호가 발송되었습니다.`;
            document.querySelector('#reqVerificationCode').classList.add('blue');
        }    
    }else{
        // 
        if (document.querySelector('#domain').value == 'manual') {
            if(document.querySelector('#email').value == '' || document.querySelector('#manualDomain').value == ''){
                document.querySelector('#emailMsg').style.display = 'block';
                document.querySelector('#emailMsg').innerHTML ='이메일을 입력해주세요';
            }else{
                document.querySelector('#emailMsg').style.display = 'none';
            }
        } else {
            if(document.querySelector('#email').value == ''){
                document.querySelector('#emailMsg').style.display = 'block';
                document.querySelector('#emailMsg').innerHTML ='이메일을 입력해주세요';
            }else{
                document.querySelector('#emailMsg').style.display = 'none';
            }
        }
    }

}