document.querySelector('#nextStep').addEventListener('click', () => {
    if (document.querySelector('#nextStep').classList.contains('active')) {
        step4();
    }
});

// 다음 버튼 활성화용 플래그
let flagObj = {
    // email_1: false,
    // email_2: false,
    pwFlag: false,
};
function flagChecker(flag, bool) {
    flagObj[flag] = bool;

    let notChecked = Object.keys(flagObj).find((key) => flagObj[key] === false);

    if (!notChecked) {
        document.querySelector('#nextStep').classList.add('active');
    } else {
        document.querySelector('#nextStep').classList.remove('active');
    }
}

// document.querySelector('#email').addEventListener('keyup', () => {
//     if (document.querySelector('#email').value == '') {
//         flagChecker('email_1', false);
//     } else {
//         flagChecker('email_1', true);
//     }
// });
// document.querySelector('#domain').addEventListener('change', () => {
//     if (document.querySelector('#domain').value == 'manual') {
//         document.querySelector('#manualDomain').removeAttribute('disabled');
//         if (document.querySelector('#manualDomain').value == '') {
//             flagChecker('email_2', false);
//         } else {
//             flagChecker('email_2', true);
//         }
//     } else {
//         document.querySelector('#manualDomain').setAttribute('disabled', true);
//         flagChecker('email_2', true);
//     }
// });
// document.querySelector('#manualDomain').addEventListener('keyup', () => {
//     if (document.querySelector('#manualDomain').value == '') {
//         flagChecker('email_2', false);
//     } else {
//         flagChecker('email_2', true);
//     }
// });
// 비밀번호 유효성 체크
document.querySelector('#pw').addEventListener('keyup', (e) => {
    let regexp = new RegExp(/^(?=.*?[a-zA-Z])(?=.*?\d)(?=.*?[!@#$%^&*+=\-[\]]).{8,20}$/);
    if (!regexp.test(e.target.value)) {
        flagChecker('pwFlag', false);
    } else {
        flagChecker('pwFlag', true);
    }
});

function step4() {

    if (!isVali()) { return false; }

    // let domain;
    // if (document.querySelector('#domain').value == 'manual') {
    //     domain = document.querySelector('#manualDomain').value;
    // } else {
    //     domain = document.querySelector('#domain').value;
    // }
    const emailAddress = document.querySelector('#cust_id').value;

    $.ajax({
        url: '/pub/member_only1/seller_check',
        data: { password: $("#pw").val(), cust_id: emailAddress },
        async: false,
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.result) {
                document.form.submit();
            } else {
                var errorCode = data.errorCode;
                if (errorCode == "AUTH-4303") {
                    showAlert("해당 사용자가 존재하지 않습니다.");
                } else if (errorCode == "AUTH-4304") {
                    showAlert("아이디나 비밀번호를 다시 확인해주세요.");
                } else {
                    showAlert("로그인 에러");
                }
            }
        },
        error: function (response, status, error) {
            showAlert("서버 에러.");
        }
    });
}
