import modalShow from '/assets/new_join/js/import/modalShow.js';

const bizName = document.querySelector('#biz_nm');
const bizNum = document.querySelector('#biz_no');
const userName = document.querySelector('#ceo_nm');
const userPhone_1 = document.querySelector('#ceo_no_svc');
const userPhone_2 = document.querySelector('#ceo_no');
const talkTime_1 = document.querySelector('#talkTime_1');
const managerName = document.querySelector('#chrg_nm');
const managerPhone_1 = document.querySelector('#charg_no_svc');
const managerPhone_2 = document.querySelector('#chrg_no');
const talkTime_2 = document.querySelector('#talkTime_2');
const funnel = document.querySelector('#inf_path_cd');

// 다음 버튼 활성화용 플래그
let flagObj = {
    bizNameFlag: false,
    bizNumFlag: false,
    userNameFlag: false,
    userPhone_2Flag: false,
    funnelFlag: false,
};
function flagChecker(flag, bool) {
    flagObj[flag] = bool;

    let notChecked = Object.keys(flagObj).find((key) => flagObj[key] === false);

    if (!notChecked) {
        document.querySelector('#nextStep').classList.add('active');
    } else {
        document.querySelector('#nextStep').classList.remove('active');
    }
}

bizName.addEventListener('keyup', () => {
    if (bizName.value) {
        document.querySelector('#msg1').style.display = 'none';
        flagChecker('bizNameFlag', true);
    } else {
        document.querySelector('#msg1').style.display = 'block';
        flagChecker('bizNameFlag', false);
    }
});

bizNum.addEventListener('keyup', () => {
    if (bizNum.value) {
        document.querySelector('#biz_noMsg').style.display = 'none';
        flagChecker('bizNumFlag', true);
    } else {
        document.querySelector('#biz_noMsg').innerHTML = '사업자번호를 입력해주세요';
        document.querySelector('#biz_noMsg').style.display = 'block';
        flagChecker('bizNumFlag', false);
    }
});

userName.addEventListener('keyup', () => {
    if (userName.value) {
        document.querySelector('#msg3').style.display = 'none';
        flagChecker('userNameFlag', true);
    } else {
        document.querySelector('#msg3').style.display = 'block';
        flagChecker('userNameFlag', false);
    }
});

userPhone_2.addEventListener('keyup', () => {
    if (userPhone_2.value) {
        document.querySelector('#msg4').style.display = 'none';
        flagChecker('userPhone_2Flag', true);
    } else {
        document.querySelector('#msg4').style.display = 'block';
        flagChecker('userPhone_2Flag', false);
    }
});

funnel.addEventListener('change', () => {
    if (funnel.value) {
        document.querySelector('#msg5').style.display = 'none';
        flagChecker('funnelFlag', true);
    } else {
        document.querySelector('#msg5').style.display = 'block';
        flagChecker('funnelFlag', false);
    }
});

document.querySelector('#nextStep').addEventListener('click', () => {
    if (!bizName.value) {
        bizName.focus();
        document.querySelector('#msg1').style.display = 'block';
        return false;
    }
    if (!bizNum.value) {
        bizNum.focus();
        document.querySelector('#msg2').style.display = 'block';
        return false;
    }
    if (document.querySelector('#bizNumCheck').dataset.verified !== 'true') {
        document.querySelector('#bizNum').focus();
        document.querySelector('#msg2').innerHTML = '사업자번호 중복확인을 해주세요.';
        document.querySelector('#msg2').style.display = 'block';
        return false;
    }
    if (!userName.value) {
        userName.focus();
        document.querySelector('#msg3').style.display = 'block';
        return false;
    }
    if (!userPhone_2.value) {
        userPhone_2.focus();
        document.querySelector('#msg4').style.display = 'block';
        return false;
    }
    if (!funnel.value) {
        funnel.focus();
        document.querySelector('#msg5').style.display = 'block';
        return false;
    }

    if (document.querySelector('#nextStep').classList.contains('active')) {
        showLoadingModal();
        var form = document.getElementById('form1');
        form.action ='/pub/member_only1/step_end';
        form.submit();
    }
});

// 비밀번호 유효성 체크
document.querySelector('#passwd').addEventListener('keyup', (e) => {
    let regexp = new RegExp(/^(?=.*?[a-zA-Z])(?=.*?\d)(?=.*?[!@#$%^&*+=\-[\]]).{8,20}$/);
    if (!regexp.test(e.target.value)) {
        document.querySelector('#msg3').style.display = 'block';
        flagChecker('pwFlag', false);
    } else {
        document.querySelector('#msg3').style.display = 'none';
        flagChecker('pwFlag', true);
    }
});
// 비밀번호 확인 체크
document.querySelector('#passwd_same').addEventListener('keyup', (e) => {
    if (e.target.value !== document.querySelector('#passwd').value) {
        document.querySelector('#msg4').style.display = 'block';
        flagChecker('pwCheckFlag', false);
    } else {
        document.querySelector('#msg4').style.display = 'none';
        flagChecker('pwCheckFlag', true);
    }
});

// 사업자번호 중복체크
document.querySelector('#bizNumCheck').addEventListener('click', () => {
    if (!bizNum.value) {
        // 미입력
        document.querySelector('#biz_noMsg').innerHTML = '사업자번호를 입력해주세요';
        document.querySelector('#biz_noMsg').style.display = 'block';
    } else if (bizNum.value.length > 10) {
        // 오입력 (10자리 미만)
        document.querySelector('#biz_noMsg').innerHTML = '사업자 번호가 정확하지 않습니다';
        document.querySelector('#biz_noMsg').style.display = 'block';
    }
    else {
        // 유효함
        if(validateBizNo(bizNum.value)) {
            $.post("/pub/member_only1/check_bizNo", { bizNo: bizNum.value }, function (data, status) {
                document.querySelector('#biz_noMsg').innerHTML = '사용 가능한 사업자번호입니다.';
                document.querySelector('#biz_noMsg').classList.add('blue');
                document.querySelector('#biz_noMsg').style.display = 'block';

                document.querySelector('#bizNumCheck').dataset.verified = 'true';
                document.querySelector('#bizNumCheck').style.pointerEvents = 'none';
                bizNum.setAttribute('readonly', true);
            }).fail(function (response) {
                if (response.status == 410) { // 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
                    location.reload();
                } else if (response.status == 409) { // 사업자 번호 확인 오류
                    if ("AUTH-4003" == response.responseText) {
                        document.querySelector('#biz_noMsg').innerHTML = '이미 등록된 사업자 번호 입니다.';
                        document.querySelector('#biz_noMsg').style.display = 'block';
                    } else if ("AUTH-4005" == response.responseText) {
                        document.querySelector('#biz_noMsg').innerHTML = '입력하신 사업자번호가 유효 하지 않습니다.';
                        document.querySelector('#biz_noMsg').style.display = 'block';
                    } else if ("AUTH-4307" == response.responseText) {
                        document.querySelector('#biz_noMsg').innerHTML = "탈퇴한 회원입니다. <a href='https://9p9j3.channel.io/lounge' target='_blank' style='color: blue'>챗봇으로 문의하기</a>";
                        document.querySelector('#biz_noMsg').style.display = 'block';
                    } else {
                        document.querySelector('#biz_noMsg').innerHTML = response.responseText;
                        document.querySelector('#biz_noMsg').style.display = 'block';
                    }
                } else {
                    alert("Error: " + response.status);
                }
            });
        }else{
            document.querySelector('#biz_noMsg').innerHTML = '사업자 번호가 정확하지 않습니다';
            document.querySelector('#biz_noMsg').style.display = 'block';
        }    
    }
});

document.querySelectorAll('#talkInfo').forEach((el) => {
    let talkInfoHtml = `
    <div class="wrapper">
    <div class="left">
        <img src="/assets/new_join/img/talkInfoModal.png" alt="talkExample">
    </div>
    <div class="right"> 
        <div class="row flexRight">
            <img src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot">
        </div>
        <div class="flexRow">
            <h2>셀러봇캐시 알림톡</h2>
            <span class="line"></span>
        </div>
        <div class="row">
            <p>정산예정금을 판매몰별, 배송상태별, 일자별로 꼼꼼히 계산해서 지정하신 시간에 리포트를 보내드립니다.</p>
        </div>
        <div class="grayBox">
            <h3>셀러봇캐시 알림톡 체험</h3>
            <div class="flexRow">
                <select id="talk_userPhone_1">
                    <option value="010" selected>010</option>
                </select>
                <input class="onlyNumber" type="text" id="talk_userPhone_2" maxLength = "8">
                <button type="button" id="submit_ph">발송</button>
                <span class="underMsg" id="underMsg">입력하신 연락처로 알림톡 SAMPLE 메세지가 발송됩니다.<br>
                    ( 샘플 발송 외 다른목적 사용 X )</span>
            </div>
        </div>
    </div>
</div>`;
    el.addEventListener('click', () => {
        modalShow({
            id: 'talkInfo',
            content: talkInfoHtml,
            function: () => {
                document.querySelector("#talk_userPhone_2").addEventListener('keyup',()=>{
                    document.querySelector("#underMsg").innerHTML = `입력하신 연락처로 알림톡 SAMPLE 메세지가 발송됩니다.<br>
                    ( 샘플 발송 외 다른목적 사용 X )`;
                    document.querySelector("#underMsg").style.color ="#bbb";
                });

                document.querySelectorAll('.onlyNumber').forEach((el) => {
                    el.addEventListener('keyup', () => {
                        el.value = el.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
                    });
                });
                document.querySelector("#submit_ph").addEventListener('click',() => {
                    var talk_userPhone_1 = $("#talk_userPhone_1").val();
                    var talk_userPhone_2 = $("#talk_userPhone_2").val();
                    let tgt_cust_no = talk_userPhone_1 + talk_userPhone_2;
                    
                    if (!isMobile(tgt_cust_no)) {
                        document.querySelector("#underMsg").innerHTML = "휴대폰 번호를 정확히 입력해주세요.";
                        document.querySelector("#underMsg").style.color ="red";
                        return false;
                    }

                    $.ajax({
                        url: "/pub/trial",
                        data: {
                            'tgt_cust_no': tgt_cust_no
                        },
                        type: 'POST',
                        success: function (data) {
                            if ("OK" == data) {
                                document.querySelector("#underMsg").innerHTML = "알림톡 체험 신청 되었습니다.";
                                document.querySelector("#underMsg").style.color ="blue";
                                return false;
                            } else {
                                document.querySelector("#underMsg").innerHTML ="알림톡 체험을 이미 이용한 번호입니다.";
                                document.querySelector("#underMsg").style.color ="red";
                                return false;
                            }
                        },
                        error: function (error) {
                            document.querySelector("#underMsg").innerHTML ="알림톡 체험 신청에 실패하였습니다.";
                            document.querySelector("#underMsg").style.color ="red";
                            return false;
                        }
                    });
                });
            },
        });
    });
});

// 숫자만 입력 가능 🔢
document.querySelectorAll('.onlyNumber').forEach((el) => {
    el.addEventListener('keyup', () => {
        el.value = el.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    });
});

function validateBizNo(bizID) {
    bizID = bizID + "";
    // bizID는 숫자만 10자리로 해서 문자열로 넘긴다. 
    var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
    var tmpBizID, i, chkSum = 0, c2, remander;
    bizID = bizID.replace(/-/gi, '');

    for (i = 0; i <= 7; i++) chkSum += checkID[i] * bizID.charAt(i);
    c2 = "0" + (checkID[8] * bizID.charAt(8));
    c2 = c2.substring(c2.length - 2, c2.length);
    chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
    remander = (10 - (chkSum % 10)) % 10;

    if (Math.floor(bizID.charAt(9)) == remander) return true; // OK! 
    return false;
}