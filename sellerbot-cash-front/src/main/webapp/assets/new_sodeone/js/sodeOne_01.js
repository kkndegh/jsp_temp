document.querySelector('#nextStep').addEventListener('click', () => {
    var checkEsseYn = termFlag();
    if(!checkEsseYn){
        var msg = '필수 약관에 동의해주세요';
        document.querySelector('#alertText').textContent = msg;
        document.querySelector('.popup_alarm').style.display = "block";

        $("#termsAgree").attr("tabindex", -1).focus();
        return false;s
    }

    if (document.querySelector('#nextStep').classList.contains('active')) {
        document.getElementById('form').submit();
    }
});

// 아코디언 🎵
let acc = document.querySelectorAll('.accordion');
acc.forEach((el) => {
    el.querySelector('.title span').addEventListener('click', () => {
        el.classList.toggle('active');

        /* Toggle between hiding and showing the active panel */
        let panel = el.querySelector('.panel');
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            if (panel.scrollHeight < 300) {
                panel.style.maxHeight = panel.scrollHeight + 'px';
            } else {
                panel.style.maxHeight = '300px';
            }
        }
    });
});

// 다음 버튼 활성화용 플래그
let flagObj = {
    terms1Flag: false,
    terms2Flag: false,
    terms6Flag: false,
    terms7Flag: false,
};
function flagChecker(flag, bool) {
    flagObj[flag] = bool;

    let notChecked = Object.keys(flagObj).find((key) => flagObj[key] === false);

    if (!notChecked) {
        document.querySelector('#nextStep').classList.add('active');
    } else {
        document.querySelector('#nextStep').classList.remove('active');
    }
}

// 약관 텍스트 클릭 시 체크박스 클릭됨
document.querySelectorAll('.text_checkBox').forEach((el) => {
    el.addEventListener('click', () => {
        el.parentNode.querySelector('input.chkBox').click();
    });
});


// 약관동의 라벨 클릭
document.querySelectorAll('.label').forEach((el) => {
    el.addEventListener('click',() =>{
        el.parentNode.querySelector('input[type="checkbox"]').click();
        sellerbotTermAllFlag();
        sodeOneTermAllFlag();
        termFlag();
    });
    
});

// 셀로브캐시 약관동의
const sellerbotTerm = document.querySelectorAll('input[name="sellerbotTerm"]');
// 셀로브캐시 마켓동의
const smtTerm = document.querySelectorAll('input[name="smtTerm"]');

// 소드원 약관 동의
const sodeOneTerm = document.querySelectorAll('input[name="sodeOneTerm"]');

// 셀로브캐시 전체동의
document.querySelector('#checkAll').addEventListener('change',() =>{
    var term_type_cd = "";
    if(document.querySelector('#checkAll').checked){
        sellerbotTerm.forEach((el) =>{
            term_type_cd = el.attributes['term_type_cd'].value;
            el.checked = true;
            if(term_type_cd == 'SMT'){
                smtTerm.forEach((el2)=>{
                    el2.checked = true;
                })
            }
        });
    }else{
        sellerbotTerm.forEach((el) =>{
            term_type_cd = el.attributes['term_type_cd'].value;
            el.checked = false;
            if(term_type_cd == 'SMT'){
                smtTerm.forEach((el2)=>{
                    el2.checked = false;
                })
            }
        });
    }
    termFlag();
});

//셀로브캐시 약관동의 클릭
sellerbotTerm.forEach((el) => {
    el.addEventListener('click',()=>{
        var term_type_cd = el.attributes['term_type_cd'].value;
        if(term_type_cd == 'SMT' ){
            if(el.checked){
                smtTerm.forEach((el2)=>{
                    el2.checked = true;
                })
            }else{
                smtTerm.forEach((el2)=>{
                    el2.checked = false;
                })
            }
        }
        sellerbotTermAllFlag();
        termFlag();
    });    
});


// 마켓동의 체크 
smtTerm.forEach((el) =>{
    el.addEventListener('click',()=>{
        let smtTermAll = false;
        smtTerm.forEach((el2) => {
            if(el2.checked){
                smtTermAll = true;
            }
        });

        if(smtTermAll){
            el.parentNode.parentNode.parentNode.querySelector('input[name="sellerbotTerm"]').checked = true;
        }else{
            el.parentNode.parentNode.parentNode.querySelector('input[name="sellerbotTerm"]').checked = false;
        }
        sellerbotTermAllFlag();
    });
})


// 소드원 약관 동의 전체동의
document.querySelector('#checkAll2').addEventListener('change', () => {
    if (document.querySelector('#checkAll2').checked) {
        sodeOneTerm.forEach((el) => {
            el.checked !== true ? el.click() : null;
        });
    } else {
        sodeOneTerm.forEach((el) => {
            el.checked !== false
                ? el.click()
                : null;
        });
    }
     termFlag();
});

// 소드원 약관 동의
sodeOneTerm.forEach((el)=>{
    el.addEventListener('click',() =>{
        sodeOneTermAllFlag();
        termFlag();
    })
});

var termFlag= function(){
    var returnFlag = true;
    document.querySelectorAll('input[type="checkbox"]').forEach((el)=>{
        var term_esse_yn = el.attributes['term_esse_yn'];
        if (nonNull(term_esse_yn)) {
            if(term_esse_yn.value == 'Y' && !el.checked){
                returnFlag = false;
            }
        }
    });

    if(returnFlag){
        document.querySelector('#nextStep').classList.add('active');
    }else{
        document.querySelector('#nextStep').classList.remove('active');
    }

    return returnFlag;
}

var sellerbotTermAllFlag = function(){
    let sellerbotTermAll = true;
    sellerbotTerm.forEach((el) =>{
        var term_type_cd = el.attributes['term_type_cd'].value;
        if(!el.checked){
            sellerbotTermAll = false;
        }
        if(term_type_cd == 'SMT' ){
            smtTerm.forEach((el2)=>{
                if(!el2.checked){
                    sellerbotTermAll = false;
                }
    
            })
                
        }
    })
    
    document.querySelector('#checkAll').checked = sellerbotTermAll;
  
}

var sodeOneTermAllFlag = function(){
    let sodeOneTermAll = true;
    sodeOneTerm.forEach((el) => {
        if(!el.checked){
            sodeOneTermAll = false;
        }
    });
  
    document.querySelector('#checkAll2').checked = sodeOneTermAll;
   
}
