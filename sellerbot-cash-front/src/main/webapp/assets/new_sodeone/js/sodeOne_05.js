import modalShow from '/assets/new_join/js/import/modalShow.js';

let price = document.getElementById('price').value;
let goodsName = document.getElementById('goodsName').value;
let typeCd = document.getElementById('typeCd').value;
let optNo = document.getElementById('optNo').value;
let goodsList = [];


let errMsg = document.getElementById('errMsg').value;
let successYN = document.getElementById('successYN').value;

if(successYN == 'N'){
    var msg = '';
    if(nonNull(errMsg)){
        msg = "결제 요청이 실패하였습니다.<br>다시 시도해주세요.<br>(" + errMsg + ")";
        document.querySelector('#alertText').textContent = msg;
        document.querySelector('.popup_alarm').style.display = "block";
    }else{
        msg = "결제 요청이 실패하였습니다";
        document.querySelector('#alertText').textContent = msg;
        document.querySelector('.popup_alarm').style.display = "block";
    }
}

var inicisDataSetting = function(price, goodsName, typeCd, optNo) {
    price = Number(parseInt(price));
    goodsList = [];
        goodsList.push({
            p: price + (price * 0.1),
            n: encodeURI(goodsName),
            t: typeCd,
            o: optNo
        });

    document.querySelector('#goodsList').value= JSON.stringify(goodsList);
    document.querySelector('#SendPayForm_id input[name=price]').value = price + (price * 0.1);
    document.querySelector('#SendPayForm_id input[name=goodname]').value = "셀러봇이용권(" + goodsName + ")";
        
};

inicisDataSetting(price, goodsName, typeCd, optNo);

// 아코디언 🎵
let acc = document.querySelectorAll('.accordion');
acc.forEach((el) => {
    el.querySelector('.title span').addEventListener('click', () => {
        el.classList.toggle('active');

        /* Toggle between hiding and showing the active panel */
        let panel = el.querySelector('.panel');
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            if (panel.scrollHeight < 200) {
                panel.style.maxHeight = panel.scrollHeight + 'px';
            } else {
                panel.style.maxHeight = '200px';
            }
        }
    });
});

// 약관 텍스트 클릭 시 체크박스 클릭됨
document.querySelectorAll('.text_checkBox').forEach((el) => {
    el.addEventListener('click', () => {
        el.parentNode.querySelector('input.chkBox').click();
    });
});

// 약관 동의 시 결제수단 활성화
document.querySelector('#terms_01').addEventListener('change', () => {
    if (document.querySelector('#terms_01').checked) {
        document.querySelector('#creditCard').classList.add('active');
        document.querySelector('.fixed').classList.add('active');
        
    } else {
        document.querySelector('#creditCard').classList.remove('active');
        document.querySelector('.fixed').classList.remove('active');
        
    }
});

document.querySelector('#nextStep').addEventListener('click',() => {
    if (document.querySelector('#terms_01').checked) {
        inicisModule();
        
    } else {
        document.getElementById('accordion').focus();
        
    }
});

//결제수단 등록 완료 시
document.querySelector('#creditCard').addEventListener('click', () => {
    inicisModule();
});


var inicisModule = function() {
    var price = $('#SendPayForm_id [name="price"]').val();
    var mid = $("#SendPayForm_id [name=mid]").val();

    $.ajax({
        url: '/pub/payment/pay_getSignature',
        data: { mid: mid, price: price },
        async: false,
        type: 'POST',
        dataType: 'json',
        success: function (r) {
            if (r.signature) {
                if ($("#DeviceType").val() == 'PC') {
                    $("#SendPayForm_id [name=oid]").val(r.oid);
                    $("#SendPayForm_id [name=timestamp]").val(r.timestamp);
                    $("#SendPayForm_id [name=signature]").val(r.signature);
                    
                    var data = new Object();
                    data.goodsList = JSON.parse($("#goodsList").val());
                    data.freeTrialYn = $("#freeTrialYn").val();
                    data.eventNo = $('#eventNo').val();
                    data.mode = 'register';
                    data.buyerEmail = $('#SendPayForm_id [name="buyeremail"]').val();

                    $("#SendPayForm_id [name=merchantData]").val(JSON.stringify(data));
                    INIStdPay.pay('SendPayForm_id');
                } else {
                    $("#SendPayForm_id [name=orderid]").val(r.oid);
                    $("#SendPayForm_id [name=timestamp]").val(r.timestamp);
                    $("#SendPayForm_id [name=hashdata]").val(r.hash);

                    var data = new Object();
                    data.goodsList = JSON.parse($("#goodsList").val());
                    data.freeTrialYn = $("#freeTrialYn").val();
                    data.eventNo = $('#eventNo').val();
                    data.mode = 'register';
                    data.buyerName = $('#SendPayForm_id [name="buyername"]').val();
                    data.buyerTel = $('#SendPayForm_id [name="buyertel"]').val();
                    data.buyerEmail = $('#SendPayForm_id [name="buyeremail"]').val();
                    
                    $("#SendPayForm_id [name=p_noti]").val(JSON.stringify(data));
                    $("#SendPayForm_id").submit();
               }
            }
        },
        error: function (response, status, error) {
            showAlert("서버 에러.");
        }
    });
};

// 유료 결제 약관
fetch('/assets/new_join/pay_term.txt')
    .then((res) => res.text())
    .then((data) => {
        document.querySelector('#termContent').innerHTML = data;
    });
