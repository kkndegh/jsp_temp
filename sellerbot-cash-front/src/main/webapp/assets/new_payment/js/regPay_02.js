import numberWithCommas from '/assets/new_join/js/import/numberWithCommas.js';

$(document).ready(function(){
    initTicketSelect();
});

let errMsg = document.getElementById('errMsg').value;
let successYN = document.getElementById('successYN').value;

if(successYN == 'N'){
    var msg = '';
    if(nonNull(errMsg)){
        showAlert("결제 요청이 실패하였습니다.<br>다시 시도해주세요.<br/>(" + errMsg + ")");
    }else{
        showAlert("결제 요청이 실패하였습니다.");
    }
}

document.querySelector('#nextStep').addEventListener('click', () => {
    if(!document.querySelector('#terms_01').checked){
        $("#terms").attr("tabindex", -1).focus();
        var msg = '셀로봇캐시 유료결제 약관에 동의해주세요.';
        document.querySelector('#alertText').textContent = msg;
        document.querySelector('.popup_alarm').style.display = "block";
    }else{
        inicisModule();
    }
});

let price = "";
let goodsName = "";
let typeCd = "";
let optNo = "";
let goodsList = [];
let goodsSeqNo ="";

//step1에서 선택한 이용권 선택
function initTicketSelect() {
    var selGoodsSeqNo = document.querySelector('#selGoodsSeqNo').value; 
    var selGoodsOptSeqNo = document.querySelector('#selGoodsOptSeqNo').value; 
    var cnt=0;

    let galleryItem = document.querySelectorAll('.ticket_gallery ul li');
 
    for(let i=0; i < galleryItem.length; i++){
        if(galleryItem[i].dataset.seq == selGoodsSeqNo && galleryItem[i].dataset.opt == selGoodsOptSeqNo){
          
            var index = galleryItem[i].dataset.idx;
            let prc = galleryItem[index].dataset.price;
            let fna_prc = galleryItem[index].dataset.fnaprc;

            if(index != 0){
                for(let j=0; j<index; j++){
                    document.querySelector('#nextTicket').click();
                }
            }else{
                document.querySelector('.ticket_gallery ul').style.left = 0 + '%';
            }

            if(fna_prc == '' || fna_prc == null || fna_prc == '0'){
                price =  prc;
            }else{
                price =  fna_prc;
            }
            document.querySelector('#ticketPrice').innerHTML = `월 ${numberWithCommas(price)}원`;
            document.querySelector('#totalPrice').innerHTML = numberWithCommas(price) + '원';
            document.querySelector('#ticketName').innerHTML = galleryItem[index].dataset.name;

            goodsName = galleryItem[index].dataset.name;
            typeCd = galleryItem[index].dataset.typ;
            optNo = galleryItem[index].dataset.opt;
            goodsSeqNo = galleryItem[index].dataset.seq;
            inicisDataSetting(price, goodsName, typeCd, optNo ,goodsSeqNo);

            cnt++;
        }
    }
}

// 이용권 좌 우 클릭
function ticketSelect() {
    let index = 0;
    let galleryItem = document.querySelectorAll('.ticket_gallery ul li');

    document.querySelector('#prevTicket').addEventListener('click', (e) => {
        index - 1 < 0 ? (index = galleryItem.length - 1) : index--;
        document.querySelector('.ticket_gallery ul').style.left = index * -100 + '%';

        let prc = galleryItem[index].dataset.price;
        let fna_prc = galleryItem[index].dataset.fnaprc;

        if(fna_prc == '' || fna_prc == null || fna_prc == '0'){
            price =  prc;
        }else{
            price =  fna_prc;
        }
        document.querySelector('#ticketPrice').innerHTML = `월 ${numberWithCommas(price)}원`;
        document.querySelector('#totalPrice').innerHTML = numberWithCommas(price) + '원';
        document.querySelector('#ticketName').innerHTML = galleryItem[index].dataset.name;
        
        goodsName = galleryItem[index].dataset.name;
        typeCd = galleryItem[index].dataset.typ;
        optNo = galleryItem[index].dataset.opt;
        goodsSeqNo = galleryItem[index].dataset.seq;
        inicisDataSetting(price, goodsName, typeCd, optNo ,goodsSeqNo);
    });

    document.querySelector('#nextTicket').addEventListener('click', () => { 
        index + 1 > galleryItem.length - 1 ? (index = 0) : index++;
        document.querySelector('.ticket_gallery ul').style.left = index * -100 + '%';

        let prc = galleryItem[index].dataset.price;
        let fna_prc = galleryItem[index].dataset.fnaprc;

        if(fna_prc == '' || fna_prc == null || fna_prc == '0'){
            price =  prc;
        }else{
            price =  fna_prc;
        }
        document.querySelector('#ticketPrice').innerHTML = `월 ${numberWithCommas(price)}원`;
        document.querySelector('#totalPrice').innerHTML = numberWithCommas(price) + '원';
        document.querySelector('#ticketName').innerHTML = galleryItem[index].dataset.name;
        
        goodsName = galleryItem[index].dataset.name;
        typeCd = galleryItem[index].dataset.typ;
        optNo = galleryItem[index].dataset.opt;
        goodsSeqNo = galleryItem[index].dataset.seq;
        inicisDataSetting(price, goodsName, typeCd, optNo ,goodsSeqNo);
    });
}
ticketSelect();

// 갤러리 좌 우 클릭
function slideGallery() {
    let index = 0;
    let galleryItem = document.querySelectorAll('.gallery ul li');
    document.querySelector('#galleryLeft').addEventListener('click', () => {
        index - 1 < 0 ? (index = galleryItem.length - 1) : index--;
        document.querySelector('.gallery ul').style.left = index * -100 + '%';
        document.querySelectorAll('ul.bottomUl li').forEach((el) => el.classList.remove('active'));
        document.querySelectorAll('ul.bottomUl li')[index].classList.add('active');
    });
    document.querySelector('#galleryRight').addEventListener('click', () => {
        index + 1 > galleryItem.length - 1 ? (index = 0) : index++;
        document.querySelector('.gallery ul').style.left = index * -100 + '%';
        document.querySelectorAll('ul.bottomUl li').forEach((el) => el.classList.remove('active'));
        document.querySelectorAll('ul.bottomUl li')[index].classList.add('active');
    });
}
slideGallery();

// 아코디언 🎵
let acc = document.querySelectorAll('.accordion');
acc.forEach((el) => {
    el.querySelector('.title span').addEventListener('click', () => {
        el.classList.toggle('active');

        /* Toggle between hiding and showing the active panel */
        let panel = el.querySelector('.panel');
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            if (panel.scrollHeight < 200) {
                panel.style.maxHeight = panel.scrollHeight + 'px';
            } else {
                panel.style.maxHeight = '200px';
            }
        }
    });
});

// 약관 텍스트 클릭 시 체크박스 클릭됨
document.querySelectorAll('.text_checkBox').forEach((el) => {
    el.addEventListener('click', () => {
        el.parentNode.querySelector('input.chkBox').click();
    });
});

// 약관 동의 시 결제수단 활성화
document.querySelector('#terms_01').addEventListener('change', () => {
    if (document.querySelector('#terms_01').checked) {
        document.querySelector('#creditCard').classList.add('active');
        document.querySelector('.fixed').classList.add('active');
    } else {
        document.querySelector('#creditCard').classList.remove('active');
        document.querySelector('.fixed').classList.remove('active');
    }
});

//결제수단 등록 완료 시
document.querySelector('#creditCard').addEventListener('click', () => {
    inicisModule();
});

var inicisModule = function() {
    var price = $('#SendPayForm_id [name="price"]').val();
    var mid = $("#SendPayForm_id [name=mid]").val();

    $.ajax({
        url: '/pub/payment/pay_getSignature',
        data: { mid: mid, price: price },
        async: false,
        type: 'POST',
        dataType: 'json',
        success: function (r) {
            if (r.signature) {
                if ($("#DeviceType").val() == 'PC') {
                    $("#SendPayForm_id [name=oid]").val(r.oid);
                    $("#SendPayForm_id [name=timestamp]").val(r.timestamp);
                    $("#SendPayForm_id [name=signature]").val(r.signature);
    
                    var data = new Object();
                    data.goodsList = JSON.parse($("#goodsList").val());
                    data.freeTrialYn = $("#freeTrialYn").val();
                    data.eventNo = $('#eventNo').val();
                    data.mode = 'register';
                    $("#SendPayForm_id [name=merchantData]").val(JSON.stringify(data));
                    INIStdPay.pay('SendPayForm_id');
                } else {
                    $("#SendPayForm_id [name=orderid]").val(r.oid);
                    $("#SendPayForm_id [name=timestamp]").val(r.timestamp);
                    $("#SendPayForm_id [name=hashdata]").val(r.hash);

                    var data = new Object();
                    data.goodsList = JSON.parse($("#goodsList").val());
                    data.freeTrialYn = $("#freeTrialYn").val();
                    data.eventNo = $('#eventNo').val();
                    data.mode = 'register';
                    data.buyerName = $('#SendPayForm_id [name="buyername"]').val();
                    data.buyerTel = $('#SendPayForm_id [name="buyertel"]').val();
                    data.buyerEmail = $('#SendPayForm_id [name="buyeremail"]').val();
                    
                    $("#SendPayForm_id [name=p_noti]").val(JSON.stringify(data));
                    $("#SendPayForm_id").submit();
               }
            }
        },
        error: function (response, status, error) {
            showAlert("서버 에러.");
        }
    });
        
};

// 유료 결제 약관
fetch('/assets/new_join/pay_term.txt')
    .then((res) => res.text())
    .then((data) => {
        document.querySelector('#termContent').innerHTML = data;
    });

var inicisDataSetting = function(price, goodsName, typeCd, optNo,goodsSeqNo) {
    price = Number(parseInt(price));
    goodsList = [];
        goodsList.push({
            p: price + (price * 0.1),
            n: encodeURI(goodsName),
            t: typeCd,
            o: optNo,
            s: goodsSeqNo
        });

    document.querySelector('#goodsList').value= JSON.stringify(goodsList);
    document.querySelector('#SendPayForm_id input[name=price]').value = price + (price * 0.1);
    document.querySelector('#SendPayForm_id input[name=goodname]').value = "셀러봇이용권(" + goodsName + ")";
        
};