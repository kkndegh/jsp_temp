
    //금융 상품 판매몰 관리 리스트 대상여부 체크박스 선택 카운트(대상여부 이미 체크되어있던 것 제외)
    var checkCnt = 0;
    
    $(document).ready(function(){

        $(".loan_save").on("click", function () {
            var confirmText = "판매몰을 등록하시면 해지는 금융기관에만 처리가 가능하므로 금융기관으로 문의바랍니다.";

            if(checkCnt > 0){
                var modalId = showConfirm(confirmText, function () {
                    var paramArray = new Array();
                    
                    $("input:checkbox[name=loan_chk]:not(:disabled):checked").each(function() {
                        var pushData  = {
                            cust_seq_no : $(this).data("cust-seq-no"),
                            cust_mall_seq_no : $(this).data("cust-mall-seq-no"),
                            mall_cd : $(this).data("mall-cd"),
                            bat_prcs_tgt_seq_no : $(this).data("bat-prcs-tgt-seq-no"),
                            loan_svc_id : $('.pop_sctbox').val()
                        }; 

                        paramArray.push(pushData);
                    });

                    $.ajax({
                        url: '/sub/loan/getLoanInfo'
                        , type: 'post'
                        , async: true
                        , dataType: 'json'
                        , contentType: 'application/json'
                        , data: JSON.stringify(paramArray)
                        , success: function (response) {
                            $(".popup_account.loan").removeClass("active");
                            if (response.result == "OK") {
                                removeModal(modalId);
                                showAlert("저장 되었습니다.", function () {
                                    location.href = "/sub/my/join/market";
                                });
                            } else {
                                removeModal(modalId);
                                showAlert(errorMessage);
                            }
                        }
                        , error: function (res) {
                            removeModal(modalId);
                            showAlert("처리 중 에러가 발생하였습니다.<br>관리자에게 문의 하세요.");
                        }
                    });
                });
            }else{
                showAlert("선택 여부를 선택하세요.");
            }
        });

        //금융상품 콤보박스
        $('.pop_sctbox').change(function(){
            //콤보박스 변경 시 대상여부 카운트 초기화
            checkCnt = 0;

            var val = this.value;

            var modalhtmlHeadStr = "금융 상품 판매몰 관리";
            var modalhtmlbodyStr = $("option:selected", this).text() + ' 대출을 이용하는 고객은 대출 대상의 판매몰을 선택할 수 있습니다.';
            if (val == 'kcbkbbank') {
                modalhtmlbodyStr = modalhtmlbodyStr + '<br><br>단, 이미 대출 중이신 경우 선택 해제는 불가능합니다.<br>대출 이용종료 후 KB국민은행 담당자분께 해당 판매몰 해지처리를 요청하신 후 셀러봇캐시에서 판매몰 선택 해제가 가능합니다.';
            }

            $(".popup_account.loan .pop_head > p").html(modalhtmlHeadStr);
            $(".popup_account.loan .pop_body .pd20").html(modalhtmlbodyStr);

            $.ajax({
                url: '/sub/loan/getLoanCustMallList'
                , type: 'post'
                , async: true
                , dataType: 'json'
                , contentType: 'application/json'
                , data: val
                , success: function (response) {
                    var tag = "";
                    var batPrcsTgtSeqNo = "";

                    if(response.batchProTgt_list != null){
                        batPrcsTgtSeqNo = response.batchProTgt_list[0].bat_prcs_tgt_seq_no;
                    }else{
                        batPrcsTgtSeqNo = response.loanCustMall_list[0].bat_prcs_tgt_seq_no;
                    }

                    response.loanCustMall_list.forEach(loanInfo => {
                        tag += "<tr>";
                        tag += "<td class=\"text-center\"><input type=\"checkbox\" name=\"loan_chk\" class=\"loan_cust_mall\"";
                        tag += "data-cust-seq-no=\"" + loanInfo.cust_seq_no + "\"";
                        tag += "data-cust-mall-seq-no=\"" + loanInfo.cust_mall_seq_no + "\"";
                        tag += "data-mall-cd=\"" + loanInfo.mall_cd + "\"";
                        tag += "data-bat-prcs-tgt-seq-no=\"" + batPrcsTgtSeqNo + "\"";
                        tag += "data-loan-sts-cd=\"" + loanInfo.loan_sts_cd + "\"";
                        tag += "data-loan-svc-descp=\"" + loanInfo.svc_descp + "\"";
                        if(loanInfo.loan_sts_cd == '01'){
                            tag += "checked disabled";
                        }
                        tag += "></td>";
                        tag += "<th>" + loanInfo.other_loan_use + "</th>";
                        
                        tag += "<th>" + loanInfo.mall_nm + "</th>";
                        tag += "<td class=\"text-center\">" + loanInfo.mall_cert_1st_id + "</td>";

                        tag += "<td class=\"text-center\">";
                        tag += "<div>";
                        tag += "<ul>";
                        
                        if(loanInfo.cust_mall_sts_cd == 'NR'){
                            tag += "<li class=\"mall_sts_cd_store_con bullet_blu\"></li>";
                            tag += "<li>정상</li>";
                        }else if(loanInfo.cust_mall_sts_cd == 'INS'){
                            tag += "<li class=\"mall_sts_cd_store_con bullet_gry\"></li>";
                            tag += "<li>점검</li>";
                        }else if(loanInfo.cust_mall_sts_cd == 'ERR'){
                            tag += "<li class=\"mall_sts_cd_store_con bullet_red\"></li>";
                            tag += "<li>인증불가</li>";
                        }
                        tag += "</ul>";
                        tag += "</div>";
                        tag += "</td>";

                        var format_reg_ts = '';
                        if (loanInfo.loan_reg_ts != null) {
                            var reg_ts = new Date(loanInfo.loan_reg_ts);
                            var year = reg_ts.getFullYear();
                            var month = reg_ts.getMonth()+1;
                            var day = reg_ts.getDate();
                            format_reg_ts = year+"-"+(("00"+month.toString()).slice(-2))+"-"+(("00"+day.toString()).slice(-2));
                        }
                        
                        tag += "<td class=\"text-center\">" + format_reg_ts + "</td>";
                        tag += "</tr>";

                        $(".table_style01 > table > tbody").empty().append(tag);
                    });
                }
                , error: function (res) {
                    showAlert("처리 중 에러가 발생하였습니다.<br>관리자에게 문의 하세요.");
                }
            });
        });
        
        if($(".pop_sctbox option").length > 0){
            $(".pop_sctbox").find("option:eq(0)").prop("selected", true).trigger('change');
        }

        $(".kbBtn").on("click", function () {
            $(".popup_account.loan").addClass("active");
        });
    
        $(".popup_loan_close, .loan_close").on("click", function () {
            $(".pop_sctbox").find("option:eq(0)").prop("selected", true).trigger('change');
            $(".popup_account.loan").removeClass("active");
        });
    });

    

    //금융 상품 판매몰 관리 리스트 대상여부 체크박스 선택/해제 이벤트
    $(document).on('change','.loan_cust_mall',function(){
        if($(this).prop("checked")){
            checkCnt++;
        }else{
            checkCnt--;
        }
    });
