<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<jsp:include page="head_inc.jsp"></jsp:include>
	</head>
	<body>
		
		<!-- Google Tag Manager (noscript) 2023.11.01 추가 -->
		<noscript>
			<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TKN4F5L9" height="0" width="0" style="display:none;visibility:hidden"></iframe>
		</noscript>	
		<!-- End Google Tag Manager (noscript) -->

		<div class="wra">
			<tiles:insertAttribute name="header" />
			<tiles:insertAttribute name="body" />
			<tiles:insertAttribute name="footer" />
		</div>
	</body>
</html>
