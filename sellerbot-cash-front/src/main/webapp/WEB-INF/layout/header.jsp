<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:useBean id="server_toDay" class="java.util.Date" />

<head>
    <link rel="stylesheet" href="/assets/css/header.css">
    <script type="module" src="/assets/js/header.js" defer></script>
</head>

<header>
    <div class="top">
        <div class="notification">
            <div class="text">
                <i class="fa-solid fa-bell"></i>
                <div class="notiWrap">
                    <ul>
                        <c:forEach var="noti" items="${sessionScope.notiListData }">
                            <li onclick="window.location.href = '/pub/cs/notice?noti_seq_no=${noti.noti_seq_no}'">[공지사항] ${noti.title }</li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
            <button id="closeHeaderNoti"><img src="/assets/images/header/xmark.svg" alt="xmark"></button>
        </div>
    </div>
    <div class="bottom">
        <div class="logo" onclick="window.location.href='/'"><img src="/assets/images/header/sellerbotLogo.svg" alt="sellerbot"></div>
        <!--로그인 전-->
        <security:authorize access="isAnonymous()">
            <div class="right">
                <button type="button" onclick="window.location.href='/login'">로그인</button>
                <span></span>
                <button type="button" onclick="window.location.href='/pub/member/step1'">회원가입</button>
                <span></span>
                <button type="button" onclick="window.location.href='/pub/cs/faq'">고객센터</button>
            </div>
        </security:authorize>
        <!--로그인 후-->
        <security:authorize access="isAuthenticated()">
            <c:choose>
                <c:when test="${sessionScope.cust.reg_mall_cnt_al != 0 && sessionScope.cust.cust_uses_goods_total_count != 0}">
                     <!-- WEB-->
                    <div class="right logined">
                        <button type="button" onclick="window.location.href='/sub/my/confirm_pw'" class="userId"><c:out value="${fn:replace(sessionScope.cust.biz_nm, '주식회사', '')}" /> 님</button>
                        <span></span>
                        <button type="button" onclick="window.location.href='/logout'">로그아웃</button>
                        <span></span>
                        <button type="button" onclick="window.location.href='/pub/cs/faq'">고객센터</button>
                    </div>
                    <!-- MOBILE-->
                    <div class="m_menu_wra">
                        <div class="m_menu_box">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="m_sideMenu_wra">
                        <div class="m_sideMenu">
                            <div class="m_sideMenu_header">
                                <div class="link_my"><a href="/sub/my/confirm_pw" class="m_my"><img src="/assets/images/member/my_icon.png" alt=""> My</a></div>
                            </div>
                            <div class="m_sideMenu_contents">
                                <!-- 모바일용 메뉴 -->
                                <ul class="gnb_side">
                                    <li></li>
                                    <li></li>
                                    <li></li>

                                    <c:forEach var="menuInfo" items="${sessionScope.menu}">
                                        <li class="menu">
                                            <!-- 정산예정금 통합관리 -->
                                            <c:if test="${menuInfo.MENU_SEQ_NO eq 1}">
                                                <p class="depth1">${menuInfo.MENU_NM}</p>
                                                <ul class="sub_gnb_side" style="display: none;">
                                                    <li class="sub_menu sub2depth focus">
                                                        <a href="/sub/settAcc/all">정산예정금 확인</a>
                                                        <ul class="sub_sub">
                                                            <c:forEach var="i" begin="0" end="2">
                                                                <c:if test="${i eq 0}">
                                                                    <li class="sub_sub_menu focus"><a href="${menuInfo.SUB_MENU_LIST[i].SUB_MENU_PATH}">- ${menuInfo.SUB_MENU_LIST[i].SUB_MENU_NM}</a></li>
                                                                </c:if>
                                                                <c:if test="${i ne 0}">
                                                                    <li class="sub_sub_menu"><a href="${menuInfo.SUB_MENU_LIST[i].SUB_MENU_PATH}">- ${menuInfo.SUB_MENU_LIST[i].SUB_MENU_NM}</a></li>    
                                                                </c:if>
                                                            </c:forEach>
                                                        </ul>
                                                    </li>
                                                    <c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}" varStatus="subStatus">
                                                        <c:if test="${subStatus.index > 2}">
                                                            <li class="sub_menu"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
                                                        </c:if>
                                                    </c:forEach>
                                                </ul>
                                            </c:if>
                                            <c:if test="${menuInfo.MENU_SEQ_NO ne 1}">
                                                <!-- 서브 메뉴 노출 -->
                                                <c:if test="${menuInfo.SHOW_SUB_MENU_YN eq 'Y'}">
                                                    <p class="depth1">${menuInfo.MENU_NM}</p>
                                                    <ul class="sub_gnb_side" style="display: none;">
                                                        <c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">
                                                            <li class="sub_menu"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>    
                                                        </c:forEach>
                                                    </ul>
                                                </c:if>
                                                <!-- 서브 메뉴 비노출 -->
                                                <c:if test="${menuInfo.SHOW_SUB_MENU_YN ne 'Y'}">
                                                    <p onclick="location.href='${menuInfo.MENU_PATH}'" class="depth1 depth1_displaynone">
                                                        <a>${menuInfo.MENU_NM}</a>
                                                    </p>
                                                </c:if>
                                            </c:if>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                            <div class="m_sideMenu_footer">
                                <div class="sideMenu_footer_left">
                                    <a href="/logout">로그아웃</a>
                                </div>
                                <div class="sideMenu_footer_right">
                                    <a href="/pub/cs/notice">공지사항</a>
                                    <a href="/pub/cs/event">이벤트</a>
                                    <a href="/pub/cs/faq">고객센터</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="right">
                        <button type="button" onclick="window.location.href='/sub/my/confirm_pw'" class="userId"><c:out value="${fn:replace(sessionScope.cust.biz_nm, '주식회사', '')}" /> 님</button>
                        <span></span>
                        <button type="button" onclick="window.location.href='/logout'">로그아웃</button>
                        <span></span>
                        <button type="button" onclick="window.location.href='/pub/cs/faq'">고객센터</button>
                    </div>
                </c:otherwise>
            </c:choose>
        </security:authorize>
    </div>
    <c:set var="common_server_time">
        <fmt:formatDate value="${server_toDay}" pattern="yyyy-MM-dd HH:mm:ss" />
    </c:set>
    <input type="hidden" id="common_server_time" value="${common_server_time}">
</header>
