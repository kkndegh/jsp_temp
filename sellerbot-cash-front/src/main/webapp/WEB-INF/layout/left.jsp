<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:eval expression="@environment.getProperty('internal.redirect.base')" var="redirectBase"/>

<div class="side_menu_area">
    <span class="icon"><img src="/assets/images/notice/sideMenu.png" alt=""></span>
    <div class="side_menu_section">
    <p class="title_menu">셀러봇캐시 통합관리
    <img src="/assets/images/member/close.png" alt=""></p>
        <!-- PC용 메뉴 -->
        <ul class="gnb_side">
            <c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
                <c:choose>
                    <c:when test="${status.last}">
                        <li class="menu last">
                    </c:when>
                    <c:otherwise>
                        <li class="menu">
                    </c:otherwise>
                </c:choose>
                    <!-- 정산예정금 통합관리 -->
                    <c:if test="${menuInfo.MENU_SEQ_NO eq 1}">
                        <p class="depth1">${menuInfo.MENU_NM}</p>
                        <ul class="sub_gnb_side">
                            <li class="sub_menu sub2depth focus">
                                <a href="/sub/settAcc/all">정산예정금 확인</a>
                                <ul class="sub_sub">
                                    <c:forEach var="i" begin="0" end="2">
                                        <li class="sub_sub_menu"><a href="${menuInfo.SUB_MENU_LIST[i].SUB_MENU_PATH}">- ${menuInfo.SUB_MENU_LIST[i].SUB_MENU_NM}</a></li>    
                                    </c:forEach>
                                </ul>
                            </li>
                            <c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}" varStatus="subStatus">
                                <c:if test="${subStatus.index > 2}">
                                    <li class="sub_menu"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </c:if>
                    <c:if test="${menuInfo.MENU_SEQ_NO ne 1}">
                        <!-- 서브 메뉴 노출 -->
                        <c:if test="${menuInfo.SHOW_SUB_MENU_YN eq 'Y'}">
                            <p class="depth1">${menuInfo.MENU_NM}</p>
                            <ul class="sub_gnb_side">
                                <c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">
                                    <li class="sub_menu">
                                        <c:choose>
                                            <c:when test="${subMenuInfo.SUB_MENU_PATH eq '/pub/cs/serviceIntro'}">
                                                <c:choose>
                                                    <c:when test="${fn:indexOf(redirectBase,'www.sellerbot.co.kr') != -1 }">
                                                        <a href="https://www.sellerbot.co.kr/static/serviceIntro/index.html" onclick="fnServiceIntroPageMove(this.href); return false;">${subMenuInfo.SUB_MENU_NM}</a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <a href="http://192.168.111.250/html/serviceIntro/index.html" onclick="fnServiceIntroPageMove(this.href); return false;">${subMenuInfo.SUB_MENU_NM}</a>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a>
                                            </c:otherwise>
                                        </c:choose>
                                    </li>
                                </c:forEach>
                            </ul>
                        </c:if>
                        <!-- 서브 메뉴 비노출 -->
                        <c:if test="${menuInfo.SHOW_SUB_MENU_YN ne 'Y'}">
                            <p class="depth_one" onclick="location.href='${menuInfo.MENU_PATH}'"><a>${menuInfo.MENU_NM}</a></p>
                        </c:if>
                    </c:if>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>