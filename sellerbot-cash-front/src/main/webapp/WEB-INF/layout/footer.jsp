<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<footer>
    <div class="footerWrap">
        <div class="left">
            <div class="row">
                <p class="footerTerms" id="footer_term_1">이용약관</p>
                <span>|</span>
                <p class="footerTerms" id="footer_term_2"><b>개인정보처리방침</b></p>
                <span>|</span>
                <p class="footerTerms" id="footer_term_3">이메일무단수집거부</p>
            </div>
            <div class="row flexWrap">
                <p>(주) 온리원</p>
                <span>|</span>
                <p>대표이사 최호식</p>
                <span>|</span>
                <p>사업자등록번호 220-88-21645</p>
                <span>|</span>
                <p>특허번호 10-1089183</p>
            </div>
            <div class="row flexWrap">
                <p>사업자정보 소프트웨어개발</p>
                <span>|</span>
                <p>소재지 경기도 하남시 미사대로 520 현대지식산업센터 한강미사2차 D동 5층 545호</p>
            </div>
        </div>
        <div class="right">
            <img src="/assets/images/header/sellerbotLogo.svg" alt="sellerbot" onclick="window.location.href='/'">
        </div>
    </div>
</footer>
<c:if test="${pageContext.request.serverName eq 'www.sellerbot.co.kr'}">
    <!-- [네이버 로그 설치] 공통 적용 스크립트 -->
    <script type="text/javascript">
        if (!wcs_add) var wcs_add = {};
        wcs_add["wa"] = "s_33132b2535c4";
        if (!_nasa) var _nasa = {};
        wcs.inflow();
        wcs_do(_nasa);
    </script>
</c:if>