<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<meta name="robots" content="index,follow">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="naver-site-verification" content="324e62a14d4c3a13a7f875040390999cc936dc83" />
<meta property="og:title" content="셀러봇캐시" />
<meta property="og:url" content="https://www.sellerbot.co.kr" />
<meta property="og:image" content="https://www.sellerbot.co.kr/attachments/sellerbot_logo_meta_image.png" />
<meta property="og:description" content="정산예정금 통합관리서비스! 셀러봇캐시" />
<link rel="canonical" href="URL입력">
<title>Sellerbot</title>

<!--font-->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/moonspam/NanumSquare@1.0/nanumsquare.css">

<!--swiper-->
<link rel="stylesheet" href="/assets/js/swiper-master/dist/css/swiper.min.css">

<!--fullcalendar-->
<link rel="stylesheet" href="/assets/js/fullcalendar/daygrid/main.css">
<link rel="stylesheet" href="/assets/js/fullcalendar/core/main.css">
<link rel="stylesheet" href="/assets/js/monthpicker/monthPicker.css">

<!--style-->
<link rel="stylesheet" href="/assets/css/jquery-ui.css">
<link rel="stylesheet" href="/assets/css/select2.min.css">
<!-- <link rel="stylesheet" href="/assets/css/style2.css"> -->
<link rel="stylesheet" href="/assets/css/footerPopup.css?ver=20230321_01">

<!--owl_slider-->
<link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/assets/css/owl.carousel.min.css">

<link rel="stylesheet" href="/assets/css/tui-date-picker.css">

<!-- 개발공통css -->
<link rel="stylesheet" href="/assets/css/developer.css?ver=20200102_01">
<link rel="stylesheet" href="/assets/css/menu_mo.css">

<!--js-->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/jquery-ui.js"></script>
<script src="/assets/js/moment.min.js"></script>
<script src="/assets/js/moment-with-locales.js"></script>
<script src="/assets/js/script.js?ver=20200102_01"></script>
<script src="/assets/js/swiper-master/dist/js/swiper.min.js"></script>
<script src="/assets/js/select2.min.js"></script>

<link rel="stylesheet" href="/assets/plugin/loadingModal/css/jquery.loadingModal.min.css">
<script src="/assets/plugin/loadingModal/js/jquery.loadingModal.min.js"></script>

<script src="/assets/js/tui-code-snippet.js"></script>
<!--  script src="/assets/js/tui-date-picker.min.js"></script -->

<script src="/assets/js/tui-dom.js"></script>
<script src="/assets/js/tui-date-picker.js"></script>

<!--owl_slider-->
<script src="/assets/js/owl.carousel.min.js"></script>

<!-- TOAST UI GRID -->
<link rel="stylesheet" href="/assets/js/toast-grid/tui-grid.css" />
<script src="/assets/js/toast-grid/tui-grid.js"></script>

<!-- 개발 공통 스크립트 -->
<script src="/assets/js/common/modal.js?ver=20200102_01"></script>
<script src="/assets/js/common/validation.js?ver=20230830_01"></script>
<script src="/assets/js/common/common.js?ver=20200819_01"></script>
<script type="module" src="/assets/new_join/js/import/rnew_common.js" defer></script>
<script src="/assets/js/common/menu.js?ver=20220124_01"></script>

<c:if test="${pageContext.request.serverName eq 'www.sellerbot.co.kr'}">
    <!-- Google Tag Manager 20210819 추가 -->
    <script> (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-WW7KKMG'); 
    </script> 
    <!-- End Google Tag Manager -->
    
    <!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-SRNM5SJFVE"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-SRNM5SJFVE');
	</script>

    <!-- [네이버 로그 설치] 공통 적용 스크립트 -->
    <script type="text/javascript" src="https://wcs.naver.net/wcslog.js"> </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) { if (f.fbq) return; n = f.fbq = function () { n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments) }; if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0; t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s) }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2404404909682543');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=2404404909682543&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Google tag (gtag.js) 2023.11.01 추가 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-8XV9P2QG6Y"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-8XV9P2QG6Y');
	</script>

	<!-- Google Tag Manager 2023.11.01 추가 -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start':
	
					new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f[removed].insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-TKN4F5L9');
	</script>
	<!-- End Google Tag Manager -->
</c:if>

<!-- Channel Plugin Scripts -->
<script>
    (function() {
        var w = window;
        if (w.ChannelIO) {
            return (window.console.error || window.console.log || function(){})('ChannelIO script included twice.');
        }
        var ch = function() {
            ch.c(arguments);
        };
        ch.q = [];
        ch.c = function(args) {
            ch.q.push(args);
        };
        w.ChannelIO = ch;
        function l() {
            if (w.ChannelIOInitialized) {
            return;
            }
            w.ChannelIOInitialized = true;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = 'https://cdn.channel.io/plugin/ch-plugin-web.js';
            s.charset = 'UTF-8';
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        }
        if (document.readyState === 'complete') {
            l();
        } else if (window.attachEvent) {
            window.attachEvent('onload', l);
        } else {
            window.addEventListener('DOMContentLoaded', l, false);
            window.addEventListener('load', l, false);
        }
    })();
    ChannelIO('boot', {
        "pluginKey": "fd2b7ab5-b960-4df0-908a-e4a1c5693828"
    });
</script>
<!-- End Channel Plugin -->