<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="container">
	<div class="member_wra">
		<div class="left_title_area">
			<div class="left_title_item">
				<div class="title_txt">관리자 페이지<br></div>
				<p class="sub_title_txt"></p>
			</div>
			<!-- <div class="cs_center_info">
				<p class="cs-info-none">고객센터 : 1666-8216<br>
					s-cash@only1fs.com</p>
			</div> -->
		</div>
		<div class="right_contents_area">
			<form id="form" name='f' action='/login' method='POST' onsubmit="return false;" autocomplete="off"
				novalidate="novalidate">
				<div class="member_right_section">
					<div class="member_frm_box">
						<div class="item_mb">
							<span class="lb">이메일(ID)</span>
							<div class="input_box">
								<input type="text" class="textbox_mb input_color_gray" name="usr" id="usr"
									required="required">
								<p id="usr_error" class="error" style="display:none;">*아이디를 입력하여 주세요.</p>
							</div>
						</div>
						<div class="item_mb last">
							<span class="lb">비밀번호</span>
							<div class="input_box">
								<input id="pwd" type="password" class="textbox_mb input_color_gray" name='pwd'
									autocomplete="new-password" required="required">
								<p id="pwd_error" class="error" style="display:none;">*비밀번호를 입력하여 주세요.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="btn_mb_group">
					<button class="btn_cicle" onclick="loginAction();return false;">로그인</button>
				</div>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input type="hidden" id="token" name="token" value="${token }" />
			</form>
		</div>
	</div>

</div>

<script>


	// 필수 입력 체크
	function isValidation() {
		var result = true;
		var focusTarget;
		$(".error").hide();
		$("[required=required]").each(function () {
			var jqThis = $(this);
			var id = jqThis.attr("id");
			if (isNull(jqThis.val())) {
				$("#" + id + "_error").show();
				if (isNull(focusTarget)) { focusTarget = id; }
				result = false;
			}
		});
		$("#" + focusTarget).focus();
		return result;
	}

	function loginAction() {

		if (!isValidation()) {
			return false;
		}

		var params = {
			"usr": $("#usr").val(),
			"pwd": $("#pwd").val() + $("#token").val()
		}



		$.ajax({
			url: '<c:url value="/login"/>',
			data: $.param(params),
			type: 'POST',
			dataType: 'json',
			beforeSend: function (xhr) {
				xhr.setRequestHeader("Accept", "application/json");
			}
		}).done(function (body) {
			var message = body.response.message;
			var error = body.response.error;
			if (error == false) {
				location.href = "/";
			} else {
				if (message == "AUTH-4303") {
					showAlert("해당 사용자가 존재하지 않습니다.");
				} else if (message == "AUTH-4304") {
					showAlert("아이디나 비밀번호를 다시 확인해주세요.");
				} else if (message == "AUTH-4305") {
					showAlert("관리자가 아닙니다 or 일반 사용자가 아닙니다");
				} else if (message == "AUTH-4306") {
					showAlert("5회 이상 로그인에 실패하여 계정이 잠김처리 되었습니다.\n 담당 관리자에게 문의해주세요.");
				} else if (message == "AUTH-4307") {
					showAlert("사용이 정지된 계정입니다.\n 시스템 관리자에게 문의해주시기 바랍니다.");
				} else if (message == "AUTH-4308") {
					showAlert("사용기간이 만료된 계정입니다.\n 시스템 관리자에게 문의해주시기 바랍니다.");
				} else if (message == "AUTH-4309") {
					showAlert("등록된 IP와 일치하지 않습니다. 등록된 접속 IP에서만 접속 가능합니다.");
				} else if (message == "AUTH-4310") {
					showAlert("아이디나 비밀번호를 다시 확인해주세요.\n 5회 이상 실패 시 계정이 잠김 처리 되오니 주의해주세요");
				} else if (message == "AUTH-4311") {
					showAlert("사용자의 비밀번호 설정이 필요합니다.");
				} else {
					showAlert("로그인 에러");
				}
			}

		});
		// 			document.f.submit();
	}

</script>