<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="container">
    <div class="member_wra">
        <div class="left_title_area">
            <div class="left_title_item">
                <div class="title_txt">아이디<br>찾기</div>
                <p class="sub_title_txt">회원가입 시 입력한 정보로<br>아이디를 찾습니다.</p>
            </div>
            <!-- <div class="cs_center_info">
                <p class="cs-info-none">고객센터 : 1666-8216<br>
                    s-cash@only1fs.com</p>
            </div> -->
        </div>
        <div class="right_contents_area">
            <form id="form" method='POST' onsubmit="return false;" novalidate="novalidate" autocomplete="off">
                <div class="member_right_section">
                    <div class="member_frm_box">
                        <div class="item_mb">
                            <span class="lb">상호명</span>
                            <div class="input_box">
                                <input type="text" class="textbox_mb" name="biz_nm" id="biz_nm"
                                    value="${custDto.biz_nm }" required="required">
                                <p id="biz_nm_error" class="error" style="display:none;">*상호명을 입력해주세요.</p>
                            </div>
                        </div>
                        <div class="item_mb">
                            <span class="lb">사업자등록번호</span>
                            <div class="input_box">
                                <input type="text" class="textbox_mb" name="biz_no" id="biz_no"
                                    value="${custDto.biz_no }" required="required" data-valitype="bizNo" maxlength="10">
                                <p id="biz_no_error" class="error" style="display:none;">*사업자 등록번호를 정확히 입력해주세요.</p>
                            </div>
                        </div>
                        <div class="item_mb">
                            <span class="lb">대표자명</span>
                            <div class="input_box">
                                <input type="text" class="textbox_mb" name="ceo_nm" id="ceo_nm"
                                    value="${custDto.ceo_nm }" required="required">
                                <p id="ceo_nm_error" class="error" style="display:none;">*대표자명을 입력해주세요.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn_mb_group">
                    <button class="btn_cicle" id="checkCustId">아이디<br>찾기</button>
                </div>
            </form>
        </div>
    </div>
    <div class="popup popup_certify">
        <div class="pop">
            <span class="close_pop"><img src="/assets/images/member/close_gray.png" alt=""></span>
            <div class="pop_body">
                <p class="desc_pop">
                    입력한 정보를 확인하세요.<br />
                    아이디가 존재하지 않습니다.
                </p>
            </div>
            <div class="pop_footer">
                <button type="button" class="close_btn">확인</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {


        $(".popup_certify .pop_footer .close_btn").click(function () {
            $(".popup_certify").hide();
        });
        $(".popup_certify .close_pop").click(function () {
            $(".popup_certify").hide();
        });


        $('input[type="text"]').keydown(function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                fnFindId();
            }
        });

        $("#checkCustId").click(function (event) {
            fnFindId();
        });


        // 아이디 찾기 홤수
        function fnFindId() {

            if (!isVali()) { return false; }

            var ceo_nm = $("#ceo_nm").val();
            $.ajax({
                url: '<c:url value="/pub/member/find_id"/>',
                data: $('#form').serialize(),
                type: 'POST',
                success: function (data) {
                    if (data == "ID 없음") {
                        alertCertify();
                    } else {
                        window.location.href = "/pub/member/result_id?cust_id=" + data + "&ceo_nm=" + ceo_nm;
                    }
                },
                error: function (error) {
                    alertCertify();
                }
            });

        };


    });

    function alertCertify() {
        $(".popup_certify").show();
    }

</script>