<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<script src="/assets/js/common/modal.js"></script>

<div class="container">
	<div class="member_wra">
		<div class="left_title_area">
			<div class="left_title_item"> 
				<div class="title_txt">비밀번호<br>찾기</div>
				<p class="sub_title_txt">회원가입 시 입력한 정보로<br>비밀번호를 찾습니다.</p>
			</div>
			<!-- <div class="cs_center_info">
			    <p class="cs-info-none">고객센터 : 1666-8216<br>
			       s-cash@only1fs.com</p>
			</div> -->
		</div>
		<div class="right_contents_area">
			<form onsubmit="return false;">
				<div class="member_right_section">
					<div class="member_frm_box">
						<div class="item_mb">
							<span class="lb">이메일(ID)</span>
							<div class="input_box">
								<input id="custId" name="custId" type="text" class="textbox_mb" value="${cust_id }">
								<p id="authNoMsg" class="error" >*인증번호가 일치하지 않습니다.</p>
							</div>
						</div>
					
					</div>
				</div>
				<div class="btn_mb_group">
					<button class="btn_cicle" id="checkAuthNo" >다음</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script>

	$(document).ready(function () {
		$(".error").hide();
		$(".pw_desc").hide();
		$("#sendSertNum").click(function(event){
			event.preventDefault();
			var custId = $("#custId").val();
			if(validateEmail(custId)){			
				$.post("/pub/api/member/join/confirmCust", {custId: custId}, function(data, status){
						$('#custId').attr('readonly', true);
						$(".error").hide();
						$(".pw_desc").show();
						alertCertify("고객님의 이메일로<br>인증번호가 전송되었습니다.");
					})
					.fail(function(response){
						if(response.status == 410){ <%-- 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음. --%>
							location.reload();
						}else if(response.status == 409){ <%-- 등록된 아이디가 있음 --%>
							alertCertify(custId+" 이메일은 <br>이미 등록되어 있습니다.<br> 아이디 찾기를 진행 하세요.");
						}else if(response.status == 4490){ <%-- 이미 ID에 인증번호가 발송된 상태 --%>
							$(".pw_desc").hide();
							$(".error").show();
							alertCertify("인증 번호가 발송된 상태 입니다.");
						} else {						
							alert("Error: " + response.status);
						}
					});
			}else{
				$(".pw_desc").hide();
				$(".error").show();
				alertCertify("*이메일 주소를 입력해주세요.");
			}
			return false;
		});
		
		$("#checkAuthNo").click(function(event){
			var custId = $("#custId").val();
			if(validateEmail(custId)){
				$.post("/pub/member/result_pw", {custId: custId}, function(data, status){
					if (data == "OK") {
						window.location.href="/pub/member/result_pw?"+ $.param({'custId': custId});	
					} else if (data == "AUTH-2009") {
						$(".pw_desc").hide();
						$(".error").show();
						alertCertify("플레이오토를 통해 가입하신 회원은 플레이오토를 통해서만 접속 가능하며, <br>  아래 경로에서 이용해주세요. <br> (*플토EMP → 상단 '셀러봇캐시' 클릭 시 자동접속)");
					}
				})
				.fail(function(res){
					$(".pw_desc").hide();
					$(".error").show();
					alertCertify("*이메일 주소를 입력해주세요.");
				});
			} else {
				$(".pw_desc").hide();
				$(".error").show();
				alertCertify("*이메일 주소를 입력해주세요.");				
			}
		});
		<%-- 다음 버튼으로 인증 번호 확인 --%>
		<%-- $("#checkAuthNo").click(function(event){
			var authNo = $("#authNo").val();
			if(authNo.length == 0){
				$(".pw_desc").hide();
				$(".error").show();
				alertCertify("인증번호를 입력하세요.");
				return;
			}
			if($('#custId').attr('readonly') ){
				$.post("/pub/api/member/join/confirmAuthNo", {authNo: authNo}, function(data, status){
					window.location.href="/pub/member/result_pw";
				})
				.fail(function(response){
					if(response.status == 410){ 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
						location.reload();
					}else if(response.status == 400){ 인증번호 오류
						$(".pw_desc").hide();
						$(".error").show();
						alertCertify("인증번호가 틀립니다.");
					}else if(response.status == 4490){ 인증 번호 발송을 요청 하지 않은 상태
						$(".pw_desc").hide();
						$(".error").show();
						alertCertify("인증번호 발송을 먼저 하세요.");
					} else {						
						alert("Error: " + response.status);
					}
				});
			}else{
				$(".pw_desc").hide();
				$(".error").show();
				alertCertify("인증번호 발송을 먼저 하세요.");
			}
		}); --%>
		
	});
	
	function alertCertify(msg) {
		$("#authNoMsg").html(msg);
	}

	function validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}
</script>