<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="container">
	<div class="member_wra">
		<div class="left_title_area">
			<div class="left_title_item">
				<div class="title_txt">아이디<br>찾기</div>
				<p class="sub_title_txt">회원가입 시 입력한 정보로<br>아이디를 찾습니다.</p>
			</div>
			<!-- <div class="cs_center_info">
				<p class="cs-info-none">고객센터 : 1666-8216<br>
					s-cash@only1fs.com</p>
			</div> -->
		</div>
		<div class="right_contents_area">
			<div class="find_box">
				<div class="find_item">
					<div class="msg_mb">
						<p>
							<c:out value="${custDto.ceo_nm}" /> 님의 아이디는<br>
							<span>
								<c:out value="${custDto.cust_id}" /></span> 입니다.</p>
						<img src="/assets/images/member/img_findid.png" alt="">
					</div>
				</div>
			</div>
			<div class="btn_mb_group">
				<button class="btn_cicle btn_gray"
					onclick="window.location.href='/pub/member/find_pw'">비밀번호<br>찾기</button>
				<button class="btn_cicle" onclick="window.location.href='/login'">로그인<br>하기</button>
			</div>
		</div>
	</div>
</div>