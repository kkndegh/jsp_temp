<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="container">
	<div class="member_wra">
		<div class="left_title_area">
			<div class="left_title_item">
				<div class="title_txt">셀러봇캐시로<br>들어가볼까요?</div>
				<p class="sub_title_txt">하나의 아이디로 셀러봇캐시가<br>준비한 특별한 서비스를 만나보세요.</p>
			</div>
			<!-- <div class="cs_center_info">
				<p class="cs-info-none">고객센터 : 1666-8216<br>
					s-cash@only1fs.com</p>
			</div> -->
		</div>
		<div class="right_contents_area">
			<form id="form" name='f' action='/login' method='POST' onsubmit="return false;" autocomplete="off"
				novalidate="novalidate">
				<div class="member_right_section">
					<div class="member_frm_box">
						<div class="item_mb">
							<span class="lb">이메일(ID)</span>
							<div class="input_box">
								<input type="text" class="textbox_mb input_color_gray" name="usr" id="usr"
									required="required">
								<div class="save_id">
									<input type="checkbox" class="chkbox" id="saveId">
									<label for="saveId">아이디저장</label>
								</div>
								<p id="usr_error" class="error" style="display:none;">*아이디를 입력하여 주세요.</p>
							</div>
						</div>
						<div class="item_mb last">
							<span class="lb">비밀번호</span>
							<div class="input_box">
								<input id="pwd" type="password" class="textbox_mb input_color_gray" name='pwd'
									autocomplete="new-password" required="required">
								<p id="pwd_error" class="error" style="display:none;">*비밀번호를 입력하여 주세요.</p>
							</div>
						</div>
						<div class="find_mb">
							<a href="/pub/member/find_id">아이디</a>
							<span class="bar">|</span>
							<a href="/pub/member/find_pw">비밀번호</a>
							<span>찾기</span>
						</div>
					</div>
				</div>
				<div class="btn_mb_group">
					<button type="button" class="btn_cicle btn_gray"
						onclick="window.location.href='/pub/member/step1'">회원가입</button>
					<button class="btn_cicle" onclick="loginAction();return false;">로그인</button>
				</div>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form>
		</div>
	</div>
	<div class="popup popup_certify alert" id="login_tmppwd_send_pop">
		<div class="pop">
			<span id="h_login_tmppwd_send_pop_cancle" class="close_pop"><img src="/assets/images/member/close_gray.png"
					alt=""></span>
			<div class="pop_body">
				<p class="desc_pop">
					<span style="font-weight: bold;">신규 셀러봇캐시 로그인을 위한 절차 안내</span>
					<br>
					<br>
					<span style="font-weight: lighter;text-align:left;">
						셀러봇캐시 홈페이지가 리뉴얼되어 기존 셀러봇캐시<br>
						고객께서는 기등록된 이메일로 임시 비밀번호를 발급<br>
						받으신 후 로그인하실 수 있습니다.
					</span>
					<br>
					<br>
					<span style="font-weight: normal;">이메일(ID):<span id="login_tmppwd_send_pop_email"
							style="color:#009aca;"></span></span>
				</p>
			</div>
			<div class="pop_footer">
				<button type="button" class="close_btn" id="login_tmppwd_send_pop_cancle"
					style="width: 50%;float:left;background-color:grey;">취소</button>
				<button type="button" class="ok_btn pop_btn2" id="login_tmppwd_send_pop_ok"
					style="width: 50%;float:left;">임시비밀번호 발급</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		var usr = getCookie("usr");
		$("input[name='usr']").val(usr);
		if (usr != "") {
			$("#saveId").prop("checked", true);
		}
		$("#login_tmppwd_send_pop_cancle, #h_login_tmppwd_send_pop_cancle").click(function () {
			$("#login_tmppwd_send_pop").hide();
		});
		$("#login_tmppwd_send_pop_ok").click(function () {
			$("#login_tmppwd_send_pop").hide();
			var custId = $("#usr").val();

			$.post("/pub/member/result_pw_old", { custId: custId }, function (data, status) {

			}).done(function (response) {
				showAlert("고객님의 이메일로 임시 비밀번호가\n전송되었습니다.");
			});
		});

		//TODO: 배포 주의.
		//$("input[name='usr']").val('s-cash@only1fs.com');
		//$("input[name='pwd']").val('xptmxm1!');
	});

	// 필수 입력 체크
	function isValidation() {
		var result = true;
		var focusTarget;
		$(".error").hide();
		$("[required=required]").each(function () {
			var jqThis = $(this);
			var id = jqThis.attr("id");
			if (isNull(jqThis.val())) {
				$("#" + id + "_error").show();
				if (isNull(focusTarget)) { focusTarget = id; }
				result = false;
			}
		});
		$("#" + focusTarget).focus();
		return result;
	}

	function loginAction() {

		if (!isValidation()) {
			return false;
		}

		if ($("#saveId").is(":checked")) {
			setCookie("usr", $("input[name='usr']").val(), 9999);
		} else {
			deleteCookie("usr");
		}

		$.ajax({
			url: '<c:url value="/login"/>',
			data: $('#form').serialize(),
			type: 'POST',
			dataType: 'json',
			beforeSend: function (xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				showLoadingModal();
			}
		}).done(function (body) {
			var message = body.response.message;
			var error = body.response.error;
			if (error == false) {
				location.href = "/";
			} else {
				if (message == "AUTH-4303") {
					showAlert("해당 사용자가 존재하지 않습니다.");
				} else if (message == "AUTH-4304") {
					showAlert("아이디나 비밀번호를 다시 확인해주세요.");
				} else if (message == "AUTH-4305") {
					showAlert("관리자가 아닙니다 or 일반 사용자가 아닙니다");
				} else if (message == "AUTH-4306") {
					showAlert("5회 이상 로그인에 실패하여 계정이 잠김처리 되었습니다.\n 담당 관리자에게 문의해주세요.");
				} else if (message == "AUTH-4307") {
					showAlert("사용이 정지된 계정입니다.\n 시스템 관리자에게 문의해주시기 바랍니다.");
				} else if (message == "AUTH-4308") {
					showAlert("사용기간이 만료된 계정입니다.\n 시스템 관리자에게 문의해주시기 바랍니다.");
				} else if (message == "AUTH-4309") {
					showAlert("등록된 IP와 일치하지 않습니다. 등록된 접속 IP에서만 접속 가능합니다.");
				} else if (message == "AUTH-4310") {
					showAlert("아이디나 비밀번호를 다시 확인해주세요.\n 5회 이상 실패 시 계정이 잠김 처리 되오니 주의해주세요");
				} else if (message == "AUTH-4311") {
					$("#login_tmppwd_send_pop_email").html($("#usr").val());
					$("#login_tmppwd_send_pop").show();
				} else if (message == "AUTH-4312") {
					showAlert("소드원서비스 탈퇴 요청 대상자입니다.\n 탈퇴 처리 결과 메일 발송 기달려주시길 바랍니다.");
				} else {
					showAlert("로그인 에러");
				}
				
				hideLoadingModal();
			}

		});
		// 			document.f.submit();
	}
	function setCookie(cookieName, value, exdays) {
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var cookieValue = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toGMTString());
		document.cookie = cookieName + "=" + cookieValue;
	}

	function getCookie(cookieName) {
		cookieName = cookieName + '=';
		var cookieData = document.cookie;
		var start = cookieData.indexOf(cookieName);
		var cookieValue = '';
		if (start != -1) {
			start += cookieName.length;
			var end = cookieData.indexOf(';', start);
			if (end == -1) end = cookieData.length;
			cookieValue = cookieData.substring(start, end);
		}
		return unescape(cookieValue);
	}

	function deleteCookie(cookieName) {
		var expireDate = new Date();
		expireDate.setDate(expireDate.getDate() - 1);
		document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
	}
</script>