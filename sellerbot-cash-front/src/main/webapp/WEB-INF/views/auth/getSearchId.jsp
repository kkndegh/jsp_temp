<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<form>
    <div class="form-group">
        <label for="biz_nm">상호명</label>
        <input type="text" class="form-control" id="biz_nm" name="biz_nm" placeholder="상호명을 입력하세요.">
    </div>
    <div class="form-group">
        <label for="biz_no">사업자 등록 번호</label>
        <input type="text" class="form-control" id="biz_no" placeholder="사업자 번호를 입력하세요.">
    </div>
    <div class="form-group">
        <label for="biz_no">대표자명</label>
        <input type="text" class="form-control" id="ceo_nm" placeholder="대표자명을 입력하세요.">
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>