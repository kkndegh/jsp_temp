<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
	function pageB2DDMove(banner_item_seq_no, banner_link_url) {
		if (banner_item_seq_no != null && banner_item_seq_no != "") {
			$.post("/sub/banner/view/count", { bannerItemSeqNo: banner_item_seq_no }, function (res) {
			})
			.fail(function (response) {
			});
		}

		if (banner_link_url != null && banner_link_url != "") {
			if (banner_link_url.indexOf('http') > -1) { //외부 페이이 호출시 새창
				window.open(banner_link_url);
			} else {
				location.href = banner_link_url;
			}
		}
	}
</script>
<c:if test="${!empty bannerB2DDDataList}">
	<div class="owl-carousel owl-theme sellerbot-banner market">
	<c:forEach items="${bannerB2DDDataList}" varStatus="status" var="data">
		<c:set var="imgPath" value="${data.stor_path}${data.stor_file_nm}"></c:set>
		<c:set var="background" value="${data.background}"></c:set>
		<div class="item" style="cursor:pointer;">
			<div class="notice_bnr">
				<img src="/imagefile/${imgPath}" onclick="javascript:pageB2DDMove('${data.banner_item_seq_no}', '${data.link_url}')" alt="">
			</div>
		</div>
	</c:forEach>
	</div>
</c:if>