<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
	function pageB2RRDMove(banner_item_seq_no, banner_link_url) {
		if (banner_item_seq_no != null && banner_item_seq_no != "") {
			$.post("/sub/banner/view/count", { bannerItemSeqNo: banner_item_seq_no }, function (res) {
			})
			.fail(function (response) {
			});
		}

		if (banner_link_url != null && banner_link_url != "") {
			if (banner_link_url.indexOf('http') > -1) { //외부 페이이 호출시 새창
				window.open(banner_link_url);
			} else {
				location.href = banner_link_url;
			}
		}
	}
	function pageB2RRDHide() {
		$('.main_bnr.big').hide();
	}
</script>
<c:if test="${!empty bannerB2RRDataList}">
	<div class="main_bnr big">
		<div class="owl-carousel owl-theme sellerbot-banner vertical">
		<c:forEach items="${bannerB2RRDataList}" varStatus="status" var="data">
			<c:set var="imgPath" value="${data.stor_path}${data.stor_file_nm}"></c:set>
			<c:set var="background" value="${data.background}"></c:set>
			<div class="item" style="cursor:pointer;">
				<img src="/imagefile/${imgPath}" onclick="javascript:pageB2RRDMove('${data.banner_item_seq_no}', '${data.link_url}')" alt="" />
			</div>
		</c:forEach>
		</div>
		<div class="owl-close" style="
			position: relative;
			width: auto;
			float: left;
			z-index: 1;
			">
			<button class="closeOwl" style="
			position: static;
			width: 1.5rem;
			height: 1.5rem;
			border: 1px solid #ddd !important;
			margin: 8px 3px 0 0 !important;
			color: #ddd;
			border-radius: 3px;
			" onclick="javascript:pageB2RRDHide()" alt="" >✕</button>
		</div>
	</div>
</c:if>