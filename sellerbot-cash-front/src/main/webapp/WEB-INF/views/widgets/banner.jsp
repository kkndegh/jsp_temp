<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script>
	$(document).ready(function () {

		// 배너 조회수 업데이트 및 페이지 이동
		var pageMoveFlag = true; // 롤링 버튼 클릭시 페이지 이동 막기.
		$(".owl-carousel").children(".owl-nav").find(".owl-prev").click(function() {
			pageMoveFlag = false;
		});

		$(".owl-carousel").children(".owl-nav").find(".owl-next").click(function() {
			pageMoveFlag = false;
		});
		$(".owl-carousel").click(function() {
			var v = $(this);
			var banner_item_seq_no = v.children(".owl-stage-outer").find(".active .item").data("banner_item_seq_no");
			var banner_link_url = v.children(".owl-stage-outer").find(".active .item").data("b1dd_banner_link_url");

			if (pageMoveFlag) {
				if (banner_item_seq_no != null && banner_item_seq_no != "") {
					$.post("/sub/banner/view/count", { bannerItemSeqNo: banner_item_seq_no }, function (res) {
					})
					.fail(function (response) {
					});
				}
	
				if (banner_link_url != null && banner_link_url != "") {
					if (banner_link_url.indexOf('http') > -1) { //외부 페이이 호출시 새창
						window.open(banner_link_url);
					} else {
						location.href = banner_link_url;
					}
				}
			}

			pageMoveFlag = true;
		});

		// 20230830 미리정산가능금액
		setTimeout(function () {
			if (typeof $('.banner_payment').html() !== 'undefined') {
				$.ajax({
					url: '/sub/finGood/preSett'
					, type: 'get'
					, async: false
					, success: function (res) {
						$(".banner_payment").attr('style', 'display: block;')
						$(".price_bfhand").html("<p>최대 <b>" + formatNumber(res) + "</b>원</p>");
					}
					, error: function (request, status, error) {
						console.error("미리정산가능금액 가져오기 실패");
					}
				});
			}
		}, 2000);
	});
</script>
<c:if test="${empty bannerDataList}">
	<div class="banner_payment" style="display: none;">
		<div class="area_banner_payment">
			<div class="bf_hand_tlt">
				<img src="/assets/images/payment/question_Icon.png" alt="">
				<p><b>미리 정산받을 수 있는 금액</b><br>
					내가 지원받을 수 있는 금액
				</p>
			</div>
			<div class="price_bfhand">
			</div>
			<a href="/sub/pre_calculate/pre_calculate" class="more_bfhand">자세히보기</a>
		</div>
	</div>
</c:if>
<c:if test="${!empty bannerDataList}">
	<div class="owl-carousel owl-theme sellerbot-banner" style="cursor:pointer;">
		<c:forEach items="${bannerDataList}" varStatus="status" var="data">
			<c:set var="imgPath" value="${data.stor_path}${data.stor_file_nm}"></c:set>
			<c:set var="background" value="${data.background}"></c:set>
			<div class="item" data-banner_item_seq_no="${data.banner_item_seq_no}" data-b1dd_banner_link_url="${data.link_url}">
				<div class="bnr_acuon" <c:if test="${!empty background}"> style="background: ${background}"</c:if>>
					<div class="area_bnr_acuon">
						<img src="/imagefile/${imgPath}" alt="온라인 마켓에 입점한  개인, 법인 사업자님들~">
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</c:if>