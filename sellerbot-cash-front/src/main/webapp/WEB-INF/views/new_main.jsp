<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>

<!--s:2200213 추가-->
<!-- <script src="/assets/js/jquery-3.3.1.min.js"></script> -->
<script src="/assets/js/jquery-confirm.min.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/jquery-confirm.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/popup.css?ver=20230118" />
<!--e:2200213 추가-->
<script>

	var gTodayModal = null;
	var gCybMonyModal = null;

	function changeMadalToday() {
		if (gCybMonyModal != null)
			gCybMonyModal.close();

		pop_today();
	}

	function changeMadalCybMony() {
		if (gTodayModal != null)
			gTodayModal.close();

		pop_store_cash();
	}

	// 오늘 받는 정산금
	function pop_today(idx) {
		var todaySettAccPrc = "${dashboardInfo.today_sett_acc.today_sett_acc_prc_sum}";
		if (todaySettAccPrc == "" || todaySettAccPrc == "0") {
			showAlert("오늘 받는 정산금이 0원입니다.");
			return;
		}

		var width = $(window).width();
		var height = $(window).height();

		var bw = '600';
		var bh = '50%';
		if (width < 600) bw = '98%';
		else bw = '600';

		if (height < 600) bh = '98%';  //모바일용
		else bh = '50%';               //PC 

		gTodayModal = $.confirm({
			theme: 'modal_box_wrap',
			buttons: {
				닫기: {
					btnClass: 'btn btn_gray2',
					action: function () {
						gTodayModal = null;
					}
				}
			},
			//	columnClass: 'col-md-6 col-md-offset-3',    				
			boxWidth: bw,
			boxHeight: bh,
			useBootstrap: false,
			title: '오늘 받는 정산금',
			content: '<div class="modal_popup position_r">' +
				'<button class="btn_tab btn_yllw" onclick="javascript:changeMadalCybMony();">판매몰 예치금</button>' +
				'	<div class="modal_con">' +
				'		<div class="pop_header pop_today fl_wrap">' +
				//'			<h3 class="txt_fff">' + moment("${dashboardInfo.today_sett_acc.today_dt}").format("YYYY년 MMM DD일") + ' 기준</h3>' +
				'			<h3 class="txt_fff">' + moment(new Date()).format("YYYY년 MMM DD일") + ' 기준</h3>' +
				'			<dl class="txt_fff fl_wrap">' +
				'				<dt class="txt_fff fl_wrap"><span class="fl_left">오늘 받는 정산금</span><span class="fl_right"><fmt:formatNumber value="${dashboardInfo.today_sett_acc.today_sett_acc_prc_sum}" pattern="#,###" />원</span></dt>' +
				'				<dd class="txt_fff fl_wrap"><span class="fl_left">오픈마켓<c:if test="${not empty dashboardInfo.today_sett_acc.open_last_dt and dashboardInfo.today_sett_acc.open_last_dt != \"\"}"> (${dashboardInfo.today_sett_acc.open_last_dt} 기준)</c:if></span><span class="fl_right"><fmt:formatNumber value="${dashboardInfo.today_sett_acc.today_sett_acc_open_prc_sum}" pattern="#,###" />원</span></dd>' +
				'				<dd class="txt_fff fl_wrap"><span class="fl_left">비오픈마켓<c:if test="${not empty dashboardInfo.today_sett_acc.nopen_last_dt and dashboardInfo.today_sett_acc.nopen_last_dt != \"\"}"> (${dashboardInfo.today_sett_acc.nopen_last_dt} 기준)</c:if></span><span class="fl_right"><fmt:formatNumber value="${dashboardInfo.today_sett_acc.today_sett_acc_nopen_prc_sum}" pattern="#,###" />원</span></dd>' +
				'			</dl>' +
				'			<p class="txt_fff txt_s12 w80 position_c">※ 실제 입금받으실 금액과는 다소 차이가 있을 수 있습니다.</p>' +
				'		</div>' +
				'		<div class="pop_list01 scroll">' +
				'			<table>' +
				'				<c:forEach var="mall" items="${dashboardInfo.today_sett_acc.mall_list}" varStatus="status">' +
				'					<tr>' +
				'						<th>' +
				'							<li class="txt_s14 txt_666"><img src="/imagefile/${mall.stor_path}${mall.stor_file_nm}" alt="몰 로고"></li>' +
				'							<li><fmt:formatNumber value="${mall.today_sett_acc_prc_sum}" pattern="#,###" />원</li>' +
				'						</th>' +
				'						<td>' +
				'							<c:forEach var="today_list" items="${mall.today_sett_acc_list}" varStatus="status">' +
				'								<p class="fl_wrap"><span class="fl_left">${today_list.mall_cert_1st_id}</span><span class="fl_right"><fmt:formatNumber value="${today_list.today_sett_acc_prc}" pattern="#,###" />원</span></p>' +
				'							</c:forEach>' +
				'						</td>' +
				'					</tr>' +
				'				</c:forEach>' +
				'			</table>' +
				'		</div>' +
				'	</div>' +
				'</div>',
		});
	}

	// 판매몰 예치금
	function pop_store_cash(idx) {
		var cybMony = "${dashboardInfo.cyb_mony.cyb_mony_sum}";
		if (cybMony == "" || cybMony == "0") {
			showAlert("판매몰 예치금 잔액이 0원입니다.");
			return;
		}

		var width = $(window).width();
		var height = $(window).height();

		var bw = '600';
		var bh = '50%';
		if (width < 600) bw = '98%';
		else bw = '600';

		if (height < 600) bh = '98%';  //모바일용
		else bh = '50%';               //PC 

		gCybMonyModal = $.confirm({
			theme: 'modal_box_wrap',
			buttons: {
				닫기: {
					btnClass: 'btn btn_gray2',
					action: function () {
						gCybMonyModal = null;
					}
				}
			},
			//	columnClass: 'col-md-6 col-md-offset-3',    				
			boxWidth: bw,
			boxHeight: bh,
			useBootstrap: false,
			title: '판매몰 예치금',
			content: '<div class="modal_popup position_r">' +
				'	<button class="btn_tab btn_blu" onclick="javascript:changeMadalToday();">오늘 받는 정산금</button>' +
				'	<div class="modal_con">' +
				'		<div class="pop_header pop_store_cash fl_wrap">' +
				'			<h3>${dashboardInfo.cyb_mony.open_last_dt} 기준</h3>' +
				'			<dl class="fl_wrap">' +
				'				<dt class="fl_wrap"><span class="fl_left">판매몰 예치금</span><span class="fl_right"><fmt:formatNumber value="${dashboardInfo.cyb_mony.cyb_mony_sum}" pattern="#,###" />원</span></dt>' +
				'			</dl>' +
				'			<p class="txt_s12 w80 position_c">※ 판매몰예치금은 오픈마켓에 적립된 금액이며 각 판매몰에서 언제든지 활용하실 수 있습니다.</p>' +
				'			<p class="txt_s12 w80 position_c">※ 괄호 안의 명칭은 각 판매몰에서 칭하는 판매몰예치금 성격의 명칭입니다. </p>' +
				'		</div>' +
				'		<div class="pop_list01 scroll">' +
				'			<table>' +
				'				<c:forEach var="mall" items="${dashboardInfo.cyb_mony.mall_list}" varStatus="status">' +
				'					<tr>' +
				'						<th>' +
				'							<li class="txt_s14 txt_666"><img src="/imagefile/${mall.stor_path}${mall.stor_file_nm}" alt="몰 로고"><span>(<c:out value="${mall.cyb_mony_cd_nm}"/>)</span></li>' +
				'							<li><fmt:formatNumber value="${mall.cyb_mony_prc_sum}" pattern="#,###" />원</li>' +
				'						</th>' +
				'						<td>' +
				'							<c:forEach var="today_list" items="${mall.cyb_mony_list}" varStatus="status">' +
				'								<p class="fl_wrap"><span class="fl_left">${today_list.mall_cert_1st_id}</span><span class="fl_right"><fmt:formatNumber value="${today_list.cyb_mony_prc}" pattern="#,###" />원</span></p>' +
				'							</c:forEach>' +
				'						</td>' +
				'					</tr>' +
				'				</c:forEach>' +
				'			</table>' +
				'		</div>' +
				'	</div>' +
				'</div>',
		});
	}
</script>
<div class="container">
	<div class="main_wra">
		<div class="dash_main">
			<div class="main_layout">
				<div class="layout_md">
					<!-- 정산예정금 요약 Start -->
					<div class="n_card_main n_line1">
						<a href="/sub/settAcc/all" class="more_info_btn"><img src="/assets/images/main/vtn_viewmore.png"
								alt=""></a>
						<h1>정산예정금 요약</h1>
						<div class="summary_payment total">
							<p class="lb">합계<span>(오픈마켓+비오픈마켓)</span></p>
							<p class="num"><b>
									<fmt:formatNumber value="${dashboardInfo.sett_acc_sche.sett_acc_pln_sum}" pattern="#,###" /></b>원
							</p>
						</div>
						<div class="summary_payment fl_wrap mt05">
							<div class="calculate fl_left">
								<dl>
									<dt class="pb05">오픈마켓<span>전일 대비</span></dt>
									<c:choose>
										<c:when test="${dashboardInfo.sett_acc_sche.sett_acc_pln_open_sum_per_day > 0}">
											<dd class="accounts_plus mt15"><b>
													<fmt:formatNumber value="${dashboardInfo.sett_acc_sche.sett_acc_pln_open_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span class="gap">+
													<fmt:formatNumber
														value="${dashboardInfo.sett_acc_sche.sett_acc_pln_open_sum_per_day}"
														pattern="#,###" />원 </span>
										</c:when>
										<c:when test="${dashboardInfo.sett_acc_sche.sett_acc_pln_open_sum_per_day < 0}">
											<dd class="accounts_minus mt15"><b>
													<fmt:formatNumber value="${dashboardInfo.sett_acc_sche.sett_acc_pln_open_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span class="gap">-
													<fmt:formatNumber
														value="${dashboardInfo.sett_acc_sche.sett_acc_pln_open_sum_per_day * -1}"
														pattern="#,###" />원</span>
										</c:when>
										<c:otherwise>
											<dd class="mt15"><b>
													<fmt:formatNumber value="${dashboardInfo.sett_acc_sche.sett_acc_pln_open_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
											<dd>-&nbsp;&nbsp;&nbsp;</dd>
										</c:otherwise>
									</c:choose>
									</dd>
								</dl>
							</div>
							<div class="calculate fl_left">
								<dl>
									<dt class="pb05">비오픈마켓<span>전일 대비</span></dt>
									<c:choose>
										<c:when test="${dashboardInfo.sett_acc_sche.sett_acc_pln_nopen_sum_per_day > 0}">
											<dd class="accounts_plus mt15"><b>
													<fmt:formatNumber value="${dashboardInfo.sett_acc_sche.sett_acc_pln_nopen_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span class="gap">+
													<fmt:formatNumber
														value="${dashboardInfo.sett_acc_sche.sett_acc_pln_nopen_sum_per_day}"
														pattern="#,###" />원 </span>
										</c:when>
										<c:when test="${dashboardInfo.sett_acc_sche.sett_acc_pln_nopen_sum_per_day < 0}">
											<dd class="accounts_minus mt15"><b>
													<fmt:formatNumber value="${dashboardInfo.sett_acc_sche.sett_acc_pln_nopen_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span class="gap">-
													<fmt:formatNumber
														value="${dashboardInfo.sett_acc_sche.sett_acc_pln_nopen_sum_per_day * -1}"
														pattern="#,###" />원</span>
										</c:when>
										<c:otherwise>
											<dd class="mt15"><b>
													<fmt:formatNumber value="${dashboardInfo.sett_acc_sche.sett_acc_pln_nopen_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span>-&nbsp;&nbsp;</span>
										</c:otherwise>
									</c:choose>
									</dd>
								</dl>
							</div>
						</div>
						<div class="update_info_box">
							<c:if test="${!empty dashboardInfo.sett_acc_sche.scra_ts}">
								<a class="icon_update"><img src="/assets/images/main/refresh.png" alt=""></a>
								<p>마지막 업데이트일자 ${dashboardInfo.sett_acc_sche.scra_ts}</p>
							</c:if>
						</div>
					</div>
					<!-- 정산예정금 요약 End -->
					<!-- 정산예정금 관리팁 Start -->
					<div class="n_tip_area tip_list_area position_r" style="overflow:hidden">
						<div class="swiper-wrapper">
							<c:choose>
								<c:when test="${!empty dashboardInfo.tip_cnt_list}">
									<c:forEach var="tip" items="${dashboardInfo.tip_cnt_list}">
										<div class="swiper-slide">
											<span class="n_ico_new"><img src="/assets/images/main/icon_new.png" alt="new"></span>
											<p class="n_tip">
												<a href="/sub/settAcc/calendar" class="btn_go">
													<!-- <img src="/assets/images/common/logo/logo${tip.mall_cd}.png" class="logo"> -->
													<img src="/imagefile/${tip.stor_path}${tip.stor_file_nm}" alt="몰 로고" class="logo">
													<c:if test="${tip.mall_cd eq '001' || tip.mall_cd eq '002' ||
													tip.mall_cd eq '003' || tip.mall_cd eq '008'}">알 수 없는 공제내역이 <b>${tip.tip_count}건</b> 있습니다.</c:if>
													<c:if test="${tip.mall_cd eq '031'}">정산예정금에 관한 관리팁이 <b>${tip.tip_count}건</b> 있습니다.</c:if>
													<c:if test="${tip.mall_cd eq '053'}">광고비 내역이 <b>${tip.tip_count}건</b> 있습니다.</c:if>
													<img src="/assets/images/main/icon_arrow.png">
												</a>
											</p>
										</div>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<div class="swiper-slide">
										<span class="n_ico_new"><img src="/assets/images/main/icon_new.png" alt="new"></span>
										<p class="n_tip">
											<a href="/sub/settAcc/calendar" class="btn_go">
												정산예정금에 관한 관리팁이 0건</b> 있습니다. 
												<img src="/assets/images/main/icon_arrow.png">
											</a>
										</p>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<!-- 정산예정금 관리팁 End -->
					<!-- 금융서비스 Start -->
					<c:set var="bizNm" value="${sessionScope.cust.biz_nm}" />
					<c:if test="${fn:length(sessionScope.cust.biz_nm) gt 12}">
						<c:set var="bizNm" value="${fn:substring(sessionScope.cust.biz_nm,0,12)}..." />
					</c:if>
					<c:choose>
						<c:when test="${dashboardInfo.pre_sett gt 5000000}">
							<div class="n_card_main n_line2">
								<a href="/sub/pre_calculate/pre_calculate" class="more_info_btn">
									<img src="/assets/images/main/vtn_viewmore.png" alt="금융서비스">
								</a>
								<h1><span class="txt_red">금융</span> 서비스</h1>
								<div class="n_ad_service mt20">
									<dl>
										<dt class="text_left">
											<b style="text-decoration: underline; font-size: 18px;">${bizNm}</b><b style="color: #666666;">&nbsp;님이</b><br>
											<b style="font-size: 22px;">지금 지원받을 수 있는</b><br>
											<span class="txt_666" style="font-size: 18px;">금융한도</span>
										</dt>
										<dt class="text_left mt10" style="display: block;">
											<span class="n_tip_link" style="position: revert;">
												<img src="/assets/images/main/tip_Icon.png" class="mb02" alt="">
											</span>
											<span style="color: #666666; font-size: 12px;">내게 딱 맞는 금융상품은? /</span>
											<a href="/sub/pre_calculate/pre_calculate">
												<span style="color: #009ACA; font-size: 12px;">
													&nbsp;‘금융 서비스’ 전체보기(클릭)
												</span>
											</a>
										</dt>
										<dd class="fl_right" style="position: absolute; right: 1.5rem; bottom: 5rem;">
											최대&nbsp;
											<b>
												<!-- 
													20211012 span 태그가 fmt 한줄 밑으로 소스 정리를 하게 되면 XXXXX 원 이렇게 되여 버림. 
													=> 기획팀에서 XXXXX원으로 요청함.
												-->
												<fmt:formatNumber value="${dashboardInfo.pre_sett}" pattern="#,###" /><span class="txt_won">원</span>
											</b>
										</dd>
									</dl>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="n_card_main n_line2" style="background-color: #753FA7; border: 1px solid #DDDDDD;">
								<a href="/sub/pre_calculate/pre_calculate" class="more_info_btn">
									<img src="/assets/images/main/vtn_viewmore.png" alt="금융서비스">
								</a>
								<h1><span class="txt_fff">금융</span><span style="color: #CCCCCC;">&nbsp;서비스</span></h1>
								<div class="n_ad_service mt20">
									<dl>
										<dt class="text_left">
											<b class="txt_fff" style="text-decoration: underline; font-size: 18px;">${bizNm}</b><b style="color: #CCCCCC;">&nbsp;님이</b><br>
											<b class="txt_fff" style="font-size: 22px;">지금 지원받을 수 있는</b><br>
											<span style="color: #CCCCCC; font-size: 18px;">금융한도</span><br>
										</dt>
										<dt class="text_left mt10" style="display: block;">
											<span class="n_tip_link" style="position: revert;">
												<img src="/assets/images/main/tip_Icon.png" class="mb02" alt="">
											</span>
											<span class="txt_fff" style="font-size: 12px;">‘미니쇼핑몰론(비대면)’ 추천 /</span>
											<a href="/sub/pre_calculate/detail?finGoodSeqNo=21">
												<span style="color: #00FFFF; font-size: 12px;">
													&nbsp;지원 받아보기(클릭)
												</span>
											</a>
										</dt>
										<dd class="fl_right txt_fff" style="position: absolute; right: 1.5rem; bottom: 5rem;">
											최대 <b>5,000,000<span class="txt_won">원</span></b>
										</dd>
									</dl>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
					<!-- 금융서비스 End -->
				</div>
				<!-- 셀러봇캐시 예보 소식 Start -->
				<div class="layout_sm">
					<div class="card_center">
					</div>
				</div>
				<!-- 셀러봇캐시 예보 소식 End -->
				<div class="layout_md2">
					<div class="n_line3 fl_wrap mb05">
						<!-- 오늘 받는 정산금 Start -->
						<div class="n_card_main n_card_half fl_left">
							<span class="n_ico_new"><img src="../../assets/images/main/icon_new.png" alt="new"></span>
							<a href="#" class="more_info_btn" onclick="pop_today(); return false;"><img
									src="../../assets/images/main/vtn_viewmore.png" alt=""></a>
							<h2>오늘 받는 정산금<span class="txt_s12 block">(오픈마켓+비오픈마켓)</span></h2>
							<p class="today_cash">
                                <c:choose>
                                    <c:when test="${empty dashboardInfo.today_sett_acc.today_sett_acc_prc_sum or dashboardInfo.today_sett_acc.today_sett_acc_prc_sum eq 0}">
                                        <b class="txt_000">-</b>
                                    </c:when>
                                    <c:otherwise>
                                        <b class="txt_000">
                                            <fmt:formatNumber value="${dashboardInfo.today_sett_acc.today_sett_acc_prc_sum}"
                                                pattern="#,###" /></b><span class="txt_won">원</span>
                                    </c:otherwise>
                                </c:choose>    
							</p>
						</div>
						<!-- 오늘 받는 정산금 End -->
						<!-- 판매몰예치금 잔액 Start -->
						<div class="n_card_main n_card_half fl_right">
							<span class="n_ico_new"><img src="../../assets/images/main/icon_new.png" alt="new"></span>
							<a href="#" class="more_info_btn" onclick="pop_store_cash(); return false;"><img
									src="../../assets/images/main/vtn_viewmore.png" alt=""></a>
							<h2>판매몰 예치금 잔액</h2>
							<p class="today_cash"><b class="txt_000">
									<fmt:formatNumber value="${dashboardInfo.cyb_mony.cyb_mony_sum}" pattern="#,###" /></b><span
									class="txt_won">원</span></p>
						</div>
						<!-- 판매몰예치금 잔액 End -->
					</div>

					<!-- //20200706_10 :: 자동 대사 현황 / 계좌 조회 현황 Start -->
					<div class="n_card_main n_line2 mb05 card_slide">
						<div class="n_tab_box">

							<!-- 탭 헤더 Start -->
							<c:choose>
								<c:when test="${!empty recoInfo}">
									<div class="n_tab_header col_2">
										<div class="n_tab_button is_active">
											<span class="label" data-target="automated_status">자동 대사 현황</span>
											<a href="/sub/account/reconcile" class="more_info_btn">
												<img src="/assets/images/main/vtn_viewmore.png" alt="">
											</a>
										</div>
										<div class="n_tab_button">
											<span class="label" data-target="account_status">계좌 조회 현황</span>
											<a href="/sub/account/account" class="more_info_btn">
												<img src="/assets/images/main/vtn_viewmore.png" alt="">
											</a>
										</div>
									</div>
								</c:when>
								<c:otherwise>
									<div class="n_tab_header full">
										<div class="n_tab_button">
											<span class="label" data-target="account_status" style="cursor:default">계좌 조회 현황</span>
											<a href="/sub/account/account" class="more_info_btn">
												<img src="/assets/images/main/vtn_viewmore.png" alt="">
											</a>
										</div>
									</div>
								</c:otherwise>
							</c:choose>
							<!-- 탭 헤더 End -->

							<!-- 자동 대사 현황 Start -->
							<c:if test="${!empty recoInfo}">
								<div class="n_tab_content automated_status">
									<!-- <div class="n_main_status fl_wrap">
										<ul>
											<c:if test="${!empty recoInfo.reco_info && fn:length(recoInfo.reco_info) == 0}">
												<li class="unconfirmed">
													<span class="label">미확인</span>
													<span class="count"><fmt:formatNumber value="${recoInfo.ident_dep_cnt}" pattern="#,###" />건</span>
												</li>
												<li class="auto">
													<span class="label">자동</span>
													<span class="count"><fmt:formatNumber value="${recoInfo.unident_dep_cnt}" pattern="#,###" />건</span>
												</li>
											</c:if>
										</ul>
									</div> -->
									<div class="slide_automatedBox">
										<div class="swiper-container slide_automated">
											<div class="swiper-wrapper">
												<c:choose>
													<c:when test="${fn:length(recoInfo.reco_info) == 0}">
														<div class="no_data">
															<div class="n_bg_gray_box">
																<ul class="n_tab_list_box">
																	<li>
																		조회된 데이터가 없습니다.
																	</li>
																</ul>
															</div>
														</div>
													</c:when>
													<c:otherwise>
														<c:set var="recoListLength" value="${fn:length(recoInfo.reco_info)}"/>
														<c:forEach begin="0" end="${fn:length(recoInfo.reco_info)}" step="3" var="list" varStatus="status">
															<c:if test="${status.index <= recoListLength-1}">
																<div>
																	<div class="n_bg_gray_box">
																		<ul class="n_tab_list_box">
																			<c:forEach begin="0" end="2" step="1" varStatus="subStatus">
																				<c:if test="${!empty recoInfo.reco_info[status.index+subStatus.index].reco_sts_cd}">
																					<c:choose>
																						<c:when test="${recoInfo.reco_info[status.index+subStatus.index].reco_sts_cd eq 'IDEP'}">
																							<li class="auto">
																						</c:when>
																						<c:when test="${recoInfo.reco_info[status.index+subStatus.index].reco_sts_cd eq 'MDEP'}">
																							<li class="unconfirmed">
																						</c:when>
																						<c:otherwise>
																							<li>
																						</c:otherwise>
																					</c:choose>
																						<span class="col col_1" title="${recoInfo.reco_info[status.index+subStatus.index].tra_dt}">${recoInfo.reco_info[status.index+subStatus.index].tra_dt}</span>
																						<span class="col col_2" title="${recoInfo.reco_info[status.index+subStatus.index].bank_nm}">${recoInfo.reco_info[status.index+subStatus.index].bank_nm}</span>
																						<span class="col col_3" title="${recoInfo.reco_info[status.index+subStatus.index].acct_no}">${recoInfo.reco_info[status.index+subStatus.index].acct_no}</span>
																						<span class="col col_4" title="<fmt:formatNumber value="${recoInfo.reco_info[status.index+subStatus.index].depo_prc}" pattern="#,###" />원"><fmt:formatNumber value="${recoInfo.reco_info[status.index+subStatus.index].depo_prc}" pattern="#,###" />원</span>
																						<span class="col col_5" title="${recoInfo.reco_info[status.index+subStatus.index].acct_tra_cont}">${recoInfo.reco_info[status.index+subStatus.index].acct_tra_cont}</span>
																						<span class="col col_6" title="${recoInfo.reco_info[status.index+subStatus.index].reco_sts_cd_nm}">${recoInfo.reco_info[status.index+subStatus.index].reco_sts_cd_nm}</span>
																					</li>
																				</c:if>
																			</c:forEach>
																		</ul>
																	</div> 
																</div>
															</c:if>
														</c:forEach>
													</c:otherwise>
												</c:choose>
											</div>
										</div>
									</div>
									<div class="n_tip_link">
										<a href="/sub/account/account"><img src="/assets/images/main/tip_Icon.png"
												alt=""><span>계좌 등록/수정 </span>바로가기 &gt;</a>
									</div>
									<c:if test="${fn:length(recoInfo.reco_info) != 0}">
										<div class="update_info_box">
											<a class="icon_update"><img src="/assets/images/main/refresh.png" alt=""
													style="vertical-align: baseline;"></a>
											<p>마지막 업데이트일자 <c:if test="${recoInfo.reco_info[0].last_update_dt ne ''}">
													${recoInfo.reco_info[0].last_update_dt}</c:if>
											</p>
										</div>
									</c:if>
								</div>
							</c:if>
							<!-- 자동 대사 현황 End -->

							<!-- 계좌 조회 현황 Start -->
							<c:if test="${!empty recoInfo}"><div class="n_tab_content account_status" style="display: none;"></c:if>
							<c:if test="${empty recoInfo}"><div class="n_tab_content account_status"></c:if>
								<div class="n_main_status fl_wrap">
									<ul>
										<li class="error">오류 ${dashboardInfo.cust_acct.err_acct_cnt }</li>
										<li class="warning">점검 ${dashboardInfo.cust_acct.ins_acct_cnt}</li>
										<li class="normal">정상 ${dashboardInfo.cust_acct.nor_acct_cnt }</li>
										<li class="all">전체 ${dashboardInfo.cust_acct.all_acct_cnt - dashboardInfo.cust_acct.del_acct_cnt }</li>
									</ul>
								</div>
								<div class="fl_wrap">
									<p class="bullet_txt fl_left"><span></span>총 잔액</p>
									<p class="fl_right txt_s24"><b class="price">
											<fmt:formatNumber value="${dashboardInfo.cust_acct.total_price}" pattern="#,###" /></b><span
											class="txt_won">원</span></p>
								</div>
								<!--계좌 조회 현황 type1)가입 후 계좌등록 전-->
								<div class="n_tip_link">
									<a href="/sub/account/account"><img src="/assets/images/main/tip_Icon.png"
											alt=""><span>계좌 등록/수정 </span>바로가기 &gt;</a>
								</div>
								<c:if test="${(dashboardInfo.cust_acct.all_acct_cnt - dashboardInfo.cust_acct.del_acct_cnt) != 0 }">
									<!--계좌 조회 현황 type2)가입 후 계좌등록 완료-->
									<div class="update_info_box">
										<a class="icon_update"><img src="/assets/images/main/refresh.png" alt=""
												style="vertical-align: baseline;"></a>
										<p>마지막 업데이트일자 <c:if test="${dashboardInfo.cust_acct.last_update_dt ne ''}">
												${dashboardInfo.cust_acct.last_update_dt }</c:if>
										</p>
									</div>
								</c:if>
							</div>
							<!-- 계좌 조회 현황 End -->

						</div>					
					</div>
					<!-- //20200706_10 :: 자동 대사 현황 / 계좌 조회 현황 End -->

					<!-- 판매몰 조회 현황 Start -->
					<div class="n_card_main n_line4 card_slide">
						<a href="/sub/my/join/market" class="more_info_btn"><img src="/assets/images/main/vtn_viewmore.png"
								alt=""></a>
                        <h1 style="font-size: 18px;">판매몰 조회 현황</h1>
						<div class="n_main_status fl_wrap">
							<ul>
								<li class="error">오류 ${dashboardInfo.cust_mall.err_mall_cnt}</li>
								<li class="warning">점검 ${dashboardInfo.cust_mall.ins_mall_cnt}</li>
								<li class="normal">정상 ${dashboardInfo.cust_mall.nor_mall_cnt}</li>
								<li class="all">전체 ${dashboardInfo.cust_mall.all_mall_cnt}</li>
							</ul>
						</div>
						<c:choose>
							<c:when test="${dashboardInfo.cust_mall.err_mall_cnt == 0}">
								<div class="slide_accountBox">
									<div class="swiper-container slide_account">
										<div class="swiper-wrapper">
											<div class="swiper-slide">
												<div class="bg_gray_box alone">
													<p class="msg_normal_account">
														모든 판매몰이 <span>정상조회</span> 되고있습니다.
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-button-prev mall-button-prev"></div>
									<div class="swiper-button-next mall-button-next"></div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="slide_accountBox">
									<div class="swiper-container slide_account">
										<div class="swiper-wrapper">
											<c:set var="mallLength" value="${fn:length(dashboardInfo.cust_mall.error_mall_list)}" />
											<c:set var="idx" value="0" />
											<c:forEach begin="0" end="${fn:length(dashboardInfo.cust_mall.error_mall_list)}" step="2" var="list"
												varStatus="status">
												<c:if test="${status.index <= mallLength-1}">
													<div class="swiper-slide">
														<div class="n_bg_gray_box">
															<div class="item_inquiry">
																<p class="lb word-block" style="width: 55%">
																	${dashboardInfo.cust_mall.error_mall_list[status.index].mall_nm }<c:if
																		test="${ !empty dashboardInfo.cust_mall.error_mall_list[status.index].mall_cert_1st_id }">
																		(${dashboardInfo.cust_mall.error_mall_list[status.index].mall_cert_1st_id})
																	</c:if>
																</p>
																<p class="status_inquiry status_inquiry_2 error again_chk"
																	data-cust_mall_seq_no="${dashboardInfo.cust_mall.error_mall_list[status.index].cust_mall_seq_no }"
																	data-title="${dashboardInfo.cust_mall.error_mall_list[status.index].scra_err_cd_desc }">
																	${empty dashboardInfo.cust_mall.error_mall_list[status.index].scra_err_cd_nm ?
																	dashboardInfo.cust_mall.error_mall_list[status.index].cust_mall_sts_cd_nm :
																	dashboardInfo.cust_mall.error_mall_list[status.index].scra_err_cd_nm }</p>
															</div>

															<div class="item_inquiry last">
																<p class="lb word-block" style="width: 55%">
																	${dashboardInfo.cust_mall.error_mall_list[status.index+1].mall_nm }<c:if
																		test="${ !empty dashboardInfo.cust_mall.error_mall_list[status.index+1].mall_cert_1st_id }">
																		(${dashboardInfo.cust_mall.error_mall_list[status.index+1].mall_cert_1st_id})
																	</c:if>
																</p>
																<p class="status_inquiry status_inquiry_2 error again_chk"
																	data-cust_mall_seq_no="${dashboardInfo.cust_mall.error_mall_list[status.index+1].cust_mall_seq_no }"
																	data-title="${dashboardInfo.cust_mall.error_mall_list[status.index+1].scra_err_cd_desc }">
																	${empty dashboardInfo.cust_mall.error_mall_list[status.index+1].scra_err_cd_nm
																	? dashboardInfo.cust_mall.error_mall_list[status.index+1].cust_mall_sts_cd_nm
																	: dashboardInfo.cust_mall.error_mall_list[status.index+1].scra_err_cd_nm }</p>
															</div>
															<div class="n_pop_chk n_pop_chk_2">
																<p><a class="n_pop_chk_link" href="/sub/my/join/market">링크입니다
																		문안을 작성해주세요.</a></p>
																<span class="n_pop_chk_close"><img
																		src="/assets/images/main/main_close_btn.png"
																		alt="닫기" /></span>
															</div>
														</div>
													</div>
												</c:if>
												<c:set var="idx" value="${idx + 1}" />
											</c:forEach>
										</div>
									</div>
									<div class="swiper-button-prev mall-button-prev"></div>
									<div class="swiper-button-next mall-button-next"></div>
								</div>
							</c:otherwise>
                        </c:choose>
						<div class="n_tip_link">
							<img src="/assets/images/main/tip_Icon.png" alt=""> <a
								href="/sub/my/join/market"><span>판매몰 등록/수정 </span>바로가기 &gt;</a>
						</div>
						<c:if test="${dashboardInfo.cust_mall.err_mall_cnt != 0}">
							<!--계좌 조회 현황 type2)가입 후 계좌등록 완료-->
							<div class="update_info_box">
								<a class="icon_update"><img src="/assets/images/main/refresh.png" alt=""
										style="vertical-align: baseline;"></a>
									<p>마지막 업데이트일자 <c:if test="${dashboardInfo.cust_mall.last_update_dt ne ''}">
											${dashboardInfo.cust_mall.last_update_dt }</c:if>
									</p>
							</div>
						</c:if>
					</div>
					<!-- 판매몰 조회 현황 End -->
					
					<!-- s: 메인 스카이 배너-->
					<c:if test="${!empty bannerDataList}">
						<div class="main_bnr">
						<c:forEach items="${bannerDataList}" varStatus="status" var="data">
							<c:set var="imgPath" value="${data.stor_path}${data.stor_file_nm}"></c:set>
							<!--  //외부 페이이 호출시 새창 => http 사용 -->
							<c:set var="openTarget" value="_self"></c:set>
							<c:if test="${fn:indexOf(data.link_url, 'http') > -1}">
								<c:set var="openTarget" value="_blank"></c:set>
							</c:if>
							<a href="${data.link_url}" target="${openTarget}">
								<img src="/imagefile/${imgPath}" alt="" />
							</a>
						</c:forEach>
						</div>
					</c:if>
					<!-- e: 메인 배너-->

				</div>
				<div class="layout_bt mt10 fl_wrap">
					<!-- 매출 통계 Start -->
					<div class="layout_bt_l fl_wrap">
						<div class="n_card_main n_line4 n_card_half">
							<a href="/sub/sales/sales" class="more_info_btn"><img
									src="/assets/images/main/vtn_viewmore.png" alt=""></a>
							<h1>매출 통계</h1>
							<div class="bt_stats">
                                <c:forEach var="sales" items="${dashboardInfo.sales_list}">
                                    <dl class="fl_wrap">
                                        <dt><b>${sales.sales_dt}</b><br><span>전월 대비</span></dt>
                                        <c:choose>
                                            <c:when test="${empty sales.sales_prc }">
                                                <dd><b>0</b><span class="txt_won">원</span><br>
                                                    <span class="gap">-&nbsp;&nbsp;&nbsp;</span></dd>
                                            </c:when>
                                            <c:when test="${sales.last_month_per_sales_prc >= 0}">
                                                <dd class="accounts_plus"><b>
                                                    <fmt:formatNumber
                                                        value="${sales.sales_prc}"
                                                        pattern="#,###" /></b><span class="txt_won">원</span><br>
                                                <span class="gap">+
                                                    <fmt:formatNumber
                                                        value="${sales.last_month_per_sales_prc}"
                                                        pattern="#,###" />원</span>
                                            </dd>
                                            </c:when>
                                            <c:otherwise>
                                                <dd class="accounts_minus"><b>
                                                        <fmt:formatNumber
                                                            value="${sales.sales_prc}"
                                                            pattern="#,###" /></b><span class="txt_won">원</span><br>
                                                    <span class="gap">-
                                                        <fmt:formatNumber
                                                            value="${sales.last_month_per_sales_prc * -1}"
                                                            pattern="#,###" />원</span>
                                                </dd>
                                            </c:otherwise>
                                        </c:choose>
                                    </dl>
                                </c:forEach>                                
							</div>
						</div>
						<!-- 매출 통계 End -->
						<!-- 과거 정산금 통계 Start -->
						<div class="n_card_main n_line4 n_card_half fl_right">
							<a href="/sub/past/past" class="more_info_btn"><img
									src="/assets/images/main/vtn_viewmore.png" alt=""></a>
							<h1>과거 정산금 통계</h1>
							<div class="bt_stats">
                                <c:forEach var="settAcc" items="${dashboardInfo.sett_acc_list}">
                                    <dl class="fl_wrap">
                                        <dt><b>${settAcc.sett_acc_dt}</b><br><span>전월 대비</span></dt>
                                        <c:choose>
                                            <c:when test="${empty settAcc.sett_acc_prc }">
                                                <dd><b>0</b><span class="txt_won">원</span><br>
                                                    <span class="gap">-&nbsp;&nbsp;&nbsp;</span></dd>
                                            </c:when>
                                            <c:when test="${settAcc.last_month_per_sett_acc_prc >= 0}">
                                                <dd class="accounts_plus"><b>
                                                    <fmt:formatNumber
                                                        value="${settAcc.sett_acc_prc}"
                                                        pattern="#,###" /></b><span class="txt_won">원</span><br>
                                                <span class="gap">+
                                                    <fmt:formatNumber
                                                        value="${settAcc.last_month_per_sett_acc_prc}"
                                                        pattern="#,###" />원</span>
                                            </c:when>
                                            <c:otherwise>
                                                <dd class="accounts_minus"><b>
                                                        <fmt:formatNumber
                                                            value="${settAcc.sett_acc_prc}"
                                                            pattern="#,###" /></b><span class="txt_won">원</span><br>
                                                    <span class="gap">-
                                                        <fmt:formatNumber
                                                            value="${settAcc.last_month_per_sett_acc_prc * -1}"
                                                            pattern="#,###" />원</span>
                                                </dd>
                                            </c:otherwise>
                                        </c:choose>
                                    </dl>
                                </c:forEach>
							</div>
						</div>
						<!-- 과거 정산금 통계 End -->
					</div>
					<div class="layout_bt_r fl_right fl_wrap">
						<!-- 반품 통계 Start -->
						<div class="n_card_main n_line5 n_card_half">
							<a href="/sub/return/return" class="more_info_btn"><img
									src="../../assets/images/main/vtn_viewmore.png" alt=""></a>
							<h1>반품 통계</h1>
							<div class="bt_return mt20">
								<p class="bullet_txt fl_wrap"><span></span>지난달 반품률</p>
								<dl>
									<dt><b>${dashboardInfo.rtn_info.rtn_dt}</b></dt>
									<dd>
                                        <b>${dashboardInfo.rtn_info.last_month_rtn_num_rto }</b><span class="txt_won">%</span>
									</dd>
								</dl>
							</div>
						</div>
						<!-- 반품 통계 End -->
						<!-- 점프 서비스 Start -->
						<div class="n_card_main n_line5 n_card_half fl_right bg_yllw">
							<span class="n_ico_new"><img src="../../assets/images/main/icon_new.png" alt="new"></span>
							<a href="/sub/jump/service" class="more_info_btn"><img
									src="../../assets/images/main/vtn_viewmore_w.png" alt=""></a>
							<h1 class="txt_000">점프 서비스</h1>
							<div class="bt_jump">
								<p>
									<a href="/sub/jump/service#1" class="jump_store">판매몰</a>
									<a href="/sub/jump/service#2" class="jump_bank">금융사</a>
									<a href="/sub/jump/service#3" class="jump_care">쇼핑몰관리</a>
									<a href="/sub/jump/service#4" class="jump_domai">매입채널</a>
									<a href="/sub/jump/service#5" class="jump_public">관공서</a>
								</p>
							</div>
						</div>
						<!-- 점프 서비스 End -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 이벤트 모달 Start -->
<c:if test="${fn:length(dashboardInfo.event_list) > 0}">
	<div class="modal modal_event" style="display: none;">
		<div class="open_event_modal">
			<!-- <span class="event_box_header">
				<input type="button" class="event_close_btn">
			</span> -->
			<div class="event_text_box">
				<img class="event_close_btn" src="/assets/images/main/event_close_btn.png" alt="닫기">	
				<c:out value="${dashboardInfo.event_list[0].event_cont}" escapeXml="false"></c:out>
			</div>
			<div class="manage_modal_box">
				<!--
				<input type="checkbox" class="day_close" id="dayClose2">
				<label for="dayClose2">오늘 하루 보지 않고 닫기</label>
				-->
				<p class="day_close">오늘 하루 보지 않고 닫기</p> 
				<p class="close_chk_modal">닫기</p>
			</div>
		</div>
	</div>
</c:if>
<!-- 이벤트 모달 End -->

<!-- SC제일 은행 이벤트 팝업창 start -->
<style type="text/css">
	
	*:focus { outline: none;}
 
 	/* 2021-05.26 미니 쇼핑몰론 버전 */
	.modal_sc{position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);z-index: 9006; background: #fff;}
	.popup_sc .popup_sc_mobile{display:none;}
	.popup_sc .top_close{position:absolute;top:10px;right:10px;}
	.popup_footer_left {position:absolute;bottom:20px; left:10px; cursor: pointer;}	
	.popup_footer_right {position:absolute;bottom:20px; right:10px; cursor: pointer;}	
	.popup_sc button.btn_close{width:48px;height:25px;font-size:12px;color:#fff;text-align: center;background:#598ae2; border: 0;border-radius: 16px;cursor: pointer; margin-top:2px;}
	
	@media screen and (max-width: 670px){
		.popup_sc{width: 75%;text-align:center;max-height: 99%;}
		.popup_sc .popup_sc_mobile{display:block;}
		.popup_sc .popup_sc_web{display:none;}	
 		.modal_sc img{max-width:99%;height:auto; }
	}

</style>

<c:if test="${isExternalPopUp}">
<div class="modal_sc popup_sc" id="modal_sc_0" style="display: none;"> 
	<div class="popup_sc_web">
		<img src="/assets/images/scBank/partnersloan/popup_web_sc01.jpg" usemap="#popup_sc_web1">
		<map name="popup_sc_web1">
		    <area target="_blank" alt="이벤트 참여하기" title="이벤트 참여하기" href="https://www.standardchartered.co.kr/np/kr/cm/et/EventOngoingView.jsp?EVNT_ID=1913" coords="133,577,362,636" shape="rect">
		</map>			
	</div>
	<div class="popup_sc_mobile">
		<img src="/assets/images/scBank/partnersloan/popup_m_sc01.jpg" usemap="#popup_sc_mobile1">
		<map name="popup_sc_mobile1">
    		<area target="_blank" shape="rect" coords="47,305,225,343" href="https://www.sc.co.kr/url/bb/ptnsl">
		</map>
	</div>
	<div class="popup_footer" style="width:100%;">
		<span style="float:left;"><input type="checkbox" class="sc_day_close" data-popup-id="modal_sc_0" > 오늘은 더이상 보지않습니다.</span>
		<button class="btn_close close_sc_modal" style="float:right; padding-bottom:2px;">close</button>
	</div>
</div>
</c:if>
<!-- SC제일 은행 이벤트 팝업창 end -->

<!-- 유료개편예정공지팝업 start -->
<!-- <style type="text/css">
	
	*:focus { outline: none;}
 
	.modal_paid_notice{position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);z-index: 9006; background: #fff;}
	.popup_paid_notice .popup_paid_notice_mobile{display:none;}
	.popup_paid_notice .popup_paid_notice_web{display:block;}
	
	@media screen and (max-width: 670px){
		.modal_paid_notice{position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);z-index: 9006; background: #fff;}
		.popup_paid_notice .popup_paid_notice_mobile{display:block;}
		.popup_paid_notice .popup_paid_notice_web{display:none;}
	}

</style>

<div class="modal_paid_notice popup_paid_notice" id="modal_paid_notice_0" style="display: none;"> 
	<div class="popup_paid_notice_web">
		<img src="/assets/images/notice/paidProductNotice_web.png" usemap="#popup_paid_notice_web1">
		<map name="popup_paid_notice_web1">
		    <area shape="rect" alt="X" title="" coords="414,5,441,32" class="close_paid_modal" href="#"/>
			<area shape="rect" alt="닫기" title="" coords="388,398,432,417" class="close_paid_modal" href="#"/>
			<area shape="rect" alt="자세히보기" title="" coords="117,346,330,377" class="go_page_paid" href="#" data-go-page-paid="https://www.sellerbot.co.kr/pub/cs/notice?noti_seq_no=374" data-popup-id="modal_paid_notice_0"/>
		</map>			
	</div>
	<div class="popup_paid_notice_mobile">
		<img src="/assets/images/notice/paidProductNotice_mod.png" usemap="#popup_paid_notice_mobile1">
		<map name="popup_paid_notice_mobile1">
    		<area shape="rect" alt="X" title="" coords="395,9,414,27" class="close_paid_modal" href="#"/>
			<area shape="rect" alt="닫기" title="" coords="374,398,404,416" class="close_paid_modal" href="#"/>
			<area shape="rect" alt="자세히보기" title="" coords="107,345,320,378" class="go_page_paid" href="#" data-go-page-paid="https://www.sellerbot.co.kr/pub/cs/notice?noti_seq_no=374" data-popup-id="modal_paid_notice_0"/>
		</map>
	</div>
</div> -->
<!-- 유료개편예정공지팝업 end -->

<!-- S:제3자 정보제공 동의 안내 팝업 20210310 -->
<div class="new_popup_wrap" id="it3_popup" style="display:none;">
	<div class="display_none_back close-display-01"></div>
	<div class="pop_up_1 pop_up_all">
		<div class="popup_top_title">
			<h1>${IT3TermsTitle}</h1>
			<div class="add_pooup_close" 
				data-close-class-id="new_popup_wrap" 
				data-close-sub-class-id="pop_up_1"
				data-close-display-class-id="close-display-01"
			>
                <div></div>
                <div></div>
            </div>
		</div>
		<h1 class="popup_title_2">서비스를 이용하시려면 약관동의가 필요합니다.</h1>
		<div class="pop_up_text_wrap">
			<h1>
				${IT3TermsCont}
			</h1>
		</div>
		<div class="popup_footer">
			<input type="checkbox" class="chkbox" name="chkbox_1" id="joinAll">
			<label for="joinAll"> 제3자 정보제공에 동의합니다.</label>
			<span class="error_popup error_popup_1_1">제3자 정보제공 동의를 하셔야 이용이 가능합니다.</span>
		</div>
		<button type="button" class="btn_confirm_terms popup_1_btn popup_1_btn_none">서비스 신청</button>
	</div>
</div>
<!-- E:제3자 정보제공 동의 안내 팝업 20210310 -->

<!-- S:금융 상품 판매몰 등록 안내 팝업 20210310 -->
<div class="popup_account guide_popup_txt type10">
	<div class="pop_account">
		<span class="pop_close"><img src="/assets/images/member/close_gray.png" alt="닫기"></span>
		<div class="pop_body pt30">
			
			<div class="guide_popup_txt_bg">
			</div>
		</div>
		<div class="pop_foot fl_wrap">
			<button type="button" class="frm_account_btn">판매몰 등록하러 가기</button>
		</div>
	</div>
</div>
<!-- E:금융 상품 판매몰 등록 안내 팝업 20210310 -->

<!-- S:셀러봇캐시 마케팅 활용 동의 팝업 -->
<div class="new_popup_wrap" name="smt_popup_wrap" id="smt_popup" style="display: none;">
	<div class="display_none_back close-display-02"></div>
	<div class="pop_up_1 pop_up_info_update">
		<div class="popup_top_title">
			<h1>회원정보 업데이트 안내</h1>
			<div class="add_pooup_close" 
				data-close-class-id="new_popup_wrap2" 
				data-close-sub-class-id="pop_up_2"
				data-close-display-class-id="close-display-02"
			>
				<div></div>
				<div></div>
			</div>
		</div>
		<div class="box">
			<div class="outer flex">
				<div class="inner flex">
					<p>대표자명</p>
					<input type="text" id="ceo_nm" value="${cust.ceo_nm}">
				</div>
				<div class="inner flex">
					<p>대표자 연락처</p>
					<input type="text" id="ceo_no" value="${cust.ceo_no}">
				</div>
			</div>
			<div class="outer flex">
				<p class = "text_error" style="color: red;font-size: 11px;"></p>
			</div>
			<span class="line"></span>
			<div class="borderBox" id="marketingTerm">
				<div class="title">
					<p>${SMTTermsTitle}(${SMTTermsEsseYn == 'Y' ? '필수':'선택' })</p>
					<button termid="${SMTTermsId}" class="link_agree">상세보기</button>
				</div>
				<div class="row flex">
					<div class="radioDiv">
						<input type="radio" class="smtchkbox" name="chkbox_2" id="smtchkbox">
						<label class="textSpan" for="agree">동의</label>
					</div>
					<div class="radioDiv">
						<input type="radio" class="smtunchkbox" name="chkbox_2" id="smtunchkbox">
						<label class="textSpan" for="disagree">미동의</label>
					</div>
				</div>
				<span class="line"></span>
				<div class="row">
					<c:forEach var="smtTermsItem" items="${smtTermsList}">
					<div class="checkBoxDiv">
						<input type="checkbox" class="smtItemChkbox" id="smtTermsAgree${smtTermsItem.com_cd}" 
							name="smtTerm"
							value="${smtTermsItem.com_cd}"smt_aggr_item_seq_no
							<c:forEach var="smtAgreeItem" items="${smtAgreeItemList}">
								<c:if test="${smtAgreeItem.smt_terms_cd == smtTermsItem.com_cd}">
									checked
								</c:if>
							</c:forEach>
						>
						<label for="smtTermsAgree${smtTermsItem.com_cd}">
							<label class="textSpan" for=""><c:out value="${smtTermsItem.cd_nm}" /></label>
						</label>
					</div>
				</c:forEach>	
				</div>
			</div>
			<div class="bottom">
				<div class="checkBoxDiv">
					<input type="checkBox" class="chkBox" id="hideToday" data-popup-id="modal_smt_0">
					<label class="checkBox" for="hideToday"></label>
					<label class="textSpan" for="hideToday">오늘 하루 보지 않기</label>
				</div>
			</div>
		</div>
		<button type="button" class="btn_mark_confirm_terms"
			data-close-class-id="new_popup_wrap2" 
			data-close-sub-class-id="pop_up_2"
			data-close-display-class-id="close-display-02">확인</button>
	</div>
</div> 
<!-- E:셀러봇캐시 마케팅 활용 동의 팝업 -->

<!-- S:셀러봇 캐시 마케팅 약관 내용 정보 -->
<!--팝업-->
<div class="new_popup_wrap" id="smt_guide_popup" style="display: none;">
	<div class="display_none_back close-display-03"></div>
	<div id="termpopup${SMTTermsId}" class="pop_up_1">
		<div class="popup_top_title">
			<h1>
				[${SMTTermsEsseYn == 'Y' ? '필수':'선택' }]
				<c:out value="${SMTTermsTitle }" />
			</h1>
			<div class="add_pooup_close" 
				data-close-class-id="new_popup_wrap3" 
				data-close-sub-class-id="pop_up_3"
				data-close-display-class-id="close-display-03"
			>
				<div></div>
				<div></div>
			</div>
		</div>
		<div class="pop_up_text_wrap">
			${sf:textToHtml(SMTTermsCont)}
		</div>
		<button type="button" class="btn_close"
			data-close-class-id="new_popup_wrap3" 
			data-close-sub-class-id="pop_up_3"
			data-close-display-class-id="close-display-03">약관확인</button>
	</div>
</div>
<!-- E:셀러봇 캐시 마케팅 약관 내용 정보 -->
<style type="text/css">

	.btn_mark_confirm_terms {
		color: #ffffff;
		font-size: 1em;
		background: #009aca;
		padding: 1.2em 0;
		width: 100%;
		font-size: 1rem;
	}

	.btn_close {
		color: #ffffff;
		font-size: 1em;
		background: #009aca;
		padding: 1.2em 0;
		width: 100%;
		font-size: 1rem;
		}

	.guide_popup_txt{
		max-width: 450px;
		left: 40%;
		}
	.guide_popup_txt_bg{
		width:385px;height:296px;
		font-size: 22px;
		color:#fff;
		text-align: center;
		font-weight: lighter;
		background: url(/assets/images/common/popup_img.png) no-repeat;
		padding-top:125px;
		margin: 0 auto -1px;
		}
		@media (max-width: 960px){
			.guide_txt{left: 50%;}
		}
		@media (max-width: 480px){
			.guide_popup_txt{left: 50%;}
			.guide_popup_txt_bg{width:95%;height:255px;font-size: 20px;background-size: 100% auto;padding-top:30%;}
		}
		@media (max-width: 400px){
			.guide_popup_txt{width: 95%;}
			.guide_popup_txt_bg{height:240px;}
		}
		@media (max-width: 300px){
			.guide_popup_txt_bg{height:190px;font-size: 16px;line-height: 20px;}
		}
</style>
<!--  E:긍융 상품 판매몰 등록 안내 팝업 20210310 -->

<script>

	var smt_popup_wrap = document.getElementsByName("smt_popup_wrap");
	var loanCustUse = '${loanCustUse}';
	var smtAgreeYN = '${smtAgreeYN}';
	var smtchkbox = document.getElementById("smtchkbox");
	var smtunchkbox = document.getElementById("smtunchkbox");
	var SMTTermsId = "${SMTTermsId}";
	var smtChkFlag = "";
	var modal_smt_0_cookie = getCookie("modal_smt_0");
	
	// 공통 script.js 이벤트 삭제
	$(".input_box .link_agree").off("click");
	// 각각 약관 마다 모달 호출
	$(".title .link_agree").click(function () {
		var termid = $(this).attr("termid");
		var targetId = $("#termpopup" + termid).parents(".new_popup_wrap").attr("id");
		$("#"+ targetId).css("display","flex");
		$("#"+ targetId).children(".pop_up_1").show();
		$("#"+ targetId).children(".display_none_back").show();

	});

	//
	$(".btn_close").click(function(){
		var target = $(this);
		var targetId = target.parents(".new_popup_wrap").attr("id");
		
		$("#"+ targetId).css("display","none");
		$("#"+ targetId).children(".pop_up_1").hide();
		$("#"+ targetId).children(".display_none_back").hide();
	});

	$(".add_pooup_close").click(function () {
		var target = $(this);
		var targetId = target.parents(".new_popup_wrap").attr("id");

		if(targetId == 'smt_popup'){
			var modalId = showConfirm("마케팅 활용 동의를 하지 않을 경우<br>일부 혜택이 제한됩니다.<br>계속 진행하시겠습니까?", function () {
				removeModal(modalId);
				$("#" + targetId).css('display','none');
				$("#" + targetId).children(".pop_up_1").hide();
				$("#" + targetId).children(".display_none_back").hide();
			});
		}else{
			$("#"+ targetId).css("display","none");
			$("#"+ targetId).children(".pop_up_1").hide();
			$("#"+ targetId).children(".display_none_back").hide();
		}
	});

	if (smtAgreeYN == 'false' && modal_smt_0_cookie != 'true') {
		$("#smt_popup").css("display", "flex");
		$("#smt_popup").children(".pop_up_1").show();
		$("#smt_popup").children(".display_none_back").show();	

		if (loanCustUse == 'true') {
			document.getElementById('ceo_nm').readOnly = true;
			document.getElementById('ceo_no').readOnly = true;
			$(".text_error").text('*금융 상품 대상 회원은 정보 수정이 제한됩니다.');
		}

		$.ajax({
			url:"/sub/pre_calculate/detail/term?termsId="+SMTTermsId,
			success:function(data){
			    if(data == 'true'){
                    smtchkbox.checked = true;
					smtunchkbox.checked = false;
                    smtChkFlag = true;
				}else if (data == 'false') {
                    smtchkbox.checked = false;
					smtunchkbox.checked = true;
                    smtChkFlag = false;
                }
			}
		});

		$(".smtchkbox").click(function () {
			$(".smtunchkbox").prop("checked", false);
            var allchk = $(this);
            
            if (allchk.is(":checked") == true) {
                $(".checkBoxDiv .smtItemChkbox").prop("checked", true);
            } else {
                $(".checkBoxDiv .smtItemChkbox").prop("checked", false);
            }

        });

		$(".checkBoxDiv .smtItemChkbox").click(function () {
			var chkThis = $(this);

			var smtItemChkCnt = chkThis.parents().find(".checkBoxDiv .smtItemChkbox:checked").length;

			if (smtItemChkCnt > 0) {
				$(".smtchkbox").prop("checked", true);
				$(".smtunchkbox").prop("checked", false);
			} else {
				$(".smtunchkbox").prop("checked", true);
				$(".smtchkbox").prop("checked", false);
			}
		});

		$(".smtunchkbox").click(function () {
			$(".smtchkbox").prop("checked", false);
            $(".checkBoxDiv .smtItemChkbox").prop("checked", false);
        });

	}

	// 마켓 동의 확인 버튼
	$(".btn_mark_confirm_terms").click(function () {
		var ceo_nm = document.getElementById("ceo_nm").value;
		var ceo_no = document.getElementById("ceo_no").value;

		var target = $(this);
		var targetId = target.parents(".new_popup_wrap").attr("id");

		var closeClassId = target.data("close-class-id");
		var closeSubClassId = target.data("close-sub-class-id");
		var closeDisplayClassId = target.data("close-display-class-id");
	
		if(!isMobile(ceo_no)){
			$(".text_error").text('*대표자 연락처를 확인해주세요.');
			return;
		}

		var hideToday = $("#hideToday").is(":checked");

		if (smtunchkbox.checked) {
			var modalId = showConfirm("마케팅 활용 동의를 하지 않을 경우<br>일부 혜택이 제한됩니다.<br>계속 진행하시겠습니까?", function () {
				removeModal(modalId);
				
				//오늘 하루 보지 않기
				$(".chkBox:checked").each(function() {
					var target = $(this);
					var popUpId = target.data("popup-id");
				
					setCookie(popUpId, true, 1);
				});

				$("#" + targetId).css('display','none');
				$("#" + targetId).children(".pop_up_1").hide();
				$("#" + targetId).children(".display_none_back").hide();
			});

			return;
		}

		//traditional : true 
		var smtChkArr = new Array();
		$(".checkBoxDiv .smtItemChkbox:checked").each(function() {
			smtChkArr.push($(this).val());
		});

		$.ajax({
			url: '/sub/my/smtTerm',
			data: { 
				ceo_nm: ceo_nm
				, ceo_no: ceo_no
				, SMTTermsId: SMTTermsId
				, smtChkArr: smtChkArr
				, smtChkFlag: smtChkFlag
			},
			traditional : true,
			async: false,
			type: 'POST',
			success: function (data) {
				$("#" + targetId).css('display','none');
				$("#" + targetId).children(".pop_up_1").hide();
				$("#" + targetId).children(".display_none_back").hide();
			},
			error: function (response) {
				if (response.responseText == "TermsErrors") {
					showAlert("마케팅 동의 정보를 확인하여 주시기 바랍니다.");
				}else {
					showAlert("저장에 실패하였습니다..");
				}
			}
		});
	});


	var eventCloseImg = document.querySelector(".event_text_box img");
	if(eventCloseImg != null) {
		eventCloseImg.addEventListener("click", function () {
			document.querySelector(".modal_event").style.display = "none";
		});
	}

	var swiper = new Swiper('.slide_automated', {
		touchRatio: 0,
		allowTouchMove: false,
		spaceBetween: 30,
		navigation: {
			nextEl: '.automated-button-next',
			prevEl: '.automated-button-prev',
		},
		breakpoints: {
			720: {
				spaceBetween: 0
			},
		},
		autoplay: {
			delay: 5000,
			disableOnInteraction: false,
		},
		loop: true,
	});

	var swiper = new Swiper('.slide_account', {
		touchRatio: 0,
		spaceBetween: 30,
		navigation: {
			nextEl: '.mall-button-next',
			prevEl: '.mall-button-prev',
		},
		breakpoints: {
			720: {
				spaceBetween: 0
			},
		}
	});

	var swiper = new Swiper('.slide_banner', {
		//touchRatio: 0,
		allowTouchMove: true,
		autoplay: {
			delay: 5000,
		},
		direction: 'vertical',
		mousewheelControl: true,
		//spaceBetween: 30,
		loop: true,
		// navigation: {
		// 	nextEl: '.swiper-button-next',
		// 	prevEl: '.swiper-button-prev',
		// },
		breakpoints: {
			720: {
				spaceBetween: 0
			},
		},
	});

	if($(".tip_list_area").find(".swiper-slide").length > 1) {
		var swiper = new Swiper('.tip_list_area', {
			autoplay: {
				delay: 3000,
			},
			direction: 'vertical',
			loop: true,
		});
	}

	/*main*/
	// 2019-07-09 디자인 변경 적용 
	// 판매몰 조회 실패 모달창
	$(".status_inquiry.again_chk").click(function () {
		var jqThis = $(this);
		var title = jqThis.data("title");
		var cust_mall_seq_no = jqThis.data("cust_mall_seq_no");
		var item_inquiry = jqThis.parent(".item_inquiry");
		$(".n_pop_chk").hide();
		var pop_chk = item_inquiry.siblings(".n_pop_chk");
		var $link = pop_chk.find(".n_pop_chk_link");
		$link.text(title);
		var url = $link.attr("href") + "?" + $.param({ "cust_mall_seq_no": cust_mall_seq_no });
		$link.attr("href", url);
		pop_chk.show();
	});

	// 판매몰 조회 현황의 에러 메시지 설명 모달 닫기
	$(".n_pop_chk_close").click(function () {
		$(".n_pop_chk").hide();
	});

	//상단 배너 삭제시 메인 패딩 css변경
	if (matchMedia("screen and (max-width: 1024px)").matches) {
		$(".close_banner").click(function () {
			$(".main_wra").css("padding", "80px 20px 3em");
		});
	}

	$(".day_close").click(function () {
		setCookie("eventPop", true, 1);
		$(".modal_event").hide();
	});

	// 이벤트 모달 닫기
	$(".close_chk_modal").click(function () {
		// if ($("#dayClose2").is(":checked")) {
		// 	setCookie("eventPop", true, 1);
		// };
		$(".modal_event").hide();
	});

	function setCookie(cookieName, value, exdays) {
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var cookieValue = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toGMTString());
		document.cookie = cookieName + "=" + cookieValue;
	}

	function getCookie(cookieName) {
		cookieName = cookieName + '=';
		var cookieData = document.cookie;
		var start = cookieData.indexOf(cookieName);
		var cookieValue = '';
		if (start != -1) {
			start += cookieName.length;
			var end = cookieData.indexOf(';', start);
			if (end == -1) end = cookieData.length;
			cookieValue = cookieData.substring(start, end);
		}
		return unescape(cookieValue);
	}

	if (!getCookie("eventPop")) {
		$(".modal_event").show();
	}

	var cnt = 2;
	var plaModalId = '';
	//외부 유입에 관련 팝업 관련
	for (var i=0; i<2 ; i++) {
		if (!getCookie("modal_pla_"+i)) {
			plaModalId = "modal_pla_"+i;
		} else {
			cnt--;
		}
	}
	
	if (cnt == 1) {
		$("#" + plaModalId).show();
		$("#" + plaModalId).css({'transform':'translate(-50%, -50%)'});
	} else if (cnt == 2) {
		$(".modal_pla").show();
		$("#modal_pla_0").css({'transform':'translate(10%, -50%)'});
		$("#modal_pla_1").css({'transform':'translate(-95%, -50%)'});
	}

	$('.inf_day_close').click(function() {
		var target = $(this);
		var popUpId = target.data("popup-id");
		
		setCookie(popUpId, true, 1);
		$(this).parents('.modal_pla').hide();
	});

	$('.close_pla_modal').click(function () {
		$(this).parents('.modal_pla').hide();
	});

	//SC이벤트 모달 팝업창 닫기
	$('.close_sc_modal').click(function () {
		
		$(this).parents('.modal_sc').hide();
	});
	//SC이벤트 오늘 하루 닫기
	$('.sc_day_close').click(function() {
		var target = $(this);
		var popUpId = target.data("popup-id");
		
		setCookie(popUpId, true, 1);
		$(this).parents('.modal_sc').hide();
	});

	// 20210412 SCBank 이벤트 종료
	if (!getCookie("modal_sc_0")) {
		$("#modal_sc_0").show();
	}
	
	// // 20230404 이용권 가격인상 사전 안내
	// $('.close_paid_modal').click(function () {
	// 	$(this).parents('.modal_paid_notice').hide();
	// });
	
	// $('.go_page_paid').click(function () {
	// 	var target = $(this);
	// 	var popUpId = target.data("popup-id");
	// 	var goPagePaid = target.data("go-page-paid");
		
	// 	setCookie(popUpId, true, 30);
	// 	$(this).parents('.modal_paid_notice').hide();
	// 	location.href = goPagePaid;
	// });

	// // 20230404 이용권 가격인상 사전 안내 종료
	// if (!getCookie("modal_paid_notice_0")) {
	// 	$("#modal_paid_notice_0").show();
	// }
</script>

<!-- 대시보드 탭 콘텐츠 컨트롤 -->
<script>
	(function($){
		$(document).ready(function(){

			if ('${!empty bannerMainData }' == 'true') {
				var stor_path = '${bannerMainData.stor_path}';
				var stor_file_nm = '${bannerMainData.stor_file_nm}';
				var link_url = '${bannerMainData.link_url}';

				if (stor_path != '' && stor_file_nm != '') {
					$(".card_center").attr('style', 'background: url(/imagefile/' + stor_path + stor_file_nm + ') center no-repeat; z-index: 1; ');
					if (link_url != '') {
						$(".card_center").attr('onclick', 'window.location.href="' + link_url + '"');
						$(".card_center").attr('style', 'background: url(/imagefile/' + stor_path + stor_file_nm + ') center no-repeat; z-index: 1; cursor : pointer;');
					}
				} else {
					$(".sw_bg_1").attr('style', 'background: #598ae2;');
				}
			}
			$('.n_tab_button .label').click(function(){                    
				if( !$(this).parent().hasClass('is_active') ) {
					const target = $(this).data('target'); 
					$(this).parents('.n_tab_header').find('.n_tab_button').removeClass('is_active');
					$(this).parent().addClass('is_active');
					$(this).parents('.n_tab_box').find('.n_tab_content').hide();
					$(this).parents('.n_tab_box').find('.' + target).fadeIn();
				}
			});

			//2021.03.10 금융 상품 관련 추가
			var IT3TermsId="${IT3TermsId}";
			var finMallCnt = "${dashboardInfo.cust_mall.fin_mall_cnt}";
			var infSiteCd = "${fn:toUpperCase(sessionScope.inf)}" ? "${fn:toUpperCase(sessionScope.inf)}" : "${fn:toUpperCase(sessionScope.cust.inf_site_cd)}";
			var infPathCd = "${fn:toUpperCase(sessionScope.cust.inf_path_cd)}";
			var checkInfFlag = false;
			var guidePopupTxt = '';
			// 제 3자 동의 여부 체크(1순위) 후 쇼핑몰 등록 여부 체크(2순위)
			if(IT3TermsId !="") { 
				if (infSiteCd == "SHIN") {
					checkInfFlag = true;
					guidePopupTxt = "\n\t\t\t\t신한 퀵정산을 이용하시려면<br><b>판매몰</b>을 <b>등록</b>하셔야만<br>이용이 가능합니다.\n\t\t\t";
				} else if (infSiteCd == "KBB") {
					checkInfFlag = true;
					guidePopupTxt = "\n\t\t\t\tKB매출더하기론을 이용하시려면<br><b>판매몰</b>을 <b>등록</b>하셔야만<br>이용이 가능합니다.\n\t\t\t";
				}

				if (infPathCd == "SCB") {
					checkInfFlag = true;
					guidePopupTxt = "\n\t\t\t\tSC파트너스론 서비스를 이용하시려면<br><b>판매몰</b>을 <b>등록</b>하셔야만<br>이용이 가능합니다.\n\t\t\t";
				}
			}
			
			if(checkInfFlag) {
				$.ajax({
					url:"/sub/pre_calculate/detail/term?termsId="+IT3TermsId,
					success:function(data){
						if(data=='false'){
							$("#it3_popup").css("display","flex");
							$("#it3_popup").children(".pop_up_1").show();
							$("#it3_popup").children(".display_none_back").show();
						}else if(data=='true'){
							// 판매몰 유무 체크 후 팝업창 활성화
							if (finMallCnt == 0) {
								$(".popup_account.type10").find(".pop_body .guide_popup_txt_bg").html(guidePopupTxt);
								$(".popup_account.type10").addClass("active");
							}
						}else{
							//오류 처리
							alert("제 3자 동의 여부 체크 오류");
						}
					}
				});
			}

			// 제 3자 동의 약관추가 후 쇼핑몰 등록 여부 체크
			$(".popup_1_btn_none").click(function() {
				var input_pre_chk = document.getElementsByName("chkbox_1");
				var targetId = $(".popup_1_btn_none").parents(".new_popup_wrap").attr("id");
					$("#" + targetId).css('display','none');
					$("#" + targetId).children(".pop_up_1").hide();
					$("#" + targetId).children(".display_none_back").hide();
				if (!input_pre_chk[0].checked) {
					$(".error_popup_1_1").css("display", "block");
				}
				if (input_pre_chk[0].checked) {
					$.ajax({
						url:"/sub/pre_calculate/detail/term/aggr?termsId="+IT3TermsId,
						success:function(data){
							if(data == true){
								// 판매몰 유무 체크 후 팝업창 활성화
								if (finMallCnt == 0) {
									$(".popup_account.type10").find(".pop_body .guide_popup_txt_bg").html(guidePopupTxt);
									$(".popup_account.type10").addClass("active");
								}
							}else{
								//오류 처리
								alert("제 3자 동의 추가 오류");
							}
						}
					});
				}
			});

			// 금융 상품 판매몰 등록 버튼
			$('.guide_popup_txt.type10 .frm_account_btn').click(function() {
				location.href = "/sub/my/join/market";
			});
			// 금융 상품 가이드 닫기 버튼
			$('.guide_popup_txt.type10 .pop_close').click(function() {
				$(".popup_account.type10").removeClass("active");
			})
		});
	})(jQuery);
</script>