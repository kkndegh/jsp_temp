<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">

<head>
	<meta charset="utf-8">
	<meta name="robots" content="index,follow">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport"
		content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="canonical" href="URL입력">
	<title>Sellerbot</title>
</head>

<body>
	<div class="container">
		<div class="error_wra">
			<div class="error_area">
				<div class="img_error">
					<img src="/assets/images/etc/img_error_500.png" alt="">
				</div>
				<div class="text_error">
					<p class="error_type">ERROR 403</p>
					<p class="error_tlt">Forbidden</p>
					<p class="error_desc">
						금지된 접근입니다.<br>메인페이지로 이동합니다.
					</p>
				</div>
			</div>
			<div class="link_home">
				<a href="/">홈으로</a>
			</div>
		</div>
	</div>
</body>