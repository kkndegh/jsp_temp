<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">

<head>
	<meta charset="utf-8">
	<meta name="robots" content="index,follow">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport"
		content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="canonical" href="URL입력">
	<title>Sellerbot</title>
</head>

<body>
	<div class="container">
		<div class="error_wra">
			<div class="error_area">
				<div class="img_error">
					<img src="/assets/images/etc/img_error_404.png" alt="">
				</div>
				<div class="text_error">
					<p class="error_type">ERROR 404</p>
					<p class="error_tlt">PAGE NOT FOUND</p>
					<p class="error_desc">
						요청하신 페이지를 찾을 수 없습니다.<br> 페이지의 URL 주소가 정확한 지 다시 확인해주세요.
					</p>
				</div>
			</div>
			<div class="link_home">
				<a href="javascript:history.back();">뒤로가기</a>
			</div>
		</div>
	</div>
</body>