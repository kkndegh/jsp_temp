<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!DOCTYPE html>
<html lang="ko">

<head>
	<meta charset="utf-8">
	<meta name="robots" content="index,follow">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport"
		content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="canonical" href="URL입력">
	<title>Sellerbot</title>
</head>

<body>
	<div class="container">
		<div class="error_wra">
			<div class="error_area">
				<div class="img_error">
					<img src="/assets/images/etc/img_error_500.png" alt="">
				</div>
				<div class="text_error">
					<p class="error_type"></p>
					<p class="error_tlt"></p>
					<p class="error_desc">
						${message }
						<!-- <br>
						셀러봇캐시 고객센터(1666-8216)로 문의하시기 바랍니다. -->
					</p>
				</div>
			</div>
			<div class="link_home">
				<a href="/login">로그인</a>
			</div>
		</div>
	</div>
</body>