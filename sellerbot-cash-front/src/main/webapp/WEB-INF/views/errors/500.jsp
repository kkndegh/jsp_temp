<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>

<!DOCTYPE html>
<html lang="ko">

<head>
	<meta charset="utf-8">
	<meta name="robots" content="index,follow">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport"
		content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="canonical" href="URL입력">
	<title>Sellerbot</title>
</head>

<body>
	<div class="container">
		<div class="error_wra">
			<div class="error_area">
				<div class="img_error">
					<img src="/assets/images/etc/img_error_500.png" alt="">
				</div>
				<div class="text_error">
					<p class="error_type">ERROR 500</p>
					<p class="error_tlt">INTERNAL SERVER ERROR</p>
					<p class="error_desc">
						<%= exception.getMessage() %>
						웹 사이트에 페이지를 표시할 수 없습니다.<br> 일시적인 서버 오류거나 및 케이블 및 네트워크 상태 확인 후
						다시 이용해주세요.
					</p>
				</div>
			</div>
			<div class="link_home">
				<a href="javascript:location.back();">뒤로가기</a>
			</div>
		</div>
	</div>
</body>