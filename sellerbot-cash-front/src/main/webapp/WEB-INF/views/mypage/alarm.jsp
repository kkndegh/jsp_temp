<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="container">
	<div class="my_wra">
		<div class="menu_top">
			<p class="menu_name">마이페이지</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">알림톡 서비스</p>
			<p class="desc">매일 매일 대표님과 함께 정산예정금 일일 리포트를 받아 볼 담당자 정보를 변경할 수 있습니다.</p>
		</div>
		<div class="my_area">
			<div class="my_section">

				<div class="member_frm_box mb_alamr_box">
					<div class="item_mb_wrap_top">
						<div class="item_mb">
							<!--<input type="checkbox" class="chk_admin" id="admin1">-->
							<h1 class="alarm_title">대표자</h1>
							<c:if test="${empty ceoData }">
								<input type="hidden" value="" id="ceo_kkont_tgt_no">
							</c:if>
							<c:if test="${not empty ceoData }">
								<input type="hidden" value="${ceoData.kkont_tgt_no}" id="ceo_kkont_tgt_no">
							</c:if>
						</div>
						<div class="item_mb">
							<span class="lb">성명</span>
							<div class="input_box">
								<c:if test="${empty ceoData }">
									<p class="value">${cust.ceo_nm}</p>
									<input type="hidden" value="${cust.ceo_nm}" id="ceo_tgt_cust_nm">
								</c:if>
								<c:if test="${not empty ceoData }">
									<p class="value">${ceoData.tgt_cust_nm}</p>
									<input type="hidden" value="${ceoData.tgt_cust_nm}" id="ceo_tgt_cust_nm">
								</c:if>
							</div>
						</div>
						<div class="item_mb">
							<span class="lb">연락처</span>
							<div class="input_box">
								<c:if test="${empty ceoData }">
									<p class="value">${fn:substring(cust.ceo_no,0,3)} - ${fn:substring(cust.ceo_no,3,7)}
										- ${fn:substring(cust.ceo_no,7,-1)}</p>
									<input type="hidden" value="${cust.ceo_no}" id="ceo_tgt_cust_no">
								</c:if>
								<c:if test="${not empty ceoData }">
									<p class="value">${fn:substring(ceoData.tgt_cust_no,0,3)} -
										${fn:substring(ceoData.tgt_cust_no,3,7)} -
										${fn:substring(ceoData.tgt_cust_no,7,-1)}</p>
									<input type="hidden" value="${ceoData.tgt_cust_no}" id="ceo_tgt_cust_no">
								</c:if>
							</div>
						</div>
						<div class="item_mb last_item_mb">
							<span class="lb">수신시간 :</span>
							<div class="input_box">
								<select class="sctbox sctbox_tel" id="ceo_trs_hop_hour">
									<option value="8">8시</option>
									<option value="9">9시</option>
									<option value="10">10시</option>
									<option value="11">11시</option>
									<option value="12">12시</option>
									<option value="13">13시</option>
									<option value="14">14시</option>
									<option value="15">15시</option>
									<option value="16">16시</option>
									<option value="17">17시</option>
									<option value="18">18시</option>
									<option value="19">19시</option>
									<option value="20">20시</option>
								</select>
							</div>
						</div>
					</div>
					<div class="item_mb_wrap_bottom">
						<div class="item_mb">
							<!--<input type="checkbox" class="chk_admin" id="admin2">-->
							<h1 class="alarm_title" style="display:inline-block">담당자</h1>
							<img src="/assets/images/member/icon_1.png" alt="" id="crg_remove_btn"
								style="display:inline-block;vertical-align:-7.5px;cursor: pointer;border-radius:20px;">
							<c:if test="${empty crgData }">
								<input type="hidden" value="" id="crg_kkont_tgt_no">
							</c:if>
							<c:if test="${not empty crgData }">
								<input type="hidden" value="${crgData.kkont_tgt_no}" id="crg_kkont_tgt_no">
							</c:if>
						</div>
						<div class="item_mb">
							<span class="lb">성명</span>
							<div class="input_box">
								<c:if test="${empty crgData }">
									<input type="text" class="textbox_mb" value="${cust.chrg_nm }" id="crg_tgt_cust_nm">
								</c:if>
								<c:if test="${not empty crgData }">
									<input type="text" class="textbox_mb" value="${crgData.tgt_cust_nm }"
										id="crg_tgt_cust_nm">
								</c:if>

							</div>
						</div>
						<div class="item_mb">
							<span class="lb">연락처</span>
							<div class="input_box">
								<div class="tel_inputBox">
									<select class="sctbox sctbox_tel" id="crg_tgt_cust_no_1">
										<option value="010">010</option>
										<option value="011">011</option>
										<option value="016">016</option>
										<option value="017">017</option>
										<option value="018">018</option>
										<option value="019">019</option>
									</select>
									<span>&#45;</span>
									<c:if test="${empty crgData }">
										<input type="text" class="textbox_mb textbox_mb_tel" required=""
											id="crg_tgt_cust_no_2" value="${fn:substring(cust.chrg_no,3,7)}">
									</c:if>
									<c:if test="${not empty crgData }">
										<input type="text" class="textbox_mb textbox_mb_tel" required=""
											id="crg_tgt_cust_no_2" value="${fn:substring(crgData.tgt_cust_no,3,7)}">
									</c:if>
									<span>&#45;</span>
									<c:if test="${empty crgData }">
										<input type="text" class="textbox_mb textbox_mb_tel2" required=""
											id="crg_tgt_cust_no_3" value="${fn:substring(cust.chrg_no,7,-1)}">
									</c:if>
									<c:if test="${not empty crgData }">
										<input type="text" class="textbox_mb textbox_mb_tel2" required=""
											id="crg_tgt_cust_no_3" value="${fn:substring(crgData.tgt_cust_no,7,-1)}">
									</c:if>
								</div>
								<p class="error">*휴대폰번호를 다시 확인해주세요</p>
							</div>
						</div>
						<div class="item_mb ">
							<span class="lb">수신시간 :</span>
							<div class="input_box">
								<select class="sctbox sctbox_tel" id="crg_trs_hop_hour">
									<option value="8">8시</option>
									<option value="9">9시</option>
									<option value="10">10시</option>
									<option value="11">11시</option>
									<option value="12">12시</option>
									<option value="13">13시</option>
									<option value="14">14시</option>
									<option value="15">15시</option>
									<option value="16">16시</option>
									<option value="16">16시</option>
									<option value="17">17시</option>
									<option value="18">18시</option>
									<option value="19">19시</option>
									<option value="20">20시</option>
								</select>
							</div>
						</div>
					</div>
					<p>
						<div class="btn_frm_group_add">
							<button type="button" class="ad_bt" style="background:#c3e2ed; color:#000;">담당자 추가</button>
						</div>
					</p>
					<div class="title_alarm_my">
						<input type="checkbox" class="chk_admin_set" id="chk_admin_set" checked>
						<label for="chk_admin_set">알림톡 신청하기</label>
					</div>
				</div>
				<div class="btn_right">
					<button id="btn_kkont_apply" class="btn_circle">확인</button>
				</div>

			</div>
		</div>
	</div>
	<div class="popup popup_confirm_edit">
		<div class="pop">
			<span class="close_pop"><img src="/assets/images/member/close_gray.png" alt=""></span>
			<div class="pop_body">
				<p class="desc_pop">정보 수정이<br>
					완료되었습니다!</p>
			</div>
			<div class="pop_footer">
				<button type="button" class="close_btn" id="completeProcess">닫기</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
		$(".error").hide();
		//담당자 추가 여부
		var showCrg = true;
		var chkAdmin = false;
		var confirmText = "알림톡 수신 정보를 변경하시겠습니까?";

		//알림톡 미신청시 수신동의 체크 해제
		if ($("#ceo_kkont_tgt_no").val() == "") {
			confirmText = "알림톡 수신 신청을 하시겠습니까";
			$("#chk_admin_set").prop("checked", false);
			$("#btn_kkont_apply").prop("disabled", true);
			$(".mb_alamr_box").css("opacity", "0.5");
		}
		//담당자 항목 보이지 않도록
		if ($("#crg_kkont_tgt_no").val() == "") {
			showCrg = false;
		}
		if (showCrg) {
			$(".item_mb_wrap_bottom").show();
			$(".ad_bt").hide();
		} else {
			$(".item_mb_wrap_bottom").hide();
			$(".ad_bt").show();
		}
		<c:if test="${not empty crgData}">
			$("#crg_tgt_cust_no_1").val("${fn:substring(crgData.tgt_cust_no,0,3)}");
			$("#crg_trs_hop_hour").val("${fn:substring(crgData.trs_hop_hour,0,3)}");
		</c:if>
		<c:if test="${not empty ceoData}">
			$("#ceo_trs_hop_hour").val("${fn:substring(ceoData.trs_hop_hour,0,3)}");
		</c:if>
		$(".ad_bt").click(function () {
			showCrg = true;
			$(".item_mb_wrap_bottom").show();
			$(".ad_bt").hide();
		});
		$("#crg_remove_btn").click(function () {
			showCrg = false;
			$(".item_mb_wrap_bottom").hide();
			$(".ad_bt").show();
		});
		$("#chk_admin_set").click(function () {
			if ($(this).prop("checked")) {
				
				$("#btn_kkont_apply").prop("disabled", false);
				$(".mb_alamr_box").css("opacity", "1");
			}else {
				$("#btn_kkont_apply").prop("disabled", false);
				$(".mb_alamr_box").css("opacity", "0.5");

			}
		});
		$("#completeProcess").click(function () {
			history.go(0);
		});
		$("#btn_kkont_apply").click(function () {
			var params = [];
			var canc_dt = null;

			chkAdmin = $("#chk_admin_set").prop("checked");
			if (!chkAdmin) {
				canc_dt = new Date();
			}
			var ceoData = {
				kkont_tgt_no: $("#ceo_kkont_tgt_no").val(),
				tgt_cust_nm: $("#ceo_tgt_cust_nm").val(),
				tgt_cust_no: $("#ceo_tgt_cust_no").val(),
				cust_posi_cd: "CEO",
				trs_hop_hour: $("#ceo_trs_hop_hour").val(),
				canc_dt: canc_dt
			};
			params.push(ceoData);
			var crgPhoneNo = $("#crg_tgt_cust_no_1").val() + "" + $("#crg_tgt_cust_no_2").val() + "" + $("#crg_tgt_cust_no_3").val();
			if (showCrg) {
				if ($("#crg_tgt_cust_nm").val() == "") {
					showAlert("담당자 명을 입력해주세요");
					return false;
				}
				if (!(crgPhoneNo.length == 10 || crgPhoneNo.length == 11) || isNaN(crgPhoneNo)) {
					$(".error").show();
					return false;
				} else {
					$(".error").hide();
				}
				var crgData = {
					kkont_tgt_no: $("#crg_kkont_tgt_no").val(),
					tgt_cust_nm: $("#crg_tgt_cust_nm").val(),
					tgt_cust_no: crgPhoneNo,
					cust_posi_cd: "CRG",
					trs_hop_hour: $("#crg_trs_hop_hour").val(),
					canc_dt: canc_dt
				};
				params.push(crgData);
			} else {
				if ($("#crg_kkont_tgt_no").val() != "") {
					var crgData = {
						kkont_tgt_no: $("#crg_kkont_tgt_no").val(),
						tgt_cust_nm: $("#crg_tgt_cust_nm").val(),
						tgt_cust_no: crgPhoneNo,
						cust_posi_cd: "CRG",
						trs_hop_hour: $("#crg_trs_hop_hour").val(),
						canc_dt: new Date()
					};
					params.push(crgData);
				}
			}

			var modalId = showConfirm(confirmText, function () {

				$.ajax({
					url: '/sub/my/alarm_input',
					data: JSON.stringify(params),
					contentType: "application/json",
					processData: false,
					async: false,
					type: 'POST',
					success: function (data) {
						if (data) {
							if (!chkAdmin) {
								removeModal(modalId);
								showAlert("정보 변경이 완료되었습니다.", function () {
									history.go(0);
								});
							}else {
								removeModal(modalId);
								showAlert("알림톡 서비스가 신청되었습니다.", function () {
									history.go(0);
								});
							}
						} else {
							removeModal(modalId);
							showAlert("저장 실패하였습니다.", function () {
								history.go(0);
							});
						}
					},
					error: function (request, status, error) {
						removeModal(modalId);
						showAlert("저장 실패하였습니다.", function () {
							history.go(0);
						});
					}
				});

			});

		});
	});
</script>