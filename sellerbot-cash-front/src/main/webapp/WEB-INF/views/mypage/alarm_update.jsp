<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<form id="form" onsubmit="return false;">
	<div class="container">
		<div class="my_wra">
			<div class="menu_top">
				<p class="menu_name">마이페이지</p>
				<div class="gnb_topBox">
					<ul class="gnb_top clearfix">
						<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
						<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
							<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
								<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
									<c:choose>
										<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
											<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:if>
						</c:forEach>
					</ul>
				</div>
			</div>
			<div class="title_bg">
				<p class="title">알림톡 서비스</p>
				<p class="desc">알림톡을 받으실 담당자명과 담당자 연락처를 변경할 수 있습니다.</p>
			</div>
			<div class="my_area">
				<div class="my_section">
					<div class="sub_menu_title">
						알림톡 수신정보 변경
						<div class="title_alarm_my">
							<input type="checkbox" class="chk_admin_set" name="chk_admin_set" id="chk_admin_set"
								onclick="check(this)" ${alarmDTO.admin1 eq 'AVT' ? 'checked="checked"' : '' }>
							<label for="chk_admin_set">알림톡 수신 동의</label>
						</div>
					</div>
					<div class="member_frm_box mb_alamr_box">
						<div class="item_mb_wrap_top">
							<div class="item_mb">
								<h1 class="alarm_title">대표자</h1>
							</div>
							<div class="item_mb">
								<span class="lb">성명</span>
								<div class="input_box">
									<input type="hidden" value="${alarmDTO.ceo_nm}" name="ceo_nm" id="ceo_nm" />
									<p class="value">${alarmDTO.ceo_nm}</p>
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">연락처</span>
								<div class="input_box">
									<input type="hidden" value="${alarmDTO.ceo_no}" name="ceo_no" id="ceo_no" />
									<p class="value">
										${fn:substring(alarmDTO.ceo_no,0,3)}-${fn:substring(alarmDTO.ceo_no,3,7)}-${fn:substring(alarmDTO.ceo_no,7,-1)}
									</p>
								</div>
							</div>
							<div class="item_mb last_item_mb">
								<span class="lb">수신시간 :</span>
								<div class="input_box">
									<select class="sctbox sctbox_tel" name="trs_hop_hour1" id="trs_hop_hour1">
										<option value="09">09시</option>
										<c:forEach begin="10" var="t" end="18">
											<option value="${t}" ${alarmDTO.trs_hop_hour1 eq t ? 'selected="selected"'
												: '' }>${t}시</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<div class="item_mb_wrap_bottom">
							<div class="item_mb">
								<h1 style="display:inline-block;" class="alarm_title">담당자</h1><img
									style="display:inline-block;" src="/assets/images/member/icon_1.png" alt="">
							</div>
							<div class="item_mb">
								<span class="lb">성명</span>
								<div class="input_box">
									<input type="text" class="textbox_mb"
										value="${not empty alarmDTO.chrg_nm ? alarmDTO.chrg_nm : cust.chrg_nm }"
										name="chrg_nm" id="chrg_nm">
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">연락처</span>
								<div class="input_box">
									<div class="tel_inputBox">
										<select class="sctbox sctbox_tel">
											<option value="010">010</option>
											<option value="011">011</option>
											<option value="016">016</option>
											<option value="017">017</option>
											<option value="018">018</option>
											<option value="019">019</option>
										</select>
										<span>&#45;</span>
										<input type="text" class="textbox_mb textbox_mb_tel" required=""
											value="${not empty alarmDTO.chrg_no ? fn:substring(alarmDTO.chrg_no,3,7) : fn:substring(cust.chrg_no,3,7)}"
											name="chrg_no1" id="chrg_no1">
										<span>&#45;</span>
										<input type="text" class="textbox_mb textbox_mb_tel2" required=""
											value="${not empty alarmDTO.chrg_no ? fn:substring(alarmDTO.chrg_no,7,11) : fn:substring(cust.chrg_no,7,11)}"
											name="chrg_no2" id="chrg_no2">
										<input type="hidden"
											value="${not empty alarmDTO.chrg_no ? alarmDTO.chrg_no : cust.chrg_no}"
											id="chrg_no" name="chrg_no">
									</div>
									<p class="error" style="display: none;">*휴대폰번호를 다시 확인해주세요</p>
								</div>
							</div>
							<div class="item_mb ">
								<span class="lb">수신시간 :</span>
								<div class="input_box">
									<select class="sctbox sctbox_tel" name="trs_hop_hour2" id="trs_hop_hour2">
										<option value="09" ${alarmDTO.trs_hop_hour2 eq '09' ? 'selected="selected"' : ''
											}>09시</option>
										<c:forEach begin="10" var="t" end="18">
											<option value="${t}" ${alarmDTO.trs_hop_hour2 eq t ? 'selected="selected"'
												: '' }>${t}시</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<p>
							<div class="btn_frm_group_add">
								<button type="button" class="ad_bt" style="background:#c3e2ed; color:#000;">담당자
									추가</button>
							</div>
						</p>
						<p class="msg_time_alarm">알림톡 해지 신청은 <a href="">1:1 문의</a>를 이용하시기 바랍니다.</p>
						<div class="btn_frm_group">
							<button>입력완료</button>
							<input type="hidden" id="kkont_tgt_no" name="kkont_tgt_no"
								value="${alarmDTO.kkont_tgt_no }" />
							<input type="hidden" id="kkont_tgt_no2" name="kkont_tgt_no2"
								value="${alarmDTO.kkont_tgt_no2 }" />
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="popup popup_confirm_edit">
			<div class="pop">
				<span class="close_pop"><img src="/assets/images/member/close_gray.png" alt=""></span>
				<div class="pop_body">
					<p class="desc_pop">정보 수정이<br>
						완료되었습니다!</p>
				</div>
				<div class="pop_footer">
					<button type="button" class="close_btn">닫기</button>
				</div>
			</div>
		</div>
	</div>
</form>
<script>

	$(document).ready(function () {

		var chrg_check = true;
		$("#chrg_no1").val("${fn:substring(cust.chrg_no,0,3)}");

		if (chrg_check) {
			$(".item_mb_wrap_bottom").show();
			$(".ad_bt").hide();
		} else {
			$(".item_mb_wrap_bottom").hide();
			$(".ad_bt").show();
		}
		$("#chrg_remove").click(function (event) {
			event.preventDefault();
			$(".item_mb_wrap_bottom").hide();
			$(".ad_bt").show();
		});
		$(".ad_bt").click(function () {
			$(".item_mb_wrap_bottom").show();
			$(".ad_bt").hide();
		});
		$(".btn_frm_group button").click(function () {

			$("#chrg_no").val($("#chrg_no1").val() + $("#chrg_no2").val() + $("#chrg_no3").val());
			var chrg_no = $("#chrg_no").val();
			if (chrg_no.length == 10 || chrg_no.length == 11) {
				$(".error").hide();
			} else {
				$(".error").show();
				return;
			}

			if (isNaN(chrg_no)) {
				$(".error").show();
				return;
			} else {
				$(".error").hide();
			}


			var formData = $("#form").serialize();
			$.ajax({
				url: '/sub/my/alarm_update',
				data: formData,
				processData: false,
				async: false,
				type: 'POST',
				success: function (data) {
					$('.popup_confirm_edit').show();
				},
				error: function (request, status, error) {
					alert("저장 실패하였습니다.");
				}
			});

			$('.popup_confirm_edit .close_pop,.popup_confirm_edit .close_btn').click(function () {
				$('.popup_confirm_edit').hide();
			});
		});

		$(".btn_circle").click(function () {
			$(".popup_confirm_edit").show();
			$(".popup_confirm_edit .close_pop,.popup_confirm_edit .close_btn").click(function () {
				$(".popup_confirm_edit").hide();
			});

		});
	});
	function check(ts) {
		if ('${alarmDTO.admin1}' == 'AVT') {
			$(ts).prop("checked", true);
			return;
		}
		if ($(ts).is(":checked")) {
			$("#admin1").prop("checked", true);
			$("#admin2").prop("checked", true);
		} else {
			$("#admin1").prop("checked", false);
			$("#admin2").prop("checked", false);
		}
	}

</script>