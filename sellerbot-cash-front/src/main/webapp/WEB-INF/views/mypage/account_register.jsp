<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="/assets/js/mngAcct.js?ver=20200819_01"></script>

<div class="container">
	<div class="my_wra">
		<div class="menu_top">
			<p class="menu_name">마이페이지</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">정산계좌 관리</p>
			<p class="desc">각 은행에서 <font style="color: red;">빠른조회서비스</font>에 가입하신 후 사용가능한 서비스입니다.</p>
		</div>
		<div class="my_area">
			<div class="my_section">
				<div class="sub_menu_title">
					<div class="back_edit_page">
						<button class="backbtn" onclick="history.go(-1);">&#60; 이전으로</button>
					</div>
					계좌 등록
				</div>
				<div class="guide_register">
					<h1>
						<p>정산계좌 통합관리 도움말</p>
						<p class="edit_video_btn3"><span>영상가이드</span></p>
					  </h1>
					<span>
						정산계좌 통합관리 서비스를 이용하시려면 각 은행별 홈페이지에서 빠른조회서비스 등록을 진행하신 후<br />
						계좌정보를 입력하셔야 합니다. 하단의 은행명을 선택해주시면 은행별 빠른조회서비스 등록 절차를 확인하실 수 있습니다.
					</span>
					<span class="guide_register_detail">
						<b>산업은행</b><br />
						▣ 개인뱅킹<br />
						① 산업은행 홈페이지 <a href="http://www.kdb.co.kr" target="_brank"
							style="color: blue;">(http://www.kdb.co.kr)<a>에 접속<br />
								② 공인인증서를 통하여 인터넷뱅킹 로그인<br />
								③ [뱅킹관리 → 계좌관리 → 계좌관리] 에서 등록
								<br />
								<br />
								▣ 개인뱅킹<br />
								① 산업은행 홈페이지 <a href="http://www.kdb.co.kr" target="_brank"
									style="color: blue;">(http://www.kdb.co.kr)<a>에 접속<br />
										② 공인인증서를 통하여 인터넷뱅킹 로그인<br />
										③ - A. 기업뱅킹 (USB 1개 사용) : [뱅킹관리 → 계좌관리 → 빠른조회대상계좌설정]에서 등록<br />
										③ - B. 기업뱅킹 (USB 2개 이상) : [대표관리자 인터넷뱅킹 접속 → 뱅킹관리 → 계좌관리 → 빠른조회대상계좌설정] 에서 등록
					</span>
				</div>
				<div class="frm_account_my">

					<p class="account_lb">계좌정보 입력</p>

					<div class="item_input_acct bank_cert_m_seq_no_use_yn">
						<p class="lb_input">은행명</p>
						<div class="box_input_acct">
							<select class="sctbox" id="bank_cert_m_seq_no" required="required">
							</select>
						</div>
					</div>
					<div class="item_input_acct bank_id_use_yn">
						<p class="lb_input">빠른조회<br>은행아이디</p>
						<div class="box_input_acct">
							<input id="bank_id" type="text" class="textbox_mb" maxlength="25" required="required">
							<p id="bank_id_error" class="error" style="display:none;">*은행아이디를 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct bank_passwd_use_yn">
						<p class="lb_input">빠른조회<br>은행비밀번호</p>
						<div class="box_input_acct">
							<input id="bank_passwd" type="password" class="textbox_mb" maxlength="20"
								required="required">
							<p id="bank_passwd_error" class="error" style="display:none;">*은행비밀번호 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct dpsi_nm_use_yn">
						<p class="lb_input">예금주</p>
						<div class="box_input_acct">
							<input id="dpsi_nm" type="text" class="textbox_mb" maxlength="20" required="required">
							<p id="dpsi_nm_error" class="error" style="display:none;">*예금주를 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct acct_no_use_yn">
						<p class="lb_input" id="acctNoTitle">계좌번호</p>
						<div class="box_input_acct">
							<input id="acct_no" type="text" class="textbox_mb" maxlength="20" autocomplete="off"
								required="required" data-valitype="acctNo">
							<p id="acct_no_error" class="error" style="display:none;">*계좌번호를 바르게 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct acct_passwd_use_yn">
						<p class="lb_input" id="acctPwdTitle">계좌비밀번호</p>
						<div class="box_input_acct">
							<input id="acct_passwd" type="password" autocomplete="new-password" class="textbox_mb"
								maxlength="20" required="required">
							<p id="acct_passwd_error" class="error" style="display:none;">*계좌비밀번호 입력해주세요.</p>
						</div>
					</div>


					<div class="item_input_acct ctz_biz_no_use_yn">
						<p class="lb_input">주민등록<br>(사업자)번호</p>
						<div class="box_input_acct">
							<input id="ctz_biz_no" type="text" class="textbox_mb" maxlength="10"
								placeholder="주민번호 앞 6자리 또는 사업자번호10자리만 입력" required="required" data-valitype="ctzBizno">
							<p id="ctz_biz_no_error" class="error" style="display:none;">*주민등록(사업자) 번호를 바르게 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct crc_use_cd">
						<p class="lb_input">통화</p>
						<div class="box_input_acct">
							<select id="crc" class="sctbox">
								<option value="">선택</option>
								<c:forEach items="${codeList }" var="code" varStatus="index">
									<option value="${code.com_cd }">${code.cd_nm }</option>
								</c:forEach>
							</select>
							<p id="crc_error" class="error" style="display:none;">*통화를 선택해주세요.</p>
						</div>
					</div>
					<div class="btn_frm_group">
						<button id="btnSave">입력완료</button>
					</div>

				</div>

			</div>
		</div>
	</div>

</div>
<div class="title_bg">
	<p class="desc">제휴사 서비스를 이용 중이신 경우, 등록된 계좌의 삭제가 제한될 수 있습니다.</p>
</div>
<div class="edit_video_popup">
    <div class="edit_video_popup_container">
      <p><img src="/assets/images/member/close.png" alt=""></p>
      <iframe width="100%" height="100%" src="https://www.youtube.com/embed/OzaIAVrgQEo?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="background_black"></div>
</div>
<style>
.edit_video_btn3 {
	text-align: right;

}
.edit_video_btn3 span {
	font-size: 1rem;
	color: #fff;
	background-color: #009aca;
	display: block;
	width: 106px;
	text-align: center;
	padding: 0.3rem 0;
	border-radius: 3rem;
	float: right;
	cursor: pointer;

}

.guide_register h1 {
	font-size: 0;
}

.guide_register h1 p {
	display: inline-block;
	width: 50%;
	font-size: 1.5rem;
	vertical-align: middle;
}
</style>
<script>
	var _bankList;
	<c:if test="${not empty bankList }">
		_bankList = ${bankList};
 	</c:if>

	$(document).ready(function () {
		$("#bank_cert_m_seq_no").on("change", function () {
			fnViewChange(this.value, _bankList);
		});

		$("#btnSave").on("click", function () {
			if (!isVali()) { return false; }
			var bankInfo = fnGetJsonData($("#bank_cert_m_seq_no").val(), _bankList);

			// 기본 공통 값
			var param = {
				"bank_cert_m_seq_no": $("#bank_cert_m_seq_no").val(),
				"reco_yn": "N"
			};
			var input = $(".frm_account_my").find("[required]");
			var jqThis;
			$.each(input, function () {
				jqThis = $(this);
				if (nonNull(jqThis.val())) {
					param[jqThis.attr("id")] = jqThis.val();
				}
			});

			if (bankInfo.crc_use_cd == "S") {
				param.crc = $("#crc").val();
			}

			$.ajax({
				url: '/sub/my/account'
				, type: 'post'
				, async: false
				, dataType: 'text'
				, contentType: 'application/json'
				, data: JSON.stringify([param])
				, success: function (res) {
					showAlert("계좌가 등록되었습니다.", function () {
						location.href = "/sub/my/account";
					});
				}
				, error: function (error) {
					// 2020-05-18 기존의 등록된 계좌 번호와 동일한 경우 undefine 문구 노출됨
					showAlert("요청이 실패하였습니다. <br>입력하신 정보를 다시 확인해주세요.");
					// if (isJsonString(error.responseText)) {
					// 	var data = JSON.parse(error.responseText);
					// 	showAlert(data.comment);
					// } else {
					// 	showAlert("저장실패 하였습니다. <br> 관리자에게 문의하세요.");
					// }
				}
			});

		});

		$("#btnGoFaq").on("click", function () {
			location.href = "/pub/cs/faq?title=" + $("#bank_cert_m_seq_no option:selected").text() + "&page=1&faq_typ_cd=F07";
		});

		fnInit(_bankList);
	});
	//영상 팝업 스크립트
	document.querySelectorAll(".edit_video_btn3")[0].addEventListener("click",function(){
		document.querySelectorAll(".edit_video_popup")[0].style.display = "flex";
	});
	document.querySelectorAll(".edit_video_popup_container p")[0].addEventListener("click",function(){
		document.querySelectorAll(".edit_video_popup")[0].style.display = "none";
		document.querySelectorAll(".edit_video_popup iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
	});

</script>