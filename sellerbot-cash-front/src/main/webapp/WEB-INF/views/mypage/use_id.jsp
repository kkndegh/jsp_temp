<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">통합 아이디 사용현황</p>
            <p class="desc">셀러봇캐시 통합아이디 하나로 셀러봇캐시와 제휴사의 회원가입 및 서비스이용이 편리해집니다.</p>
        </div>
        <div class="my_area">
            <div class="my_section">
                <div class="ing_id_list">
                    <div class="use_ing_box">
                        <p class="ing_title">통합아이디 사용 중인 제휴사</p>
                        <p class="ing_desc">현재 셀러봇캐시 통합아이디로 회원 가입된 제휴사입니다.</p>

                        <c:set value="0" var="useSvcCnt"></c:set>
                        <c:forEach items="${svcList }" var="useSvc" varStatus="state">
                            <c:if test="${useSvc.svc_join_dt!=null }">
                                <c:set value="${useSvcCnt+1}" var="useSvcCnt"></c:set>
                                <div class="mall_id_card">
                                    <p class="name_m">${useSvc.svc_nm}<span class="use_status">회원가입</span></p>
                                    <p class="desc_m">${useSvc.svc_descp}</p>
                                    <div class="other_info_mall">
                                        <span class="lb">이용 시작일</span>
                                        <p class="text_other">
                                            ${fn:substring(useSvc.svc_join_dt,0,4)}.${fn:substring(useSvc.svc_join_dt,4,6)}.${fn:substring(useSvc.svc_join_dt,6,-1)}
                                        </p>
                                    </div>
                                    <c:set var="termChk" value="0"></c:set>

                                    <c:forEach items="${useSvc.terms }" var="chkTerm">
                                        <c:if test="${chkTerm.terms_esse_yn=='N' && chkTerm.aggr_yn=='N' }">
                                            <c:set var="termChk" value="${termChk+1}"></c:set>
                                        </c:if>
                                    </c:forEach>

                                    <c:if test="${termChk>0 }">
                                        <div class="other_info_mall">
                                            <span class="lb">약관동의</span>
                                            <c:forEach items="${useSvc.terms }" var="term">
                                                <c:if test="${term.terms_esse_yn=='N' && term.aggr_yn=='N' }">
                                                    <div class="chk_mall_agree">
                                                        <input type="checkbox" class="chkbox" id="agreeMall1">
                                                        <label for="agreeMall1">[선택]${term.terms_title }</label>
                                                        <input type="hidden" class="modalTitle"
                                                            value="[선택]<c:out value=" ${term.terms_title}"></c:out>">
                                                        <input type="hidden" class="modalData" value="<c:out value="
                                                            ${term.terms_cont}"></c:out>">
                                                        <a href="" class="dt_agree_pop">자세히보기</a>
                                                    </div>
                                                </c:if>
                                            </c:forEach>
                                        </div>
                                    </c:if>

                                    <a href="${useSvc.recv_dm }" target="_blank" class="homepage">홈페이지</a>
                                </div>
                            </c:if>

                        </c:forEach>
                        <c:if test="${useSvcCnt==0 }">
                            <div class="no_data no_data_type_2">
                                <p>이용중인 제휴사가 없습니다.</p>
                            </div>
                        </c:if>
                    </div>
                    <div class="use_ing_box">
                        <p class="ing_title">통합아이디 사용 가능한 제휴사</p>
                        <p class="ing_desc">셀러봇캐시 통합아이디로 회원 가입 및 서비스 이용이 가능한 제휴사입니다.</p>
                        <c:set value="0" var="nUseSvcCnt"></c:set>
                        <c:forEach items="${svcList }" var="nUseSvc" varStatus="state">
                            <c:if test="${nUseSvc.svc_join_dt==null }">
                                <c:set value="${nUseSvcCnt+1 }" var="nUseSvcCnt"></c:set>
                                <div class="mall_id_card">
                                    <p class="name_m">${nUseSvc.svc_nm}</p>
                                    <p class="desc_m">${nUseSvc.svc_descp}</p>
                                    <div class="other_info_mall">
                                        <!-- 제휴일자 없을경우 보이지않도록 -->
                                        <c:if test="${not empty nUseSvc.svc_p_ship_dt and nUseSvc.svc_p_ship_dt!=''}">
                                            <span class="lb">제휴일자</span>
                                            <p class="text_other">
                                                ${fn:substring(nUseSvc.svc_p_ship_dt,0,4)}.${fn:substring(nUseSvc.svc_p_ship_dt,4,6)}.${fn:substring(nUseSvc.svc_p_ship_dt,6,-1)}
                                            </p>
                                        </c:if>
                                    </div>
                                    <a href="${nUseSvc.recv_dm }" target="_blank" class="homepage">홈페이지</a>
                                </div>
                            </c:if>
                        </c:forEach>
                        <c:if test="${nUseSvcCnt==0 }">
                            <div class="no_data no_data_type_2">
                                <p>이용가능 제휴사가 없습니다.</p>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--팝업 완료-->
    <div class="popup popup_confirm_edit">
        <div class="pop">
            <span class="close_pop"><img src="/assets/images/member/close_gray.png" alt=""></span>
            <div class="pop_body">
                <p class="desc_pop">정보 수정이<br>
                    완료되었습니다!</p>
            </div>
            <div class="pop_footer">
                <button type="button" class="close_btn">닫기</button>
            </div>
        </div>
    </div>
    <!--팝업 이용약관-->
    <div class="popup popup_terms">
        <div class="pop">
            <div class="pop_head">
                <p class="title_pop">[선택]이용약관</p>
            </div>
            <div class="pop_body">
                <div class="terms_box">
                    <p class="scrollblack">이용약관<br><br>
                        제 1조(목적)<br>
                        &nbsp;이 약관은 샐러봇캐시 (전자상거래 사업자)가 운영하는 ㅇㅇ사이버몰 (이하 “을”이라 한다)에서 제공하는 인
                        터넷 관련 서비스 (이하 “서비스”라한다)를 이용함에 있어 사이버몰과 이용자의 권리,의무 및 책임사항을 규
                        정함을 목적으로 합니다.<br>
                        제 2조(목적)<br>
                        &nbsp;이 약관은 샐러봇캐시 (전자상거래 사업자)가 운영하는 ㅇㅇ사이버몰 (이하 “을”이라 한다)에서 제공하는 인
                        터넷 관련 서비스 (이하 “서비스”라한다)를 이용함에 있어 사이버몰과 이용자의 권리,의무 및 책임사항을 규
                        정함을 목적으로 합니다.<br>
                        제 2조 1항<br>
                        &nbsp;이 약관은 샐러봇캐시 (전자상거래 사업자)가 운영하는 ㅇㅇ사이버몰 (이하 “을”이라 한다)에서 제공하는 인
                        터넷 관련 서비스 (이하 “서비스”라한다)를 이용함에 있어 사이버몰과 이용자의 권리,의무 및 책임사항을 규
                        정함을 목적으로 합니다.<br>
                        제 2조 2항<br>
                        &nbsp;이 약관은 샐러봇캐시 (전자상거래 사업자)가 운영하는 ㅇㅇ사이버몰 (이하 “을”이라 한다)에서 제공하는 인
                        터넷 관련 서비스 (이하 “서비스”라한다)를 이용함에 있어 사이버몰과 이용자의 권리,의무 및 책임사항을 규
                        정함을 목적으로 합니다.<br>
                        제 2조 3항<br>
                        &nbsp;이 약관은 샐러봇캐시 (전자상거래 사업자)가 운영하는 ㅇㅇ사이버몰 (이하 “을”이라 한다)에서 제공하는 인
                        터넷 관련 서비스 (이하 “서비스”라한다)를 이용함에 있어 사이버몰과 이용자의 권리,의무 및 책임사항을 규
                        정함을 목적으로 합니다.
                    </p>
                </div>
            </div>
            <div class="pop_foot">
                <button type="button" class="btn_confirm_terms">약관확인</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(".btn_circle").click(function () {
        $(".popup_confirm_edit").show();
        $(".popup_confirm_edit .close_pop,.popup_confirm_edit .close_btn").click(function () {
            $(".popup_confirm_edit").hide();
        });

    });

    $(".dt_agree_pop").click(function () {
        var title = $(this).parent().find(".modalTitle").val();
        var cont = $(this).parent().find(".modalData").val();
        $(".title_pop").empty().html(title);
        $(".terms_box").empty().html(cont).css("overflow-y", "auto");
        $(".popup_terms").show();
        $(".popup_terms .btn_confirm_terms").click(function () {
            $(".popup_terms").hide();
        });
        return false;
    });
</script>