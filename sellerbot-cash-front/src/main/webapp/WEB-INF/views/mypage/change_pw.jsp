<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">회원정보</p>
            <p class="desc">회원 정보를 수정 및 비밀번호 변경이 가능합니다.</p>
        </div>
        <div class="my_area">
            <div class="my_section">
                <div class="sub_menu_title">
                    <div class="back_edit_page">
                        <a href="/sub/my/confirm_pw">&#60; 이전으로</a>
                    </div>
                    비밀번호 변경
                    <!-- <a href="/sub/service/use.html" class="icon_using">
                            <img src="/assets/images/my/icon_diamond.png" alt=""> 프리미엄 서비스 이용중
                        </a> -->
                </div>
                <div class="member_frm_box mb_pw_box">
                    <div class="item_mb">
                        <span class="lb">현재 비밀번호</span>
                        <div class="input_box">
                            <input type="password" class="textbox_mb" id="passwd" required="required">
                            <p class="error" id="passwd_error">*현재 비밀번호를 정확히 입력하여 주세요.</p>
                        </div>
                    </div>
                    <div class="item_mb">
                        <span class="lb">비밀번호</span>
                        <div class="input_box">
                            <input type="password" class="textbox_mb" id="new_passwd" required="required"
                                data-valitype="password">
                            <p class="desc_mb">
                                영문, 숫자, 특수문자를 조합하여 8자 이상 ~ 20자 이내로 입력해야 합니다.<br>( 사용가능한 특수문자 : !@#$%^&*+=-[] )
                            </p>
                            <p class="error" id="new_passwd_error">*비밀번호를 정확히 입력하여 주세요.</p>
                        </div>
                    </div>
                    <div class="item_mb">
                        <span class="lb">비밀번호 확인</span>
                        <div class="input_box">
                            <input type="password" class="textbox_mb" id="new_passwd_confirm" required="required"
                                data-valitype="password" for="new_passwd">
                            <p class="error" id="new_passwd_confirm_error">*입력하신 비밀번호가 맞지 않습니다.</p>
                        </div>
                    </div>

                </div>

            </div>
            <div class="btn_right">
                <button class="btn_circle">수정완료</button>
            </div>
        </div>
    </div>
    <div class="popup popup_confirm_edit">
        <div class="pop">
            <span class="close_pop"><img src="/assets/images/member/close_gray.png" alt=""></span>
            <div class="pop_body">
                <p class="desc_pop" id="desc_pop">정보 수정이<br>
                    완료되었습니다!</p>
            </div>
            <div class="pop_footer">
                <button type="button" class="close_btn">닫기</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(".btn_circle").click(function () {

        if (!isVali()) { return false; }

        $.post("/sub/my/password", { passwd: $("#passwd").val(), new_passwd: $("#new_passwd").val(), new_passwd_confirm: $("#new_passwd_confirm").val() }, function (data, status) {
            if (data == "OK") {
                $(".error").hide();
                $(".popup_confirm_edit").show();
                $(".popup_confirm_edit .close_pop,.popup_confirm_edit .close_btn").click(function () {
                    $(".popup_confirm_edit").hide();
                    location.href = "/sub/my/edit_info";
                });
            } else if (data == "AUTH-4002") {
                $("#error3").show();
            } else if (data == "AUTH-4003") {
                $("#error1").show();
            } else if(data == "AUTH-2009") {
                $(".error").hide();
                $(".popup_confirm_edit").show();
                $(".popup_confirm_edit .close_pop,.popup_confirm_edit .close_btn").click(function () {
                    $(".popup_confirm_edit").hide();
                });
                $("#desc_pop").html("회원님은 플레이오토를 통해 가입 하셨습니다." +'<br/>'+ "플레이오토를 통해 가입한 회원의 비밀번호는" +'<br/>'+  "플레이오토를 통해 변경 해주시기 바랍니다." +'<br/>'+ "감사합니다.");
            }
        })
            .fail(function (response) {
                alertCertify("정보 수정 중 에러가 발생 되었습니다.");
            });

    });
    function regPasswordType(data) {
        var regex = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,20}/;
        return regex.test(data);
    }
</script>