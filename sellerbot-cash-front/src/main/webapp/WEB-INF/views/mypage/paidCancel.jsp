<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="/assets/css/ticket.css?ver=20230407">
<script src="/assets/js/paidCancel.js"></script>

<input type="hidden" id="goods_req_seq_no" value="${goods_req_seq_no}">
<input type="hidden" id="ceo_nm" value="${paidInfo.ceo_nm}">
<input type="hidden" id="ceo_no" value="${paidInfo.ceo_no}">

<input type="hidden" id="cert" value="${cert}">

<input class="certInfo" type="hidden" id="CP_CD" />
<input class="certInfo" type="hidden" id="TX_SEQ_NO" />
<input class="certInfo" type="hidden" id="RSLT_CD" name="rlt_cd" />
<input class="certInfo" type="hidden" id="RSLT_MSG" name="rlt_msg" />
<input class="certInfo" type="hidden" id="RSLT_NAME" />
<input class="certInfo" type="hidden" id="RSLT_BIRTHDAY" name="req_birthday" />
<input class="certInfo" type="hidden" id="RSLT_SEX_CD" name="req_gender" />
<input class="certInfo" type="hidden" id="RSLT_NTV_FRNR_CD" name="req_nation" />
<input class="certInfo" type="hidden" id="DI" name="di_val" />
<input class="certInfo" type="hidden" id="CI" name="ci_val" />
<input class="certInfo" type="hidden" id="CI_UPDATE" />
<input class="certInfo" type="hidden" id="TEL_COM_CD" name="req_telcmmcd" />
<input class="certInfo" type="hidden" id="TEL_NO" name="req_ceph_no" />
<input class="certInfo" type="hidden" id="RETURN_MSG" name="retrunmsg" />

<div class="container">
    <!-- my_wra start -->
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
    </div>
    <!-- my_wra end -->
    <!-- container_wrapper start -->
    <div class="container_wrapper">
        
        <!-- //20200706_10 :: 해지 관련 요소 추가 Start -->
        <h1 id="close_account_title">
            <span class="cancel_type">
                <c:choose>
                    <c:when test="${!empty goods_seq_no}">${paidInfo.using_goods_list[0].goods_nm}</c:when>
                    <c:otherwise>일괄</c:otherwise>
                </c:choose>
            </span> 해지신청
        </h1>
        <!-- ticket_type_ingfo2 start -->
        <div id="ticket_type_ingfo2">
            <c:forEach items="${paidInfo.using_goods_list}" var="list" varStatus="status">
                <div class="row">
                    <div id="left">
                        <h1>${list.goods_nm}(${list.pay_typ_cd_nm})</h1>
                        <h2>이용기간: ${list.use_stt_dt} ~ ${list.use_end_dt}</h2>
                    </div>
                    <div id="right">
                        <h1 class="useing">이용 중</h1>
                    </div>
                </div>
            </c:forEach>
        </div>        
        <!-- ticket_type_ingfo2 end -->
        <!-- //20200706_10 :: 해지 관련 요소 추가 End -->
        <!-- m_sec1 start -->
        <div id="m_sec1">
            <div class="row">
                <h1 class="m_sec_title">해지 정보</h1>
                <!-- <img class="q_icon" src="/assets/images/icon/q.png" alt="">
                <div class="q_popup">
                    <h1>당일 해지시 전액 환불, 14일 해지시 50% 환불,<br> 14일 이후에는 환불 불가능</h1>
                    <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                </div> -->
            </div>
            <table id="useing_service">
                <thead>
                    <th>상품명</th>
                    <th>결제수단</th>
                    <th>결제 유형</th>
                    <th>최종결제금액</th>
                    <th>결제일</th>
                    <th>이용일</th>
                </thead>
                <tbody>
                    <c:forEach items="${paidInfo.using_goods_list}" var="list" varStatus="status">
                        <c:choose>
                            <c:when test="${list.req_goods_sts == 'PCOM'}">   
                                <tr>
                                    <td>${list.goods_nm}</td>
                                    <c:choose>
                                        <c:when test="${!empty list.pay_method}">
                                            <td>${list.pay_method}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>-</td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td>${list.pay_typ_cd_nm}</td>
                                    <c:if test="${!empty list.fna_pay_prc}">
                                        <td><fmt:formatNumber value="${list.fna_pay_prc}" pattern="#,###" />원</td>
                                    </c:if>
                                    <c:if test="${empty list.fna_pay_prc}">
                                        <td>-</td>
                                    </c:if>
                                    <c:choose>
                                        <c:when test="${!empty list.pay_date}"><td>${list.pay_date}</td></c:when>
                                        <c:otherwise><td>-</td></c:otherwise>
                                    </c:choose>
                                    <td>${list.used_days}일</td>
                                </tr>
                            </c:when> 
                            <c:otherwise>
                                <tr>
                                    <td>${list.goods_nm}</td>
                                    <c:choose>
                                        <c:when test="${!empty list.pay_method}">
                                            <td>${list.pay_method}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>-</td>
                                        </c:otherwise>
                                    </c:choose>
                                        <td>${list.pay_typ_cd_nm}</td>
                                        <td>0원</td>
                                        <c:choose>
                                            <c:when test="${!empty list.pay_date}"><td>${list.pay_date}</td></c:when>
                                            <c:otherwise><td>-</td></c:otherwise>
                                        </c:choose>
                                        <td>${list.used_days}일</td>
                                    </tr>        
                            </c:otherwise>
                        </c:choose>      
                    </c:forEach>
                </tbody>
            </table>
            <!-- TODO : 추후 결제유형이 추가되면 문구 변경 필요 -->
            <p id="this-alert">
                ※ 정기결제의 경우, 카드사를 통해 환불이 이루어집니다.<br/>
                ※ 환불은 이용권 일할 계산 후, 사용일만큼 공제 및 환불 수수료 공제 후 잔액이 최종 환불됩니다.
            </p>
            <div class="row">
                <h1 class="m_sec_title">예상 환불금액</h1>
            </div>
            <p>
                <jsp:useBean id="now" class="java.util.Date" />
                <fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm" var="today" />
                {<c:out value="${today}"/> 기준}
            </p>
            <table id="expected_refund_amount">
                <thead>
                    <th>실 결제금액<br>[A]</th>
                    <th>최대 이용가능일(유상)<br>[B]</th>
                    <th>이용일</th>
                    <th>잔여일<br>[C]</th>
                    <th>일 결제단가<br>D = [A / B]</th>
                    <th>환불금액<br>E = [D * C]</th>
                    <th>환불수수료<img class="q_icon" src="/assets/images/icon/q.png" alt="" style="display: inline-block; width: 1.2rem; margin-left: 5px; margin-bottom: -3px;"><br>F
                        <div class="q_popup" id="q_popup1" style="display:none; ">
                            <h1 style="color: #fff;
                            font-size: 0.75rem;
                            display: inline-block;
                            vertical-align: middle; text-align:left;"> ① (환불금액 > 10,000원) =  환불금액 – 환불금액의 10 %   [ X ] <br/>
 ② (환불금액 <= 10,000원) =  환불금액 - 1,000원  </h1>
                            <img class="close_popup" src="/assets/images/icon/x_white.png" alt="" style="display: inline-block;
                            vertical-align: middle;
                            margin-left: 1.5rem;
                            cursor: pointer;">
                        </div></th>
                    <th>최종 환불금액<br>[E - F]</th>
                </thead>
                <tbody>
                    <c:forEach items="${paidInfo.using_goods_list}" var="list" varStatus="status">
                        <c:choose>
                            <c:when test="${list.req_goods_sts == 'PCOM'}">   
                                <tr>
                                <c:choose>
                                    <c:when test="${!empty list.fna_pay_prc}">
                                        <td><fmt:formatNumber value="${list.fna_pay_prc}" pattern="#,###" />원</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>-</td>
                                    </c:otherwise>
                                </c:choose>
                                    <td>${list.total_use_days}일</td>
                                    <td>${list.used_days}일</td>
                                    <c:set var="remainingDays" value="${list.total_use_days - list.used_days}" />
                                    <td>${remainingDays}일</td>
                                    <c:set var="dayPrice" value="${list.fna_pay_prc / list.total_use_days}" />
                                    <td><fmt:formatNumber value="${dayPrice}" pattern="#,###.#" />원</td>
                                    <c:set var="refundAmount" value="${dayPrice * remainingDays}" />
                                    <td><fmt:formatNumber value="${refundAmount}" pattern="#,###.#" />원</td>
                                    <c:choose>
                                        <c:when test="${refundAmount >= 10000}">
                                            <c:set var="refundFee" value="${refundAmount * 0.1}" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="refundFee" value="1000" />
                                        </c:otherwise>
                                    </c:choose>
                                    <td><fmt:formatNumber value="${refundFee}" pattern="#,###.#" />원</td>
                                    <td><fmt:formatNumber value="${refundAmount - refundFee}" pattern="#,###" />원</td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <c:choose>
                                        <c:when test="${!empty list.fna_pay_prc}">
                                            <td>0 원</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>-</td>
                                        </c:otherwise>
                                    </c:choose>
                                        <td>${list.total_use_days}일</td>
                                        <td>${list.used_days}일</td>
                                        <c:set var="remainingDays" value="${list.total_use_days - list.used_days}" />
                                        <td>${remainingDays}일</td>
                                        <td>0원</td>
                                        <td>0원</td>
                                        <td>0원</td>
                                        <td>0원</td>
                                    </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tbody>
            </table>
            <div id="close_account_reason">
                <div class="line01">
                    <h1>해지사유</h1>
                    <select name="" id="reason">
                        <option value="1" disabled selected>선택해주세요</option>
                        <option value="T13">사업중단(폐업)</option>
                        <option value="T14">업종변경(전업)</option>
                        <option value="T15">다른 아이디로 재가입</option>
                        <option value="T16">타 사이트(유사서비스) 이용</option>
                        <option value="T17">데이터 불만</option>
                        <option value="T18">고객응대 불만</option>
                        <option value="T19">서비스이용료 불만</option>
                        <option value="T20">정보보호 침해 우려</option>
                        <option value="T21">기타</option>
                    </select>
                </div>
                <div class="line01">
                    <h1>상세사유<span>(선택)</span></h1>
                    <textarea name="" id="reason_detail" cols="30" rows="10"
                        placeholder="셀러봇캐시에&nbsp;하시고&nbsp;싶은&nbsp;말씀이&nbsp;있나요&#63;"></textarea>
                </div>
                <div class="line01">
                    <h1>휴대폰 본인인증<br><span>(대표자)</span></h1>
                    <div class="line02">
                        <h1>대표자명</h1>
                        <h2>${paidInfo.ceo_nm}</h2>
                        <button id="check_user_btn" onclick="authPhoneNumber();">본인인증 하기</button>
                        <!---본인인증 완료시-->
                        <div id="checked_user">
                            <img src="/assets/images/icon/check_green.png" alt="">
                            <p>인증이 완료되었습니다.</p>
                        </div>
                    </div>
                </div>
                <div id="line01">
                    <h1 id="disagree_text">
                        해지 사유를 선택해주세요.
                    </h1>
                </div>
                <div class="line01" id="btn_center">
                    <button type="button" id="back" onclick="historyBack();">
                        뒤로가기
                    </button>
                    <button type="button" id="agree" onclick="pressedBtnCancel();">
                        해지신청
                    </button>
                </div>
            </div>
        </div>
        <!-- m_sec1 end -->
        <!-- m_sec1_mb start  모바일 -->
        <div class="m_sevice_mb" id="m_sec1_mb">
            <div class="row">
                <h1 class="m_sec_title">해지 정보</h1>
            </div>
            <ul class="mb_table_service">
                <c:forEach items="${paidInfo.using_goods_list}" var="list" varStatus="status">
                    <c:choose>
                        <c:when test="${list.req_goods_sts == 'PCOM'}">  
                            <li>
                                <h1>상품명</h1>
                                <h2>${list.goods_nm}</h2>
                            </li>
                            <li>
                                <h1>결제수단 </h1>
                                <c:if test="${!empty list.pay_method}">
                                    <h2>${list.pay_method}</h2>
                                </c:if>
                                <c:if test="${empty list.pay_method}">
                                    <h2>-</h2>
                                </c:if>
                            </li>
                            <li>
                                <h1>결제유형</h1>
                                <h2>${list.pay_typ_cd_nm}</h2>
                            </li>
                            <li>
                                <h1>최종결제금액</h1>
                                <c:if test="${!empty list.fna_pay_prc}">
                                    <h2><fmt:formatNumber value="${list.fna_pay_prc}" pattern="#,###" />원</h2>
                                </c:if>
                                <c:if test="${empty list.fna_pay_prc}">
                                    <h2>-</h2>
                                </c:if>
                            </li>
                            <li>
                                <h1>결제일</h1>
                                <c:choose>
                                    <c:when test="${!empty list.pay_date}"><h2>${list.pay_date}</h2></c:when>
                                    <c:otherwise><h2>-</h2></c:otherwise>
                                </c:choose>
                            </li>
                            <li>
                                <h1>이용일</h1>
                                <h2>${list.used_days}일</h2>
                            </li> 
                        </c:when>
                        <c:otherwise>
                            <li>
                                <h1>상품명</h1>
                                <h2>${list.goods_nm}</h2>
                            </li>
                            <li>
                                <h1>결제수단 </h1>
                                <c:if test="${!empty list.pay_method}">
                                    <h2>${list.pay_method}</h2>
                                </c:if>
                                <c:if test="${empty list.pay_method}">
                                    <h2>-</h2>
                                </c:if>
                            </li>
                            <li>
                                <h1>결제유형</h1>
                                <h2>${list.pay_typ_cd_nm}</h2>
                            </li>
                            <li>
                                <h1>최종결제금액</h1>
                                <h2>0원</h2>
                            </li>
                            <li>
                                <h1>결제일</h1>
                                <c:choose>
                                    <c:when test="${!empty list.pay_date}"><h2>${list.pay_date}</h2></c:when>
                                    <c:otherwise><h2>-</h2></c:otherwise>
                                </c:choose>
                            </li>
                            <li>
                                <h1>이용일</h1>
                                <h2>${list.used_days}일</h2>
                            </li> 
                        </c:otherwise>
                    </c:choose>            
                    <li>
                        <P id="this-alert">
                            ※ 정기결제의 경우, 카드사를 통해 환불이 이루어집니다.<br/>
                            ※ 환불은 이용권 일할 계산 후, 사용일만큼 공제 및 환불 수수료 공제 후 잔액이 최종 환불됩니다.
                        </P>
                    </li>
                </c:forEach>
            </ul>
            <div class="row">
                <h1 class="m_sec_title">예상 환불금액</h1>
                <!-- <img class="q_icon" src="/assets/images/icon/q.png" alt="">
                <div class="q_popup">
                    <h1>이용권을 변경하고 싶으시다면 변경 버튼을 눌러주세요~</h1>
                    <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                </div> -->
            </div>
            <ul class="mb_table_service">
                <c:forEach items="${paidInfo.using_goods_list}" var="list" varStatus="status">
                    <c:choose>
                        <c:when test="${list.req_goods_sts == 'PCOM'}">  
                            <li>
                                <h1>실 결제금액<br/>[A]</h1>
                                <h2>
                                <c:choose>
                                    <c:when test="${!empty list.fna_pay_prc}">
                                        <fmt:formatNumber value="${list.fna_pay_prc}" pattern="#,###" />원
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                                </h2>
                            </li>
                            <li>
                                <h1>최대 이용가능일 (유상)<br/>[B]</h1>
                                <h2>${list.total_use_days}일</h2>
                            </li>
                            <li>
                                <h1>이용일</h1>
                                <h2>${list.used_days}일</h2>
                            </li>
                            <c:set var="remainingDays" value="${list.total_use_days - list.used_days}" />
                            <li>
                                <h1>잔여일<br/>[C]</h1>
                                <h2>${remainingDays}일</h2>
                            </li>
                            <c:set var="dayPrice" value="${list.fna_pay_prc / list.total_use_days}" />
                            <li>
                                <h1>일 결제단가<br/>D = [A / B]</h1>
                                <h2><fmt:formatNumber value="${dayPrice}" pattern="#,###.#" />원</h2>
                            </li>
                            <c:set var="refundAmount" value="${dayPrice * remainingDays}" />
                            <li>
                                <h1>환불금액<br/>E = [D * C]</h1>
                                <h2><fmt:formatNumber value="${refundAmount}" pattern="#,###.#" />원</h2>
                            </li>
                            <c:choose>
                                <c:when test="${refundAmount >= 10000}">
                                    <c:set var="refundFee" value="${refundAmount * 0.1}" />
                                </c:when>
                                <c:otherwise>
                                    <c:set var="refundFee" value="1000" />
                                </c:otherwise>
                            </c:choose>
                            <li>
                                <h1>환불수수료
                                    <img class="q_icon" src="/assets/images/icon/q.png" alt="" style="display: inline-block; width: 1.3rem; margin-left: 5px; margin-bottom: -3px;">
                                    <br/>[F]
                                    <div class="q_popup" id="q_popup2_mb" style="display:none;">
                                        <h1 style="color: #fff;
                                        font-size: 0.9rem;
                                        width: 85%;
                                        display: inline-block;
                                        vertical-align: middle; text-align:left;">① (환불금액 > 10,000원) =  환불금액 – 환불금액의 10 %   [ X ] <br/>
                                        ② (환불금액 <= 10,000원) =  환불금액 - 1,000원</h1>
                                        <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                                    </div>
                                </h1>
                                <h2><fmt:formatNumber value="${refundFee}" pattern="#,###.#" />원</h2>
                            </li>
                            <li>
                                <h1>최종 환불금액<br/>[E - F]</h1>
                                <h2><fmt:formatNumber value="${refundAmount - refundFee}" pattern="#,###" />원</h2>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <h1>실 결제금액<br/>[A]</h1>
                                <h2>
                                <c:choose>
                                    <c:when test="${!empty list.fna_pay_prc}">
                                        0원
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                                </h2>
                            </li>
                            <li>
                                <h1>최대 이용가능일 (유상)<br/>[B]</h1>
                                <h2>${list.total_use_days}일</h2>
                            </li>
                            <li>
                                <h1>이용일</h1>
                                <h2>${list.used_days}일</h2>
                            </li>
                            <c:set var="remainingDays" value="${list.total_use_days - list.used_days}" />
                            <li>
                                <h1>잔여일<br/>[C]</h1>
                                <h2>${remainingDays}일</h2>
                            </li>
                            <c:set var="dayPrice" value="${list.fna_pay_prc / list.total_use_days}" />
                            <li>
                                <h1>일 결제단가<br/>D = [A / B]</h1>
                                <h2>0원</h2>
                            </li>
                            <c:set var="refundAmount" value="${dayPrice * remainingDays}" />
                            <li>
                                <h1>환불금액<br/>E = [D * C]</h1>
                                <h2>0원</h2>
                            </li>
                            <li>
                                <h1>환불수수료
                                    <img class="q_icon" src="/assets/images/icon/q.png" alt="" style="display: inline-block; width: 1.3rem; margin-left: 5px; margin-bottom: -3px;">
                                    <br/>[F]
                                    <div class="q_popup" id="q_popup2_mb" style="display:none;">
                                        <h1 style="color: #fff;
                                        font-size: 0.9rem;
                                        width: 85%;
                                        display: inline-block;
                                        vertical-align: middle; text-align:left;">① (환불금액 > 10,000원) =  환불금액 – 환불금액의 10 %   [ X ] <br/>
                                        ② (환불금액 <= 10,000원) =  환불금액 - 1,000원</h1>
                                        <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                                    </div>
                                </h1>
                                <h2>0원</h2>
                            </li>
                            <li>
                                <h1>최종 환불금액<br/>[E - F]</h1>
                                <h2>0원</h2>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </ul>
            <div id="close_account_reason">
                <div class="line01">
                    <h1>해지사유</h1>
                    <select name="" id="reason_mb">
                        <option value="1" disabled selected>선택해주세요</option>
                        <option value="T13">사업중단(폐업)</option>
                        <option value="T14">업종변경(전업)</option>
                        <option value="T15">다른 아이디로 재가입</option>
                        <option value="T16">타 사이트(유사서비스) 이용</option>
                        <option value="T17">데이터 불만</option>
                        <option value="T18">고객응대 불만</option>
                        <option value="T19">서비스이용료 불만</option>
                        <option value="T20">정보보호 침해 우려</option>
                        <option value="T21">기타</option>
                    </select>
                </div>
                <div class="line01">
                    <h1>상세사유<span>(선택)</span></h1>
                    <textarea name="" id="reason_detail_mb" cols="30" rows="10"
                        placeholder="셀러봇캐시에&nbsp;하시고&nbsp;싶은&nbsp;말씀이&nbsp;있나요&#63;"></textarea>
                </div>
                <div class="line01">
                    <h1>휴대폰 본인인증<br><span>(대표자)</span></h1>
                    <div class="line02">
                        <h1>대표자명</h1>
                        <h2>${paidInfo.ceo_nm}</h2>
                        <button id="check_user_btn_mb" onclick="authPhoneNumber();">본인인증 하기</button>
                        <!---본인인증 완료시-->
                        <div id="checked_user_mb" style="display: none;">
                            <img src="/assets/images/icon/check_green.png" alt="">
                            <p>인증이 완료되었습니다.</p>
                        </div>
                    </div>
                </div>
                <div id="line01">
                    <h1 id="disagree_text_mb" style="display: none;">
                        해지 사유를 선택해주세요.
                    </h1>
                </div>
                <div class="line01" id="btn_center">
                    <button type="button" id="back" onclick="historyBack();">
                        뒤로가기
                    </button>
                    <button type="button" id="agree" onclick="pressedBtnCancel();">
                        해지신청
                    </button>
                </div>
            </div>
        </div>
        <!-- m_sec1_mb end -->
    </div>
    <style>
        #useing_service {
            margin-bottom: 0.5rem !important;
        }

        #btn_center {
            text-align: center;
            margin-top: 6rem !important;
        }

        #select option:first-child {
            color: #888888;
        }

        @media(max-width:880px){
            .m_sevice_mb div.row h1.m_sec_title {
                vertical-align: middle;
                font-size: 1.1rem;
                font-weight: bold;
                color: #1A1A1A;
                display: inline-block;
                padding-left: 0;
            }

            .m_sevice_mb ul.mb_table_service li:last-of-type{
                border-bottom: none;
            }
        }
    </style>
    <!-- close_account_alert_div start -->
    <div id="close_account_alert_div">
        <ul>
            <img src="/assets/images/icon/x_black.png" alt="">
            <li>
                <c:set var="bankBotPromotionYn" value="N" />
                <c:forEach items="${paidInfo.using_goods_list}" var="list" varStatus="status">
                    <c:if test="${list.goods_nm eq '뱅크봇' && list.pay_typ_cd_nm eq '외부연동 무료 이용권'}">
                        <c:set var="bankBotPromotionYn" value="Y" />
                    </c:if>
                </c:forEach>
                
                <c:choose>
                    <c:when test="${bankBotPromotionYn eq 'Y'}">
                        <h1>
                            서비스 이용 종료 후에는 재신청을 원하시는 경우<br>
                            <span style="color: blue;">이용요금안내</span> 메뉴에서 신청하여 주시길 바랍니다.<br><br>
                            정말<span>이용권 해지</span>신청을 하시겠습니까?
                        </h1>
                    </c:when>
                    <c:otherwise>
                        <h1><span>
                            지금 해지하시면 정산예정금 상세정보와<br>매일 보내드리는 알림톡 Report 수신이 불가하며,<br>
                            그래도 정말 해지 하시겠어요?
                        </span></h1>
                    </c:otherwise>
                </c:choose>
            </li> 
            <li>
                <c:choose>
                    <c:when test="${bankBotPromotionYn eq 'Y'}">
                        <button type="button" id="close_this_modal">닫기</button>
                        <button type="button" id="account_agree">해지</button>
                    </c:when>
                    <c:otherwise>
                        <button type="button" id="account_agree"style="background: #C0C0C0;">혜택 포기하기(해지)</button>
                        <button type="button" id="close_this_modal" style="background: #009ACA;">혜택 유지하기</button>
                    </c:otherwise>
                </c:choose>
            </li>
        </ul>
        <div id="bg"></div>
    </div>
    <!-- close_account_alert_div end -->
</div>
<style>
    .my_wra {
        padding-top: 5em;
        min-height: auto;
        overflow-x: hidden;
    }
    .q_icon {
    display: inline-block;
    width: 1rem;
    vertical-align: middle;
    margin-left: 0.3rem;
    cursor: pointer;
    height: 1rem;
    }
    th div.q_popup {
    display: inline-block;
    background: #1a1a1a;
    padding: 0.5rem 1rem;
    border-radius: 5px;
    display: none;
    position: absolute;
    margin-left: 1.5rem;
    }

    @media(max-width:880px){
        li .q_popup {
            margin-left: 0;
            position: absolute;
            z-index: 99;
            max-width: 80%;
            left: 2rem;
            width: 100%;
            bottom: -41rem;
        }
    }
    
    li .q_popup{
        background: #1a1a1a;
    padding: 0.5rem 1rem;
    border-radius: 5px;
    }

    @media (max-width: 880px){
    li .q_popup img.close_popup {
        width: 2rem;
        margin-right: 0;
        display: inline-block;
        position: absolute;
        right: 1rem;
    }
}

</style>
<script>
    $("th .q_icon").click(function () {
        $("th .q_popup").css("display", "inline-block");
    });

    $("th .q_popup .close_popup").click(function () {
        $("th .q_popup").css("display", "none");
    })

    $("li .q_icon").click(function () {
        $("#q_popup2_mb").css("display", "inline-block");
    });

    $("li .q_popup .close_popup").click(function () {
        $("#q_popup2_mb").css("display", "none");
    })

    $("#close_account_alert_div img, #close_this_modal").click(function() {
        $("#close_account_alert_div").css("display","none");
        $("html,body").css("overflowY","visible");
    });

    $("#account_agree").click(function() {
        // $("#close_account_alert_div ul li h1").text("이용권 해지가 완료되었습니다.");
        $("#close_account_alert_div").css("display","none");
        $("html,body").css("overflowY","visible");

        pressedBtnAgree();
    });
</script>