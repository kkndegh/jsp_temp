<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="/assets/js/mngAcct.js?ver=20200819_01"></script>

<div class="container">
	<div class="my_wra">
		<div class="menu_top">
			<p class="menu_name">마이페이지</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">정산계좌 관리</p>
			<p class="desc">각 은행에서 <font style="color: red;">빠른조회서비스</font>에 가입하신 후 사용가능한 서비스입니다.</p>
		</div>
		<div class="my_area">
			<div class="my_section">
				<div class="sub_menu_title">
					<div class="back_edit_page">
						<button class="backbtn" onclick="history.go(-1);">&#60; 이전으로</button>
					</div>
					계좌 수정
				</div>
				<div class="guide_register">
					<h1>정산계좌 통합관리 도움말</h1>
					<span>
						정산계좌 통합관리 서비스를 이용하시려면 각 은행별 홈페이지에서 빠른조회서비스 등록을 진행하신 후<br />
						계좌정보를 입력하셔야 합니다. 하단의 은행명을 선택해주시면 은행별 빠른조회서비스 등록 절차를 확인하실 수 있습니다.
					</span>
					<span class="guide_register_detail">
						<b>산업은행</b><br />
						▣ 개인뱅킹<br />
						① 산업은행 홈페이지 <a href="http://www.kdb.co.kr" target="_brank"
							style="color: blue;">(http://www.kdb.co.kr)<a>에 접속<br />
								② 공인인증서를 통하여 인터넷뱅킹 로그인<br />
								③ [뱅킹관리 → 계좌관리 → 계좌관리] 에서 등록
								<br />
								<br />
								▣ 개인뱅킹<br />
								① 산업은행 홈페이지 <a href="http://www.kdb.co.kr" target="_brank"
									style="color: blue;">(http://www.kdb.co.kr)<a>에 접속<br />
										② 공인인증서를 통하여 인터넷뱅킹 로그인<br />
										③ - A. 기업뱅킹 (USB 1개 사용) : [뱅킹관리 → 계좌관리 → 빠른조회대상계좌설정]에서 등록<br />
										③ - B. 기업뱅킹 (USB 2개 이상) : [대표관리자 인터넷뱅킹 접속 → 뱅킹관리 → 계좌관리 → 빠른조회대상계좌설정] 에서 등록
					</span>
				</div>

				<div class="frm_account_my">

					<p class="account_lb">계좌정보 입력</p>

					<div class="item_input_acct bank_cert_m_seq_no_use_yn">
						<p class="lb_input">은행명</p>
						<div class="box_input_acct">
							<select class="sctbox" id="bank_cert_m_seq_no" required="required">
							</select>
						</div>
					</div>
					<div class="item_input_acct bank_id_use_yn">
						<p class="lb_input">빠른조회<br>은행아이디</p>
						<div class="box_input_acct">
							<input id="bank_id" type="text" class="textbox_mb" maxlength="25" required="required">
							<p id="bank_id_error" class="error" style="display:none;">*은행아이디를 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct bank_passwd_use_yn">
						<p class="lb_input">빠른조회<br>은행비밀번호</p>
						<div class="box_input_acct">
							<input id="bank_passwd" type="password" class="textbox_mb" maxlength="20"
								required="required">
							<p id="bank_passwd_error" class="error" style="display:none;">*은행비밀번호 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct dpsi_nm_use_yn">
						<p class="lb_input">예금주</p>
						<div class="box_input_acct">
							<input id="dpsi_nm" type="text" class="textbox_mb" maxlength="20" required="required">
							<p id="dpsi_nm_error" class="error" style="display:none;">*예금주를 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct acct_no_use_yn">
						<c:choose>
							<c:when test="{bank_cert_m_seq_no == 10}">
								<p class="lb_input" id="acctNoTitle">안전계좌번호</p>
							</c:when>
							<c:otherwise>
								<p class="lb_input" id="acctNoTitle">계좌번호</p>
							</c:otherwise>
						</c:choose>
						<div class="box_input_acct">
							<!-- 계좌번호가 숫자 + 알파벳 혼합인 경우 예외처리 -->
							<!-- 대구은행 seq 10 -->
							<c:choose>
								<c:when test="{bank_cert_m_seq_no == 10}">
									<input id="acct_no" type="text" autocomplete="off" class="textbox_mb" maxlength="20"
										required="required" data-valitype="acctNo_numberNLetter">
								</c:when>
								<c:otherwise>
									<input id="acct_no" type="text" autocomplete="off" class="textbox_mb" maxlength="20"
										required="required" data-valitype="acctNo">
								</c:otherwise>
							</c:choose>
							<p id="acct_no_error" class="error" style="display:none;">*계좌번호를 바르게 입력해주세요.</p>
						</div>

					</div>
					<div class="item_input_acct acct_passwd_use_yn">
						<c:choose>
							<c:when test="{bank_cert_m_seq_no == 10}">
								<p class="lb_input" id="acctPwdTitle">안전계좌비밀번호</p>
							</c:when>
							<c:otherwise>
								<p class="lb_input" id="acctPwdTitle">계좌비밀번호</p>
							</c:otherwise>
						</c:choose>
						<div class="box_input_acct">
							<input id="acct_passwd" type="password" autocomplete="new-password" class="textbox_mb"
								maxlength="20" required="required">
							<p id="acct_passwd_error" class="error" style="display:none;">*계좌비밀번호를 바르게 입력해주세요.</p>
						</div>
					</div>

					<div class="item_input_acct ctz_biz_no_use_yn">
						<p class="lb_input">주민등록<br>(사업자)번호</p>
						<div class="box_input_acct">
							<input id="ctz_biz_no" type="text" placeholder="주민번호 앞 6자리 또는 사업자번호10자리만 입력"
								class="textbox_mb" maxlength="10" required="required" data-valitype="ctzBizno">
							<p id="ctz_biz_no_error" class="error" style="display:none;">*주민등록(사업자) 번호를 바르게 입력해주세요.</p>
						</div>
					</div>
					<div class="item_input_acct crc_use_cd">
						<p class="lb_input">통화</p>
						<div class="box_input_acct">
							<select id="crc" class="sctbox">
								<option value="">선택</option>
								<c:forEach items="${codeList }" var="code" varStatus="index">
									<option value="${code.com_cd }">${code.cd_nm }</option>
								</c:forEach>
							</select>
							<p id="crc_error" class="error" style="display:none;">*통화를 선택해주세요.</p>
						</div>
					</div>
					<div class="btn_frm_group">
						<button id="btnSave">입력완료</button>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>
<div class="title_bg">
	<p class="desc">각 은행별 빠른조회서비스 등록절차가 궁금하시다면 고객센터의<span id="btnGoFaq" class="pointer"
			style="color: #2dabe8">FAQ</span>를 확인해주세요</p>
</div>
<script>
	var _bankList;
	var _acctInfo;

	<c:if test="${not empty bankList }">
		_bankList = ${bankList};
	</c:if>
	<c:if test="${not empty acctInfo }">
		_acctInfo = ${acctInfo};
	</c:if>

			$(document).ready(function () {
				// 시작
				function fnInit() {
					// 은행 설정
					var selectBank = $("#bank_cert_m_seq_no");
					selectBank.empty();
					var options = [];
					$.each(_bankList, function () {
						options.push("<option value=\"" + this.bank_cert_m_seq_no + "\">" + this.bank_nm + " </option>");
					});
					selectBank.append(options.join(""));

					fnViewChange(_acctInfo.bank_cert_m_seq_no, _bankList);

					var keys = Object.keys(_acctInfo);
					var key;
					for (var i in keys) {
						key = keys[i];
						$("#" + key).val(_acctInfo[key]);
					}
				}

				$("#bank_cert_m_seq_no").on("change", function () {
					fnViewChangeForMdfy(this.value, _bankList);
				});


				$("#btnSave").on("click", function () {
					if (!isVali()) { return false; }
					var bankInfo = fnGetJsonData($("#bank_cert_m_seq_no").val(), _bankList);

					// 기본 공통 값
					var param = {
						"bank_cert_m_seq_no": $("#bank_cert_m_seq_no").val(),
						"reco_yn": "N"
					};

					var input = $(".frm_account_my").find("[required]");
					var jqThis;
					$.each(input, function () {
						jqThis = $(this);
						if (nonNull(jqThis.val())) {
							param[jqThis.attr("id")] = jqThis.val();
						}
					});

					if (bankInfo.crc_use_cd == "S") {
						param.crc = $("#crc").val();
					}

					$.ajax({
						url: '/sub/my/account/' + _acctInfo.cust_acct_seq_no
						, type: 'put'
						, async: false
						// , dataType: 'text json'   
						, contentType: 'application/json'
						, data: JSON.stringify([param])
						, success: function (response) {
							showAlert("계좌가 변경되었습니다.", function () {
								location.href = "/sub/my/account";
							});
						}
						, error: function (error) {
							if (isJsonString(error.responseText)) {
								var data = JSON.parse(error.responseText);
								showAlert(data.comment);
							} else {
								showAlert("저장실패 하였습니다. <br> 관리자에게 문의하세요.");
							}
						}
					});

				});

				$("#btnGoFaq").on("click", function () {
					location.href = "/pub/cs/faq?title=" + $("#bank_cert_m_seq_no option:selected").text() + "&page=1&faq_typ_cd=F07";
				});

				fnInit();
			});
</script>