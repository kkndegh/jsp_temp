<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="/assets/js/select2/select2.min.js"></script>
<script src="/assets/js/mngMall.js?ver=20221117_01"></script>

<style>
    .my_section {
        max-width: 51rem !important;
    }

    @media (max-width: 1024px) {
        .my_area .market_box {
            float: left;
            margin: 1.625em 0 0;
            margin-top: 0;
        }

        /* .my_area .choiced_mk_box {
            padding: 0 0 0 7em;
        } */

        .my_area .choiced_mk_box {
            margin-left: 3rem;
            margin-top: -5rem;
            padding: 0;
        }

        .list_choiced_mk {
            padding: 0;
        }
    }

    @media (max-width: 990px) {
        .my_section {
            max-width: 47rem !important;
        }

        .my_area .choiced_mk_box {
            padding: 0 0 0 4em;
        }
    }

    @media (max-width: 720px) {
        .market_box_find {
            width: 100%;
        }

        .my_area .market_box {
            width: 100%;
            float: unset;
        }

        .my_area .choiced_mk_box {
            margin: 0;
            padding: 0;
        }

        .choiced_mk_box .status {
            margin-top: 2rem;
        }
    }
</style>

<input type="hidden" id="currentRegCount" value="${custMall.all_mall_cnt - custMall.del_mall_cnt - custMall.mall008.dup_mall_cnt}">
<input type="hidden" id="maxRegCount" value="${maxRegCountInfo.SALE_MALL_REG_ID_PSB_CNT}">
<div class="container">
    

    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
                    <c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
                        <c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
                            <c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">    
                                <c:choose>
                                    <c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
                                        <li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">판매몰 관리</p>
            <p class="desc">등록된 판매몰을 수정, 삭제, 추가 할 수 있습니다..</p>
        </div>
        <div class="container_wrapper">
            <ul id="two_menu_ul" style="margin-top: 2rem; margin-bottom: 2rem;">
                <li onclick="location.href='/sub/my/marketRegList'">
                    <a>판매몰 현황</a>
                </li>
                <li class="blue_active2" onclick="location.href='/sub/my/market'">
                    <a>판매몰 등록/수정/삭제</a>
                </li>
            </ul>
        </div>
        <div class="my_area">
            <div class="my_section">
                <div class="market_choice clearfix">
                    <div class="market_box_find">
                        <div class="ui-widget">
                            <input placeholder="판매몰 검색" class="market_box_find_text_area">
                        </div>
                    </div>
                    <div class="market_box">
                        <div class="search_market">
                        </div>
                        <div class="group_list_mall" data-type-mk="mk_type1">
                            <p class="type_market open"><span>오픈마켓</span> <img
                                    src="/assets/images/member/btn_arrows.png" alt=""></p>
                            <ul class="scrollblack mall_lists_1">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <c:if test="${mall.mall_typ_cd eq 'OPM' && mall.mall_sts_cd eq 'NOR' }">
                                        <li name="open_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>
                                            <span name="open_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="group_list_mall" data-type-mk="mk_type2">
                            <p class="type_market"><span>소셜커머스</span> <img src="/assets/images/member/btn_arrows.png"
                                    alt=""></p>
                            <ul class="scrollblack mall_lists_2">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <c:if test="${mall.mall_typ_cd eq 'SNM' && mall.mall_sts_cd eq 'NOR' }">
                                        <li name="soci_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>
                                            <span name="soci_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="group_list_mall" data-type-mk="mk_type3">
                            <p class="type_market"><span>종합몰(백화점)</span> <img src="/assets/images/member/btn_arrows.png"
                                    alt=""></p>
                            <ul class="scrollblack mall_lists_3">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <!-- 2020-09-10 롯데ON 노출 조건 반영, 롯데ON은 오픈마켓, 종합몰(백화점) 모두 표시 -->
                                    <c:if test="${(mall.mall_typ_cd eq 'COM' && mall.mall_sts_cd eq 'NOR') || (mall.mall_typ_cd eq 'OPM' && mall.mall_cd eq '060' && mall.mall_sts_cd eq 'NOR')}">
                                        <li name="syn_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>
                                            <span name="syn_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="group_list_mall" data-type-mk="mk_type4">
                            <p class="type_market"><span>PG사/VAN사</span> <img src="/assets/images/member/btn_arrows.png"
                                    alt=""></p>
                            <ul class="scrollblack mall_lists_4">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <c:if test="${mall.mall_typ_cd eq 'VAM' && mall.mall_sts_cd eq 'NOR' or mall.mall_typ_cd eq 'PMM' && mall.mall_sts_cd eq 'NOR' }">
                                        <li name="pg_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>    
                                            <span name="pg_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="group_list_mall" data-type-mk="mk_type5">
                            <p class="type_market"><span>전문몰</span> <img src="/assets/images/member/btn_arrows.png"
                                    alt=""></p>
                            <ul class="scrollblack mall_lists_5">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <c:if test="${mall.mall_typ_cd eq 'SPM' && mall.mall_sts_cd eq 'NOR'}">
                                        <li name="spe_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>
                                            <span name="spe_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="group_list_mall">
                            <p class="type_market"><span>3PL물류(상품자산)</span> <img src="/assets/images/member/btn_arrows.png"
                                    alt=""></p>
                            <ul class="scrollblack mall_lists_6">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <c:if test="${mall.mall_typ_cd eq 'OFM' && mall.mall_sts_cd eq 'NOR' }">
                                        <li name="off_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>
                                            <span name="off_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="group_list_mall">
                            <p class="type_market"><span>기타</span> <img src="/assets/images/member/btn_arrows.png"
                                    alt=""></p>
                            <ul class="scrollblack mall_lists_8">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <c:if test="${mall.mall_typ_cd eq 'ETC' && mall.mall_sts_cd eq 'NOR' }">
                                        <li name="etc_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>
                                            <span name="etc_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="group_list_mall" data-type-mk="mk_type7">
                            <p class="type_market" style="background: #ffcb00; color: #000;">
                                <span>오픈예정몰</span> <img src="/assets/images/member/btn_arrows.png" alt="">
                            </p>
                            <ul class="scrollblack mall_lists_7">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <c:if test="${mall.mall_typ_cd eq 'OCM' && mall.mall_sts_cd eq 'NOR' }">
                                        <li name="spe_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>
                                            <span name="spe_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="group_list_mall" data-type-mk="mk_type9" style="display: none;">
                            <p class="type_market">
                                <span>준비중인몰</span> <img src="/assets/images/member/btn_arrows.png" alt="">
                            </p>
                            <ul class="scrollblack mall_lists_9">
                                <c:set var="cnt" value="1" />
                                <c:forEach items="${mallList}" var="mall" varStatus="status">
                                    <c:if test="${mall.mall_typ_cd eq 'OCM' && mall.mall_sts_cd eq 'RDY' }">
                                        <li name="spe_${cnt }" id="mallList${mall.mall_cd}">
                                            <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                            </c:if>
                                            <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                            </c:if>
                                            <span name="spe_${mall.mall_cd }_tag">${mall.mall_nm }</span>
                                            <input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
                                                class="sub_mall_cert_1st_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
                                                class="sub_mall_cert_2nd_nm" />
                                            <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
                                                class="mall_cert_2nd_passwd_nm" />
                                            <input type="hidden" value="${mall.sub_mall_cert_step }"
                                                class="sub_mall_cert_step" />
                                            <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                                            <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                                            <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
                                                class="sub_mall_auth_1st_esse_yn" />
                                            <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
                                                class="sub_mall_auth_2nd_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                            <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                            <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                            <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                                            <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                            <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                            <input type="hidden" value="${mall.help_info }" class="help_info" />
                                            <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                            <input type="hidden" value="${mall.video_url }" class="video_url" />
                                            <input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
                                            <input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
                                            <c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
                                                varStatus="typStatus">
                                                <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
                                                    value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                                            </c:forEach>
                                        </li>
                                        <c:set var="cnt" value="${cnt + 1 }" />
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                    <div class="choiced_mk_box">
                        <div class="status">
                            <p id="filterAll" class="all pointer">전체 <span
                                    class="all_num">${custMall.all_mall_cnt-custMall.del_mall_cnt - custMall.mall008.dup_mall_cnt }</span></p> /
                            <p id="filterPass" class="pass pointer">정상 <span>${custMall.nor_mall_cnt }</span></p> /
                            <p id="filterWarning" class="warning pointer">점검 <span>${custMall.ins_mall_cnt }</span></p>
                            /
                            <p id="filterError" class="error pointer">오류 <span>${custMall.err_mall_cnt}</span></p>
                        </div>
                        <div class="list_choiced_mk">
                            <p class="title_list_choiced">입력항목</p> 
                            <!--리스트 없을때-->
                            <div class="list_choiced_empty_box"
                                style="display: ${fn:length(custMall.mall) == 0 ? 'block' : 'none'}">
                                <p>좌측에서<br>
                                    판매몰을 선택해주세요.</p>
                            </div>
                            <!--리스트 있을때-->
                            <div class="list_choiced_mk_box scrollblack"
                                style="display: ${fn:length(custMall.mall) == 0 ? 'none' : 'block'}">
                                <div class="group_mall group_mall_warning">
                                    <p class="type_market_choiced">점검</p>
                                    <ul class="open_append">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if test="${mall.cust_mall_sts_cd eq 'INS' and mall.del_yn ne 'Y'}">
                                                <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                    data-mall_cd="${mall.mall_cd }">
                                                    <p class="mk_name">${mall.mall_cd_nm }</p>
                                                    <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                    <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                    <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                    <div class="account_btn">
                                                        <p class="btn_edit"
                                                            onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )">
                                                            <img src="/assets/images/member/icon_2.png" alt="">
                                                        </p>
                                                        <p class="btn_remove"
                                                            data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                            onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"> <img
                                                                src="/assets/images/member/icon_1.png" alt=""></p>
                                                    </div>
                                                </li>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall group_mall_error">
                                    <p class="type_market_choiced">오류</p>
                                    <ul class="open_append">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if test="${mall.cust_mall_sts_cd eq 'ERR' and mall.del_yn ne 'Y'}">
                                                <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                    data-mall_cd="${mall.mall_cd }">
                                                    <p class="mk_name">${mall.mall_cd_nm }</p>
                                                    <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                    <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                    <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                    <div class="account_btn">
                                                        <p class="btn_edit"
                                                            onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                src="/assets/images/member/icon_2.png" alt=""></p>
                                                        <p class="btn_remove"
                                                            data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                            onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                src="/assets/images/member/icon_1.png" alt=""></p>
                                                    </div>
                                                </li>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall" data-choice-mk="mk_type1">
                                    <p class="type_market_choiced">오픈마켓</p>
                                    <ul class="open_append">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'OPM' and mall.del_yn ne 'Y' }">
                                                <c:if
                                                    test="${mall.cust_mall_sts_cd eq 'NR' or mall.cust_mall_sts_cd eq 'REG' or mall.cust_mall_sts_cd eq 'MOD' }">
                                                    <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                        data-mall_cd="${mall.mall_cd }">
                                                        <p class="mk_name">${mall.mall_cd_nm }</p>
                                                        <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                        <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                        <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                        <div class="account_btn">
                                                            <p class="btn_edit"
                                                                onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_2.png" alt=""></p>
                                                            <p class="btn_remove"
                                                                data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                                onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_1.png" alt=""></p>
                                                        </div>
                                                    </li>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall" data-choice-mk="mk_type2">
                                    <p class="type_market_choiced">소셜커머스</p>
                                    <ul class="open_append_2">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'SNM' and mall.del_yn ne 'Y'}">
                                                <c:if
                                                    test="${mall.cust_mall_sts_cd eq 'NR' or mall.cust_mall_sts_cd eq 'REG' or mall.cust_mall_sts_cd eq 'MOD' }">
                                                    <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                        data-mall_cd="${mall.mall_cd }">
                                                        <p class="mk_name">${mall.mall_cd_nm }</p>
                                                        <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                        <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                        <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                        <div class="account_btn">
                                                            <p class="btn_edit"
                                                                onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_2.png" alt=""></p>
                                                            <p class="btn_remove"
                                                                data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                                onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_1.png" alt=""></p>
                                                        </div>
                                                    </li>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall" data-choice-mk="mk_type3">
                                    <p class="type_market_choiced">종합몰(백화점)</p>
                                    <ul class="open_append_3">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <!-- 2020-09-10 롯데ON 노출 조건 반영, 롯데ON은 오픈마켓, 종합몰(백화점) 모두 표시 -->
                                            <c:if test="${(mall.mall_typ_cd eq 'COM' and mall.del_yn ne 'Y') || (mall.mall_typ_cd eq 'OPM' and mall.del_yn ne 'Y' and mall.mall_cd eq '060')}">
                                                <c:if
                                                    test="${mall.cust_mall_sts_cd eq 'NR' or mall.cust_mall_sts_cd eq 'REG' or mall.cust_mall_sts_cd eq 'MOD' }">
                                                    <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                        data-mall_cd="${mall.mall_cd }">
                                                        <p class="mk_name">${mall.mall_cd_nm }</p>
                                                        <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                        <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                        <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                        <div class="account_btn">
                                                            <p class="btn_edit"
                                                                onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_2.png" alt=""></p>
                                                            <p class="btn_remove"
                                                                data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                                onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_1.png" alt=""></p>
                                                        </div>
                                                    </li>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall" data-choice-mk="mk_type4">
                                    <p class="type_market_choiced">PG사/VAN사</p>
                                    <ul class="open_append_4">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if
                                                test="${(mall.mall_typ_cd eq 'VAM' or mall.mall_typ_cd eq 'PMM') and mall.del_yn ne 'Y' }">
                                                <c:if
                                                    test="${mall.cust_mall_sts_cd eq 'NR' or mall.cust_mall_sts_cd eq 'REG' or mall.cust_mall_sts_cd eq 'MOD' }">
                                                    <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                        data-mall_cd="${mall.mall_cd }">
                                                        <p class="mk_name">${mall.mall_cd_nm }</p>
                                                        <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                        <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                        <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                        <div class="account_btn">
                                                            <p class="btn_edit"
                                                                onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_2.png" alt=""></p>
                                                            <p class="btn_remove"
                                                                data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                                onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_1.png" alt=""></p>
                                                        </div>
                                                    </li>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall" data-choice-mk="mk_type5">
                                    <p class="type_market_choiced">전문몰</p>
                                    <ul class="open_append_5">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'SPM' and mall.del_yn ne 'Y' }">
                                                <c:if
                                                    test="${mall.cust_mall_sts_cd eq 'NR' or mall.cust_mall_sts_cd eq 'REG' or mall.cust_mall_sts_cd eq 'MOD' }">
                                                    <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                        data-mall_cd="${mall.mall_cd }">
                                                        <p class="mk_name">${mall.mall_cd_nm }</p>
                                                        <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                        <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                        <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                        <div class="account_btn">
                                                            <p class="btn_edit"
                                                                onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_2.png" alt=""></p>
                                                            <p class="btn_remove"
                                                                data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                                onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_1.png" alt=""></p>
                                                        </div>
                                                    </li>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall" data-choice-mk="mk_type6">
                                    <p class="type_market_choiced">3PL물류(상품자산)</p>
                                    <ul class="open_append_6">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'OFM' and mall.del_yn ne 'Y'  }">
                                                <c:if
                                                    test="${mall.cust_mall_sts_cd eq 'NR' or mall.cust_mall_sts_cd eq 'REG' or mall.cust_mall_sts_cd eq 'MOD' }">
                                                    <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                        data-mall_cd="${mall.mall_cd }">
                                                        <p class="mk_name">${mall.mall_cd_nm }</p>
                                                        <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                        <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                        <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                        <div class="account_btn">
                                                            <p class="btn_edit"
                                                                onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_2.png" alt=""></p>
                                                            <p class="btn_remove"
                                                                data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                                onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_1.png" alt=""></p>
                                                        </div>
                                                    </li>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall" data-choice-mk="mk_type8">
                                    <p class="type_market_choiced">기타</p>
                                    <ul class="open_append_8">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'ETC' and mall.del_yn ne 'Y'  }">
                                                <c:if
                                                    test="${mall.cust_mall_sts_cd eq 'NR' or mall.cust_mall_sts_cd eq 'REG' or mall.cust_mall_sts_cd eq 'MOD' }">
                                                    <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                        data-mall_cd="${mall.mall_cd }">
                                                        <p class="mk_name">${mall.mall_cd_nm }</p>
                                                        <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                        <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                        <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                        <div class="account_btn">
                                                            <p class="btn_edit"
                                                                onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_2.png" alt=""></p>
                                                            <p class="btn_remove"
                                                                data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                                onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_1.png" alt=""></p>
                                                        </div>
                                                    </li>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_mall" data-choice-mk="mk_type7">
                                    <p class="type_market_choiced">오픈예정몰</p>
                                    <ul class="open_append_7">
                                        <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'OCM' and mall.del_yn ne 'Y'  }">
                                                <c:if
                                                    test="${mall.cust_mall_sts_cd eq 'NR' or mall.cust_mall_sts_cd eq 'REG' or mall.cust_mall_sts_cd eq 'MOD' }">
                                                    <li data-cust_mall_sts_cd="${mall.cust_mall_sts_cd }"
                                                        data-mall_cd="${mall.mall_cd }">
                                                        <p class="mk_name">${mall.mall_cd_nm }</p>
                                                        <p class="mk_id">${mall.mall_cert_1st_id }</p>
                                                        <p class="mk_sub_id" style="display:none">${mall.sub_mall_cert_1st }</p>
                                                        <p class="mk_sub_2nd_id" style="display:none">${mall.sub_mall_cert_2nd }</p>
                                                        <div class="account_btn">
                                                            <p class="btn_edit"
                                                                onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_2.png" alt=""></p>
                                                            <p class="btn_remove"
                                                                data-cust_mall_seq_no='${mall.cust_mall_seq_no}'
                                                                onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )"><img
                                                                    src="/assets/images/member/icon_1.png" alt=""></p>
                                                        </div>
                                                    </li>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${not empty loanBatchProTgtInfo.batchProTgt_list }">
                                <c:set var="loanSvcId" value="${loanBatchProTgtInfo.batchProTgt_list[0].svc_id }" />
                                <c:set var="loanSvcDescp" value="${loanBatchProTgtInfo.batchProTgt_list[0].svc_descp }" />
                                <!-- <p class="text_right mt10"><button class="btn_blue02" data-loan-svc-id="${loanSvcId}" data-loan-svc-descp="${loanSvcDescp}">${loanSvcDescp} 판매몰 관리</button></p> -->
                                <p class="text_right mt10"><button class="btn_blue02" data-loan-svc-id="${loanSvcId}" data-loan-svc-descp="${loanSvcDescp}">금융 상품 판매몰 관리</button></p>
                            </c:when>
                            <c:otherwise>
                                <c:set var="loanSvcDescp" value="신한 퀵정산" />
                            </c:otherwise>
                        </c:choose>
                        <!--등록결과 에러-->
                        <p class="error_account_msg" style="display: none;">계정 입력 오류 항목이 있습니다. 다시 확인해주세요.</p>
                    </div>
                </div>

                <style>
                    .ocm_tip_area {
                        background-color: beige;
                        color: #888888;
                        margin-top: 30px;
                        font-size: 1em;
                        padding: 10px 15px 10px 15px;
                    }

                    .ocm_tip_area a {    
                        color: #009aca;
                        font-weight: 700;
                    }
                </style>
                <p class="ocm_tip_area">&lt;설명TIP&gt;<br>
                    오픈예정몰이란, 셀러봇캐시에서 더많은 정산예정금을 보여주려고 준비중인 판매몰입니다.<br>
                    지금, 오픈예정몰 관련 정보(몰계정, 정산계좌등)을 미리 등록하시면, ‘등록 감사 혜택’과 해당 판매몰이 오픈했을때 정산예정금 정보를 ‘즉시 확인’하실 수 있습니다.<br>
                    자세한 내용은 <a href="http://pf.kakao.com/_kfjyC" target="_blank">바로가기</a> (셀러봇캐시 카카오친구)를 클릭하시고, 소식에서 확인해주세요.<br>
                </p>

                <style>
                    /* 툴팁 balloon */
                    .msg_time_alarm .bt-tooltip {
                        position: absolute;
                        display: inline-block;
                    }

                    .msg_time_alarm .bt-tooltip:hover[data-title]:before,
                    .msg_time_alarm .bt-tooltip:hover[data-title]:after {
                        visibility: visible;
                        pointer-events: auto
                    }

                    .msg_time_alarm .bt-tooltip[data-title]:before,
                    .msg_time_alarm .bt-tooltip[data-title]:after {
                        pointer-events: none;
                        visibility: hidden;
                    }

                    .msg_time_alarm .bt-tooltip[data-title]:after {
                        content: attr(data-title);
                        position: absolute;
                        top: 110%;
                        right: -55%;
                        transform: translateX(-50%);
                        margin-left: 49%;
                        text-transform: none;
                        text-align: left;
                        white-space: pre-line;
                        font-size: 1rem;
                        border: 1px solid #d4d4d5;
                        line-height: 1.4285em;
                        max-width: none;
                        background: #fff;
                        padding: .633em 1em;
                        font-weight: 100;
                        font-style: normal;
                        color: rgba(0, 0, 0, .87);
                        border-radius: .48571429rem;
                        -webkit-box-shadow: 0 2px 4px 0 rgba(34, 36, 38, .12), 0 2px 10px 0 rgba(34, 36, 38, .15);
                        box-shadow: 0 2px 4px 0 rgba(34, 36, 38, .12), 0 2px 10px 0 rgba(34, 36, 38, .15);
                        z-index: 1;
                    }
                </style>
                <p class="msg_time_alarm">주의<br>
                    1. 등록된 판매몰 정보가 회원 가입시 사업자 정보와 다를 경우 서비스가 제한 될 수 있습니다.<br>
                    2. 제휴사 서비스를 이용 중인 고객은 등록된 판매몰을 삭제 또는 변경이 제한 될 수 있습니다.<br>
                    3. <strong style="cursor:pointer" class="tooltipEvent bt-tooltip" data-title="${finMallList}">${loanSvcDescp}을 이용하기 위해서는 쇼핑몰 등록이 필요합니다.<img src="/assets/images/icon/icon_smRlavy.png" style="width: 23px;height: 20px;display: inline-block; margin-left: 10px;" alt=""></strong><br>
                </p>

                <div id="usable_store">
                    <div class="usable_store_header">
                        <h3>이용 가능 판매몰</h3>
                        <p>판매몰 계정 등록 후 바로 데이터 조회가 가능한 판매몰들이에요~</p>
                    </div>
                    <div class="usable_store_body">
                        <ul></ul>
                    </div>
                    <div class="usable_store_footer">
                        <p>판매몰은 계속 추가되고 있으며, 원하시는 판매몰이 있으실 경우 <a href="/sub/my/ask" target="_blank">1:1문의</a>를 통해 요청해주시기 바랍니다.</p>
                    </div>
                </div>

                <div id="pre_store">
                    <div class="pre_store_header">
                        <h3>오픈 예정 판매몰</h3>
                        <p>곧 오픈될 예정인 판매몰계정을 사전등록하시면 이용권 혜택을 드리고 있어요~</p>
                        <p><a href="/pub/cs/event_detail?event_seq_no=60" target="_blank">(오픈예정몰 판매몰 정보등록 이벤트 참여하기)</a></p>
                    </div>
                    <div class="pre_store_body">
                        <ul></ul>
                    </div>
                    <div class="pre_store_footer">
                    
                    </div>
                </div>

                <div id="rdy_store">
                    <div class="rdy_store_header">
                        <h3>준비 중인 판매몰</h3>
                        <p>셀러봇캐시에서 열심히 검토하고 있는 중인 판매몰들이에요. 많은 기대 부탁드려요!</p>
                    </div>
                    <div class="rdy_store_body">
                        <ul></ul>
                    </div>
                    <div class="rdy_store_footer">
                    
                    </div>
                </div>
                
                <!-- s: 슬라이드 배너 20210415 -->
                <%@include file="../widgets/banner_vertical.jsp" %>
                <!-- e: 슬라이드 배너 20210415 -->

                
            </div>
        </div>

        <!-- s: 애큐온 배너 20210415 -->
        <div class="market_banner_container">
            <%@include file="../widgets/banner_market.jsp" %>
        </div>
        
        <!-- e: 애큐온 배너 20210415 -->

        <!--계정 입력 팝업-->
        <div class="popup_account type1">
            <div class="pop_account">
                <div class="pop_head">
                    <p>계정입력</p>
                    <p class="popup_gui_btn">?</p>
                    <div class="popup_gui_container">
                        <span id="helpInfoText">
                            업체번호와 공급계약번호는
                            인터파크 판매자 매니저의 좌측메뉴
                            판매자 정보 관리의 관리 계정(ID)
                            관리에서 확인할 수 있습니다.
                        </span>
                        <img class="popup_gui_close" src="/assets/images/member/close.png" alt="" />
                    </div>
                    <a href="javascript:type1Close();" class="close_pop"><img src="/assets/images/member/close.png" alt=""></a>
                </div>
                <div class="pop_body">
                    <p class="edit_video_btn2"><span>영상가이드</span></p>
                    <style>
                        .edit_video_btn2 {
                            text-align: right;
                            position: absolute;
                            right: 0.5rem;
                            top: 4.5rem;
                            z-index: 6;
                        }

                        .edit_video_btn2 span {
                        font-size: 1rem;
                        color: #fff;
                        background-color: #009aca;
                        display: block;
                        width: 106px;
                        text-align: center;
                        padding: 0.3rem 0;
                        border-radius: 3rem;
                        float: right;
                        cursor: pointer;

                        }
                        div.edit_video_popup {
                            width: 100%;
                            height: 100%;
                            position: fixed;
                            top: 0;
                            left: 0;
                            display: flex;
                            align-items: center;
                            justify-content: center;
                            display: none;
                            z-index: 9999;
                        }

                        div.edit_video_popup div.edit_video_popup_container {
                            position: relative;
                            z-index: 6;
                            max-width: 50rem;
                            width: 90%;
                            height: 30rem;
                        }

                        .background_black {
                            width: 100%;
                            height: 100%;
                            position: absolute;
                            left: 0;
                            top: 0;
                            background: #000;
                            opacity: 0.5;
                            z-index: 5;
                        }

                        div.edit_video_popup div.edit_video_popup_container p img {
                            float: right;
                            margin-bottom: 0.5rem;
                            cursor: pointer;
                        }
                    </style>
                    <div class="logo_area_account">
                        <img src="/assets/images/member/logo/logo011.png" alt="">
                    </div>
                    <form class="frm_account" name="frmAccount" onsubmit="return false;">
                        <div class="input_account" id="div_mall_cert_typ_cd" style="display: none;">
                            <p class="lb">몰 인증 유형</p>
                            <select class="sctbox" id="i_mall_cert_typ_cd" name="mall_cert_typ_cd">
                            </select>
                        </div>
                        <div class="input_account">
                            <p class="lb">ID</p>
                            <div class="input_box">
                                <input type="text" class="textbox_mb" name="mall_cert_1st_id">
                                <p id="mall_cert_1st_id_error" class="error">*아이디를 입력해주세요.</p>
                            </div>
                        </div>
                        <div class="input_account">
                            <p class="lb">PW</p>
                            <div class="input_box">
                                <input type="password" class="textbox_mb" name="mall_cert_1st_passwd">
                                <p id="mall_cert_1st_passwd_error" class="error">*비밀번호를 입력해주세요.</p>
                            </div>
                        </div>
                        <div class="input_account" style="display: none;" id="div_sub_mall_cert_1st_nm">
                            <p class="lb" id="t_sub_mall_cert_1st_nm"></p>
                            <div class="input_box">
                                <input type="text" class="textbox_mb" name="sub_mall_cert_1st"
                                    id="i_sub_mall_cert_1st_nm">
                                <button type="button" id="mall_btn" onClick="window.open('https://sell.smartstore.naver.com/#/sellers/store/detail/api');return false">
                                    <p style="font-size: 0.8em;">API ID 확인</p></button>
                                <p id="sub_mall_cert_1st_error" class="error"></p>
                            </div>
                        </div>
                        <div class="input_account" style="display: none;" id="div_sub_mall_cert_2nd_nm">
                            <div class="input_box">
                                <p class="lb" id="t_sub_mall_cert_2nd_nm"></p>
                                <input type="text" class="textbox_mb" name="sub_mall_cert_2nd"
                                    id="i_sub_mall_cert_2nd_nm">
                                <input type="hidden" class="textbox_mb" name="cert_step" id="cert_step">
                                <input type="hidden" class="textbox_mb" name="mall_cd" id="mall_cd">
                                <input type="hidden" class="textbox_mb" name="mall_nm" id="mall_nm">
                                <input type="hidden" class="textbox_mb" name="passwd_step" id="passwd_step">
                                <input type="hidden" class="textbox_mb" name="sub_mall_auth_1st_esse_yn"
                                    id="sub_mall_auth_1st_esse_yn">
                                <input type="hidden" class="textbox_mb" name="sub_mall_auth_2nd_esse_yn"
                                    id="sub_mall_auth_2nd_esse_yn">
                                <input type="hidden" id="btn_type">
                                <input type="hidden" id="cust_mall_seq_no" name="cust_mall_seq_no" />
                                <input type="hidden" id="submitType" />
                                <input type="hidden" name="goods_cate_seq_no" id="goods_cate_seq_no">
                                <p id="t_sub_mall_cert_2nd_nm_error" class="error"></p>
                            </div>
                        </div>
                        <div class="input_account" style="display: none;" id="div_mall_cert_2nd_passwd_nm">
                            <p class="lb" id="t_mall_cert_2nd_passwd_nm"></p>
                            <div class="input_box">
                                <input type="password" class="textbox_mb" name="mall_cert_2nd_passwd">
                                <p id="mall_cert_2nd_passwd_error" class="error"></p>
                            </div>
                        </div>

                        <!-- OTP 키인증 값 -->
                        <div class="input_account" style="display: none;" id="div_mall_auth_otp_key">
                            <p class="lb" id="t_mall_auth_otp_key">OTP인증</p>
                            <div class="input_otp file">
                                <input type="password" class="textbox_otp_file" name="mall_auth_otp_key">
                                <label for="fileUpload">QR등록</label>
                                <input type="file" id="fileUpload" name="qr_code_file" class="hidden_file">
                                <div class="ask_sub_text_area error" id="qr_code_file_err" style="display: none;">
                                    <h1 class="ask_sub_text_area_text">
                                        파일 크기는 10MB 이하의 파일만 업로드 가능합니다.<br />
                                        파일 유형은 JPG,GIF,BMP,TIF,PNG,PDF 파일만 업로드 가능합니다.
                                    </h1>
                                </div>
                                <input type="hidden" class="textbox_mb" name="otp_auth_use_yn" id="otp_auth_use_yn">
                                <input type="hidden" class="textbox_mb" name="otp_auth_esse_yn" id="otp_auth_esse_yn">
                                <input type="hidden" class="textbox_mb" name="otp_auth_typ_cd" id="otp_auth_typ_cd">
                                <input type="hidden" class="textbox_mb" name="otp_auth_typ_cd_nm" id="otp_auth_typ_cd_nm">
                                <p id="mall_auth_otp_key_error" class="error"></p>
                            </div>
                        </div>
                        <!-- 카테고리 추가 -->
                        <div class="input_account" id="div_goods_cate_seq_no">
                            <p class="lb">카테고리</p>
                            <div class="input_box">
                                <select class="sctbox" id="goods_cate_seq_no" name="goods_cate_seq_no">
                                    <option value="">선택</option>
                                    <c:forEach items="${categoryList }" var="category">
                                        <option value="${category.goods_cate_seq_no }">${category.cate_nm }</option>
                                    </c:forEach>
                                </select>
                                <p id="goods_cate_seq_no_error" class="error"></p>
                            </div>
                        </div>

                        <a href="#" class="add_account"></a>
                    </form>
                    <p class="mall_error_msg">*아이디와 비밀번호를 다시 확인해주세요.</p>
                </div>
                <div class="pop_foot">
                    <button type="button" class="frm_account_btn">입력완료</button>
                </div>
            </div>
        </div>

        <!-- S:금융대출 제휴사 판매몰 관리 팝업 20210303--> 
        <div class="popup_account popup_account2 loan" style="max-width : 50%;">
            <div class="pop_account">
                <div class="pop_head">
                    <p></p>
                    <img class="popup_loan_close" src="/assets/images/member/close.png" alt="" />
                </div>
                <div class="pop_body pb20">
                    <div class="pd20">
                    </div>
                        <style>
                            /* TODO 2022.12.07 수정 */
                            .pop_list {
                                border: 1px solid #ddd;
                                border-radius: 8px;
                                padding: 0 0.6em 0 0;
                                margin-left: 60%;
                                margin-right: 4%;
                                margin-bottom: 10px;
                            }
                            .pop_sctbox {
                                display: inline-block;
                                vertical-align: middle;
                                font-size: 1em;
                                -webkit-appearance: none;
                                -moz-appearance: none;
                                appearance: none;
                                border: 0;
                                color: #888888;
                                width: 12.438em;
                                line-height: 1.7em;
                                background: url(/assets/images/common/sct.png) center right 10px no-repeat;
                                -webkit-background-size: 12px 10px;
                                background-size: 12px 10px;
                                width: 100%;
                            }
                        </style>
                        <!-- //TODO 2022.12.07 수정 -->
                        <div class="pop_list">
                            <select class="pop_sctbox" id="pop_mall" name="pop_mall">
                                <c:if test="${selectList != null}">
                                    <c:forEach var="sData" items="${selectList}" varStatus="status">
                                        <option value="${sData.cd}">
                                            ${sData.cd_nm}
                                        </option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    <div class="table_style01 scroll_y">
                        <style>
                            .mall_sts_cd_store_con {
                                position: relative;
                                display: block;
                                left: 2px;
                                top: 8px;
                                width: 8px;
                                height: 8px;
                                border-radius: 50%;
                            }

                            .mall_sts_cd_store_con.bullet_blu {
                                background-color: #598ae2;
                            }

                            .mall_sts_cd_store_con.bullet_red {
                                background-color: #fd0e19;
                            }

                            .mall_sts_cd_store_con.bullet_gry {
                                background-color: #aaa;
                            }

                        </style>
                        <table class="w95">
                            <c:set var="RONIBOT" value="" />
                            <thead>
                                <tr id="frist_line">
                                    <th style="width: 17%;">선택 여부</th>
                                    <th>타사 선정산 여부</th>
                                    <th>판매몰 명</th>
                                    <th>로그인<br>아이디</th>
                                    <th>데이터 수집<br> 현황</th>
                                    <th>등록 일자</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pop_foot fl_wrap">
                    <button type="button" class="btn_left fl_left loan_save">저장</button>
                    <button type="button" class="btn_gray2 btn_cancle fl_left loan_close">취소</button>
                </div>
            </div>
        </div>
        <!-- E:금융대출 제휴사 판매몰 관리 팝업 20210303 --> 
    </div>
</div>
<div class="edit_video_popup">
    <div class="edit_video_popup_container">
      <p><img src="/assets/images/member/close.png" alt=""></p>
      <iframe id="iframeVideoPopup" width="100%" height="100%" src="https://www.youtube.com/embed/gr1TWqaR3Rs?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="background_black"></div>
</div>
<!-- init시 수정 대상 몰 정보   -->
<input type="hidden" id="custMallSeqNo" value="${cust_mall_seq_no }">
<script>
    //금융 상품 판매몰 관리 리스트 대상여부 체크박스 선택 카운트(대상여부 이미 체크되어있던 것 제외)
    var checkCnt = 0;

    $(document).ready(function () {
        // 2019-07-17 스크롤 유지
        $(".modal_alarm, .market_box").off("mouseover");
        $(".modal_alarm, .market_box").off("mouseout");

        var clickM;
        var img_mall_name;
        var img_mall_text;
        // 판매몰 등록
        $(".group_list_mall li, #usable_store li, #pre_store li").on("click", function () {

            $(".mall_error_msg").hide();
            $(".input_box .error").hide();
            $(".input_otp .error").hide();

            $("#i_mall_cert_typ_cd").prop("disabled", false);
            $("input[name='mall_cert_1st_id']").prop("disabled", false);
            $("input[name='mall_cert_1st_id']").attr("readonly", false);
            $("input[name='mall_cert_1st_passwd']").prop("disabled", false);
            $("#submitType").val("insert");
            
            clickM = $(this);

            var imgLogo = clickM.find("img").attr("src");
            var mallImg = clickM.find("img");
            img_mall_name = clickM.attr("name");

            var sub_mall_cert_1st_nm = clickM.children(".sub_mall_cert_1st_nm").val();
            var sub_mall_cert_2nd_nm = clickM.children(".sub_mall_cert_2nd_nm").val();
            var mall_cert_2nd_passwd_nm = clickM.children(".mall_cert_2nd_passwd_nm").val();
            var sub_mall_cert_step = clickM.children(".sub_mall_cert_step").val();
            
            var passwd_step = clickM.children(".passwd_step").val();
            var sub_mall_auth_1st_esse_yn = clickM.children(".sub_mall_auth_1st_esse_yn").val();
            var sub_mall_auth_2nd_esse_yn = clickM.children(".sub_mall_auth_2nd_esse_yn").val();
            var otp_auth_use_yn = clickM.children(".otp_auth_use_yn").val();
            var otp_auth_esse_yn = clickM.children(".otp_auth_esse_yn").val();
            var otp_auth_typ_cd = clickM.children(".otp_auth_typ_cd").val();
            var otp_auth_typ_cd_nm = clickM.children(".otp_auth_typ_cd_nm").val();
            var mall_nm = clickM.children(".mall_nm").val();
            var mall_cd = clickM.children(".mall_cd").val();
            
            $("input[name='mall_nm']").val(mall_nm);
            $("input[name='mall_cd']").val(mall_cd);
            $("input[name='cert_step']").val(sub_mall_cert_step);
            $("input[name='mall_cert_1st_id']").val('');
            $("input[name='mall_cert_1st_passwd']").val('');
            $("input[name='sub_mall_cert_1st']").val('');
            $("input[name='sub_mall_cert_2nd']").val('');
            $("input[name='passwd_step']").val(passwd_step);
            $("#i_mall_cert_typ_cd").html('<option value="A00"></option>');
            $("input[name='mall_cert_2nd_passwd']").val('');
            $("input[name='sub_mall_auth_1st_esse_yn']").val(sub_mall_auth_1st_esse_yn);
            $("input[name='sub_mall_auth_2nd_esse_yn']").val(sub_mall_auth_2nd_esse_yn);
            $("input[name='mall_auth_otp_key']").val('');
            $("input[name='qr_code_file']").val('');
            $("input[name='otp_auth_use_yn']").val(otp_auth_use_yn);
            $("input[name='otp_auth_esse_yn']").val(otp_auth_esse_yn);
            $("input[name='otp_auth_typ_cd']").val(otp_auth_typ_cd);
            $("input[name='otp_auth_typ_cd_nm']").val(otp_auth_typ_cd_nm);

            if (clickM.children(".mall_cd").val() != "008" || mall_cd != "008") {
                if(!fnIsRegData($("#currentRegCount").val(), $("#maxRegCount").val())) {
                    showAlert("현재 이용 중이신 서비스는<br>판매몰 " + $("#maxRegCount").val() + "개까지만 등록이 가능합니다.");
                    return;
                }
            }

            // 카테고리 추가
            $("select[name='goods_cate_seq_no']").val($("select[name='goods_cate_seq_no'] option:first").val());


            if (clickM.children(".sub_mall_cert_1st_nm").val() == "" || sub_mall_cert_1st_nm == null) {
                $("#div_sub_mall_cert_1st_nm").hide();
            } else {
                $("#t_sub_mall_cert_1st_nm").html(sub_mall_cert_1st_nm);
                $("#div_sub_mall_cert_1st_nm").show();
            }

            if (clickM.children(".sub_mall_cert_2nd_nm").val() == "" || sub_mall_cert_2nd_nm == null) {
                $("#div_sub_mall_cert_2nd_nm").hide();
            } else {
                $("#t_sub_mall_cert_2nd_nm").html(sub_mall_cert_2nd_nm);
                $("#div_sub_mall_cert_2nd_nm").show();
            }

            if (clickM.children(".mall_cert_2nd_passwd_nm").val() == "" || mall_cert_2nd_passwd_nm == null) {
                $("#div_mall_cert_2nd_passwd_nm").hide();
            } else {
                $("#t_mall_cert_2nd_passwd_nm").html(mall_cert_2nd_passwd_nm);
                $("#div_mall_cert_2nd_passwd_nm").show();
            }

            if (clickM.children(".otp_auth_use_yn").val() == "Y" || otp_auth_use_yn == "Y") {
                if (otp_auth_typ_cd == "OTP001") {
                    $(".input_otp.file > input[name=mall_auth_otp_key]").attr("disabled", true);
                    $(".input_otp.file > input[name=mall_auth_otp_key]").attr("class", "textbox_otp_file");
                    $(".input_otp.file > label[for=fileUpload]").show();
                } else if (otp_auth_typ_cd == "OTP002") {
                    $(".input_otp.file > input[name=mall_auth_otp_key]").attr("disabled", false);
                    $(".input_otp.file > input[name=mall_auth_otp_key]").attr("class", "textbox_mb");
                    $(".input_otp.file > label[for=fileUpload]").hide();
                }

                $("#div_mall_auth_otp_key").show();
            } else {
                $("#div_mall_auth_otp_key").hide();
            }

            if (clickM.children(".mall_cert_typ").length == 0) {
                $("#div_mall_cert_typ_cd").hide();
            } else {
                var html = "";
                clickM.children(".mall_cert_typ").each(function (i, v) {
                    html += "<option value='" + $(this).val() + "'>" + $(this).data("type_nm") + "</option>";
                });
                $("#i_mall_cert_typ_cd").html(html);
                $("#div_mall_cert_typ_cd").show();
            }

            if (clickM.children(".mall_cd").val() == "008" || mall_cd == "008") {

                document.getElementById("mall_btn").style.display = "inline-block";
                document.getElementById("i_sub_mall_cert_1st_nm").style.width = "70%";
                
            } else {
                document.getElementById("mall_btn").style.display = "none";
                document.getElementById("i_sub_mall_cert_1st_nm").style.width = "100%";
            }

            /**
             * 2020-06-22 판매몰별 도움말 & 영상 설정
             */
            setHelpInfo(clickM);
            setVideoInfo(clickM);

            var open_leng = $(".mall_lists_1 li").length;
            $(".popup_account.type1").addClass("active");
            $(".popup_account.type1 .logo_area_account img").attr('src', imgLogo);
        });

        $(function () {
            var all_mall = new Array();
            $(".market_choice .mall_nm").each(function(){ 
                if ($(this).parent().find(".mall_sts_cd").val() == 'NOR') {
                    all_mall.push($(this).val()) 
                }
            });

            $(".market_box_find_text_area").autocomplete({
                source: all_mall,
                select: function (event, ui) {
                    $(".popup_account.type1").addClass("active");
                    for (var i = 0; i < $('.group_list_mall li').length; i++) {
                        if (ui.item.value == $('.group_list_mall li span').eq(i).text()) {
                            var img_tags = $('.group_list_mall li span').eq(i).prev('img').attr('src');
                            clickM = $('.group_list_mall li').eq(i);
                            clickM.click();

                        }
                    }
                },
                focus: function (event, ui) {
                    return false;
                }
            });
        });

        //select2
        function format(option) {
            var originalOption = option.element;
            var valOption = $(originalOption).val();
            if (!option.id) return option.text; // optgroup
            return "<div class='mall_logo'><img src='/assets/images/member/logo/" + valOption + ".png'/>" + option.text + "</div>";
        }

        $(".select_mall").select2({
            placeholder: "판매몰 검색",
            allowClear: true,
            templateResult: format,
            templateSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });

        $(".select_mall").on("select2:select", function (e) {
            var selectMImg = $(".select2-selection__rendered .mall_logo img").attr("src");
            $(".sh_mall_btn").click(function () {
                $(".popup_account.type1").addClass("active");
                $(".popup_account.type1 .logo_area_account img").attr('src', selectMImg);
                $(".popup_account.type1 .close_pop").click(function () {
                    $(".popup_account.type1").removeClass("active");
                    return false;
                });
            });
        });


        // 전체
        $("#filterAll").on("click", function () {
            $(".group_mall").show();
            $(".group_mall_error").show();
            $(".group_mall_warning").show();
        });
        // 정상
        $("#filterPass").on("click", function () {
            $(".group_mall").show();
            $(".group_mall_error").hide();
            $(".group_mall_warning").hide();
        });
        // 점검
        $("#filterWarning").on("click", function () {
            $(".group_mall").hide();
            $(".group_mall_error").hide();
            $(".group_mall_warning").show();
        });
        // 에러
        $("#filterError").on("click", function () {
            $(".group_mall").hide();
            $(".group_mall_error").show();
            $(".group_mall_warning").hide();
        });

        //init
        (function () {
            if (nonNull($("#custMallSeqNo").val())) {
                fn_updateData($("#custMallSeqNo").val(), '', '', '');
            }

            // 기본 전체
            $(".group_mall").show();
            $(".group_mall_error").show();
            $(".group_mall_warning").show();

        })();

        // 2021.03.09 추가 금융대출 제휴사 판매몰 팝업창
        $(".btn_blue02").on("click", function () {
            // var loanSvcId = $(this).data("loan-svc-id");
            // var loanSvcDescp = $(this).data("loan-svc-descp");
            // var loanSvcDescp = $('.pop_sctbox option:selected').text();
            
            //TODO 2022.12.07 수정
            // var modalhtmlHeadStr = loanSvcDescp + ' 대출 판매몰 관리';
            // var modalhtmlHeadStr = "금융 상품 판매몰 관리";
            // var modalhtmlbodyStr = loanSvcDescp + ' 대출을 이용하는 고객은 대출 대상의 판매몰을 선택, 해지 할 수 있습니다.';
            
            // $(".popup_account.loan .pop_head > p").html(modalhtmlHeadStr);
            // $(".popup_account.loan .pop_body .pd20").html(modalhtmlbodyStr);
            $(".popup_account.loan").addClass("active");
        });

        $(".popup_loan_close, .loan_close").on("click", function () {
            $(".pop_sctbox").find("option:eq(0)").prop("selected", true).trigger('change');
            $(".popup_account.loan").removeClass("active");
        });

        $(".loan_save").on("click", function () {
            var confirmText = "판매몰을 등록하시면 해지는 금융기관에만 처리가 가능하므로 금융기관으로 문의바랍니다.";

            if(checkCnt > 0){
                var modalId = showConfirm(confirmText, function () {
                    var paramArray = new Array();
                    
                    $("input:checkbox[name=loan_chk]:not(:disabled):checked").each(function() {
                        var pushData  = {
                            cust_seq_no : $(this).data("cust-seq-no"),
                            cust_mall_seq_no : $(this).data("cust-mall-seq-no"),
                            mall_cd : $(this).data("mall-cd"),
                            bat_prcs_tgt_seq_no : $(this).data("bat-prcs-tgt-seq-no"),
                            loan_svc_id : $('.pop_sctbox').val()
                        }; 

                        paramArray.push(pushData);
                    });

                    $.ajax({
                        url: '/sub/loan/getLoanInfo'
                        , type: 'post'
                        , async: true
                        , dataType: 'json'
                        , contentType: 'application/json'
                        , data: JSON.stringify(paramArray)
                        , success: function (response) {
                            $(".popup_account.loan").removeClass("active");
                            if (response.result == "OK") {
                                removeModal(modalId);
                                showAlert("저장 되었습니다.", function () {
                                    location.href = "/sub/my/market";
                                });
                            } else {
                                removeModal(modalId);
                                showAlert(errorMessage);
                            }
                        }
                        , error: function (res) {
                            removeModal(modalId);
                            showAlert("처리 중 에러가 발생하였습니다.<br>관리자에게 문의 하세요.");
                        }
                    });
                });
            }else{
                showAlert("선택 여부를 선택하세요.");
            }
        });

        //20210720. 금융상품 사용가능 쇼핑몰 정보 말풍선
        $('.tooltipEvent').on({
            mouseover: function(){
                var c = $(this).attr("class");
                if (c.indexOf("bt-tooltip") > 0) {
                } else {
                    $(this).addClass('bt-tooltip');
                }
            },
            mouseout: function(){
                $(this).removeClass('bt-tooltip');
            },
            click: function(){
                var c = $(this).attr("class");
                if (c.indexOf("bt-tooltip") > 0) {
                    $(this).removeClass('bt-tooltip');
                } else {
                    $(this).addClass('bt-tooltip');
                }
            }
        });

        //금융상품 콤보박스
        $('.pop_sctbox').change(function(){
            //콤보박스 변경 시 대상여부 카운트 초기화
            checkCnt = 0;

            var val = this.value;

            var modalhtmlHeadStr = "금융 상품 판매몰 관리";
            var modalhtmlbodyStr = $("option:selected", this).text() + ' 대출을 이용하는 고객은 대출 대상의 판매몰을 선택 할 수 있습니다.';

            $(".popup_account.loan .pop_head > p").html(modalhtmlHeadStr);
            $(".popup_account.loan .pop_body .pd20").html(modalhtmlbodyStr);

            $.ajax({
                url: '/sub/loan/getLoanCustMallList'
                , type: 'post'
                , async: true
                , dataType: 'json'
                , contentType: 'application/json'
                , data: val
                , success: function (response) {
                    var tag = "";
                    var batPrcsTgtSeqNo = "";

                    if(response.batchProTgt_list != null){
                        batPrcsTgtSeqNo = response.batchProTgt_list[0].bat_prcs_tgt_seq_no;
                    }

                    response.loanCustMall_list.forEach(loanInfo => {
                        tag += "<tr>";
                        tag += "<td class=\"text-center\"><input type=\"checkbox\" name=\"loan_chk\" class=\"loan_cust_mall\"";
                        tag += "data-cust-seq-no=\"" + loanInfo.cust_seq_no + "\"";
                        tag += "data-cust-mall-seq-no=\"" + loanInfo.cust_mall_seq_no + "\"";
                        tag += "data-mall-cd=\"" + loanInfo.mall_cd + "\"";
                        tag += "data-bat-prcs-tgt-seq-no=\"" + batPrcsTgtSeqNo + "\"";
                        tag += "data-loan-sts-cd=\"" + loanInfo.loan_sts_cd + "\"";
                        tag += "data-loan-svc-descp=\"" + loanInfo.svc_descp + "\"";
                        if(loanInfo.check_batch_pro_tge != null && loanInfo.check_batch_pro_tge != ''){
                            tag += "checked disabled";
                        }
                        tag += "></td>";
                        tag += "<th>" + loanInfo.other_loan_use + "</th>";
                        
                        tag += "<th>" + loanInfo.mall_nm + "</th>";
                        tag += "<td class=\"text-center\">" + loanInfo.mall_cert_1st_id + "</td>";

                        tag += "<td class=\"text-center\">";
                        tag += "<div>";
                        tag += "<ul>";
                        
                        if(loanInfo.cust_mall_sts_cd == 'NR'){
                            tag += "<li class=\"mall_sts_cd_store_con bullet_blu\"></li>";
                            tag += "<li>정상</li>";
                        }else if(loanInfo.cust_mall_sts_cd == 'INS'){
                            tag += "<li class=\"mall_sts_cd_store_con bullet_gry\"></li>";
                            tag += "<li>점검</li>";
                        }else if(loanInfo.cust_mall_sts_cd == 'ERR'){
                            tag += "<li class=\"mall_sts_cd_store_con bullet_red\"></li>";
                            tag += "<li>인증불가</li>";
                        }
                        tag += "</ul>";
                        tag += "</div>";
                        tag += "</td>";

                        var reg_ts = new Date(loanInfo.loan_reg_ts);
                        var year = reg_ts.getFullYear();
                        var month = reg_ts.getMonth()+1;
                        var day = reg_ts.getDate();
                        var format_reg_ts = year+"-"+(("00"+month.toString()).slice(-2))+"-"+(("00"+day.toString()).slice(-2));
                        tag += "<td class=\"text-center\">" + format_reg_ts + "</td>";
                        tag += "</tr>";

                        $(".table_style01 > table > tbody").empty().append(tag);
                    });
                }
                , error: function (res) {
                    showAlert("처리 중 에러가 발생하였습니다.<br>관리자에게 문의 하세요.");
                }
            });
        });
        
        if($(".pop_sctbox option").length > 0){
            $(".pop_sctbox").find("option:eq(0)").prop("selected", true).trigger('change');
        }
    });

    //금융 상품 판매몰 관리 리스트 대상여부 체크박스 선택/해제 이벤트
    $(document).on('change','.loan_cust_mall',function(){
        if($(this).prop("checked")){
            checkCnt++;
        }else{
            checkCnt--;
        }
    });

    /**
     * 이용 가능 판매몰 목록 추가
     */
     if( $('#usable_store').length > 0 ) {
        $('.market_box li').each(function(){
            const tt = $(this);
            var mall_typ_cd = tt.children(".mall_typ_cd").val();
            var mall_sts_cd = tt.children(".mall_sts_cd").val();

            if(mall_typ_cd != 'OCM') {
                const marketBoxLi = $(this).clone();
                $('#usable_store .usable_store_body ul').append(marketBoxLi);
            }
        });
    }

    /**
     * 오픈 예정 판매몰 목록 추가
     */
     if( $('#pre_store').length > 0 ) {
        $('.market_box li').each(function(){
            const tt = $(this);
            var mall_typ_cd = tt.children(".mall_typ_cd").val();
            var mall_sts_cd = tt.children(".mall_sts_cd").val();
            
            if(mall_typ_cd == 'OCM' && mall_sts_cd == 'NOR') {
                const marketBoxLi = $(this).clone();
                $('#pre_store .pre_store_body ul').append(marketBoxLi);
            }
        });
    }

    /**
     * 준비 중인 판매몰 목록 추가
     */
     if( $('#rdy_store').length > 0 ) {
        $('.market_box li').each(function(){
            const tt = $(this);
            var mall_typ_cd = tt.children(".mall_typ_cd").val();
            var mall_sts_cd = tt.children(".mall_sts_cd").val();
            
            if(mall_typ_cd == 'OCM' && mall_sts_cd == 'RDY') {
                const marketBoxLi = $(this).clone();
                $('#rdy_store .rdy_store_body ul').append(marketBoxLi);
            }
        });
    }

    //20200519 "?"버튼 추가 스크립트
    document.querySelectorAll(".popup_gui_close")[0].addEventListener("click",function(){
        document.querySelectorAll(".popup_gui_container")[0].style.display = "none";
    });
    document.querySelectorAll(".popup_gui_btn")[0].addEventListener("click",function(){
        document.querySelectorAll(".popup_gui_container")[0].style.display = "block";
    });
    //영상 팝업 스크립트
    document.querySelectorAll(".edit_video_btn2")[0].addEventListener("click",function(){
        document.querySelectorAll(".edit_video_popup")[0].style.display = "flex";
    });
    document.querySelectorAll(".edit_video_popup_container p")[0].addEventListener("click",function(){
        document.querySelectorAll(".edit_video_popup")[0].style.display = "none";
        document.querySelectorAll(".edit_video_popup iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });
    
</script>