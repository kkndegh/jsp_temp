<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="/assets/js/select2/select2.min.js"></script>
<script src="/assets/js/mngMall.js?ver=20221117_01"></script>

<style>
	.my_section {
		max-width: 51rem !important;
	}

	@media (max-width: 1024px) {
		.my_area .market_box {
			float: left;
			margin: 1.625em 0 0;
			margin-top: 0;
		}

		/* .my_area .choiced_mk_box {
			padding: 0 0 0 7em;
		} */

		.my_area .choiced_mk_box {
			margin-left: 3rem;
			margin-top: -5rem;
			padding: 0;
		}

		.list_choiced_mk {
			padding: 0;
		}
	}

	@media (max-width: 990px) {
		.my_section {
			max-width: 47rem !important;
		}

		.my_area .choiced_mk_box {
			padding: 0 0 0 4em;
		}
	}

	@media (max-width: 720px) {
		.market_box_find {
			width: 100%;
		}

		.my_area .market_box {
			width: 100%;
			float: unset;
		}

		.my_area .choiced_mk_box {
			margin: 0;
			padding: 0;
		}

		.choiced_mk_box .status {
			margin-top: 2rem;
		}
	}

	#usable_store .usable_store_body ul li {
    display: inline;
    align-items: center;
    justify-content: center;
    border: 1px solid #d7d7d7;
    margin: 0 2px 4px;
    height: 150px;
	width: 500px;
    overflow: hidden;
	}

	div#sec_pc div.care_tip div.q_popup {
        background: #1a1a1a;
        padding: 0.5rem 1rem;
        border-radius: 5px;
        display: none;
        position: absolute;
        margin-left: 1.5rem;
    }
    div#sec_mo div.care_tip div.q_popup {
        background: #1a1a1a;
        padding: 0.25rem 0.25rem;
        border-radius: 5px;
        display: none;
        position: absolute;
        margin-left: -19.5rem;
    }

    div#sec_pc div.care_tip div.q_popup h1,
    div#sec_mo div.care_tip div.q_popup h1 {
        color: #fff;
        font-size: 0.75rem;
        display: inline-block;
        vertical-align: middle;
    }

    div#sec_pc div.care_tip div.q_popup img.close_popup,
    div#sec_mo div.care_tip div.q_popup img.close_popup {
        display: inline-block;
        vertical-align: middle;
        margin-left: 1.5rem;
        cursor: pointer;
    }


#usable_store .usable_store_body ul li input{
	align-items: center;
}
</style>

<input type="hidden" id="currentRegCount" value="${custMall.all_mall_cnt - custMall.del_mall_cnt - custMall.mall008.dup_mall_cnt}">
<input type="hidden" id="maxRegCount" value="${maxRegCountInfo.SALE_MALL_REG_ID_PSB_CNT}">
<div class="container">
	<div class="my_wra">
		<div class="title_bg">
			<p class="title">판매몰 관리1</p>
			<p class="desc">등록된 판매몰을 수정, 삭제, 추가 할 수 있습니다..</p>
		</div>
		<div id="sec_pc" style="width:80% !important">
            <div class="row">
                <h1 style="margin-bottom:1rem" class="sec_pc_title">판매몰 현황</h1>
            </div>
            <div id="care_tip" class="care_tip" style="max-width: inherit; margin-left: 50px;">
				<div class="marketReg_box">
                    <div class="status">
                        <p style="float: left;">
                            <select id="menuSelect" style="width: 170px !important;">
                                <option value="ALL">전체</option>
                                <option value="NR">정상</option>
                                <option value="INS">점검</option>
                                <option value="ERR">오류</option>
                                <option value="DGW">정보제공현황</option>
                            </select>
                        </p>
                        <p class="all">전체
                            <span class="all_num">0</span>
                            <br/>
                        </p> /
                        <p class="pass">정상
                            <span>0</span>
                        </p> /
                        <p class="warning">점검
                            <span>0</span>
                        </p> /
                        <p class="error">오류
                            <span>0</span>
                        </p>
                    </div>
                </div>
                <section id="tip_list" class="care_tip_list fl_wrap w100">
                    <ul id="test">
                        <!-- <li class="fl_wrap item1" style="background-color: #f5f2f2; font-size: 16px;">
                            <dl class="th_line">
								<dt style="width: 10%;">상태</dt>
                                <dt style="width: 20%;">판매몰명</dt>
                                <dt style="width: 17%;">아이디</dt>
                                <dt style="width: 12%;">부가 정보</dt>
                                <dt style="width: 20%;">제휴서비스</dt>
								<dt style="width: 10%;">TIP</dt>
                                <dt style="width: 10%;">관리</dt>
                            </dl>
                        </li> -->
                        <!-- <c:choose>
                            <c:when test="${fn:length(custMall.mallSub) == 0}">
                                <li class="fl_wrap">
                                    <dl class="text_center">
                                        판매몰 등록현황 정보가 없습니다.
                                    </dl>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <c:forEach items="${custMall.mallSub}" var="mall" varStatus="status">
                                        <li class="fl_wrap item2 ${mall.cust_mall_sts_cd}" id="${mall.cust_mall_sts_cd }" style="font-size: 14px;">
                                            <dl>
												<dd class="text_left pt10 pl40 pb10" style="width: 10%;">
                                                    <c:choose>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'NR'}">
                                                            <font style="color: #2dabe8;">정상</font>
                                                        </c:when>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'ERR'}">
                                                            <c:choose>
                                                                <c:when test="${mall.scra_err_cd eq 'E013' and (mall.mall_cd eq '046' or mall.mall_cd eq '047')}">
                                                                    <a class="trans_naver_pop" style="cursor:pointer; text-decoration:underline;" data-cust_mall_seq_no="${mall.cust_mall_seq_no}">
                                                                        <font style="color: #ff0000;">인증절차 필요(클릭)</font>
                                                                    </a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <font style="color: #ff0000;">오류</font>
                                                                    <img class="q_icon" src="/assets/images/icon/q.png" data-cust_mall_seq_no="${mall.cust_mall_seq_no}" alt="">
                                                                    <div class="q_popup" id="q_popup_${mall.cust_mall_seq_no}">
                                                                        <h1>${mall.cd_descp}</h1>
                                                                        <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                                                                    </div>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:when>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'INS'}">
                                                            <font style="color: #777777;">점검</font>
                                                        </c:when>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'REG'}">
                                                            등록
                                                        </c:when>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'MOD'}">
                                                            수정
                                                        </c:when>
                                                    </c:choose>
                                                </dd>	
                                                <dd class="text_left pt10 pl05 pb10" style="width: 20%; display: flex; align-items: center;">
                                                    <c:if test="${mall.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.stor_path}${mall.stor_file_nm}" style="width: 5.750em; height: 2em; display: inline-flex; margin-right: 3%;" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" style="width: 5.750em; height: 2em; display: inline-flex; margin-right: 3%;" alt="몰 로고">
                                                    </c:if>
                                                    ${mall.mall_nm }
                                                </dd>
                                                <dd class="text_center pt10 pb10" style="width: 17%;">${mall.mall_cert_1st_id_crypt_val }</dd>
                                                <dd class="text_center pt10 pb10" style="width: 10%;"  style="cursor:pointer;">
													${mall.biz_no}
														<c:if test="${mall.sub_mall_cert_1st_crypt_val != null}">
															/ ${mall.sub_mall_cert_1st_crypt_val}
														</c:if>
												</dd>
                                                <dd class="text_center pt10 pb10" style="width: 20%;">
                                                    <c:set var="loop_flag" value="false" />
                                                    <c:set var="loanUseNm" value="추천금융상품보기" />
                                                    <c:forEach items="${loanCustUseMallList}" var="loanCustUseInfo" varStatus="status">
                                                        <c:if test="${not loop_flag }">
                                                            <c:if test="${mall.cust_mall_seq_no eq loanCustUseInfo.cust_mall_seq_no }">
                                                                <c:if test="${!empty loanCustUseInfo.svc_id}">
                                                                    <c:set var="loop_flag" value="true" />
                                                                    <c:set var="loanUseNm" value="${loanCustUseInfo.svc_nm}" />
                                                                </c:if>
                                                            </c:if>
                                                        </c:if>
                                                    </c:forEach>
                                                    <c:choose>
                                                        <c:when test="${loop_flag }">
                                                            <font style="color: #000000;">${loanUseNm}</font>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="/sub/pre_calculate/pre_calculate" style="color: #2DABE8;text-decoration: underline;">
																 ${loanUseNm}
															</a>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </dd>
												<dd class="text_center account_btn pt10 pb10" style="width: 10%;">
													${mall.cd_nm}
												</dd>
                                                <dd class="text_center account_btn pt10 pb10" style="width: 10%;">
													<div style="text-align:center;">
														<p class="btn_edit" style="cursor: pointer;" onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt}' )">
															<img src="/assets/images/member/icon_2.png" alt="">
														</p>
														<p class="btn_remove" style="cursor: pointer;" data-cust_mall_seq_no='${mall.cust_mall_seq_no}' onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt}' )">
															<img src="/assets/images/member/icon_1.png" alt="">
														</p>
													</div>
                                                </dd>
                                            </dl>
                                        </li> 
                                </c:forEach>
                            </c:otherwise>
                        </c:choose> -->
                    </ul>
                </section>
            </div>
        </div>
		<div class="my_area">
			<div class="my_section">
				<div class="market_choice clearfix">
					<div class="market_box_find">
						<div class="ui-widget">
							<input placeholder="판매몰 검색" class="market_box_find_text_area">
						</div>
					</div>
					<div class="market_box">
						<div class="search_market">
						</div>
						<div class="group_list_mall" data-type-mk="mk_type1">
							<p class="type_market open"><span>오픈마켓</span> <img
									src="/assets/images/member/btn_arrows.png" alt=""></p>
							<ul class="scrollblack mall_lists_1">
								<c:set var="cnt" value="1" />
								<c:forEach items="${mallList}" var="mall" varStatus="status">
									<c:if test="${mall.mall_typ_cd eq 'OPM' && mall.mall_sts_cd eq 'NOR' }">
										<li name="open_${cnt }" id="mallList${mall.mall_cd}">
											<c:if test="${mall.mallLG_file.stor_file_nm != null }">
												<img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
											</c:if>
											<c:if test="${mall.mallLG_file.stor_file_nm == null }">
												<img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
											</c:if>
											<span name="open_${mall.mall_cd }_tag">${mall.mall_nm }</span>
											<input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
												class="sub_mall_cert_1st_nm" />
											<input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
												class="sub_mall_cert_2nd_nm" />
											<input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
												class="mall_cert_2nd_passwd_nm" />
											<input type="hidden" value="${mall.sub_mall_cert_step }"
												class="sub_mall_cert_step" />
											<input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
											<input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
											<input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
												class="sub_mall_auth_1st_esse_yn" />
											<input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
												class="sub_mall_auth_2nd_esse_yn" />
											<input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
											<input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
											<input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
											<input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
											<input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
											<input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
											<input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
											<input type="hidden" value="${mall.help_info }" class="help_info" />
											<input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
											<input type="hidden" value="${mall.video_url }" class="video_url" />
											<input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
											<input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
											<c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
												varStatus="typStatus">
												<input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
													value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
											</c:forEach>
										</li>
										<c:set var="cnt" value="${cnt + 1 }" />
									</c:if>
								</c:forEach>
							</ul>
						</div>
						<div class="group_list_mall" data-type-mk="mk_type2">
							<p class="type_market"><span>소셜커머스</span> <img src="/assets/images/member/btn_arrows.png"
									alt=""></p>
							<ul class="scrollblack mall_lists_2">
								<c:set var="cnt" value="1" />
								<c:forEach items="${mallList}" var="mall" varStatus="status">
									<c:if test="${mall.mall_typ_cd eq 'SNM' && mall.mall_sts_cd eq 'NOR' }">
										<li name="soci_${cnt }" id="mallList${mall.mall_cd}">
											<c:if test="${mall.mallLG_file.stor_file_nm != null }">
												<img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
											</c:if>
											<c:if test="${mall.mallLG_file.stor_file_nm == null }">
												<img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
											</c:if>
											<span name="soci_${mall.mall_cd }_tag">${mall.mall_nm }</span>
											<input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
												class="sub_mall_cert_1st_nm" />
											<input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
												class="sub_mall_cert_2nd_nm" />
											<input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
												class="mall_cert_2nd_passwd_nm" />
											<input type="hidden" value="${mall.sub_mall_cert_step }"
												class="sub_mall_cert_step" />
											<input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
											<input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
											<input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
												class="sub_mall_auth_1st_esse_yn" />
											<input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
												class="sub_mall_auth_2nd_esse_yn" />
											<input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
											<input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
											<input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
											<input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
											<input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
											<input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
											<input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
											<input type="hidden" value="${mall.help_info }" class="help_info" />
											<input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
											<input type="hidden" value="${mall.video_url }" class="video_url" />
											<input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
											<input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
											<c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
												varStatus="typStatus">
												<input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
													value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
											</c:forEach>
										</li>
										<c:set var="cnt" value="${cnt + 1 }" />
									</c:if>
								</c:forEach>
							</ul>
						</div>
					</div>

					<div id="usable_store">
						<div class="usable_store_header">
							<h3>이용 가능 판매몰</h3>
							<p>판매몰 계정 등록 후 바로 데이터 조회가 가능한 판매몰들이에요~</p>
						</div>
						<div class="usable_store_body">
							<ul></ul>
						</div>
						<div class="usable_store_footer">
							<p>판매몰은 계속 추가되고 있으며, 원하시는 판매몰이 있으실 경우 <a href="/sub/my/ask" target="_blank">1:1문의</a>를 통해 요청해주시기 바랍니다.</p>
						</div>
					</div>
					<button type="button" class="btn_confirm_terms" id="mallSubmit" style="width: 55%;">몰 저장</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- init시 수정 대상 몰 정보   -->
<input type="hidden" id="custMallSeqNo" value="${cust_mall_seq_no }">
<script>

	$(document).ready(function () {

		fn_custMallList();

		// 2019-07-17 스크롤 유지
		$(".modal_alarm, .market_box").off("mouseover");
		$(".modal_alarm, .market_box").off("mouseout");

		var clickM;
		var img_mall_name;
		var img_mall_text;

		$(function () {
			var all_mall = new Array();
			$(".market_choice .mall_nm").each(function(){ 
				if ($(this).parent().find(".mall_sts_cd").val() == 'NOR') {
					all_mall.push($(this).val()) 
				}
			});

			$(".market_box_find_text_area").autocomplete({
				source: all_mall,
				select: function (event, ui) {
					$(".popup_account.type1").addClass("active");
					for (var i = 0; i < $('.group_list_mall li').length; i++) {
						if (ui.item.value == $('.group_list_mall li span').eq(i).text()) {
							var img_tags = $('.group_list_mall li span').eq(i).prev('img').attr('src');
							clickM = $('.group_list_mall li').eq(i);
							clickM.click();

						}
					}
				},
				focus: function (event, ui) {
					return false;
				}
			});
		});

		//select2
		function format(option) {
			var originalOption = option.element;
			var valOption = $(originalOption).val();
			if (!option.id) return option.text; // optgroup
			return "<div class='mall_logo'><img src='/assets/images/member/logo/" + valOption + ".png'/>" + option.text + "</div>";
		}	
		function fn_explain(mall_cd){
			alert("설명 데이터");
		}

		$(".select_mall").select2({
			placeholder: "판매몰 검색",
			allowClear: true,
			templateResult: format,
			templateSelection: format,
			escapeMarkup: function (m) {
				return m;
			}
		});

		$(".select_mall").on("select2:select", function (e) {
			var selectMImg = $(".select2-selection__rendered .mall_logo img").attr("src");
			$(".sh_mall_btn").click(function () {
				$(".popup_account.type1").addClass("active");
				$(".popup_account.type1 .logo_area_account img").attr('src', selectMImg);
				$(".popup_account.type1 .close_pop").click(function () {
					$(".popup_account.type1").removeClass("active");
					return false;
				});
			});
		});


		// 전체
		$("#filterAll").on("click", function () {
			$(".group_mall").show();
			$(".group_mall_error").show();
			$(".group_mall_warning").show();
		});
		// 정상
		$("#filterPass").on("click", function () {
			$(".group_mall").show();
			$(".group_mall_error").hide();
			$(".group_mall_warning").hide();
		});
		// 점검
		$("#filterWarning").on("click", function () {
			$(".group_mall").hide();
			$(".group_mall_error").hide();
			$(".group_mall_warning").show();
		});
		// 에러
		$("#filterError").on("click", function () {
			$(".group_mall").hide();
			$(".group_mall_error").show();
			$(".group_mall_warning").hide();
		});

		//init
		(function () {
			if (nonNull($("#custMallSeqNo").val())) {
				fn_updateData($("#custMallSeqNo").val(), '', '', '');
			}

			// 기본 전체
			$(".group_mall").show();
			$(".group_mall_error").show();
			$(".group_mall_warning").show();

		})();
		var mallAddCnt = 1;
		//등록할 판매몰 선택
		$('.market_box').children('.group_list_mall').children('ul').children('li').on('click',function(){
			var $this = $(this);
			var mall_id = $this.attr('id');
			var mall_nm = $this.find('.mall_nm').val(); 								// 몰명		
			var mall_cd = $this.find('.mall_cd').val(); 								// 몰코드				insertParam  : mall_cd
			var sub_mall_cert_step = $this.find('.sub_mall_cert_step').val(); 			// 인증단계				insertParam  : cert_step
			var passwd_step = $this.find(".passwd_step").val(); 						// 비밀번호 단계		insertParam  : passwd_step
			var sub_mall_cert_1st_nm = $this.find('.sub_mall_cert_1st_nm').val();		// 서브몰 인증 1st 명		
			var sub_mall_cert_2nd_nm = $this.find('.sub_mall_cert_2nd_nm').val();		// 서브몰 인증 2nd 명
			var mall_cert_2nd_passwd_nm = $this.find('.mall_cert_2nd_passwd_nm').val(); // 서브몰 인증 2nd 비밀번호 명
			var mall_cert_typ_cd = $this.find('.mall_cert_typ_cd').val(); 				// 몰 인증 유형 코드	insertParam	 : mall_cert_typ_cd(default:A00)
			var otp_auth_use_yn = $this.find(".otp_auth_use_yn").val(); 				// OTP 인증 사용 여부	
			var help_info = $this.find(".help_info").val();								// 등록 방법 

			//var mall_cert_2nd_passwd = 
			// goods_cate_seq_no	// 상품 카테고리 일련번호
			//mall_auth_otp_key		// OTP 인증 KEY 
			html = "";

			html += '<li name = '+mall_id+' style="cursor:pointer;" class="addMall" data-mall_cd="'+mall_cd+'">'+mall_nm+ mallAddCnt;
			html += '<br/>';

			html += '<input type="hidden" class="mall_cd" id="mall_cd" value="'+mall_cd+'"/>';
			html += '<input type="hidden" class="cert_step" id="cert_step" value="'+sub_mall_cert_step+'"/>';
			html += '<input type="hidden" class="passwd_step" id="passwd_step" value="'+passwd_step+'"/>';	
			html += '<input type="hidden" class="otp_auth_use_yn" id="otp_auth_use_yn" value="'+otp_auth_use_yn+'"/>';
			html += '<input type="hidden" class="help_info" id="help_info" value="'+help_info+'"/>';
			
			if($this.find('.mall_cert_typ').length > 0){
				html += '몰인증 유형2 : ';
				html += '<select class="sctbox" id="mall_cert_typ_cd">';
				$this.find(".mall_cert_typ").each(function (i, v) {
					html += "<option value='" + $(this).val() + "'>" + $(this).data("type_nm") + "</option>";
				});
			}	html +='</select>';

			html += 'ID : <input type="text" class="mall_cert_1st_id" id="mall_cert_1st_id" name="mall_cert_1st_id" /><br/>';	
			html += 'PW : <input type="text" class="mall_cert_1st_passwd" id="mall_cert_1st_passwd" name="mall_cert_1st_passwd"/><br/>';

			if($this.find(".sub_mall_cert_1st_nm").val() != "" && sub_mall_cert_1st_nm != null){
				html +=  sub_mall_cert_1st_nm +  ' : <input type="text" class="sub_mall_cert_1st" id="sub_mall_cert_1st" name="sub_mall_cert_1st" /><br/>';
				
				if(mall_cd == '008'){ //스마트스토어
					html += '<button type="button" id="mall_btn">';
					html += '<p style="font-size: 0.8em;">API ID 확인</p></button>';	
				}
			 }

			if ($this.find(".sub_mall_cert_2nd_nm").val() != "" && sub_mall_cert_2nd_nm != null) {
				html +=  sub_mall_cert_2nd_nm +  ' : <input type="text" class="sub_mall_cert_2nd" id="sub_mall_cert_2nd" name="sub_mall_cert_2nd" /><br/>';
			} 

			if ($this.find(".mall_cert_2nd_passwd_nm").val() != "" && mall_cert_2nd_passwd_nm != null) {
				html +=  mall_cert_2nd_passwd_nm +  ' : <input type="text" class="mall_cert_2nd_passwd" id="mall_cert_2nd_passwd" name="mall_cert_2nd_passwd" /><br/>';
			} 
		
			html += '<button class="delete" id="delete" style="float:right;" >닫기</button>';
			html += '</li>';
			$('.usable_store_body').children('ul').append(html);
			mallAddCnt++;
			
		});
		
		//API ID 확인
		$(document).on("click","#mall_btn",function(){
			url = "https://sell.smartstore.naver.com/#/sellers/store/detail/api";
			var win = window.open(url, '_blank');
			win.focus();
		});

		//추가 판매몰 삭제
		$(document).on("click",".delete",function(){
			var $this = $(this);
			$this.parents("li").remove();
		});
		
		$(document).on("click",".addMall",function(e){
			var $this = $(this);
			var mall_cd = $this.data("mall_cd");
			var delBtn = $this.children("button").attr("id");
			alert(delBtn);
			var mall_id = $this.attr("name");
			var help_info = $this.find('.help_info').val();
		
			//$("#'"+mall_cd+"']")
			

			//닫기 버튼 이벤트 제외
			if(!$(e.target).hasClass(delBtn)){
				// 등록방법 데이터 불러 오기 
				//console.log(mall_cd);
				//alert(help_info);
			}
			
		});

		//판매몰 일괄 등록
		$('#mallSubmit').on('click',function(){
			var formData = new Array();
			var length = $('.usable_store_body').children('ul').children('li').length;
			var retrunVal = true;

			if(length > 0){
				$.each($('.usable_store_body').children('ul').children('li') , function(indxe , item){
					const $this = $(this);
					var li_name = $this.attr('name');
					var cust_mall_seq_no = $this.children("#cust_mall_seq_no").val();
					var mall_cd = $this.children(".mall_cd").val();
					var mall_nm = $this.children(".mall_nm").val();
					var cert_step = $this.children(".cert_step").val();
					var passwd_step = $this.children(".passwd_step").val();
					var mall_cert_typ_cd = $this.children("#mall_cert_typ_cd").val();
					var mall_cert_1st_id = $this.children(".mall_cert_1st_id").val();
					var mall_cert_1st_passwd = $this.children(".mall_cert_1st_passwd").val();
					var sub_mall_cert_1st = $this.children(".sub_mall_cert_1st").val();
					var sub_mall_cert_2st = $this.children(".sub_mall_cert_2nd").val();
					var mall_cert_2nd_passwd = $this.children(".mall_cert_2nd_passwd").val();

					// 테스트 값
					var goods_cate_seq_no = 8; // 카테고리 코드
					var mall_auth_otp_key = null;

					if(mall_cert_typ_cd == null || mall_cert_typ_cd == ''){
						mall_cert_typ_cd = "A00";
					}
		
					// if(mall_cert_1st_id == null || mall_cert_1st_id == ''){
					// 	alert("아이디 값을 입력해주세요.");
					// 	$this.children("#mall_user_id").focus();
					// 	retrunVal = false;
					// 	return false;
					// }	

					// if(mall_cert_1st_passwd == null || mall_cert_1st_passwd == ''){
					// 	alert("패스워드를 입력해주세요.");
					// 	$this.children("#mall_user_pass").focus();
					// 	retrunVal = false;
					// 	return false;
					// }	
					
					var pushData  = {
						li_name : li_name,	
						mall_cd : mall_cd,
						cert_step: cert_step,
						mall_cert_1st_id : mall_cert_1st_id,
						mall_cert_1st_passwd : mall_cert_1st_passwd,
						sub_mall_cert_1st : sub_mall_cert_1st,
						sub_mall_cert_2nd : sub_mall_cert_2st,
						mall_cert_2nd_passwd : mall_cert_2nd_passwd,
						passwd_step : passwd_step,
						goods_cate_seq_no : goods_cate_seq_no,
						mall_auth_otp_key : mall_auth_otp_key,
						mall_cert_typ_cd : mall_cert_typ_cd,
						cust_mall_seq_no : cust_mall_seq_no
					};

					formData.push(pushData);
				
				});

				//저장& 수정 몰 등록 폼 삭제
				// $.each(formData,function(i,v){
					
				// 	$('li[name='+v.li_name+'').remove();
					
				// });
				

				// 판매몰 등록
				if(retrunVal){
					var url = "";
					var message = "";
					var errorMessage = "";
					url = '/sub/my/join/custMallSave';
					message = "저장 되었습니다.";
					errorMessage = "저장 실패하였습니다.";

					$.ajax({
						url: url
						, type: 'post'
						, async: true
						, dataType: 'json'
						, contentType: 'application/json'
						, data: JSON.stringify(formData)
						, success: function (data) {
							
						}
						, error: function () {
							
						}
					});
				}
			}else {
				alert("판매몰을 등록해주세요.");
			}
		});

		//판매몰 수정 버튼
		$(document).on('click' ,'.btn_edit',function(){

			var $this = $(this).parents().parents().parents();
			
			var mall_nm = $this.children('.mall_nm').val(); 								// 몰명		
			var mall_cd = $this.children('.mall_cd').val(); 								// 몰코드				insertParam  : mall_cd
			var sub_mall_cert_step = $this.children('.sub_mall_cert_step').val(); 			// 인증단계				insertParam  : cert_step
			var passwd_step = $this.children(".passwd_step").val(); 						// 비밀번호 단계		insertParam  : passwd_step
			var sub_mall_cert_1st_nm = $this.children('.sub_mall_cert_1st_nm').val();		// 서브몰 인증 1st 명		
			var sub_mall_cert_2nd_nm = $this.children('.sub_mall_cert_2nd_nm').val();		// 서브몰 인증 2nd 명
			var mall_cert_2nd_passwd_nm = $this.children('.mall_cert_2nd_passwd_nm').val(); // 서브몰 인증 2nd 비밀번호 명
			//var mall_cert_typ_cd = $this.children('.mall_cert_typ_cd').val(); 				// 몰 인증 유형 코드	insertParam	 : mall_cert_typ_cd(default:A00)
			var otp_auth_use_yn = $this.children(".otp_auth_use_yn").val(); 				// OTP 인증 사용 여부	
			var help_info = null;							// 등록 방법 
			var mall_cert_1st_id = $this.find('dd').eq(2).text();
			var cust_mall_seq_no = $this.children('.cust_mall_seq_no').val();
			

			html = "";

			html += '<li name = "update'+mall_cd+'" style="cursor:pointer;" class="addMall" data-mall_cd="'+mall_cd+'">'+mall_nm+ mallAddCnt;
			html += '<br/>';

			html += '<input type="hidden" class="mall_cd" id="mall_cd" value="'+mall_cd+'"/>';
			html += '<input type="hidden" class="cust_mall_seq_no" id="cust_mall_seq_no" value="'+cust_mall_seq_no+'"/>';
			html += '<input type="hidden" class="cert_step" id="cert_step" value="'+sub_mall_cert_step+'"/>';
			html += '<input type="hidden" class="passwd_step" id="passwd_step" value="'+passwd_step+'"/>';	
			html += '<input type="hidden" class="otp_auth_use_yn" id="otp_auth_use_yn" value="'+otp_auth_use_yn+'"/>';
			html += '<input type="hidden" class="help_info" id="help_info" value="'+help_info+'"/>';
			
			if($this.children('.mall_cert_typ').length > 0){
				html += '몰인증 유형2 : ';
				html += '<select class="sctbox" id="mall_cert_typ_cd">';
				$this.children(".mall_cert_typ").each(function (i, v) {
					html += "<option value='" + $(this).val() + "'>" + $(this).data("type_nm") + "</option>";
				});
			}	html +='</select>';

			html += 'ID : <input type="text" class="mall_cert_1st_id" id="mall_cert_1st_id" name="mall_cert_1st_id" value="'+mall_cert_1st_id+'"/><br/>';	
			html += 'PW : <input type="text" class="mall_cert_1st_passwd" id="mall_cert_1st_passwd" name="mall_cert_1st_passwd"/><br/>';
			
			// if (typeof otp_auth_use_yn != "undefined" ) {
			// 	console.log("2222222222222");
			// }
			
			// if($this.children(".sub_mall_cert_1st_nm").val() != "" && sub_mall_cert_1st_nm != null){
			// 	console.log("1111111111111111");
			// }else{
			// 	console.log("2222222222222222222");
			// }

			if($this.children(".sub_mall_cert_1st_nm").val() != "" && sub_mall_cert_1st_nm != null){
				console.log($this.children(".sub_mall_cert_1st_nm").val());
				html +=  sub_mall_cert_1st_nm +  ' : <input type="text" class="sub_mall_cert_1st" id="sub_mall_cert_1st" name="sub_mall_cert_1st" /><br/>';
				
				if(mall_cd == '008'){ //스마트스토어
					html += '<button type="button" id="mall_btn">';
					html += '<p style="font-size: 0.8em;">API ID 확인</p></button>';	
				}
			 }

			if ($this.children(".sub_mall_cert_2nd_nm").val() != "" && sub_mall_cert_2nd_nm != null) {
				html +=  sub_mall_cert_2nd_nm +  ' : <input type="text" class="sub_mall_cert_2nd" id="sub_mall_cert_2nd" name="sub_mall_cert_2nd" /><br/>';
			} 

			if ($this.children(".mall_cert_2nd_passwd_nm").val() != "" && mall_cert_2nd_passwd_nm != null) {
				html +=  mall_cert_2nd_passwd_nm +  ' : <input type="text" class="mall_cert_2nd_passwd" id="mall_cert_2nd_passwd" name="mall_cert_2nd_passwd" /><br/>';
			} 
		
			html += '<button class="delete" id="delete" style="float:right;" >닫기</button>';
			html += '</li>';
			$('.usable_store_body').children('ul').append(html);
			mallAddCnt++;
			
			
		});

		$("#menuSelect").on("change", function () {
            if (this.value == "ALL") {
                $(".item2").show();
            } else if (this.value == "NR") {
                $(".item2").show();
                $(".INS").hide();
                $(".ERR").hide();
            } else if (this.value == "INS") {
                $(".item2").hide();
                $(".INS").show();
            } else if (this.value == "ERR") {
                $(".item2").hide();
                $(".ERR").show();
            } else if (this.value == "DGW") {
                $(".item2").show();
                $(".item2").find('a').parent().parent().parent().hide();
            }
        });

	
	});
	
	// 판매몰 리스트 html
	var fn_custMallList = function() {

		$.ajax({
			url: '/sub/my/join/custMallList',
			type: 'GET',
			dataType: 'json',
			contentType: 'application/json',
			async : false,
			success: function (data) {
				var mallList = data.mallList;
				var count = data.extReCustMallCntDTO;
				var loanList = data.loanMallList;
				var mallCertTyp = data.mallList.mall_cert_typ;

				//$('#tip_list').children('ul').empty();
				var html = "";
				$('#tip_list').children('ul').empty();

				html +='<li class="fl_wrap item1" style="background-color: #f5f2f2; font-size: 16px;">';
				html +='<dl class="th_line">';
				html +='<dt style="width: 10%;">상태</dt>';
				html +='<dt style="width: 20%;">판매몰명</dt>';
				html +='<dt style="width: 17%;">아이디</dt>';
				html +='<dt style="width: 12%;">부가 정보</dt>';
				html +='<dt style="width: 20%;">제휴서비스</dt>';
				html +='<dt style="width: 10%;">TIP</dt>';
				html +='<dt style="width: 10%;">관리</dt>';
				html +='</dl>';
				html +='</li>';

				if(mallList.length > 0){
					// 
					$('.all_num').text(count.all_cnt - count.del_cnt - count.dup_cnt);
					$('.pass').children('span').text(count.nr_cnt + count.reg_cnt);
					$('.warning').children('span').text(count.ins_cnt);
					$('.error').children('span').text(count.err_cnt);

					$.each(mallList,function(index ,item){
						html += '<li class="fl_wrap item2 '+item.cust_mall_sts_cd +'" id="'+item.cust_mall_sts_cd+'" style="font-size: 14px;">';
						
						html += '<dl>';
						html += '<input type="hidden" class="cust_mall_seq_no" id="cust_mall_seq_no" value="'+item.cust_mall_seq_no+'" />';	
						html += '<input type="hidden" class="mall_nm" id="mall_nm" value="'+item.mall_nm+'" />';
						
						html += '<input type="hidden" class="mall_cd" id="mall_cd" value="'+item.mall_cd+'" />';	
						html += '<input type="hidden" class="sub_mall_cert_step" id="sub_mall_cert_step" value="'+item.sub_mall_cert_step+'" />';	
						html += '<input type="hidden" class="passwd_step" id="passwd_step" value="'+item.passwd_step+'" />';	
						html += '<input type="hidden" class="sub_mall_cert_1st_nm" id="sub_mall_cert_1st_nm" value="'+item.sub_mall_cert_1st_nm+'" />';	
						html += '<input type="hidden" class="sub_mall_cert_2nd_nm" id="sub_mall_cert_2nd_nm" value="'+item.sub_mall_cert_2nd_nm+'" />';	
						html += '<input type="hidden" class="mall_cert_2nd_passwd_nm" id="mall_cert_2nd_passwd_nm" value="'+item.mall_cert_2nd_passwd_nm+'" />';
						html += '<input type="hidden" class="mall_cert_typ_cd" id="mall_cert_typ_cd" value="'+item.mall_cert_typ_cd+'" />';
						html += '<input type="hidden" class="otp_auth_use_yn" id="otp_auth_use_yn" value="'+item.otp_auth_use_yn+'" />';
					
						if(item.mall_cert_typ.length > 0){
							$.each(item.mall_cert_typ,function(index,cert){
								html += '<input type="hidden" class="mall_cert_typ"  data-type_nm="'+cert.mall_cert_typ_nm+'" value="'+cert.mall_cert_typ_cd+'" />';
							})
						}
						
						html += '<dd class="text_left pt10 pl40 pb10" style="width: 10%;">';
							if(item.cust_mall_sts_cd == 'NR'){
								html += '<font style="color: #2dabe8;">정상</font>';
							}else if(item.cust_mall_sts_cd == 'INS'){
								html += '<font style="color: #777777;">점검</font>';
							}else if(item.cust_mall_sts_cd == 'REG'){
								html += '등록';
							}else if(item.cust_mall_sts_cd == 'MOD'){
								html += '수정';	
							}else if(item.cust_mall_sts_cd == 'ERR'){
								if(item.scra_err_cd == 'E013' && (item.mall_cd == '046' || item.mall_cd == '047')){
									html += '<a class="trans_naver_pop" style="cursor:pointer; text-decoration:underline;" data-cust_mall_seq_no="'+item.cust_mall_seq_no+'">';
									html += '<font style="color: #ff0000;">인증절차 필요(클릭)</font>';
									html += '</a>'
								}else{
									html += '<font style="color: #ff0000;">오류</font>';
								}
							}
						html += '<img class="q_icon" src="/assets/images/icon/q.png" data-cust_mall_seq_no="54801" alt="">';
						html += '<div class="q_popup" id="q_popup_54801">';
						html += '<h1>판매몰 아이디, 패스워드를 확인해 주시기 바랍니다.';
						html += '</h1>';
						html += '<img class="close_popup" src="/assets/images/icon/x_white.png" alt="">';
						html += '</div>';
						html += '</dd>';
						html += '<dd class="text_left pt10 pl05 pb10" style="width: 20%; display: flex; align-items: center;">';
						if(item.stor_file_nm != null){
							html += '<img src="/imagefile/'+item.stor_path+item.stor_file_nm+'" style="width: 5.750em; height: 2em; display: inline-flex; margin-right: 3%;" alt="몰 로고">';
						}else{
							html += '<img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" style="width: 5.750em; height: 2em; display: inline-flex; margin-right: 3%;" alt="몰 로고">';	
						} 	
						html += ''+item.mall_nm+'</dd>';
						html += '<dd class="text_center pt10 pb10" style="width: 17%;">'+item.mall_cert_1st_id+'</dd>';
						html += '<dd class="text_center pt10 pb10" style="width: 10%;">';
						html += item.biz_no;
						if(item.sub_mall_cert_1st != null && item.sub_mall_cert_1st !=""){
							html += '/ '+item.sub_mall_cert_1st+'';
						} 
						html += '</dd>';
						html += '<dd class="text_center pt10 pb10" style="width: 20%;">';
							//이미지로 할거임
							$.each(loanList , function(i,loan){
								html += i;
								if(item.cust_mall_seq_no == loan.cust_mall_seq_no ){
									if(i > 0){
										html += ', ';
									}
									html += loan.svc_nm;
								}
							});
						html += '</dd>';
						html += '<dd class="text_center account_btn pt10 pb10" style="width: 10%;">'
							if(item.cd_nm != null){
								html += item.cd_nm;
							}
						html +=	'</dd>';
						html += '<dd class="text_center account_btn pt10 pb10" style="width: 10%;">';
						html += '<div style="text-align:center;">';
						html += '<p class="btn_edit" style="cursor: pointer;" data-cust_mall_seq_no="'+item.cust_mall_seq_no+'">';
						html += '<img src="/assets/images/member/icon_2.png" alt="">';
						html += '</p>';
						html += '<p class="btn_remove" style="cursor: pointer;" data-cust_mall_seq_no="'+item.cust_mall_seq_no+'">';
						html += '<img src="/assets/images/member/icon_1.png" alt="">';
						html += '</p>';
						html += '</div>';
						html += '</dd>';
						html += '</dl>';
						html += '</li>';
					});
				}else{
					html += '<li class="fl_wrap">';
					html += '<dl class="text_center">';
					html += '판매몰 등록현황 정보가 없습니다.';
					html += '</dl>';
					html += '</li>';
				}		
			
				$('#tip_list').children('ul').append(html);
			}
		});
	}

	//20200519 "?"버튼 추가 스크립트


	/**
	 * 이용 가능 판매몰 목록 추가
	 */
	//  if( $('#usable_store').length > 0 ) {
	// 	$('.market_box li').each(function(){
	// 		const tt = $(this);
	// 		var mall_typ_cd = tt.children(".mall_typ_cd").val();
	// 		var mall_sts_cd = tt.children(".mall_sts_cd").val();

	// 		if(mall_typ_cd != 'OCM') {
	// 			const marketBoxLi = $(this).clone();
	// 			$('#usable_store .usable_store_body ul').append(marketBoxLi);
	// 		}
	// 	});
	// }
	
</script>
<style>
	

</style>