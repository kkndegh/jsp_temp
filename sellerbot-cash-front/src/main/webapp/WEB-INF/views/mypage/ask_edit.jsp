<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>

<div class="container">
	<div class="my_wra">
		<div class="menu_top">
			<p class="menu_name">마이페이지</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">1:1 문의</p>
			<p class="desc">담당자에게 1:1 문의를 등록할 수 있으며 질문했던 내역을 볼 수 있습니다.</p>
		</div>
		<div class="cs_area">
			<div class="ask_section">
				<form action="/sub/my/ask.html" onsubmit="return false;">
					<div class="ask_frm">
						<p class="title">1:1 문의</p>
						<div class="item_ask_input">
							<p class="lb_ask"><b class="must_mark_ask">*</b>문의유형</p>
							<div class="input_ask">
								<select class="sctbox" id="adv_typ_cd" name="adv_typ_cd" required="required">
									<option value="">선택</option>
									<option value="USG" ${req.adv_typ_cd eq 'USG' ? 'selected="selected"' : '' }>이용 방법
									</option>
									<option value="LIN" ${req.adv_typ_cd eq 'LIN' ? 'selected="selected"' : '' }>로그인
									</option>
									<option value="ERR" ${req.adv_typ_cd eq 'ERR' ? 'selected="selected"' : '' }>장애
									</option>
									<option value="ETC" ${req.adv_typ_cd eq 'ETC' ? 'selected="selected"' : '' }>기타
									</option>
								</select>
								<div class="ask_sub_text_area error" id="adv_typ_cd_err" style="display: none;">
									<h1>문의유형을 선택해주세요</h1>
								</div>
							</div>
						</div>
						<div class="item_ask_input">
							<p class="lb_ask"><b class="must_mark_ask">*</b>제목</p>
							<div class="input_ask">
								<input type="text" class="textbox_mb" id="adv_title" name="adv_title"
									placeholder="제목을 입력해주세요." value="${req.adv_title }" required="required">
								<div class="ask_sub_text_area error" id="adv_title_err" style="display: none;">
									<h1>제목이 입력되지 않았습니다.</h1>
								</div>
							</div>

						</div>
						<div class="item_ask_input item3">
							<p class="lb_ask"><b class="must_mark_ask">*</b>내용</p>
							<div class="input_ask">
								<textarea class="txtarea" id="adv_cont" name="adv_cont" placeholder="문의하실 내용을 구체적으로 입력해주시면 신속한 답변 처리가 가능합니다.
(내용을 10자 이상 입력해주세요.)" required="required">${req.adv_cont }</textarea>
								<div class="ask_sub_text_area error" id="adv_cont_err" style="display: none;">
									<h1>내용이 10자 이상 입력되지 않았습니다.</h1>
								</div>
							</div>
						</div>
						<div class="item_ask_input">
							<p class="lb_ask">이미지</p>
							<div class="input_ask file">
								<input type="text" class="textbox_file" disabled>
								<label for="fileUpload">찾아보기</label>
								<input type="file" id="fileUpload" name="adv_req_file" class="hidden_file"
									disabled="disabled">
								<input type="hidden" id="adv_req_file_del_yn" value="N">
								<button id="btnDelete" class="btn ask-file-delete">삭제</button>
								<div class="ask_sub_text_area error" id="adv_req_file_err" style="display: none;">
									<h1 class="ask_sub_text_area_text">
										파일 크기는 10MB 이하의 파일만 업로드 가능합니다.<br />
										파일 유형은 JPG,GIF,BMP,TIF,PNG,PDF 파일만 업로드 가능합니다.
									</h1>
								</div>
							</div>

						</div>
					</div>
					<div class="btn_ask_frm">
						<button class="btn btn_blue" onclick="fn_update(); return false;">수정</button>
						<button type="button" class="btn" onclick="window.location.href='/sub/my/ask'">취소</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	var _stor_file_nm = '${req.adv_req_file.stor_file_nm }';

	$(document).ready(function () {

		if (isNull(_stor_file_nm)) {
			$("#btnDelete").hide();
			$("#fileUpload").prop("disabled", false);
		} else {
			$("#btnDelete").show();
			$(".textbox_file").val(_stor_file_nm);
			$("#fileUpload").prop("disabled", true);
		}

		// 파일 삭제시
		$("#btnDelete").on("click", function () {
			$("#adv_req_file_del_yn").val("Y");
			$(".textbox_file").val("");
			$("#fileUpload").prop("disabled", false);
		});

	});


	$(".text_n").click(function () {
		var notice = $(this).parents(".title_notice");
		if (notice.hasClass("active") == true) {
			notice.removeClass("active");
		} else {
			$(".title_notice").removeClass("active");
			notice.addClass("active");
		}
		$(".close_btn_notice").click(function () {
			$(this).parents(".title_notice").removeClass("active");
		});
	});

	$(".title_ask").click(function () {
		var prTd = $(this).parents(".td_item_ask");
		if (prTd.hasClass("open") == true) {
			prTd.removeClass("open");
		} else {
			$(".td_item_ask").removeClass("open");
			prTd.addClass("open");
		}
	});

	//file custom
	var fileTarget = $('.input_ask .hidden_file');

	fileTarget.on('change', function () {

		var filename = $("input[name=adv_req_file]")[0].files[0].name.split(".");
		var fileExtension = filename[filename.length - 1];
		var fileSize = $("input[name=adv_req_file]")[0].files[0].size;
		var target = $(this).siblings('.textbox_file');

		if (fileSize < 10485760 && isAllowImage(fileExtension)) {
			$("#adv_req_file_err").hide();
		} else {
			$("#adv_req_file_err").show();
			$(this).val("");
			target.val("");
			return false;
		}
		target.val(filename.join(".") + "(" + fnFileSizeUnit(fileSize) + ")");
		$("#adv_req_file_del_yn").val("Y");

	});

	// 필수 입력 체크
	function isValidation() {
		var result = true;
		var focusTarget;
		$(".error").hide();
		$("[required=required]").each(function () {
			var jqThis = $(this);
			var id = jqThis.attr("id");
			if (isNull(jqThis.val())) {
				$("#" + id + "_err").show();
				if (isNull(focusTarget)) { focusTarget = id; }
				result = false;
			}

			if (id == "adv_cont") {
				if (jqThis.val().length < 10) {
					$("#" + id + "_err").show();
					if (isNull(focusTarget)) { focusTarget = id; }
					result = false;
				}
			}
		});
		$("#" + focusTarget).focus();
		return result;
	}

	function fn_update() {

		// 필수 등록 체크
		if (!isValidation()) {
			return false;
		}

		var formData = new FormData();
		if ($("input[name=adv_req_file]").val()) {
			var filename = $("input[name=adv_req_file]")[0].files[0].name.split(".");
			var fileExtension = filename[filename.length - 1]
			var fileSize = $("input[name=adv_req_file]")[0].files[0].size;
			if (fileSize < 10485760 && isAllowImage(fileExtension)) {
				$("#adv_req_file_err").hide();
			} else {
				$("#adv_req_file_err").show();
			}
			formData.append("adv_req_file", $("input[name=adv_req_file]")[0].files[0]);
		}

		formData.append("adv_typ_cd", $("#adv_typ_cd").val());
		formData.append("adv_title", $("#adv_title").val());
		formData.append("adv_cont", $("#adv_cont").val());
		formData.append("adv_req_seq_no", '${req.adv_req_seq_no}');
		formData.append("adv_req_file_del_yn", $("#adv_req_file_del_yn").val());


		$.ajax({
			url: '/sub/my/ask_update',
			data: formData,
			processData: false,
			contentType: false,
			async: false,
			type: 'POST',
			success: function (data) {
				showAlert("수정 하였습니다.", function () {
					location.href = '/sub/my/ask';
				});
			},
			error: function (request, status, error) {
				showAlert("수정 실패하였습니다.");
			}
		});
	}
</script>