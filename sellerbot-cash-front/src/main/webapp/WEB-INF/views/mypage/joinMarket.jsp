<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
<script src="/assets/pubMarket/js/loanPop.js?ver=20231130_01"></script>
<script type="module" src="/assets/pubMarket/js/index.js?ver=20230719_01" defer></script>
<link rel="stylesheet" href="/assets/pubMarket/css/index.css?ver=20230719_01">

<link rel="stylesheet" href="https://uicdn.toast.com/editor/latest/toastui-editor-viewer.css" />
<script src="https://uicdn.toast.com/editor/latest/toastui-editor-viewer.js"></script>

<input type="hidden" id="currentRegCount" value="0">
<input type="hidden" id="maxRegCount" value="${maxRegCountInfo.SALE_MALL_REG_ID_PSB_CNT}">

<div id="wrap">
	<button id="goTop">
		<i class="fa-solid fa-caret-up"></i>
		<p>TOP</p>
	</button>
	<div class="myModal">
		<div class="content">
			<div class="top">
				<p class="modalTitle">제목</p>
				<button id="title_closeModal"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
			</div>
			<button id="closeModal"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
			<div class="main">
				<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem quo distinctio esse laudantium
					vero
					dolores quasi, magnam tenetur ratione repellat delectus ipsam, recusandae vel soluta amet
					consectetur voluptatum? Veniam, adipisci?</p>
			</div>
			<div class="bottom">
			</div>
		</div>
	</div>
	<div class="menu_top" style="padding: 8.5em 29px 3.5em;">
		<p class="menu_name">마이페이지</p>
		<div class="gnb_topBox">
			<ul class="gnb_top clearfix">
				<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
				<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
					<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
						<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">    
							<c:choose>
								<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
									<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>
	<div class="title_bg">
		<p class="title">판매몰 관리</p>
		<p class="desc">등록된 판매몰을 수정, 삭제, 추가 할 수 있습니다..</p>
	</div>
	<article id="article_0">
		<p class="subTitle">등록한 판매몰</p>
		<span class="line"></span>
		<c:if test="${fn:length(selectList) > 0}">
			<button class="black kbBtn">금융 상품 판매몰 관리</button>
		</c:if>
		<div class="listTopFlex">
			<div class="statusBtns">
				<button class="color_black on" id="ALL">전체<btn-num id="all_cnt">0</btn-num></button>
				<button class="color_blue" id="NR">정상<btn-num id="nr_cnt">0</btn-num></button>
				<button class="color_gray" id="RUN">수집중<btn-num id="run_cnt">0</btn-num></button>
				<button class="color_silver" id="RDY">대기<btn-num id="rdy_cnt">0</btn-num></button>
				<button class="color_red" id="ERR">오류<btn-num id="err_cnt">0</btn-num></button>
				<button class="color_yellow" id="INS">점검<btn-num id="ins_cnt">0</btn-num></button>
				<button class="color_purple" id="OCM">오픈예정<btn-num id="ocm_cnt">0</btn-num></button>
			</div>
		</div>
		<div class="listHead">
			<dt>상태</dt>
			<dt>판매몰</dt>
			<dt>아이디</dt>
			<dt>부가정보</dt>
			<dt>제휴서비스</dt>
			<dt>TIP</dt>
			<dt>관리</dt>
		</div>
		<div class="listWrap scrollable">
			<ul></ul>
			<div class="cv-spinner">
				<span class="spinner"></span>
			</div>
			<ol></ol>
		</div>
		<div class="listBottom">
			<button id="showListBtn"><b>전체보기</b></button>
		</div>
	</article>
	<article id="article_1">
		<p class="subTitle">지원 판매몰 리스트</p>
		<span class="line"></span>
		<div class="gridTopFlex">
			<div class="left">
				<input type="text" placeholder="판매몰 검색" class="market_box_find_text_area">
				<button id="searchText"><img src="/assets/pubMarket/img/magnifier.svg" alt="search"></button>
			</div>
			<div class="right">
				<button id="beOpenedBtn" class="black">오픈예정몰(사전등록)</button>
				<a id="guideBtn" href="https://www.sellerbot.co.kr/static/guide/index.html" target="_blank">
					<button class="black">등록가이드</button>
				</a>
			</div>
		</div>
		<div id="top10" class="grayBox scrollable">
			<p class="gridTitle">TOP10</p>
			<div class="gridArea"></div>
		</div>
		<div id="allMalls" class="grayBox scrollable">
			<p class="gridTitle">이름순</p>
			<div class="gridArea">
				
			</div>
		</div>
	</article>
	<article id="article_2">
		<p class="subTitle">판매몰 등록/수정</p>
		<span class="line"></span>
		<div class="flexArea">
			<div class="left">
				<div class="contents scrollable">
					<h3 class="noDataText"> 등록할 판매몰을<br>[ 지원 판매몰 리스트 ] 에서 선택해주세요. </h3>
					<div class="modifyArea">

					</div>
					<div class="registerArea">
						
					</div>
				</div>
				<div class="btns">
					<button id="initialize">초기화</button>
					<button id="updateAll">한 번에 업데이트</button>
				</div>
			</div>
			<div class="right">
				<div class="topTitle">
					<p>판매몰 도움말이 없습니다.</p>
                    <button class="reg" style="display:none;">등록 방법</button>
				</div>
				<!-- <div class="contents scrollable" id="helpInfo"></div> -->
				<div class="contents scrollable" id="viewer"></div>
				<div class="btns">
					<button id="videoGuide">영상가이드</button>
					<button id="showDetail">상세보기</button>
				</div>
			</div>
		</div>
	</article>
	<div class="bottomBar">
		<div class="closeBtnBox">
			<button id="closeBottomBar"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
		</div>
		<div class="centerBox">
			<p>현재 선택한 판매몰은 <b id="total">0</b>건 입니다. (<text-highlight>등록 <b id="reg">0</b>건 / 수정 <b id="mod">0</b>건</text-highlight>)<br>아래 판매몰 등록/수정에서 입력을 완료해주세요.</p>
			<div class="btns">
				<button id="hideBottomBar">닫기</button>
				<button id="goto">바로가기</button>
			</div>
		</div>
	</div>

	<div style="display: none;">
        <c:if test="${not empty loanBatchProTgtInfo.batchProTgt_list }">
            <c:set var="batPrcsTgtSeqNo" value="${loanBatchProTgtInfo.batchProTgt_list[0].bat_prcs_tgt_seq_no }" />
        </c:if>
        <c:forEach items="${loanBatchProTgtInfo.loanCustMall_list}" var="loanInfo" varStatus="status">
            <input type="checkbox" name="loan_chk" class="loan_cust_mall"
                    data-cust-seq-no="${loanInfo.cust_seq_no}"
                    data-cust-mall-seq-no="${loanInfo.cust_mall_seq_no}" 
                    data-mall-cd="${loanInfo.mall_cd}"
                    data-bat-prcs-tgt-seq-no="${batPrcsTgtSeqNo}"
                    data-loan-sts-cd="${loanInfo.loan_sts_cd}"
                    data-loan-svc-descp="${loanInfo.svc_descp}"
                    <c:if test="${not empty loanInfo.check_batch_pro_tge}"> checked disabled </c:if> 
            >
        </c:forEach>
        </div>
	<!-- S:금융대출 제휴사 판매몰 관리 팝업 20210303--> 
	<div class="popup_account popup_account2 loan" style="max-width : 50%;">
		<div class="pop_account">
			<div class="pop_head">
				<p></p>
				<img class="popup_loan_close" src="/assets/images/member/close.png" alt="" />
			</div>
			<div class="pop_body pb20">
				<div class="pd20">
				</div>
					<style>
						/* TODO 2022.12.07 수정 */
						.pop_list {
							border: 1px solid #ddd;
							border-radius: 8px;
							padding: 0 0.6em 0 0;
							margin-left: 60%;
							margin-right: 4%;
							margin-bottom: 10px;
						}
						.pop_sctbox {
							display: inline-block;
							vertical-align: middle;
							font-size: 1em;
							-webkit-appearance: none;
							-moz-appearance: none;
							appearance: none;
							border: 0;
							color: #888888;
							width: 12.438em;
							line-height: 1.7em;
							background: url(/assets/images/common/sct.png) center right 10px no-repeat;
							-webkit-background-size: 12px 10px;
							background-size: 12px 10px;
							width: 100%;
						}
					</style>
					<!-- //TODO 2022.12.07 수정 -->
					<div class="pop_list">
						<select class="pop_sctbox" id="pop_mall" name="pop_mall">
							<c:if test="${fn:length(selectList) > 0}">
								<c:forEach var="sData" items="${selectList}" varStatus="status">
									<option value="${sData.cd}">
										${sData.cd_nm}
									</option>
								</c:forEach>
							</c:if>
						</select>
					</div>
				<div class="table_style01 scroll_y">
					<style>
						.mall_sts_cd_store_con {
							position: relative;
							display: block;
							left: 2px;
							top: 8px;
							width: 8px;
							height: 8px;
							border-radius: 50%;
						}

						.mall_sts_cd_store_con.bullet_blu {
							background-color: #598ae2;
						}

						.mall_sts_cd_store_con.bullet_red {
							background-color: #fd0e19;
						}

						.mall_sts_cd_store_con.bullet_gry {
							background-color: #aaa;
						}

					</style>
					<table class="w95">
						<c:set var="RONIBOT" value="" />
						<thead>
							<tr id="frist_line">
								<th style="width: 17%;">선택 여부</th>
								<th>타사 선정산<br> 사용 여부</th>
								<th>판매몰 명</th>
								<th>로그인<br>아이디</th>
								<th>데이터 수집<br> 현황</th>
								<th>등록 일자</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="pop_foot fl_wrap">
				<button type="button" class="btn_left fl_left loan_save">저장</button>
				<button type="button" class="btn_gray2 btn_cancle fl_left loan_close">취소</button>
			</div>
		</div>
	</div>
	<!-- E:금융대출 제휴사 판매몰 관리 팝업 20210303 --> 
	<div class="container"></div>
	<!-- s: 슬라이드 배너 20210415 -->
	<%@include file="../widgets/banner_vertical.jsp" %>
	<!-- e: 슬라이드 배너 20210415 -->
</div>

<div class="new_popup_wrap">
    <div class="display_none_back"></div>
    <div class="pop_up_1 pop_up_all">
        <div class="popup_top_title">
            <h1>인증절차 필요 - 네이버페이</h1>
            <div class="add_pooup_close">
                <div></div>
                <div></div>
            </div>
        </div>
        <h1 class="popup_title_2">네이버페이 업데이트를 위해선,</br>
            아래의 이메일 인증 절차가 필요해요~</br>
            <a style="color: #2DABE8;">인증 이메일 : <span id="user_mail"></span></a>
        </h1>
        <div class="pop_up_text_wrap" style="height:15rem;">
            <span class="naver_text">
                <p>&lt;인증 절차&gt;</p>
                <p>1. 아래 업데이트 클릭</p>
                <p>2. <a style="color: #2DABE8;">인증 이메일</a>로 로그인 후 받은 메일함 확인</p>
                <p>3. 네이버페이에서 발송된 인증용 메일 클릭</p>
                <p>4. ‘인증하기’ 클릭</p>
                <p>(업데이트 클릭 후 3분 안에 메일 ‘인증하기‘ 해주셔야 해요~)</p>
            </span>
        </div>
        <div class="button_row">
            <button type="button" class="close">닫기</button>
            <button type="button" class="chg_ticket" data-cust_mall_seq_no="">업데이트</button>
        </div>
    </div>
</div>

<script>
	new toastui.Editor({
		el: document.querySelector('#viewer'),
		initialValue: ""
	});
</script>