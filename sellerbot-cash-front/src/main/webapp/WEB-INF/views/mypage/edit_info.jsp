<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">회원정보</p>
            <p class="desc">회원정보(비밀번호, 대표자휴대폰번호, 담당자, 담당자휴대폰번호) 변경이 가능합니다.</p>
        </div>
        <div class="my_area">
            <!--프리미언 서비스 가입전-->
            <!--<a href="/sub/service/guide.html" class="p_icon"><img src="/assets/images/member/premiumService_button.png" alt=""></a>-->
            <div class="my_section">
                <!-- <div class="sub_menu_title">
                        <a href="/sub/service/use.html" class="icon_using">
                            <img src="/assets/images/my/icon_diamond.png" alt=""> 프리미엄 서비스 이용중
                        </a>
                    </div> -->
                <div class="member_frm_box">
                    <div class="item_mb">
                        <span class="lb">아이디</span>
                        <div class="input_box">
                            <p class="value">${cust.cust_id}</p>
                        </div>
                    </div>
                    <div class="item_mb">
                        <span class="lb">비밀번호</span>
                        <div class="input_box">
                            <a href="/sub/my/change_pw" class="change_pw"><img src="/assets/images/my/icon_lock.png"
                                    alt="">비밀번호 변경</a>
                        </div>
                    </div>

                    <div class="item_mb">
                        <span class="lb">상호명</span>
                        <div class="input_box">
                            <p class="value">${cust.biz_nm}</p>
                        </div>
                    </div>
                    <div class="item_mb ">
                        <span class="lb">사업자 번호</span>
                        <div class="input_box">
                            <p class="value">${cust.biz_no}</p>
                        </div>
                    </div>
                    <div class="item_mb">
                        <span class="lb">대표자명</span>
                        <div class="input_box">
                            <p class="value">${cust.ceo_nm}</p>
                            <%-- <input type="text" style="max-width:200px;" class="textbox_mb" id="ceo_nm" value="${cust.ceo_nm}"> --%>
                            <input type="hidden" id="ceo_nm" value="${cust.ceo_nm}">
                        </div>
                    </div>
                    <div class="item_mb">
                        <span class="lb">대표자 연락처</span>
                        <div class="input_box">
                            <select class="sctbox sctbox_tel" id="ceo_no1">
                                <option value="010">010</option>
                                <option value="011">011</option>
                                <option value="016">016</option>
                                <option value="017">017</option>
                                <option value="018">018</option>
                                <option value="019">019</option>
                            </select>
                            <input type="text" class="textbox_mb textbox_mb_tel" required="" id="ceo_no2">
                            <p class="error" id="ceoNoError" style="display: none;">*휴대폰번호를 다시 확인해주세요</p>
                        </div>
                    </div>
                    <div class="item_mb">
                        <span class="lb">담당자명</span>
                        <div class="input_box">
                            <input type="text" style="max-width:200px;" class="textbox_mb" id="chrg_nm"
                                value="${cust.chrg_nm}">
                        </div>
                    </div>
                    <div class="item_mb">
                        <span class="lb">담당자 연락처</span>
                        <div class="input_box">
                            <select class="sctbox sctbox_tel" id="chrg_no1">
                                <option value="010">010</option>
                                <option value="011">011</option>
                                <option value="016">016</option>
                                <option value="017">017</option>
                                <option value="018">018</option>
                                <option value="019">019</option>
                            </select>
                            <input type="text" class="textbox_mb textbox_mb_tel" required="" id="chrg_no2">
                            <p class="error" id="chrgNoError" style="display: none;">*휴대폰번호를 다시 확인해주세요</p>
                        </div>
                    </div>
                    <div class="item_mb">
                        <p class="msg_time_alarm">아이디변경 및 사업장정보(상호명,사업자번호, 대표자명) 변경은 <a href="/sub/my/ask">1:1 문의</a>를 이용하여주시기 바랍니다.</p>
                    </div>
                    <c:if test="${!empty MT3TermsId}">
                    <div class="item_mb">
                        <div class="input_box">
                            <input type="checkbox" class="mktchkbox" name="chkbox_1" id="mktchkbox">
                            <label for="mktchkbox" class="value">[${MT3TermsEsseYn == 'Y' ? '필수':'선택' }] ${MT3TermsTitle}</label>
                            <a href="" termid="${MT3TermsId}" class="link_agree">자세히보기</a>
                        </div>
                    </div>
                    </c:if>
                    <c:if test="${!empty SMTTermsId}">
                        <div class="item_mb">
                            <div class="input_box">
                                <input type="checkbox" class="smtchkbox" name="chkbox_2" id="smtchkbox">
                                <label for="smtchkbox" class="value">[${SMTTermsEsseYn == 'Y' ? '필수':'선택' }] ${SMTTermsTitle}</label>
                                <a href="" termid="${SMTTermsId}" class="link_agree">자세히보기</a><br>
                                <c:forEach var="smtTermsItem" items="${smtTermsList}">
                                    <div class="item_agree_smt">
                                        <input type="checkbox" class="smtItemChkbox" id="smtTermsAgree${smtTermsItem.com_cd}" 
                                            name="smtTerm"
                                            value="${smtTermsItem.com_cd}"smt_aggr_item_seq_no
                                            <c:forEach var="smtAgreeItem" items="${smtAgreeItemList}">
                                                <c:if test="${smtAgreeItem.smt_terms_cd == smtTermsItem.com_cd}">
                                                    checked
                                                </c:if>
                                            </c:forEach>
                                        >
                                        <label for="smtTermsAgree${smtTermsItem.com_cd}">
                                            <c:out value="${smtTermsItem.cd_nm}" />
                                        </label>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </c:if>
                </div>


            </div>
            <div class="btn_right">
                <button class="btn_circle">수정완료</button>
            </div>
        </div>
    </div>
    <div class="popup popup_confirm_edit">
        <div class="pop">
            <span class="close_pop"><img src="/assets/images/member/close_gray.png" alt=""></span>
            <div class="pop_body">
                <p class="desc_pop">정보 수정이<br>
                    완료되었습니다!</p>
            </div>
            <div class="pop_footer">
                <button type="button" class="close_btn">닫기</button>
            </div>
        </div>
    </div>

    <!-- 약관 정보 -->
    <!--팝업-->
    <div class="new_popup_wrap">
        <div class="display_none_back"></div>
        <div id="termpopup${MT3TermsId}" class="pop_up_1 pop_up_all">
            <div class="popup_top_title">
                <h1>
                    [${MT3TermsEsseYn == 'Y' ? '필수':'선택' }]
                    <c:out value="${MT3TermsTitle }" />
                </h1>
                <div class="add_pooup_close">
                    <div></div>
                    <div></div>
                </div>
            </div>
            <div class="pop_up_text_wrap">
                ${sf:textToHtml(MT3TermsCont)}
            </div>
            <button type="button" class="btn_confirm_terms join_step_popup_close">약관확인</button>
        </div>
        <div id="termpopup${SMTTermsId}" class="pop_up_1 pop_up_all">
            <div class="popup_top_title">
                <h1>
                    [${SMTTermsEsseYn == 'Y' ? '필수':'선택' }]
                    <c:out value="${SMTTermsTitle }" />
                </h1>
                <div class="add_pooup_close">
                    <div></div>
                    <div></div>
                </div>
            </div>
            <div class="pop_up_text_wrap">
                ${sf:textToHtml(SMTTermsCont)}
            </div>
            <button type="button" class="btn_confirm_terms join_step_popup_close">약관확인</button>
        </div>
    </div>
    <!-- 약관 정보 종료 -->

    <!-- 셀러봇 캐시 마케팅 약관 정보 -->
    <!--팝업-->
    <!-- 셀러봇 캐시 마케팅 약관 정보 종료 -->
</div>

<script>
    $(document).ready(function () {

        var _ceo_no = "${cust.ceo_no}";
        var _chrg_no = "${cust.chrg_no}";

        if (isMobile(_ceo_no)) {
            $("#ceo_no1").val(_ceo_no.substr(0, 3));
            $("#ceo_no2").val(_ceo_no.substr(3));
        }

        if (isMobile(_chrg_no)) {
            $("#chrg_no1").val(_chrg_no.substr(0, 3));
            $("#chrg_no2").val(_chrg_no.substr(3));
        }

        // 공통 script.js 이벤트 삭제
        $(".input_box .link_agree").off("click");
        // 각각 약관 마다 모달 호출
        $(".input_box .link_agree").click(function () {
            var termid = $(this).attr("termid");
            $(".new_popup_wrap").css("display", "flex");
            $(".display_none_back").css("display", "block");
            $("#termpopup" + termid).show();
            return false;
        });

        // 약관 닫기
        $(".join_step_popup_close").click(function () {
            $(".new_popup_wrap").css("display", "none");
            $(".pop_up_all").css("display", "none");
            $(".display_none_back").css("display", "none");
        });
        $(".add_pooup_close").click(function () {
            $(".new_popup_wrap").css("display", "none");
            $(".pop_up_all").css("display", "none");
            $(".display_none_back").css("display", "none");
        });

        var MT3TermsId = "${MT3TermsId}";
        var mktchkbox = document.getElementById("mktchkbox");
        var chkterms = "";
        
        $.ajax({
			url:"/sub/pre_calculate/detail/term?termsId="+MT3TermsId,
			success:function(data){
			    if(data == 'true'){
                    mktchkbox.checked = true;
                    chkterms = true;
				}else if (data == 'false') {
                    mktchkbox.checked = false;
                    chkterms = false;
                }
			}
		});

        var SMTTermsId = "${SMTTermsId}";
        var smtchkbox = document.getElementById("smtchkbox");
        var smtChkFlag = "";

        $.ajax({
			url:"/sub/pre_calculate/detail/term?termsId="+SMTTermsId,
			success:function(data){
			    if(data == 'true'){
                    smtchkbox.checked = true;
                    smtChkFlag = true;
				}else if (data == 'false') {
                    smtchkbox.checked = false;
                    smtChkFlag = false;
                }
			}
		});

        // 20221118 셀러봇 캐시 마케팅 정보 제공 동의 추가
        $(".smtchkbox").click(function () {
            var allchk = $(this);
            
            if (allchk.is(":checked") == true) {
                $(".item_agree_smt .smtItemChkbox").prop("checked", true);
            } else {
                $(".item_agree_smt .smtItemChkbox").prop("checked", false);
            }
        });

        $(".item_agree_smt .smtItemChkbox").click(function () {
            var chkThis = $(this);

            var smtItemChkCnt = chkThis.parents().find(".item_agree_smt .smtItemChkbox:checked").length;

            if (smtItemChkCnt > 0) {
                $(".smtchkbox").prop("checked", true);
            } else {
                $(".smtchkbox").prop("checked", false);
            }
        });

        $(".btn_circle").click(function () {
            var ceo_no = $("#ceo_no1").val() + $("#ceo_no2").val();
            var chrg_no = $("#chrg_no1").val() + $("#chrg_no2").val();
            var chkVal = $("#mktchkbox").val();

            if (mktchkbox.checked == chkterms) {
                chkVal = "fixed";
            }else {
                if (mktchkbox.checked == true) {
                    chkVal = "true";
                }else if (mktchkbox.checked == false) {
                    chkVal = "false";
                }
            }

            if (!isMobile(chrg_no)) { chrg_no = ""; }

            if (!isMobile(ceo_no)) {
                $("#ceoNoError").show();
                return;
            } else {
                $("#ceoNoError").hide();
            }
            //traditional : true 
            var smtChkArr = new Array();
            $(".item_agree_smt .smtItemChkbox:checked").each(function() {
                smtChkArr.push($(this).val());
            });

            $.ajax({
                url: '/sub/my/edit',
                data: { 
                    chrg_nm: $("#chrg_nm").val()
                    , chrg_no: chrg_no
                    , ceo_nm: $("#ceo_nm").val()
                    , ceo_no: ceo_no
                    , MT3TermsId: MT3TermsId
                    , chkVal: chkVal 
                    , SMTTermsId: SMTTermsId
                    , smtChkFlag: smtChkFlag
                    , smtChkArr: smtChkArr
                },
                traditional : true,
                async: false,
                type: 'POST',
                success: function (data) {
                    $(".popup_confirm_edit").show();
                    $(".popup_confirm_edit .close_pop,.popup_confirm_edit .close_btn").click(function () {
                        $(".popup_confirm_edit").hide();
                        location.reload(true);
                    });
                },
                error: function (response) {
                    if (response.responseText == "TermsErrors") {
                        showAlert("마케팅 동의 정보를 확인하여 주시기 바랍니다.");
                    }else {
                        showAlert("회원 정보 항목들을 확인하여 주시기 바랍니다.");
                    }
                }
            });

            // $.post("/sub/my/edit", { 
            //         chrg_nm: $("#chrg_nm").val()
            //         , chrg_no: chrg_no
            //         , ceo_nm: $("#ceo_nm").val()
            //         , ceo_no: ceo_no
            //         , MT3TermsId: MT3TermsId
            //         , chkVal: chkVal 
            //         , SMTTermsId: SMTTermsId
            //         , smtChkFlag: smtChkFlag
            //         , smtChkArr: smtChkArr
            // }
            // , function (data, status) {
            //     $(".popup_confirm_edit").show();
            //     $(".popup_confirm_edit .close_pop,.popup_confirm_edit .close_btn").click(function () {
            //         $(".popup_confirm_edit").hide();
            //         location.reload(true);
            //     });
            // }).fail(function (response) {
            //     if (response.responseText == "TermsErrors") {
            //         showAlert("마케팅 동의 정보를 확인하여 주시기 바랍니다.");
            //     }else {
            //         showAlert("회원 정보 항목들을 확인하여 주시기 바랍니다.");
            //     }
            // });


        });
    });

</script>