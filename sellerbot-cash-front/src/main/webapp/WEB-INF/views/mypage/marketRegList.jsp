<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="/assets/js/select2/select2.min.js"></script>
<script src="/assets/js/mngMall.js?ver=20221117_01"></script>

<style>
    .my_wra {
        padding-top: 5em;
        min-height: auto;
        overflow-x: hidden;
    }

    div#sec_pc,
    div#sec_mo {
        margin-bottom: 5rem;
    }

    div#sec_pc div.row,
    div#sec_mo div.row {
        margin-bottom: 0;
        font-size: 0;
    }

    div#sec_pc div.row h1,
    div#sec_mo div.row h1 {
        margin-bottom: 0;
        font-size: 1.3rem;
        display: inline-block;
        vertical-align: middle;
        margin-top: -3px;
    }

    div#sec_pc div.row .peer_rank,
    div#sec_mo div.row .peer_rank {
        max-width: inherit;
        width: 100%;
        margin: 0 auto;
        border-top: 1px solid #bcbcbc;
        padding-bottom: 2rem;
        font-size: 1rem;
        font-weight: 400;
        color: #888888;
    }

    .marketReg_box {
        margin-top: -2rem;
    }

    .marketReg_box .status {
        text-align: right;
        padding-bottom: 0.5em
    }

    .marketReg_box .status p {
        display: inline-block;
        vertical-align: middle;
        font-size: 0.875em
    }

    .marketReg_box .status p.pass {
        color: #2dabe8 !important
    }

    .marketReg_box .status p.error {
        color: #d00017 !important
    }

    .marketReg_box .status p.warning {
        color: #777777 !important
    }

    .marketReg_box .status p span {
        color: inherit;
        text-decoration: none
    }

    .account_btn p {
        display: inline-block;
        vertical-align: middle;
        line-height: 1.813em;
        font-size: 0.875em;
        color: #888888;
    }

    div#sec_pc div.care_tip img.q_icon,
    div#sec_mo div.care_tip img.q_icon {
        display: inline-block;
        width: 1rem;
        vertical-align: middle;
        margin-left: 0.3rem;
        cursor: pointer;
        height: 1rem;
    }

    div#sec_pc div.care_tip div.q_popup {
        background: #1a1a1a;
        padding: 0.5rem 1rem;
        border-radius: 5px;
        display: none;
        position: absolute;
        margin-left: 1.5rem;
    }
    div#sec_mo div.care_tip div.q_popup {
        background: #1a1a1a;
        padding: 0.25rem 0.25rem;
        border-radius: 5px;
        display: none;
        position: absolute;
        margin-left: -19.5rem;
    }

    div#sec_pc div.care_tip div.q_popup h1,
    div#sec_mo div.care_tip div.q_popup h1 {
        color: #fff;
        font-size: 0.75rem;
        display: inline-block;
        vertical-align: middle;
    }

    div#sec_pc div.care_tip div.q_popup img.close_popup,
    div#sec_mo div.care_tip div.q_popup img.close_popup {
        display: inline-block;
        vertical-align: middle;
        margin-left: 1.5rem;
        cursor: pointer;
    }

    #sec_mo {
        display: none;
    }

    img.trans_naver_pop {
        display: inline-block;
        width: 1.3rem;
        vertical-align: middle;
        margin-left: 0.3rem;
        cursor: pointer;
        height: 1.3rem;
    }

    @media (max-width: 880px) {
        #sec_mo {
            display: block;
            margin: 0 1rem;
        }

        #sec_pc {
            display: none;
        }
    }
</style>
<script>
    $(document).ready(function () {
        $("#menuSelect").on("change", function () {
            if (this.value == "ALL") {
                $(".item2").show();
            } else if (this.value == "NR") {
                $(".item2").show();
                $(".INS").hide();
                $(".ERR").hide();
            } else if (this.value == "INS") {
                $(".item2").hide();
                $(".INS").show();
            } else if (this.value == "ERR") {
                $(".item2").hide();
                $(".ERR").show();
            } else if (this.value == "DGW") {
                console.log();
                $(".item2").show();
                $(".item2").find('a').parent().parent().parent().hide();
            }
        });

        // 쇼핑몰 에러 상태 일때 에러 사유 팝업창 활성화(PC)
        $("div#sec_pc div.care_tip img.q_icon").click(function () {
            // 활성화 된 팝업창 비활성화로 변경
            $(".q_popup").css("display", "none");

            var custMallSeqNo = $(this).data("cust_mall_seq_no");
            $("div#sec_pc div.care_tip div#q_popup_" + custMallSeqNo).css("display", "inline-block");
        });

        // 에러 사유 팝업창 비활성화(PC)
        $("div#sec_pc div.care_tip div.q_popup .close_popup").click(function () {
            $(".q_popup").css("display", "none");
        });
        
        // 쇼핑몰 에러 상태 일때 에러 사유 팝업창 활성화(모바일)
        $("div#sec_mo div.care_tip img.q_icon").click(function () {
            // 활성화 된 팝업창 비활성화로 변경
            $(".q_popup").css("display", "none");

            var custMallSeqNo = $(this).data("cust_mall_seq_no");
            $("div#sec_mo div.care_tip div#q_popup_" + custMallSeqNo).css("display", "inline-block");
        });

        // 에러 사유 팝업창 비활성화(모바일)
        $("div#sec_mo div.care_tip div.q_popup .close_popup").click(function () {
            $(".q_popup").css("display", "none");
        });

        //인증절차 필요 - 네이버페이 팝업 오픈
        $(".trans_naver_pop").click(function(){
            var cust_mall_seq_no = $(this).data("cust_mall_seq_no");

            var params = {
                    cust_mall_seq_no: cust_mall_seq_no
                };

            var url = '/sub/member/get_cust_sub_auth_mail';
            var errorMessage = "처리 실패하였습니다.";

            $.ajax({
                url: url
                , type: 'post'
                , async: true
                , dataType: 'json'
                , contentType: 'application/json'
                , data: JSON.stringify(params)
                , success: function (response) {
                    if (response.result == "OK") {
                        $(".new_popup_wrap").css("display", "flex");
                        $(".pop_up_1").show();
                        $(".display_none_back").show();
                        $("#user_mail").text(response.data);
                    }else {
                        showAlert(errorMessage);
                    }
                }
            });
        });
        
        //인증절차 필요 - SUB 인증 메일 발송
        $(".chg_ticket").click(function(){
            var cust_mall_seq_no = $(".trans_naver_pop").data("cust_mall_seq_no");

            var params = {
                    cust_mall_seq_no: cust_mall_seq_no
                };

            var url = '/sub/member/send_sub_auth_mail';
            var errorMessage = "처리 실패하였습니다.";

            $.ajax({
                url: url
                , type: 'post'
                , async: true
                , dataType: 'json'
                , contentType: 'application/json'
                , data: JSON.stringify(params)
                , success: function (response) {
                    $(".new_popup_wrap").css("display", "none");
                    
                    if (response.result == "OK") {
                        showAlert("업데이트 요청 완료<br>(나중에 다시 확인해주세요~)", function () {
                            location.reload();
                        });
                    } else {
                        showAlert(errorMessage);
                    }
                }
            });
        });

        //인증절차 필요 - 네이버페이 팝업 닫기
        $(".add_pooup_close, .close").click(function () {
            $(".new_popup_wrap").css("display", "none");
        });
    });
</script>

<div class="container">
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
                    <c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
                        <c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
                            <c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">    
                                <c:choose>
                                    <c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
                                        <li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">판매몰 관리</p>
            <p class="desc">등록된 판매몰을 수정, 삭제 할 수 있습니다.</p>
        </div>

        
        <!--계정 입력 팝업-->
        <div class="popup_account type1">
            <div class="pop_account">
                <div class="pop_head">
                    <p>계정입력</p>
                    <p class="popup_gui_btn">?</p>
                    <div class="popup_gui_container">
                        <span id="helpInfoText">
                            업체번호와 공급계약번호는
                            인터파크 판매자 매니저의 좌측메뉴
                            판매자 정보 관리의 관리 계정(ID)
                            관리에서 확인할 수 있습니다.
                        </span>
                        <img class="popup_gui_close" src="/assets/images/member/close.png" alt="" />
                    </div>
                    <a href="javascript:type1Close();" class="close_pop"><img src="/assets/images/member/close.png" alt=""></a>
                </div>
                <div class="pop_body">
                    <p class="edit_video_btn2"><span>영상가이드</span></p>
                    <style>
                        .edit_video_btn2 {
                            text-align: right;
                            position: absolute;
                            right: 0.5rem;
                            top: 4.5rem;
                            z-index: 6;
                        }

                        .edit_video_btn2 span {
                        font-size: 1rem;
                        color: #fff;
                        background-color: #009aca;
                        display: block;
                        width: 106px;
                        text-align: center;
                        padding: 0.3rem 0;
                        border-radius: 3rem;
                        float: right;
                        cursor: pointer;

                        }
                        div.edit_video_popup {
                            width: 100%;
                            height: 100%;
                            position: fixed;
                            top: 0;
                            left: 0;
                            display: flex;
                            align-items: center;
                            justify-content: center;
                            display: none;
                            z-index: 9999;
                        }

                        div.edit_video_popup div.edit_video_popup_container {
                            position: relative;
                            z-index: 6;
                            max-width: 50rem;
                            width: 90%;
                            height: 30rem;
                        }

                        .background_black {
                            width: 100%;
                            height: 100%;
                            position: absolute;
                            left: 0;
                            top: 0;
                            background: #000;
                            opacity: 0.5;
                            z-index: 5;
                        }

                        div.edit_video_popup div.edit_video_popup_container p img {
                            float: right;
                            margin-bottom: 0.5rem;
                            cursor: pointer;
                        }

                        .naver_text {
                            font-size: 22px
                        }
                    </style>
                    <div class="logo_area_account">
                        <img src="/assets/images/member/logo/logo011.png" alt="">
                    </div>
                    <form class="frm_account" name="frmAccount" onsubmit="return false;">
                        <div class="input_account" id="div_mall_cert_typ_cd" style="display: none;">
                            <p class="lb">몰 인증 유형</p>
                            <select class="sctbox" id="i_mall_cert_typ_cd" name="mall_cert_typ_cd">
                            </select>
                        </div>
                        <div class="input_account">
                            <p class="lb">ID</p>
                            <div class="input_box">
                                <input type="text" class="textbox_mb" name="mall_cert_1st_id">
                                <p id="mall_cert_1st_id_error" class="error">*아이디를 입력해주세요.</p>
                            </div>
                        </div>
                        <div class="input_account">
                            <p class="lb">PW</p>
                            <div class="input_box">
                                <input type="password" class="textbox_mb" name="mall_cert_1st_passwd">
                                <p id="mall_cert_1st_passwd_error" class="error">*비밀번호를 입력해주세요.</p>
                            </div>
                        </div>
                        <div class="input_account" style="display: none;" id="div_sub_mall_cert_1st_nm">
                            <p class="lb" id="t_sub_mall_cert_1st_nm"></p>
                            <div class="input_box">
                                <input type="text" class="textbox_mb" name="sub_mall_cert_1st"
                                    id="i_sub_mall_cert_1st_nm">
                                <p id="sub_mall_cert_1st_error" class="error"></p>
                            </div>
                        </div>
                        <div class="input_account" style="display: none;" id="div_sub_mall_cert_2nd_nm">
                            <div class="input_box">
                                <p class="lb" id="t_sub_mall_cert_2nd_nm"></p>
                                <input type="text" class="textbox_mb" name="sub_mall_cert_2nd"
                                    id="i_sub_mall_cert_2nd_nm">
                                <input type="hidden" class="textbox_mb" name="cert_step" id="cert_step">
                                <input type="hidden" class="textbox_mb" name="mall_cd" id="mall_cd">
                                <input type="hidden" class="textbox_mb" name="mall_nm" id="mall_nm">
                                <input type="hidden" class="textbox_mb" name="passwd_step" id="passwd_step">
                                <input type="hidden" class="textbox_mb" name="sub_mall_auth_1st_esse_yn"
                                    id="sub_mall_auth_1st_esse_yn">
                                <input type="hidden" class="textbox_mb" name="sub_mall_auth_2nd_esse_yn"
                                    id="sub_mall_auth_2nd_esse_yn">
                                <input type="hidden" id="btn_type">
                                <input type="hidden" id="cust_mall_seq_no" name="cust_mall_seq_no" />
                                <input type="hidden" id="submitType" />
                                <input type="hidden" name="goods_cate_seq_no" id="goods_cate_seq_no">
                                <p id="t_sub_mall_cert_2nd_nm_error" class="error"></p>
                            </div>
                        </div>
                        <div class="input_account" style="display: none;" id="div_mall_cert_2nd_passwd_nm">
                            <p class="lb" id="t_mall_cert_2nd_passwd_nm"></p>
                            <div class="input_box">
                                <input type="password" class="textbox_mb" name="mall_cert_2nd_passwd">
                                <p id="mall_cert_2nd_passwd_error" class="error"></p>
                            </div>
                        </div>

                        <!-- QR코드 인증 -->
                        <div class="input_account" style="display: none;" id="div_mall_auth_otp_key">
                            <p class="lb" id="t_mall_auth_otp_key">OTP인증</p>
                            <div class="input_otp file">
                                <input type="password" class="textbox_otp_file" name="mall_auth_otp_key">
                                <label for="fileUpload">QR등록</label>
                                <input type="file" id="fileUpload" name="qr_code_file" class="hidden_file">
                                <div class="ask_sub_text_area error" id="qr_code_file_err" style="display: none;">
                                    <h1 class="ask_sub_text_area_text">
                                        파일 크기는 10MB 이하의 파일만 업로드 가능합니다.<br />
                                        파일 유형은 JPG,GIF,BMP,TIF,PNG,PDF 파일만 업로드 가능합니다.
                                    </h1>
                                </div>
                                <input type="hidden" class="textbox_mb" name="otp_auth_use_yn" id="otp_auth_use_yn">
                                <input type="hidden" class="textbox_mb" name="otp_auth_esse_yn" id="otp_auth_esse_yn">
                                <input type="hidden" class="textbox_mb" name="otp_auth_typ_cd" id="otp_auth_typ_cd">
                                <input type="hidden" class="textbox_mb" name="otp_auth_typ_cd_nm" id="otp_auth_typ_cd_nm">
                                <p id="mall_auth_otp_key_error" class="error"></p>
                            </div>
                        </div>
                        
                        <!-- 카테고리 추가 -->
                        <div class="input_account" id="div_goods_cate_seq_no">
                            <p class="lb">카테고리</p>
                            <div class="input_box">
                                <select class="sctbox" id="goods_cate_seq_no" name="goods_cate_seq_no">
                                    <option value="">선택</option>
                                    <c:forEach items="${categoryList }" var="category">
                                        <option value="${category.goods_cate_seq_no }">${category.cate_nm }</option>
                                    </c:forEach>
                                </select>
                                <p id="goods_cate_seq_no_error" class="error"></p>
                            </div>
                        </div>

                        <a href="#" class="add_account"></a>
                    </form>
                    <p class="mall_error_msg">*아이디와 비밀번호를 다시 확인해주세요.</p>
                </div>
                <div class="pop_foot">
                    <button type="button" class="frm_account_btn">입력완료</button>
                </div>
            </div>
        </div>

        <div style="display: none;">
            <c:set var="cnt" value="1" />
            <c:forEach items="${mallList}" var="mall" varStatus="status">
                <li name="mall_${cnt }" id="mallList${mall.mall_cd}">
                    <input type="hidden" value="${mall.sub_mall_cert_1st_nm }" class="sub_mall_cert_1st_nm" />
                    <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }" class="sub_mall_cert_2nd_nm" />
                    <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }" class="mall_cert_2nd_passwd_nm" />
                    <input type="hidden" value="${mall.sub_mall_cert_step }" class="sub_mall_cert_step" />
                    <input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
                    <input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
                    <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }" class="sub_mall_auth_1st_esse_yn" />
                    <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }" class="sub_mall_auth_2nd_esse_yn" />
                    <input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
                    <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                    <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                    <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                    <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                    <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                    <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                    <input type="hidden" value="${mall.help_info }" class="help_info" />
                    <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                    <input type="hidden" value="${mall.video_url }" class="video_url" />
                <c:forEach items="${mall.mall_cert_typ}" var="mallTyp" varStatus="typStatus">
                    <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }" value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
                </c:forEach>
                </li>
                <c:set var="cnt" value="${cnt + 1 }" />
            </c:forEach>
        </div>

        <div style="display: none;">
        <c:if test="${not empty loanBatchProTgtInfo.batchProTgt_list }">
            <c:set var="batPrcsTgtSeqNo" value="${loanBatchProTgtInfo.batchProTgt_list[0].bat_prcs_tgt_seq_no }" />
        </c:if>
        <c:forEach items="${loanBatchProTgtInfo.loanCustMall_list}" var="loanInfo" varStatus="status">
            <input type="checkbox" name="loan_chk" class="loan_cust_mall"
                    data-cust-seq-no="${loanInfo.cust_seq_no}"
                    data-cust-mall-seq-no="${loanInfo.cust_mall_seq_no}" 
                    data-mall-cd="${loanInfo.mall_cd}"
                    data-bat-prcs-tgt-seq-no="${batPrcsTgtSeqNo}"
                    data-loan-sts-cd="${loanInfo.loan_sts_cd}"
                    data-loan-svc-descp="${loanInfo.svc_descp}"
                    <c:if test="${not empty loanInfo.check_batch_pro_tge}"> checked disabled </c:if> 
            >
        </c:forEach>
        </div>
    </div>

    <!-- container_wrapper start -->
    <div class="container_wrapper">
        <!-- tree_menu_ul start -->
        <!-- <ul id="tree_menu_ul"> -->
        <ul id="two_menu_ul" style="margin-top: 2rem; margin-bottom: 2rem;">
            <li class="blue_active2" onclick="location.href='/sub/my/marketRegList'">
                <a>판매몰 현황</a>
            </li>
            <li onclick="location.href='/sub/my/market'">
                <a>판매몰 등록/수정/삭제</a>
            </li>
        </ul>
        <!-- tree_menu_ul end -->
        <!-- sec_pc start -->
        <div id="sec_pc">
            <div class="row">
                <h1 style="margin-bottom:1rem" class="sec_pc_title">판매몰 현황</h1>
            </div>
            <div class="row">
                <h1 style="margin-bottom:1rem" class="peer_rank">현재 셀러봇캐시에 등록한 판매몰 정보입니다.</h1>
            </div>
            <div id="care_tip" class="care_tip" style="max-width: inherit;">
                <div class="marketReg_box">
                    <div class="status">
                        <p style="float: left;">
                            <select id="menuSelect" style="width: 170px !important;">
                                <option value="ALL">전체</option>
                                <option value="NR">정상</option>
                                <option value="INS">점검</option>
                                <option value="ERR">오류</option>
                                <option value="DGW">정보제공현황</option>
                            </select>
                        </p>
                        <p class="all">전체
                            <span class="all_num">${custMall.all_mall_cnt-custMall.del_mall_cnt - custMall.mall008.dup_mall_cnt }</span>
                        </p> /
                        <p class="pass">정상
                            <span>${custMall.nor_mall_cnt }</span>
                        </p> /
                        <p class="warning">점검
                            <span>${custMall.ins_mall_cnt }</span>
                        </p> /
                        <p class="error">오류
                            <span>${custMall.err_mall_cnt}</span>
                        </p>
                    </div>
                </div>
                <section id="tip_list" class="care_tip_list fl_wrap w100">
                    <ul>
                        <li class="fl_wrap item1" style="background-color: #f5f2f2; font-size: 16px;">
                            <dl class="th_line">
                                <dt style="width: 28%;">판매몰명</dt>
                                <dt style="width: 17%;">아이디</dt>
                                <dt style="width: 10%;">등록일</dt>
                                <dt style="width: 14%;">상태</dt>
                                <dt style="width: 20%;">정보제공현황</dt>
                                <dt style="width: 10%;">관리</dt>
                            </dl>
                        </li>
                        <c:choose>
                            <c:when test="${fn:length(custMall.mall) == 0}">
                                <li class="fl_wrap">
                                    <dl class="text_center">
                                        판매몰 등록현황 정보가 없습니다.
                                    </dl>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                                    <c:if test="${mall.del_yn ne 'Y'}">
                                        <li class="fl_wrap item2 ${mall.cust_mall_sts_cd }" id="${mall.cust_mall_sts_cd }" style="font-size: 14px;">
                                            <dl>
                                                <dd class="text_left pt10 pl05 pb10" style="width: 28%; display: flex; align-items: center;">
                                                    <c:if test="${mall.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.stor_path}${mall.stor_file_nm}" style="width: 5.750em; height: 2em; display: inline-flex; margin-right: 3%;" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" style="width: 5.750em; height: 2em; display: inline-flex; margin-right: 3%;" alt="몰 로고">
                                                    </c:if>
                                                    ${mall.mall_cd_nm }
                                                </dd>
                                                <fmt:parseDate value="${mall.mod_ts}" pattern="yyyy-MM-dd'T'HH:mm" var="mod_ts" type="both"></fmt:parseDate>
                                                <dd class="text_center pt10 pb10" style="width: 17%;">${mall.mall_cert_1st_id }</dd>
                                                <dd class="text_center pt10 pb10" style="width: 10%;"><fmt:formatDate value="${mod_ts}" pattern="yyyy.MM.dd" /></dd>
                                                <dd class="text_left pt10 pl40 pb10" style="width: 14%;">
                                                    <c:choose>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'NR'}">
                                                            <font style="color: #2dabe8;">정상</font>
                                                        </c:when>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'ERR'}">
                                                            <c:choose>
                                                                <c:when test="${mall.scra_err_cd eq 'E013' and (mall.mall_cd eq '046' or mall.mall_cd eq '047')}">
                                                                    <a class="trans_naver_pop" style="cursor:pointer; text-decoration:underline;" data-cust_mall_seq_no="${mall.cust_mall_seq_no}">
                                                                        <font style="color: #ff0000;">인증절차 필요(클릭)</font>
                                                                    </a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <font style="color: #ff0000;">오류</font>
                                                                    <img class="q_icon" src="/assets/images/icon/q.png" data-cust_mall_seq_no="${mall.cust_mall_seq_no}" alt="">
                                                                    <div class="q_popup" id="q_popup_${mall.cust_mall_seq_no}">
                                                                        <h1>${mall.scra_err_cd_desc}</h1>
                                                                        <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                                                                    </div>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:when>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'INS'}">
                                                            <font style="color: #777777;">점검</font>
                                                        </c:when>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'REG'}">
                                                            등록
                                                        </c:when>
                                                        <c:when test="${mall.cust_mall_sts_cd eq 'MOD'}">
                                                            수정
                                                        </c:when>
                                                    </c:choose>
                                                </dd>
                                                <dd class="text_center pt10 pb10" style="width: 20%;">
                                                    <c:set var="loop_flag" value="false" />
                                                    <c:set var="loanUseNm" value="추천금융상품보기" />
                                                    <c:forEach items="${loanCustUseMallList}" var="loanCustUseInfo" varStatus="status">
                                                        <c:if test="${not loop_flag }">
                                                            <c:if test="${mall.cust_mall_seq_no eq loanCustUseInfo.cust_mall_seq_no }">
                                                                <c:if test="${!empty loanCustUseInfo.svc_id}">
                                                                    <c:set var="loop_flag" value="true" />
                                                                    <c:set var="loanUseNm" value="${loanCustUseInfo.svc_nm}" />
                                                                </c:if>
                                                            </c:if>
                                                        </c:if>
                                                    </c:forEach>
                                                    <c:choose>
                                                        <c:when test="${loop_flag }">
                                                            <font style="color: #000000;">${loanUseNm}</font>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="/sub/pre_calculate/pre_calculate" style="color: #2DABE8;text-decoration: underline;">${loanUseNm}</a>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </dd>
                                                <dd class="text_center account_btn pt10 pb10" style="width: 10%;">
                                                    <p class="btn_edit" style="cursor: pointer;" onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )">
                                                        <img src="/assets/images/member/icon_2.png" alt="">
                                                    </p>
                                                    <p class="btn_remove" style="cursor: pointer;" data-cust_mall_seq_no='${mall.cust_mall_seq_no}' onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )">
                                                        <img src="/assets/images/member/icon_1.png" alt="">
                                                    </p>
                                                </dd>
                                            </dl>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </section>
            </div>
        </div>
        <!-- sec_pc end -->

        <!-- sec_mo start -->
        <div class="m_sevice_mb" id="sec_mo">
            <div class="row">
                <h1 style="margin-bottom:1rem" class="sec_mo_title">판매몰 현황</h1>
            </div>
            <div class="row">
                <h1 style="margin-bottom:1rem" class="peer_rank">현재 셀러봇캐시에 등록한 판매몰 정보입니다.</h1>
            </div>
            <div id="care_tip" class="care_tip" style="max-width: inherit;">
                <div class="marketReg_box">
                    <div class="status pb10">
                        <p class="pl10" style="float: left;">
                            <select id="menuSelect" style="width: 170px !important;">
                                <option value="ALL">전체</option>
                                <option value="NR">정상</option>
                                <option value="INS">점검</option>
                                <option value="ERR">오류</option>
                                <option value="DGW">정보제공현황</option>
                            </select>
                        </p>
                        <p class="all">전체
                            <span class="all_num">${custMall.all_mall_cnt-custMall.del_mall_cnt - custMall.mall008.dup_mall_cnt }</span>
                        </p> /
                        <p class="pass">정상
                            <span>${custMall.nor_mall_cnt }</span>
                        </p> /
                        <p class="warning">점검
                            <span>${custMall.ins_mall_cnt }</span>
                        </p> /
                        <p class="error">오류
                            <span>${custMall.err_mall_cnt}</span>
                        </p>
                    </div>
                </div>

            <c:choose>
                <c:when test="${fn:length(custMall.mall) == 0}">
                    <ul class="mb_table_service ticket_info">
                        <li class="gray_bg2 mb_header" style="align-items: center; display: flex;">
                            판매몰 등록현황 정보가 없습니다.
                        </li>
                    </ul>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${custMall.mall}" var="mall" varStatus="status">
                        <c:if test="${mall.del_yn ne 'Y'}">
                            <ul class="mb_table_service ticket_info">
                                <li class="gray_bg2 mb_header" style="align-items: center; display: flex;">
                                    <h1>판매몰명</h1>
                                    <h2 style="display: flex; align-items: center; justify-content: flex-end; width: 100%;">
                                        <c:if test="${mall.stor_file_nm != null }">
                                            <img src="/imagefile/${mall.stor_path}${mall.stor_file_nm}" style="width: 5.750em; height: 2em; display: inline-flex; margin-right: 3%;" alt="몰 로고">
                                        </c:if>
                                        <c:if test="${mall.stor_file_nm == null }">
                                            <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" style="width: 5.750em; height: 2em; display: inline-flex; margin-right: 3%;" alt="몰 로고">
                                        </c:if>
                                        ${mall.mall_cd_nm }
                                    </h2>
                                </li>
                                <li>
                                    <h1>아이디</h1>
                                    <h2>${mall.mall_cert_1st_id }</h2>
                                </li>
                                <li>
                                    <h1>등록일</h1>
                                    <h2>
                                        <fmt:parseDate value="${mall.mod_ts}" pattern="yyyy-MM-dd'T'HH:mm" var="mod_ts" type="both"></fmt:parseDate>
                                        <fmt:formatDate value="${mod_ts}" pattern="yyyy.MM.dd" />
                                    </h2>
                                </li>
                                <li>
                                    <h1>상태</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${mall.cust_mall_sts_cd eq 'NR'}">
                                                <font style="color: #2dabe8;">정상</font>
                                            </c:when>
                                            <c:when test="${mall.cust_mall_sts_cd eq 'ERR'}">
                                                <font style="color: #ff0000;">오류</font>
                                                <img class="q_icon" src="/assets/images/icon/q.png" data-cust_mall_seq_no="${mall.cust_mall_seq_no}" alt="">
                                                <div class="q_popup" id="q_popup_${mall.cust_mall_seq_no}">
                                                    <h1>${mall.scra_err_cd_desc}</h1>
                                                    <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                                                </div>
                                            </c:when>
                                            <c:when test="${mall.cust_mall_sts_cd eq 'INS'}">
                                                <font style="color: #777777;">점검</font>
                                            </c:when>
                                            <c:when test="${mall.cust_mall_sts_cd eq 'REG'}">
                                                등록
                                            </c:when>
                                            <c:when test="${mall.cust_mall_sts_cd eq 'MOD'}">
                                                수정
                                            </c:when>
                                            <c:otherwise>
                                                <a class="trans_naver_pop" style="cursor:pointer" data-cust_mall_seq_no="${mall.cust_mall_seq_no}">
                                                    <font style="color: #ff0000;">인증절차 필요(클릭)</font>
                                                </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </li>
                                <li>
                                    <h1>정보제공현황</h1>
                                    <h2>
                                        <c:set var="loop_flag" value="false" />
                                        <c:set var="loanUseNm" value="추천금융상품보기" />
                                        <c:forEach items="${loanCustUseMallList}" var="loanCustUseInfo" varStatus="status">
                                            <c:if test="${not loop_flag }">
                                                <c:if test="${mall.cust_mall_seq_no eq loanCustUseInfo.cust_mall_seq_no }">
                                                    <c:if test="${!empty loanCustUseInfo.svc_id}">
                                                        <c:set var="loop_flag" value="true" />
                                                        <c:set var="loanUseNm" value="${loanCustUseInfo.svc_nm}" />
                                                    </c:if>
                                                </c:if>
                                            </c:if>
                                        </c:forEach>
                                        <c:choose>
                                            <c:when test="${loop_flag }">
                                                <font style="color: #000000;">${loanUseNm}</font>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="/sub/pre_calculate/pre_calculate" style="color: #2DABE8;text-decoration: underline;">${loanUseNm}</a>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </li>
                                <li>
                                    <h1>관리</h1>
                                    <h2 class="account_btn">
                                        <p class="btn_edit" onclick="fn_updateData('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )">
                                            <img src="/assets/images/member/icon_2.png" alt="">
                                        </p>
                                        <p class="btn_remove" data-cust_mall_seq_no='${mall.cust_mall_seq_no}' onclick="fn_remove('${mall.cust_mall_seq_no}','${mall.mall_cd }','${mall.scb_loan_cnt }' )">
                                            <img src="/assets/images/member/icon_1.png" alt="">
                                        </p>
                                    </h2>
                                </li>
                            </ul>
                        </c:if>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
            </div>
        </div>
        <!-- sec_mo end -->

    </div>
    <!-- container_wrapper end -->
</div>

<div class="new_popup_wrap">
    <div class="display_none_back"></div>
    <div class="pop_up_1 pop_up_all">
        <div class="popup_top_title">
            <h1>인증절차 필요 - 네이버페이</h1>
            <div class="add_pooup_close">
                <div></div>
                <div></div>
            </div>
        </div>
        <h1 class="popup_title_2">네이버페이 업데이트를 위해선,</br>
            아래의 이메일 인증 절차가 필요해요~</br>
            <a style="color: #2DABE8;">인증 이메일 : <span id="user_mail"></span></a>
        </h1>
        <div class="pop_up_text_wrap" style="height:15rem;">
            <span class="naver_text">
                <p>&lt;인증 절차&gt;</p>
                <p>1. 아래 업데이트 클릭</p>
                <p>2. <a style="color: #2DABE8;">인증 이메일</a>로 로그인 후 받은 메일함 확인</p>
                <p>3. 네이버페이에서 발송된 인증용 메일 클릭</p>
                <p>4. ‘인증하기’ 클릭</p>
                <p>(업데이트 클릭 후 3분 안에 메일 ‘인증하기‘ 해주셔야 해요~)</p>
            </span>
        </div>
        <div class="button_row">
            <button type="button" class="close">닫기</button>
            <button type="button" class="chg_ticket">업데이트</button>
        </div>
    </div>
</div>


<div class="edit_video_popup">
    <div class="edit_video_popup_container">
      <p><img src="/assets/images/member/close.png" alt=""></p>
      <iframe id="iframeVideoPopup" width="100%" height="100%" src="https://www.youtube.com/embed/gr1TWqaR3Rs?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="background_black"></div>
</div>
<input type="hidden" id="custMallSeqNo" value="${cust_mall_seq_no }">
<script>
    

    //20200519 "?"버튼 추가 스크립트
    document.querySelectorAll(".popup_gui_close")[0].addEventListener("click",function(){
        document.querySelectorAll(".popup_gui_container")[0].style.display = "none";
    });
    document.querySelectorAll(".popup_gui_btn")[0].addEventListener("click",function(){
        document.querySelectorAll(".popup_gui_container")[0].style.display = "block";
    });
    //영상 팝업 스크립트
    document.querySelectorAll(".edit_video_btn2")[0].addEventListener("click",function(){
        document.querySelectorAll(".edit_video_popup")[0].style.display = "flex";
    });
    document.querySelectorAll(".edit_video_popup_container p")[0].addEventListener("click",function(){
        document.querySelectorAll(".edit_video_popup")[0].style.display = "none";
        document.querySelectorAll(".edit_video_popup iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });
</script>