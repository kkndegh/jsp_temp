<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />


<% pageContext.setAttribute("newLineChar", "\n"); %>

<div class="container">
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">1:1 문의</p>
            <p class="desc">무엇을 도와드릴까요? 셀러봇캐시에게 질문했던 1:1 문의내용도 기억하고 있습니다.</p>
            <form name="reqForm" action="/sub/my/ask" method="get">
                <input name="page" type="hidden" value="${paging.pageNo}">
            </form>
        </div>
        <div class="cs_area">
            <div class="ask_section">
                <div class="ask_list">
                    <div class="th_item_ask clearfix">
                        <div class="item_col">
                            <p>No.</p>
                        </div>
                        <div class="item_col m_diplaynone">
                            <p>유형</p>
                        </div>
                        <div class="item_col">
                            <p>제목</p>
                        </div>
                        <div class="item_col">
                            <p>상태</p>
                        </div>
                        <div class="item_col m_diplaynone">
                            <p>작성일</p>
                        </div>
                    </div>
                    <c:if test="${reqList.totalCount == 0}">
                        <div class="no_data">
                            <p>문의가 없습니다.</p>
                        </div>
                    </c:if>
                    <c:forEach items="${reqList.dataList}" var="list" varStatus="status">
                        <div class="td_item_ask">
                            <div class="title_ask clearfix">
                                <div class="item_col">
                                    <p class="txt_td">${ (paging.pageNo - 1) * paging.pageSize + status.index + 1 }</p>
                                </div>
                                <div class="item_col m_diplaynone">
                                    <p class="txt_td">${list.adv_typ_cd_nm }</p>
                                </div>
                                <div class="item_col">
                                    <p class="txt_td">${list.adv_title }</p>
                                </div>
                                <div class="item_col">
                                    <p class="status end">${list.adv_prgs_sts_cd_nm }</p>
                                </div>
                                <div class="item_col m_diplaynone">
                                    <p class="etc_td">
                                        <fmt:parseDate value="${list.adv_req_tm}" pattern="yyyyMMddHHmmss"
                                            var="adv_req_tm" />
                                        <fmt:formatDate value="${adv_req_tm}" pattern="yyyy-MM-dd HH:mm:ss" />
                                    </p>
                                </div>
                            </div>
                            <div class="detail_ask">
                                <p class="dt_ask_txt">
                                    ${fn:replace(list.adv_cont, newLineChar, "<br />")}
                                </p>
                                <c:if test="${not empty list.adv_req_file }">
                                    <c:if test="${fn:indexOf(list.adv_req_file.stor_file_nm, '.pdf') != -1}">
                                        <p class="dt_ask_img">
                                            <a
                                                href="/imagefile/${list.adv_req_file.stor_path}${list.adv_req_file.stor_file_nm}">${list.adv_req_file.org_file_nm}</a>
                                        </p>
                                    </c:if>
                                    <c:if test="${fn:indexOf(list.adv_req_file.stor_file_nm, '.pdf') == -1}">
                                        <p class="dt_ask_img">
                                            <img src="/imagefile/${list.adv_req_file.stor_path}${list.adv_req_file.stor_file_nm}"
                                                alt="이미지" style="width: 100%">
                                        </p>
                                    </c:if>
                                </c:if>
                                <p class="m_date">${sf:formatLocalDateTime(list.reg_ts, 'yyyy-MM-dd HH:mm:ss')}</p>
                                <div class="my_edit_btn">
                                    <a href="/sub/my/ask_edit?adv_req_seq_no=${list.adv_req_seq_no}">
                                        <h1>수정</h1>
                                    </a>
                                </div>
                                <!--                   2018-01-12 16:00:03 -->
                            </div>
                            <c:if test="${not empty list.asw_cont }">
                                <div class="answer_ask clearfix">
                                    <div class="item_col m_diplaynone">
                                        <p class="no">${list.asw_id }</p>
                                    </div>
                                    <div class="item_col">
                                        <p class="answer_txt">
                                            <span class="lb_re">Re.</span>
                                            <br>${list.asw_cont }
                                        </p>
                                        <c:if test="${not empty list.aws_file }">
                                            <p class="dt_ask_img">
                                                <img src="/imagefile/${list.aws_file.stor_path}${list.aws_file.stor_file_nm}"
                                                    alt="이미지" style="width: 100%">
                                            </p>
                                        </c:if>
                                    </div>
                                    <div class="item_col m_re_date">
                                        <p class="date">${dateUtilBean.makeNonFormatToFormat(list.asw_reg_tm)
                                            }<span></span></p>
                                    </div>
                                </div>
                            </c:if>
                        </div>
                    </c:forEach>
                    <div class="pagination">
                        <div class="register_ask">
                            <!-- <a href="/sub/my/register_ask">등록</a> -->
                            <a href="https://9p9j3.channel.io/lounge" target='_blank'>채널톡 문의</a>
                        </div>
                        <jsp:include page="/WEB-INF/jsputils/pagination.jsp">
                            <jsp:param name="firstPageNo" value="${paging.firstPageNo}" />
                            <jsp:param name="prevPageNo" value="${paging.prevPageNo}" />
                            <jsp:param name="startPageNo" value="${paging.startPageNo}" />
                            <jsp:param name="pageNo" value="${paging.pageNo}" />
                            <jsp:param name="endPageNo" value="${paging.endPageNo}" />
                            <jsp:param name="nextPageNo" value="${paging.nextPageNo}" />
                            <jsp:param name="finalPageNo" value="${paging.finalPageNo}" />
                        </jsp:include>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".text_n").click(function () {
        var notice = $(this).parents(".title_notice");
        if (notice.hasClass("active") == true) {
            notice.removeClass("active");
        } else {
            $(".title_notice").removeClass("active");
            notice.addClass("active");
        }
        $(".close_btn_notice").click(function () {
            $(this).parents(".title_notice").removeClass("active");
        });
    });

    $(".title_ask").click(function () {
        var prTd = $(this).parents(".td_item_ask");
        if (prTd.hasClass("open") == true) {
            prTd.removeClass("open");
        } else {
            $(".td_item_ask").removeClass("open");
            prTd.addClass("open");
        }
    });

    function goPage(p) {
        if (document.reqForm.page.value != p) {
            document.reqForm.page.value = p;
            document.reqForm.submit();
        }
    };
</script>