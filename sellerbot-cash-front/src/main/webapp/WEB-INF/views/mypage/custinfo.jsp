<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="custom" value="${sessionScope.cust}" />
<div class="row">
    <table class="table table-striped">
        <tbody>
            <tr>
                <th scope="row">cust_id</th>
                <td>${custom.cust_id }</td>
            </tr>
            <tr>
                <th scope="row">biz_no</th>
                <td>${custom.biz_no}</td>
            </tr>
            <tr>
                <th scope="row">biz_nm</th>
                <td>${custom.biz_nm}</td>
            </tr>
            <tr>
                <th scope="row">ceo_nm</th>
                <td>${custom.ceo_nm}</td>
            </tr>
            <tr>
                <th scope="row">ceo_no</th>
                <td>${custom.ceo_no}</td>
            </tr>
            <tr>
                <th scope="row">chrg_nm</th>
                <td>${custom.chrg_nm}</td>
            </tr>
            <tr>
                <th scope="row">chrg_no</th>
                <td>${custom.chrg_no}</td>
            </tr>
            <tr>
                <th scope="row">inf_path_cd</th>
                <td>${custom.inf_path_cd}</td>
            </tr>
            <tr>
                <th scope="row">passwd_err_cnt</th>
                <td>${custom.passwd_err_cnt}</td>
            </tr>
        </tbody>
    </table>
</div>