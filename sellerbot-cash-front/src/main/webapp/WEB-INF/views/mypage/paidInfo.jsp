<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />

<link rel="stylesheet" href="/assets/css/ticket.css">

<input type="hidden" name="successYN" id="successYN" value="${successYN}">
<input type="hidden" name="errMsg" id="errMsg" value="${errMsg}">
<input type="hidden" id="DeviceType" value="${DeviceType}">
<input type="hidden" id="isRegMall" value="${isRegMall}">
<input type="hidden" id="isRegAcct" value="${isRegAcct}">
<input type="hidden" id="basic_goods_req_seq_no" value="${paidInfo.using_goods_info.basic_goods_req_seq_no}">
<input type="hidden" id="basic_goods_seq_no" value="${paidInfo.using_goods_info.basic_goods_seq_no}">
<input type="hidden" id="basic_goods_opt_seq_no" value="${paidInfo.using_goods_info.basic_goods_opt_seq_no}">
<input type="hidden" id="basic_goods_nm" value="${paidInfo.using_goods_info.basic_goods_nm}">
<input type="hidden" id="basic_goods_typ_cd" value="${paidInfo.using_goods_info.basic_goods_typ_cd}">
<input type="hidden" id="basic_fna_pay_prc" value="${paidInfo.using_goods_info.basic_fna_pay_prc}">
<input type="hidden" id="addn_goods_req_seq_no" value="${paidInfo.using_goods_info.addn_goods_req_seq_no}">
<input type="hidden" id="addn_goods_seq_no" value="${paidInfo.using_goods_info.addn_goods_seq_no}">
<input type="hidden" id="addn_goods_opt_seq_no" value="${paidInfo.using_goods_info.addn_goods_opt_seq_no}">
<input type="hidden" id="addn_goods_nm" value="${paidInfo.using_goods_info.addn_goods_nm}">
<input type="hidden" id="addn_fna_pay_prc" value="${paidInfo.using_goods_info.addn_fna_pay_prc}">
<input type="hidden" id="addn_period" value="${paidInfo.using_goods_info.addn_period}">
<input type="hidden" id="addn_cancle_product_yn" value="${paidInfo.using_goods_info.addn_cancle_product_yn}">

<input type="hidden" name="rnbFnaPrc" id="rnbFnaPrc" value="${RNB.FNA_PRC}">
<input type="hidden" name="pybFnaPrc" id="pybFnaPrc" value="${PYB.FNA_PRC}">
<input type="hidden" name="megaFnaPrc" id="megaFnaPrc" value="${PASB.FNA_PRC}">
<input type="hidden" name="gigaFnaPrc" id="gigaFnaPrc" value="${PASP.FNA_PRC}">
<input type="hidden" name="teraFnaPrc" id="teraFnaPrc" value="${PAFP.FNA_PRC}">
<input type="hidden" name="usingGoodsFnaPrc" id="usingGoodsFnaPrc" value="${paidInfo.using_goods_info.basic_fna_prc}">

<input type="hidden" name="rnbGoodsTypeCd" id="rnbGoodsTypeCd" value="${RNB.GOODS_TYP_CD}">
<input type="hidden" name="pybGoodsTypeCd" id="pybGoodsTypeCd" value="${PYB.GOODS_TYP_CD}">
<input type="hidden" name="megaGoodsTypeCd" id="megaGoodsTypeCd" value="${PASB.GOODS_TYP_CD}">
<input type="hidden" name="gigaGoodsTypeCd" id="gigaGoodsTypeCd" value="${PASP.GOODS_TYP_CD}">
<input type="hidden" name="teraGoodsTypeCd" id="teraGoodsTypeCd" value="${PAFP.GOODS_TYP_CD}">

<input type="hidden" name="rnbGoodsSeqNo" id="rnbGoodsSeqNo" value="${RNB.GOODS_SEQ_NO}">
<input type="hidden" name="pybGoodsSeqNo" id="pybGoodsSeqNo" value="${PYB.GOODS_SEQ_NO}">
<input type="hidden" name="megaGoodsSeqNo" id="megaGoodsSeqNo" value="${PASB.GOODS_SEQ_NO}">
<input type="hidden" name="gigaGoodsSeqNo" id="gigaGoodsSeqNo" value="${PASP.GOODS_SEQ_NO}">
<input type="hidden" name="teraGoodsSeqNo" id="teraGoodsSeqNo" value="${PAFP.GOODS_SEQ_NO}">

<input type="hidden" name="rnbGoodsOptSeqNo" id="rnbGoodsOptSeqNo" value="${RNB.GOODS_OPT_SEQ_NO}">
<input type="hidden" name="pybGoodsOptSeqNo" id="pybGoodsOptSeqNo" value="${PYB.GOODS_OPT_SEQ_NO}">
<input type="hidden" name="megaGoodsOptSeqNo" id="megaGoodsOptSeqNo" value="${PASB.GOODS_OPT_SEQ_NO}">
<input type="hidden" name="gigaGoodsOptSeqNo" id="gigaGoodsOptSeqNo" value="${PASP.GOODS_OPT_SEQ_NO}">
<input type="hidden" name="teraGoodsOptSeqNo" id="teraGoodsOptSeqNo" value="${PAFP.GOODS_OPT_SEQ_NO}">

<input type="hidden" name="basic_goods" id="basic_goods" value="${basic_goods}">
<input type="hidden" name="new_basic_goods_seq_no" id="new_basic_goods_seq_no">
<input type="hidden" name="new_basic_goods_type_cd" id="new_basic_goods_type_cd">
<input type="hidden" name="new_basic_goods_opt_seq_no" id="new_basic_goods_opt_seq_no">

<input type="hidden" name="eventNo" id="eventNo" value="${eventNo}">
<input type="hidden" name="basic_can_deny_yn" id="basic_can_deny_yn" value="${paidInfo.using_goods_info.basic_can_deny_yn}">
<input type="hidden" name="promoEligible" id="promoEligible" value="${promoEligible}"/>
<input type="hidden" name="isPromoChk" id="isPromoChk" value="${isPromoChk}"/>

<!-- This needs to be here, because it uses eventNo value!!! -->
<script src="/assets/js/paidInfo.js?ver=20221212_01"></script>

<div class="container">
    <!-- my_wra start -->
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">결제정보</p>
            <p class="desc">셀러봇캐시가 준비한 서비스들을 제한 없이 누려보세요!</p>
        </div>
    </div>
    <!-- my_wra end -->
    <!-- container_wrapper start -->
    <div class="container_wrapper">
        <!-- tree_menu_ul start -->
        <!-- <ul id="tree_menu_ul"> -->
        <ul id="two_menu_ul">
            <li class="blue_active2" onclick="location.href='/sub/my/paidInfo'">
                <a>이용정보 요약</a>
            </li>
            <li onclick="location.href='/sub/my/payHist'">
                <a>결제/취소 내역</a>
            </li>
            <!-- <li onclick="location.href='/sub/my/poiHist'">
                <a>S포인트 내역</a>
            </li> -->
        </ul>
        <!-- tree_menu_ul end -->
        <!-- two_menu_ul start -->
        <!-- <ul id="two_menu_ul">
            <li class="blue_active2">이용 정보 요약</li>
            <li>결제 내역</li>
        </ul> -->
        <!-- two_menu_ul end -->
        <!-- m_sec1 start -->
        <div id="m_sec1">
            <div class="row">
                <img class="left_icon" src="/assets/images/icon/s_ticket.png" alt="">
                <h1 class="m_sec_title">이용중인 서비스</h1>
                <img class="q_icon" src="/assets/images/icon/q.png" alt="">
                <div class="q_popup" id="q_popup1">
                    <h1>이용권을 변경하고 싶으시다면 변경 버튼을 눌러주세요~</h1>
                    <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                </div>
            </div>
            <c:choose>
                <c:when test="${!empty paidInfo.using_goods_info}">
                    <!-- //20200706_10 :: 테이블 수정 및 타입 추가 Start -->
                    <table id="using_service_new">
                        <thead>
                            <th>이용권</th>
                            <th>결제유형</th>
                            <th>이용시작일</th>
                            <th>이용종료일</th>
                            <th>최근결제일</th>
                            <th>다음결제일</th>
                            <th>상태</th>
                            <th colspan="2">관리</th>
                        </thead>
                        <tbody>
                            <c:choose>
                                <c:when test="${!empty paidInfo.using_goods_info.basic_goods_req_seq_no && !empty paidInfo.using_goods_info.addn_goods_req_seq_no}">
                                    <!-- type2 Start -->
                                    <tr>
                                        <td>${paidInfo.using_goods_info.basic_goods_nm}</td>
                                        <td>${paidInfo.using_goods_info.basic_pay_typ_cd_nm}</td>
                                        <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_use_stt_dt, 4)}</td>
                                        <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_use_end_dt, 4)}</td>
                                        <td>
                                            <c:if test="${!empty paidInfo.using_goods_info.basic_last_pay_date}">
                                                ${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_last_pay_date, 4)}
                                            </c:if>
                                            <c:if test="${empty paidInfo.using_goods_info.basic_last_pay_date}">
                                                -
                                            </c:if>
                                        </td>
                                        <td>
                                            <c:if test="${!empty paidInfo.using_goods_info.basic_pay_pln_dt}">
                                                ${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_pay_pln_dt, 4)}
                                            </c:if>
                                            <c:if test="${empty paidInfo.using_goods_info.basic_pay_pln_dt}">
                                                -
                                            </c:if>
                                        </td>
                                        <td>${paidInfo.using_goods_info.basic_opr_sts_cd_nm}</td>
                                        <td>
                                            <c:if test="${!empty paidInfo.pay_method_info}">
                                                <c:if test="${!empty basic_goods}">
                                                    <span class="change_payment_btn" onclick="pressedBtnShowModalForTicket();">변경</span>
                                                </c:if>
                                            </c:if> 
                                            <span class="stop_paying" onclick="pressedBtnCancel('${paidInfo.using_goods_info.basic_goods_req_seq_no}');">해지</span>
                                        </td>
                                        <td rowspan="2">
                                            <span class="bundle_cancel" onclick="pressedBtnCancel(null);">일괄 해지</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>${paidInfo.using_goods_info.addn_goods_nm}</td>
                                        <td>${paidInfo.using_goods_info.addn_pay_typ_cd_nm}</td>
                                        <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_use_stt_dt, 4)}</td>
                                        <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_use_end_dt, 4)}</td>
                                        <td>
                                            <c:if test="${!empty paidInfo.using_goods_info.addn_last_pay_date}">
                                                ${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_last_pay_date, 4)}
                                            </c:if>
                                            <c:if test="${empty paidInfo.using_goods_info.addn_last_pay_date}">
                                                -
                                            </c:if>
                                        </td>
                                        <td>
                                            <c:if test="${!empty paidInfo.using_goods_info.addn_pay_pln_dt}">
                                                ${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_pay_pln_dt, 4)}
                                            </c:if>
                                            <c:if test="${empty paidInfo.using_goods_info.addn_pay_pln_dt}">
                                                -
                                            </c:if>
                                        </td>
                                        <td>${paidInfo.using_goods_info.addn_opr_sts_cd_nm}</td>
                                        <td>
                                            <span class="stop_paying" onclick="pressedBtnCancel('${paidInfo.using_goods_info.addn_goods_req_seq_no}');">해지</span>
                                        </td>
                                    </tr>
                                    <!-- type2 End -->
                                </c:when>
                                <c:when test="${!empty paidInfo.using_goods_info.basic_goods_req_seq_no}">
                                    <!-- type1 Start -->
                                    <tr>
                                        <c:set var="using_goods" value="${paidInfo.using_goods_info}"/>
                                        <td>
                                            ${using_goods.basic_goods_nm}(${using_goods.basic_period}개월)
                                            <c:if test="${!empty using_goods.basic_event_nm}"> 
                                                <br/>[${fn:substring(using_goods.basic_event_nm, 0, 9)}]
                                            </c:if>
                                        </td>
                                        <td>${paidInfo.using_goods_info.basic_pay_typ_cd_nm}</td>
                                        <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_use_stt_dt, 4)}</td>
                                        <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_use_end_dt, 4)}</td>
                                        <td>
                                            <c:if test="${!empty paidInfo.using_goods_info.basic_last_pay_date}">
                                                ${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_last_pay_date, 4)}
                                            </c:if>
                                            <c:if test="${empty paidInfo.using_goods_info.basic_last_pay_date}">
                                                -
                                            </c:if>
                                        </td>
                                        <td>
                                            <c:if test="${!empty paidInfo.using_goods_info.basic_pay_pln_dt}">
                                                ${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_pay_pln_dt, 4)}
                                            </c:if>
                                            <c:if test="${empty paidInfo.using_goods_info.basic_pay_pln_dt}">
                                                -
                                            </c:if>
                                        </td>
                                        <td>${paidInfo.using_goods_info.basic_opr_sts_cd_nm}</td>
                                        <td colspan="2">
                                            <!-- <span class="paying00">결제</span> -->
                                            <c:if test="${!empty paidInfo.pay_method_info}">
                                                <c:if test="${!empty basic_goods}">
                                                    <span class="change_payment_btn" onclick="pressedBtnShowModalForTicket();">변경</span>
                                                </c:if>
                                            </c:if>
                                            <span class="stop_paying" onclick="pressedBtnCancel('${paidInfo.using_goods_info.basic_goods_req_seq_no}');">해지</span>
                                        </td>
                                    </tr>
                                    <!-- type1 End -->
                                </c:when>
                                <c:otherwise>
                                    <!-- type3 Start -->
                                    <tr>
                                        <td>프리봇</td>
                                        <td>평생무료</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>
                                            <span class="change_payment_btn" onclick="pressedBtnShowModalForTicket();">변경</span>
                                        </td>
                                        <td rowspan="2">
                                            <span class="bundle_cancel" onclick="pressedBtnCancel(null);">일괄 해지</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>${paidInfo.using_goods_info.addn_goods_nm}</td>
                                        <td>${paidInfo.using_goods_info.addn_pay_typ_cd_nm}</td>
                                        <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_use_stt_dt, 4)}</td>
                                        <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_use_end_dt, 4)}</td>
                                        <td>
                                            <c:if test="${!empty paidInfo.using_goods_info.addn_last_pay_date}">
                                                ${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_last_pay_date, 4)}
                                            </c:if>
                                            <c:if test="${empty paidInfo.using_goods_info.addn_last_pay_date}">
                                                -
                                            </c:if>
                                        </td>
                                        <td>
                                            <c:if test="${!empty paidInfo.using_goods_info.addn_pay_pln_dt}">
                                                ${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_pay_pln_dt, 4)}
                                            </c:if>
                                            <c:if test="${empty paidInfo.using_goods_info.addn_pay_pln_dt}">
                                                -
                                            </c:if>
                                        </td>
                                        <td>${paidInfo.using_goods_info.addn_opr_sts_cd_nm}</td>
                                        <td>
                                            <span class="stop_paying" onclick="pressedBtnCancel('${paidInfo.using_goods_info.addn_goods_req_seq_no}');">해지</span>
                                        </td>
                                    </tr>
                                    <!-- type3 End -->
                                </c:otherwise>
                            </c:choose>
                        </tbody>
                    </table>
                    <div class="notice_msg">
                        <h3 class="notice_title">Note</h3>
                        <div class="notice_content">
                            <ul>
                                <li>- 일괄해지는 이용중인 서비스를 모두 해지 하게 됩니다.</li>
                                <li>- 결제수단 변경은 이용중인 서비스의 모든 결제수단이 변경 됩니다.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- //20200706_10 :: 테이블 수정 및 타입 추가 End -->
                </c:when>
                <c:otherwise>
                    <table id="using_service_new" style="margin-bottom:0px;">
                        <thead>
                            <th>이용권</th>
                            <th>결제유형</th>
                            <th>이용시작일</th>
                            <th>이용종료일</th>
                            <th>최근결제일</th>
                            <th>다음결제일</th>
                            <th>상태</th>
                            <th>관리</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div style="margin-top:0; margin-bottom:80px;" class="grid_noData">
                        <h1>셀러봇캐시 유료 서비스를 이용해보실래요?</h1>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <!-- m_sec1 end -->


        <!-- m_sec1_mb start -->
        <div class="m_sevice_mb" id="m_sec1_mb">
            <div class="row m_sevice_mb_header">
                <div>
                    <img class="left_icon" src="/assets/images/icon/s_ticket.png" alt="">
                    <h1 class="m_sec_title">이용중인 서비스</h1>
                    <img class="q_icon" src="/assets/images/icon/q.png" alt="">
                    <div class="q_popup" id="q_popup1_mb">
                        <h1>이용권을 변경하고 싶으시다면 변경 버튼을 눌러주세요~</h1>
                        <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                    </div>
                </div>
                <c:if test="${!empty paidInfo.using_goods_info.basic_goods_req_seq_no && !empty paidInfo.using_goods_info.addn_goods_req_seq_no}">
                    <button class="button" style="border-radius: 4px;" onclick="pressedBtnCancel(null);">일괄 해지</button>
                </c:if>
            </div>
            <!-- //20200706_10 :: 모바일 테이블 수정 및 타입 추가 Start -->

            <c:choose>
                <c:when test="${!empty paidInfo.using_goods_info.basic_goods_req_seq_no || !empty paidInfo.using_goods_info.addn_goods_req_seq_no}">
                    <c:if test="${!empty paidInfo.using_goods_info.basic_goods_req_seq_no}">
                        <ul class="mb_table_service ticket_info">
                            <li class="gray_bg2 mb_header">
                                <h1>이용권</h1>
                                <h2>${paidInfo.using_goods_info.basic_goods_nm}</h2>
                            </li>
                            <li class="gray_bg">
                                <h1>결제유형</h1>
                                <h2 class="blue_liner0">${paidInfo.using_goods_info.basic_pay_typ_cd_nm}</h2>
                            </li>
                            <li>
                                <h1>이용시작일</h1>
                                <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_use_stt_dt, 4)}</h2>
                            </li>
                            <li>
                                <h1>이용종료일</h1>
                                <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_use_end_dt, 4)}</h2>
                            </li>
                            <li>
                                <h1>최근결제일</h1>
                                <c:if test="${!empty paidInfo.using_goods_info.basic_last_pay_date}">
                                    <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_last_pay_date, 4)}</h2>
                                </c:if>
                                <c:if test="${empty paidInfo.using_goods_info.basic_last_pay_date}">
                                    <h2>-</h2>
                                </c:if>
                            </li>
                            <li>
                                <h1>다음결제일</h1>
                                <c:if test="${!empty paidInfo.using_goods_info.basic_pay_pln_dt}">
                                    <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.basic_pay_pln_dt, 4)}</h2>
                                </c:if>
                                <c:if test="${empty paidInfo.using_goods_info.basic_pay_pln_dt}">
                                    <h2>-</h2>
                                </c:if>
                            </li>
                            <li>
                                <h1>상태</h1>
                                <h2>${paidInfo.using_goods_info.basic_opr_sts_cd_nm}</h2>
                            </li>
                            <li>
                                <h1>관리</h1>
                                <div class="s_mb_btn_row">
                                    <!-- <button type="button" class="payment">결제</button> -->
                                    <c:if test="${!empty paidInfo.pay_method_info}">
                                        <c:if test="${!empty basic_goods}">
                                            <button type="button" class="change_payment_btn" onclick="pressedBtnShowModalForTicket();">변경</button>
                                        </c:if>
                                    </c:if>
                                    <button type="button" class="stop_paying"
                                        onclick="pressedBtnCancel('${paidInfo.using_goods_info.basic_goods_req_seq_no}');">해지</button>
                                </div>
                            </li>
                        </ul>
                    </c:if>
                    <c:if test="${!empty paidInfo.using_goods_info.addn_goods_req_seq_no}">
                        <ul class="mb_table_service ticket_info">
                            <li class="gray_bg2 mb_header">
                                <h1>이용권</h1>
                                <h2>${paidInfo.using_goods_info.addn_goods_nm}</h2>
                            </li>
                            <li class="gray_bg">
                                <h1>결제유형</h1>
                                <h2 class="blue_liner0">${paidInfo.using_goods_info.addn_pay_typ_cd_nm}</h2>
                            </li>
                            <li>
                                <h1>이용시작일</h1>
                                <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_use_stt_dt, 4)}</h2>
                            </li>
                            <li>
                                <h1>이용종료일</h1>
                                <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_use_end_dt, 4)}</h2>
                            </li>
                            <li>
                                <h1>최근결제일</h1>
                                <c:if test="${!empty paidInfo.using_goods_info.addn_last_pay_date}">
                                    <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_last_pay_date, 4)}</h2>
                                </c:if>
                                <c:if test="${empty paidInfo.using_goods_info.addn_last_pay_date}">
                                    <h2>-</h2>
                                </c:if>
                            </li>
                            <li>
                                <h1>다음결제일</h1>
                                <c:if test="${!empty paidInfo.using_goods_info.addn_pay_pln_dt}">
                                    <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.using_goods_info.addn_pay_pln_dt, 4)}</h2>
                                </c:if>
                                <c:if test="${empty paidInfo.using_goods_info.addn_pay_pln_dt}">
                                    <h2>-</h2>
                                </c:if>
                            </li>
                            <li>
                                <h1>상태</h1>
                                <h2>${paidInfo.using_goods_info.addn_opr_sts_cd_nm}</h2>
                            </li>
                            <li>
                                <h1>관리</h1>
                                <div class="s_mb_btn_row">
                                    <button type="button" class="stop_paying"
                                        onclick="pressedBtnCancel('${paidInfo.using_goods_info.addn_goods_req_seq_no}');">해지</button>
                                </div>
                            </li>
                        </ul>
                    </c:if>
                </c:when>
                <c:otherwise>
                    <div class="grid_noData">
                        <h1>셀러봇캐시 유료 서비스를 이용해보실래요?</h1>
                    </div>
                </c:otherwise>
            </c:choose>
            <!-- //20200706_10 :: 모바일 테이블 수정 및 타입 추가 End -->
        </div>
        <!-- m_sec1_mb end -->

        <!-- m_sec2 start -->
        <div id="m_sec2">
            <div class="row">
                <img class="left_icon" src="/assets/images/icon/m_card.png" alt="">
                <h1 class="m_sec_title">결제수단</h1>
                <img class="q_icon" src="/assets/images/icon/q.png" alt="">
                <div class="q_popup" id="q_popup2">
                    <h1>등록하신 결제수단에서 결제가 이루어집니다</h1>
                    <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                </div>
            </div>
            <c:choose>
                <c:when test="${!empty paidInfo.pay_method_info}">
                    <table id="useing_service">
                        <thead>
                            <th>상태</th>
                            <th>구분</th>
                            <th>금융사</th>
                            <th>카드번호</th>
                            <th>등록일</th>
                            <th>관리</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${paidInfo.pay_method_info.pay_typ_cd_nm}</td>
                                <td>${paidInfo.pay_method_info.pay_method}</td>
                                <td>${paidInfo.pay_method_info.card_code_nm}</td>
                                <td>${paidInfo.pay_method_info.card_num}</td>
                                <td>${dateUtilBean.changeDateFormatForIndex(paidInfo.pay_method_info.reg_ts, 4)}</td>
                                <td><span class="change_payment_btn" onclick="pressedBtnShowModalForCard();">변경</span></td>
                            </tr>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <table id="useing_service" style="margin-bottom:0px;">
                        <thead>
                            <th>상태</th>
                            <th>구분</th>
                            <th>금융사</th>
                            <th>카드번호</th>
                            <th>등록일</th>
                            <th>관리</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div style="margin-top:0; margin-bottom:80px;" class="grid_noData">
                        <h1>등록된 결제 수단이 없어요~.</h1>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <!-- m_sec2 end -->
        <!-- m_sec2_mb start -->
        <div class="m_sevice_mb" id="m_sec2_mb">
            <div class="row">
                <img class="left_icon" src="/assets/images/icon/m_card.png" alt="">
                <h1 class="m_sec_title">결제수단</h1>
                <img class="q_icon" src="/assets/images/icon/q.png" alt="">
                <div class="q_popup" id="q_popup2_mb">
                    <h1>등록하신 결제수단에서 결제가 이루어집니다</h1>
                    <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                </div>
            </div>
            <ul class="mb_table_service">
                <c:choose>
                    <c:when test="${!empty paidInfo.pay_method_info}">
                        <li class="gray_bg">
                            <h1>상태</h1>
                            <h2 class="blue_liner0">${paidInfo.pay_method_info.pay_typ_cd_nm}</h2>
                        </li>
                        <li>
                            <h1>구분</h1>
                            <h2>${paidInfo.pay_method_info.pay_method}</h2>
                        </li>
                        <li>
                            <h1>금융사</h1>
                            <h2>${paidInfo.pay_method_info.card_code_nm}</h2>
                        </li>
                        <li>
                            <h1>카드번호</h1>
                            <h2>${paidInfo.pay_method_info.card_num}</h2>
                        </li>
                        <li>
                            <h1>등록일</h1>
                            <h2>${dateUtilBean.changeDateFormatForIndex(paidInfo.pay_method_info.reg_ts, 4)}</h2>
                        </li>
                        <li>
                            <h1>관리</h1>
                            <div class="s_mb_btn_row">
                                <button type="button" class="change_payment_btn" onclick="pressedBtnShowModalForCard();">변경</button>
                            </div>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <div class="grid_noData">
                            <h1>등록된 결제 수단이 없어요~.</h1>
                        </div>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
        <!-- m_sec2_mb end -->
        <!-- m_sec3 start -->
        <%--<div id="m_sec3">
            <div class="row">
                <img class="left_icon" src="/assets/images/icon/m_coin.png" alt="">
                <h1 class="m_sec_title">나의 S포인트</h1>
                <img class="q_icon" src="/assets/images/icon/q.png" alt="">
                <div class="q_popup" id="q_popup3">
                    <h1>정기결제로 이용하시는 경우, S포인트 사용예약을 하시면 다음 회차에 포인트가 한 번 사용됩니다</h1>
                    <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                </div>
            </div>
            <table id="useing_service">
                <thead>
                    <th>포인트합계</th>
                    <th>60일 이내 소멸예정</th>
                    <th>상태</th>
                    <th>사용예정</th>
                    <th>관리</th>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <fmt:formatNumber value="${paidInfo.poi_info.poi_sum}" pattern="#,###" /> P</td>
                        <td><span class="s_point">${paidInfo.poi_info.expire_pln_poi} P</span>
                            <button type="button" class="open_s_point">내역보기</button></td>
                        <td class="two_row">
                            <c:if test="${paidInfo.poi_info.reserv_cd eq 'RESE'}">
                                <h1>${paidInfo.poi_info.reserv_cd_nm}</h1>
                                <h2>(사용예정일: ${dateUtilBean.changeDateFormatForIndex(paidInfo.poi_info.use_reserv_dt, 1)})</h2>
                            </c:if>
                            <c:if test="${paidInfo.poi_info.reserv_cd ne 'RESE'}">
                                -
                            </c:if>
                        </td>
                        <td>${paidInfo.poi_info.use_reserv_poi} P</td>
                        <td>
                            <!-- 이용중인 서비스가 없으면 예약 불가 -->
                            <c:if test="${empty paidInfo.poi_info.goods_req_seq_no}">
                                -
                            </c:if>
                            <c:if test="${!empty paidInfo.poi_info.goods_req_seq_no}">
                                <!-- 예약 중인 경우 취소 가능 -->
                                <c:if test="${paidInfo.poi_info.reserv_cd eq 'RESE'}">
                                    <span class="reservation_point"
                                        onclick="pressedBtnReservCancel('${paidInfo.poi_info.use_reserv_seq_no}');">사용예약
                                        취소</span>
                                </c:if>
                                <!-- 포인트가 존재하는 경우만 예약 가능 -->
                                <c:if test="${paidInfo.poi_info.reserv_cd ne 'RESE'}">
                                    <c:if test="${paidInfo.poi_info.poi_sum > 0}">
                                        <span class="reservation_point"
                                            onclick="pressedBtnReservPoi('${paidInfo.poi_info.poi_sum}');">사용예약</span>
                                    </c:if>
                                    <c:if test="${paidInfo.poi_info.poi_sum == 0}">
                                        <span class="cancel_reservation_point">사용예약</span>
                                    </c:if>
                                </c:if>
                            </c:if>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>--%>
        <!-- m_sec3 end -->
        <!-- m_sec3_mb start -->
        <%--<div class="m_sevice_mb" id="m_sec3_mb">
            <div class="row">
                <img class="left_icon" src="/assets/images/icon/m_coin.png" alt="">
                <h1 class="m_sec_title">나의 S포인트</h1>
                <img class="q_icon" src="/assets/images/icon/q.png" alt="">
                <div class="q_popup" id="q_popup3_mb">
                    <h1>정기결제로 이용하시는 경우,S포인트 사용예약을 하시면 <br>다음 회차에 포인트가 한 번 사용됩니다</h1>
                    <img class="close_popup" src="/assets/images/icon/x_white.png" alt="">
                </div>
            </div>
            <ul class="mb_table_service">
                <li class="gray_bg">
                    <h1>포인트 합계</h1>
                    <h2 class="blue_liner0">
                        <fmt:formatNumber value="${paidInfo.poi_info.poi_sum}" pattern="#,###" /> P</h2>
                </li>
                <li>
                    <h1 class="width50">60일 이내 소멸예정</h1>
                    <h2 class="point_s">${paidInfo.poi_info.expire_pln_poi} P <button type="button"
                            class="open_s_point open_s_point_mb ">내역보기</button></h2>
                </li>
                <li>
                    <h1>상태</h1>
                    <c:if test="${paidInfo.poi_info.reserv_cd eq 'RESE'}">
                        <h2><span
                                class="gray_s_date">(사용예정일:${dateUtilBean.changeDateFormatForIndex(paidInfo.poi_info.use_reserv_dt,
                                1)})</span>${paidInfo.poi_info.reserv_cd_nm}</h2>
                    </c:if>
                    <c:if test="${paidInfo.poi_info.reserv_cd ne 'RESE'}">
                        <h2>-</h2>
                    </c:if>
                </li>
                <li>
                    <h1>사용예정</h1>
                    <h2>${paidInfo.poi_info.use_reserv_poi} P</h2>
                </li>
                <li>
                    <h1>관리</h1>
                    <!-- 이용중인 서비스가 없으면 예약 불가 -->
                    <c:if test="${empty paidInfo.poi_info.goods_req_seq_no}">
                        <h2>-</h2>
                    </c:if>
                    <c:if test="${!empty paidInfo.poi_info.goods_req_seq_no}">
                        <div class="s_mb_btn_row">
                            <!-- 예약 중인 경우 취소 가능 -->
                            <c:if test="${paidInfo.poi_info.reserv_cd eq 'RESE'}">
                                <button type="button" class="reservation_point_mb"
                                    onclick="pressedBtnReservCancel('${paidInfo.poi_info.use_reserv_seq_no}');">사용예약 취소</button>
                            </c:if>
                            <!-- 포인트가 존재하는 경우만 예약 가능 -->
                            <c:if test="${paidInfo.poi_info.reserv_cd ne 'RESE'}">
                                <c:if test="${paidInfo.poi_info.poi_sum > 0}">
                                    <button type="button" class="reservation_point_mb"
                                        onclick="pressedBtnReservPoi('${paidInfo.poi_info.poi_sum}');">사용예약</button>
                                </c:if>
                                <c:if test="${paidInfo.poi_info.poi_sum == 0}">
                                    <button type="button" class="reservation_point_mb_cancel" disabled>사용예약</button>
                                </c:if>
                            </c:if>
                        </div>
                    </c:if>
                </li>
            </ul>
        </div>--%>
        <!-- m_sec3_mb end -->
    </div>
    <!-- container_wrapper end -->
    <!-- s_modal start -->
    <div class="s_modal">
        <div id="s_modal_body">
            <div id="s_header">
                <img id="close_btn" src="/assets/images/icon/x_black.png" alt="">
                <img id="y_alert" src="/assets/images/icon/s_modal_icon.png" alt="">
                <h1>소멸예정 S포인트 내역</h1>
                <h2>(60일 이내)</h2>
            </div>
            <h1 id="text2">
                S포인트는 적입 후 365일 동안 사용 가능합니다. <br> 회원님의 소중한 S포인트가 자동 소멸되기 전에 사용 해 주세요.
            </h1>
            <div class="popup_table_form">
                <div class="popup_table_form_color">
                    <div class="row">
                        <div class="col">소멸 예정일</div>
                        <div class="col">소멸 예정 포인트</div>
                    </div>
                </div>
                <div class="popup_table_form_body">
                    <c:if test="${poiHistInfo.POI_SUM > 0}">
                        <c:forEach items="${poiHistInfo.poi_hist}" var="hist" varStatus="status">
                            <div class="row">
                                <div class="col">${dateUtilBean.changeDateFormatForIndex(hist.EXPIRE_TS, 2)}</div>
                                <div class="col">${hist.POI_SAV_PRC}P</div>
                            </div>
                        </c:forEach>
                    </c:if>
                    <c:if test="${poiHistInfo.POI_SUM == 0}">
                        <div class="row">
                            <div class="col">-</div>
                            <div class="col">0P</div>
                        </div>
                    </c:if>
                </div>
                <div class="popup_table_form_color">
                    <div class="row">
                        <div class="col bold">합계 포인트</div>
                        <div class="col color bold">${poiHistInfo.POI_SUM}P</div>
                    </div>
                </div>
            </div>
            <c:if test="${poiHistInfo.POI_SUM == 0}">
                <h1 id="null_s_point">
                    소멸 예정인 포인트가 없습니다.
                </h1>
            </c:if>
            <button type="button" id="close_s_modal">
                닫기
            </button>
        </div>
        <div id="s_modal_bg"></div>
    </div>
    <!-- s_modal end -->
    <div class="popup_change_1" id="ChangeTicketModal">
        <div class="popup_box popup_box_2">
            <div class="popup_header">
                <h1>셀러봇캐시 이용권 변경</h1>
                <img id="close_btn" src="/assets/images/icon/x_black.png" alt="">
            </div>
            <div id="changeTicketDetailArea">
            <c:if test="${eventNo != 0}">
                <div class="popup_2_contents">
                    <div id="ticket_types">
                        <div class="ticket_type_header"><h1>현재 이용권</h1></div>
                        <div class="ticket_type_body">
                        <c:set var="usingGoods" value="${paidInfo.using_goods_info}"/>
                        <c:if test="${ usingGoods.basic_goods_typ_cd eq 'RNB' }">
                            <div class="ticket_type_box ronibot">
                                <h2><span id="roni">로니봇</span></h2>
                                <h3>매월 <strong><fmt:formatNumber value="${usingGoods.basic_fna_prc}" pattern="#,###" /> 원</strong></h3>
                            </div>
                        </c:if>
                        <c:if test="${ usingGoods.basic_goods_typ_cd eq 'PYB' }">
                            <div class="ticket_type_box paibot">
                                <h2><span id="pai">파이봇</span></h2>
                                <h3>매월 <strong><fmt:formatNumber value="${usingGoods.basic_fna_prc}" pattern="#,###" /> 원</strong></h3>
                            </div>
                        </c:if>
                        <c:if test="${ usingGoods.basic_goods_typ_cd eq 'PASB' }">
                            <div class="ticket_type_box ronibot">
                                <h2><span id="roni">메가봇</span></h2>
                                <h3>매월 <strong><fmt:formatNumber value="${usingGoods.basic_fna_prc}" pattern="#,###" /> 원</strong></h3>
                            </div>
                        </c:if>
                        <c:if test="${ usingGoods.basic_goods_typ_cd eq 'PASP' }">
                            <div class="ticket_type_box paibot">
                                <h2><span id="pai">기가봇</span></h2>
                                <h3>매월 <strong><fmt:formatNumber value="${usingGoods.basic_fna_prc}" pattern="#,###" /> 원</strong></h3>
                            </div>
                        </c:if>
                        <c:if test="${ usingGoods.basic_goods_typ_cd eq 'PAFP' }">
                            <div class="ticket_type_box paibot">
                                <h2><span id="pai">테라봇</span></h2>
                                <h3>매월 <strong><fmt:formatNumber value="${usingGoods.basic_fna_prc}" pattern="#,###" /> 원</strong></h3>
                            </div>
                        </c:if>
                        </div>
                    </div>
                </div>
                <div class="popup_2_contents">
                    <div id="ticket_types">
                        <div class="ticket_type_header"><h1>변경할 이용권</h1></div>
                        <div class="ticket_type_body">
                            <div class="ticket_type_box ronibot" id="newTicketPASB">
                                <h2><span id="roni">메가봇</span></h2>
                                <h3>매월 <strong><fmt:formatNumber value="${PASB.FNA_PRC}" pattern="#,###" /> 원</strong></h3>
                                <div class="btn_line">
                                    <div class="current_selected_ticket">
                                        <span class="ronibot active"></span><span class="paibot"></span>
                                    </div>
                                    <select id="selectBox_ronibot">
                                        <c:forEach var="g" items="${basic_goods}">
                                            <c:if test="${ g.GOODS_TYP_CD eq 'PASB' }">
                                                <option data-seq="${g.GOODS_OPT_SEQ_NO}" data-prc="${g.FNA_PRC}" data-period="${g.PERIOD}" ><fmt:formatNumber value="${g.FNA_PRC}" pattern="#,###" />원/${g.PERIOD}개월</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="ticket_type_box paibot" id="newTicketPASP">
                                <h2><span id="pai">기가봇</span></h2>
                                <h3>매월 <strong><fmt:formatNumber value="${PASP.FNA_PRC}" pattern="#,###" /> 원</strong></h3>
                                <div class="btn_line">
                                    <div class="current_selected_ticket">
                                        <span class="ronibot"></span><span class="paibot active"></span>
                                    </div>
                                    <select id="selectBox_paibot">
                                        <c:forEach var="g" items="${basic_goods}">
                                            <c:if test="${ g.GOODS_TYP_CD eq 'PASP' }">
                                                <option data-seq="${g.GOODS_OPT_SEQ_NO}" data-prc="${g.FNA_PRC}" data-period="${g.PERIOD}" ><fmt:formatNumber value="${g.FNA_PRC}" pattern="#,###" />원/${g.PERIOD}개월</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="ticket_type_box ronibot" id="newTicketPAFP">
                                <h2><span id="roni">테라봇</span></h2>
                                <h3>매월 <strong><fmt:formatNumber value="${PAFP.FNA_PRC}" pattern="#,###" /> 원</strong></h3>
                                <div class="btn_line">
                                    <div class="current_selected_ticket">
                                        <span class="ronibot active"></span><span class="paibot"></span>
                                    </div>
                                    <select id="selectBox_ronibot">
                                        <c:forEach var="g" items="${basic_goods}">
                                            <c:if test="${ g.GOODS_TYP_CD eq 'PAFP' }">
                                                <option data-seq="${g.GOODS_OPT_SEQ_NO}" data-prc="${g.FNA_PRC}" data-period="${g.PERIOD}" ><fmt:formatNumber value="${g.FNA_PRC}" pattern="#,###" />원/${g.PERIOD}개월</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
            </div>
            <span class="gui_text">
                ※다음 결제일부터 변경된 요금제의 요금이 매월 청구되며, 데이터 제공량은 즉시 변경 적용됩니다.
            </span>
            <span class="error" id="errorForTicket">해당 이용권으로 변경이 불가능합니다.</span>
            <div class="button_row">
                <button type="button" class="close">닫기</button>
                <button type="button" class="chg_ticket" onclick="agreeChangeTicket();">변경</button>
                <!-- <input class="button" type="button" value="변경" onclick="agreeChangeTicket();"/> -->
            </div>
        </div>
        <div class="popup_black"></div>
    </div>
    <div class="agree_text_popup_wrap">
        <div class="display_none_back">
            <div id="title">
                <div class="add_pooup_close">
                <div></div>
                <div></div>
            </div>
                <h1>셀러봇캐시 유료 서비스 약관</h1>
            </div>
            <h1>
                제 12 조 (유료 서비스 이용관련) <br>
                (1) 회사가 정한 사용요건과 약관에 동의한 회원만이 유료 서비스를 이용할 수 있습니다.<br><br>
                (2) 결제 승인은 회원이 선택한 결제방법으로 신청한 경우 회사가 승낙함으로써 성립되며 회사는 결제 요청시 회원이 입력한 내용을 모두 사실로 간주 하며 허위로 내용을 입력한
                회원은 법적인 보호를 받을 수 없습니다. 또한 본 항을 어긴 회원의 경우 당사 사용을 중지하거나 회원자격 박탈 시킬 수 있으며, 재가입을 불허할 수 있습니다.<br>
                <br><br>
                (3) 회사는 신용불량자로 등록된 회원이나 대금 지급을 연체하는 회원에 대하여 유료 서비스 이용을 제한할 수 있습니다.
                <br><br>
                (4) 당사 서비스의 중대한 하자에 의하여 서비스의 기능이 상실되었거나 불량했을 경우 장애시간에 따른 사용기간 연장 혹은 해당 기능 복원으로 보상 받을 수 있습니다. 단,
                사용기간 연장 시 최대 15일까지만 보상이 가능합니다.
                <br><br>
                (5) 회원은 다음 각 호의 사유가 있으면 적법한 절차를 거친 후 회원 자신이 직접 유료 서비스 이용을 하기 위해 소비한 금액을 회사로부터 환불받을 수 있습니다.
                ① 유료 서비스 신청을 했으나 사용할 수 있는 서비스가 전무하며 그에 대한 책임이 전적으로 당사에 있을 경우 (단, 시스템 정기 점검 등 불가피한 경우를 제외)
                ② 유료 서비스 신청을 하여 사용료를 결제 하였으나, 유료서비스가 3일 이내에 실시되지 않아 회원이 환불을 요청한 경우
                ③ 결제 후 14일 이내 환불을 신청한 경우
                ④ 기타 소비자 보호를 위하여 당사에서 따로 정하는 경우
                <br><br>
                단, 회사는 결제일 이후 지난 기간에 따라 환불 수수료 및 위약금 등을 고려하여 해지 시 환불 금액을 조정할 수 있습니다.
                ① 결제 당일 해지 시 100% 환불
                ② 결제 후 14일 이전 해지 시 50% 환불
                ③ 결제 후 14일 이후 해지 시 0% 환불 (환불 불가능)
                <br><br>
                (6) 서비스 환불 시 적립금 처리 관련 하여 다음과 같이 정의합니다.
                ① 환불 시 해당 상품 구매로 인해 회사가 부여한 적립금은 회수합니다.
                ② 결제 시 사용한 적립금은 환불이 불가합니다.
                ③ 적립금은 현금으로 환불 받을 수 없습니다.
                <br><br>
                (7) 환불절차는 다음 각 호와 같습니다.<br>
                ① 본조 제5항 ①, ②, ③호에 의한 이유로 환불을 원하는 회원은 유료 서비스 신청을 한 후 10일 이내에 회사로 연락하여 환불을 신청해야 하며, 당사는 환불 신청이
                정당함을 심사한 후, 정당한 이유가 있음으로 판명된 회원에게 환불합니다. 유료 서비스 신청 후 10일 이상 지난 후에는 환불신청을 하여도 환불을 받으실 수 없습니다.
                ② 본조 제5항 ④호의 경우 회사는 해지 시, 환불 절차를 병행하여 진행합니다.<br>
                <br>
                (8) 당사 서비스 이용 시 이용약관에 위반한 행위를 하여 일시 사용 정지되는 경우, 해당 회원이 신청한 유료 서비스 사용 기간은 당사가 보관 하며, 사용정지기간이 끝나면
                회원에게 다시 귀속되어 사용할 수 있습니다.
                <br><br>
                (9) 미납금액이 있는 회원이 탈퇴신청 할 경우 미납액을 해결하기 전에 탈퇴가 불가능하며, 회사는 미납액에 대해 탈퇴를 신청한 회원에게 청구할 권리와 의무가 있습니다.
                <br><br>
                (10) 결제수단을 이용하여 유료서비스를 신청한 경험이 없는 회원이 해지할 경우, 남아있던 사용 기간은 전액 소멸됩니다.
                <br><br>
                (11) 법률에서 정한 미성년자에 해당되는 회원은 유료 서비스를 신청할 경우 법정대리인의 동의를 얻어야 하며 동의를 얻지 못한 미성년자에게는 유료 서비스 신청에 제한을 둘
                수 있습니다. 동의 방법은 유료 서비스 신청시 회사가 정하는 소정의 동의방법에 따릅니다.
                <br><br>
                (12) 회사는 당 서비스와 관련하여 이용자가 제기하는 정당한 의견이나 불만을 반영하여 우선적으로 그 사항을 처리합니다. 다만, 신속한 처리가 곤란한 경우에는 이용자에게 그
                사유와 처리일정을 즉시 통보해 드립니다. 회사와 이용자간에 발생한 분쟁은 전자거래기본법 제28조 및 동 시행령 제 15조에 의하여 설치된 전자거래분쟁조정위원회의 조정에 따를
                수 있습니다.
                <br><br>
                (13) 회사는 실시간 계좌이체와 가상계좌로 결제한 이용금액에 대하여 매월 말일 기준으로 합계 액에 대한 세금 계산서를 발행합니다.
                <br><br>
                (14) 회원은 세금계산서의 발행을 위해 회원의 사업자 정보를 서비스 화면을 통해 입력해야 합니다. (마이페이지 -> 회원정보입력)
                <br><br>
                (15) 적립 후 사용하지 않고 유효기간이 경과된 적립금은 유효기간 경과분에 한해 기간 만료일 다음날 자동 소멸됩니다.
                ① 적립금의 유효기간은 적립된 시점으로부터 1년(365일)입니다.
            </h1>
            <button type="button" id="check_script"
                class="btn_confirm_terms join_step_popup_close">약관확인</button>
        </div>
        <div id="bg"></div>
    </div>
    <script>
        document.querySelectorAll(".popup_header img")[0].addEventListener("click", function () {
            document.querySelectorAll(".popup_change_1")[0].style.display = "none";
        });
        document.querySelectorAll(".button_row button")[0].addEventListener("click", function () {
            document.querySelectorAll(".popup_change_1")[0].style.display = "none";
        });
    </script>
</div>
<style>
    .my_wra {
        padding-top: 5em;
        min-height: auto;
        overflow-x: hidden;
    }
</style>
<%
    String mid = (String)request.getAttribute("mid");
    if (mid == "INIpayTest") {
        out.println("<script language='javascript' type='text/javascript' src='https://stgstdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>");
    } else {
        out.println("<script language='javascript' type='text/javascript' src='https://stdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>");
    }
%>
<div>
<c:choose>
    <c:when test="${DeviceType eq 'PC'}">
        <c:choose>
            <c:when test="${mid eq 'INIpayTest'}">
                <script language='javascript' type='text/javascript' src='https://stgstdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>
            </c:when>
            <c:otherwise>
                <script language='javascript' type='text/javascript' src='https://stdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>
            </c:otherwise>
        </c:choose>
        <form id="SendPayForm_id" method="POST" >
            <input type="hidden" name="version" value="1.0" >
            <input type="hidden" name="mid" value="${mid}">
            <input type="hidden" name="goodname">
            <input type="hidden" name="oid">
            <input type="hidden" name="price">
            <input type="hidden" name="currency" value="WON">
            <input type="hidden" name="buyername" value=${buyername}>
            <input type="hidden" name="buyertel" value="${buyertel}">
            <input type="hidden" name="buyeremail" value="${buyeremail}">
            <input type="hidden" name="timestamp">
            <input type="hidden" name="signature">
            <input type="hidden" name="returnUrl" value="${siteDomain}/sub/payment/pay_payresponse">
            <input type="hidden" name="closeUrl" value="${siteDomain}/pub/payment/pay_close">
            <input type="hidden" name="mKey" value="${mKey}">
            <input type="hidden" name="gopaymethod" value="">
            <input type="hidden" name="acceptmethod" value="BILLAUTH(card):FULLVERIFY"> 
            <input type="hidden" name="merchantData"> 
            <input type="hidden" name="offerPeriod" value="M2">
        </form>
    </c:when>
    <c:otherwise>
        <form id="SendPayForm_id" name="SendPayForm_id" method="POST" action="https://inilite.inicis.com/inibill/inibill_card.jsp">
            <input type="hidden" name="mid" value="${mid}">
            <input type="hidden" name="buyername" value=${buyername}>            
            <input type="hidden" name="goodname">
            <input type="hidden" name="price">
            <input type="hidden" name="orderid">
            <input type="hidden" name="returnurl" value="${siteDomain}/sub/payment/pay_mobilepayresponse">
            <input type="hidden" name="timestamp">
            <input type="hidden" name="period" value="M2">
            <input type="hidden" name="p_noti">
            <input type="hidden" name="hashdata">
            <input type="hidden" name="buyername" value=${buyername}>
            <input type="hidden" name="buyertel" value="${buyertel}">
            <input type="hidden" name="buyeremail" value="${buyeremail}">
        </form>
    </c:otherwise>
</c:choose>
</div>
<script>
    $("ul#tree_menu_ul li").click(function () {
        $(this).addClass("blue_active2");
        $("ul#tree_menu_ul li").not($(this)).removeClass("blue_active2");
    });

    $("#m_sec1_mb .q_icon").click(function () {
        $("#q_popup1_mb").css("display", "inline-block");
    });

    $("#m_sec1 .q_icon").click(function () {
        $("#q_popup1").css("display", "inline-block");
    });

    $("#q_popup1 .close_popup").click(function () {
        $("#q_popup1").css("display", "none");
    });

    $("#m_sec1_mb .q_popup .close_popup").click(function () {
        $("#q_popup1_mb").css("display", "none");
    });

    $("#m_sec2_mb .q_icon").click(function () {
        $("#q_popup2_mb").css("display", "inline-block");
    });

    $("#m_sec2 .q_icon").click(function () {
        $("#q_popup2").css("display", "inline-block");
    });

    $("#q_popup2 .close_popup").click(function () {
        $("#q_popup2").css("display", "none");
    });

    $("#q_popup2_mb .close_popup").click(function () {
        $("#q_popup2_mb").css("display", "none");
    });

    $("#m_sec3_mb .q_icon").click(function () {
        $("#q_popup3_mb").css("display", "inline-block");
    });

    $("#m_sec3 .q_icon").click(function () {
        $("#q_popup3").css("display", "inline-block");
    });

    $("#q_popup3 .close_popup").click(function () {
        $("#q_popup3").css("display", "none");
    });

    $("#q_popup3_mb .close_popup").click(function () {
        $("#q_popup3_mb").css("display", "none");
    });

    $(".open_s_point").click(function () {
        $(".s_modal").css("display", "flex");
        $("html,body").css("overflowY", "hidden");
    });

    $("#s_header img#close_btn, #s_modal_body button#close_s_modal").click(function () {
        $(".s_modal").css("display", "none");
        $("html,body").css("overflowY", "auto");
    });

    $("#agree_script_div").click(function () {
        $(".agree_text_popup_wrap").css("display", "flex");
        $("html,body").css("overflowY","hidden");
    });

    $("#check_script").click(function () {
        $(".agree_text_popup_wrap").css("display", "none");
        $("html,body").css("overflowY","visible");
    });

    $(".agree_text_popup_wrap div.display_none_back #title .add_pooup_close").click(function() {
        $(".agree_text_popup_wrap").css("display", "none");
        $("html,body").css("overflowY","visible");
    })

    $("#agree_chk").click(function() {
        if ($(this).is(":checked")) {
            $("#errorForCard").css("display", "none");
        }
        else {
            $("#errorForCard").css("display", "block");
        }
    });
</script>
<script>
    $(document).ready(function () {
        let successYn = $("#successYN").val();
        if (successYn == "Y") {
            showAlert("결제수단을 변경하였습니다.");
        } else if (successYn == "N") {
            showAlert("결제수단 변경 시 오류가 발생하였습니다.<br>다시 시도해주세요.<br>(" + $("#errMsg").val() + ")");
        }
    });
</script>

<c:if test="${eventNo == 52}">
<script>
    $(".ronibot").click(function(){
        selectBasicGoods("RNB");
        return false;
    });

    $(".paibot").click(function() {
        selectBasicGoods("PYB");
        return false;
    });

    let selectBasicGoods = function(type) {
        $('#errorForTicket').css('display', 'none');
        $("#newTicketRNB").css('display', 'none');
        $("#newTicketPYB").css('display', 'none');
        switch (type) {
            case "RNB":
                $("#newTicketRNB").css('display', 'block');
                $('#selectBox_ronibot option:eq(0)').attr('selected', 'selected');
                break;
            case "PYB":
                $("#newTicketPYB").css('display', 'block');
                $('#selectBox_paibot option:eq(0)').attr('selected', 'selected');
                break;
            default:
                break;
        }
    }

    $("#selectBox_ronibot").click(function() {
            return false;
    });

    $("#selectBox_paibot").click(function() {
        return false;
    });

    $("#selectBox_ronibot").change(function() {
        $('#errorForTicket').css('display', 'none');
        return false;
    });

    $("#selectBox_paibot").change(function() {
        $('#errorForTicket').css('display', 'none');
        return false;
    });

</script>
</c:if>
