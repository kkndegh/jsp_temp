<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/assets/js/select2/select2.css">
<style>
	.summary_col:hover {
		background-color: #009aca;
	}

	.summary_col:hover p {
		color: #fff !important;
	}

	.clearfix_title:hover p {
		color: #555555 !important;
	}
</style>

<input type="hidden" id="currentRegCount" value="${acctDTo.all_acct_cnt - acctDTo.del_acct_cnt}">
<input type="hidden" id="maxRegCount" value="${maxRegCountInfo.SETT_ACC_ACCT_REG_PSB_CNT}">

<div class="container">
	<div class="my_wra">
		<div class="menu_top">
			<p class="menu_name">마이페이지</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">정산계좌 관리</p>
			<p class="desc">각 은행에서 <font style="color: red;">빠른조회서비스</font>에 가입하신 후 사용가능한 서비스입니다.</p>
		</div>
		<div class="my_area">
			<div class="my_section">
				<!--계좌 등록 리스트-->
				<section class="account_my">
					<div class="status_my_account">
						<p class="all">전체 ${acctDTo.all_acct_cnt - acctDTo.del_acct_cnt } /</p>
						<p class="normal">정상 ${acctDTo.nor_acct_cnt } /</p>
						<p class="error">오류 ${acctDTo.err_acct_cnt } /</p>
						<p class="ins">점검 ${acctDTo.ins_acct_cnt }</p>
						<button class="register_acct" onclick="pressedBtnRegAcct();">계좌 등록</button>
					</div>
					<ul class="account_my">
						<li class="clearfix clearfix_title">
							<div class="summary_col summary_col_title">
								<div class="col col1">
									<p class="bank">은행명</p>
								</div>
								<div class="col col2">
									<p>계좌번호</p>
								</div>
								<div class="col col3">
									<p>계좌잔액</p>
								</div>
								<div class="col col4">
									<p>정산몰</p>
								</div>
								<div class="col col5">
								</div>
							</div>
						</li>

						<c:forEach items="${acctDTo.acct }" var="acct" varStatus="status">
							<c:if test="${acct.del_yn eq 'N' }">
								<li class="clearfix <c:if test=" ${acct.cust_acct_sts_cd eq 'ERR' }">error
							</c:if>">
							<div class="summary_col">
								<div class="col col1">
									<img src="/assets/images/bank/${acct.bank_cd }.png" alt="">
									<p class="bank">${acct.bank_nm }</p>
								</div>
								<div class="col col2">
									<p>${acct.acct_no }</p>
								</div>
								<div class="col col3">
									<p>
										<c:if test="${not empty acct.acct_prc }">
											<fmt:formatNumber value="${acct.acct_prc}" pattern="#,###" />원</c:if>
										<c:if test="${empty acct.acct_prc }">0원</c:if>
									</p>
								</div>
								<div class="col col4">
									<p>
										<c:if test="${fn:length(acct.mall_l) > 1}"> ${acct.mall_l[0].mall_cd_nm } 외
											${fn:length(acct.mall_l)-1}개</c:if>
										<c:if test="${fn:length(acct.mall_l) == 1}"> ${acct.mall_l[0].mall_cd_nm }
										</c:if>
										<c:if test="${fn:length(acct.mall_l) == 0}"> 없음</c:if>
									</p>
									<c:if test="${fn:length(acct.mall_l) > 0}">
										<div class="all_mall_list">
											<c:forEach items="${acct.mall_l }" var="mall" varStatus="mStatus">
												<p class="mall">
													<img src="/assets/images/member/logo/logo${mall.mall_cd }.png"
														alt="">${mall.mall_cd_nm }</p>
											</c:forEach>
										</div>
									</c:if>
								</div>
								<div class="col col5">
									<c:if test="${not empty acct.acct_prc_mod_ts && acct.cust_acct_sts_cd ne 'INQ' }">
										<img src="/assets/images/main/refresh.png" alt="">
										<p class="date">업데이트 일자: ${acct.acct_prc_mod_ts }</p>
									</c:if>
									<c:if test="${acct.cust_acct_sts_cd eq 'INQ' }">
										<ul class="loading_motion">
											<li class="active"></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
										</ul>
									</c:if>
								</div>
								<c:if test="${acct.cust_acct_sts_cd eq 'INS' }">
									<div class="error_type_2">
										<h1>점검 중 입니다.</h1>
									</div>
								</c:if>
								<c:if test="${acct.cust_acct_sts_cd eq 'ERR' }">
									<div class="error_modal my_error_modal" style="display: none;">
										<h1 style="width: 80%">${acct.acct_inq_rlt_cd_desc }</h1>
										<ul class="error_modal_close">
											<li></li>
											<li></li>
										</ul>
									</div>
								</c:if>

							</div>
							<div class="detail_col">
								<div class="box_detail_acoount">
									<div class="title_dt_acct">
										정산계좌 정보 상세
										<div class="btn_mg_acct">
											<a href="/sub/my/account_edit/${acct.cust_acct_seq_no }"
												class="edit_acct"></a> <a href="javascript:void(0);"
												data-cust_acct_seq_no="${acct.cust_acct_seq_no }"
												data-cust_acct_sts_cd="${acct.cust_acct_sts_cd }"
												class="delete_acct"></a>
										</div>
									</div>
									<div class="list_dt_info">
										<div class="date_detail">
											<p>등록일시 ${acct.reg_ts }</p>
											<p>수정일시 ${acct.mod_ts }</p>
										</div>
										<div class="item_dt_info">
											<span class="lb">은행명</span>
											<p class="txt_dt">${acct.bank_nm }</p>
										</div>
										<c:if test="${not empty acct.bank_id_crypt_val }">
											<div class="item_dt_info">
												<span class="lb">빠른조회<br>은행아이디
												</span>
												<p class="txt_dt">${acct.bank_id_crypt_val }</p>
											</div>
										</c:if>

										<div class="item_dt_info">
											<span class="lb">예금주</span>
											<p class="txt_dt">${acct.dpsi_nm }</p>
										</div>

										<div class="item_dt_info">
											<span class="lb">계좌번호</span>
											<p class="txt_dt">${acct.acct_no }</p>
										</div>

										<c:if test="${not empty acct.ctz_biz_no_crypt_val }">
											<div class="item_dt_info">
												<span class="lb">주민등록<br>(사업자)번호
												</span>
												<p class="txt_dt">${acct.ctz_biz_no_crypt_val }</p>
											</div>
										</c:if>

										<!-- 통화 코드 미정의 실제 미사용 -->
										<c:if test="${not empty acct.crc }">
											<div class="item_dt_info">
												<span class="lb">통화</span>
												<c:forEach items="${codeList }" var="code" varStatus="index">
													<c:if test="${acct.crc eq code.com_cd }">
														<p class="txt_dt">${code.cd_nm }</p>
													</c:if>
												</c:forEach>
											</div>
										</c:if>
										<c:if test="${fn:length(acct.mall_l) > 0 }">
											<div class="item_dt_info">
												<span class="lb">정산<br>판매몰
												</span>
												<p class="txt_dt">
													<c:forEach items="${acct.mall_l }" var="mall" varStatus="index">
														<c:if test="${!index.last }">${mall.mall_cd_nm }, </c:if>
														<c:if test="${index.last }">${mall.mall_cd_nm } </c:if>
													</c:forEach>
												</p>
											</div>
										</c:if>

									</div>
								</div>
							</div>
							</li>
							</c:if>

						</c:forEach>

					</ul>
				</section>

			</div>
		</div>
		<div class="title_bg">
			<p class="desc">제휴사 서비스를 이용 중이신 경우, 등록된 계좌의 삭제가 제한될 수 있습니다.</p>
		</div>
	</div>

</div>

<script>
	$(document).ready(function () {

		// 2020-03-18 계좌 삭제 불가 처리
		if ("${delConfirm}" == "N") {
			$("a.delete_acct").remove();
		}

		$(".delete_acct").click(function () {
			var cust_acct_seq_no = $(this).data("cust_acct_seq_no");
			var cust_acct_sts_cd = $(this).data("cust_acct_sts_cd");

			if (cust_acct_sts_cd == 'INQ') {
				showAlert("조회중인 계좌는 삭제 하실수 없습니다.");
				return false;
			}

			var modalId = showConfirm("계좌를 삭제하시겠습니까?", function () {
				$.ajax({
					url: '/sub/my/account/' + cust_acct_seq_no,
					type: 'delete',
					async: false,
					dataType: 'text',
					contentType: 'application/json',
					success: function (
						data) {
						location.href = "/sub/my/account/";
					},
					error: function (
						res,
						status,
						error) {
						removeModal(modalId);
						showAlert("삭제에 실패 하였습니다.");
					}
				});
			});

		});

		$(".btn_circle").click(function () {
			$(".popup_confirm_edit").show();
			$(".popup_confirm_edit .close_pop,.popup_confirm_edit .close_btn").click(function () {
				$(".popup_confirm_edit").hide();
			});
		});

		if ($(".loading_motion").length > 0) {
			var a = 0;
			setInterval(function () {
				a++;
				$(".loading_motion").each(
					function () {
						var li = $(this).find("li");
						li.eq(a).addClass("active");
						if (a == 13) {
							a = 0;
							$(".loading_motion li")
								.removeClass("active");
							$(".loading_motion li").eq(a)
								.addClass("active");
						}
					});
			}, 150);
		}

		$("li.clearfix").on("click", function () {

			var jqThis = $(this);

			if (jqThis.hasClass("open")) {
				jqThis.removeClass("open");
			} else {
				$("li.clearfix").removeClass("open");
				jqThis.addClass("open");
				jqThis.find(".error_modal").show();
			}
		});

		$("ul.error_modal_close").on("click", function (e) {
			e.stopPropagation();
			$(this).parent().hide();
		});

		$(".col4 p").mouseover(function () {
			var target = $(this).siblings(".all_mall_list");

			if (target.find("p").length > 15) {
				target.find("p").eq(15).remove("img").html("...").css("text-align", "center").nextAll().remove();
			}
			target.show();

		});

		$(".col4 p").mouseout(function () {
			$(this).siblings(".all_mall_list").hide();
		});

	});

	// 계좌 등록 버튼 이벤트
    var pressedBtnRegAcct = function() {
		if(!fnIsRegData($("#currentRegCount").val(), $("#maxRegCount").val())) {
			showAlert("현재 이용 중이신 서비스는<br>계좌 " + $("#maxRegCount").val() + "개까지만 등록이 가능합니다.");
			return;
		}

        location.href="/sub/my/account_register";
    };
</script>