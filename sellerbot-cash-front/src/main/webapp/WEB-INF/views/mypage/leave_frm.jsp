<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"
%><%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  
%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<input type="hidden" name="isPayingUser" id="isPayingUser" value="${isPayingUser}">
<input type="hidden" name="sodeChk" id="sodeChk" value="${sodeChk}"/>
<input type="hidden" name="reqChk" id="reqChk" value="${reqChk}"/>
<div class="container">
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
                    <c:if test="${sessionScope.cust.cust_uses_goods_total_count > 0}">
                        <c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
                            <c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
                                <c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
                                    <c:choose>
                                        <c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
                                            <li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">회원탈퇴</p>
            <p class="desc">다시 한 번 생각해도 꼭 탈퇴하시겠다면 회원 탈퇴 절차를 도와드리겠습니다.</p>
        </div>
        <div class="cs_area">
            <!-- <form id="form" action="/sub/my/leave" method="post"> -->
                <div class="leave_frm">
                    <p class="title">회원탈퇴 신청</p>
                    <div class="item_leave_input">
                        <p class="lb_leave">안내</p>
                        <div class="input_leave">
                            <p class="info_leave">
                                - 셀러봇캐시 통합아이디로 제휴사의 회원 가입 및 서비스를 이용중인 고객님은 제휴서비스 종료 확인 후 탈퇴가 가능합니다. 먼저 서비스이용 중인 제휴사에
                                문의하세요. <a href="/sub/my/use_id"><span>(통합아이디 사용 중인 제휴사 바로가기)</span></a><br />
                                - 제휴사 문의가 끝나셨다면 1:1문의에 "회원탈퇴요청"으로 내용을 남겨주세요. <a href="/sub/my/ask"><span>(1:1문의
                                        바로가기)</span></a><br />
                                - 사용중인 아이디 (<span>${cust.cust_id}</span>)는 탈퇴 후 재사용 및 복구가 불가능합니다. <br />
                                - 탈퇴 후 <strong><u>회원정보 및 개인형 서비스 이용 데이터는 모두 삭제됩니다.</u></strong><br />
                                - 제휴사인 <a href="https://1st.sode.co.kr/"><span>소드원</span></a> 웹사이트에서도 동시에 회원 탈퇴가 진행됩니다. <br />

                                - <span style="color:red;"><strong>회원 탈퇴로 인해 발생한 불이익에 대한 책임은 오로지 회원 본인이 부담하며, 회원 탈퇴 시 회사가 회원에게 제공한 각종 유무상의 혜택은 자동 소멸합니다.</strong>
                                </span><br /><br />
                                
                            </p>
                        </div>
                        <div style="text-align: center;">
                            <input type="checkbox" class="chkbox" name="chkbox_1" id="agree">
                            <label for="agree"> 안내 사항을 모두 확인했으며, 이에 동의합니다.</label> 
                        </div>
                    </div>
                    <div class="item_leave_input">
                        <p class="lb_leave">탈퇴사유</p>
                        <div class="input_leave">
                            <select class="sctbox" name="canc_reas_cd" id="canc_reas_cd">
                                <option value="BLANK">선택해주세요</option>
                                <option value="T13">사업중단(폐업)</option>
                                <option value="T14">업종변경(전업)</option>
                                <option value="T15">다른 아이디로 재가입</option>
                                <option value="T16">타 사이트(유사서비스) 이용</option>
                                <option value="T17">데이터 불만</option>
                                <option value="T18">고객응대 불만</option>
                                <option value="T19">서비스이용료 불만</option>
                                <option value="T20">정보보호 침해 우려</option>
                                <option value="T21">기타</option>
                            </select>
                        </div>
                    </div>
                    <div class="item_leave_input">
                        <p class="lb_leave">상세사유<br>(선택입력)</p>
                        <div class="input_leave">
                            <textarea class="txtarea" name="canc_dti_reas" id="canc_dti_reas" placeholder="셀러봇캐시에게 한마디~"></textarea>
                        </div>
                    </div>
                    <div class="item_leave_input">
                        <p class="lb_leave">휴대폰 본인 인증<br>(대표자)</p>
                        <div class="input_leave">
                            <div class="input_leave_content" id="certBefore">
                                <h1>대표자명</h1>
                                <h2>${cust.ceo_nm}</h2><a class="confirm_input_leave" id="certPopup" href="#">본인인증
                                    하기</a>
                            </div>
                            <div class="input_leave_complete" id="certAfter">
                                <h1>대표자명</h1>
                                <h2>${cust.ceo_nm}</h2><a class="confirm_input_leave_complete" href="#">본인인증 완료</a>
                                <div class="input_leave_complete_ph">
                                    <select class="input_leave_complete_sel" id="certPhoneNum1">
                                        <option value="010">010</option>
                                        <option value="011">011</option>
                                        <option value="016">016</option>
                                        <option value="017">017</option>
                                        <option value="018">018</option>
                                        <option value="019">019</option>
                                    </select>
                                    <div class="input_leave_complete_ph_box_1">
                                        <span id="certPhoneNum2"></span>
                                    </div>
                                    <div class="input_leave_complete_ph_box_2">
                                        <span id="certPhoneNum3"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item_leave_input last">
                        <input class="certInfo" type="hidden" id="CP_CD" />
                        <input class="certInfo" type="hidden" id="TX_SEQ_NO" />
                        <input class="certInfo" type="hidden" id="RSLT_CD" name="rlt_cd" />
                        <input class="certInfo" type="hidden" id="RSLT_MSG" name="rlt_msg" />
                        <input class="certInfo" type="hidden" id="RSLT_NAME" />
                        <input class="certInfo" type="hidden" id="RSLT_BIRTHDAY" name="req_birthday" />
                        <input class="certInfo" type="hidden" id="RSLT_SEX_CD" name="req_gender" />
                        <input class="certInfo" type="hidden" id="RSLT_NTV_FRNR_CD" name="req_nation" />
                        <input class="certInfo" type="hidden" id="DI" name="di_val" />
                        <input class="certInfo" type="hidden" id="CI" name="ci_val" />
                        <input class="certInfo" type="hidden" id="CI_UPDATE" />
                        <input class="certInfo" type="hidden" id="TEL_COM_CD" name="req_telcmmcd" />
                        <input class="certInfo" type="hidden" id="TEL_NO" name="req_ceph_no" />
                        <input class="certInfo" type="hidden" id="RETURN_MSG" name="retrunmsg" />
                        <div class="leave_confirm_btn">회원탈퇴</div>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>
    <div class="leave_popup">
        <div class="leave_popup_all leave_popup_1">
            <div class="leave_close_btn" id="btnCloseForPopup">
                <div></div>
                <div></div>
            </div>
            <c:choose>
                <c:when test="${isPayingUser eq 'Y'}">
                    <h1>정말 탈퇴하시겠어요?</br>탈퇴 시에는 이용 중인 이용권이 해지됩니다.</h1>
                </c:when>
                <c:otherwise>
                    <h1>정말 셀러봇캐시 회원탈퇴를 하시겠습니까?</h1>
                </c:otherwise>
            </c:choose>
            <ul class="leave_popup_btn">
                <li class="leave_popup_btn_no"><a href="javascript:return false;">아니오</a>
                <li>
                <li class="leave_popup_btn_yes"><a href="javascript:return false;">예</a>
                <li>
            </ul>
        </div>
        <div class="leave_popup_all leave_popup_2">
            <div class="leave_close_btn" onclick="location.href='/sub/my/use_id'">
                <div></div>
                <div></div>
            </div>
            <h1>셀러봇캐시 아이디와 연동된 모든 서비스에서<br />탈퇴하셔야 셀러봇캐시 회원탈퇴가 가능합니다.</h1>
            <ul class="leave_popup_btn">
                <li class="leave_popup_2_btn"><a href="/sub/my/use_id">이용 중인 서비스 바로가기</a></li>
            </ul>
        </div>
        <div class="leave_popup_all leave_popup_3">
            <div class="leave_close_btn" id="btnCloseForPopup3">
                <div></div>
                <div></div>
            </div>
            <h1>이미 회원탈퇴를 신청했습니다. <br />회원탈퇴완료까지 2~3일이 소요될 수 <br /> 있으며, 결과는 메일로 발송해 드립니다. 
            </h1>
            <ul class="leave_popup_btn">
                <li class="leave_popup_3_btn">확인</li>
            </ul>
        </div>
        <div class="leave_popup_black"></div>
    </div>
</div>
<c:if test="${sessionScope.cust.cust_uses_goods_total_count > 0}">
<script src="/assets/js/my_menu.js"></script>
</c:if>
<script>
    var cert = false;
    function checkCertInfo(certName, certPhone) {
        var orgName = "${cust.ceo_nm}";
        var orgPhone = "${cust.ceo_no}";
        if (orgPhone == certPhone) {
            $("#certBefore").hide();
            $("#certPhoneNum1").val("${fn:substring(cust.ceo_no,0,3)}");
            $("#certPhoneNum1").prop("disabled", true);
            $("#certPhoneNum2").html(${fn: substring(cust.ceo_no, 3, 7)});
            $("#certPhoneNum3").html(${fn: substring(cust.ceo_no, 7, -1)});
            $("#certAfter").show();
            cert = true;
        } else {
            showAlert("인증정보가 회원정보와 일치하지 않습니다.");
            $(".certInfo").val("");
        }
    }
    $(document).ready(function () {
        if ('${fn:length(batchProTgtList) }' != "0" ) {
            var svcDescp = '${loanBatchProTgtInfo.batchProTgt_list[0].svc_descp }';
            showAlert(svcDescp + " 대출을 이용 중인 고객님은 대출을 사용하시는 <br>기간 동안 탈퇴가 불가능합니다.", function () {
                showLoadingModal();
                location.href = "/";
            });
        }

        if ('${scbLoanCnt}' > 0) {
            showAlert("SC 제일은행 대출을 이용 중인 고객님은 대출을 <br>사용하시는 기간 동안 탈퇴가 불가능합니다.", function () {
                showLoadingModal();
                location.href = "/";
            });
        }

        if (${svcChk}){
            $(".leave_popup").css("display", "flex");
            $(".leave_popup_2").css("display", "block");
        }
        
        // 소드원 회원 여부 확인
        var custSvcSodeYn = $("#sodeChk").val();
        // 탈퇴 요청 여부
        var reqChk = $("#reqChk").val();

        if(reqChk == 'Y'){
            $(".leave_popup").css("display", "flex");
            $(".leave_popup_3").css("display", "block");
        }
        $("#certPopup").click(function () {
            var popOption = "width=370, height=360, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
            window.open("/authPopup/phone_popup1", "", popOption);
        });
        

        $(".leave_confirm_btn").click(function () {
            reqChk = $("#reqChk").val();
            var isAgreeChecked = $('#agree').is(':checked');
            
            // 탈퇴신청 여부 확인
            if(reqChk == 'Y'){
                $(".leave_popup").css("display", "flex");
                $(".leave_popup_3").css("display", "block");
                return;
            }

            // 안내사항 동의 여부 확인
            if(!isAgreeChecked){
                showAlert("탈퇴 유의사항 동의 후 회원탈퇴를 신청해주세요.");
                return;
            }
            
            // 본인인증 여부 확인
            if (cert) {
                $(".leave_popup").css("display", "flex");
                $(".leave_popup_1").css("display", "block");
            } else {
                showAlert("본인인증 후 탈퇴 가능합니다.");
            }

        });

        $(".leave_popup_btn_no").click(function () {
            $(".leave_popup_1").css("display", "none");
        });

        $(".leave_popup_btn_yes").click(function () {
            if ($("#canc_reas_cd").val() != 'BLANK') {
                var params = {
                    "canc_reas_cd": $("#canc_reas_cd").val(),
                    "canc_dti_reas": $("#canc_dti_reas").val()
                };
            } else {
                closePopup();
                showAlert("탈퇴 사유를 선택해주세요.");
                return;
            }
           
            if(custSvcSodeYn == 'Y'){
                $.ajax({
                    url: '/sub/my/sodeCust/terminateReq',
                    type: 'post',
                    async: false,
                    success: function (response) {
                        $("#reqChk").val("Y");
                        closePopup();
                        showAlert("회원탈퇴가 신청되었습니다.  만약 제휴사 연동으로 탈퇴가 어려울 경우, 개별 문자를 발송해 드립니다.  그 동안 셀러봇캐시를 이용해주셔서 감사합니다.", function () {
                            location.href = "/logout";
                        });
                    },
                    error: function (error) {
                        closePopup();
                        showAlert("제휴사 사이트 점검중으로 탈퇴신청이 원활하지 않습니다. 불편하시더라도 잠시 후 다시 탈퇴신청을 해주세요.");
                    }
                });

            }else{
                $.post("/sub/my/leave", $.param(params), function (res) {
                location.href = "/sub/my/leaveSuccess";
                })
                .fail(function (response) {
                    closePopup();
                    showAlert("요청이 실패하였습니다.");
                });
            } 
           
        });

        $(".leave_popup_btn_no , .leave_popup_3_btn").click(function () {
            closePopup();
        });
        $("#btnCloseForPopup , #btnCloseForPopup3").click(function () {
            closePopup();
        });
        
        var closePopup = function() {
            $(".leave_popup").css("display", "none");
            $(".leave_popup_all").css("display", "none");
        };
        
    });
</script>