<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="container">
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">회원정보</p>
            <p class="desc">회원님의 소중한 개인정보를 안전하게 보호하기 위해 비밀번호를 한번 더 입력해주세요.</p>
        </div>
        <div class="my_area">
            <div class="my_section">
                <div class="member_frm_box mb_pw_box">
					<div class="item_mb">
                        <span class="lb">이메일(ID)</span>
                        <div class="input_box">
                            <p class="value">${cust.cust_id}</p>
                        </div>
                    </div>
                    <div class="item_mb">
                        <span class="lb">비밀번호</span>
                        <div class="input_box">
                            <input type="password" class="textbox_mb" id="password" required="required">
                            <p class="error" id="passwd_error">*비밀번호를 정확히 입력하여 주세요.</p>
                        </div>
                    </div>
					<div class="item_mb">
                        <p class="msg_time_alarm">본인확인을 위하여 비밀번호를 입력하여 주시기 바랍니다.</p>
                    </div>
                </div>
            </div>
            <div class="btn_right">
                <button class="btn_circle">확인</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(".btn_circle").click(function () {

        if (!isVali()) { return false; }

        $.ajax({
			url: '/sub/my/check_pw',
			data: { password: $("#password").val() },
			async: false,
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				if (data.result) {
					location.href = "/sub/my/edit_info";
				} else {
					var errorCode = data.errorCode;
					if (errorCode == "AUTH-4303") {
						showAlert("해당 사용자가 존재하지 않습니다.");
					} else if (errorCode == "AUTH-4304") {
						showAlert("비밀번호가 일치하지 않습니다. <br> 다시한번 확인하여 주시기 바랍니다.");
					} else {
						showAlert("로그인 에러");
					}
				}
			},
			error: function (response, status, error) {
				showAlert("서버 에러.");
			}
		});

    });
</script>