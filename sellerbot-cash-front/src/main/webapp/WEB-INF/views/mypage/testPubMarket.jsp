<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="/assets/js/select2/select2.min.js"></script>
<script src="/assets/js/mngMall.js?ver=20221117_01"></script>

<style>
	.my_section {
		max-width: 51rem !important;
	}

	@media (max-width: 1024px) {
		.my_area .market_box {
			float: left;
			margin: 1.625em 0 0;
			margin-top: 0;
		}

		/* .my_area .choiced_mk_box {
			padding: 0 0 0 7em;
		} */

		.my_area .choiced_mk_box {
			margin-left: 3rem;
			margin-top: -5rem;
			padding: 0;
		}

		.list_choiced_mk {
			padding: 0;
		}
	}

	@media (max-width: 990px) {
		.my_section {
			max-width: 47rem !important;
		}

		.my_area .choiced_mk_box {
			padding: 0 0 0 4em;
		}
	}

	@media (max-width: 720px) {
		.market_box_find {
			width: 100%;
		}

		.my_area .market_box {
			width: 100%;
			float: unset;
		}

		.my_area .choiced_mk_box {
			margin: 0;
			padding: 0;
		}

		.choiced_mk_box .status {
			margin-top: 2rem;
		}
	}
</style>

<input type="hidden" id="currentRegCount" value="${custMall.all_mall_cnt - custMall.del_mall_cnt - custMall.mall008.dup_mall_cnt}">
<input type="hidden" id="maxRegCount" value="${maxRegCountInfo.SALE_MALL_REG_ID_PSB_CNT}">
<div class="container">
	

	<div class="my_wra">
		<div class="title_bg">
			<p class="title">판매몰 관리</p>
			<p class="desc">등록된 판매몰을 수정, 삭제, 추가 할 수 있습니다..</p>
		</div>
		<div class="my_area">
			<div class="my_section">
				<div class="market_choice clearfix">
					<div class="market_box_find">
						<div class="ui-widget">
							<input placeholder="판매몰 검색" class="market_box_find_text_area">
						</div>
					</div>
					<div class="market_box">
						<div class="search_market">
						</div>
						<div class="group_list_mall" data-type-mk="mk_type1">
							<p class="type_market open"><span>오픈마켓</span> <img
									src="/assets/images/member/btn_arrows.png" alt=""></p>
							<ul class="scrollblack mall_lists_1">
								<c:set var="cnt" value="1" />
								<c:forEach items="${mallList}" var="mall" varStatus="status">
									<c:if test="${mall.mall_typ_cd eq 'OPM' && mall.mall_sts_cd eq 'NOR' }">
										<li name="open_${cnt }" id="mallList${mall.mall_cd}">
											<c:if test="${mall.mallLG_file.stor_file_nm != null }">
												<img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
											</c:if>
											<c:if test="${mall.mallLG_file.stor_file_nm == null }">
												<img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
											</c:if>
											<span name="open_${mall.mall_cd }_tag">${mall.mall_nm }</span>
											<input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
												class="sub_mall_cert_1st_nm" />
											<input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
												class="sub_mall_cert_2nd_nm" />
											<input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
												class="mall_cert_2nd_passwd_nm" />
											<input type="hidden" value="${mall.sub_mall_cert_step }"
												class="sub_mall_cert_step" />
											<input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
											<input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
											<input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
												class="sub_mall_auth_1st_esse_yn" />
											<input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
												class="sub_mall_auth_2nd_esse_yn" />
											<input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
											<input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
											<input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
											<input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
											<input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
											<input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
											<input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
											<input type="hidden" value="${mall.help_info }" class="help_info" />
											<input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
											<input type="hidden" value="${mall.video_url }" class="video_url" />
											<input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
											<input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
											<c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
												varStatus="typStatus">
												<input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
													value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
											</c:forEach>
										</li>
										<c:set var="cnt" value="${cnt + 1 }" />
									</c:if>
								</c:forEach>
							</ul>
						</div>
						<div class="group_list_mall" data-type-mk="mk_type2">
							<p class="type_market"><span>소셜커머스</span> <img src="/assets/images/member/btn_arrows.png"
									alt=""></p>
							<ul class="scrollblack mall_lists_2">
								<c:set var="cnt" value="1" />
								<c:forEach items="${mallList}" var="mall" varStatus="status">
									<c:if test="${mall.mall_typ_cd eq 'SNM' && mall.mall_sts_cd eq 'NOR' }">
										<li name="soci_${cnt }" id="mallList${mall.mall_cd}">
											<c:if test="${mall.mallLG_file.stor_file_nm != null }">
												<img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
											</c:if>
											<c:if test="${mall.mallLG_file.stor_file_nm == null }">
												<img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
											</c:if>
											<span name="soci_${mall.mall_cd }_tag">${mall.mall_nm }</span>
											<input type="hidden" value="${mall.sub_mall_cert_1st_nm }"
												class="sub_mall_cert_1st_nm" />
											<input type="hidden" value="${mall.sub_mall_cert_2nd_nm }"
												class="sub_mall_cert_2nd_nm" />
											<input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }"
												class="mall_cert_2nd_passwd_nm" />
											<input type="hidden" value="${mall.sub_mall_cert_step }"
												class="sub_mall_cert_step" />
											<input type="hidden" value="${mall.mall_cd }" class="mall_cd" />
											<input type="hidden" value="${mall.passwd_step }" class="passwd_step" />
											<input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }"
												class="sub_mall_auth_1st_esse_yn" />
											<input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }"
												class="sub_mall_auth_2nd_esse_yn" />
											<input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
											<input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
											<input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
											<input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
											<input type="hidden" value="${mall.mall_nm }" class="mall_nm" />
											<input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
											<input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
											<input type="hidden" value="${mall.help_info }" class="help_info" />
											<input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
											<input type="hidden" value="${mall.video_url }" class="video_url" />
											<input type="hidden" value="${mall.mall_typ_cd }" class="mall_typ_cd" />
											<input type="hidden" value="${mall.mall_sts_cd }" class="mall_sts_cd" />
											<c:forEach items="${mall.mall_cert_typ}" var="mallTyp"
												varStatus="typStatus">
												<input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }"
													value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ" />
											</c:forEach>
										</li>
										<c:set var="cnt" value="${cnt + 1 }" />
									</c:if>
								</c:forEach>
							</ul>
						</div>
					</div>

					<div id="usable_store">
						<div class="usable_store_header">
							<h3>이용 가능 판매몰</h3>
							<p>판매몰 계정 등록 후 바로 데이터 조회가 가능한 판매몰들이에요~</p>
						</div>
						<div class="usable_store_body">
							<ul></ul>
						</div>
						<div class="usable_store_footer">
							<p>판매몰은 계속 추가되고 있으며, 원하시는 판매몰이 있으실 경우 <a href="/sub/my/ask" target="_blank">1:1문의</a>를 통해 요청해주시기 바랍니다.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- init시 수정 대상 몰 정보   -->
<input type="hidden" id="custMallSeqNo" value="${cust_mall_seq_no }">
<script>

	$(document).ready(function () {

		// 2019-07-17 스크롤 유지
		$(".modal_alarm, .market_box").off("mouseover");
		$(".modal_alarm, .market_box").off("mouseout");

		var clickM;
		var img_mall_name;
		var img_mall_text;
		// 판매몰 등록
		$(".group_list_mall li, #usable_store li, #pre_store li").on("click", function () {

		});

		$(function () {
			var all_mall = new Array();
			$(".market_choice .mall_nm").each(function(){ 
				if ($(this).parent().find(".mall_sts_cd").val() == 'NOR') {
					all_mall.push($(this).val()) 
				}
			});

			$(".market_box_find_text_area").autocomplete({
				source: all_mall,
				select: function (event, ui) {
					$(".popup_account.type1").addClass("active");
					for (var i = 0; i < $('.group_list_mall li').length; i++) {
						if (ui.item.value == $('.group_list_mall li span').eq(i).text()) {
							var img_tags = $('.group_list_mall li span').eq(i).prev('img').attr('src');
							clickM = $('.group_list_mall li').eq(i);
							clickM.click();

						}
					}
				},
				focus: function (event, ui) {
					return false;
				}
			});
		});

		//select2
		function format(option) {
			var originalOption = option.element;
			var valOption = $(originalOption).val();
			if (!option.id) return option.text; // optgroup
			return "<div class='mall_logo'><img src='/assets/images/member/logo/" + valOption + ".png'/>" + option.text + "</div>";
		}

		$(".select_mall").select2({
			placeholder: "판매몰 검색",
			allowClear: true,
			templateResult: format,
			templateSelection: format,
			escapeMarkup: function (m) {
				return m;
			}
		});

		$(".select_mall").on("select2:select", function (e) {
			var selectMImg = $(".select2-selection__rendered .mall_logo img").attr("src");
			$(".sh_mall_btn").click(function () {
				$(".popup_account.type1").addClass("active");
				$(".popup_account.type1 .logo_area_account img").attr('src', selectMImg);
				$(".popup_account.type1 .close_pop").click(function () {
					$(".popup_account.type1").removeClass("active");
					return false;
				});
			});
		});


		// 전체
		$("#filterAll").on("click", function () {
			$(".group_mall").show();
			$(".group_mall_error").show();
			$(".group_mall_warning").show();
		});
		// 정상
		$("#filterPass").on("click", function () {
			$(".group_mall").show();
			$(".group_mall_error").hide();
			$(".group_mall_warning").hide();
		});
		// 점검
		$("#filterWarning").on("click", function () {
			$(".group_mall").hide();
			$(".group_mall_error").hide();
			$(".group_mall_warning").show();
		});
		// 에러
		$("#filterError").on("click", function () {
			$(".group_mall").hide();
			$(".group_mall_error").show();
			$(".group_mall_warning").hide();
		});

		//init
		(function () {
			if (nonNull($("#custMallSeqNo").val())) {
				fn_updateData($("#custMallSeqNo").val(), '', '', '');
			}

			// 기본 전체
			$(".group_mall").show();
			$(".group_mall_error").show();
			$(".group_mall_warning").show();

		})();
		
	});

	/**
	 * 이용 가능 판매몰 목록 추가
	 */
	 if( $('#usable_store').length > 0 ) {
		$('.market_box li').each(function(){
			const tt = $(this);
			var mall_typ_cd = tt.children(".mall_typ_cd").val();
			var mall_sts_cd = tt.children(".mall_sts_cd").val();

			if(mall_typ_cd != 'OCM') {
				const marketBoxLi = $(this).clone();
				$('#usable_store .usable_store_body ul').append(marketBoxLi);
			}
		});
	}
	
</script>