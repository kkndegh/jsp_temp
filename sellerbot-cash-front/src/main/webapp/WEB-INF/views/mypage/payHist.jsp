<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="/assets/js/common/util.js"></script>
<script src="/assets/js/payHist.js?ver=20200827_01"></script>

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-pagination.js"></script>
<script src="/assets/js/tui-grid.js"></script>

<div class="container">
    <!-- my_wra start -->
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">결제정보</p>
            <p class="desc">셀러봇캐시가 준비한 서비스들을 제한 없이 누려보세요!</p>
        </div>
    </div>
    <!-- my_wra end -->
    <!-- container_wrapper start -->
    <div class="container_wrapper">
        <!-- tree_menu_ul start -->
        <!-- <ul id="tree_menu_ul"> -->
        <ul id="two_menu_ul">
            <li onclick="location.href='/sub/my/paidInfo'">
                <a>이용정보 요약</a>
            </li>
            <li class="blue_active2" onclick="location.href='/sub/my/payHist'">
                <a>결제/취소 내역</a>
            </li>
            <!-- <li onclick="location.href='/sub/my/poiHist'">
                <a>S포인트 내역</a>
            </li> -->
        </ul>
        <!-- tree_menu_ul end -->
        <!-- two_menu_ul start -->
        <!-- <ul id="two_menu_ul">
            <li class="blue_active2">이용 정보 요약</li>
            <li>결제 내역</li>
        </ul> -->
        <!-- two_menu_ul end -->
        <h1 class="mobile_asset mobile_title">결제내역</h1>
        <!-- time_select_wrapper start -->
        <div class="time_select_wrapper">
            <!-- <div class="time_select_item_1">
                <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                    <input type="text" id="tui-date-picker-target" aria-label="Date-Time">
                    <span class="tui-ico-date"></span>
                </div>
                <div id="tui-date-picker-container" style="margin-top: -1px;"></div>
            </div>
            <h1 class="middle_text"> ~ </h1>
            <div class="time_select_item_2">
                <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                    <input type="text" id="tui-date-picker-target2" aria-label="Date-Time">
                    <span class="tui-ico-date"></span>
                </div>
                <div id="tui-date-picker-container2" style="margin-top: -1px;"></div>
            </div> -->
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="datepicker-input-start" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
            </div>
            <div class="datepicker-cell" id="datepicker-month-start" style="margin-top: -1px;"></div>
            <h1 style="display:inline-block;display: inline-block;vertical-align: middle;font-size: 1rem;"> ~ </h1>
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="datepicker-input-end" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
            </div>
            <div class="datepicker-cell" id="datepicker-month-end" style="margin-top: -1px;"></div>
            <button class="search_button" onclick="search(1);">조회</button>
        </div>
        <!-- time_select_wrapper end -->
        <!-- <script src="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.js"></script> -->
        <script>
            // var DatePicker = tui.DatePicker;
            // var container = document.getElementById('tui-date-picker-container');
            // var target = document.getElementById('tui-date-picker-target');
            // var instance = new DatePicker(container, {
            //     input: {
            //         element: target
            //     },
            //     language: 'ko'
            // });

            // var container2 = document.getElementById('tui-date-picker-container2');
            // var target2 = document.getElementById('tui-date-picker-target2');
            // var instance = new DatePicker(container2, {
            //     input: {
            //         element: target2
            //     },
            //     language: 'ko'
            // });
            // 데이트피커 설정
        </script>
        <!-- m_sec1 start -->
        <div id="m_sec1">
            <div class="row">
                <h1 style="margin-bottom:1rem" class="m_sec_title">결제 내역</h1>
            </div>
            <table id="useing_service">
                <thead>
                    <th>기준일시</th>
                    <th>결제수단</th>
                    <th>결제번호</th>
                    <th>내역</th>
                    <th>금액</th>
                    <th>
                        <div class="row">
                            <h1>상태</h1><img class="q_icon" src="/assets/images/icon/q.png" alt="">
                            <div class="q_popup">
                                <h1 style="text-align:left">
                                    결제 성공 : 정상 결제되었어요.<br />취소 : 결제가 취소되었어요.<br />실패 : 결제가 실패했으니 다시 결제해주세요.
                                </h1>
                                <img onclick="popupClose2()" class="close_popup" src="/assets/images/icon/x_white.png"
                                    alt="">
                            </div>
                        </div>
                    </th>
                    <th>취소일시</th>
                    <th>관리</th>
                </thead>
                <tbody id="tableBodyForPC">
                    <!-- <tr>
                        <td>2020/04/13 14:00</td>
                        <td>신용카드</td>
                        <td>0000000005</td>
                        <td>셀러봇캐시로니봇(정기결제)</td>
                        <td>9,600원</td>
                        <td>-</td>
                        <td>
                            <span onclick="activeActive()" class="bold underLine popup_type_grid">매출전표</span>
                            <span onclick="activeActive()" class="bold underLine popup_type_grid">영수증</span>
                        </td>
                    </tr> -->
                </tbody>
            </table>
            <div style="margin-top:0;" class="grid_noData">
                <h1>결제 내역이 없습니다.</h1>
            </div>
            <div class="table_pager_wrapper">
                <div id="grid-pagination-container" class="tui-pagination"></div>
            </div>
        </div>
        <!-- m_sec1 end -->
        <!-- mobile_table_type_ac start -->
        <div class="mobile_table_type_ac">
            <div class="m_sevice_mb" id="m_sec1_mb">
                <!-- <ul class="mb_table_service">
                    <li class="gray_bg">
                        <h1>기준일시</h1>
                        <h2 class="blue_liner0">2020/04/14 14:00</h2>
                        <img class="ac_menu_icon" src="/assets/images/icon/icon_top.png" alt="">
                    </li>
                    <li>
                        <h1>결제수단</h1>
                        <h2>신용카드</h2>
                    </li>
                    <li>
                        <h1>결제번호</h1>
                        <h2>0000000000005</h2>
                    </li>
                    <li>
                        <h1>내역</h1>
                        <h2>셀러봇캐쉬로니봇(정기결제)</h2>
                    </li>
                    <li>
                        <h1>금액</h1>
                        <h2>9,600원</h2>
                    </li>
                    <li>
                        <h1>상태</h1>
                        <h2>-</h2>
                    </li>
                    <li>
                        <h1>관리</h1>
                        <div class="s_mb_btn_row">
                            <button onclick="activeActive()" type="button" class="change_payment_btn">매출전표</button>
                        </div>
                    </li>
                </ul> -->
            </div>
            <div class="table_pager_wrapper mobile_asset">
                <div id="grid-pagination-container-mb" class="tui-pagination"></div>
            </div>
        </div>
        <!-- mobile_table_type_ac end -->
    </div>
    <!-- container_wrapper end -->
</div>
<style>
    .my_wra {
        padding-top: 5em;
        min-height: auto;
        overflow-x: hidden;
    }
</style>
<style>
    #useing_service thead tr th {
        width: auto !important;
    }

    #useing_service tbody tr td {
        width: auto !important;
    }

    .bold {
        font-weight: bold;
        padding: 0 0.3rem;
        cursor: pointer;
    }

    .underLine {
        text-decoration: underline;
    }
</style>
<style>
    .time_select_wrapper {
        width: 100%;
        text-align: right;
    }

    .middle_text {
        display: inline-block;
        font-size: 1rem;
    }

    .time_select_item_1 {
        display: inline-block;
    }

    .time_select_item_2 {
        display: inline-block;
    }

    .tui-datepicker-input {
        width: 120px !important;
    }

    .search_button {
        display: inline-block;
        vertical-align: middle;
        background-color: #2DABE8;
        color: #fff;
        width: 80px;
        height: 30px;
        border-radius: 4px;
        margin-bottom: 10px;
    }

    div.table_pager_wrapper {}

    div.table_pager_wrapper ul.table_pager_list {
        text-align: center;
        font-size: 0;
    }

    div.table_pager_wrapper ul.table_pager_list li {
        display: inline-block;
        vertical-align: middle;
        font-size: 1rem;
        width: 23px;
        cursor: pointer;
    }

    div.table_pager_wrapper ul.table_pager_list li:nth-of-type(1) {
        background: url(/assets/images/icon/icon_left_2.png) no-repeat;
        background-size: 14px;
        background-position: center;
        height: 15px;
        margin-right: 5px;
    }

    div.table_pager_wrapper ul.table_pager_list li:nth-of-type(2) {
        background: url(/assets/images/icon/icon_left.png) no-repeat;
        background-size: 7px;
        background-position: center;
        height: 15px;
        margin-right: 5px;
    }

    div.table_pager_wrapper ul.table_pager_list li:nth-of-type(8) {
        background: url(/assets/images/icon/icon_right.png) no-repeat;
        background-size: 7px;
        background-position: center;
        height: 15px;
        margin-left: 5px;
    }

    div.table_pager_wrapper ul.table_pager_list li:nth-of-type(9) {
        background: url(/assets/images/icon/icon_right_2.png) no-repeat;
        background-size: 14px;
        background-position: center;
        height: 15px;
        margin-left: 5px;
    }

    div.table_pager_wrapper ul.table_pager_list li img {}

    .table_pager_list .active {
        color: #2DABE8;
    }

    div#m_sec1 table#useing_service {
        margin-bottom: 0;
    }

    .table_pager_wrapper {
        margin-bottom: 5rem;
        margin-top: 2.5rem;
    }

    .pop_up_1 {
        background-color: #fff;
        z-index: 50;
        display: none;
        width: auto;
        min-width: 35rem;
        max-width: 90%;
    }

    .popup_contents h1 {
        padding: 1rem;
    }

    .index_popup_wrap {
        display: none;
    }

    div#m_sec1 div.row {
        margin-bottom: 0;
        font-size: 0;
    }

    div#m_sec1 div.row h1 {
        margin-bottom: 0;
        font-size: 1rem;
        display: inline-block;
        vertical-align: middle;
        margin-top: -3px;
    }

    div.row div.q_popup {
        position: absolute;
    }

    #m_sec1 .q_icon {}

    .tui-datepicker-input {
        vertical-align: middle;
    }

    .mobile_title {
        display: none;
    }

    .mobile_asset {
        display: none;
    }

    @media(max-width:880px) {
        .mobile_asset {
            display: block;
        }

        .mobile_title {
            display: block;
            padding: 0 2rem;
            font-size: 1.1rem;
            font-weight: bold;
        }

        .time_select_wrapper {
            text-align: left;
            margin-left: 2rem;
        }

        .m_sevice_mb ul.mb_table_service li.gray_bg::after {
            content: "";
            position: absolute;
            right: 3rem;
            background: #FFB800;
            opacity: 0.3;
            height: 1rem;
            top: 1.4rem;
            width: 10rem;
        }
    }
</style>
<style>
    .tui-datepicker-input>input {
        width: 100%;
        height: 100%;
        padding: 6px 27px 6px 10px;
        font-size: 12px;
        line-height: 14px;
        vertical-align: top;
        border: 0;
        color: #333;
        border-radius: 3rem;
    }

    .tui-datepicker-input {
        position: relative;
        display: inline-block;
        width: 120px;
        height: 2.5rem;
        vertical-align: top;
        border: 1px solid #ddd;
        border-radius: 3rem;
    }

    .datepicker-cell {
        display: inline-block;
    }

    .tui-datepicker-input.tui-has-focus {
        vertical-align: middle;
        /* border: 0; */
    }

    .tui-datepicker {
        border: 1px solid #aaa;
        background-color: white;
        position: absolute;
        margin-left: -11rem;
        margin-top: 1rem;
        z-index: 999;
    }

    /*paging*/
    .tui-pagination {
        padding-top: 1rem;
    }

    .tui-pagination .tui-is-selected,
    .tui-pagination strong {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-is-selected:hover {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-first-child.tui-is-selected {
        border: 1px solid #a4a4a4;
    }
</style>
<script>
    var today = Date.now();

    var startMonthPicker = new tui.DatePicker('#datepicker-month-start', {
        date: today,
        language: 'ko',
        type: 'month',
        input: {
            element: '#datepicker-input-start',
            format: 'yyyy-MM'
        },
        selectableRanges: [
            [new Date(0), today]
        ]
    });

    var endMonthPicker = new tui.DatePicker('#datepicker-month-end', {
        date: today,
        language: 'ko',
        type: 'month',
        input: {
            element: '#datepicker-input-end',
            format: 'yyyy-MM'
        },
        selectableRanges: [
            [new Date(0), today]
        ]
    });
</script>
<script>
    $(".table_pager_list li").click(function () {
        if ($(this).text() == "1" || $(this).text() == "2" || $(this).text() == "3" || $(this).text() == "4" || $(this).text() == "5") {
            $(this).addClass("active");
            $(".table_pager_list li").not($(this)).removeClass("active");
        }
    });

    function activeActive(obj) {
        // $(".index_popup_wrap").css("display", "flex");
        // ex. https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid=INIAPICARDBILlseller20200513202428820042&noMethod=1
        var popOption = "width=420, height=540, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
        var url = "https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid=";
        var tid = $(obj).attr("value");
        var callUrl = url + tid + "&noMethod=1";
        
        window.open(callUrl, "", popOption);
    }

    function popupClose() {
        $(".index_popup_wrap").css("display", "none");
    }

    function popupClose2() {
        $(".q_popup").css("display", "none");
    }

    $("#m_sec1 .q_icon").click(function () {
        $(".row div.q_popup").css("display", "block");
    });
</script>
<div class="index_popup_wrap" style="display: none;">
    <div class="pop_up_1 pop_up_all pop_up_font-size" style="display: block;">
        <div class="popup_top_title">
            <h1></h1>
            <div onclick="popupClose()" class="add_pooup_close">
                <div></div>
                <div></div>
            </div>
        </div>
        <div class="popup_contents">
            <h1>해당 텍스트를 지우고 컨텐츠를 <br />넣어주시면 됩니다. 가로사이즈 최대 : 90%</h1>
        </div>
        <button type="button" class="btn_confirm_terms index_popup_close">확인</button>
    </div>
    <div class="index_background_black"></div>
</div>
<script>
    $(document).ready(function () {
        initPayHist();
        search(1);
    });
</script>