<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="/assets/js/common/util.js"></script>

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-pagination.js"></script>
<script src="/assets/js/tui-grid.js"></script>

<div class="container">
    <!-- my_wra start -->
    <div class="my_wra">
        <div class="menu_top">
            <p class="menu_name">마이페이지</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
                    <c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
					<c:forEach var="menuInfo" items="${sessionScope.menu}" varStatus="status">
						<c:if test="${menuInfo.MENU_NM eq '마이페이지'}">
							<c:forEach var="subMenuInfo" items="${menuInfo.SUB_MENU_LIST}">	
								<c:choose>
									<c:when test="${path eq subMenuInfo.SUB_MENU_PATH}">
										<li class="focus"><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:when>
									<c:otherwise>
										<li><a href="${subMenuInfo.SUB_MENU_PATH}">${subMenuInfo.SUB_MENU_NM}</a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
					</c:forEach>
                </ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">결제정보</p>
            <p class="desc">셀러봇캐시가 준비한 서비스들을 제한 없이 누려보세요!</p>
        </div>
    </div>
    <!-- my_wra end -->
    <!-- container_wrapper start -->
    <div class="container_wrapper">
        <!-- tree_menu_ul start -->
        <ul id="tree_menu_ul">
            <li onclick="location.href='/sub/my/paidInfo'">
                <a>이용정보 요약</a>
            </li>
            <li onclick="location.href='/sub/my/payHist'">
                <a>결제 내역</a>
            </li>
            <li class="blue_active2" onclick="location.href='/sub/my/poiHist'">
                <a>S포인트 내역</a>
            </li>
        </ul>
        <!-- tree_menu_ul end -->
        <!-- two_menu_ul start -->
        <!-- <ul id="two_menu_ul">
            <li class="blue_active2">이용 정보 요약</li>
            <li>결제 내역</li>
        </ul> -->
        <!-- two_menu_ul end -->
        <h1 class="mobile_asset mobile_title">결제내역</h1>
        <!-- tui grid -->
        <!-- time_select_wrapper start -->
        <div class="time_select_wrapper">
            <!-- <div class="time_select_item_1">
                <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                    <input type="text" id="tui-date-picker-target" aria-label="Date-Time">
                    <span class="tui-ico-date"></span>
                </div>
                <div id="tui-date-picker-container" style="margin-top: -1px;"></div>
            </div>
            <h1 class="middle_text"> ~ </h1>
            <div class="time_select_item_2">
                <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                    <input type="text" id="tui-date-picker-target2" aria-label="Date-Time">
                    <span class="tui-ico-date"></span>
                </div>
                <div id="tui-date-picker-container2" style="margin-top: -1px;"></div>
            </div> -->
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="datepicker-input-start" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
            </div>
            <div class="datepicker-cell" id="datepicker-month-start" style="margin-top: -1px;"></div>
            <h1 style="display:inline-block;display: inline-block;vertical-align: middle;font-size: 1rem;"> ~ </h1>
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="datepicker-input-end" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
            </div>
            <div class="datepicker-cell" id="datepicker-month-end" style="margin-top: -1px;"></div>
            <button class="search_button" onclick="search(1);">조회</button>
        </div>
        <!-- time_select_wrapper end -->
        <!-- <script src="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.js"></script>
        <script>
            var DatePicker = tui.DatePicker;
            var container = document.getElementById('tui-date-picker-container');
            var target = document.getElementById('tui-date-picker-target');
            var instance = new DatePicker(container, {
                input: {
                    element: target
                },
                language: 'ko'
            });

            var container2 = document.getElementById('tui-date-picker-container2');
            var target2 = document.getElementById('tui-date-picker-target2');
            var instance = new DatePicker(container2, {
                input: {
                    element: target2
                },
                language: 'ko'
            });
        </script> -->
        <!-- m_sec1 start -->
        <div id="m_sec1">
            <div class="row">
                <h1 style="margin-bottom:1rem" class="m_sec_title">S포인트 내역</h1>
            </div>
            <table id="useing_service">
                <thead>
                    <th>날짜</th>
                    <th>내역</th>
                    <th>적립</th>
                    <th>차감</th>
                    <th>S포인트 합계</th>
                </thead>
                <tbody id="tableBodyForPC">
                    <!-- <tr>
                        <td>2020/04/13 14:00</td>
                        <td>셀러봇캐시로니봇(정기결제)</td>
                        <td>+ 1,000 P</td>
                        <td>-350 P</td>
                        <td>1,000 P</td>
                    </tr> -->
                </tbody>
            </table>
            <div style="margin-top:0;" class="grid_noData">
                <h1>S포인트 내역이 없습니다.</h1>
            </div>
            <div class="table_pager_wrapper">
                <div id="grid-pagination-container" class="tui-pagination"></div>
            </div>
        </div>
        <!-- m_sec1 end -->
        <!-- mobile_table_type_ac start -->
        <div class="mobile_table_type_ac">
            <div class="m_sevice_mb" id="m_sec1_mb">
                <!-- <ul class="mb_table_service">
                    <li state="on" class="gray_bg">
                        <h1>날짜</h1>
                        <h2 class="blue_liner0">2020/04/14 14:00</h2>
                        <img class="ac_menu_icon" src="/assets/images/icon/icon_top.png" alt="">
                    </li>
                    <li>
                        <h1>내역</h1>
                        <h2>셀러봇캐시로니봇(정기결제)</h2>
                    </li>
                    <li>
                        <h1>적립</h1>
                        <h2>+ 1,000 P</h2>
                    </li>
                    <li>
                        <h1>차감</h1>
                        <h2>-350 P</h2>
                    </li>
                    <li>
                        <h1>S포인트 합계</h1>
                        <h2>1,000 P</h2>
                    </li>
                </ul> -->
                <!-- <div class="grid_noData mobile_asset">
                    <h1>등록된 결제 수단이 없어요~.</h1>
                </div> -->
            </div>
            <div class="table_pager_wrapper mobile_asset">
                <div id="grid-pagination-container-mb" class="tui-pagination"></div>
            </div>
        </div>
        <!-- mobile_table_type_ac end -->
    </div>
    <!-- container_wrapper end -->
</div>
<style>
    .my_wra {
        padding-top: 5em;
        min-height: auto;
        overflow-x: hidden;
    }
</style>
<style>
    #useing_service thead tr th {
        width: auto !important;
    }

    #useing_service tbody tr td {
        width: auto !important;
    }

    .bold {
        font-weight: bold;
        padding: 0 0.3rem;
        cursor: pointer;
    }

    .underLine {
        text-decoration: underline;
    }
</style>
<style>
    .time_select_wrapper {
        width: 100%;
        text-align: right;
    }

    .middle_text {
        display: inline-block;
        font-size: 1rem;
    }

    .time_select_item_1 {
        display: inline-block;
    }

    .time_select_item_2 {
        display: inline-block;
    }

    .tui-datepicker-input {
        width: 120px !important;
    }

    .search_button {
        display: inline-block;
        vertical-align: middle;
        background-color: #2DABE8;
        color: #fff;
        width: 80px;
        height: 30px;
        border-radius: 4px;
        margin-bottom: 10px;
    }

    div.table_pager_wrapper {}

    div#m_sec1 table#useing_service {
        margin-bottom: 0;
    }

    .table_pager_wrapper {
        margin-bottom: 5rem;
        margin-top: 2.5rem;
    }

    div#m_sec1 div.row {
        margin-bottom: 0;
        font-size: 0;
    }

    div#m_sec1 div.row h1 {
        margin-bottom: 0;
        font-size: 1rem;
        display: inline-block;
        vertical-align: middle;
        margin-top: -3px;
    }

    #m_sec1 .q_icon {}

    .tui-datepicker-input {
        vertical-align: middle;
    }

    .mobile_title {
        display: none;
    }

    .mobile_asset {
        display: none;
    }

    @media(max-width:880px) {
        .mobile_asset {
            display: block;
        }

        .mobile_title {
            display: block;
            padding: 0 2rem;
            font-size: 1.1rem;
            font-weight: bold;
        }

        .time_select_wrapper {
            text-align: left;
            margin-left: 2rem;
        }

        .m_sevice_mb ul.mb_table_service li.gray_bg::after {
            content: "";
            position: absolute;
            right: 1rem;
            background: #FFB800;
            opacity: 0.3;
            height: 1rem;
            top: 1.4rem;
            width: 9rem;
            right: 4rem;
        }
    }
</style>
<style>
    .tui-datepicker-input>input {
        width: 100%;
        height: 100%;
        padding: 6px 27px 6px 10px;
        font-size: 12px;
        line-height: 14px;
        vertical-align: top;
        border: 0;
        color: #333;
        border-radius: 3rem;
    }

    .tui-datepicker-input {
        position: relative;
        display: inline-block;
        width: 120px;
        height: 2.5rem;
        vertical-align: top;
        border: 1px solid #ddd;
        border-radius: 3rem;
    }

    .datepicker-cell {
        display: inline-block;
    }

    .tui-datepicker-input.tui-has-focus {
        vertical-align: middle;
        /* border: 0; */
    }

    .tui-datepicker {
        border: 1px solid #aaa;
        background-color: white;
        position: absolute;
        margin-left: -11rem;
        margin-top: 1rem;
        z-index: 999;
    }

    /*paging*/
    .tui-pagination {
        padding-top: 1rem;
    }

    .tui-pagination .tui-is-selected,
    .tui-pagination strong {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-is-selected:hover {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-first-child.tui-is-selected {
        border: 1px solid #a4a4a4;
    }
</style>
<script>
    var today = Date.now();

    var startMonthPicker = new tui.DatePicker('#datepicker-month-start', {
        date: today,
        language: 'ko',
        type: 'month',
        input: {
            element: '#datepicker-input-start',
            format: 'yyyy-MM'
        },
        selectableRanges: [
            [new Date(0), today]
        ]
    });

    var endMonthPicker = new tui.DatePicker('#datepicker-month-end', {
        date: today,
        language: 'ko',
        type: 'month',
        input: {
            element: '#datepicker-input-end',
            format: 'yyyy-MM'
        },
        selectableRanges: [
            [new Date(0), today]
        ]
    });
</script>
<script>
    $(document).ready(function () {
        search(1);
    });

    // PC용 페이징
    var pagination = new tui.Pagination('grid-pagination-container', {
        visiblePages: 10,
        itemsPerPage: 10,
        page: 1,
        centerAlign: false,
        firstItemClassName: 'tui-first-child',
        lastItemClassName: 'tui-last-child',
        usageStatistics: true
    });

    pagination.on('beforeMove', function (eventData) {
        search(eventData.page);
    });

    // 모바일용 페이징
    var paginationMB = new tui.Pagination('grid-pagination-container-mb', {
        visiblePages: 3,
        itemsPerPage: 3,
        page: 1,
        centerAlign: false,
        firstItemClassName: 'tui-first-child',
        lastItemClassName: 'tui-last-child',
        usageStatistics: true
    });

    paginationMB.on('beforeMove', function (eventData) {
        search(eventData.page);
    });

    // S포인트 내역 조회
    var search = function (page) {
        var isPC = true;
        if ($(".mobile_asset").css("display") != "none")
            isPC = false

        var startDateArray = $('#datepicker-input-start').val().split('-');
        var endDateArray = $('#datepicker-input-end').val().split('-');
        var start_dt = startDateArray[0] + startDateArray[1];
        var end_dt = endDateArray[0] + endDateArray[1];

        var param = {
            "start_dt": start_dt,
            "end_dt": end_dt,
            "page": page - 1,
            "size": isPC ? pagination._options.itemsPerPage : paginationMB._options.itemsPerPage
        };

        $.get("/sub/my/poiHistList", $.param(param), function (res) {
            // 테이블 갱신
            if (isPC)
                updatePayHistListForPC(res);
            else
                updatePayHistListForMB(res);
        });
    }

    // PC용 리스트 업데이트
    var updatePayHistListForPC = function (res) {
        var htmlStr = "";

        if (res.dataList == null || res.dataList.length == 0) {
            $(".grid_noData").show();
        }
        else {
            $(".grid_noData").hide();

            for (var i = 0; i < res.dataList.length; i++) {
                htmlStr += "<tr>";
                htmlStr += "    <td>" + res.dataList[i].occur_TS + "</td>";
                htmlStr += "    <td>" + res.dataList[i].poi_OCCUR_REAS + " - " + res.dataList[i].poi_OCCUR_CD_NM + "</td>";

                if (res.dataList[i].poi_SAV_PRC != null)
                    htmlStr += "    <td>+" + fnAddComma(res.dataList[i].poi_SAV_PRC) + " P</td>";
                else
                    htmlStr += "    <td></td>";

                if (res.dataList[i].poi_USE_PRC != null)
                    htmlStr += "    <td>-" + fnAddComma(res.dataList[i].poi_USE_PRC) + " P</td>";
                else
                    htmlStr += "    <td></td>";

                htmlStr += "    <td>" + fnAddComma(res.dataList[i].poi_SAV_PRC - res.dataList[i].poi_USE_PRC) + " P</td>";
                htmlStr += "</tr>";
            }
        }

        // 조회 시 pagination 초기화
        if (res.page == 0) {
            pagination.setTotalItems(res.totalCount);
            pagination._currentPage = 0;
            pagination.reset();
        }

        $("#tableBodyForPC").html(htmlStr);

        // pagination 노출 처리
        if (res.dataList == null || res.dataList.length == 0)
            $("#grid-pagination-container").hide();
        else
            $("#grid-pagination-container").show();
    };

    // 모바일용 리스트 업데이트
    var updatePayHistListForMB = function (res) {
        var htmlStr = "";

        if (res.dataList == null || res.dataList.length == 0) {
            htmlStr += '<div class="grid_noData mobile_asset">';
            htmlStr += '    <h1>S포인트 내역이 없습니다.</h1>';
            htmlStr += '</div>';
        }
        else {
            for (var i = 0; i < res.dataList.length; i++) {
                htmlStr += '<ul class="mb_table_service">';
                htmlStr += '    <li state="on" class="gray_bg" onclick="pressedMobileTableTitle(this);">';
                htmlStr += '        <h1>날짜</h1>';
                htmlStr += '        <h2 class="blue_liner0">' + res.dataList[i].occur_TS +'</h2>';
                htmlStr += '        <img class="ac_menu_icon" src="/assets/images/icon/icon_top.png" alt="">';
                htmlStr += '    </li>';
                htmlStr += '    <li>';
                htmlStr += '        <h1>내역</h1>';
                htmlStr += '        <h2>' + res.dataList[i].poi_OCCUR_REAS + " - " + res.dataList[i].poi_OCCUR_CD_NM + '</h2>';
                htmlStr += '    </li>';
                htmlStr += '    <li>';
                htmlStr += '        <h1>적립</h1>';

                if (res.dataList[i].poi_SAV_PRC != null)
                    htmlStr += "        <h2>+" + fnAddComma(res.dataList[i].poi_SAV_PRC) + " P</h2>";
                else
                    htmlStr += '        <h2></h2>';

                htmlStr += '    </li>';
                htmlStr += '    <li>';
                htmlStr += '        <h1>차감</h1>';

                if (res.dataList[i].poi_USE_PRC != null)
                    htmlStr += "        <h2>-" + fnAddComma(res.dataList[i].poi_USE_PRC) + " P</h2>";
                else
                    htmlStr += '        <h2></h2>';
                
                htmlStr += '    </li>';
                htmlStr += '    <li>';
                htmlStr += '        <h1>S포인트 합계</h1>';
                htmlStr += '        <h2>' + fnAddComma(res.dataList[i].poi_SAV_PRC - res.dataList[i].poi_USE_PRC) + ' P</h2>';
                htmlStr += '    </li>';
                htmlStr += '</ul>';
            }
        }

        // 조회 시 pagination 초기화
        if (res.page == 0) {
            paginationMB.setTotalItems(res.totalCount);
            paginationMB._currentPage = 0;
            paginationMB.reset();
        }

        $("#m_sec1_mb").html(htmlStr);

        // pagination 노출 처리
        if (res.dataList == null || res.dataList.length == 0)
            $("#grid-pagination-container-mb").hide();
        else
            $("#grid-pagination-container-mb").show();
    };

    var pressedMobileTableTitle = function(obj) {
        var state = $(obj).attr("state");
        if(state == "on") {
            $(obj).attr("state", "off");
            $(obj).siblings().css("display", "none");
            $(obj).find(".ac_menu_icon").attr("src", "/assets/images/icon/icon_bottom2.png");
        }
        else {
            $(obj).attr("state", "on");
            $(obj).siblings().css("display", "block");
            $(obj).find(".ac_menu_icon").attr("src", "/assets/images/icon/icon_top.png");
        }
    };
</script>