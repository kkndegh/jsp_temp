<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />
<spring:eval expression="@environment.getProperty('internal.redirect.base')" var="redirectBase"/>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 
<!-- <script src="https://uicdn.toast.com/editor/latest/toastui-editor-viewer.js"></script> -->

<div class="container">
    <div class="cs_wra">
        <div class="menu_top">
            <p class="menu_name">고객센터</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
					<li><a href="/pub/cs/notice">공지사항</a></li>
					<li class="focus"><a href="/pub/cs/event">이벤트</a></li>
					<li><a href="/pub/cs/faq">FAQ</a></li>
					<li><a href="/pub/cs/ask">문의하기</a></li>
				</ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">이벤트</p>
            <p class="desc">셀러봇캐시의 파트너사가 제공하는 이벤트입니다.</p>
        </div>
        <div class="cs_area">
            <!-- <a href="/sub/service/guide.html" class="p_icon"><img src="/assets/images/member/premiumService_button.png" alt=""></a> -->
            <div class="cs_section">
                <section class="event">
                    <div class="event_list">
                        <div class="type_event">
                            <button onclick="history.go(-1)" class="back"><img src="/assets/images/notice/arrows.png"
                                    alt=""> 목록으로</button>
                        </div>
                        <div class="event_contents">
                            <div class="etc_event">
                                <p class="title_event_detail">${eventDetail.event_title }</p>
                                <span class="date">
                                    <label class="lb">이벤트 날짜 : </label>
                                    ${dateUtilBean.makeNonFormatToFormat(eventDetail.show_stt_dt) } ~
                                    ${dateUtilBean.makeNonFormatToFormat(eventDetail.show_end_dt)}
                                </span>
                                <%-- <p class="view">
                                        <img src="/assets/images/notice/eyes.png" alt=""> ${eventDetail.scr_inq_num eq null ? 0 : eventDetail.scr_inq_num}
                                    </p> --%>

                            </div>
                            <img class="event_banner_img"
                                src="/imagefile/${eventDetail.event_content_file.stor_path}${eventDetail.event_content_file.stor_file_nm}"
                                alt="배너">
                            <div class="title_big">${eventDetail.event_title}</div>
                            <div class="section_line_detail">
                                <div class="info_detail_event">
                                    <div class="item_event">
                                        <span class="lb">이벤트 기간</span>
                                        <p class="text_e">${dateUtilBean.makeNonFormatToFormat(eventDetail.show_stt_dt) }
                                            ~ ${dateUtilBean.makeNonFormatToFormat(eventDetail.show_end_dt)} 까지</p>
                                    </div>
                                    <div class="item_event">
                                        <span class="lb">이벤트 내용</span>
                                        <p class="text_e">
                                            <!-- <div id="viewer" initialValue="${eventDetail.event_cont }"></div> -->
                                            <c:out value="${eventDetail.event_cont }" escapeXml="false"></c:out>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<script>
    $(".type_event .chk_circle").click(function () {
        var chk = $(this);
        if (chk.attr("id") == "e_all") {
            $(".event_list li.ing").show();
            $(".event_list li.end").show();
        } else if (chk.attr("id") == "e_ing") {
            $(".event_list li.ing").show();
            $(".event_list li.end").hide();
        } else if (chk.attr("id") == "e_end") {
            $(".event_list li.ing").hide();
            $(".event_list li.end").show();
        }
    })

    // const viewer = new toastui.Editor({
    //     el: document.querySelector('#viewer')
    // });
    
</script>