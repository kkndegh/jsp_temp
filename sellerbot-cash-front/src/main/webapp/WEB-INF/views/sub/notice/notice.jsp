<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:eval expression="@environment.getProperty('internal.redirect.base')" var="redirectBase"/>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 

<div class="container">
	<div class="cs_wra">
		<div class="menu_top">
			<p class="menu_name">고객센터</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<li class="focus"><a href="/pub/cs/notice">공지사항</a></li>
					<li><a href="/pub/cs/event">이벤트</a></li>
					<li><a href="/pub/cs/faq">FAQ</a></li>
					<li><a href="/pub/cs/ask">문의하기</a></li>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">공지사항</p>
			<p class="desc">셀러봇캐시 고객님들께 공지합니다~ 꼭 한번 읽어봐주세요</p>
		</div>
		<div class="cs_area">
			<!-- <a href="#" class="p_icon"><img src="/assets/images/member/premiumService_button.png" alt=""></a> -->
			<section class="notice">
				<div class="notice_list">
					<div class="title">전체 | <b>${noticeList.totalCount }</b>건
						<div class="search_notice">
							<form name="noticeForm" action="/pub/cs/notice" method="get">
								<input name="title" type="text" class="notice_sh_box" value="${title}"
									placeholder="검색어를 입력해주세요.">
								<input name="page" type="hidden" value="${paging.pageNo}">
								<button onclick="goPage(0);return false;"><img
										src="/assets/images/notice/btn_search.png" alt=""></button>
							</form>
						</div>
					</div>
					<div class="list_item_notice">
						<c:forEach var="eitem" items="${noticeList.dataList}">
							<div class="title_notice" id="seq${eitem.noti_seq_no }">
								<p class="notice_info">${sf:formatLocalDateTime(eitem.reg_ts, 'yyyy-MM-dd HH:mm')}</p>
								<p class="text_n">
									<c:if test="${eitem.noti_mth_cd eq 'PRI' }">[공지]</c:if>
									<c:out value="${eitem.title}" />
									<c:if test="${not empty eitem.noti_file }"><img
											src="/assets/images/notice/pic_icon.png" alt=""></c:if>
								</p>
								<div class="text_detail">
									<c:if
										test="${not empty eitem.noti_file && eitem.noti_file.stor_file_loc_cd eq 'CUP' }">
										<p class="imgbox">
											<img src="/imagefile/${eitem.noti_file.stor_path}${eitem.noti_file.stor_file_nm}"
												alt="" style="width: 100%">
										</p>
									</c:if>
									${eitem.cont}
									<c:if
										test="${not empty eitem.noti_file && eitem.noti_file.stor_file_loc_cd eq 'CDW' }">
										<p class="imgbox">
											<img src="/imagefile/${eitem.noti_file.stor_path}${eitem.noti_file.stor_file_nm}"
												alt="" style="width: 100%">
										</p>
									</c:if>
									<div class="close_btn_notice">
										<img src="/assets/images/notice/arrow_close.png" alt="">닫기
									</div>
								</div>
							</div>
						</c:forEach>

						<c:if test="${ noticeList.totalCount == 0 }">
							<div class="no_data">
								<p>검색 결과가 없습니다.</p>
							</div>
						</c:if>


					</div>
					<jsp:include page="/WEB-INF/jsputils/pagination.jsp">
						<jsp:param name="firstPageNo" value="${paging.firstPageNo}" />
						<jsp:param name="prevPageNo" value="${paging.prevPageNo}" />
						<jsp:param name="startPageNo" value="${paging.startPageNo}" />
						<jsp:param name="pageNo" value="${paging.pageNo}" />
						<jsp:param name="endPageNo" value="${paging.endPageNo}" />
						<jsp:param name="nextPageNo" value="${paging.nextPageNo}" />
						<jsp:param name="finalPageNo" value="${paging.finalPageNo}" />
					</jsp:include>
				</div>

				<!-- 20220620 숨김 요청 -->
				<!-- <div class="new_owl_slider_wrap fl_wrap">
					<div class="new_owl_slider_num_wrap new_owl_slider_1_wrap">
						<h1>파트너사</h1>
						// s: 20210319
						<c:forEach items="${bannerB3DDDataList}" varStatus="status" var="data">
							<c:set var="imgPath" value="${data.stor_path}${data.stor_file_nm}"></c:set>
							<c:set var="background" value="${data.background}"></c:set>
							<div class="notice_bnr">
								<a href="${data.link_url}">
									<img src="/imagefile/${imgPath}" alt="" />
								</a>
							</div>
						</c:forEach>
						<c:if test="${empty bannerB3DDDataList}">
							// 디폴트 이미지가 있어야함. 
							<div class="new_owl_slider_1">
								<img src="/assets/images/notice/icon_partner1.png" alt="" />
								<img src="/assets/images/notice/icon_partner2.png" alt="" />
								<img src="/assets/images/notice/icon_partner3.png" alt="" />
								<img src="/assets/images/notice/icon_partner4.png" alt="" />
							</div>
						</c:if>
						// e: 20210319
					</div>
					<div class="new_owl_slider_num_wrap new_owl_slider_2_wrap">
						<h1>특허/이력</h1>
						<div class="new_owl_slider_2">
							<img src="/assets/images/notice/icon_patent1.png" alt="" />
							<img src="/assets/images/notice/icon_patent2.png" alt="" />
							<img src="/assets/images/notice/icon_patent3.png" alt="" />
							<img src="/assets/images/notice/icon_patent4.png" alt="" />
						</div>
					</div>
				</div> -->

				<div class="new_owl_slider_wrap fl_wrap">
					<div class="new_owl_slider_num_wrap new_owl_slider_1_wrap">
						<h1>제휴사(Partner)</h1>
						<div class="notice_bnr">
							<img src="/assets/images/notice/partner_432_264.jpg" alt="" />
						</div>
					</div>
					<div class="new_owl_slider_num_wrap new_owl_slider_2_wrap">
						<h1>특허/이력</h1>
						<div class="notice_bnr">
							<img src="/assets/images/notice/patent_history_865_529.png" alt="" />
						</div>
					</div>
				</div>

			</section>
		</div>
	</div>
</div>

<script>

	$(document).ready(function () {
		var noti_seq_no = '${noti_seq_no}';
		if (noti_seq_no != "") {
			$("#seq" + noti_seq_no).addClass("active");
		}
	});

	function goPage(p) {
		if (document.noticeForm.page.value != p) {
			document.noticeForm.page.value = p;
			document.noticeForm.submit();
		}
	};

</script>


<script type="text/javascript">
	// 디자인 스크립트
	// <!--
	$(document).ready(function () {

		$(".text_n").click(function () {
			var notice = $(this).parents(".title_notice");
			if (notice.hasClass("active") == true) {
				notice.removeClass("active");
			} else {
				$(".title_notice").removeClass("active");
				notice.addClass("active");
				var id = $(this).parents(".title_notice").attr("id")
				viewCounting(id);
			}
			$(".close_btn_notice").click(function () {
				$(this).parents(".title_notice").removeClass("active");
			});
		});


		var owl = $('.new_owl_slider_1');
		owl.owlCarousel({
			items: 3,
			loop: true,
			autoplay: true,
			margin: 60,
			autoplayTimeout: 2000,
			autoplayHoverPause: false,

		});
		var owl2 = $('.new_owl_slider_2');
		owl2.owlCarousel({
			items: 3,
			margin: 60,
			loop: true,
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: false,
		});


	});

	var viewCounting = function(seq) {
		var seqNo = seq.replace("seq", "");
		var param = {
			"notiSeqNo": seqNo
		};
		$.post("/pub/cs/viewCountingNoti", $.param(param), function (res) {
		})
		.fail(function (response) {
		});
	}

//-->
</script>