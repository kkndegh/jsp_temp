<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />
<spring:eval expression="@environment.getProperty('internal.redirect.base')" var="redirectBase"/>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 

<div class="container">
	<div class="cs_wra">
		<div class="menu_top">
			<p class="menu_name">고객센터</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<li><a href="/pub/cs/notice">공지사항</a></li>
					<li class="focus"><a href="/pub/cs/event">이벤트</a></li>
					<li><a href="/pub/cs/faq">FAQ</a></li>
					<li><a href="/pub/cs/ask">문의하기</a></li>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">이벤트</p>
			<p class="desc">셀러봇캐시와 파트너사가 준비한 특별한 이벤트가 되도록 언제나 노력하겠습니다.</p>
		</div>
		<div class="cs_area">
			<!-- <a href="#" class="p_icon"><img src="/assets/images/member/premiumService_button.png" alt=""></a> -->
			<div class="cs_section">
				<section class="event">
					<div class="event_list">
						<div class="type_event">
							<form name="eventForm" action="/pub/cs/event" method="get">
								<input type="radio" class="chk_circle" id="e_all" name="event_sts_cd" value="ALL" <c:if
									test="${event_sts_cd eq 'ALL'}">checked</c:if> >
								<label for="e_all">전체</label>
								<input type="radio" class="chk_circle" id="e_ing" name="event_sts_cd" value="DOI" <c:if
									test="${event_sts_cd eq 'DOI'}">checked</c:if> >
								<label for="e_ing">진행중인 이벤트</label>
								<input type="radio" class="chk_circle" id="e_end" name="event_sts_cd" value="COM" <c:if
									test="${event_sts_cd eq 'COM'}">checked</c:if> >
								<label for="e_end">종료된 이벤트</label>
								<input name="page" type="hidden" value="${paging.pageNo}">
							</form>
						</div>
						<ul>
							<c:forEach items="${eventList.dataList}" var="event" varStatus="status">
								<li class="${event.event_sts_cd_nm eq '진행' ? 'ing' : 'end'} clearfix">
									<a href="/pub/cs/event_detail?event_seq_no=${event.event_seq_no}"
										class="imgbox"><img
											src="/imagefile/${event.event_benner_file.stor_path}${event.event_benner_file.stor_file_nm}"
											alt=""></a>
									<div class="preview_event">
										<a href="/pub/cs/event_detail?event_seq_no=${event.event_seq_no}"
											class="name">${event.event_title }</a>
										<p class="date">${dateUtilBean.makeNonFormatToFormat(event.show_stt_dt) } ~
											${dateUtilBean.makeNonFormatToFormat(event.show_end_dt) }</p>

									</div>
									<a href="/pub/cs/event_detail?event_seq_no=${event.event_seq_no}"
										class="detail_btn_event">
										<p class="arrow_btn">
											<img src="/assets/images/notice/btn_eventDetail.png" alt="">
										</p>
									</a>
								</li>
							</c:forEach>

							<c:if test="${paging.totalCount == 0 }">
								<div class="no_data">
									<p>진행 중인 이벤트가 없습니다.</p>
								</div>
							</c:if>


						</ul>
						<jsp:include page="/WEB-INF/jsputils/pagination.jsp">
							<jsp:param name="firstPageNo" value="${paging.firstPageNo}" />
							<jsp:param name="prevPageNo" value="${paging.prevPageNo}" />
							<jsp:param name="startPageNo" value="${paging.startPageNo}" />
							<jsp:param name="pageNo" value="${paging.pageNo}" />
							<jsp:param name="endPageNo" value="${paging.endPageNo}" />
							<jsp:param name="nextPageNo" value="${paging.nextPageNo}" />
							<jsp:param name="finalPageNo" value="${paging.finalPageNo}" />
						</jsp:include>
					</div>

					<div class="new_owl_slider_wrap">
						<div class="new_owl_slider_num_wrap new_owl_slider_1_wrap">
							<h1>파트너사</h1>
							<div class="new_owl_slider_1">
								<img src="/assets/images/notice/icon_partner1.png" alt="" />
								<img src="/assets/images/notice/icon_partner2.png" alt="" />
								<img src="/assets/images/notice/icon_partner3.png" alt="" />
								<img src="/assets/images/notice/icon_partner4.png" alt="" />
							</div>
						</div>
						<div class="new_owl_slider_num_wrap new_owl_slider_2_wrap">
							<h1>특허/이력</h1>
							<div class="new_owl_slider_2">
								<img src="/assets/images/notice/icon_patent1.png" alt="" />
								<img src="/assets/images/notice/icon_patent2.png" alt="" />
								<img src="/assets/images/notice/icon_patent3.png" alt="" />
								<img src="/assets/images/notice/icon_patent4.png" alt="" />
							</div>
						</div>
					</div>

				</section>
			</div>
		</div>
	</div>
</div>

<script>

	// 개발 스크립트
	$(document).ready(function () {
		$("[name='event_sts_cd']").on("change", function () {
			document.eventForm.page.value = 1;
			document.eventForm.submit();
		});


	});

	function goPage(p) {
		if (document.eventForm.page.value != p) {
			document.eventForm.page.value = p;
			document.eventForm.submit();
		}
	};
</script>

<script type="text/javascript">
	// 디자인 스크립트
	// <!--
	$(".text_n").click(function () {
		var notice = $(this).parents(".title_notice");
		if (notice.hasClass("active") == true) {
			notice.removeClass("active");
		} else {
			$(".title_notice").removeClass("active");
			notice.addClass("active");
		}
		$(".close_btn_notice").click(function () {
			$(this).parents(".title_notice").removeClass("active");
		});
	});
	$(document).ready(function () {
		var owl = $('.new_owl_slider_1');
		owl.owlCarousel({
			items: 3,
			loop: true,
			autoplay: true,
			margin: 60,
			autoplayTimeout: 2000,
			autoplayHoverPause: false,

		});
		var owl2 = $('.new_owl_slider_2');
		owl2.owlCarousel({
			items: 3,
			margin: 60,
			loop: true,
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: false,
		});
	});


//-->
</script>