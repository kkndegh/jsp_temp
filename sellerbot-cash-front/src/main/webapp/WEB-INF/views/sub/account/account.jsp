<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"
%><%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" 
%>

<script src="/assets/js/tuiGridCommon.js"></script>
<script src="/assets/js/account.js?ver=20211025_01"></script>
<script src="/assets/js/accountModal.js?ver=20221128_01"></script>

<input type="hidden" name="cafe24_usr_yn" id="cafe24_usr_yn" value="${cafe24_usr_yn}">
<input type="hidden" name="addn_goods_req_seq_no" id="addn_goods_req_seq_no" value="${addn_goods_req_seq_no}">
<input type="hidden" name="cust_acct_seq_no" id="cust_acct_seq_no">
<input type="hidden" name="cust_acct_sts_cd" id="cust_acct_sts_cd">
<input type="hidden" id="currentRegCount" value="${fn:length(custAcctInfo.acct_list)}">
<input type="hidden" id="maxRegCount" value="${maxRegCountInfo.SETT_ACC_ACCT_REG_PSB_CNT}">

<input type="hidden" id="regAcctTestSuccYN" name="regAcctTestSuccYN" value="N">
<input type="hidden" id="mdfyAcctTestSuccYN" name="mdfyAcctTestSuccYN" value="N">
<input type="hidden" id="chkAcctDuplYN" name="chkAcctDuplYN" value="">

<style media="screen">
	.item_signup_info:after {
		display: none;
	}
</style>

<div class="container">
	<div class="account_wra">
		<div class="menu_top">
			<p class="menu_name">정산계좌 통합관리</p>
			<!-- //20200706_10 :: 메뉴 변경 Start -->
			<ul class="gnb_top clearfix">
				<li class="focus"><a href="/sub/account/account">정산계좌</a></li>
				<li><a href="/sub/account/detail">거래내역 조회</a></li>
				<c:if test="${cafe24_usr_yn eq 'Y' or shopby_usr_yn eq 'Y'}">
					<li><a href="/sub/account/reconcile">대사 서비스</a></li>
				</c:if>
			</ul>
			<!-- //20200706_10 :: 메뉴 변경 End -->
		</div>
		<div class="account_section">

			<!-- //20200706_10 :: 그리드 추가 Start -->
			<div class="grid_container">
				<div class="grid_header">
					<div class="grid_row">
						<h2 class="title">정산계좌 목록</h2>
						<button class="button" id="MyAccountRegister">계좌등록</button>
					</div>
					<div class="grid_row grid_summary_box">
						<div class="asset">
							<span class="label">총 잔액:</span>
							<span class="content"><fmt:formatNumber value="${custAcctInfo.total_acct_prc}" pattern="#,###" />원</span>
						</div>
						<div class="guide">
							<ul>
								<li><i class="icon check_2"></i> 정상</li>
								<li><i class="icon check"></i> 계좌 조회 중</li>
								<li><i class="icon warning"></i> 인증정보 오류</li>
								<li><i class="icon comment"></i> 관리자 문의 필요</li>
								<li>
									<span class="label">조회기준일시:</span>
									<span class="content">${custAcctInfo.last_update_dt}</span>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="grid_body">
					<div id="grid"></div>
					<c:if test="${fn:length(custAcctInfo.acct_list) == 0}">
						<div class="list_register_account_noData" style="display: block;">
							<h1>등록된 계좌가 없습니다.</h1>
						</div>
					</c:if>
				</div>
			</div>
			<!-- //20200706_10 :: 그리드 추가 End -->

			<div class="account_sum">
				<h1 class="account_sum_title">판매몰별 입금 내역 요약</h1>

				<div class="account_sum_wrap" style="display: block;">
					<div class="account_sum_date_wrap">
						<div class="account_sum_date_wrap_wrap">
							<ul class="account_sum_date_mon">
								<li id="month1" class="account_sum_date_mon_active">1개월</li>
								<li id="month2">2개월</li>
								<li id="month3">3개월</li>
							</ul>
							<div class="account_sum_date_sel">
								<input id="sta_dt" type="text" class="account_sum_date_dp"> ~ <input id="end_dt"
									type="text" class="account_sum_date_dp">
							</div>
							<button id="btnSearch" class="account_sum_date_sub">조회</button>
						</div>
					</div>

					<div class="account_sum_text_wrap" style="display: none;">
						최근 <span class="account_sum_start_date">2019년 2월 15일</span>부터 <span
							class="account_sum_end_date">2019년 3월 14일</span>까지<br />
						<h1>판매몰에서 입금된 금액의 합계는</h1><span class="account_sum_pr">0원</span>
						<h1 class="inlineFix">입니다.</h1>
					</div>

					<div class="account_sum_bank_info" style="display: none;">
						<h1 class="account_sum_bank_info_title">입금 합계</h1>
						<ul class="account_sum_bank_info_list" style="font-size: 0.92rem">
							<li class="text-left">
								<div class="account_sum_bank_info_bank">
									<img src="/assets/images/member/shinhan.png" alt="">
									<p class="bank_name">신한</p>
								</div>
								<h1 class="account_sum_bank_account">1002-843-957860</h1>
								<div class="account_sum_mall_wrap">
									<span class="account_sum_mall_name">G마켓</span>
									<span class="account_sum_mall_id">street1</span>
								</div>
								<div class="account_sum_bank_info_pr_wrap">
									<h1 class="account_sum_bank_info_pr">0</h1>원
								</div>
							</li>
						</ul>
					</div>

					<div class="list_register_account_noData" style="display: block;">
						<h1>입금내역이 없습니다.</h1>
					</div>


				</div>

			</div>
		</div>
		<!--account_section_end -->
	</div>
	<!--account_wra_end -->
	<div class="banner_payment">
		<div class="area_banner_payment">
			<div class="bf_hand_tlt">
				<img src="/assets/images/payment/question_Icon.png" alt="">
				<p><b>미리 정산받을 수 있는 금액</b><br>
					내가 지원받을 수 있는 금액
				</p>
			</div>
			<div class="price_bfhand">
				<p>최대 <b>
						<fmt:formatNumber value="${custAcctInfo.pre_sett}" pattern="#,###" /></b>원</p>
			</div>
			<a href="/sub/pre_calculate/pre_calculate" class="more_bfhand">자세히보기</a>
		</div>
	</div>

</div>
<!--container_end-->


<!-- //20200706_10 :: 계좌정보 입력 팝업 Start -->
<div class="popup_container" id="regModal">
	<div class="popup_box" style="height: 97%; max-height: 875px;">
		<div class="popup_header">
			<h1>계좌정보 등록</h1>
			<div class="button_container">
				<button class="help_btn" onclick="btnPressedHelp(this, 'reg');"><img src="/assets/images/icon/q.png" alt=""></button>
				<button class="close_btn"><img src="/assets/images/icon/x_black.png" alt=""></button>
			</div>
		</div>
		<div class="popup_body">
			<div class="notice_msg">
				<div class="notice_content">
					<p>정산계좌 통합관리 서비스를 이용하시려면 각 <strong class="em">은행별 홈페이지에서 빠른조회서비스 등록</strong>을 진행하신 후 계좌정보를 입력하셔야
						합니다.</p>
				</div>
			</div>
			<div class="form_container">
				<!-- <form> -->
					<div class="form_row">
						<label class="label">
							<span>은행명</span>
							<div class="links">
								<!-- <button class="link mov_guide">영상가이드</button>
								<button class="link go_selected_bank">선택은행 바로가기</button> -->
								<span class="link mov_guide modal_link_btn" onclick="btnPressedGuideMov();">영상가이드</span>
								<span class="link go_selected_bank modal_link_btn" onclick="btnPressedOpenBank('reg');">선택은행 바로가기</span>
							</div>
						</label>
						<select class="form" id="reg_bank_cert_m_seq_no" onchange="onChangeBankSel(this, 'reg');">
							<c:forEach items="${bankList}" var="bank">
								<option value="${bank.bank_cert_m_seq_no}" 
									data-bankcd="${bank.bank_cd}"
									data-acctnoyn="${bank.acct_no_use_yn}"
									data-acctpwyn="${bank.acct_passwd_use_yn}"
									data-bankidyn="${bank.bank_id_use_yn}"
									data-bankpwyn="${bank.bank_passwd_use_yn}"
									data-crccd="${bank.crc_use_cd}"
									data-ctznoyn="${bank.ctz_biz_no_use_yn}">${bank.bank_nm}</option>
							</c:forEach>
						</select>
					</div>
					<div class="form_row" id="reg_bank_id_area">
						<label class="label">
							<span>빠른조회 은행아이디</span>
						</label>
						<input class="form" id="reg_bank_id" type="text" maxlength="25" required="required" onchange="changeRegModalData();">
						<p id="reg_bank_id_error" class="error input_field_error" style="display:none;">*은행아이디를 입력해주세요.</p>
					</div>
					<div class="form_row" id="reg_bank_passwd_area">
						<label class="label">
							<span>빠른조회 은행비밀번호</span>
						</label>
						<input class="form" id="reg_bank_passwd" type="password" maxlength="20" required="required" onchange="changeRegModalData();">
						<p id="reg_bank_passwd_error" class="error input_field_error" style="display:none;">*은행비밀번호 입력해주세요.</p>
					</div>
					<div class="form_row" id="reg_dpsi_nm_area">
						<label class="label">
							<span>예금주</span>
						</label>
						<input class="form" id="reg_dpsi_nm" type="text" maxlength="20" required="required" onchange="changeRegModalData();">
						<p id="reg_dpsi_nm_error" class="error input_field_error" style="display:none;">*예금주를 입력해주세요.</p>
					</div>
					<div class="form_row" id="reg_acct_no_area">
						<label class="label">
							<span id="reg_acctNoTitle">계좌번호</span>
							<p id="reg_checkDupl_info"></p>
						</label>
						<input class="form" id="reg_acct_no" type="text" style="width: 75%;"  maxlength="20" autocomplete="off" data-valitype="acctNo" required="required" onchange="changeRegModalData();">
						<p id="reg_acct_no_error" class="error input_field_error" style="display:none;">*계좌번호를 바르게 입력해주세요.</p>
						<button class="button" id="dupleChkbtn" style="position: relative; height: 5.55%; width: 18.5%; margin-left: 0.3rem; padding: 0.6rem 0.9rem;" onclick="regCheckDupl()">중복 확인</button>
					</div>
					<div class="form_row" id="reg_acct_passwd_area">
						<label class="label">
							<span id="reg_acctPwdTitle">계좌 비밀번호</span>
						</label>
						<input class="form" id="reg_acct_passwd" type="password" autocomplete="new-password" maxlength="20" required="required" onchange="changeRegModalData();">
						<p id="reg_acct_passwd_error" class="error input_field_error" style="display:none;">*계좌비밀번호 입력해주세요.</p>
					</div>
					<div class="form_row" id="reg_ctz_biz_no_area">
						<label class="label">
							<span>주민등록 (사업자)번호</span>
						</label>
						<input class="form" id="reg_ctz_biz_no" type="text" maxlength="10" placeholder="주민번호 앞 6자리 또는 사업자번호 10자리만 입력" data-valitype="ctzBizno" required="required" onchange="changeRegModalData();">
						<p id="reg_ctz_biz_no_error" class="error input_field_error" style="display:none;">*주민등록(사업자) 번호를 바르게 입력해주세요.</p>
					</div>
					<div class="form_row" id="reg_crc_area">
						<label class="label">
							<span>통화</span>
						</label>
						<select class="form" id="reg_crc">
							<option value="">선택</option>
							<c:forEach items="${codeList}" var="code">
								<option value="${code.com_cd}">${code.cd_nm}</option>
							</c:forEach>
						</select>
						<p id="reg_crc_error" class="error input_field_error" style="display:none;">*통화를 선택해주세요.</p>
					</div>
					<div class="form_row">
						<label class="label">
							<span>계좌 사업자 여부</span>
						</label>
						<select class="form" id="acct_biz_yn">
							<option value="">선택</option>
							<option value="Y">Y</option>
							<option value="N">N</option>
						</select>
					</div>
					<c:if test="${cafe24_usr_yn eq 'Y' && !empty addn_goods_req_seq_no}">
						<div class="form_row notice_msg">
							<div class="notice_content">
								<p>카페24 ‘뱅크봇-대사서비스’ 이용을 원하시는 회원님은 반드시 <strong class="em">입금확인용계좌</strong>를 체크해주시기 바랍니다.
								</p>
							</div>
						</div>
						<div class="form_row" id="reg_reco_yn_area">
							<label class="label_form">
								<input type="checkbox" id="reg_reco_yn"/>
								<span>입금 확인용 계좌</span>
							</label>
						</div>
					</c:if>
				<!-- </form> -->
			</div>

			<!-- 계좌정보 입력 가이드 Start -->
			<div class="account_info_guide">
				<p>정산계좌 통합관리 서비스를 이용하시려면 각 은행별 홈페이지에서 빠른조회서비스 등록을 진행하신 후 계좌정보를 입력하셔야 합니다. 하단의 은행명을 선택해주시면 은행별 빠른조회서비스 등록
					절차를 확인하실 수 있습니다.</p>
				<div class="banking_info">
					<h3>KEB하나은행</h3>
					<div class="banking_info_section">
						<h4>▣ 개인뱅킹</h4>
						<ul>
							<li>
								<span class="order">①</span>
								<p>KEB하나은행 홈페이지(<a href="http://www.kebhana.com"
										target="_blank">http://www.kebhana.com</a>)에 접속</p>
							</li>
							<li>
								<span class="order">②</span>
								<p>공인인증서를 통한 인터넷뱅킹에 로그인</p>
							</li>
							<li>
								<span class="order">③</span>
								<p><strong>마이하나</strong> &gt; <strong>계좌정보관리</strong> &gt; <strong>빠른조회관리</strong>에서 신청
								</p>
							</li>
						</ul>
					</div>
					<div class="banking_info_section">
						<h4>▣ 기업뱅킹</h4>
						<ul>
							<li>
								<span class="order">①</span>
								<p>KEB하나은행 홈페이지(<a href="http://www.kebhana.com"
										target="_blank">http://www.kebhana.com</a>)에 접속</p>
							</li>
							<li>
								<span class="order">②</span>
								<p>공인인증서를 통한 인터넷뱅킹에 로그인 - 기업/CMS 로 분류되며, CMS인 경우 지점방문 후 신청해야 합니다.</p>
							</li>
							<li>
								<span class="order">③</span>
								<p><strong>뱅킹관리</strong> &gt; <strong>계좌관리</strong> &gt; <strong>빠른조회계좌관리</strong>에서 신청
								</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- 계좌정보 입력 가이드 End -->
		</div>
		<div class="popup_footer">
			<div class="button_row">
				<button class="button left" onclick="btnPressedTestAccount('reg');" dis>계정 테스트</button>
				<button class="button primary" class="save_btn" onclick="btnPressedAcctReg();">등록</button>
				<button class="button close_btn">닫기</button>
				<div style="float: right;width: 66px;height: 1px;"></div>
			</div>
		</div>
	</div>
	<!-- 영상 가이드 팝업 Start -->
	<div class="account_mov_guide_container">
		<button class="mov_close_btn"><img src="/assets/images/icon/x_white.png" alt=""></button>
		<div class="mov_guide_box">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/OzaIAVrgQEo?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
				allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				allowfullscreen></iframe>
		</div>
	</div>
	<!-- 영상 가이드 팝업 End -->
	<div class="popup_black"></div>
</div>
<!-- //20200706_10 :: 계좌정보 입력 팝업 End -->

<!-- 계좌 수정 팝업 Start -->
<div class="popup_container" id="mdfyModal">
	<div class="popup_box" style="height: 97%; max-height: 875px;">
		<div class="popup_header">
			<h1>계좌정보 수정</h1>
			<div class="button_container">
				<button class="help_btn" onclick="btnPressedHelp(this, 'mdfy');"><img src="/assets/images/icon/q.png" alt=""></button>
				<button class="close_btn"><img src="/assets/images/icon/x_black.png" alt=""></button>
			</div>
		</div>
		<div class="popup_body">
			<div class="notice_msg">
				<div class="notice_content">
					<p>정산계좌 통합관리 서비스를 이용하시려면 각 <strong class="em">은행별 홈페이지에서 빠른조회서비스 등록</strong>을 진행하신 후 계좌정보를 입력하셔야
						합니다.</p>
				</div>
			</div>
			<div class="form_container">
				<!-- <form> -->
					<div class="form_row">
						<label class="label">
							<span>은행명</span>
							<div class="links">
								<!-- <button class="link mov_guide">영상가이드</button>
								<button class="link go_selected_bank" onclick="btnPressedOpenBank('mdfy');">선택은행 바로가기</button> -->
								<span class="link mov_guide modal_link_btn" onclick="btnPressedGuideMov();">영상가이드</span>
								<span class="link go_selected_bank modal_link_btn" onclick="btnPressedOpenBank('mdfy');">선택은행 바로가기</span>
							</div>
						</label>
						<select class="form" id="mdfy_bank_cert_m_seq_no" onchange="onChangeBankSel(this, 'mdfy');">
							<c:forEach items="${bankList}" var="bank">
								<option value="${bank.bank_cert_m_seq_no}" 
									data-bankcd="${bank.bank_cd}"
									data-acctnoyn="${bank.acct_no_use_yn}"
									data-acctpwyn="${bank.acct_passwd_use_yn}"
									data-bankidyn="${bank.bank_id_use_yn}"
									data-bankpwyn="${bank.bank_passwd_use_yn}"
									data-crccd="${bank.crc_use_cd}"
									data-ctznoyn="${bank.ctz_biz_no_use_yn}">${bank.bank_nm}</option>
							</c:forEach>
						</select>
					</div>
					<div class="form_row" id="mdfy_bank_id_area">
						<label class="label">
							<span>빠른조회 은행아이디</span>
						</label>
						<input class="form" id="mdfy_bank_id" type="text" maxlength="25" onchange="changeMdfyModalData();">
						<p id="mdfy_bank_id_error" class="error input_field_error" style="display:none;">*은행아이디를 입력해주세요.</p>
					</div>
					<div class="form_row" id="mdfy_bank_passwd_area">
						<label class="label">
							<span>빠른조회 은행비밀번호</span>
						</label>
						<input class="form" id="mdfy_bank_passwd" type="password" maxlength="20" onchange="changeMdfyModalData();">
						<p id="mdfy_bank_passwd_error" class="error input_field_error" style="display:none;">*은행비밀번호 입력해주세요.</p>
					</div>
					<div class="form_row" id="mdfy_dpsi_nm_area">
						<label class="label">
							<span>예금주</span>
						</label>
						<input class="form" id="mdfy_dpsi_nm" type="text" maxlength="20" required="required" onchange="changeMdfyModalData();">
						<p id="mdfy_dpsi_nm_error" class="error input_field_error" style="display:none;">*예금주를 입력해주세요.</p>
					</div>
					<div class="form_row" id="mdfy_acct_no_area">
						<label class="label">
							<span id="mdfy_acctNoTitle">계좌번호</span>
							<p id="mdfy_checkDupl_info"></p>
						</label>
						<input class="form" id="mdfy_acct_no" type="text" style="width: 75%;" maxlength="20" autocomplete="off" data-valitype="acctNo" onchange="changeMdfyModalData();">
						<p id="mdfy_acct_no_error" class="error input_field_error" style="display:none;">*계좌번호를 바르게 입력해주세요.</p>
						<button class="button" id="mdfy_dupleChkbtn" style="position: relative; height: 5.55%; width: 18.5%; margin-left: 0.3rem; padding: 0.6rem 0.9rem;" onclick="mdfyCheckDupl()">중복 확인</button>
					</div>
					<div class="form_row" id="mdfy_acct_passwd_area">
						<label class="label">
							<span id="mdfy_acctPwdTitle">계좌 비밀번호</span>
						</label>
						<input class="form" id="mdfy_acct_passwd" type="password" autocomplete="new-password" maxlength="20" onchange="changeMdfyModalData();">
						<p id="mdfy_acct_passwd_error" class="error input_field_error" style="display:none;">*계좌비밀번호 입력해주세요.</p>
					</div>
					<div class="form_row" id="mdfy_ctz_biz_no_area">
						<label class="label">
							<span>주민등록 (사업자)번호</span>
						</label>
						<input class="form" id="mdfy_ctz_biz_no" type="text" maxlength="10" placeholder="주민번호 앞 6자리 또는 사업자번호 10자리만 입력" data-valitype="ctzBizno" onchange="changeMdfyModalData();">
						<p id="mdfy_ctz_biz_no_error" class="error input_field_error" style="display:none;">*주민등록(사업자) 번호를 바르게 입력해주세요.</p>
					</div>
					<div class="form_row" id="mdfy_crc_area">
						<label class="label">
							<span>통화</span>
						</label>
						<select class="form" id="mdfy_crc">
							<option value="">선택</option>
							<c:forEach items="${codeList}" var="code">
								<option value="${code.com_cd}">${code.cd_nm}</option>
							</c:forEach>
						</select>
						<p id="mdfy_crc_error" class="error input_field_error" style="display:none;">*통화를 선택해주세요.</p>
					</div>
					<c:if test="${cafe24_usr_yn eq 'Y' && !empty addn_goods_req_seq_no}">
						<div class="form_row notice_msg">
							<div class="notice_content">
								<p>카페24 ‘뱅크봇-대사서비스’ 이용을 원하시는 회원님은 반드시 <strong class="em">입금확인용계좌</strong>를 체크해주시기 바랍니다.
								</p>
							</div>
						</div>
						<div class="form_row" id="mdfy_reco_yn_area">
							<label class="label_form">
								<input type="checkbox" id="mdfy_reco_yn"/>
								<span>입금 확인용 계좌</span>
							</label>
						</div>
					</c:if>
				<!-- </form> -->
			</div>

			<!-- 계좌정보 입력 가이드 Start -->
			<div class="account_info_guide">
				<p>정산계좌 통합관리 서비스를 이용하시려면 각 은행별 홈페이지에서 빠른조회서비스 등록을 진행하신 후 계좌정보를 입력하셔야 합니다. 하단의 은행명을 선택해주시면 은행별 빠른조회서비스 등록
					절차를 확인하실 수 있습니다.</p>
				<div class="banking_info">
					<h3>KEB하나은행</h3>
					<div class="banking_info_section">
						<h4>▣ 개인뱅킹</h4>
						<ul>
							<li>
								<span class="order">①</span>
								<p>KEB하나은행 홈페이지(<a href="http://www.kebhana.com"
										target="_blank">http://www.kebhana.com</a>)에 접속</p>
							</li>
							<li>
								<span class="order">②</span>
								<p>공인인증서를 통한 인터넷뱅킹에 로그인</p>
							</li>
							<li>
								<span class="order">③</span>
								<p><strong>마이하나</strong> &gt; <strong>계좌정보관리</strong> &gt; <strong>빠른조회관리</strong>에서 신청
								</p>
							</li>
						</ul>
					</div>
					<div class="banking_info_section">
						<h4>▣ 기업뱅킹</h4>
						<ul>
							<li>
								<span class="order">①</span>
								<p>KEB하나은행 홈페이지(<a href="http://www.kebhana.com"
										target="_blank">http://www.kebhana.com</a>)에 접속</p>
							</li>
							<li>
								<span class="order">②</span>
								<p>공인인증서를 통한 인터넷뱅킹에 로그인 - 기업/CMS 로 분류되며, CMS인 경우 지점방문 후 신청해야 합니다.</p>
							</li>
							<li>
								<span class="order">③</span>
								<p><strong>뱅킹관리</strong> &gt; <strong>계좌관리</strong> &gt; <strong>빠른조회계좌관리</strong>에서 신청
								</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- 계좌정보 입력 가이드 End -->

		</div>
		<div class="popup_footer">
			<div class="button_row">
				<button class="button left" onclick="btnPressedTestAccount('mdfy');">계정 테스트</button>
				<button class="button primary" class="save_btn" onclick="btnPressedAcctMdfy();">저장</button>
				<button class="button close_btn">닫기</button>
				<c:choose>
					<c:when test="${delConfirm eq 'N'}">
						<div style="float: right;width: 66px;height: 1px;"></div>
					</c:when>
					<c:otherwise>
						<button class="button gray right" onclick="btnPressedAcctDel();">삭제</button>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<!-- 영상 가이드 팝업 Start -->
	<div class="account_mov_guide_container">
		<button class="mov_close_btn"><img src="/assets/images/icon/x_white.png" alt=""></button>
		<div class="mov_guide_box">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/OzaIAVrgQEo?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
				allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
				allowfullscreen></iframe>
		</div>
	</div>
	<!-- 영상 가이드 팝업 End -->
	<div class="popup_black"></div>
</div>
<!-- 계좌 수정 팝업 End -->


<!-- //20200706_10 :: TOAST 그리드 초기화 및 팝업 컨트롤 Start -->
<script>
	$(document).ready(function () {
		"use strict";

		// 요청 계좌 정보
		function fnGetAcctList() {
			// 조회 중인 항목
			var isINQ = false;
			for(var i=0; i<girdData.length; i++) {
				if(!isINQ && girdData[i].cust_acct_sts_cd == "INQ")
					isINQ = true;
			}

			if (isINQ) {
				$.get("/sub/account/acct", null, function (res) {
					if (res.acct.length > 0) {
						var list = res.acct.filter(function (item) {
							if (item.del_yn == "N" && item.cust_acct_sts_cd == 'INQ') {
								return item;
							}
						});
						if (list.length > 0) {
							setTimeout(fnGetAcctList, 10000);
						} else {
							showAlert("계좌 정보가 갱신되었습니다.", function () {
								location.href = "/sub/account/account";
							});
						}
					}
				});
			}
		}

		$(".account_sum_date_mon li").on("click", function () {
			var today = new Date();
			var jqThis = $(this);
			var id = jqThis.attr("id");
			var minusMonth = 1;

			if (id == "month2") {
				minusMonth = 2;
			} else if (id == "month3") {
				minusMonth = 3;
			}
			$("#sta_dt").val(moment(today).subtract(minusMonth, "months").add(1, "d").format(
				"YYYY/MM/DD"));
			$("#end_dt").val(moment(today).format("YYYY/MM/DD"));

			$(".account_sum_date_mon li").removeClass('account_sum_date_mon_active');
			$(this).addClass('account_sum_date_mon_active');
		});

		// 조회 이벤트
		$("#btnSearch").on("click", function () {
			fnSearch();
		});

		// 기초 데이터
		$("#sta_dt").val(moment(new Date()).subtract(1, "month").add(1, "d").format("YYYY/MM/DD"));
		$("#end_dt").val(moment(new Date()).format("YYYY/MM/DD"));
		$(".account_sum_date_dp").datepicker();
		fnSearch();
		setTimeout(fnGetAcctList, 10000);

		/**
		 * 드롭다운 컨트롤 
		 */
		$('body').on('click', '.dropdown .item', function () {
			if (!$(this).hasClass('is_selected')) {
				var currentItemLabel = $(this).text(),
					currentItemValue = $(this).data('value');
				$(this).parents('.dropdown_list').find('.item').removeClass('is_selected');
				$(this).addClass('is_selected').parents('.dropdown').find('.button .label').text(
					currentItemLabel).attr('data-value', currentItemValue);

				switch ($(this).data('value')) {
					case "manual": // 거래내역상세
						var seqNo = $(this).data('seqno');
						location.href = "/sub/account/detail?cust_acct_seq_no=" + seqNo;
						break;
					case "modify": // 계좌수정
						var seqNo = $(this).data('seqno');
						showMdfyModal(seqNo);
						break;
					case "request": // 신규조회요청
						var acctNo = $(this).data('acctno');
						var modalId = showConfirm("선택하신 은행계좌를<br><strong>신규 조회 요청</strong> 하시겠습니까?<br><small>소요시간: 약 5분</small>", function () {
							$.ajax({
								url: '/sub/account/tra/req',
								data: {acct_no: acctNo},
								type: 'post',
								async: false,
								success: function (response) {
									removeModal(modalId);
									showAlert("업데이트요청이 완료되었습니다.", function () {
										location.href = "/sub/account/account";
									});
								},
								error: function (error) {
									removeModal(modalId);
									showAlert("업데이트 요청에 실패 하였습니다.");
								}
							});
						});
						break;
					default:
						break;
				}
			}

			$(this).parents('.dropdown_list').hide();
		});
		
		/**
		 * TOAST 그리드 초기화
		 */
		var isIe = false,
			agent = navigator.userAgent.toLowerCase();
		if ( (navigator.appName == 'Netscape' && agent.indexOf('trident') != -1) || (agent.indexOf("msie") != -1)) {
			isIe = true;
		}

		var girdDataStr = '${custAcctList}';
		if(girdDataStr != null && girdDataStr != '') {
			girdData = JSON.parse(girdDataStr);
		}
		var gridBodyHeight = (41*girdData.length);
		if(gridBodyHeight < 420)
			gridBodyHeight = 420;
		
		if(girdData.length > 0) {
			var grid = new tui.Grid({
				el: document.getElementById('grid'),
				data: girdData,
				bodyHeight: gridBodyHeight,
            	showDummyRows: true,
				scrollX: true,
				scrollY: isIe,
				rowHeight: 'auto',
				columns: [{
					header: '상태',
					name: 'usr_err_yn',
					width: 50,
					align: 'center',
					renderer: {
						type: CustomIconRenderer
					}
				}, {
					header: '은행명',
					name: 'bank_nm',
					width: 100,
					align: 'center'
				}, {
					header: '계좌번호',
					name: 'acct_no',
					width: 200,
					align: 'center'
				}, {
					header: '계좌잔액',
					name: 'acct_prc',
					width: 180,
					align: 'right',
					renderer: {
						type: CustomPriceRenderer
					}
				}, {
					header: '최종거래일',
					name: 'acct_prc_mod_ts',
					width: 170,
					align: 'center'
				}, {
					header: '정산몰',
					name: 'mall_list',
					width: 150,
					align: 'center',
					renderer: {
						type: CustomMallRenderer
					}
				}, {
					header: '기능',
					name: 'cust_acct_sts_cd',
					width: 152,
					align: 'center',
					renderer: {
						type: CustomFunctionRenderer
					}
				}],
				columnOptions: {
					frozenCount: 2,
					frozenBorderWidth: 1
				}
			});
		}

		/**
		 * 팝업 컨트롤
		 */
		$('#MyAccountRegister').click(function () {
			if(!fnIsRegData($("#currentRegCount").val(), $("#maxRegCount").val())) {
				showAlert("현재 이용 중이신 서비스는<br>계좌 " + $("#maxRegCount").val() + "개까지만 등록이 가능합니다.");
				return;
			}

			showRegModal();
		});
		// $('.help_btn').click(function () {
		// 	if (!$(this).hasClass('on')) {
		// 		$(this).addClass('on');
		// 		$('.account_info_guide').show();
		// 	} else {
		// 		$(this).removeClass('on');
		// 		$('.account_info_guide').hide();
		// 	}
		// });
		$('.close_btn').click(function () {
			$(this).parents('.popup_container').fadeOut();

			//셋팅된 정보 초기화
			$("#reg_checkDupl_info").text("");
			$("#chkAcctDuplYN").val("");
			$("#mdfy_checkDupl_info").text("");
			$("#chkAcctDuplYN").val("");
		});
		// $('.mov_guide').click(function (e) {
		// 	e.preventDefault();
		// 	$('.account_mov_guide_container').show();
		// 	$('.popup_box').fadeOut();
		// });
		$('.mov_close_btn').click(function () {
			$('.account_mov_guide_container').fadeOut();
			$('.popup_box').show();
			document.querySelectorAll(".mov_guide_box iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
			document.querySelectorAll(".mov_guide_box iframe")[1].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
		});

		$("#reg_acct_no").keyup(function () { 
			$("#reg_checkDupl_info").text("");
			$("#chkAcctDuplYN").val("");
		});

		$("#mdfy_acct_no").keyup(function () { 
			$("#mdfy_checkDupl_info").text("");
			$("#chkAcctDuplYN").val("");
		});
	});
</script>
<!-- //20200706_10 :: TOAST 그리드 초기화 및 팝업 컨트롤 End -->
<style>
    .modal_link_btn {
		font-size: 0.875rem !important;
		color: #598ae2 !important;
		border-bottom: 1px solid #598ae2;
		cursor: pointer;
	}
	.input_field_error {
		font-size: 0.875rem;
	}
</style>