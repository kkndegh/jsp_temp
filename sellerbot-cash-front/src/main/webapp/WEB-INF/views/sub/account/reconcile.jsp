<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style media="screen">
    .item_signup_info:after {
        display: none;
    }
</style>

<script src="/assets/js/common/common.js"></script>
<script src="/assets/js/tuiGridCommon.js"></script>
<script src="/assets/js/reconcile.js"></script>

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-pagination.js"></script>
<script src="/assets/js/tui-grid.js"></script>

<input type="hidden" name="cust_acct_seq_no" id="cust_acct_seq_no" value="${cust_acct_seq_no}">
<input type="hidden" name="startDT" id="startDT" value="${startDT}">
<input type="hidden" name="endDT" id="endDT" value="${endDT}">
<input type="hidden" name="acctTraCont" id="acctTraCont" value="${acctTraCont}">
<input type="hidden" name="recoStsCd" id="recoStsCd" value="${recoStsCd}">
<input type="hidden" name="page" id="page">
<input type="hidden" name="acct_tra_cont_seq_no" id="acct_tra_cont_seq_no">
<!-- <input type="hidden" name="ord_seq_no" id="ord_seq_no"> -->

<div class="container">
    <div class="account_wra">
        <div class="menu_top">
            <p class="menu_name">정산계좌 통합관리</p>
            <!-- //20200706_10 :: 메뉴 변경 Start -->
            <ul class="gnb_top clearfix">
                <li><a href="/sub/account/account">정산계좌</a></li>
                <li><a href="/sub/account/detail">거래내역 조회</a></li>
                <li class="focus"><a href="/sub/account/reconcile">대사 서비스</a></li>
            </ul>
            <!-- //20200706_10 :: 메뉴 변경 End -->
        </div>
        <!--account_section_end -->
        <!-- //20200706_10 :: 수정 Start -->

        <div class="account_section">
            <div class="grid_container">
                <div class="grid_header">
                    <div class="grid_row">
                        <h2 class="title">대사 목록</h2>
                    </div>
                    <div class="search_box">
                        <ul>
                            <li class="col-6">
                                <span class="label">계좌번호</span>
                                <div class="form_container">
                                    <select id="bankSelect" class="account_sum_bank_sel" name="bank"
                                        placeholder="계좌번호를 선택하세요.">
                                        <option value="">계좌번호를 선택하세요.</option>
                                        <c:forEach var="acct" items="${custAcctList}" varStatus="status">
                                            <option value="${acct.cust_acct_seq_no}" id="${acct.bank_cd}_${acct.acct_no}">${acct.bank_nm} ${acct.acct_no}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </li>
                            <li class="col-6">
                                <span class="label">입금자 명</span>
                                <div class="form_container">
                                    <input type="text" class="form" id="acctTraContInput">
                                </div>
                            </li>
                            <li>
                                <span class="label">조회기간</span>
                                <div class="form_container">
                                    <div class="dropdown_selector">
                                        <div class="dropdown per_day">
                                            <button class="button line">
                                                <span class="label">일별</span>
                                                <img src="/assets/images/icon/icon_bottom.png" alt="">
                                            </button>
                                            <div class="dropdown_list"></div>
                                        </div>
                                        <div class="dropdown per_month">
                                            <button class="button line">
                                                <span class="label">월별</span>
                                                <img src="/assets/images/icon/icon_bottom.png" alt="">
                                            </button>
                                            <div class="dropdown_list"></div>
                                        </div>
                                        <div class="dropdown per_quarter">
                                            <button class="button line">
                                                <span class="label">분기별</span>
                                                <img src="/assets/images/icon/icon_bottom.png" alt="">
                                            </button>
                                            <div class="dropdown_list"></div>
                                        </div>
                                        <div class="dropdown per_year">
                                            <button class="button line">
                                                <span class="label">연도별</span>
                                                <img src="/assets/images/icon/icon_bottom.png" alt="">
                                            </button>
                                            <div class="dropdown_list"></div>
                                        </div>
                                    </div>
                                    <div class="account_sum_date_sel">
                                        <input id="sta_dt" type="text" class="account_sum_date_dp"> ~ <input id="end_dt"
                                            type="text" class="account_sum_date_dp">
                                        <!-- <input class="account_detail_search" type="text" id="searchWord" name="searchWord"
                                            placeholder="거래내용" /> -->
                                    </div>
                                </div>
                                <!-- <span class="label">조회기간</span>
                                <div class="form_container">
                                    <div class="range_selector">
                                        <button class="button line">전일</button>
                                        <button class="button line">당일</button>
                                        <button class="button line">1주일</button>
                                        <button class="button line">1개월</button>
                                        <button class="button line">3개월</button>
                                    </div>
                                    <div class="account_sum_date_sel">
                                        <input id="sta_dt" type="text" class="account_sum_date_dp"> ~ <input id="end_dt" type="text"
                                            class="account_sum_date_dp">
                                    </div>
                                </div> -->
                            </li>
                            <li>
                                <span class="label">대사 현황</span>
                                <div class="form_container">
                                    <label class="label_form">
                                        <input type="checkbox" name="reco_sts_cd_cb" data-sts-cd="IDEP" checked>
                                        <span>자동</span>
                                    </label>
                                    <label class="label_form">
                                        <input type="checkbox" name="reco_sts_cd_cb" data-sts-cd="MDEP" checked>
                                        <span>수동</span>
                                    </label>
                                    <label class="label_form">
                                        <input type="checkbox" name="reco_sts_cd_cb" data-sts-cd="UDEP">
                                        <span>미확인</span>
                                    </label>
                                    <label class="label_form">
                                        <input type="checkbox" name="reco_sts_cd_cb" data-sts-cd="EXCL" checked>
                                        <span>대사 제외</span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                        <div class="search_submit">
                            <button id="detailSearchbtn" class="button gray account_sum_date_sub">조회</button>
                        </div>
                    </div>
                </div>

                <div class="grid_body" style="display: none;">
                    <div id="grid"></div>
                    <div class="table_pager_wrapper">
                        <div id="grid-pagination-container" class="tui-pagination"></div>
                    </div>
                </div>
                <div class="list_register_account_noData" style="display: block;">
                    <h1>데이터가 없습니다.</h1>
                </div>
            </div>
        </div>

    </div>
    <!-- //20200706_10 :: 수정 End -->
    <!--account_wra_end -->
    <div class="banner_payment">
        <div class="area_banner_payment">
            <div class="bf_hand_tlt">
                <img src="/assets/images/payment/question_Icon.png" alt="">
                <p><b>미리 정산받을 수 있는 금액</b><br>
                    내가 지원받을 수 있는 금액
                </p>
            </div>
            <div class="price_bfhand">
                <p>최대 <b>
                        <fmt:formatNumber value="${preSett}" pattern="#,###" /></b>원</p>
            </div>
            <a href="/sub/pre_calculate/pre_calculate" class="more_bfhand">자세히보기</a>
        </div>
    </div>
</div>



<!-- //20200706_10 :: 자동대사 변경 팝업 Start -->
<div class="popup_container auto_reconcile">
    <div class="popup_box fluid">
        <div class="popup_header">
            <h1>자동대사 변경</h1>
            <div class="button_container">
                <button class="close_btn"><img src="/assets/images/icon/x_black.png" alt=""></button>
            </div>
        </div>
        <div class="popup_body">
            <div class="notice_msg">
                <div class="notice_content">
                    <p>선택한 자동대사를 미확인 처리 합니다.</p>
                </div>
            </div>
            <div class="form_container">
                <form>
                    <div class="form_row">
                        <label class="label required">
                            <span>입금내역</span>
                        </label>
                        <input class="form" type="text" id="depHistInput" readonly>
                    </div>
                    <div class="form_row">
                        <label class="label required">
                            <span>주문내역</span>
                        </label>
                        <input class="form" type="text" id="ordHistInput" readonly>
                    </div>
                    <!-- <div class="form_row">
                        <label class="label required">
                            <span>무료체험 일자</span>
                        </label>
                        <div class="account_sum_date_sel">
                            <input id="sta_dt" type="text" class="account_sum_date_dp"> ~ <input id="end_dt" type="text"
                                class="account_sum_date_dp">
                        </div>
                    </div> -->
                    <div class="form_row">
                        <label class="label_form">
                            <!--input type="checkbox" id="agreeCb"/-->
                            <!--span>카페24 입금 현황을 미확인으로 변경</span-->
                            <span>* 카페24의 입금상태는 변경 되지 않습니다.<br>* 자동(입금)확인에서 미확인으로 변경 됩니다.</span>
                        </label>
                    </div>
                </form>
            </div>
        </div>
        <div class="popup_footer">
            <div class="button_row">
                <button class="button primary" class="save_btn" onclick="chgRecoSts();">저장</button>
                <button class="button close_btn">취소</button>
            </div>
        </div>
    </div>
</div>
<!-- //20200706_10 :: 자동대사 변경 팝업 End -->





<!-- //20200706_10 :: 수동입금 확인 팝업 Start -->
<div class="popup_container menual_deposit_check" style="z-index: 9998;">
    <div class="popup_box full" style="height: 90vh;width: 125vh; max-width: 90%;">
        <div class="popup_header">
            <h1>수동입금 및 대사제외 등록</h1>
            <div class="button_container">
                <button class="close_btn"><img src="/assets/images/icon/x_black.png" alt=""></button>
            </div>
        </div>
        <div class="popup_body">

            <div class="notice_msg">
                <div class="notice_content" style="margin: 0 0 7px;">
                    <p>자동확인 되지 않은 입금건에 대하여 '수동입금'을 확인하시거나, '대사제외' 할 수 있습니다.</p>
                </div>
                <div class="notice_content" style="margin: 0 10px;">
                    <p style="color: #A4A4A3;">▣ 수동입금 방법 : 해당 입금내역에 맞는 '주문건'을 직접 선택하고, <strong style="color: blue;">입금확인</strong> 하기</p>
                    <p style="color: #A4A4A3;">▣ 대사제외 방법 : 해당 입금내역에 주문건과 일치시키지 않고, <strong style="color: blue;">대사처리</strong>에서 <strong style="color: blue;">제외</strong> 하기</p>
                </div>
            </div>

            <div class="select_mov_table_container">
                <div class="col search_item">
                    <div class="col_header">
                        <h3>거래내역</h3>
                        <!-- 2021-08-09 뱅크봇 프로모션 기획 UI 변경
                        <div class="table_filter">
                            <div class="form_box">
                                <select class="form" id="depSearchSel" onchange="resetDepSearchInput();">
                                    <option value="">검색 선택</option>
                                    <option value="bank_nm">은행</option>
                                    <option value="acct_no">계좌번호</option>
                                    <option value="depositor">입금자</option>
                                    <option value="depo_prc">입금 금액</option>
                                </select>
                            </div>
                            <div class="form_box">
                                <input type="text" class="form" id="depSearchInput" placeholder="검색어">
                            </div>
                            <button class="button gray search_submit" onclick="btnPressedDep();"><img src="/assets/images/notice/btn_search.png" alt="검색"></button>
                        </div>
                        -->
                    </div>
                    <div style="margin: -15px 0px 10px;">
                        <p>해당 거래내역 정보를 확인하신 후에 <strong>수동입금 확인</strong> 또는 <strong>대사제외</strong>하실 거래내역을 '<strong style="color: blue;">선택</strong>' 해주세요.</p>
                    </div>
                    <div class="table_container">
                        <table>
                            <thead>
                                <tr>
                                    <th class="date">입금 일자</th>
                                    <th class="bank">은행</th>
                                    <th class="account">입금계좌번호</th>
                                    <th class="name">입금자</th>
                                    <th class="amount">입금 금액</th>
                                    <th class="select">선택</th>
                                </tr>
                            </thead>
                            <tbody id="depHistTbody">
                                <tr>
                                    <td colspan="6">조회된 결과가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table_paging" style="display: none;">
                        <div class="table_pager_wrapper">
                            <div id="dep-pagination-container" class="tui-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="select_mov_table_container">
                <div class="col search_item">
                    <div class="col_header">
                        <h3>주문내역 검색</h3>
                        <div class="table_filter">
                            <div class="form_box">
                                <select class="form" id="ordSearchSel" onchange="resetOrdSearchInput();">
                                    <option value="">검색 선택</option>
                                    <option value="ord_seq_no">주문번호</option>
                                    <option value="bank_cd_nm">은행</option>
                                    <option value="bank_acct_no">주문계좌번호</option>
                                    <option value="buyer_nm">주문자</option>
                                    <option value="paid_amt">주문 금액</option>
                                </select>
                            </div>
                            <div class="form_box">
                                <input type="text" class="form" id="ordSearchInput" placeholder="검색어">
                            </div>
                            <button class="button gray search_submit" onclick="btnPressedOrd();"><img src="/assets/images/notice/btn_search.png" alt="검색"></button>
                        </div>
                    </div>
                    <div style="margin: -5px 0px 10px;">
                        <p><strong>검색 화면</strong>에서 카페24의 <strong>주문번호, 주문자, 주문금액</strong> 중에 먼저 선택하신 후에 주문건을 검색해주세요</p>
                        <p>검색된 주문 내역에서 해당 입금건과 <strong style="color: blue;">일치한 주문건</strong>을 '<strong style="color: blue;">선택</strong>'해주세요</p>
                    </div>
                    <div class="table_container">
                        <table>
                            <thead>
                                <tr>
                                    <th class="orderNumber">주문번호</th>
                                    <th class="bank">은행</th>
                                    <th class="account">주문계좌번호</th>
                                    <th class="name">주문자</th>
                                    <th class="amount">주문 금액</th>
                                    <th class="select">선택</th>
                                </tr>
                            </thead>
                            <tbody id="ordHistTbody">
                                <tr>
                                    <td colspan="6">조회된 결과가 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table_paging">
                        <div class="table_pager_wrapper">
                            <div id="ord-pagination-container" class="tui-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="popup_footer">
            <div class="button_row">
                <button class="button primary excl_save_btn" onclick="exclRecoSts();">대사 제외</button>
                <button class="button primary save_btn" onclick="matchingDepNOrd();">입금 확인</button>
                <button class="button close_btn">취소</button>
            </div>
        </div>
    </div>
</div>
<!-- //20200706_10 :: 수동입금 확인 팝업 End -->





<!-- //20200706_10 :: TOAST 그리드 초기화 Start -->
<script>
    $(document).ready(function () {
        "use strict";

        initReconcile();

        /**
         * 드롭다운 컨트롤 
         */
        $('.dropdown_selector .dropdown').hover(function () {
            $(this).find('.dropdown_list').show();
        }, function () {
            $(this).find('.dropdown_list').hide();
        });
        $('body').on('click', '.dropdown .item', function () {
            if (!$(this).hasClass('is_selected')) {
                onChangeSel($(this));
            }
            
            $(this).parents('.dropdown_list').hide();
        }); // 팝업 닫기 버튼 컨트롤

        $('.close_btn').click(function () {
            $(this).parents('.popup_container').hide();
        });
        /**
         * 수동입금 확인 목록 이동
         */
        // $('.select_mov_table_container .row_select_btn').click(function () {
        //     var parent = $(this).parents('.select_mov_table_container'),
        //         row = $(this).parents('tr'),
        //         currentDate = row.find('.date').text(),
        //         currentOrderNumber = row.find('.orderNumber').text(),
        //         currentBank = row.find('.bank').text(),
        //         currentAccount = row.find('.account').text(),
        //         currentName = row.find('.name').text(),
        //         currentAmount = row.find('.amount').text();
        //     var checkOrder; // 선택된 목록 체크

        //     parent.find('.selected_item .orderNumber').each(function () {
        //         if ($(this).text() == currentOrderNumber) {
        //             checkOrder = true;
        //             return;
        //         }
        //     });

        //     if (checkOrder) {
        //         alert('이미 선택된 목록이 있습니다.');
        //         return;
        //     }

        //     $(this).parents('tr').addClass('selected');
        //     parent.find('.selected_item tbody').append('<tr>\
        //             <td class="delete">\
        //                 <button class="row_delete_btn">\
        //                     <img src="/assets/images/icon/x_black.png" alt="">\
        //                 </button>\
        //             </td>\
        //             <td class="orderNumber">' + currentOrderNumber + '</td>\
        //             <td class="date">' + currentDate + '</td>\
        //             <td class="bank">' + currentBank + '</td>\
        //             <td class="account">' + currentAccount + '</td>\
        //             <td class="name">' + currentName + '</td>\
        //             <td class="amount">' + currentAmount + '</td>\
        //         </tr>');
        // }); // 수동입금 확인 목록 삭제

        // $('body').on('click', '.row_delete_btn', function () {
        //     var orderNumber = $(this).parents('tr').find('.orderNumber').text();
        //     $(this).parents('.select_mov_table_container').find('.search_item .orderNumber').each(
        //         function () {
        //             if ($(this).text() == orderNumber) {
        //                 $(this).parents('tr').removeClass('selected');
        //                 return;
        //             }
        //         });
        //     $(this).parents('tr').remove();
        // });
    });
</script>
<!-- //20200706_10 :: TOAST 그리드 초기화 End -->
<script>
    function formatData(data) {
        if (!data.id) {
            return data.text;
        }
        var baseUrl = "/assets/images/bank/";

        var idArray = data.element.id.split("_");
        var $result = $(
            '<img style="display:inline-block; vertical-align: middle; margin-right: 0.5rem;" src="' + baseUrl +
            idArray[0] + '.png" />' + '<span style="display:inline-block; vertical-align:middle;"> ' + data.text +
            '</span>'
        );

        return $result;
    };

    $('.account_sum_bank_sel').select2({
        minimumResultsForSearch: -1,
        templateResult: formatData,
        templateSelection: formatData
    });
</script>
<style>
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 0 !important;
        margin-left: 1rem;
    }

    .select2-container {
        width: 18rem !important;
        height: 2.5rem;
        line-height: 2.5rem;
        margin-right: 1rem;
        margin-top: -4px;
    }

    @media(max-width:962px) {
        .select2-container {
            width: 300px !important;
        }

    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 26px;
        position: absolute;
        top: 10px;
        right: 1px;
        width: 20px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 2.5rem !IMPORTANT;
        text-align: left;
    }

    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 0px;
        height: auto;
        line-height: 3rem;
    }
</style>
<style>
    /*paging*/
    .tui-pagination {
        padding-top: 1rem;
    }

    .tui-pagination .tui-is-selected,
    .tui-pagination strong {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-is-selected:hover {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-first-child.tui-is-selected {
        border: 1px solid #a4a4a4;
    }
</style>
<script>
    $(document).ready(function () {
        
        if($("#cust_acct_seq_no").val() != '') {
            $("#bankSelect").val($("#cust_acct_seq_no").val()).trigger('change');
            $("#sta_dt").val($("#startDT").val());
            $("#end_dt").val($("#endDT").val());

            if($("#acctTraCont").val() != "") {
                $("#acctTraContInput").val($("#acctTraCont").val());
            }

            search(1);
        } else {
            $("#bankSelect option:eq(1)").prop("selected", true).trigger('change');
            $("#detailSearchbtn").trigger('click');
        }
    });

    // 조회하기
    $("#detailSearchbtn").click(function () {
        search(1);
    });
</script>