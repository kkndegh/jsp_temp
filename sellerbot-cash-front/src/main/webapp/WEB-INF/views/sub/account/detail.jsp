<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style media="screen">
    .item_signup_info:after {
        display: none;
    }
</style>

<script src="/assets/js/common/common.js"></script>
<script src="/assets/js/tuiGridCommon.js"></script>
<script src="/assets/js/acctDetail.js?ver=20200831_01"></script>

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-pagination.js"></script>
<script src="/assets/js/tui-grid.js"></script>

<input type="hidden" name="cust_acct_seq_no" id="cust_acct_seq_no" value="${cust_acct_seq_no}">
<input type="hidden" name="page" id="page">

<div class="container">
    <div class="account_wra">
        <div class="menu_top">
            <p class="menu_name">정산계좌 통합관리</p>
            <!-- //20200706_10 :: 메뉴 변경 Start -->
            <ul class="gnb_top clearfix">
                <li><a href="/sub/account/account">정산계좌</a></li>
                <li class="focus"><a href="/sub/account/detail">거래내역 조회</a></li>
                <c:if test="${cafe24_usr_yn eq 'Y' or shopby_usr_yn eq 'Y'}">
					<li><a href="/sub/account/reconcile">대사 서비스</a></li>
				</c:if>
            </ul>
            <!-- //20200706_10 :: 메뉴 변경 End -->
        </div>
        <!--account_section_end -->
        <!-- //20200706_10 :: 수정 Start -->

        <div class="account_section">
            <div class="grid_container">
                <div class="grid_header">
                    <div class="grid_row">
                        <h2 class="title">거래내역 조회</h2>
                    </div>
                    <div class="search_box">
                        <ul>
                            <li>
                                <span class="label">계좌번호</span>
                                <div class="form_container">
                                    <select id="bankSelect" class="account_sum_bank_sel" name="bank"
                                        placeholder="계좌번호를 선택하세요.">
                                        <option value="">계좌번호를 선택하세요.</option>
                                        <c:forEach var="acct" items="${custAcctList}" varStatus="status">
                                            <option value="${acct.cust_acct_seq_no}" id="${acct.bank_cd}_${acct.acct_no}">${acct.bank_nm} ${acct.acct_no}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <span class="label">조회기간</span>
                                <div class="form_container">
                                    <div class="dropdown_selector">
                                        <div class="dropdown per_day">
                                            <button class="button line">
                                                <span class="label">일별</span>
                                                <img src="/assets/images/icon/icon_bottom.png" alt="">
                                            </button>
                                            <div class="dropdown_list"></div>
                                        </div>
                                        <div class="dropdown per_month">
                                            <button class="button line">
                                                <span class="label">월별</span>
                                                <img src="/assets/images/icon/icon_bottom.png" alt="">
                                            </button>
                                            <div class="dropdown_list"></div>
                                        </div>
                                        <div class="dropdown per_quarter">
                                            <button class="button line">
                                                <span class="label">분기별</span>
                                                <img src="/assets/images/icon/icon_bottom.png" alt="">
                                            </button>
                                            <div class="dropdown_list"></div>
                                        </div>
                                        <div class="dropdown per_year">
                                            <button class="button line">
                                                <span class="label">연도별</span>
                                                <img src="/assets/images/icon/icon_bottom.png" alt="">
                                            </button>
                                            <div class="dropdown_list"></div>
                                        </div>
                                    </div>
                                    <div class="account_sum_date_sel">
                                        <input id="sta_dt" type="text" class="account_sum_date_dp"> ~ <input id="end_dt"
                                            type="text" class="account_sum_date_dp">
                                        <!-- <input class="account_detail_search" type="text" id="searchWord" name="searchWord"
                                            placeholder="거래내용" /> -->
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="search_submit">
                            <button id="detailSearchbtn" class="button gray account_sum_date_sub">조회</button>
                        </div>
                    </div>
                </div>

                <div class="grid_body" style="display: none;">

                    <!-- 거래내역 정보 테이블 Start -->
                    <div class="grid_info_table">
                        <!-- <table>
                            <tbody>
                                <tr>
                                    <th>은행</th>
                                    <td></td>
                                    <th>계좌번호</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>잔액</th>
                                    <td></td>
                                    <th>조회기간</th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table> -->
                    </div>
                    <!-- 거래내역 정보 테이블 End -->

                    <div id="grid"></div>
                    <!-- <c:if test="${ (cusAcct.all_acct_cnt - cusAcct.del_acct_cnt) == 0}">
                        <div class="list_register_account_noData" style="display: block;">
                            <h1>등록된 계좌가 없습니다.</h1>
                        </div>
                    </c:if> -->
                    <div class="table_pager_wrapper">
                        <div id="grid-pagination-container" class="tui-pagination"></div>
                    </div>
                </div>
                <div class="list_register_account_noData" style="display: block;">
                    <h1>데이터가 없습니다.</h1>
                </div>
            </div>
        </div>

    </div>
    <!-- //20200706_10 :: 수정 End -->
    <!--account_wra_end -->
    <div class="banner_payment">
        <div class="area_banner_payment">
            <div class="bf_hand_tlt">
                <img src="/assets/images/payment/question_Icon.png" alt="">
                <p><b>미리 정산받을 수 있는 금액</b><br>
                    내가 지원받을 수 있는 금액
                </p>
            </div>
            <div class="price_bfhand">
                <p>최대 <b>
                        <fmt:formatNumber value="${preSett}" pattern="#,###" /></b>원</p>
            </div>
            <a href="/sub/pre_calculate/pre_calculate" class="more_bfhand">자세히보기</a>
        </div>
    </div>
</div>


<!-- //20200706_10 :: TOAST 그리드 초기화 Start -->
<script>
    $(document).ready(function () {
        "use strict";

        initDetail();

        /**
         * 드롭다운 컨트롤 
         */
        $('.dropdown_selector .dropdown').hover(function () {
            $(this).find('.dropdown_list').show();
        }, function () {
            $(this).find('.dropdown_list').hide();
        });
        $('body').on('click', '.dropdown .item', function () {
            if (!$(this).hasClass('is_selected')) {
                onChangeSel($(this));
            }

            $(this).parents('.dropdown_list').hide();
        }); // 직접입력 취소

        $('body').on('click', '.memo .edit_cancel', function () {
            $(this).parents('.memo').find('.dropdown').show();
            $(this).parents('.memo').find('.self_edit').remove();
            $(this).remove();
        });
        
        /**
         * TOAST 그리드 초기화
         */
        // var grid = new tui.Grid({
        //     el: document.getElementById('grid'),
        //     data: girdData,
        //     // pageOptions: {
        //     //     perPage: 10
        //     // },
        //     scrollX: true,
        //     scrollY: false,
        //     columns: [{
        //         header: '거래일시',
        //         name: 'tra_dt',
        //         align: 'center',
        //         width: 140
        //     }, {
        //         header: '적용',
        //         name: 'apply',
        //         align: 'center',
        //         width: 100
        //     }, {
        //         header: '기재내용',
        //         name: 'write',
        //         align: 'center',
        //         width: 200
        //     }, {
        //         header: '찾으신 금액',
        //         name: 'withdraw',
        //         align: 'center',
        //         width: 120
        //     }, {
        //         header: '맡기신 금액',
        //         name: 'save',
        //         align: 'center',
        //         width: 120
        //     }, {
        //         header: '거래 후 잔액',
        //         name: 'balance',
        //         align: 'center',
        //         width: 120
        //     }, {
        //         header: '취급점',
        //         name: 'store',
        //         align: 'center',
        //         width: 100
        //     }, {
        //         header: '메모',
        //         name: 'memo',
        //         align: 'center',
        //         width: 102,
        //         renderer: {
        //             type: CustomMemoRenderer
        //         }
        //     }],
        //     columnOptions: {
        //         frozenCount: 2,
        //         frozenBorderWidth: 1
        //     }
        // });
    });
</script>
<!-- //20200706_10 :: TOAST 그리드 초기화 End -->

<script>
    function formatData(data) {
        if (!data.id)
            return data.text;

        var baseUrl = "/assets/images/bank/";
        var idArray = data.element.id.split("_");
        var $result = $(
            '<img style="display:inline-block; vertical-align: middle; margin-right: 0.5rem;" src="' + baseUrl +
            idArray[0] + '.png" />' + '<span style="display:inline-block; vertical-align:middle;"> ' + data.text +
            '</span>'
        );

        return $result;
    };

    $('.account_sum_bank_sel').select2({
        minimumResultsForSearch: -1,
        templateResult: formatData,
        templateSelection: formatData
    });
</script>
<style>
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 0 !important;
        margin-left: 1rem;
    }

    .select2-container {
        width: 18rem !important;
        height: 2.5rem;
        line-height: 2.5rem;
        margin-right: 1rem;
        margin-top: -4px;
    }

    @media(max-width:962px) {
        .select2-container {
            width: 300px !important;
        }

    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 26px;
        position: absolute;
        top: 10px;
        right: 1px;
        width: 20px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 2.5rem !IMPORTANT;
        text-align: left;
    }

    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 0px;
        height: auto;
        line-height: 3rem;
    }

    /* .memo {
        white-space: pre-line;
    } */
</style>
<style>
    /*paging*/
    .tui-pagination {
        padding-top: 1rem;
    }

    .tui-pagination .tui-is-selected,
    .tui-pagination strong {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-is-selected:hover {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-first-child.tui-is-selected {
        border: 1px solid #a4a4a4;
    }
</style>
<!-- 정산계좌 상세내역 화면용 스크립트 -->
<script>
    $(document).ready(function () {
        if($("#cust_acct_seq_no").val() != '') {
            $("#bankSelect").val($("#cust_acct_seq_no").val()).trigger('change');
            search(1);
        }
    });

    // 조회하기
    $("#detailSearchbtn").click(function () {
        search(1);
    });
</script>