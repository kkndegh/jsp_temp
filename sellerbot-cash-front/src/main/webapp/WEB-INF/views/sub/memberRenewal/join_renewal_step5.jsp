<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
    <link rel="stylesheet" href="/assets/new_join/css/step_05.css">
    <script type="module" src="/assets/new_join/js/step_05.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://uicdn.toast.com/editor/latest/toastui-editor-viewer.css" />
    <script src="https://uicdn.toast.com/editor/latest/toastui-editor-viewer.js"></script>
</head>
<body>
    <main>
        <section>
            <input type="hidden" id="currentRegCount" value="0">
            <button class="fixed active" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>서비스 정보 등록</h1>
                    <span id="mallGuide">판매몰 등록가이드</span>
                </div>
                <div class="processBar">
                    <div class="first step active">
                        <span class="line"></span>
                        <p>판매몰 등록</p>
                    </div>
                    <div class="second step">
                        <span class="line"></span>
                        <p>결제수단 등록</p>
                    </div>
                    <div class="third step">
                        <span class="line"></span>
                        <p>계좌 등록</p>
                    </div>
                </div>
            </article>
            <div id="wrap">
                <button id="goTop">
                    <i class="fa-solid fa-caret-up"></i>
                    <p>TOP</p>
                </button>
                <div class="myModal">
                    <div class="content">
                        <div class="top">
                            <p class="modalTitle">제목</p>
                            <button id="title_closeModal"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
                        </div>
                        <button id="closeModal"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
                        <div class="main">
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem quo distinctio esse laudantium
                                vero
                                dolores quasi, magnam tenetur ratione repellat delectus ipsam, recusandae vel soluta amet
                                consectetur voluptatum? Veniam, adipisci?</p>
                        </div>
                        <div class="bottom">
                        </div>
                    </div>
                </div>
                <article id="article_0">
                    <p class="subTitle">등록한 판매몰</p>
                    <p>메가봇 이용권을 선택하신 경우, 판매몰 5개 이상 등록 시 <b>기가봇으로 자동 변경</b>됩니다.</p>
                    <span class="line"></span>
                    <!-- <c:if test="${fn:length(selectList) > 0}"> -->
                        <button class="black kbBtn">금융 상품 판매몰 관리</button>
                    <!-- </c:if> -->
                    <div class="listTopFlex">
                        <div class="statusBtns">
                            <button class="color_black on" id="ALL">전체<btn-num id="all_cnt">0</btn-num></button>
                            <button class="color_blue" id="NR">정상<btn-num id="nr_cnt">0</btn-num></button>
                            <button class="color_gray" id="RUN">수집중<btn-num id="run_cnt">0</btn-num></button>
                            <button class="color_silver" id="RDY">대기<btn-num id="rdy_cnt">0</btn-num></button>
                            <button class="color_red" id="ERR">오류<btn-num id="err_cnt">0</btn-num></button>
                            <button class="color_yellow" id="INS">점검<btn-num id="ins_cnt">0</btn-num></button>
                            <button class="color_purple" id="OCM">오픈예정<btn-num id="ocm_cnt">0</btn-num></button>
                        </div>
                    </div>
                    <div class="listHead">
                        <dt>상태</dt>
                        <dt>판매몰</dt>
                        <dt>아이디</dt>
                        <dt>부가정보</dt>
                        <dt>제휴서비스</dt>
                        <dt>TIP</dt>
                        <dt>관리</dt>
                    </div>
                    <div class="listWrap scrollable">
                        <ul></ul>
                        <ol></ol>
                    </div>
                    <div class="listBottom">
                        <button id="showListBtn"><b>전체보기</b></button>
                    </div>
                </article>
                <article id="article_1">
                    <p class="subTitle">지원 판매몰 리스트</p>
                    <span class="line"></span>
                    <div class="gridTopFlex">
                        <div class="left">
                            <input type="text" placeholder="판매몰 검색" class="market_box_find_text_area">
                            <button id="searchText"><img src="/assets/pubMarket/img/magnifier.svg" alt="search"></button>
                        </div>
                        <div class="right">
                            <button id="beOpenedBtn" class="black">오픈예정몰(사전등록)</button>
                            <a id="guideBtn" href="https://www.sellerbot.co.kr/static/guide/index.html" target="_blank">
                                <button class="black">등록가이드</button>
                            </a>
                        </div>
                    </div>
                    <div id="top10" class="grayBox scrollable">
                        <p class="gridTitle">TOP10</p>
                        <div class="gridArea"></div>
                    </div>
                    <div id="allMalls" class="grayBox scrollable">
                        <p class="gridTitle">이름순</p>
                        <div class="gridArea">
                            
                        </div>
                    </div>
                </article>
                <article id="article_2">
                    <p class="subTitle">판매몰 등록/수정</p>
                    <span class="line"></span>
                    <div class="flexArea">
                        <div class="left">
                            <div class="contents scrollable">
                                <h3 class="noDataText"> 등록할 판매몰을<br>[ 지원 판매몰 리스트 ] 에서 선택해주세요. </h3>
                                <div class="modifyArea">
            
                                </div>
                                <div class="registerArea">
                                    
                                </div>
                            </div>
                            <div class="btns">
                                <button id="initialize">초기화</button>
                                <button id="updateAll">한 번에 업데이트</button>
                            </div>
                        </div>
                        <div class="right">
                            <div class="topTitle">
                                <p>판매몰 도움말이 없습니다.</p>
                                <button class="reg" style="display:none;">등록 방법</button>
                            </div>
                            <!-- <div class="contents scrollable" id="helpInfo"></div> -->
                            <div class="contents scrollable" id="viewer"></div>
                            <div class="btns">
                                <button id="videoGuide">영상가이드</button>
                                <button id="showDetail">상세보기</button>
                            </div>
                        </div>
                    </div>
                </article>
                <article id="article_3">
                    <div class="grayBox">
                        <pre><b>[확인사항]</b>
1. 판매몰 등록 결과를 확인하는 데는 최대 1시간까지 소요될 수 있습니다. (결과는 알림톡 발송)
2. 스마트스토어와 네이버페이(결제형 /주문형) 정보 입력 시 혼동하지 않도록 유의바랍니다.
3. 탈퇴를 원하시면 <b><a style="cursor: pointer;" href="/sub/my/leave_frm">여기</a></b>를 클릭해주세요.</pre>
                    </div>
                </article>
                <div class="bottomBar">
                    <div class="closeBtnBox">
                        <button id="closeBottomBar"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
                    </div>
                    <div class="centerBox">
                        <p>현재 선택한 판매몰은 <b id="total">0</b>건 입니다. (<text-highlight>등록 <b id="reg">0</b>건 / 수정 <b id="mod">0</b>건</text-highlight>)<br>아래 판매몰 등록/수정에서 입력을 완료해주세요.</p>
                        <div class="btns">
                            <button id="hideBottomBar">닫기</button>
                            <button id="goto">바로가기</button>
                        </div>
                    </div>
                </div>
            
                <div style="display: none;">
                    <c:if test="${not empty loanBatchProTgtInfo.batchProTgt_list }">
                        <c:set var="batPrcsTgtSeqNo" value="${loanBatchProTgtInfo.batchProTgt_list[0].bat_prcs_tgt_seq_no }" />
                    </c:if>
                    <c:forEach items="${loanBatchProTgtInfo.loanCustMall_list}" var="loanInfo" varStatus="status">
                        <input type="checkbox" name="loan_chk" class="loan_cust_mall"
                                data-cust-seq-no="${loanInfo.cust_seq_no}"
                                data-cust-mall-seq-no="${loanInfo.cust_mall_seq_no}" 
                                data-mall-cd="${loanInfo.mall_cd}"
                                data-bat-prcs-tgt-seq-no="${batPrcsTgtSeqNo}"
                                data-loan-sts-cd="${loanInfo.loan_sts_cd}"
                                data-loan-svc-descp="${loanInfo.svc_descp}"
                                <c:if test="${not empty loanInfo.check_batch_pro_tge}"> checked disabled </c:if> 
                        >
                    </c:forEach>
                </div>
                <div class="container"></div>
                <!-- s: 슬라이드 배너 20210415 -->
                <!-- e: 슬라이드 배너 20210415 -->
            </div>
            <input type="hidden" id="ceo_no" value="${sessionScope.cust.ceo_no}"/>
        </section>
    </main>
</body>