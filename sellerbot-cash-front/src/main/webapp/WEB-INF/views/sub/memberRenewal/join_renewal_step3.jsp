<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf" %>
<head>
    <link rel="stylesheet" href="/assets/new_join/css/step_03.css?ver=20230519_01">
    <script type="module" src="/assets/new_join/js/step_03.js?ver=20231122_01" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <form id="form" name="form" action="/pub/member/step3" method="post">
            <input type="hidden" id ="req_dt" name="req_dt" value="" />
            <input type="hidden" id ="spCode" name="spCode" value="${spCode}" />
            <section>
                <button type="button" class="fixed2" id="prevStep">
                    <span>이전</span>
                </button>
                <button type="button" class="fixed _70" id="nextStep">
                    <span>다음</span>
                    <img src="/assets/new_join/img/rightArrow.svg" />
                </button>
                <button type="button" class="fixed3" id="talkInfo">
                    <img class="normal" src="/assets/new_join/img/talkInfo.png" alt="talkInfo">
                </button>
                <article id="top">
                    <div class="title">
                        <h1>셀러봇캐시 회원가입</h1>
                        <img id="talkInfo" class="mini" src="/assets/new_join/img/talkInfo_mini.png" alt="talkInfo">
                    </div>
                    <div class="processBar">
                        <div class="first step">
                            <span class="line"></span>
                            <p>구독 유형 선택</p>
                        </div>
                        <div class="second step">
                            <span class="line"></span>
                            <p>약관동의/본인인증</p>
                        </div>
                        <div class="third step active">
                            <span class="line"></span>
                            <p>사업자정보 입력</p>
                        </div>
                    </div>
                </article>
                <article id="bizInfo">
                    <h2>사업자정보 입력<small id="userEmail">(${cust_id})</small></h2>
                    <div class="bg">
                        <div class="wrapper">
                            <p class="essential">상호명</p>
                            <div class="row">
                                <input type="text" id="biz_nm" name="biz_nm" placeholder="상호명 입력" targetId="biz_nm">
                                <span id="biz_nmMsg" class="underMsg red">상호명을 입력해주세요.</span>
                            </div>
                            <p class="essential">사업자등록번호</p>
                            <div class="flexRow">
                                <input class="onlyNumber" type="text" id="biz_no" name="biz_no" placeholder="사업자등록번호 입력" maxlength="10" targetId="biz_no">
                                <button type="button" id="bizNumCheck">중복확인</button>
                                <span class="underMsg">('-'없이 숫자만 입력)</span>
                                <span id="biz_noMsg" class="errMsg">사업자번호를 입력해주세요.</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg">
                        <div class="wrapper">
                            <p class="essential">대표자명</p>
                            <input type="hidden" id="ceo_no" name="ceo_no" vaule=""/>
                            <div class="row">
                                <input type="text" id="ceo_nm" name="ceo_nm" placeholder="대표자명 입력" targetId="ceo_nm">
                                <span id="ceo_nmMsg" class="underMsg red">대표자명을 입력해주세요.</span>
                            </div>
                            <p class="essential">대표자 연락처</p>
                            <div class="flexRow">
                                <select id="ceo_no1">
                                    <option value="010" selected>010</option>
                                </select>
                                <input class="onlyNumber" type="text" id="ceo_no2" maxlength="8" targetId="ceo_no2">
                                <span class="underMsg marginLeft">('-'없이 숫자입력 / 입력하신 연락처로 중요한 공지 및 혜택을 안내드리거나 알림톡
                                    리포트를 보내드리고 있어 정확히 입력해주세요.)</span>
                                <span id="msg4" class="errMsg"></span>
                            </div>
                            <p class="question">알림톡 신청 시간 <img id="talkInfo" src="/assets/new_join/img/questionBtn.svg" alt="manual open"></p>
                            <div class="row">
                                <select id="trs_hop_hour1" name="trs_hop_hour1">
                                    <option value="">알림톡 신청시간 선택</option>
                                    <option value="08">08:00</option>
                                    <option value="09">09:00</option>
                                    <option value="10" selected>10:00</option>
                                    <option value="11">11:00</option>
                                    <option value="12">12:00</option>
                                    <option value="13">13:00</option>
                                    <option value="14">14:00</option>
                                    <option value="15">15:00</option>
                                    <option value="16">16:00</option>
                                    <option value="17">17:00</option>
                                    <option value="18">18:00</option>
                                    <option value="19">19:00</option>
                                    <option value="20">20:00</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="bg">
                        <div class="wrapper">
                            <p>담당자명(선택)</p>
                            <input type="hidden" id="chrg_no" name="chrg_no" value="" />
                            <div class="row">
                                <input type="text" id="chrg_nm" name="chrg_nm" placeholder="담당자명 입력" targetId="chrg_nm">
                                <span id="chrg_nmMsg" class="underMsg red">담당자명을 입력해주세요.</span>
                            </div>
                            <p>담당자 연락처(선택)</p>
                            <div class="flexRow">
                                <select id="chrg_no1">
                                    <option value="010" selected>010</option>
                                </select>
                                <input class="onlyNumber" type="text" id="chrg_no2" maxlength="8">
                                <span class="underMsg marginLeft">('-'없이 숫자입력 / 입력하신 연락처로 중요한 공지 및 혜택을 안내드리거나 알림톡
                                    리포트를 보내드리고 있어 정확히 입력해주세요.)</span>
                                <span id="chrg_no2Msg" class="errMsg"></span>
                            </div>
                            <p class="question">알림톡 신청 시간 <img id="talkInfo" src="/assets/new_join/img/questionBtn.svg" alt="manual open"></p>
                            <div class="row">
                                <select id="trs_hop_hour2" name="trs_hop_hour2">
                                    <option value="" selected>알림톡 신청시간 선택</option>
                                    <option value="08">08:00</option>
                                    <option value="09">09:00</option>
                                    <option value="10">10:00</option>
                                    <option value="11">11:00</option>
                                    <option value="12">12:00</option>
                                    <option value="13">13:00</option>
                                    <option value="14">14:00</option>
                                    <option value="15">15:00</option>
                                    <option value="16">16:00</option>
                                    <option value="17">17:00</option>
                                    <option value="18">18:00</option>
                                    <option value="19">19:00</option>
                                    <option value="20">20:00</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="bg">
                        <div class="wrapper">
                            <p class="essential">유입경로</p>
                            <div class="row">
                                <select class="sctbox" id="inf_path_cd" name="inf_path_cd" required="required" targetId="inf_path_cd">
                                    <option value="">경로선택</option>
                                    <c:forEach items="${codeList }" var="code" varStatus="state">
                                        <c:choose>
                                            <c:when test="${fn:toUpperCase(sessionScope.infPath) eq 'SCB'}">
                                                <c:if test="${code.com_cd eq 'SCB'}">
                                                    <option value="${code.com_cd}" selected>${code.cd_nm}</option>
                                                </c:if>
                                            </c:when>
                                            <c:when test="${!empty spCode}">
                                                <c:if test="${code.com_cd eq spCode}">
                                                    <option value="${code.com_cd}" selected>${code.cd_nm}</option>
                                                </c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${code.com_cd}">${code.cd_nm}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                                <span id="inf_path_cdMsg" class="underMsg red">유입경로를 선택해주세요.</span>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </form>
    </main>
</body>