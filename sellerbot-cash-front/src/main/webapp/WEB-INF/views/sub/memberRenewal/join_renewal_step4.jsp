<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf" %>
<head>
    <link rel="stylesheet" href="/assets/new_join/css/step_04.css">
    <script type="module" src="/assets/new_join/js/step_04.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <section>
            <button class="fixed active" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="flex">
                    <span class="line"></span>
                    <div class="checkCircle"></div>
                    <span class="line"></span>
                </div>
                <h2>회원가입이 완료되었습니다</h2>
            </article>
            <article id="content">
                <div class="left">
                    <button id="mallReg" class="active">판매몰 등록</button>
                    <button id="accountReg">계좌 등록</button>
                </div>
                <div class="center">
                    <div class="bg">
                        <video class="mallRegVid active" src="/assets/new_join/vid/mallReg_4sec.mp4" muted autoplay loop></video>
						<video class="accRegVid" src="/assets/new_join/vid/accountReg_4sec.mp4" muted autoplay loop>
                    </div>
                    <div class="timeOutBar"></div>
                    <div class="bottom">
                        <p><b id="leftSec">5</b>초 후 판매몰 등록화면으로 이동합니다.</p>
                        <div class="rightBtns">
                            <button id="stop">대기</button>
                            <button id="nextStep_2" class="blue">다음</button>
                        </div>
                    </div>
                </div>
                <div class="right"></div>
            </article>
        </section>
    </main>
</body>