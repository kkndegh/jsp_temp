<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_join/css/step_02.css">
    <script type="module" src="/assets/new_join/js/step_02.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <form id="form" name="form" action="/pub/member/step2" method="post">
            <input type="hidden" id="cust_id" name="cust_id" value="">
            
            <section>
                <button type="button" class="fixed2" id="prevStep">
                    <span>이전</span>
                </button>
                <button type="button" class="fixed _70" id="nextStep">
                    <span>다음</span>
                    <img src="/assets/new_join/img/rightArrow.svg" />
                </button>
                <article id="top">
                    <div class="title">
                        <h1>셀러봇캐시 회원가입</h1>
                    </div>
                    <div class="processBar">
                        <div class="first step">
                            <span class="line"></span>
                            <p>구독 유형 선택</p>
                        </div>
                        <div class="second step active">
                            <span class="line"></span>
                            <p>약관동의/본인인증</p>
                        </div>
                        <div class="third step">
                            <span class="line"></span>
                            <p>사업자정보 입력</p>
                        </div>
                    </div>
                </article>
                <article id="termsAgree">
                    <h2>약관동의</h2>
                    <div class="formBg">
                        <div class="formDiv">
                            <div class="checkAll">
                                <div class="checkBoxDiv">
                                    <input type="checkbox" id="checkAll" class="chkBox">
                                    <label class="checkBox" for="checkAll"></label>
                                    <label class="text" for="checkAll" style="color:black;"></label>
                                </div>
                                <!-- 20230310 수정 -->
                                <p class="text_checkBox">셀러봇캐시 약관 전체 동의</p>
                            </div>
                            <div class="grayBox">
                                <pre>전체 동의에는 필수 및 선택 정보에 대한 동의가 포함되어 있으며, 개별적으로 동의를 선택 하실 수 있습니다. 선택 항목에 대한 동의를 거부하시는 경우에도 서비스 이용이 가능합니다.</pre>
                            </div>
                            <!--  -->
                            <c:forEach var="titem" items="${sellerbotTerms}" varStatus="status">
                                <c:set var="termsId" value="${titem.terms_id}" />
                                <div class="accordion">
                                    <div class="title">
                                        <div class="left">
                                            <div class="checkBoxDiv">
                                                <input type="checkbox" id="terms_0${status.count}"
                                                name="sellerbotTerm" value="${termsId}" term_esse_yn="${titem.terms_esse_yn}"
                                                term_type_cd="${titem.terms_typ_cd}" class="chkBox"  />
                                                <label class="checkBox" for="terms_0${status.count}"></label>
                                                <label class="text" for="terms_0${status.count}" style="color:black;"></label>
                                            </div>
                                            <p class="label">
                                                <c:if test="${titem.terms_esse_yn == 'Y'}">
                                                    <b>[필수]</b>
                                                </c:if>
                                                <c:if test="${titem.terms_esse_yn != 'Y'}">
                                                    [선택]
                                                </c:if>
                                                ${titem.terms_title}
                                            </p>
                                        </div>
                                        <span><img src="/assets/new_join/img/right-chevron.png" alt="right-chevron"></span>
                                    </div>
                                    <c:choose>
                                        <c:when test="${titem.terms_typ_cd == 'SMT'}">
                                            <div class="panel marketing">
                                                <div class="paddingBox">
                                                    <pre>${titem.terms_cont}</pre>
                                                </div>
                                            </div>
                                            <div class="checkBoxRow">
                                                 <c:forEach items="${smtTermsList}" var="smt">
                                                    <div class="checkBoxDiv square">
                                                        <input type="checkbox" id="${smt.com_cd}" name="smtTerm" value="${smt.com_cd}" class="chkBox">
                                                        <label class="checkBox" for="${smt.com_cd}"></label>
                                                        <label class="text" for="${smt.com_cd}" style="color:black;">${smt.cd_nm}</label>
                                                    </div>
                                                </c:forEach>   
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="panel">
                                                <div class="paddingBox">
                                                    <pre>
                                                        ${titem.terms_cont}
                                                    </pre>
                                                </div>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </article>
                <article id="selfVerification">
                    <h2>본인인증</h2>
                    <div class="formBg">
                        <div class="formDiv">
                            <p class="essential">이메일 </p>
                            <div class="flexRow emailRow">
                                <div class="left">
                                    <input type="text" id="email" name="email" placeholder="이메일 입력" targetId="email">
                                    <input type="hidden" id="sendEmailHidInfo" name="sendEmailHidInfo">
                                    <span>@</span>
                                    <select id="domain" name="domain">
                                        <!-- 20230310 수정 -->
                                        <option value="manual">직접입력</option>
                                        <option value="naver.com">naver.com</option>
                                        <option value="daum.net">daum.net</option>
                                        <option value="hanmail.net">hanmail.net</option>
                                        <option value="gmail.com">gmail.com</option>
                                        <option value="nate.com">nate.com</option>
                                        <option value="me.com">me.com</option>
                                        <option value="icloud.com" >icloud.com</option>
                                        <option value="hotmail.com">hotmail.com</option>
                                        <option value="yahoo.com">yahoo.com</option>
                                    </select>
                                    <input type="text" id="manualDomain" name="manualDomain" targetId="email">
                                </div>
                                <button type="button" id="reqVerificationCode">인증번호 요청</button>
                                <span id="emailMsg" class="underMsg">이메일을 입력해주세요</span>
                            </div>
                            <p class="essential">인증번호</p>
                            <div class="flexRow">
                                <div class="left">
                                    <input class="onlyNumber" type="text" id="verificationCode" placeholder="인증번호 입력" targetId="verificationCode">
                                </div>
                                <button type="button" id="confirmVerificationCode" >확인</button>
                                <span id="verificationCodeMsg" class="underMsg red">인증번호를 다시 확인해주세요.</span>
                            </div>
                            <p class="essential">비밀번호</p>
                            <div class="row">
                                <input type="password" id="passwd" name="passwd" value="${tempInfo.passwd}" placeholder="비밀번호 입력" targetId="passwd">
                                <span class="underMsg">영문, 숫자, 특수문자를 조합하여 8자 이상 ~20자 이내로 입력 ( 사용가능한 특수문자 : !@#$%^&*+=-[] )</span>
                                <span id="passwdMsg" class="errMsg">비밀번호가 형식에 맞지 않습니다.</span>
                            </div>
                            <p class="essential">비밀번호 확인</p>
                            <div class="row">
                                <input type="password" id="passwd_same" name="passwd_same" value="${tempInfo.passwd_same}" placeholder="비밀번호 입력" targetId="passwd_same">
                                <!-- 20230310 수정 -->
                                <span class="underMsg">비밀번호를 한 번 더 입력해주세요.</span>
                                <!-- 20230310 수정 -->
                                <span id="passwd_sameMsg" class="errMsg">비밀번호가 일치하지 않습니다.</span>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </form>
    </main>
</body>