<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf" %>
<head>
    <link rel="stylesheet" href="/assets/new_join/css/step_07.css">
    <script type="module" src="/assets/new_join/js/step_07.js?ver=20230919" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
    <jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />
</head>
<body>
    <main>
        <section>
            <input type="hidden" id="goodsTypCd" name="goodsTypCd" value="${goodsInfo.goodsTyp}">
            <input type="hidden" id="price" name="price" value="${goodsInfo.price}">
            <input type="hidden" id="nextBillYMD" name="nextBillYMD" value="${goodsInfo.nextBillDate}">
            <input type="hidden" id="nextBillDate" name="nextBillDate" value="${goodsInfo.nextBillDate}">
            <input type="hidden" id="goodName" name="goodName" value="${goodsInfo.name}">
            <input type="hidden" id="successYN" name="successYN" value="${successYN}">
            <input type="hidden" id="cardNum" name="cardNum" value="${cardNum}" />
            <input type="hidden" id="cafe24_usr_yn" name="cafe24_usr_yn" value="${cafe24_usr_yn}">
            <input type="hidden" id="acctRegPsbCnt" name="" value="${maxRegCountInfo.SETT_ACC_ACCT_REG_PSB_CNT}">
            <input type="hidden" id="mallRegIdPsbCnt" name="" value="${maxRegCountInfo.SALE_MALL_REG_ID_PSB_CNT}">
            <button class="fixed active" type="button" id="nextStep">
                <span>완료</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>서비스 정보 등록</h1>
                    <c:if test="${successYN == 'Y'}">
                        <span id="nextBillYMdFmt" style="display:none;" >${dateUtilBean.getDateTimeKrFormatYMD(goodsInfo.nextBillDate)}</span>
                    </c:if>
                </div>
                <div class="processBar">
                    <div class="first step">
                        <span class="line"></span>
                        <p>판매몰 등록</p>
                    </div>
                    <div class="second step">
                        <span class="line"></span>
                        <p>결제수단 등록</p>
                    </div>
                    <div class="third step active">
                        <span class="line"></span>
                        <p>계좌 등록</p>
                    </div>
                </div>
            </article>
            <article id="accountReg">
                <!-- 20230323 수정 -->
                <p class="topText">통장 잔액을 한 눈에 확인하고, 판매몰별 입금 현황을 통해 받기로 한 정산금이 제대로 지급되었는지 확인하세요.</p>
                <div class="tableTop">
                    <div class="left">
                        <p>정산계좌 등록현황</p>
                        <span>(<b id="all">전체 0</b> / <b id="nor">정상 0</b> / <b id="err" class="err">오류 0</b> /<b id="ins">점검 0</b>)</span>
                    </div>
                    <div class="right">
                        <button class="blue_outline" id="vidGuide">영상가이드</button>
                        <button class="blue" id="regAcc">+ 계좌등록</button>
                    </div>
                </div>
                <div class="tableDiv">
                    <table id="accountList">
                        <thead>
                            <tr>
                                <td>은행</td>
                                <td>계좌번호</td>
                                <td>계좌잔액</td>
                                <td>최종거래일</td>
                                <td>정산몰</td>
                                <td>내용</td>
                            </tr>
                        </thead>
                        <tbody id="accountListTbody">
                        </tbody>
                    </table>
                </div>
            </article>
            <article id="extraInfo">
                <div class="left">
                    <div class="content">
                        <div class="gallery">
                            <ul>
                                <li><img src="/assets/new_join/img/판매몰별입금내역.jpg" alt="summary"></li>
                                <li><img src="/assets/new_join/img/거래내역조회2.jpg" alt="summary"></li>
                            </ul>
                        </div>
                        <ul class="bottomUl">
                            <li class="active"></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
                <div class="right">
                    <div class="content">
                        <!-- 20230323 수정 -->
                        <h3>국내 20개 은행의 정산계좌 지원</h3>
                        <p>흩어져 있는 내 모든 계좌 현황과 잔액을 한 곳에서 확인!</p>
                        <p>판매몰별 정산금 입금 현황으로 제대로 받긴 받았는지 샅샅이 확인!</p>
                        <p>공인인증서 없이 바로 가능한 빠른 조회</p>
                    </div>
                </div>
            </article>
        </section>
    </main>
    <div class="modal" id="modal_01" style="display:none; z-index: 9000;" ><div class="layerPopup" ><div class="popupHead"><button class="closeModal" id="closeModal4"><span>닫기</span></button></div><div class="popupBody"><div class="head">
        <h5>계좌정보 등록 <img id="regAccManual" src="/assets/new_join/img/questionBtn.svg" alt="manual open"></h5>
        <img src="/assets/images/header/sellerbotLogo.svg" alt="sellerbot cash">
    </div>
    <div class="body form_container" style="background-color: white;">
        <input type="hidden" id="chkAcctDuplYN" name="chkAcctDuplYN" value=""  alt="계좌번호 중복 체크 여브"/>
        <input type="hidden" id="regAcctTestSuccYN" name="regAcctTestSuccYN" value="" alt="계정테스트 여부"/>
        <p>정산계좌 통합관리 서비스를 이용하시려면</p>
        <p class="red">은행별 홈페이지에서 빠른조회서비스 등록을 진행하신 후 계좌정보를 입력하셔야 합니다.</p>
        <form onsubmit="return false;">
            <div class="titleFlex">
                <p>은행명</p>
                <a id="btnPressedOpenBank" name="btnPressedOpenBank" href="#">선택은행 바로가기</a>
            </div>
            <div class="row">
                <select id="reg_bank_cert_m_seq_no" >
                    <c:forEach items="${bankList}" var="bank">
                        <option value="${bank.bank_cert_m_seq_no}" 
                            data-bankcd="${bank.bank_cd}"
                            data-acctnoyn="${bank.acct_no_use_yn}"
                            data-acctpwyn="${bank.acct_passwd_use_yn}"
                            data-bankidyn="${bank.bank_id_use_yn}"
                            data-bankpwyn="${bank.bank_passwd_use_yn}"
                            data-crccd="${bank.crc_use_cd}"
                            data-ctznoyn="${bank.ctz_biz_no_use_yn}">${bank.bank_nm}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="row" id="reg_bank_id_area" >
                <p>빠른조회 은행아이디</p>
                <input id="reg_bank_id" class="inputText" type="text" maxlength="25"  required="required" >
                <p id="reg_bank_id_error" class="error input_field_error" style="display:none;color: red;font-size: 12px;">*은행아이디를 입력해주세요.</p>
            </div>
            <div class="row" id="reg_bank_passwd_area" >
                <p>빠른조회 은행비밀번호</p>
                <input id="reg_bank_passwd" class="inputText" type="password" maxlength="20" required="required">
                <p id="reg_bank_passwd_error" class="error input_field_error" style="display:none;color: red;font-size: 12px;">*은행비밀번호 입력해주세요.</p>
            </div>
            <div id="reg_dpsi_nm_area">
                <p>예금주</p>
                <div class="row">
                    <input type="text" class="inputText" id="reg_dpsi_nm" placeholder="예금주 입력" required="required" maxlength="20">
                    <p id="reg_dpsi_nm_error" class="error input_field_error" style="display:none;color: red;font-size: 12px;">*예금주를 입력해주세요.</p>
                </div>
            </div>
            <div id="reg_acct_no_area" >
                <p>계좌번호</p>
                <div class="flexRow">
                    <input type="text"  id="reg_acct_no" class="onlyNumber inputText" placeholder="계좌번호 입력" required="required" maxlength="20" data-valitype="acctNo">
                    <button type="button" id="confirmAccNum">중복확인</button>
                </div>
                <p id="reg_acct_no_error" class="error input_field_error" style="display:none;color: red;font-size: 12px;">*계좌번호를 바르게 입력해주세요.</p>
            </div>
            <div id="reg_acct_passwd_area">
                <p>계좌 비밀번호</p>
                <div class="row">
                    <input type="password" id="reg_acct_passwd" class="onlyNumber inputText" placeholder="계좌 비밀번호 입력" required="required" maxlength="20"> 
                    <p id="reg_acct_passwd_error" class="error input_field_error" style="color:red;font-size: 12px;">*계좌비밀번호 입력해주세요.</p>
                </div>
            </div>
            <div id="reg_ctz_biz_no_area">
                <p>주민등록(사업자) 번호</p>
                <div class="row">
                    <input type="text" id="reg_ctz_biz_no" class="onlyNumber inputText" placeholder="주민등록번호 앞 6자리 또는 사업자번호 10자리 입력" required="required" maxlength="10" data-valitype="ctzBizno">
                    <p id="reg_ctz_biz_no_error" class="error input_field_error" style="display:none;color:red;font-size: 12px;">*주민등록(사업자) 번호를 바르게 입력해주세요.</p>
                </div>
            </div>
            <div id="acct_biz_yn_area">
                <p>계좌 사업자 여부</p>
                <div class="row">
                    <select class="form" id="acct_biz_yn">
                        <option value="">선택</option>
                        <option value="Y">Y</option>
                        <option value="N">N</option>
                    </select>
                </div>
            </div>
            <c:if test="${cafe24_usr_yn eq 'Y' && !empty addn_goods_req_seq_no}">
                <div class="row grayBox">
                    <p>카페24 '뱅크봇-대사서비스' 이용을 원하시는 회원님은 반드시 <b>입금확인용계좌</b>를 체크해주시기 바랍니다.</p>
                </div>
                <div class="row">
                    <div class="checkBoxDiv">
                        <input type="checkbox" id="reg_reco_yn" class="chkBox">
                        <label class="checkBox" for="reg_reco_yn"></label>
                        <label class="text" for="reg_reco_yn" style="color:black;"> 입금 확인용 계좌</label>
                    </div>
                </div>
            </c:if>
        </form>
        <div class="bottomDiv">
            <button type="button" id="accTest" class="gray">계정테스트</button>
            <button type="button" id="closeModal" class="blue_outline closeModal">닫기</button>
            <button type="button" id="submit" class="blue">등록</button>
        </div>
        </div></div><div class="btnBox"></div></div>
    </div>   
    <!-- 모달 1 -->
    <div class="modal" id="modal_02" style="display:none; z-index: 9000;"  ><div class="layerPopup"><div class="popupHead"><button class="closeModal2" id="closeModal2" ><span>닫기</span></button></div><div class="popupBody">
        <div class="popupWrap">
            <div class="grayBox">
                <p>정산계좌 통합관리 서비스를 이용하시려면 각 은행별 홈페이지에서 빠른조회서비스 등록을 진행하신 후 계좌정보를 입력하셔야 합니다. 하단의 은행명을 선택해주시면 은행별 빠른조회서비스 등록 절차를 확인하실 수 있습니다.</p>
            </div>
            <div class="bank_info">
            
        </div>    <!-- bank_info end   -->
        <button type="button" class="blue_outline closeModal2" >닫기</button> 
    </div>
    </div><div class="btnBox"></div></div></div>
</body>