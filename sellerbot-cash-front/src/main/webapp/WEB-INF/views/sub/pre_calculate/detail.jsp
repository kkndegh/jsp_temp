<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<input type="hidden" id="DeviceType" value="${DeviceType}">

<div class="container">
    <!--페이지 _1-->
    <div class="pre_page_con_wrapper pre_page_1">
      	<div class="pre_page_con">
        	<div class="pre_page_con_left">
      	       	<div class="btn_go_menu">
        			<h1><a href="/sub/pre_calculate/pre_calculate">목록으로</a></h1>
      			</div>
				<c:set var="imgPath" value=""></c:set>
				<c:forEach items="${finGood.loan_goods_file }" var="file">
					<c:if test="${file.stor_file_typ_cd eq 'LMA' }">
						<c:set var="imgPath" value="${file.stor_path}${file.stor_file_nm}"></c:set>
					</c:if>
				</c:forEach>
				<div class="left_background_box 
					<c:if test="${rcm eq 'Y'}">left_background_icon_2</c:if> 
					<c:if test="${finGood.best_goods_yn eq 'Y'}">left_background_icon_1</c:if>" 
					<c:if test="${!empty imgPath}">style="background-image:url(/imagefile/${imgPath})" imgPath="${imgPath}"</c:if> >
				
					<c:if test="${not empty finGood.main_img_url and finGood.main_img_url!=''}">
						<embed width="100%" height="100%" src="https://www.youtube.com/v/${finGood.main_img_url}" frameborder="0" allowfullscreen></embed>
					</c:if>
					<!-- 
					<video class="pre_page_con_left_vid" controls="controls">
					<source src="/assets/video/pre_video.webm" width="400" type="video/webm" />
					</video>
					-->
				</div>
				<div class="pre_page_con_left_bottom" >
					<h1>${finGood.notice }</h1>
				<!-- <h1 style="display:none;" class="left_bottom_res">※ 정확한 활용 가능금액과 예상 수수료는<br />이용 신청 후 확인 가능합니다.</h1> -->
				</div>
			</div>
			<div class="pre_page_con_right">
				<div class="pre_page_con_right_text_0">
					<c:forEach items="${finGood.loan_goods_file}" var="file" varStatus="status">
						<c:if test="${file.stor_file_typ_cd =='LTL'}">
							<img src="/imagefile/${file.stor_path }${file.stor_file_nm}" alt="" />
						</c:if>
					</c:forEach>
					<h1>${finGood.title_1st }</h1>
					<h2>${finGood.goods_descp }</h2>
				</div>
				<div class="pre_page_con_right_text">
					<h1>가능금액</h1>
					<c:choose>
						<c:when test="${finGood.goods_nm eq 'KB국민은행 매출더하기론'}">
							<h2 style="font-size: 1.8rem;display: block; margin-left: 3rem;">법인사업자 최대 한도
								<!-- <b style="font-weight: bold;font-size: 1.5rem;color: #333333;"><fmt:formatNumber value="300000000" pattern="#,###" /></b> -->
								<b style="font-weight: normal;font-size: 1.8rem;color: #333333;">삼억 원</b>
							</h2>
							<h2 style="font-size: 1.8rem;display: block; margin-left: 3rem;">개인사업자 최대 한도
								<!-- <b style="font-weight: bold;font-size: 1.5rem;color: #333333;"><fmt:formatNumber value="100000000" pattern="#,###" /></b> -->
								<b style="font-weight: normal;font-size: 1.8rem;color: #333333;">일억 원</b>
							</h2>
						</c:when>
						<c:when test="${finGood.goods_nm eq 'KB국민은행 메가셀러론'}">
							<h2 style="font-size: 1.3rem;display: inline-block;vertical-align: middle;margin-left: 1rem;">
								- 총 소요자금 범위내에서 고객별 신용등급, 매출액등에 따라 상이
							</h2>
						</c:when>
						<c:when test="${finGood.svc_id eq 'kcbkwbank'}">
							<h2 style="margin-left: 0rem;">최저 200만원 ~ 최고 5,000 만원</h2>
						</c:when>
						<c:when test="${finGood.goods_nm eq '파트너스론'}">
							<h2>최대 
								<b class="pre_bold_text"><fmt:formatNumber value="${b_noti_bef_sett_acc_svc_prc_sum}" pattern="#,###" /></b>
								<b class="pre_normal_text">원</b>
							</h2>
							<h1 style="margin-left: 7rem;">
								단, 개인사업자는 최대 500,000,000원
							</h1>
						</c:when>
						<c:otherwise>
							<h2>최대 <c:if test="${finGood.goods_nm eq '신한 퀵 정산' || finGood.goods_nm eq '신한 퀵 정산 대출'}">한도</c:if>
								<b class="pre_bold_text"><fmt:formatNumber value="${b_noti_bef_sett_acc_svc_prc_sum}" pattern="#,###" /></b>
								<b class="pre_normal_text">원</b>
								<c:if test="${finGood.fin_goods_typ_cd eq 'GAS' }">
									<b class="new_cal_alp">+α</b>
								</c:if>
							</h2>
						</c:otherwise>
					</c:choose>
				</div>
				<ul class="pre_page_con_right_list">
					<c:choose>
						<c:when test="${finGood.goods_nm eq '애큐온셀러론'}">
							<li>
								<h1>금리</h1>
								<!-- <h2>약 <fmt:formatNumber value="${customTotalInt}" type="number"></fmt:formatNumber> 만원<c:if test="${finGood.fin_goods_typ_cd eq 'GAS' }"><b class="new_cal_alp_s">+α</b></c:if></h2> -->
								<h2>연 7.9% (일 0.021%)</h2>
							</li>
							<li>
								<h1>상환방법</h1>
								<h2>${finGood.reim_mth_cd_nm}</h2>
							</li>
						</c:when>
						<c:when test="${finGood.goods_nm eq '온리원페이'}">
							<li>
								<h1>예상 이용 기간</h1>
								<h2>${maxUseDate}일<c:if test="${finGood.fin_goods_typ_cd eq 'GAS' }"><b class="new_cal_alp_s">+α</b></c:if></h2>
							</li>
							<li>
								<fmt:parseNumber value="${totalInterest/10000}" var="customTotalInt" integerOnly="true"></fmt:parseNumber>
								<h1>예상 이용료</h1>
								<h2>약 <fmt:formatNumber value="${customTotalInt}" type="number"></fmt:formatNumber> 만원<c:if test="${finGood.fin_goods_typ_cd eq 'GAS' }"><b class="new_cal_alp_s">+α</b></c:if></h2>
							</li>
							<li>
								<h1>상환방법</h1>
								<h2>${finGood.reim_mth_cd_nm}</h2>
							</li>
						</c:when>
						<c:when test="${finGood.goods_nm eq '신한 퀵 정산' || finGood.goods_nm eq '신한 퀵 정산 대출'}">
							<li>
								<h1>금리</h1>
								<!-- <h2>약 <fmt:formatNumber value="${customTotalInt}" type="number"></fmt:formatNumber> 만원<c:if test="${finGood.fin_goods_typ_cd eq 'GAS' }"><b class="new_cal_alp_s">+α</b></c:if></h2> -->
								<h2>최저 연 6.42% ~ 최고 연 8.83%</h2>
								<h4>(2023.03.06 기준)</h4>
							</li>
							<li>
								<h1>상환방법</h1>
								<h2>만기 일시 상환(정산일자에 자동 상환)</h2>
							</li>
						</c:when>
						<c:when test="${finGood.goods_nm eq '파트너스론'}">
							<li>
								<h1>금리</h1>
								<!-- <h2>약 <fmt:formatNumber value="${customTotalInt}" type="number"></fmt:formatNumber> 만원<c:if test="${finGood.fin_goods_typ_cd eq 'GAS' }"><b class="new_cal_alp_s">+α</b></c:if></h2> -->
								<h2>연 ${finGood.apply_roi}% (고정금리)</h2>
								<h2>변동금리: *기준금리+가산금리(2.0%)</h2>
							</li>
							<br>
							<br>
							<li>
								<h1>상환방법</h1>
								<h2>${finGood.reim_mth_cd_nm}</h2>
							</li>
						</c:when>
						<c:when test="${finGood.goods_nm eq '다모아론'}">
							<li>
								<h1>예상 이용료</h1>
								<h2>일 0.032% (전산수수료 별도)</h2>
							</li>
							<li>
								<h1>상환방법</h1>
								<h2>${finGood.reim_mth_cd_nm}</h2>
							</li>
						</c:when>
						<c:when test="${finGood.goods_nm eq 'KB국민은행 매출더하기론'}">
							<li>
								<h1>금리</h1>
								<h2>최저 연 6.47% ~ 최고 연 8.11%</h2>
							</li>
							<li>
								<h1>상환방법</h1>
								<h2>정산일자에 자동 상환</h2>
							</li>
						</c:when>
						<c:when test="${finGood.goods_nm eq 'KB국민은행 메가셀러론'}">
							<li>
								<h1>예상금리</h1>
								<h2>최저 연 5.96% ~ 최고 연 7.78%</h2>
							</li>
							<li>
								<h1>상환방법</h1>
								<h2>대출만기일에 자동상환</h2>
							</li>
						</c:when>
						<c:when test="${finGood.svc_id eq 'kcbkwbank'}">
							<li>
								<h1>예상금리</h1>
								<h2>최저 연 9.7% ~ 최고 연 19.9% &nbsp;</h2>
							</li>
							<li>
								<h1>상환방법</h1>
								<h2>원리금균등분할</h2>
							</li>	
						</c:when>
						<c:otherwise>
							<li>
								<fmt:parseNumber value="${totalInterest/10000}" var="customTotalInt" integerOnly="true"></fmt:parseNumber>
								<h1>예상 이용료</h1>
								<!-- <h2>약 <fmt:formatNumber value="${customTotalInt}" type="number"></fmt:formatNumber> 만원<c:if test="${finGood.fin_goods_typ_cd eq 'GAS' }"><b class="new_cal_alp_s">+α</b></c:if></h2> -->
								<h2>일 0.043% (전산수수료 별도)</h2>
							</li>
							<li>
								<h1>상환방법</h1>
								<h2>${finGood.reim_mth_cd_nm}</h2>
							</li>
						</c:otherwise>
					</c:choose>
				</ul>
				<div class="pre_page_con_right_btn_wrap">
				<!--- s: 20210303 -->
				<c:choose>
					<c:when test="${finGood.svc_id eq 'shinhanbank'}">
						<button class="btn_blue02 loan_use">
							이용 신청 
						</button>
						<c:if test="${not empty loanBatchProTgtInfo.batchProTgt_list }">
							<button class="btn_blue02 loan_pop">
								판매몰 관리
							</button>
						</c:if>
						<p class="mt10">
							<button class="pre_page_con_right_btn_app btn_appstore">
								<span><b>SOL Biz</b> Download</span>
								App Store
							</button>
							<button class="pre_page_con_right_btn_app btn_google">
								<span><b>SOL Biz</b> Download</span>
								Google play
							</button>
						</p>
					</c:when>
					
					<c:when test="${finGood.svc_id eq 'scbank'}">
						<button class="pre_page_con_right_btn_1">
							상품 안내
						</button>
						<button class="pre_page_con_right_btn_2">
							상담 신청 
						</button>
					</c:when>

					<c:when test="${finGood.svc_id eq 'kcbkbbank'}">
						<p>
							<button class="pre_page_con_right_btn_app btn_appstore" style="height: 75px !important;">
								<span><b>KB스타기업뱅킹</b> Download</span>
								App Store
							</button>
							<button class="pre_page_con_right_btn_app btn_google" style="height: 75px !important;">
								<span><b>KB스타기업뱅킹</b> Download</span>
								Google play
							</button>
						</p>
						<p style="text-align: center;">
							<button class="pre_page_con_right_btn_2" style="float: left; margin-top: 10px; margin-right: 20px;">
								상담 신청 
							</button>
							<c:choose>
								<c:when test="${finGood.goods_nm eq 'KB국민은행 매출더하기론'}">
									<button class="btn_kb_sales_qr">
									</button>
								</c:when>
								<c:when test="${finGood.goods_nm eq 'KB국민은행 메가셀러론'}">
									<button class="btn_kb_mega_qr">
									</button>
								</c:when>
							</c:choose>
							
						</p>
						<div style="width: 30rem; color:#666666; margin-left: auto;">
							<p>
							KB국민은행  기업뱅킹이 없으신 개인사업자 고객님은
							QR코드를 찍으시면 ‘상담신청’으로 연결됩니다
							※ 준비사항: 본인명의 휴대폰, 신분증, 개인용인증서(공동인증서,금융인증서,KB국민인증서 중1)
							</p>
						</div>
					</c:when>
					<c:when test="${finGood.svc_id eq 'kcbkwbank'}">
						<!-- 키움 나중에 추가  -->
						<p style="text-align: center;">
							<button class="pre_page_con_right_btn_1" style="float: left; margin-top: 10px; margin-left: 15px;">
								상품 안내
							</button>
							<button class="btn_kw_qr" style="margin-right: 40px;">		
							</button>
						</p>
					</c:when>
					<c:otherwise>
						<button class="pre_page_con_right_btn_1">
							상담 신청
						</button>
						<!-- 20191025 황건웅 요청으로 disabled 삭제처리 -->
						<button class="pre_page_con_right_btn_2" 
							<%--c:if test="${b_noti_bef_sett_acc_svc_prc_sum == 0}">
								disabled="disabled"
								</c:if --%>
						>
							이용 신청 
						</button>
					</c:otherwise>
				</c:choose>
				<!--- e: 20210303 -->
				</div>
			</div>
    	</div>
	</div>
</div>
<div class="new_popup_wrap">
	<div class="display_none_back"></div>
	<!--팝업 첫번 째-->
	<div class="pop_up_1 pop_up_all">
		<div class="popup_top_title">
			<h1>${terms.terms_title}</h1>
		</div>
		<h1 class="popup_title_2">서비스를 이용하시려면 약관동의가 필요합니다.</h1>
		<div class="pop_up_text_wrap">
			<h1>
				${terms.terms_cont}
			</h1>
		</div>
		<div class="popup_footer">
			<input type="checkbox" class="chkbox" name="chkbox_1" id="joinAll">
			<label for="joinAll"> 제3자 정보제공에 동의합니다.</label>
			<span class="error_popup error_popup_1_1">제3자 정보제공 동의를 하셔야 이용이 가능합니다.</span>
		</div>
		<button type="button" class="btn_confirm_terms popup_1_btn popup_1_btn_none">서비스 신청</button>
	</div>
	<!--팝업 두번 째-->
	<div class="pop_up_2 pop_up_all">
		<div class="new_popup_close">
			<div></div>
			<div></div>
		</div>
		<h1 class="popup_title_2">상담받으실 분을 선택해주세요.</h1>
		<ul class="sel_pop_up_2">
			<li class="sel_pop_up_2_ceo">대표자 (${cust.ceo_nm }, ${cust.ceo_no})</li>
			<input type="hidden" id="biz_info_nm" value="${cust.biz_nm }">
			<input type="hidden" id="ceo_info_no" value="${cust.ceo_no}">
		<c:if test="${not empty cust.chrg_nm and cust.chrg_nm!=''}">
			<li class="sel_pop_up_2_chrg">담당자 (${cust.chrg_nm}, ${cust.chrg_no})</li>
			<input type="hidden" id="chrg_info_no" value="${cust.chrg_no}">
		</c:if>
		</ul>
		<button type="button" class="btn_confirm_terms popup_2_btn">확인</button>
	</div>
	<!--팝업 네번 째-->
	<div class="pop_up_alert pop_up_all pop_up_alert_1">
		<div class="new_popup_close">
			<div></div>
			<div></div>
		</div>
		<h1>
			상담신청이 완료되었습니다.<br />
			<b class="popup_text_color">입력하신 번호</b>로 연락이 갈 거에요.
		</h1>
		<button type="button" class="btn_confirm_terms popup_4_btn">확인</button>
	</div>
	<!--팝업 다섯번 째-->
	<div class="pop_up_alert pop_up_all pop_up_alert_2">
		<div class="new_popup_close">
			<div></div>
			<div></div>
		</div>
		<h1>
			상담신청이 완료되었습니다.<br />
			<b class="popup_text_color">선택하신 번호</b>로 연락이 갈 거에요.
		</h1>
		<button type="button" class="btn_confirm_terms popup_5_btn">확인</button>
	</div>
	<!--팝업 여섯번 째-->
	<div class="pop_up_alert pop_up_all pop_up_alert_3">
		<div class="new_popup_close">
			<div></div>
			<div></div>
		</div>
		<h1>
			고객님께서는 기존에 제3자 정보제공<br />
			동의를 하셧으므로 금융서비스 이용이 가능합니다.
		</h1>
		<h2 class="pop_up_alert_3_detail">제 3자 정보제공 동의<b class="popup_detail_btn">[자세히보기]</b></h2>
		<button type="button" class="btn_confirm_terms popup_6_btn">이용신청</button>
	</div>
</div>

<!-- S:신한퀵정산 판매몰 관리 팝업 20210303--> 
<div class="popup_account popup_account2 loan">
	<div class="pop_account">
		<div class="pop_head">
			<p>신한 퀵정산 대출 판매몰 관리</p>
			<img class="popup_loan_close" src="/assets/images/member/close.png" alt="" />
		</div>
		<div class="pop_body">
			<div class="pd20 ">
				신한은행의 퀵정산 대출을 이용하는 고객은 대출 대상의 판매몰을 선택 할 수 있습니다.
			</div>
			<div class="table_style01 scroll_y">
				<style>
					.mall_sts_cd_store_con {
						position: relative;
						display: block;
						left: 2px;
						top: 8px;
						width: 8px;
						height: 8px;
						border-radius: 50%;
					}

					.mall_sts_cd_store_con.bullet_blu {
						background-color: #598ae2;
					}

					.mall_sts_cd_store_con.bullet_red {
						background-color: #fd0e19;
					}

					.mall_sts_cd_store_con.bullet_gry {
						background-color: #aaa;
					}

					.pre_page_con_right_btn_3 {
						background-color: #fff;
						width: 9rem;
						height: 3rem;
						border-radius: 10px;
						font-size: 1rem;
						display: inline-block;
						font-weight: normal;
						border: 1px solid #C6C6C6;
					}
				</style>
				<table class="w95">
					<c:set var="RONIBOT" value="" />
					<thead>
						<tr id="frist_line">
							<th>대상 여부</th>
							<th>판매몰 명</th>
							<th>로그인<br>아이디</th>
							<th>데이터 수집<br> 현황</th>
							<th>등록 일자</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${not empty loanBatchProTgtInfo.batchProTgt_list }">
							<c:set var="batPrcsTgtSeqNo" value="${loanBatchProTgtInfo.batchProTgt_list[0].bat_prcs_tgt_seq_no }" />
						</c:if>
						<c:forEach items="${loanBatchProTgtInfo.loanCustMall_list}" var="loanInfo" varStatus="status">
							<tr>
								<td class="text-center"><input type="checkbox" name="loan_chk"
									data-cust-seq-no="${loanInfo.cust_seq_no}"
									data-cust-mall-seq-no="${loanInfo.cust_mall_seq_no}" 
									data-mall-cd="${loanInfo.mall_cd}"
									data-bat-prcs-tgt-seq-no="${batPrcsTgtSeqNo}"
									<c:if test="${not empty loanInfo.check_batch_pro_tge}"> checked disabled </c:if> 
								></td>
								<th>${loanInfo.mall_nm}</th>
								<td class="text-center">${loanInfo.mall_cert_1st_id}</td>
								<td class="text-center">
									<div>
										<ul>
									<c:choose>
										<c:when test="${loanInfo.cust_mall_sts_cd eq 'NR'}">
											<li class="mall_sts_cd_store_con bullet_blu"></li>
											<li>정상</li>
										</c:when>
										<c:when test="${loanInfo.cust_mall_sts_cd eq 'INS'}">
											<li class="mall_sts_cd_store_con bullet_gry"></li>
											<li>점검</li>
										</c:when>
										<c:when test="${loanInfo.cust_mall_sts_cd eq 'ERR'}">
											<li class="mall_sts_cd_store_con bullet_red"></li>
											<li>인증불가</li>
										</c:when>
										<c:otherwise>
										</c:otherwise>
									</c:choose>
										</ul>
									</div>
								</td>
								<fmt:parseDate value="${loanInfo.loan_reg_ts}" pattern="yyyy-MM-dd'T'HH:mm" var="loan_reg_ts" type="both"></fmt:parseDate>
								<td class="text-center"><fmt:formatDate value="${loan_reg_ts}" pattern="yyyy-MM-dd" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="pop_foot fl_wrap">
			<button type="button" class="btn_left fl_left loan_save">저장</button>
			<button type="button" class="btn_gray2 btn_cancle fl_left loan_close">취소</button>
		</div>
	</div>
</div>
<!-- E:신한퀵정산 판매몰 관리 팝업 20210303 --> 

<script>
  	$(document).ready(function(){
  		var IT3TermsId="${terms.terms_id}";
  		var selectTarget="";
  		var isConfirm=false;
  		var _clickTarget = ""; 
		

  		// 신청 api
  		function fnApply(){
  			$(".pop_up_1").css("display", "none");
  	      	$(".pop_up_2").css("display", "none");
  	  		$(".display_none_back").hide();
  	  		$(".new_popup_wrap").css("display", "none");
  			// 신청
			$.ajax({
				url:"/sub/pre_calculate/detail/setFinGoodStat?fin_goods_seq_no=" + '${finGood.fin_goods_seq_no}' + "&fin_goods_req_typ_cd=LRE",
				method:"POST",
				success:function(data){
					if(data==true){
						var url = '${sodeone_home}' + "/join/apply.php?cust_id=${cust.cust_id}&finGoodSeqNo=${finGood.fin_goods_seq_no}";
						if ('${finGood.svc_id}' == "Only1Pay" ) {
							url = "https://only1pay.co.kr/";
							//에큐온캐피탈 별도처리
						} else if ('${finGood.svc_id}' == "AcuonCapital") {
							url = "https://www.acuoncapital.com/info/sellerLoanInfo.do";
						} else if ('${finGood.svc_id}' == "scbank") {
							if($("#DeviceType").val() == 'PC') {
								url = "https://open.standardchartered.co.kr/loan/SM7181D.jsp?LOAN_GUBUN=Y&id=5348";
							} else {
								url = "https://m.sc.co.kr/product/product/MA3PRDLON001.mnu?changeViewService=/product/product/MA3PRDLON002_24V.view&pCd=PRDLON0005348&bizType=WCDL";
							}
						} else if ('${finGood.svc_id}' == "kcbkbbank") {
							if ('${finGood.goods_nm}' == 'KB국민은행 매출더하기론') {
								if($("#DeviceType").val() == 'PC') {
									url = "https://obank.kbstar.com/quics?page=C041244&scheme=kbbiz&pageid=C106356";
								} else {
									url = "https://obank.kbstar.com/quics?page=C041244&scheme=kbbiz&pageid=C106356";
								}
							} else if ('${finGood.goods_nm}' == 'KB국민은행 메가셀러론') {
								if($("#DeviceType").val() == 'PC') {
									url = "https://obiz.kbstar.com/quics?page=C107218";
								} else {
									url = "https://obank.kbstar.com/quics?page=C041244&scheme=kbbiz&pageid=C106974";
								}
							}
							
						} else if ("${finGood.goods_nm eq '다모아론'}") {
							url = '${sodeone_home}' + "/sub/sub302020.php?loginid=${sessionScope.cust.cust_id}";
						}
						
						var win = window.open(url, '_blank');
						
			  			win.focus();
					}else{
						//오류 처리
						showAlert("신청중 오류가 발생했습니다.");
					}
				}
			});
			
			isConfirm=true;
  		}
  		
  		$(".pre_page_con_right_btn_1").click(function() {
  			_clickTarget = "consulting";
  			if(IT3TermsId!=""){
  	  			$.ajax({
  	  				url:"/sub/pre_calculate/detail/term?termsId="+IT3TermsId,
  	  				success:function(data){
  	  					if(data=='false'){
	  	  					$(".new_popup_wrap").css("display", "flex");
		  	  		  	    $(".pop_up_1").show();
		  	  		  	    $(".display_none_back").show();
  	  					}else if(data=='true'){
							isConfirm=true;
							//에큐온캐피탈 별도처리
							if('${finGood.svc_id}' == "AcuonCapital"){
								url = "https://www.acuoncapital.com/info/sellerLoanInfo.do";
								var win = window.open(url, '_blank');
								win.focus();
							}
							else if('${finGood.svc_id}' == "scbank"){
								url = "https://mkt.sc.co.kr/6ddpko";
								var win = window.open(url, '_blank');
								win.focus();
							}else if('${finGood.svc_id}' == "kcbkwbank"){
								url = "https://www.kiwoombank.com/jsp/loans/loans32p.jsp";
								var win = window.open(url, '_blank');
								win.focus();
							}else{
								$(".new_popup_wrap").css("display", "flex");
								$(".pop_up_2").css("display", "block");
								$(".display_none_back").show();
							}
  	  						
  	  					}else{
  	  						//오류 처리
  	  						alert("오류");
  	  					}
  	  				}
  	  			});
  	  		}
	  	});
  		$(".pre_page_con_right_btn_2").click(function(){
  			_clickTarget = "apply";
  			if(IT3TermsId!=""){
  				$.ajax({
  	  				url:"/sub/pre_calculate/detail/term?termsId="+IT3TermsId,
  	  				success:function(data){
  	  					if(data=='false'){
  	  						// 이용 약관
	  	  					$(".new_popup_wrap").css("display", "flex");
		  	  		  	    $(".pop_up_1").show();
		  	  		  	    $(".display_none_back").show();
  	  					}else if(data=='true'){
  	  						fnApply();
  	  					}else{
  	  						//오류 처리
  	  						alert("오류");
  	  					}
  	  				}
  	  			});
  			} else {
  				// 신청
  				fnApply();
  			}
  		});
  		
		//pre_calculate_popup//
  	 	$(".sel_pop_up_2 li").click(function() {
  			if($(this).hasClass('sel_pop_up_2_ceo')){
  				selectTarget="ceo";
  			}else if($(this).hasClass('sel_pop_up_2_chrg')){
  				selectTarget="chrg";
  			}
  	    	$(this).addClass("sel_pop_up_2_on");
  	    	$(".sel_pop_up_2 li").not(this).removeClass("sel_pop_up_2_on");
  	  	});

  	  	$(".sel_pop_up_2_dir_input").click(function() {
  	    	$(".sel_pop_up_2 li").removeClass("sel_pop_up_2_on");
  	  	});

  	  	$(".new_popup_close").click(function() {
  			selectTarget="";
  			isConfirm=false;
  	    	$(".new_popup_wrap").css("display", "none");
  	    	$(".display_none_back").hide();
  	    	$(".pop_up_all").hide();
  	  	});
		
  	  	//팝업 첫 번째 검증//
  	  	$(".popup_1_btn_none").click(function() {
  	    	var input_pre_chk = document.getElementsByName("chkbox_1");
  	    	if (!input_pre_chk[0].checked) {
  	     		$(".error_popup_1_1").css("display", "block");
  	    	}
  	    	if (input_pre_chk[0].checked) {
  	    		$.ajax({
  					url:"/sub/pre_calculate/detail/term/aggr?termsId="+IT3TermsId,
  					success:function(data){
  						if(data==true){
  							if(_clickTarget == "apply"){
  	  			  				// 신청 처리
  	  	  						fnApply();
  							} else {
								$(".pop_up_1").css("display", "none");
								//에큐온캐피탈 별도처리
								if('${finGood.svc_id}' == "AcuonCapital"){
									url = "https://www.acuoncapital.com/info/sellerLoanInfo.do";
									var win = window.open(url, '_blank');
									win.focus();
								}else if('${finGood.svc_id}' == 'shinhanbank'){
									$(".new_popup_wrap").css("display", "none");
									var win = window.open("https://sbizbank.shinhan.com/landing.jsp?mid=RP0630", '_blank');
									win.focus();
								}else if('${finGood.svc_id}' == 'kcbkwbank'){
									$(".display_none_back").hide();
  	  								$(".new_popup_wrap").css("display", "none");
									url = "https://www.kiwoombank.com/jsp/loans/loans32p.jsp";
									var win = window.open(url, '_blank');
									win.focus();
								}else{	
  	  			  	    	  		$(".pop_up_2").css("display", "block");	
								}	
  							}
  			  	    		isConfirm=true;
  						}else{
  							//오류 처리
  							alert("오류");
  						}
  					}
  				});
  	    	}
  	  	});
		
  	  	$(".popup_1_btn_join").click(function() {
  	    	var input_pre_chk = document.getElementsByName("chkbox_1");
  	    	if (!input_pre_chk[0].checked) {
  	    		$(".error_popup_1_1").css("display", "block");
  	    	}
  	    	if (input_pre_chk[0].checked) {
  	      		$(".new_popup_wrap").css("display", "none");
  	      		$(".pop_up_1").hide();
  	      		$(".display_none_back").hide();
  	    	}
  	  	});
  	  	//팝업 두 번째 담당자 검증//
  	  	$(".popup_2_btn").click(function() {
  		  	if(isConfirm){
  				if(selectTarget!=""){
  	  				var json = {
  	    	  			"biz_nm": $("#biz_info_nm").val(),
  	    	  			"ceph_no": $("#"+selectTarget+"_info_no").val(),
  	    	  			"fin_goods_seq_no" : '${finGood.fin_goods_seq_no}'
  	    	  		};
					$.ajax({
						url:"/sub/pre_calculate/detail/consel?"+ $.param(json) ,
						method:"POST",
						success:function(data){
							if(data==true){
								$.ajax({
									url:"/sub/pre_calculate/detail/setFinGoodStat?fin_goods_seq_no="+ '${finGood.fin_goods_seq_no}' +"&fin_goods_req_typ_cd=CRE",
									method:"POST",
									success:function(data){
										if(data==true){
											$(".pop_up_2").hide();
											$(".pop_up_alert_2").show();
										}else{
											//오류 처리
											showAlert("신청중 오류가 발생했습니다.");
										}
									}
								});	
							}else{
								//오류 처리
								showAlert("신청중 오류가 발생했습니다.");
							}
						}
					});	
  				}else{
  				
  				}
  			}else{
  				//비정상적인 접근 경고문구
  				alert("비정상적인 접근입니다.");
  			}
		});
  	 
		//팝업 6번 넘기기//
		$(".popup_5_btn").click(function() {
			selectTarget="";
			isConfirm=false;
			$(".new_popup_wrap").css("display", "none");
			$(".display_none_back").hide();
			$(".pop_up_all").hide();
  		});

		// 신한퀵정산 관련 제3자 정보제공 동의 체크
		$(".loan_use").on("click", function() {
			if(IT3TermsId!=""){
  	  			$.ajax({
  	  				url:"/sub/pre_calculate/detail/term?termsId="+IT3TermsId,
  	  				success:function(data){
  	  					if(data=='false'){
	  	  					$(".new_popup_wrap").css("display", "flex");
		  	  		  	    $(".pop_up_1").show();
		  	  		  	    $(".display_none_back").show();
  	  					}else if(data=='true'){
							isConfirm=true;
							var win = window.open("https://sbizbank.shinhan.com/landing.jsp?mid=RP0630", '_blank');
							win.focus();
  	  					}else{
  	  						//오류 처리
  	  						alert("오류");
  	  					}
  	  				}
  	  			});
  	  		}
		});
		
		//App Store
		$(".btn_appstore").on("click", function() {
			if ('${finGood.svc_id}' == "shinhanbank") {
				window.open('https://apps.apple.com/kr/app/id587766126');
			} else if ('${finGood.svc_id}' == "kcbkbbank") {
				window.open('https://apps.apple.com/kr/app/id660616512');
			}
		});

		//Google play
		$(".btn_google").on("click", function() {
			if ('${finGood.svc_id}' == "shinhanbank") {
				window.open('https://play.google.com/store/apps/details?id=com.shinhan.sbizbank');
			} else if ('${finGood.svc_id}' == "kcbkbbank") {
				window.open('https://play.google.com/store/apps/details?id=com.kbstar.kbbiz');
			}
		});

		// 2021.03.09 추가 신한퀵정산 판매몰 팝업창
		$(".loan_pop").on("click", function () {
			$(".popup_account.loan").addClass("active");
		});
		$(".popup_loan_close, .loan_close").on("click", function () {
			$(".popup_account.loan").removeClass("active");
		});
		$(".loan_save").on("click", function () {
			var paramArray = new Array();
			$("input:checkbox[name=loan_chk]:not(:disabled):checked").each(function() {
				var pushData  = {
					cust_seq_no : $(this).data("cust-seq-no"),
					cust_mall_seq_no : $(this).data("cust-mall-seq-no"),
					mall_cd : $(this).data("mall-cd"),
					bat_prcs_tgt_seq_no : $(this).data("bat-prcs-tgt-seq-no")
				};

				paramArray.push(pushData);
			});

			$.ajax({
				url: '/sub/loan/getLoanInfo'
				, type: 'post'
				, async: true
				, dataType: 'json'
				, contentType: 'application/json'
				, data: JSON.stringify(paramArray)
				, success: function (response) {
					$(".popup_account.loan").removeClass("active");
					if (response.result == "OK") {
						showAlert("저장 되었습니다.", function () {
							location.href = "/sub/pre_calculate/detail?finGoodSeqNo=${finGoodSeqNo}";
						});
					} else {
						showAlert(errorMessage);
					}
				}
				, error: function (res) {
					showAlert("처리 중 에러가 발생하였습니다.<br>관리자에게 문의 하세요.");
				}
			});
		});

		// 2022.03.10 KB국민은행 매출더하기론 css용 처리 추가
		if ('${finGood.svc_id}' == "kcbkbbank") {
			$(".pre_page_con_left_bottom h1").css('white-space' , 'pre-line');
			if($("#DeviceType").val() == 'PC') {
				$("header").css('position' , 'sticky');
				$("footer").css('margin-top', '45px');
			}
		}

		if ('${finGood.svc_id}' == "scbank") {
			if($("#DeviceType").val() == 'PC') {
				$("footer").css('margin-top', '45px');
				$("header").css('position' , 'sticky');
			}
		}


		// 2022.11.16 키움저축은행 e-셀러 css용 처리 추가
		if ('${finGood.svc_id}' == "kcbkwbank") {
			$(".pre_page_con_left_bottom h1").css('white-space' , 'pre-line');
			$(".pre_page_con_wrapper .pre_page_con").css('margin-bottom' , '50px');
			if($("#DeviceType").val() == 'PC') {
				$("header").css('position' , 'sticky');
			}
		}

		// 2022.11.16 신한은행 css용 처리 추가
		if ('${finGood.svc_id}' == "shinhanbank") {
			$(".pre_page_con_left_bottom h1").css('white-space' , 'pre-line');
			$(".pre_page_con_wrapper .pre_page_con").css('margin-bottom' , '50px');
			if($("#DeviceType").val() == 'PC') {
				$("header").css('position' , 'sticky');
			}
			if($("#DeviceType").val() == 'PC') {
				$("footer").css('margin-top', '190px');
				$("header").css('position' , 'sticky');
				$("header").css('margin-bottom' , '210px');
			}
		}
	});
</script>