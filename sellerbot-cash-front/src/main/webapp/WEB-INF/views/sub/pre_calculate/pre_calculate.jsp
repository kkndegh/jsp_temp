<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!--  화면 전용 css -->
<link rel="stylesheet" href="/assets/css/calculate.css">

<!--start-->
<div class="container">
	<!--상단 배너-->
	<div class="cal_main_banner_1_wrap">
		<div class="cal_main_banner_1">
			<div class="cal_main_banner_1_text">
				<div class="cal_text_wrap_box">
					<h1>금융서비스 가성비 꿀 금융템</h1>
				</div>
				<h2>
					든든하고 똑똑하게<br /> 사용하시라고 준비했습니다.
				</h2>
			</div>
		</div>
	</div>
	<!--컨텐츠-->
	<div class="cal_content">
		<div class="cal_content_wrap">
			<h1 class="cal_content_wrap_title">금융상품 LIST</h1>
			<ul class="cal_content_wrap_list">
				<!--box-->
				<c:forEach items="${finGoodList }" var="finGood" varStatus="state">
					<li
						onclick="location.href='/sub/pre_calculate/detail?finGoodSeqNo=${finGood.list.fin_goods_seq_no}'">
						<!--top-->
						<c:set var="bannerPath" value=""></c:set>
						<c:set var="hoverBannerPath" value=""></c:set>
						<c:forEach items="${finGood.list.loan_goods_file }" var="file" varStatus="fileState">
							<c:if test="${file.stor_file_typ_cd=='LBE' }">
								<c:set var="bannerPath" value="${file.stor_path}${file.stor_file_nm}"></c:set>
							</c:if>
							<c:if test="${file.stor_file_typ_cd=='LBO' }">
								<c:set var="hoverBannerPath" value="${file.stor_path}${file.stor_file_nm}"></c:set>
							</c:if>
						</c:forEach>
						<div class="cal_content_wrap_list_top" bannerPath="${bannerPath }"
							hoverBannerPath="${hoverBannerPath}" style="background-image:url(/imagefile/${bannerPath})">
							<h1>
								${finGood.list.title_1st }
							</h1>
							<h2 class="cal_hover_text">
								${finGood.list.goods_add_descp}
							</h2>
							<c:if test="${finGood.list.best_goods_yn=='Y'}">
								<p class="cal_box_icon"></p>
							</c:if>
							<c:if test="${finGood.rcm eq 'Y'}">
								<!-- 추천 -->
								<p class="cal_box_icon_2">
									<img src="" alt="" />
								</p>
							</c:if>
							<div class="cal_gage">
								<div class="cal_gage_bar" style="width:${finGood.list.inq_cnt/totalInq*100}%"
									per="${finGood.list.inq_cnt/totalInq*100}">
									<div class="cal_gage_alert" style="display: none">
										<span>
											<fmt:parseNumber value="${finGood.list.inq_cnt/totalInq*100}"
												integerOnly="true"></fmt:parseNumber>%
										</span>
									</div>
								</div>
							</div>
						</div>
						<!--bottom-->
						<div class="cal_content_wrap_list_bottom">
							<div class="cal_content_wrap_list_bottom_text">
								<h1>가능금액</h1>
								<h2>
									<c:choose>
										<c:when test="${finGood.list.goods_nm eq 'KB국민은행 메가셀러론'}">
											<b style="font-size: 21px;">- 총 소요자금 범위내에서 고객별 신용등급, 매출액등에 따라 상이</b>
										</c:when>
										<c:when test="${finGood.list.goods_nm eq 'KB국민은행 매출더하기론'}">
											최대 <b>삼억 </b>원
										</c:when>
										<c:otherwise>
											최대 
											<c:if test="${finGood.list.goods_nm eq '신한 퀵 정산' || finGood.list.goods_nm eq '신한 퀵 정산 대출' }">한도</c:if>
												<b><fmt:formatNumber value="${finGood.pre}" pattern="#,###" /></b>원
											<c:if test="${finGood.list.fin_goods_typ_cd eq 'GAS' }">
												<span style="color: #5980D0;">+α</span>
											</c:if>
										</c:otherwise>
									</c:choose>
								</h2>
							</div>
						</div>
					</li>
				</c:forEach>
			</ul>
		</div>
		<div class="cal_content_2">
			<h1 class="cal_content_2_title">
				쇼핑몰 판매자들을 위한<br /> <b>셀러봇캐시의 특별한 금융서비스</b>
			</h1>
			<div class="cal_content_2_wrap">

				<div class="cal_content_2_banner">
					<div class="cal_content_2_wrap_text">
						<h1>
							모바일에서도<br /> <b> 빠르게 서비스 신청하고<br /> 편하게 관리받자!
							</b>
						</h1>
						<span>언제 어디서나, 쉽고 빠른 금융 서비스 신청</span>
					</div>
				</div>
				<div class="cal_content_2_banner_m">
					<div class="cal_content_2_banner_m_left_img_area">
						<img src="/assets/images/new_cal/cal_new_phone.png" alt="" />
					</div>
					<div class="cal_content_2_banner_m_text_area">
						<h1>
							모바일에서도<br />
							<b>
								빠르게 서비스 신청하고<br />
								편하게 관리받자!
							</b>
						</h1>
					</div>
				</div>
				<div class="cal_content_2_banner_m_2">
					<div class="cal_content_2_banner_m_text_area">
						<h1>
							모바일에서도<br />
							<b>
								빠르게 서비스 신청하고<br />
								편하게 관리받자!
							</b>
						</h1>
						<span>
							언제 어디서나, 쉽고 빠른 금융 서비스 신청
						</span>
					</div>
					<div class="cal_content_2_banner_m_left_img_area">
						<img src="/assets/images/new_cal/cal_new_phone.png" alt="" />
					</div>
				</div>
				<ul class="cal_content_2_wrap_bottom">
					<li><span class="cal_content_2_wrap_bottom_color"> 특별한<br />
							정산 예정금
						</span>
						<h3>
							내 돈인듯, 내 돈아닌<br /> 내 돈같은 내 돈!
						</h3>
					</li>
					<li><span class="cal_content_2_wrap_bottom_color_2">
							똑똑한<br /> 금융서비스
						</span>
						<h3>
							필요할 때 사용하고<br /> 정산일에 맞춰<br /> 돌려주니까!
						</h3>
					</li>
					<li><span class="cal_content_2_wrap_bottom_color_3">
							경쟁우위<br /> 확보
						</span>
						<h3>
							빠른 자금회전,<br /> 경쟁력 확보 효율적인<br /> 자금관리까지!
						</h3>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>


<script>
	$(window).scroll(function () {
		var now_offset = $(this).scrollTop();
		var offset_first = $(".cal_content_wrap_title").offset().top;
		if (now_offset >= offset_first) {
			$(".header_wra").addClass("header_wra_on");
		} else {
			$(".header_wra").removeClass("header_wra_on");
		}
	});
	$(document).ready(function () {
		$(".cal_content_wrap_list_top").hover(function () {
			if ($(this).attr('hoverBannerPath') != "") {
				$(this).css("background-image", "url(/imagefile/" + $(this).attr('hoverBannerPath') + ")");
			} else {
				$(this).css("background-image", "none");
			}
			$(this).find("h1").hide();
			$(this).find("h2").show();
			var alert = $(this).find(".cal_gage_alert");
			if (alert.attr("showYn") == "Y") {
				alert.show();
			}
		}, function () {
			if ($(this).attr('BannerPath') != "") {
				$(this).css("background-image", "url(/imagefile/" + $(this).attr('BannerPath') + ")");
			} else {
				$(this).css("background-image", "none");
			}
			$(this).find("h2").hide();
			$(this).find("h1").show();
			$(this).find(".cal_gage_alert").hide();
		});
		$(".cal_gage_bar").each(function () {
			if ($(this).attr("per") < 10) {
				$(this).find(".cal_gage_alert").attr("showYn", "N");
			} else {
				$(this).find(".cal_gage_alert").attr("showYn", "Y");
			}
		});
	});

</script>