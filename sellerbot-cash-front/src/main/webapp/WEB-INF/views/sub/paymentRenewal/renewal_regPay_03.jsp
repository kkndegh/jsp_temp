<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_payment/css/regPay_03.css">
    <script type="module" src="/assets/new_payment/js/regPay_03.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
    <jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />
</head>
<body>
    <main>
        <section>
            <article id="top">
                <div class="title">
                    <h1>결제수단 등록 완료</h1>
                </div>
            </article>
            <c:forEach var="product" items="${products}" varStatus="status">
                <article id="content">
                    <p>셀러봇캐시를 이용해주셔서 감사합니다.</p>
                    <p>셀러봇캐시 요금제(${product.name}) 이용이 시작되었습니다.</p>
                    <c:if test="${!empty product.nextBillDate}">
                        <p><b>다음 정기결제일은 ${dateUtilBean.getDateTimeKrFormatYMD(product.nextBillDate)} 입니다.</b></p>
                    </c:if>
                    <h3>최종 결제금액</h3>
                    <div class="flexBox">
                        <div class="left">
                            <h5>총 결제 금액</h5>
                            <h4>
                                <fmt:formatNumber value="${product.price}"
                                pattern="#,###" />    
                               원</h4>
                            <small>(VAT 포함)</small>
                        </div>
                        <div class="right">
                            <div class="content">
                                <p class="gray">셀러봇캐시 이용권</p>
                                <div class="box">
                                    <div class="blue">${product.name}</div>
                                    <p>월 
                                        <fmt:formatNumber value="${product.price}"
                                pattern="#,###" />    
                                       원</p>
                                </div>
                                <span class="line"></span>
                                <p class="gray">자동대사서비스</p>
                                <div class="box">
                                    <div class="blue_outline">뱅크봇</div>
                                    <p>무료</p>
                                </div>
                                <span class="line"></span>
                                <p class="gray">가입비</p>
                                <p>가입비 무료</p>
                            </div>
                        </div>
                    </div>
                    <div id="caution">
                        <h3>유의사항</h3>
                        <div class="grayBox">
                            <pre>선택해주신 이용권과 결제수단으로 매월 정기결제가 진행됩니다.

    해지는 언제든지 가능하며, 해지 경로는 아래와 같습니다.
    - [셀러봇캐시 홈페이지 → 마이페이지 → 결제정보 → (이용권 우측) 해지]</pre>
                        </div>
                    </div>
                    <h3>결제정보</h3>
                    <div class="flexBox payInfo">
                        <div class="left">
                            <h5>신용카드</h5>
                        </div>
                        <div class="right">
                            <p>${cardNum}</p>
                        </div>
                    </div>
                    <div class="btnBox">
                        <button id="payInfoBtn">결제정보</button>
                        <button id="dashboardBtn" class="blue">대시보드 이동</button>
                    </div>
                </article>
            </c:forEach>
        </section>
    </main>
</body>