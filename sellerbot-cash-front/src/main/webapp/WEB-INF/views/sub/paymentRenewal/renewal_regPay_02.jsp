<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_payment/css/regPay_02.css">
    <script type="module" src="/assets/new_payment/js/regPay_02.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <input type="hidden" id="DeviceType" value="${DeviceType}">
        <!-- 상품정보 가지고 올거임  -->
        <input type="hidden" name="goodsList"  id="goodsList"    value="">
        <!-- 두개 값 모르겟음 -->
        <input type="hidden" name="freeTrialYn" id="freeTrialYn"   value="Y"/>
        <input type="hidden" name="eventNo" id="eventNo" value="0">

        <input type="hidden" id="selGoodsSeqNo" value="${goodsHis.goodsSeqNo}">
        <input type="hidden" id="selGoodsOptSeqNo" value="${goodsHis.goodsOptSeqNo}">
        
        <input type="hidden" id="errMsg" value="${errMsg}" />

        <!-- 결제 결과 값 -->
        <input type="hidden" name="successYN" id="successYN" value="${successYN}">

        <section>
            <button class="fixed" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>결제수단 등록</h1>
                </div>
            </article>
            <article id="payInfo">
                <div class="flexBox">
                    <div class="left">
                        <div class="top">
                            <h5>셀러봇캐시는 지금 가입비 무료 이벤트 중! 🎉</h5>
                        </div>
                        <!-- 20230320 수정 -->
                        <div class="ticket_gallery">
                            <div class="btns">
                                <div id="prevTicket" class="leftArrow">
                                    <img src="/assets/new_join/img/step_05_rightArrow.svg" alt="left">
                                    <p id="prevTicketName">이전</p>
                                </div>
                                <div id="nextTicket" class="rightArrow">
                                    <img src="/assets/new_join/img/step_05_rightArrow.svg" alt="right">
                                    <p id="nextTicketName">다음</p>
                                </div>
                            </div>
                            <ul>
                                <c:forEach var="goods" items="${goodsList}" varStatus="status">
                                    <li class="items" data-idx="${status.index}" data-seq="${goods.GOODS_SEQ_NO}" data-opt="${goods.GOODS_OPT_SEQ_NO}" 
                                        data-fnaprc="${goods.FNA_PRC}" data-price="${goods.GOODS_BAS_PRC}" data-name="${goods.GOODS_NM}" data-typ="${goods.GOODS_TYP_CD}">
                                        <div class="center">
                                            <c:if test="${promoEligible == true}">
                                                <p class="specialText">* 특별 할인 프로모션 적용</p>
                                            </c:if>
                                            <h5>${goods.GOODS_NM}</h5>
                                            <div class="flex">
                                                <div class="mid">
                                                    <h4>
                                                        <c:choose>
                                                            <c:when test="${goods.FNA_PRC == '' || goods.FNA_PRC == null || goods.FNA_PRC == '0'}">
                                                                <fmt:formatNumber value="${goods.GOODS_BAS_PRC}"
                                                                        pattern="#,###" />
                                                                원
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:formatNumber value="${goods.FNA_PRC}"
                                                                        pattern="#,###" />
                                                                원
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </h4>
                                                    <small>(VAT 별도)</small>
                                                </div>
                                            </div>
                                            <span>무료체험 14일 이후 정기결제</span>
                                        </div>
                                        <div class="bottom">
                                            <div class="row">
                                                <span class="check"></span>
                                                <p>판매몰 계좌 : 
                                                    <c:choose>
                                                        <c:when test="${goods.SETT_ACC_ACCT_REG_PSB_CNT > 0}">
                                                            <p>${goods.SETT_ACC_ACCT_REG_PSB_CNT} 개 등록</p>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <p style="font-weight:bold;">무제한등록</p>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </p>
                                            </div>
                                            <div class="row">
                                                <span class="check"></span>
                                                <p>판매몰 ID : 
                                                    <c:choose>
                                                        <c:when test="${goods.SALE_MALL_REG_ID_PSB_CNT > 0}">
                                                            <p>${goods.SALE_MALL_REG_ID_PSB_CNT} 개 등록</p>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <p style="font-weight:bold;">무제한등록</p>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </p>
                                            </div>
                                            <div class="row">
                                                <span class="check"></span>
                                                <p>데이터 조회 : 일자별 상세 정산내역, 판매통계분석 및 그래프</p>
                                            </div>
                                            <div class="row">
                                                <span class="check"></span>
                                                <p>리포팅 서비스 : 매영업일 정산예정금 알림톡 리포트 발송</p>
                                            </div>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                    <div class="right">
                        <img class="logo" src="/assets/new_join/img/sellerbotLogo.svg" alt="sellerbot">
                        <div class="gallery">
                            <ul>
                                <li>
                                    <img src="/assets/new_join/img/step_06_img_01.png" alt="01">
                                </li>
                                <li>
                                    <img src="/assets/new_join/img/step_06_img_02.png" alt="02">
                                </li>
                                <li>
                                    <img src="/assets/new_join/img/step_06_img_03.png" alt="03">
                                </li>
                            </ul>
                        </div>
                        <div class="arrowBtns">
                            <div id="galleryLeft"><img src="/assets/new_join/img/rightArrow.svg" alt="leftArrow"></div>
                            <div id="galleryRight"><img src="/assets/new_join/img/rightArrow.svg" alt="rightArrow"></div>
                        </div>
                        <ul class="bottomUl">
                            <li class="active" data-text="정산예정금 조회"></li>
                            <li data-text="판매 분석"></li>
                            <li data-text="리포팅 서비스"></li>
                        </ul>
                    </div>
                </div>
            </article>
            <article id="totalPay">
                <h2>최종 결제금액</h2>
                <div class="flexBox">
                    <div class="left">
                        <h5>총 결제 금액</h5>
                        <h4 id="totalPrice">원</h4>
                        <small>(VAT 별도)</small>
                    </div>
                    <div class="right">
                        <div class="content">
                            <p class="gray">셀러봇캐시 이용권</p>
                            <div class="box">
                                <div class="blue" id="ticketName"></div>
                                <p id="ticketPrice"></p>
                            </div>
                            <span class="line"></span>
                            <p class="gray">가입비</p>
                            <div class="box">
                                <p>무료</p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article id="terms">
                <h2>약관동의</h2>
                <div class="accordion">
                    <div class="title">
                        <div class="left">
                            <div class="checkBoxDiv">
                                <input type="checkbox" id="terms_01" class="chkBox">
                                <label class="checkBox" for="terms_01"></label>
                                <label class="text" for="terms_01" style="color:black;"></label>
                            </div>
                            <p class="text_checkBox"><b>[필수]</b> 셀러봇캐시 유료결제 약관</p>
                        </div>
                        <span><img src="/assets/new_join/img/right-chevron.png" alt="right-chevron"></span>
                    </div>
                    <div class="panel">
                        <div class="paddingBox">
                            <pre id="termContent"></pre>
                        </div>
                    </div>
                </div>
            </article>
            <article id="payment">
                <h2>결제수단</h2>
                <button id="creditCard">신용카드</button>
            </article>
            <article id="caution">
                <h2>유의사항</h2>
                <div class="grayBox">
                    <pre>셀러봇캐시 가입을 위해서는 이용권 정기 결제를 위한 결제 수단 등록이 필수입니다.

첫 결제수단 등록 시, 무료체험 14일이 제공되며, 체험 종료 후 매월 정기결제가 진행됩니다.
결제 7일 전 알림을 보내드리고 있는 점 참고해주세요.

해지는 언제든지 가능하며, 해지 경로는 아래와 같습니다.
- [셀러봇캐시 홈페이지 → 마이페이지 → 결제정보 → (이용권 우측) 해지]
<!-- 20230323 문구 추가 -->
가상계좌 무통장입금 등 다른 결제 방식을 원하시는 경우, 우측 하단의 '1:1 상담톡' 을 통해 문의 해주세요.
                    </pre>
                </div>
            </article>
        </section>
        <c:choose>
            <c:when test="${DeviceType eq 'PC'}">
                <c:choose>
                    <c:when test="${mid eq 'INIBillTst'}">
                        <script language='javascript' type='text/javascript' src='https://stgstdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>
                    </c:when>
                    <c:otherwise>
                        <script language='javascript' type='text/javascript' src='https://stdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>
                    </c:otherwise>
                </c:choose>
                <form id="SendPayForm_id" method="POST">
                    <input type="hidden" name="version" value="1.0" >
                    <input type="hidden" name="mid" value="${mid}">
                    <input type="hidden" name="goodname">
                    <input type="hidden" name="oid">
                    <input type="hidden" name="price">
                    <input type="hidden" name="currency" value="WON">
                    <input type="hidden" name="buyername" value=${buyername}>
                    <input type="hidden" name="buyertel" value="${buyertel}">
                    <input type="hidden" name="buyeremail" value="${buyeremail}">
                    <input type="hidden" name="timestamp">
                    <input type="hidden" name="signature">
                    <input type="hidden" name="returnUrl" value="${siteDomain}/sub/payment/pay_payresponse">
                    <input type="hidden" name="closeUrl" value="${siteDomain}/pub/payment/pay_close">
                    <input type="hidden" name="mKey" value="${mKey}">
                    <input type="hidden" name="gopaymethod" value="">
                    <input type="hidden" name="acceptmethod" value="BILLAUTH(card):FULLVERIFY"> 
                    <input type="hidden" name="merchantData"> 
                    <input type="hidden" name="offerPeriod" value="M2">
                </form>
            </c:when>
            <c:otherwise>
                <form id="SendPayForm_id" name="SendPayForm_id" method="POST" action="https://inilite.inicis.com/inibill/inibill_card.jsp">
                    <input type="hidden" name="mid" value="${mid}">
                    <input type="hidden" name="buyername" value=${buyername}>            
                    <input type="hidden" name="goodname">
                    <input type="hidden" name="price">
                    <input type="hidden" name="orderid">
                    <input type="hidden" name="returnurl" value="${siteDomain}/sub/payment/pay_mobilepayresponse">
                    <input type="hidden" name="timestamp">
                    <input type="hidden" name="period" value="M2">
                    <input type="hidden" name="p_noti">
                    <input type="hidden" name="hashdata">
                    <input type="hidden" name="buyername" value=${buyername}>
                    <input type="hidden" name="buyertel" value="${buyertel}">
                    <input type="hidden" name="buyeremail" value="${buyeremail}">
                </form>
            </c:otherwise>
        </c:choose>
    </main>
</body>