<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_payment/css/regPay_01.css?ver=20231106_01">
    <script type="module" src="/assets/new_payment/js/regPay_01.js?ver=20231106_01" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <input type="hidden" id="mallCnt" value="${mallCnt}">
    <input type="hidden" id="usingGoodsReqNo" value="${usingGoods.basic_goods_seq_no}"/>
    <c:if test="${promAlertYn == 'Y'}">
        <script>
            alert("본 프로모션 대상이 아닙니다.");
        </script>
    </c:if>
    <main>
        <section>
            <div class="ticketFixed">
                <p><b>파이봇 할인형</b> 선택</p>
                <span class="close"></span>
            </div>
            <div class="ticketFixed_pc">
                <div class="closeBtnBox">
                    <button type="button" id="closeTicketFixed_pc"><img src="/assets/new_join/img/xmark.svg" alt="xmark"></button>
                </div>
                <div class="centerBox">
                    <p>
                        <selected-item>로니봇</selected-item>이 선택되었습니다.
                    </p>
                    <p>결제수단을 등록해주세요.</p>
                    <div class="btns">
                        <button type="button" id="hideTicketFixed_pc">닫기</button>
                        <button type="button" id="goto">결제수단 등록</button>
                    </div>
                </div>
            </div>
            <button type="button" class="fixed" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>셀러봇캐시 정기구독</h1>
                    <button type="button" id="serviceVid">서비스 소개 영상</button>
                </div>
                <p>정기 구독을 통해 정산예정금 조회, 판매분석, 리포팅서비스 등
                    셀러에게 꼭 필요한 다양한 컨텐츠를 셀러봇캐시에서 경험해보세요.</p>
            </article>
            <article id="subscribingTicket">
                <h3 class="articleTitle">구독중인 이용권</h3>
                <div class="grayBox">
                    <c:choose>
                        <c:when test="${usingGoods.basic_goods_req_seq_no > 0 || usingGoods.addn_goods_req_seq_no > 0}">
                            <div class="content">
                                <p>셀러봇캐시 이용권</p>
                                <c:choose>
                                    <c:when test="${usingGoods.basic_goods_req_seq_no > 0}">
                                        <c:set var="basicText" value="${usingGoods.basic_goods_nm}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="basicText" value="미사용"/>
                                    </c:otherwise>
                                </c:choose>
                                <div class="blue" id="basicName">${basicText}</div>
                                <span class="line"></span>
                                <p>자동대사서비스</p>
                                <div class="box">
                                    <c:choose>
                                        <c:when test="${usingGoods.addn_goods_req_seq_no > 0}">
                                            <c:set var="bankBotText" value="${usingGoods.addn_goods_nm}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="bankBotText" value="미사용"/>
                                        </c:otherwise>
                                    </c:choose>
                                    <div class="blue_outline" id="bankBotName">${bankBotText}</div>
                                </div>
                                <span class="line last"></span>
                                <p class="gray">이용권 변경, 해지는 [마이페이지 → 결제정보] 를 이용해주세요.</p>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <p class="noData">사용중인 이용권이 없습니다.<br>이용권을 등록하여 셀러봇캐시를 자유롭게 이용해보세요.</p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </article>
            <article id="subscribe">
                <h3 class="articleTitle">이용권 선택</h3>
                <div class="flexBox">
                    <!-- 기존 구독중인 이용권이 있을경우 item class에 subscribing을 추가합니다 -->
                    <!-- <div class="item subscribing" data-itemname="로니봇"> -->
                    <c:forEach var="goods" items="${goodsList}">
                        <c:if test="${usingGoods.basic_goods_opt_seq_no == goods.GOODS_OPT_SEQ_NO}">
                            <c:set var="classNm" value="item subscribing"/>
                        </c:if>
                        <c:if test="${usingGoods.basic_goods_opt_seq_no != goods.GOODS_OPT_SEQ_NO}">
                            <c:set var="classNm" value="item"/>
                        </c:if>
                        <c:if test="${goods.GOODS_TYP_CD == 'RNB' || goods.GOODS_TYP_CD == 'PYB'}">
                            <c:set var="visibleNm" value="style='display:none'"/>
                        </c:if>
                        <c:if test="${goods.GOODS_TYP_CD != 'RNB' && goods.GOODS_TYP_CD != 'PYB'}">
                            <c:set var="visibleNm" value=""/>
                        </c:if>
                        
                        <div class="${classNm}" ${visibleNm} data-itemname="${goods.GOODS_NM}" data-typecd="${goods.GOODS_TYP_CD}" data-goodsSeqNo = "${goods.GOODS_SEQ_NO}" data-optno = "${goods.GOODS_OPT_SEQ_NO}">
                            <div class="title">
                                <h2>${goods.GOODS_NM}</h2>
                                <span></span>
                            </div>
                            <div class="content">
                                <div class="tag"> 
                                    <c:if test="${goods.GOODS_TYP_CD == 'PYB' && goods.ADDN_YN == 'N' && goods.DEF_YN == 'Y'}">
                                        <img src="/assets/new_join/img/step_01_tag_blue.png" alt="추천">
                                    </c:if>
                                </div>
                                <c:if test="${goods.GOODS_TYP_CD != 'PAFP' && promoEligible == true}">
                                    <p class="specialText">* 특별 할인 프로모션 적용</p>
							        <p class="canceledText">월 <fmt:formatNumber value="${goods.GOODS_BAS_PRC}"
                                        pattern="#,###" />
                                원</p>
                                </c:if>
                                <h3>
                                    <c:choose>
                                        <c:when test="${goods.GOODS_TYP_CD == 'PAFP' && goods.ADDN_YN == 'N' && goods.DEF_YN == 'Y'}">
                                            금융사 고객 전용 이용권
                                        </c:when>
                                        <c:otherwise>
                                            월
                                            <c:choose>
                                                <c:when test="${goods.FNA_PRC == '' || goods.FNA_PRC == null || goods.FNA_PRC == '0'}">
                                                    <fmt:formatNumber value="${goods.GOODS_BAS_PRC}"
                                                            pattern="#,###" />
                                                    원<small>(VAT 별도)</small>
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:formatNumber value="${goods.FNA_PRC}"
                                                            pattern="#,###" />
                                                    원<small>(VAT 별도)</small>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </h3>
                                <span class="line"></span>
                                <div class="flexRow">
                                    <div class="left">
                                        <p>가입비</p>
                                    </div>
                                    <div class="right">
                                        <span class="check"></span>
                                        <div class="discount">
                                            <p class="dc">30,000원</p>
                                            <p class="blue">무료</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="flexRow">
                                    <div class="left">
                                        <p>판매몰 계좌</p>
                                    </div>
                                    <div class="right">
                                        <span class="check"></span>
                                        <c:choose>
                                            <c:when test="${goods.SETT_ACC_ACCT_REG_PSB_CNT > 0}">
                                                <p>${goods.SETT_ACC_ACCT_REG_PSB_CNT} 개 등록</p>
                                            </c:when>
                                            <c:otherwise>
                                                <p style="font-weight:bold;">무제한등록</p>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                                <div class="flexRow">
                                    <div class="left">
                                        <p>판매몰 ID</p>
                                    </div>
                                    <div class="right">
                                        <span class="check"></span>
                                        <c:choose>
                                            <c:when test="${goods.SALE_MALL_REG_ID_PSB_CNT > 0}">
                                                <p>${goods.SALE_MALL_REG_ID_PSB_CNT} 개 등록</p>
                                            </c:when>
                                            <c:otherwise>
                                                <p style="font-weight:bold;">무제한등록</p>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                                <div class="btnImg">
                                    <c:choose>
                                        <c:when test="${freeYn == 'Y'}">
                                            <img class="white" src="/assets/new_join/img/button_off_payment.png" alt="btnImg">
                                            <img class="blue" src="/assets/new_join/img/button_on_payment.png" alt="btnImgActivated">
                                        </c:when>
                                        <c:otherwise>
                                            <img class="white" src="/assets/new_join/img/step_01_btn_white.png" alt="btnImg">
                                            <img class="blue" src="/assets/new_join/img/step_01_btn_blue.png" alt="btnImgActivated">
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <c:if test="${goods.GOODS_TYP_CD == 'PAFP' && goods.ADDN_YN == 'N' && goods.DEF_YN == 'Y'}">
                                    <div class="banner-container">
                                        <div class="banner">
                                            <!-- <img src="/assets/new_join/img/logo_kb.jpg" alt="kb"> -->
                                            <img src="/assets/new_join/img/logo_kiwoom.png" alt="kw">
                                            <img src="/assets/new_join/img/logo_shb.jpg" alt="shb">
                                            <img src="/assets/new_join/img/logo_sc.jpg" alt="sc">
                                            <img src="/assets/new_join/img/logo_sodeone.jpg" alt="sodeone">
                                            <!-- <img src="/assets/new_join/img/logo_kb.jpg" alt="kb"> -->
                                            <img src="/assets/new_join/img/logo_kiwoom.png" alt="kw">
                                            <img src="/assets/new_join/img/logo_shb.jpg" alt="shb">
                                            <img src="/assets/new_join/img/logo_sc.jpg" alt="sc">
                                            <img src="/assets/new_join/img/logo_sodeone.jpg" alt="sodeone">
                                        </div>
                                    </div>
                                </c:if>
                            </div>
                            <c:choose>
                                <c:when test="${freeYn == 'Y'}">
                                    <span class="noti">첫 결제수단 등록 및 14일 무료체험 종료 후 매월 자동결제</span>
                                </c:when>
                                <c:otherwise>
                                    <span class="noti">14일 무료체험 종료 후 매월 자동결제</span>
                                </c:otherwise>
                            </c:choose>
                           
                        </div>
                    </c:forEach>
                </div>
            </article>
            <c:if test="${cafe24_usr_yn == 'Y'}">
                <article id="bankBot">
                    <h3 class="articleTitle">셀러봇캐시 자동대사 서비스<i class="blue">(각 이용권에 추가하여 사용 가능합니다. 예시: 기가봇-뱅크봇)</i></h3>
                    <div class="flexBox">
                        <c:choose>
                            <c:when test="${usingGoods.addn_goods_opt_seq_no > 0}">
                                <c:set var="bankBotClass" value="item subscribing"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="bankBotClass" value="item"/>
                            </c:otherwise>
                        </c:choose>
                        <div class="${bankBotClass}" data-itemname="뱅크봇">
                            <div class="title">
                                <h2>뱅크봇</h2>
                            </div>
                            <div class="content">
                                <h3>기간한정 무료</h3>
                                <p class="gray">프로모션 종료 시 결제 후 이용가능</p>
                                <span class="line"></span>
                                <div class="flexRow">
                                    <div class="right">
                                        <span class="check"></span>
                                        <p>카페24 회원전용</p>
                                    </div>
                                </div>
                                <div class="flexRow">
                                    <div class="right">
                                        <span class="check"></span>
                                        <p>자동입금확인서비스</p>
                                    </div>
                                </div>
                                <div class="flexRow">
                                    <div class="right">
                                        <span class="check"></span>
                                        <p>자동입금확인 현황 알림톡</p>
                                    </div>
                                </div>
                                <div class="btnDiv">
                                    <p>무료 프로모션 진행 중</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </c:if>
            <c:if test="${shopby_usr_yn == 'Y'}">
                <article id="bankBot">
                    <h3 class="articleTitle">셀러봇캐시 자동대사 서비스<i class="blue">(각 이용권에 추가하여 사용 가능합니다. 예시: 기가봇-뱅크봇)</i></h3>
                    <div class="flexBox">
                        <c:choose>
                            <c:when test="${usingGoods.addn_goods_opt_seq_no > 0}">
                                <c:set var="bankBotClass" value="item subscribing"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="bankBotClass" value="item"/>
                            </c:otherwise>
                        </c:choose>
                        <div class="${bankBotClass}" data-itemname="뱅크봇">
                            <div class="title">
                                <h2>뱅크봇</h2>
                            </div>
                            <div class="content">
                                <h3>기간한정 무료</h3>
                                <p class="gray">프로모션 종료 시 결제 후 이용가능</p>
                                <span class="line"></span>
                                <div class="flexRow">
                                    <div class="right">
                                        <span class="check"></span>
                                        <p>샵바이 회원전용</p>
                                    </div>
                                </div>
                                <div class="flexRow">
                                    <div class="right">
                                        <span class="check"></span>
                                        <p>자동입금확인서비스</p>
                                    </div>
                                </div>
                                <div class="flexRow">
                                    <div class="right">
                                        <span class="check"></span>
                                        <p>자동입금확인 현황 알림톡</p>
                                    </div>
                                </div>
                                <div class="btnDiv">
                                    <p>무료 프로모션 진행 중</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </c:if>
            <div class="grayBanner">
                <div class="content">
                    <img src="/assets/new_join/img/step_01_bill.png" alt="bill">
                    <p>유료 버전 사용 후 만족스럽지 않으면 언제든지 해지 가능합니다.</p>
                </div>
            </div>
            <article id="detail">
                <h1>상세기능</h1>
                <div class="flexBox">
                    <div class="box">
                        <div class="background shadow">
                            <span></span>
                            <p>정산예정금 조회</p>
                            <img src="/assets/new_join/img/step_01_detail_1.png" alt="01">
                        </div>
                        <p class="bottom">한 번에 확인 가능한 일자별 상세 정산내역</p>
                    </div>
                    <div class="box">
                        <div class="background">
                            <span></span>
                            <p>판매분석</p>
                            <img src="/assets/new_join/img/step_01_detail_2.png" alt="02">
                        </div>
                        <p class="bottom">통계분석 및 그래프 등 매출 현황 및 추이 분석</p>
                    </div>
                    <div class="box last">
                        <div class="background">
                            <span></span>
                            <p>리포팅 서비스</p>
                            <img src="/assets/new_join/img/step_01_detail_3.png" alt="03">
                        </div>
                        <p class="bottom">일자별/판매몰별/배송상태별<br>정산예정금 알림톡 서비스</p>
                    </div>
                </div>
                <div class="gridBox">
                    <div class="gridItem">
                        <div class="left">
                            <div class="content">
                                <span class="check"></span>
                                <p>정산계좌 통합관리</p>
                            </div>
                        </div>
                        <div class="right">
                            <p>-판매몰별 입금내역</p>
                            <p>-정산계좌 입출금 상세내역</p>
                        </div>
                    </div>
                    <div class="gridItem">
                        <div class="left">
                            <div class="content">
                                <span class="check"></span>
                                <p>점프 서비스</p>
                            </div>
                        </div>
                        <div class="right">
                            <p>-판매몰부터 금융사, 쇼핑몰관리, 매입채널, 관공서까지 로그인 절차없이 필요한 사이트로 바로 이동</p>
                        </div>
                    </div>
                    <div class="gridItem">
                        <div class="left">
                            <div class="content">
                                <span class="check"></span>
                                <p>동종업계 매출추이</p>
                            </div>
                        </div>
                        <div class="right">
                            <p>-나의 매출랭킹</p>
                            <p>-매출랭킹 변동추이</p>
                            <p>-분야별 랭킹 TOP100</p>
                        </div>
                    </div>
                    <div class="gridItem">
                        <div class="left">
                            <div class="content">
                                <span class="check"></span>
                                <p>금융 서비스</p>
                            </div>
                        </div>
                        <div class="right">
                            <p>-내게 맞는 금융상품 추천 받기</p>
                        </div>
                    </div>
                </div>
            </article>
            <div class="serviceBanner">
                <div class="left">
                    <h3>셀러들에게 꼭 필요한 서비스</h3>
                </div>
                <div class="right">
                    <!-- 20230323 data-number 및 row 순서 변경 -->
                    <div class="row">
						<h4 class="decimal" data-text="연간 고객 매출액" data-number="${fn:substring(siteInfo.sumSalsePrc,0,2) }" data-intersected="false" data-number-string="${numberString}">0</h4>
                        <p>오늘도 많은 고객님들이 셀러봇캐시와 함께 성장을 경험하고 있습니다.</p>
                    </div>
                    <span></span>
                    <!-- 20230323 data-number 및 row 순서 변경 -->
                    <div class="row">
						<h4 data-text="누적 판매량" data-number="${siteInfo.goodsCnt}" data-intersected="false">0</h4>
                        <p>지금 이 순간에도 매일 셀러봇캐시를 이용하는 사람들이 증가하고 있습니다.</p>
                    </div>
                    <span></span>
                    <div class="row">
						<h4 data-text="지원 판매몰" data-number="${siteInfo.mallCnt}" data-intersected="false">0</h4>
                        <p>11번가, 티몬, 롯데온, 이니시스 등 오픈마켓부터 소셜커머스, 종합몰, 전문몰, PG/VAN사까지 다양한 판매몰들의 정산내역을 통합 관리할 수 있습니다.</p>
                    </div>
                </div>
            </div>
            <div class="questionBanner">
                <div class="left">
                    <h3>셀러봇캐시 요금 관련 자주 묻는 질문</h3>
                </div>
                <div class="right">
                    <div class="accordion">
                        <div class="title">
                            <p>셀러봇캐시 요금제(메가봇, 기가봇, 테라봇)는 어떤 차이가 있나요 ?</p>

                            <span><img src="/assets/new_join/img/right-chevron.png" alt="right-chevron"></span>
                        </div>
                        <div class="panel">
                            <div class="paddingBox">
                                <pre>셀러봇캐시의 요금제는 해당 대상과 데이터 제공량에 따라 구분됩니다.

                                    메가봇 요금제는 판매몰 계좌와 판매몰 ID를 각각 5개씩 등록 가능하며 셀러봇캐시 일반 고객 대상의 요금제입니다.
                                    기가봇 요금제는 판매몰 계좌와 판매몰 ID를 무제한으로 등록 가능하며 셀러봇캐시 일반 고객 대상의 요금제입니다.
                                    테라봇 요금제는 판매몰 계좌와 판매몰 ID를 무제한으로 등록 가능하며 KB국민은행, 신한은행, SC제일은행 등 금융사를 이용하는 고객 대상의 금융사 이용 고객 전용 요금제입니다.
                                    
                                    해당 이용권은 제공 되는 내용은 기가봇 요금제와 동일하나 이용 요금은 메가봇 요금제와 동일하게 적용됩니다.</pre>
                            </div>
                        </div>
                    </div>
                    <div class="accordion">
                        <div class="title">
                            <p>14일 무료체험이 끝나면 바로 결제가 이루어지나요?</p>

                            <span><img src="/assets/new_join/img/right-chevron.png" alt="right-chevron"></span>
                        </div>
                        <div class="panel">
                            <div class="paddingBox">
                                <pre>네! 14일 무료체험 이후 가입시 등록한 자동이체 방법으로 이용요금을 납입합니다.<br>
                                    셀러봇캐시는 무료체험 기간 종료 7일 전 고객님께 알림을 보내 드리고 있으니 혹시나 갑작스러운 정기 결제에 안심하셔도 됩니다 :&#41;</pre>
                            </div>
                        </div>
                    </div>
                    <div class="accordion">
                        <div class="title">
                            <p>결제수단 등록 중 오류가 발생했어요!</p>

                            <span><img src="/assets/new_join/img/right-chevron.png" alt="right-chevron"></span>
                        </div>
                        <div class="panel">
                            <div class="paddingBox">
                                <pre>결제수단 등록 중 오류가 발생하시는 경우, 아래의 사항을 확인해주세요.<br>
                                    1&#41; 개인/법인 구분이잘 되었는지 확인을 해주세요.
                                    - 실물 카드의 겉면에 '성함'만 표시되어 있는 경우 '개인' 카드일 가능성이 높아요.
                                    반대로 '성함+법인명' 또는 '법인명'만 표시되어 있는 경우에는 '법인'으로 등록해주세요.<br>
                                    2&#41; 카드 정보와 명의자 정보가 제대로 입력되어있는지 확인해주세요.
                                    - 카드번호나 비밀번호 등 카드 정보와 성함, 생년월일 등 명의자 정보가 일치하지 않는 경우 오류가 나타날 수 있어요.<br>
                                    3&#41; 그 외의 오류, 문의사항은 우측 하단의 [1:1 상담톡] 으로 문의를 남겨주시면, 고객센터 운영시간 중에 빠르고 친절한 답변을 해드릴게요!
                                    </pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</body>
<form id="form" name="form" action="/sub/payment/step1" method="post">
    <input type="hidden" id="goodsSeqNo" name="goodsSeqNo" value=""/>
    <input type="hidden" id="goodsOptSeqNo" name="goodsOptSeqNo" value=""/>
</form>
