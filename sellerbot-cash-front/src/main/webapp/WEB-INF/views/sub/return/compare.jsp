<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script src="/assets/js/Chart.bundle.min.js"></script>

<!--amChart-->
<script src="/assets/amchart/core.js"></script>
<script src="/assets/amchart/charts.js"></script>
<script src="/assets/amchart/animated.js"></script>

<script src="/assets/js/common/common.js"></script>
<script src="/assets/js/common/util.js"></script>
<script src="/assets/js/return.js"></script>

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-grid.js"></script>

<div class="container">
    <div class="sales_wra">
        <div class="sales_area">
        <div class="menu_top">
            <p class="menu_name">반품통계</p>
            <div class="gnb_topBox">
            <ul class="gnb_top clearfix">
                <li><a href="/sub/return/return">반품 현황</a></li>
                <li class="focus"><a href="/sub/return/compare">반품분석 그래프</a></li>
            </ul>
            </div>
        </div>
                    <div class="summary_report_section_wrap">
            <div class="summary_report_section">
            <p class="title_summary open">[반품현황 요약보고]</p>
            <div class="summary_section_card">
                <div id="dataFirst" class="summary_card first" style="display: none;">
                <div class="title_highlight">
                    <p>${prevMonth} 반품률</p>
                </div>
                <div class="graph_summary type1">
                    <canvas id="spGh1" height="150"></canvas>
                    <div class="gh_line"></div>
                    </div>
                <p class="desc_summary">
                        ${prevMonth} 반품률은<br>
                        <c:if test="${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto > 0}">
                            전월 대비 <span class="red">${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}% 증가</span>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto ne 0}">
                            하였고,<br>
                            </c:if>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto eq 0}">
                            하였습니다.
                            </c:if>
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto < 0}">
                            전월 대비 <span class="blue">${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}% 감소</span>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto ne 0}">
                            하였고,<br>
                            </c:if>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto eq 0}">
                            하였습니다.
                            </c:if>
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto > 0}">
                            전년 동월대비 <span class="red">${rtnSummaryLast.last_year_same_month_rtn_num_rto}% 증가</span>하였습니다.
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto < 0}">
                            전년 동월대비 <span class="blue">${rtnSummaryLast.last_year_same_month_rtn_num_rto}% 감소</span>하였습니다.
                        </c:if>
                </p>
                </div>
                <!--nodata_1-->
                <div id="no_dataFirst" class="summary_card first first_nodata">
                    <div class="title_highlight">
                    <p class="first_nodata_title">${prevMonth} 반품률</p>
                    </div>
                    <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                    </div>
                </div>
                <div id="dataSecond" class="summary_card second" style="display: none;">
                <div class="title_highlight">
                    <p>최근 1년 반품률</p>
                </div>
                <div class="graph_summary type2">
                    <canvas id="spGh2" height="150"></canvas>
                </div>
                <p class="desc_summary">
                    최근 1년 중 반품률은<br>
                            <span class="red"><script>document.write(convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_max_month}', "년 ", "월"));</script></span>이
                            <span class="red"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_max}" pattern="#,###.##" />%</span>로 가장 높았고,<br>
                            <span class="blue"><script>document.write(convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_min_month}', "년 ", "월"));</script></span>이
                            <span class="blue"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_min}" pattern="#,###.##" />%</span>로 가장 낮았으며,<br>
                            <span class="puple average">월 평균 반품률</span>은 
                            <span class="puple average"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_avg}" pattern="#,###.##" />%</span>입니다.
                </p>
                </div>
                <!--nodata_2-->
                <div id="no_dataSecond" class="summary_card second first_nodata">
                    <div class="title_highlight">
                    <p class="second_nodata_title">최근 1년 반품률</p>
                    </div>
                    <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                    </div>
                </div>
                <div id="dataThird" class="summary_card third" style="display: none;">
                </div>
                <!--nodata_3-->
                <div id="no_dataThird" class="summary_card third first_nodata">
                    <div class="title_highlight">
                    <p id="thirdDateRange" class="third_nodata_title">조회기간 기준 (${searchDateRangeStr})</p>
                    </div>
                    <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                    </div>
                </div>
                <div class="summary_card last">
                <div class="return_last_top_title">
                    <h1>${prevMonth2}</h1>
                    <h2>지난달 반품률</h2>
                </div>
                <h1 class="retuen_per"><b class="per_bold_up">${rtnSummaryLast.last_month_rtn_num_rto}</b>%</h1>
                <div class="talk_advice_box">
                    <p class="lb_advice">[도움말]</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>반품률 산출기준</b><br>
                    판매몰에서 제공하는 반품관리에서 필요한 데이터를<br />
                    집계하여 산출합니다.</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">반품률은 수량 대비 반품률입니다.</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로 가능합니다.</p>
                </div>
                </div>
            </div>
            </div>
        </div>
        <!-- 모바일버전을 위한 상단 swiper 추가 -->
        <!--960 반응형-->
        <div class="summary_report_section_m_Box" style="display:none;">
            <div class="swiper-container summary_report_section_m">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[반품현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div id="dataFirst_m_Box" class="summary_card first" style="display: none;">
                        <div class="title_highlight">
                        <p>${prevMonth} 반품률</p>
                        </div>
                        <div class="graph_summary type1">
                            <canvas id="spGh1_m" height="150"></canvas>
                            <div class="gh_line"></div>
                        </div>
                        <p class="desc_summary">
                            ${prevMonth} 반품률은<br>
                            <c:if test="${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto > 0}">
                            전월 대비 <span class="red">${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}% 증가</span>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto ne 0}">
                            하였고,<br>
                            </c:if>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto eq 0}">
                            하였습니다.
                            </c:if>
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto < 0}">
                            전월 대비 <span class="blue">${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}% 감소</span>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto ne 0}">
                            하였고,<br>
                            </c:if>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto eq 0}">
                            하였습니다.
                            </c:if>
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto > 0}">
                            전년 동월대비 <span class="red">${rtnSummaryLast.last_year_same_month_rtn_num_rto}% 증가</span>하였습니다.
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto < 0}">
                            전년 동월대비 <span class="blue">${rtnSummaryLast.last_year_same_month_rtn_num_rto}% 감소</span>하였습니다.
                        </c:if>
                        </p>
                    </div>
                    <!--nodata_1-->
                    <div id="no_dataFirst_m_Box" class="summary_card first first_nodata">
                        <div class="title_highlight">
                        <p class="first_nodata_title">${prevMonth} 반품률</p>
                        </div>
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>
                    <div id="dataSecond_m_Box" class="summary_card second" style="display: none;">
                        <div class="title_highlight">
                        <p>최근 1년 반품률</p>
                        </div>
                        <div class="graph_summary type2">
                        <canvas id="spGh2_m" height="150"></canvas>
                        </div>
                        <p class="desc_summary">
                            최근 1년 중 반품률은<br>
                                    <span class="red"><script>document.write(convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_max_month}', "년 ", "월"));</script></span>이
                                    <span class="red"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_max}" pattern="#,###.##" />%</span>로 가장 높았고,<br>
                                    <span class="blue"><script>document.write(convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_min_month}', "년 ", "월"));</script></span>이
                                    <span class="blue"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_min}" pattern="#,###.##" />%</span>로 가장 낮았으며,<br>
                                    <span class="puple average">월 평균 반품률</span>은 
                                    <span class="puple average"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_avg}" pattern="#,###.##" />%</span>입니다.
                        </p>
                    </div>
                    <!--nodata_2-->
                    <div id="no_dataSecond_m_Box" class="summary_card second first_nodata">
                        <div class="title_highlight">
                            <p class="second_nodata_title">최근 1년 반품률</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[반품현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div id="dataThird_m_Box" class="summary_card third" style="display: none;">
                    </div>
                    <!--nodata_3-->
                    <div id="no_dataThird_m_Box" class="summary_card third first_nodata">
                        <div class="title_highlight">
                            <p id="thirdDateRange" class="third_nodata_title">조회기간 기준 (${searchDateRangeStr})</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    <div class="summary_card last">
                        <div class="return_last_top_title">
                        <h1>${prevMonth2}</h1>
                        <h2>지난달 반품률</h2>
                        </div>
                        <h1 class="retuen_per"><b class="per_bold_up">${rtnSummaryLast.last_month_rtn_num_rto}</b>%</h1>
                        <div class="talk_advice_box">
                        <p class="lb_advice">[도움말]</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>반품률 산출기준</b><br>
                            판매몰에서 제공하는 반품관리에서 필요한 데이터를
                            집계하여 산출합니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">반품률은 수량 대비 반품률입니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로 가능합니다.</p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="swiper-pagination1">
            </div>
        </div>
        <!--320 반응형-->
        <div class="summary_report_section_m_Box_2" style="display:none;">
            <div class="swiper-container summary_report_section_m_2">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[반품현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div id="dataFirst_m_Box_2" class="summary_card first" style="display: none;">
                        <div class="title_highlight">
                        <p>${prevMonth} 반품률</p>
                        </div>
                        <div class="graph_summary type1">
                            <canvas id="spGh1_m_1" height="150"></canvas>
                            <div class="gh_line"></div>
                        </div>
                        <p class="desc_summary">
                            ${prevMonth} 반품률은<br>
                            <c:if test="${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto > 0}">
                            전월 대비 <span class="red">${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}% 증가</span>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto ne 0}">
                            하였고,<br>
                            </c:if>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto eq 0}">
                            하였습니다.
                            </c:if>
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto < 0}">
                            전월 대비 <span class="blue">${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}% 감소</span>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto ne 0}">
                            하였고,<br>
                            </c:if>
                            <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto eq 0}">
                            하였습니다.
                            </c:if>
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto > 0}">
                            전년 동월대비 <span class="red">${rtnSummaryLast.last_year_same_month_rtn_num_rto}% 증가</span>하였습니다.
                        </c:if>
                        <c:if test="${rtnSummaryLast.last_year_same_month_rtn_num_rto < 0}">
                            전년 동월대비 <span class="blue">${rtnSummaryLast.last_year_same_month_rtn_num_rto}% 감소</span>하였습니다.
                        </c:if>
                        </p>
                    </div>
                    <!--nodata_1-->
                    <div id="no_dataFirst_m_Box_2" class="summary_card first first_nodata">
                        <div class="title_highlight">
                            <p class="first_nodata_title">${prevMonth} 반품률</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[반품현황 요약보고]</p>
                    <div class="summary_section_card summary_section_card_display">
                    <div id="dataSecond_m_Box_2" class="summary_card second" style="display: none;">
                        <div class="title_highlight">
                        <p>최근 1년 반품률</p>
                        </div>
                        <div class="graph_summary type2">
                        <canvas id="spGh2_m_2" height="150"></canvas>
                        </div>
                        <p class="desc_summary">
                            최근 1년 중 반품률은<br>
                                    <span class="red"><script>document.write(convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_max_month}', "년 ", "월"));</script></span>이
                                    <span class="red"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_max}" pattern="#,###.##" />%</span>로 가장 높았고,<br>
                                    <span class="blue"><script>document.write(convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_min_month}', "년 ", "월"));</script></span>이
                                    <span class="blue"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_min}" pattern="#,###.##" />%</span>로 가장 낮았으며,<br>
                                    <span class="puple average">월 평균 반품률</span>은 
                                    <span class="puple average"><fmt:formatNumber value="${rtnSummaryLast.last_year_rtn_rto_num_avg}" pattern="#,###.##" />%</span>입니다.
                        </p>
                    </div>
                    <!--nodata_2-->
                    <div id="no_dataSecond_m_Box_2" class="summary_card second first_nodata">
                        <div class="title_highlight">
                            <p class="second_nodata_title">최근 1년 반품률</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[반품현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div id="dataThird_m_Box_2" class="summary_card third" style="display: none;">
                    </div>
                    <!--nodata_3-->
                    <div id="no_dataThird_m_Box_2" class="summary_card third first_nodata">
                        <div class="title_highlight">
                            <p id="thirdDateRange" class="third_nodata_title">조회기간 기준 (${searchDateRangeStr})</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[반품현황 요약보고]</p>
                    <div class="summary_section_card summary_section_card_display">
                    <div class="summary_card last">
                        <div class="return_last_top_title">
                        <h1>${prevMonth2}</h1>
                        <h2>지난달 반품률</h2>
                        </div>
                        <h1 class="retuen_per"><b class="per_bold_up">${rtnSummaryLast.last_month_rtn_num_rto}</b>%</h1>
                        <div class="talk_advice_box">
                        <p class="lb_advice">[도움말]</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>반품률 산출기준</b><br>
                            판매몰에서 제공하는 반품관리에서 필요한 데이터를
                            집계하여 산출합니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">반품률은 수량 대비 반품률입니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로 가능합니다.</p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="swiper-pagination2">
            </div>
        </div>
        <!--swiper모바일-->
        <script>
            $(function() {
                var swiper = new Swiper('.summary_report_section_m', {
                    pagination: {
                        el: '.swiper-pagination1',
                    },
                });
                var swiper = new Swiper('.summary_report_section_m_2', {
                    pagination: {
                        el: '.swiper-pagination2',
                    },
                });
            });
        </script>
        <!--end-->
        <div class="summary_graph_section">
            <div class="period_graph_box">
            
            <!-- datepicker -->
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="datepicker-input-start" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
                </div>
                <div class="datepicker-cell" id="datepicker-month-start" style="margin-top: -1px;"></div>
                <h1 style="display:inline-block;display: inline-block;vertical-align: middle;font-size: 1rem;"> ~ </h1>
                <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="datepicker-input-end" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
                </div>
                <div class="datepicker-cell" id="datepicker-month-end" style="margin-top: -1px;"></div>
                <!-- datepicker -->
                
            <div class="btn_period_group">
                <button id="searchBtn">조회하기</button>
                <button id="lastYearBtn" type="button" class="btn_gray">최근 1년보기</button>
            </div>
            </div>
            <div class="period_graph_box period_graph_box_m" style="display:none;">
            <div class="choice_mall_type">
                <span class="lb">쇼핑몰 선택</span>
                <select class="sctbox_mall">
                <option value="">전체</option>
                </select>
            </div>
            <div class="choice_period">
                <span class="lb">기간선택</span>
                <div class="period_control year period_control_margin_1">
                <span class="arrow_prev">◀</span>
                <p>2018년</p>
                <span class="arrow_next">▶</span>
                </div>
                <div class="period_control month">
                <span class="arrow_prev">◀</span>
                <p>4월</p>
                <span class="arrow_next">▶</span>
                </div>
                <div class="icon_period">부터</div>
                <div class="choice_sub_wrap">
                <span class="lb">까지</span>
                <div class="period_control year period_control_margin_2">
                    <span class="arrow_prev">◀</span>
                    <p>2018년</p>
                    <span class="arrow_next">▶</span>
                </div>
                <div class="period_control month">
                    <span class="arrow_prev">◀</span>
                    <p>4월</p>
                    <span class="arrow_next">▶</span>
                </div>
                <div class="icon_period">까지</div>
                </div>
            </div>
            <div class="btn_period_group">
                <button>조회하기</button>
                <button type="button" class="btn_gray">최근 1년보기</button>
            </div>
            </div>
        </div>
        <div class="new_chart_top new_chart_wrapper">
            <div id="mallGraphArea" class="chart_wrapper">
            <span class="chart_wrapper_title">
                판매몰별 반품률
            </span>
            <div id="re_compare_grid_1">
            </div>
            <div class="chart_text_wrap">
                <h1>합계</h1>
                <h2 id="mallRtnNumSum"><span><script>document.write(fnAddComma('${graphInfo.rtn_num_sum}'));</script></span>건</h2>
            </div>
            </div>
            <div id="marketGraphArea" class="chart_wrapper">
            <span class="chart_wrapper_title">
                마켓유형별 반품률
            </span>
            <div id="re_compare_grid_2"></div>
            <div class="chart_text_wrap">
                <h1>합계</h1>
                <h2 id="marketRtnNumSum"><span><script>document.write(fnAddComma('${graphInfo.rtn_num_sum}'));</script></span>건</h2>
            </div>
            </div>
            <!--no_data-->
            <div id="graphNoDataArea" class="new_chart_bottom new_chart_wrapper new_chart_noData">
            <div class="chart_wrapper">
                <span class="chart_wrapper_title">
                판매몰별 반품률
                </span>

                <div id="graph_no_data">데이터가 없습니다.</div>

            </div>
            <div class="chart_wrapper">
                <span class="chart_wrapper_title">
                마켓유형별 반품률
                </span>
                <div id="graph_no_data">데이터가 없습니다.</div>
            </div>
            </div>
        </div>
        <!--amchart-->
        <!-- <script>
            am4core.ready(function() {
            
            }); // end am4core.ready()
        </script> -->
        </div>
    </div>
    </div>

<style>
    .swiper-pagination1,
    .swiper-pagination2 {
        margin: 0 auto;
        text-align: center;
        width: 100%;
        bottom: 2.5rem;
        position: absolute;
        z-index: 6;
    }

    .swiper-pagination-bullet {
        margin-right: 0.5rem;
    }

    .swiper-pagination-bullet:last-of-type {
        margin-right: 0;
    }

    .tui-datepicker-input>input {
        width: 100%;
        height: 100%;
        padding: 6px 27px 6px 10px;
        font-size: 12px;
        line-height: 14px;
        vertical-align: top;
        border: 0;
        color: #333;
        border-radius: 3rem;
    }

    .tui-datepicker-input {
        position: relative;
        display: inline-block;
        width: 120px;
        height: 2.5rem;
        vertical-align: top;
        border: 1px solid #ddd;
        border-radius: 3rem;
    }

    .datepicker-cell {
        display: inline-block;
    }

    .tui-datepicker-input.tui-has-focus {
        vertical-align: middle;
        border: 0;
    }

    .tui-datepicker {
        border: 1px solid #aaa;
        background-color: white;
        position: absolute;
        margin-left: -11rem;
        margin-top: 1rem;
        z-index: 999;
    }
</style>
<!-- 반품 통계 화면용 스크립트 -->
<script>
// 데이트피커 설정
var today = Date.now();

var startMonthPicker = new tui.DatePicker('#datepicker-month-start', {
    date: stringToDate_YYYYMM('${startDate}'),
    language: 'ko',
    type: 'month',
    input: {
        element: '#datepicker-input-start',
        format: 'yyyy-MM'
    },
    selectableRanges: [
        [new Date(0), moment(today).subtract(11, 'month').toDate()]
    ]
});

var endMonthPicker = new tui.DatePicker('#datepicker-month-end', {
    date: stringToDate_YYYYMM('${endDate}'),
    language: 'ko',
    type: 'month',
    input: {
        element: '#datepicker-input-end',
        format: 'yyyy-MM'
    },
    selectableRanges: [
        [new Date(0), today]
    ]
});

// 현황 표시
// 지난달 반품률
var last_month_per_before_last_month_rtn_num_rto = parseFloat(nvl('${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}', "0.0"));
var last_year_same_month_rtn_num_rto = parseFloat(nvl('${rtnSummaryLast.last_year_same_month_rtn_num_rto}', "0.0"));

if (last_month_per_before_last_month_rtn_num_rto != 0 || last_year_same_month_rtn_num_rto != 0) {
    visibleStatus("dataFirst", true)
}

// 최근 1년 반품률
var last_year_rtn_num_max_month = '${rtnSummaryLast.last_year_rtn_num_max_month}';
var last_year_rtn_num_min_month = '${rtnSummaryLast.last_year_rtn_num_min_month}';

if (last_year_rtn_num_max_month != last_year_rtn_num_min_month) {
    visibleStatus("dataSecond", true)
}

// 조회기간 기준
var rtnSummaryPeriod = JSON.parse('${rtnSummaryPeriod}');

if ((rtnSummaryPeriod.max_rtn_num_rto_mall_cd != rtnSummaryPeriod.min_rtn_num_mall_cd)
    || (rtnSummaryPeriod.max_rtn_num_rto_mall_typ_cd != rtnSummaryPeriod.min_rtn_num_mall_typ_cd)) {
    visibleStatus("dataThird", true)
    createSummaryPeriod(rtnSummaryPeriod);
}

// 그래프 표시
var rtnNumSum = '${graphInfo.rtn_num_sum}';
if (rtnNumSum == 0) {
    $("#graphNoDataArea").show();
    $("#mallGraphArea").hide();
    $("#marketGraphArea").hide();
}
else {
    $("#graphNoDataArea").hide();
    $("#mallGraphArea").show();
    $("#marketGraphArea").show();
}

// 조회하기 이벤트
$("#searchBtn").click(function () {
    searchGraph();
});

// 최근 1년보기
$("#lastYearBtn").click(function () {
    endMonthPicker.setDate(getAddMonth(new Date(), -1));
});

// 조회 시작일 변경 이벤트
// 조회 시작일 기준 12개월 후 혹은 금월까지 범위 설정
startMonthPicker.on('change', function () {
    var today = new Date();
    var end = getAddMonth(startMonthPicker.getDate(), 11);

    if (end > today)
        endMonthPicker.setDate(today);
    else
        endMonthPicker.setDate(end);
});

// 조회 종료일 변경 이벤트
// 조회 종료일 기준 12개월 전으로 범위 설정
endMonthPicker.on('change', function () {
    startMonthPicker.setDate(getAddMonth(endMonthPicker.getDate(), -11));
});
</script>
<!--chart.js-->
<script>

    //전월대비, 전년 동월대비 
    function getMaxValue(){
        var dataList = ['${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}', '${rtnSummaryLast.last_year_same_month_rtn_num_rto}'];
        return Math.abs(dataList[0]) > Math.abs(dataList[1]) ? Math.abs(dataList[0]) : Math.abs(dataList[1]);
    }
    //전월대비, 전년 동월대비 
    var _option_max = getMaxValue();

    am4core.useTheme(am4themes_animated);
    am4core.options.commercialLicense = true;
    var chart = am4core.create("re_compare_grid_1", am4charts.PieChart);
    var chart4 = am4core.create("re_compare_grid_2", am4charts.PieChart);
    //chart
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    var pieSeries4 = chart4.series.push(new am4charts.PieSeries());
    //1
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "mall";
    chart.innerRadius = am4core.percent(50);
    pieSeries.alignLabels = true;
    pieSeries.fontSize = 10;
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "mall";
    //4
    pieSeries4.dataFields.value = "litres";
    pieSeries4.dataFields.category = "type";
    chart4.innerRadius = am4core.percent(50);
    pieSeries4.alignLabels = true;
    pieSeries4.fontSize = 10;

    var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
    var shadow4 = pieSeries4.slices.template.filters.push(new am4core.DropShadowFilter);
    shadow.opacity = 0;
    shadow4.opacity = 0;
    var hoverState = pieSeries.slices.template.states.getKey("hover");
    var hoverState4 = pieSeries4.slices.template.states.getKey("hover");
    var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
    var hoverShadow4 = hoverState4.filters.push(new am4core.DropShadowFilter);
    hoverShadow.opacity = 0.7;
    hoverShadow.blur = 5;
    hoverShadow4.opacity = 0.7;
    hoverShadow4.blur = 5;

    var mallGraphList = JSON.parse('${mallGraphList}');
    var marketGraphList = JSON.parse('${marketGraphList}');
    chart.data = getMallGraphData(mallGraphList);
    chart4.data = getMarketGraphData(marketGraphList);

var ctx5 = document.getElementById('spGh1').getContext('2d');
var ctx6 = document.getElementById('spGh2').getContext('2d');
var ctx7 = document.getElementById('spGh1_m').getContext('2d');
var ctx8 = document.getElementById('spGh2_m').getContext('2d');
var ctx9 = document.getElementById('spGh1_m_1').getContext('2d');
var ctx10 = document.getElementById('spGh2_m_2').getContext('2d');
var option2 = {
    layout: {
    padding: {
        top: 10
    }
    },
    legend: {
        display: false
    },
    title: {
        display: false
    },
    scales: {
    xAxes: [{
        //barPercentage: 0.8,
        gridLines: {
        display: false,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 1000,
        color: "rgba(255,99,132,0)"
        }
    }],
    yAxes: [{
        ticks: {
        max: _option_max,
        min: (_option_max * -1),
        display: false,
        suggestedMin: 0,
        },
        gridLines: {
        display: true,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 0,
        color: "rgba(255,99,132,0)"
        },
    }]
    },
    tooltips: {
        enabled: false,
    }
}
var option_eidt_1 = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
    padding: {
        top: 10
    }
    },
    legend: {
        display: false
    },
    title: {
        display: false
    },
    scales: {
    xAxes: [{
        //barPercentage: 0.8,
        gridLines: {
        display: false,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 1000,
        color: "rgba(255,99,132,0)"
        }
    }],
    yAxes: [{
        ticks: {
        max: _option_max,
        min: (_option_max * -1),
        display: false,
        suggestedMin: 0,
        },
        gridLines: {
        display: true,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 0,
        color: "rgba(255,99,132,0)"
        },
    }]
    },
    tooltips: {
        enabled: false,
    }
}
var maxRtn = '${rtnSummaryLast.last_year_rtn_rto_num_max}';
var option_eidt_2 = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
    padding: {
        top: 10
    }
    },
    legend: {
        display: false
    },
    title: {
        display: false
    },
    scales: {
    xAxes: [{
        //barPercentage: 0.8,
        gridLines: {
        display: false,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 1000,
        color: "rgba(255,99,132,0)"
        }
    }],
    yAxes: [{
        ticks: {
        display: false,
        suggestedMin: 0,
        max: Math.ceil(maxRtn * 1.1)
        },
        gridLines: {
        display: true,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 0,
        color: "rgba(255,99,132,0)"
        },
    }]
    },
    tooltips: {
        enabled: false,
    }
}

var color = [];

if( '${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}' > 0){ color.push("#f4715b"); } 
else { color.push("#75ced2"); }

if( '${rtnSummaryLast.last_year_same_month_rtn_num_rto}' > 0){ color.push("#f4715b"); } 
else { color.push("#75ced2"); }

var myChart = new Chart(ctx5, {
    type: 'bar',
    data: {
    labels: ['전월 대비', '전년 동월대비'],
    datasets: [{
        data: ['${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}', '${rtnSummaryLast.last_year_same_month_rtn_num_rto}'],
        backgroundColor: color,
        hoverBackgroundColor: color,
        borderColor: color,
        hoverBorderColor: color
    }]
    },
    options: option2
});
var myChart = new Chart(ctx6, {
    type: 'bar',
    data: {
    labels: [convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_max_month}', '-', '월', true), convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_min_month}', '-', '월', true), '월평균'],
    datasets: [{
        data: ['${rtnSummaryLast.last_year_rtn_rto_num_max}', '${rtnSummaryLast.last_year_rtn_rto_num_min}', '${rtnSummaryLast.last_year_rtn_rto_num_avg}'],
        backgroundColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        hoverBackgroundColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        borderColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        hoverBorderColor: ["#f4715b", "#75ced2", "#d6a7d1"]
    }]
    },
    options: {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
    padding: {
        top: 10
    }
    },
    legend: {
        display: false
    },
    title: {
        display: false
    },
    scales: {
    xAxes: [{
        //barPercentage: 0.8,
        gridLines: {
        display: false,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 1000,
        color:"rgba(255,99,132,0)"
        }
    }],
    yAxes: [{
        ticks: {
        display: false,
        suggestedMin: 0,
        max: Math.ceil(maxRtn * 1.1)
        },
        gridLines: {
        display: true,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 0,
        color:"rgba(255,99,132,0)"
        },
    }]
    },
    tooltips: {
        enabled: false,
    }
}
});
var myChart = new Chart(ctx7, {
    type: 'bar',
    data: {
    labels: ['전월 대비', '전년 동월대비'],
    datasets: [{
        data: ['${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}', '${rtnSummaryLast.last_year_same_month_rtn_num_rto}'],
        backgroundColor: color,
        hoverBackgroundColor: color,
        borderColor: color,
        hoverBorderColor: color
    }]
    },
    options: option_eidt_1
});
var myChart = new Chart(ctx8, {
    type: 'bar',
    data: {
    labels: [convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_max_month}', '-', '월', true), convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_min_month}', '-', '월', true), '월평균'],
    datasets: [{
        data: ['${rtnSummaryLast.last_year_rtn_rto_num_max}', '${rtnSummaryLast.last_year_rtn_rto_num_min}', '${rtnSummaryLast.last_year_rtn_rto_num_avg}'],
        backgroundColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        hoverBackgroundColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        borderColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        hoverBorderColor: ["#f4715b", "#75ced2", "#d6a7d1"]
    }]
    },
    options: option_eidt_2
});
var myChart = new Chart(ctx9, {
    type: 'bar',
    data: {
    labels: ['전월 대비', '전년 동월대비'],
    datasets: [{
        data: ['${rtnSummaryLast.last_month_per_before_last_month_rtn_num_rto}', '${rtnSummaryLast.last_year_same_month_rtn_num_rto}'],
        backgroundColor: color,
        hoverBackgroundColor: color,
        borderColor: color,
        hoverBorderColor: color
    }]
    },
    options: option_eidt_1
});
var myChart = new Chart(ctx10, {
    type: 'bar',
    data: {
    labels: [convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_max_month}', '-', '월', true), convertDate_YYYYMM('${rtnSummaryLast.last_year_rtn_num_min_month}', '-', '월', true), '월평균'],
    datasets: [{
        data: ['${rtnSummaryLast.last_year_rtn_rto_num_max}', '${rtnSummaryLast.last_year_rtn_rto_num_min}', '${rtnSummaryLast.last_year_rtn_rto_num_avg}'],
        backgroundColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        hoverBackgroundColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        borderColor: ["#f4715b", "#75ced2", "#d6a7d1"],
        hoverBorderColor: ["#f4715b", "#75ced2", "#d6a7d1"]
    }]
    },
    options: option_eidt_2
});
//custom tooltip
Chart.plugins.register({
    afterDatasetsDraw: function(chartInstance, easing) {
    // To only draw at the end of animation, check for easing === 1
    var ctx = chartInstance.chart.ctx;
    chartInstance.data.datasets.forEach(function(dataset, i) {
        var meta = chartInstance.getDatasetMeta(i);
        var chartID = chartInstance.chart.canvas.id;
        if (!meta.hidden) {
        if (chartID.indexOf("spGh2") > -1) {
            meta.data.forEach(function(element, index) {
            // Draw the text in black, with the specified font
            ctx.fillStyle = '#212121';
            var fontSize = 12;
            var fontStyle = 'normal';
            var fontFamily = 'NanumSquare';
            var backgroundColor = 'rgba(79,105,232,0.2)';
            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
            // Just naively convert to string for now
            var dataString = dataset.data[index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            // Make sure alignment settings are correct
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var padding = 5;
            var position = element.tooltipPosition();
            ctx.fillText(dataString + "%", position.x, position.y - (fontSize / 2) - padding);
            });
        } else if (chartID.indexOf("spGh1") > -1) {
            meta.data.forEach(function(element, index) {
            // Draw the text in black, with the specified font
            ctx.fillStyle = '#000';
            var fontSize = 12;
            var fontStyle = 'normal';
            var fontFamily = 'NanumSquare';
            var backgroundColor = 'rgba(79,105,232,0.2)';
            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
            // Just naively convert to string for now
            var dataNum = dataset.data[index];
            var dataString = dataset.data[index].toString();
            // Make sure alignment settings are correct
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var padding = 5;
            var position = element.tooltipPosition();
            if (dataNum > 0) {
                if (matchMedia("screen and (max-width: 5000px) and (min-width:751px)").matches) {
                    ctx.fillText("▲" + dataString + "%", position.x, position.y + (fontSize / 2) + padding);
                }
                if (matchMedia("screen and (max-width: 750px)").matches) {
                    ctx.fillText(dataString + "%", position.x, position.y + (fontSize / 2) + padding);
                }
            } else {
                // dataNum = dataNum.replace(/[^0-9]/g, '');
                dataNum = Math.abs(dataNum);
                if (matchMedia("screen and (max-width: 5000px) and (min-width:751px)").matches) {
                    ctx.fillText("▼" + dataNum + "%", position.x, position.y - (fontSize / 2) - padding);
                }
                if (matchMedia("screen and (max-width: 750px)").matches) {
                    // dataNum = dataNum.replace(/[^0-9]/g, '');
                    dataNum = Math.abs(dataNum);
                    ctx.fillText(dataNum + "%", position.x, position.y - (fontSize / 2) - padding);
                }
            }
            });
        } else if (chartID.indexOf("doughnut") > -1) {
            meta.data.forEach(function(element, index) {
            // Draw the text in black, with the specified font
            ctx.fillStyle = 'black';
            var fontSize = 16;
            var fontStyle = 'normal';
            var fontFamily = 'NanumSquare';
            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
            // Just naively convert to string for now
            var dataString = dataset.data[index].toString();
            var labelString = dataset.labels[index].toString();
            // Make sure alignment settings are correct
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var padding = 5;
            var position = element.tooltipPosition();
            ctx.fillText(labelString, position.x, position.y - (fontSize));
            ctx.fillText(dataString + '%', position.x, position.y + (fontSize / 2));
            });
        }
        }
    });
    }
});
</script>