<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="ko" class="js">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>셀러봇캐시 간편가입</title>
	<link rel="stylesheet" type="text/css" href="/assets/lib/css/jquery-confirm.min.css" />
	<link rel="stylesheet" type="text/css" href="/assets/lib/css/contents.css" />
	<link rel="stylesheet" href="/assets/lib/css/normalize.css">
	<link rel="stylesheet" href="/assets/lib/css/headhesive.css">

	<script type="text/javascript" src="/assets/lib/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="/assets/lib/js/jquery-confirm.min.js"></script>
	<script type="text/javascript" src="/assets/lib/js/ui.js"></script>
	<script type="text/javascript" src="/assets/lib/dist/headhesive.js"></script>
	<script type="text/javascript" src="/assets/js/common/validation.js?ver=20230830_01"></script>
</head>

<body id="top">
	<div class="wrap">

		<header class="banner">
			<nav class="container">
				<h1 class="logo"><a href="/">SELLER BOT</a></h1>
			</nav>
		</header>
		<section class="main_visual">
			<!-- header -->
			<dl class="fC">
				<dt>셀러봇캐시 간편가입</dt>
				<dd>인터파크 정산예정금 통합관리서비스 회원님의 회원정보를 이전하여 셀러봇캐시<br>간편가입을 할 수 있어요.</dd>
			</dl>
		</section>
		<!-- //header -->

		<a id="showHere"></a>

		<!-- contents -->
		<article class="container">

			<div>
				<a href="#startHere"><img src="/assets/images/simple_join/ad_money_inner.jpg"></a>
			</div>			

			<!-- 컨텐츠 -->
			<div class="contents" id="startHere">

				<div class="join_wrap" style="max-height: 250px">
					<img src="/assets/images/simple_join/interpark.png">
				</div>
				<div class="join_wrap">

					<h2 class="txt40">셀러봇캐시 간편가입</h2>
					<p class="font_normal txt_18 B50">인터파크 정산예정금 통합관리서비스 회원님의 회원정보를 이전하여 셀러봇캐시 간편가입을 할 수 있어요.</p>


					<h2>간편가입을 위한 정보제공 동의</h2>
					<p class="font_normal txt_18"><span class="txt_blue">셀러봇캐시</span> 간편가입을 신청하신 회원님의 간편한 서비스 회원가입 및 이용을
						위하여,<br>
						아래의 동의 절차를 거쳐 인터파크 판매자님의 회원정보가 <span class="txt_blue">셀러봇캐시</span>로 제공됩니다.</p>

					<div id="step1" class="join_info clearfix B60">
						<dl>
							<dt>1. 제공 목적 </dt>
							<dd>- (주)온리원이 제공하는 <span class="txt_blue">셀러봇캐시</span> 서비스의 간편 회원가입을 위한 회원정보 제공</dd>
							<dt>2. 제공되는 회원정보</dt>
							<dd>- <span class="txt_blue">정산예정금 통합관리 서비스</span> 에 등록된 판매몰 계정정보 (ID/PW 등)</dd>
							<dd>- 회원의 상호명, 사업자번호, 대표자명</dd>
						</dl>
						<p class="txt_4d txt_18 mT5">*제공되는 회원 정보는 엄격하게 보안 처리 관리됩니다. </p>

						<p class="inp_check mT30"><input type="checkbox" id="check_agree" required><label
								for="check_agree">간편 가입을 위한 정보제공 동의</label></p>
						<p id="check_agree_msg" class="comment txt_red">*정보제공 동의를 해주셔야 간편가입이 가능합니다.</p>
					</div>

					<hr class="middle_line">

					<div id="step2" class="rule_check">
						<h2 class="mT60">셀러봇캐시 가입을 위한 약관</h2>
						<div class="B40 mB60">
							<ul>
								<c:forEach items="${terms }" var="term">
									<li class="inp_check"><input type="checkbox" id="${term.terms_id }"
											data-terms_id="${term.terms_id }" <c:if
											test="${term.terms_esse_yn eq 'Y' }">required</c:if>>
										<label for="${term.terms_id }">${term.terms_title }(${term.terms_esse_yn == 'Y'
											? '필수':'선택' })</label>
										<sapn class="link_agree">자세히보기</sapn>
										<div name="temp_terms_cont" style="display: none;">${term.terms_cont }</div>
									</li>
								</c:forEach>

							</ul>
							<p class="txt_4d txt_18 mT5">*약관 제목을 클릭하시면 자세한 내용을 확인하실 수 있습니다.</p>
							<div class="tC"><button id="btn_agree" class="btn Mbtn mT60">약관 전체 동의</button></div>
							<p id="terms_msg" class="comment txt_red">*필수 약관에 동의해주셔야 서비스 이전 가입이 가능합니다.</p>
						</div>

						<hr class="middle_line">

					</div>

					<div id="step3" class="join_input">
						<h2 class="mT60">회원정보 입력</h2>
						<ul class="mB60 B30">
							<li class="inp_default">
								<label for="custId">아이디 (이메일)</label>
								<input type="text" id="custId" class="w75" maxlength="50"
									placeholder="수신 가능한 이메일 주소를 입력해주세요." required><button id="btn_sendMail"
									class="btn btn_blue">중복확인</button>
								<p class="comment txt_blue">*사용 가능한 이메일입니다.</p>
								<p class="comment txt_red">*사용 불가능한 이메일입니다.</p>
							</li>
							<li class="inp_default">
								<label for="authNo">이메일 인증번호 입력</label>
								<input type="text" id="authNo" class="w75" placeholder="인증번호를 입력해 주세요."
									autocomplete="off" required><button id="btn_authNo" maxlength="10"
									class="btn btn_blue">인증번호확인</button>
								<p class="comment txt_red">*인증번호가 올바르지 않습니다. (<a href="#">인증번호 재발송</a>)</p>
								<p class="comment txt_blue">*인증이 완료되었습니다.</p>
							</li>
							<li class="inp_default">
								<label for="passwd">비밀번호</label>
								<input type="password" id="passwd" placeholder="영문, 숫자, 특수문자 포함 8~20자 이내로 입력해주세요."
									style="font-family: auto;" autocomplete="new-password" class="w99" maxlength="20"
									required>
								<p class="comment txt_red">*비밀번호 형식이 올바르지 않습니다.</p>
							</li>
							<li class="inp_default">
								<label for="passwd_same">비밀번호 확인</label>
								<input type="password" id="passwd_same" autocomplete="new-password"
									style="font-family: auto;" class="w99" required>
								<p class="comment txt_red">*비밀번호가 일치하지 않습니다.</p>
							</li>
							<li class="inp_default">
								<label for="biz_nm">상호명</label>
								<input type="text" id="biz_nm" class="w99 inp_disabled" value="${userInfo.COMPANY }"
									disabled>
							</li>
							<li class="inp_default">
								<label for="biz_no">사업자등록번호</label>
								<input type="text" id="biz_no" class="w99 inp_disabled" value="${userInfo.BIZNO }"
									disabled>
							</li>
							<li class="inp_default">
								<label for="ceo_nm">대표자명</label>
								<input type="text" id="ceo_nm" class="w99 inp_disabled" value="${userInfo.CEO }"
									maxlength="15" disabled>
							</li>
							<li class="inp_default">
								<label for="ceo_no">대표자 연락처</label>
								<input type="text" id="ceo_no" placeholder="숫자만 입력하여 주세요." class="w99" maxlength="11"
									required>
								<p class="comment txt_red">*숫자만 입력해주세요.</p>
							</li>
							<li class="inp_default">
								<label for="chrg_nm">담당자명</label>
								<input type="text" id="chrg_nm" placeholder="(선택)담당자명을 입력하여 주세요" maxlength="15"
									class="w99">
							</li>
							<li class="inp_default">
								<label for="chrg_no">담당자 연락처</label>
								<input type="text" id="chrg_no" placeholder="(선택)숫자만 입력하여 주세요." class="w99"
									maxlength="11">
								<p class="comment txt_red">*숫자만 입력해주세요.</p>
							</li>
							<!--
							<li class="inp_default">
								<label for="ipk_partner_no">인터파크 업체번호</label>
								<input type="text" id="ipk_partner_no" placeholder="숫자만 입력하여 주세요." class="w99"
									maxlength="30" required>
								<p class="comment txt_red">*숫자만 입력해주세요.</p>
							</li>
							<li class="inp_default">
								<label for="ipk_supply_seq">인터파크 공급계약 일련번호</label>
								<input type="text" id="ipk_supply_seq" placeholder="숫자만 입력하여 주세요." class="w99"
									maxlength="30" required>
								<p class="comment txt_red">*숫자만 입력해주세요.</p>
							</li>
							-->
						</ul>
						<input type="hidden" id="token" value="${token }">
						<input type="hidden" id="inf_path_cd" value="${inf_path_cd }">
						<hr class="middle_line">

					</div>
				</div>


				<!-- 사용자 정보 리스트 테이블 -->

				<div id="step4" class="tbl_list_02 B30">
					<h2 class="mT60 T30 B20">판매몰 계정 정보</h2>
					<table>
						<caption>사용자 정보 리스트 테이블</caption>
						<colgroup>
							<col style="width:18%" />
							<col style="width:*" />
							<col style="width:15%" />
							<col style="width:*" />
							<col style="width:18%" />
							<col style="width:*" />
						</colgroup>
						<tbody>
							<c:forEach items="${userMallList }" var="mall">
								<tr>
									<th>${mall.mall_nm }</th>
									<td>${mall.mall_cert_1st_id }</td>
									<th>${mall.sub_mall_cert_1st_nm }</th>
									<td>${mall.sub_mall_cert_1st }</td>
									<th>${mall.sub_mall_cert_2nd_nm }</th>
									<td>${mall.sub_mall_cert_2nd }</td>
								</tr>
							</c:forEach>
							<c:if test="${empty userMallList}">
								<tr>
									<td colspan="6" class="tC">판매몰 계정 정보가 없습니다.</td>
								</tr>
							</c:if>
						</tbody>
					</table>
					<p class="txt_4d txt_18 mT5">*셀러봇캐시에 이전되는 판매몰 정보입니다. 셀러봇캐시의 판매몰 관리에서 변경/추가 가능합니다.</p>
					<div class="tC mB40"><button id="btn_join" class="btn Mbtn mT60 mB60">간편가입 완료</button></div>

				</div>
				<!-- //사용자 정보 리스트 테이블 -->

			</div>
			<!-- //컨텐츠 -->

			<div id="modalTemple" style="display: none;">

			</div>


			<!-- contents -->

			<hr>
			<!-- footer -->
			<footer>
				<div class="footer">
					<div><span>(주) 온리원</span><span>대표이사 최호식</span><span>사업자 등록번호 220-88-21645</span><span>특허번호
							10-1089183</span></div>
					<div><span>사업자정보 소프트웨어개발</span><span>소재지 경기도 하남시 미사대로 520 현대지식산업센터 한강미사2차 D동 5층 545호</span></div>
					<div class="footer_logo">SELLER BOT</div>
				</div>
			</footer>
			<!-- //footer -->

	</div>


	<script>
		'use strict';

		$(document).ready(function () {

			function showAlert(btnOk, content, callback) {

				var buttonOption = {};
				buttonOption[btnOk] = {
					btnClass: 'btn btn_blue w100',
					action: function () {
						if (typeof callback == "function") {
							callback();
						}
					}
				};

				$.alert({
					theme: 'mms_box_wrap',
					buttons: buttonOption,
					boxWidth: '500px',
					useBootstrap: false,
					title: '',
					content: '<div class="mss_box">' +
						'<div class="mms_con fC" style="height:134px;">' +
						'<div class="T40 mT20 txt_23">' + content + '</div>' +
						'</div>' +
						'</div>',
				});

			}

			// 안내 메시지
			function showMessage(targetId, msg, color) {
				var group = removeMessage(targetId);
				group.append("<p class=\"comment show " + color + "\">*" + msg + "</p>");
			}
			function removeMessage(targetId) {
				var group = $("#" + targetId).parent();
				group.find(".comment").remove();
				return group;
			}

			function disabled(id, isDisabled) {

				$('#' + id).attr('disabled', isDisabled);
				if (isDisabled) {
					$('#' + id).addClass("inp_disabled");
				} else {
					$('#' + id).removeClass("inp_disabled");
				}

			}

			function fnNext(step) {
				var target = $("#" + step);
				target.find("input").eq(0).focus();
			}

			// Set options
			var options = {
				offset: '#showHere',
				offsetSide: 'top',
				classes: {
					clone: 'banner--clone',
					stick: 'banner--stick',
					unstick: 'banner--unstick'
				}
			};

			var banner = new Headhesive('.banner', options);

			// 간편 가입 정보제공
			$("#check_agree").on("change", function () {
				if ($(this).is(":checked")) {
					fnNext("step2");
				}
			});

			// 약관 자세히 보기
			$(".link_agree").on("click", function () {

				var jqThis = $(this);

				var html = [];
				html.push("<div class=\"modal_popup\">");
				html.push('	<div class="modal_con t_scroll line_dd" style="height:350px;">');
				html.push('		<div class="view">');
				html.push('		' + jqThis.next().html());
				html.push('		</div>');
				html.push('	</div>');
				html.push('</div>');


				var result = $.confirm({
					theme: 'modal_box_wrap',
					buttons: {
						"약관확인": {
							btnClass: 'btn btn_blue w100',
							action: function () {
								jqThis.prev().prev().prop("checked", true);
							}
						}
					},
					closeIcon: function () {
						return 'aRandomButton';
					},
					boxWidth: '600px',
					useBootstrap: false,
					title: jqThis.prev().text(),
					content: html.join("")
				});
			});

			// 약관 전체 동의
			$("#btn_agree").on("click", function () {
				$("#step2").find("[type=checkbox]").each(function () {
					$(this).prop("checked", true);
				});
				fnNext("step3");
			});

			$("#btn_sendMail").on("click", function () {

				if (!isValid("custId")) { return false; }

				var custId = $("#custId").val();

				$.post("/pub/api/member/join/confirmCust", { custId: custId }, function (data, status) {
					if ($("#custId").is(":disabled")) {
						showMessage("custId", "재발송 되었습니다.", "txt_blue");
					} else {
						showMessage("custId", "사용가능한 이메일입니다(인증번호 발송완료).", "txt_blue");
						disabled("custId", true);
						disabled("btn_sendMail", true);
					}

					$("#authNo").focus();


				}).fail(function (response) {
					if (response.status == 409) {
						showMessage("custId", custId + " 이메일은 <br>이미 등록되어 있습니다.<br> 아이디 찾기를 진행 하세요.", "txt_red");
					} else if (response.status == 4490) { // 이미 ID에 인증번호가 발송된 상태
						showMessage("custId", "인증 번호가 발송된 상태 입니다.", "txt_red");
						showAlert("확인", "인증 번호가 발송된 상태 입니다.");
						$("#authNo").focus();
					} else if (response.status == 410) {
						showAlert("확인", "세션이 종료되었습니다.", function () {
							location.reload();
						});

					} else {
						showAlert("확인", "[시스템 장애]관리자에게 문의하세요.");
					}
				});
			});

			// 인증번호 확인
			$("#btn_authNo").on("click", function () {
				if (!$("#custId").is(":disabled")) {
					showMessage("custId", "인증번호를 발송하여 주시기 바랍니다..", "txt_red");
					$("#custId").focus();
					return false;
				}

				if (!isValid("authNo")) { return false; }

				var authNo = $("#authNo").val();

				$.post("/pub/api/member/join/confirmAuthNo", { authNo: authNo }, function (data, status) {
					showMessage("authNo", "인증이 완료되었습니다.", "txt_blue");
					disabled("authNo", true);
					disabled("btn_authNo", true);
					$("#passwd").focus();
				})
					.fail(function (response) {

						if (response.status == 410) { // 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
							location.reload();
						} else if (response.status == 400) { // 인증번호 오류

							showAlert("확인", "인증 번호를 다시 확인 하여주세요.", function () {
								showMessage("authNo", "인증번호가 올바르지 않습니다. (<a href=\"javascript:$('#btn_sendMail').click();\">인증번호 재발송</a>)", "txt_red");
								$("#authNo").focus();
							});
						} else if (response.status == 4490) { // 인증 번호 발송을 요청 하지 않은 상태
							showAlert("확인", "인증번호 발송을 먼저 하세요.", function () {
								$("#custId").focus();
							});
						} else {
							showAlert("Error: " + response.status);
						}
					});
			});

			$("#passwd").on("keyup", function () {
				if (isPassword($(this).val())) {
					showMessage("passwd", "사용가능한 비밀번호입니다.", "txt_blue");
				} else {
					showMessage("passwd", "영문, 숫자, 특수문자 포함 8~20자 이내로 입력해주세요.", "txt_red");
				}
			});

			$("#passwd_same").on("keyup", function () {
				if (!isValid("passwd")) {
					$("#passwd").focus();
					return false;
				}
				if ($("#passwd").val() == $("#passwd_same").val()) {
					showMessage("passwd_same", "비밀번호가 일치합니다.", "txt_blue");
				} else {
					showMessage("passwd_same", "비밀번호가 일치하지 않습니다.", "txt_red");
				}
			});

			$("#ceo_no, #chrg_no, #ipk_partner_no, #ipk_supply_seq").on("keyup", function () {
				if (nonNull($(this).val())) {
					if (isNumber($(this).val())) {
						removeMessage($(this).attr("id"));
					} else {
						showMessage($(this).attr("id"), "숫자만 입력가능합니다.", "txt_red");
					}
				} else {
					removeMessage($(this).attr("id"));
				}
			});

			// 회원가입
			$("#btn_join").on("click", function () {

				if (!isValid()) {
					return false;
				}

				if (!$("#custId").is(":disabled")) {
					showMessage("custId", "이메일 중복을 확인하여 주세요.", "txt_red");
					$("#custId").focus();
					return false;
				}

				if (!$("#authNo").is(":disabled")) {
					showMessage("authNo", "인증번호를 확인하여 주세요.", "txt_red");
					$("#authNo").focus();
					return false;
				}

				var url = "/pub/api/simple_Join";
				var json = {
					"cust_id": $("#custId").val(),
					"biz_no": $("#biz_no").val(),
					"biz_nm": $("#biz_nm").val(),
					"ceo_nm": $("#ceo_nm").val(),
					"ceo_no": $("#ceo_no").val(),
					"chrg_nm": $("#chrg_nm").val(),
					"chrg_no": $("#chrg_no").val(),
					"ipk_partner_no": $("#ipk_partner_no").val(),
					"ipk_supply_seq": $("#ipk_supply_seq").val(),
					"inf_path_cd": $("#inf_path_cd").val(),
					"passwd": $("#passwd").val(),
					"passwd_same": $("#passwd_same").val(),
					"token": $("#token").val(),
					"terms": [],
				};

				$("#step2").find("[type=checkbox]:checked").each(function () {
					json.terms.push($(this).attr("id"));
				});


				$.ajax({
					type: 'POST',
					url: url,
					data: JSON.stringify(json),
					contentType: "application/json",
					dataType: 'text',
					success: function (res) {
						showAlert("셀러봇캐시 로그인", "환영합니다!<br>셀러봇캐시 간편가입이 완료되었습니다.", function () {
							location.href = "/login";
						});
					}, error: function (res, sts, text) {
						showAlert("회원가입 실패", "관리자에게 문의하여 주세요");
					}

				});

			});


			function isValid(targetId) {

				var result = true;
				var objFocus = null;

				if (typeof targetId == "undefined") {
					$("input[required]").each(function (item) {
						var jqThis = $(this);
						if (!valid(jqThis)) {
							result = false;
						}
						if (!result && isNull(objFocus)) {
							objFocus = jqThis;
						}
					});
				} else {
					result = valid($("#" + targetId));
				}

				if (nonNull(objFocus)) {
					objFocus.focus();
				}

				return result;
			}

			function valid(jqObj) {
				var result = true;
				var jqThis = jqObj;
				var id = jqThis.attr("id");
				var val = jqThis.val();

				if (jqThis.attr("type") == "checkbox") {
					if (!jqThis.is(":checked")) {
						if (id == "check_agree") {
							$("#check_agree_msg").show();
							result = false;
						} else {
							$("#terms_msg").show();
							result = false;
						}
					}
				} else {
					if (val == "") {
						showMessage(id, $("label[for=" + id + "]").text() + "항목은 필수 입력입니다.", "txt_red");
						result = false;
					} else {
						if (id == "custId") {
							// 이메일 형식 체크
							if (!isEmail(val)) {
								showMessage(id, "이메일 형식으로 입력해주세요", "txt_red");
								result = false;
							}
						} else if (id == "ceo_no") {
							if (!isMobile(val)) {
								showMessage(id, "휴대폰번호형식이 아닙니다.", "txt_red");
								result = false;
							}
						}


					}
				}
				return result;
			}




		});


	</script>

	<style>
		.comment.txt_red {
			display: none;
		}

		.comment.txt_blue {
			display: none;
		}

		.comment.txt_red.show {
			display: block;
		}

		.comment.txt_blue.show {
			display: block;
		}

		sapn.link_agree {
			margin-left: 0.4rem;
			cursor: pointer;
			font-size: 0.7rem;
			text-decoration: underline;
			color: #00738c;
			vertical-align: middle;
		}

		td {
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
		}
	</style>

</body>

</html>