<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="/assets/css/ticket.css?ver=20221013_01">
<link rel="stylesheet" href="/assets/css/popup.css?ver=20230106_01">
<script src="/assets/js/paymentForInicis.js?ver=20211116_01"></script>

<input type="hidden" name="goodsTypCd" id="goodsTypCd" value="${goodsTypCd}">
<input type="hidden" name="freeTrialYn" id="freeTrialYn" value="${freeTrialYn}">
<input type="hidden" name="forced" id="forced" value="${forced}">
<input type="hidden" id="DeviceType" value="${DeviceType}">
<input type="hidden" name="basic_goods_req_seq_no" id="basic_goods_req_seq_no" value="${basic_goods_req_seq_no}">
<input type="hidden" name="addn_goods_req_seq_no" id="addn_goods_req_seq_no" value="${addn_goods_req_seq_no}">
<input type="hidden" name="addn_cancle_product_yn" id="addn_cancle_product_yn" value="${addn_cancle_product_yn}">
<input type="hidden" name="addn_goods_nm" id="addn_goods_nm" value="${addn_goods_nm}">
<input type="hidden" name="cafe24_usr_yn" id="cafe24_usr_yn" value="${cafe24_usr_yn}">

<div class="container">
    <div class="container_wrapper">
        <div class="my_wra product_guide">
            <!-- menu_top start -->
            <div class="menu_top">
                <p class="menu_name">이용요금 안내</p>
                <div class="gnb_topBox">
                    <ul class="gnb_top clearfix">
                        <li class="focus"><a href="/sub/payment/product">이용권 소개</a></li>
                        <li><a href="#" onclick="pressedBtnNextForPayment();">결제하기</a></li>
                    </ul>
                </div>
            </div>
            <!-- menu_top end -->
            <!-- section1 start -->
            <div id="section1">
                <!-- 20211122 기존 문구 삭제 -->
                <!-- <div id="sec1_title_div">
                    <h1>월 만원으로 고용하는 우리 회사 <span>'정산예정금 비서'</span></h1>
                    <h2>셀러봇 캐시 100% 활용하기</h2>
                </div> -->
                <div class="swiper-container_ticket">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" id="first_slider"></div>
                        <div class="swiper-slide" id="first_slider2"></div>
                    </div>
                </div>
                <div class="swiper_1_button_wrapper">
                    <div class="swiper-button-prev prev_top button_edit1"></div>
                    <div class="swiper-button-next next_top button_edit2"></div>
                </div>
            </div>
            <!-- section1 end -->
            <!-- section2_pc start -->
            <div id="section2_pc"> 
                <div class="vid_btn_container">
                    <div class="vid_btn">서비스 소개 영상</div>
                </div>

                <!-- //20200706_10 :: 이용권 안내 Start -->
                <div class="ticket_select_button_container">
                    <h1 class="forced_notice">
                        <span id="forced_notice" style="color: red;"></span>
                    </h1>
                    <h1 id="select_ticket">
                        <div class="ticket_name"></div>
                        <span class="selected_msg">이용권을 선택해주세요.</span>
                    </h1>
                    <div id="row">
                        <button type="button" id="next_btn_top" onclick="pressedBtnNextForPayment();">결제수단 등록</button>
                    </div>
                </div>

                <div class="table_container">
                    <!-- 상품 선택 버튼 Start -->
                    <c:choose>
                        <c:when test="${cafe24_usr_yn eq 'Y'}">
                            <!-- 이용 중인 서비스가 있는 경우 -->
                            <c:choose>
                                <c:when test="${basic_goods_req_seq_no > 0 && addn_goods_req_seq_no > 0}">
                                    <c:if test="${goodsTypCd eq 'RNB'}">
                                        <label class="bot_select_label ronibot_selector">
                                            <input type="checkbox" name="bot_selector" value="로니봇" class="ronibot" />
                                        </label>
                                    </c:if>
                                    <c:if test="${goodsTypCd eq 'PYB'}">
                                        <label class="bot_select_label paibot_selector">
                                            <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                        </label>
                                    </c:if>
                                </c:when>
                                <c:when test="${basic_goods_req_seq_no > 0}">
                                    <c:if test="${empty goodsTypCd}">
                                        <label class="bot_select_label freebot_selector">
                                            <input type="checkbox" name="bot_selector" value="프리봇" class="freebot" />
                                        </label>
                                    </c:if>
                                    <c:if test="${goodsTypCd eq 'RNB'}">
                                        <label class="bot_select_label ronibot_selector">
                                            <input type="checkbox" name="bot_selector" value="로니봇" class="ronibot" />
                                        </label>
                                    </c:if>
                                    <c:if test="${goodsTypCd eq 'PYB'}">
                                        <label class="bot_select_label paibot_selector">
                                            <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                        </label>
                                    </c:if>
                                </c:when>
                                <c:when test="${addn_goods_req_seq_no > 0}">
                                    <c:if test="${forced eq 'Y'}">
                                        <label class="bot_select_label freebot_selector">
                                            <input type="checkbox" name="bot_selector" value="프리봇" class="freebot" />
                                        </label>
                                        <label class="bot_select_label paibot_selector">
                                            <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                        </label>
                                    </c:if>
                                    <c:if test="${forced eq 'N'}">
                                        <label class="bot_select_label freebot_selector">
                                            <input type="checkbox" name="bot_selector" value="프리봇" class="freebot" />
                                        </label>
                                        <label class="bot_select_label ronibot_selector">
                                            <input type="checkbox" name="bot_selector" value="로니봇" class="ronibot" />
                                        </label>
                                        <label class="bot_select_label paibot_selector">
                                            <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                        </label>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    <c:if test="${forced eq 'Y'}">
                                        <label class="bot_select_label freebot_selector">
                                            <input type="checkbox" name="bot_selector" value="프리봇" class="freebot" />
                                        </label>
                                        <c:if test="${goodsTypCd eq 'RNB'}">
                                            <label class="bot_select_label ronibot_selector">
                                                <input type="checkbox" name="bot_selector" value="로니봇" class="ronibot" />
                                            </label>
                                        </c:if>    
                                        <c:if test="${goodsTypCd eq 'PYB'}">
                                            <label class="bot_select_label paibot_selector">
                                                <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                            </label>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${forced eq 'N'}">
                                        <label class="bot_select_label freebot_selector">
                                            <input type="checkbox" name="bot_selector" value="프리봇" class="freebot" />
                                        </label>
                                        <label class="bot_select_label ronibot_selector">
                                            <input type="checkbox" name="bot_selector" value="로니봇" class="ronibot" />
                                        </label>
                                        <label class="bot_select_label paibot_selector">
                                            <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                        </label>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                            <!-- TODO 2021.07.26 무료 프로모션으로 임시 주석처리
                                <label class="bot_select_label bankbot_selector">
                                        <input type="checkbox" name="bot_selector" value="뱅크봇" class="bankbot" />
                                </label>
                            -->
                        </c:when>
                        <c:otherwise>
                            <!-- 이용 중인 서비스가 있는 경우 -->
                            <c:if test="${basic_goods_req_seq_no > 0}">
                                <c:if test="${goodsTypCd eq 'RNB'}">
                                    <label class="bot_select_label ronibot_selector_nb">
                                        <input type="checkbox" name="bot_selector" value="로니봇" class="ronibot" />
                                    </label>
                                </c:if>
                                <c:if test="${goodsTypCd eq 'PYB'}">
                                    <label class="bot_select_label paibot_selector_nb">
                                        <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                    </label>
                                </c:if>        
                            </c:if>
                            <!-- 이용 중인 서비스가 없는 경우 -->
                            <c:if test="${empty basic_goods_req_seq_no}">
                                <c:if test="${forced eq 'Y'}">
                                    <c:if test="${goodsTypCd eq 'RNB'}">
                                        <label class="bot_select_label ronibot_selector_nb">
                                            <input type="checkbox" name="bot_selector" value="로니봇" class="ronibot" />
                                        </label>
                                    </c:if>
                                    <c:if test="${goodsTypCd eq 'PYB'}">
                                        <label class="bot_select_label paibot_selector_nb">
                                            <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                        </label>
                                    </c:if>
                                </c:if>
                                <c:if test="${forced eq 'N'}">
                                    <label class="bot_select_label ronibot_selector_nb">
                                        <input type="checkbox" name="bot_selector" value="로니봇" class="ronibot" />
                                    </label>
                                    <label class="bot_select_label paibot_selector_nb">
                                        <input type="checkbox" name="bot_selector" value="파이봇" class="paibot" />
                                    </label>
                                </c:if>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                    <!-- 상품 선택 버튼 End -->
                    <table>
                        <c:set var="RONIBOT" value="" />
                        <thead>
                            <tr id="frist_line">
                                <c:choose>
                                    <c:when test="${cafe24_usr_yn eq 'Y'}">
                                        <th colspan="2" class="b_gray">이용권</th>
                                        <th class="freebot">프리봇</th>
                                        <th class="ronibot">로니봇</th>
                                        <th class="paibot">파이봇</th>
                                        <th class="blank"></th>
                                        <th class="bankbot">뱅크봇</th>
                                    </c:when>
                                    <c:otherwise>
                                        <th colspan="2" class="b_gray" style="width: 320px;">이용권</th>
                                        <th class="freebot" style="width: 240px;">프리봇</th>
                                        <th class="ronibot" style="width: 240px;">로니봇</th>
                                        <th class="paibot" style="width: 240px;">파이봇</th>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2" class="b_gray">요금(VAT별도)</td>
                                <td class="freebot">무료</td>
                                <td class="ronibot">
                                    <c:choose>
                                        <c:when test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                            <h1 class="original_price">
                                                <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.GOODS_BAS_PRC}" pattern="#,###" />원
                                            </h1>
                                            <h2 class="discount_price">
                                                <c:if test="${freeTrialYn eq 'N'}">
                                                    무료체험 이후
                                                </c:if>
                                                <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" />원
                                            </h2>
                                        </c:when>
                                        <c:otherwise>
                                            ${goodsInfo.basic_goods_info.RNB.GOODS_DESC}
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="paibot">
                                    <c:choose>
                                        <c:when test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                            <h1 class="original_price">
                                                <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.GOODS_BAS_PRC}" pattern="#,###" />원
                                            </h1>
                                            <h2 class="discount_price">
                                                <c:if test="${freeTrialYn eq 'N'}">
                                                    무료체험 이후
                                                </c:if>
                                                <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" />원
                                            </h2>
                                        </c:when>
                                        <c:otherwise>
                                            ${goodsInfo.basic_goods_info.PYB.GOODS_DESC}
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <!-- Cafe24 고객만 뱅크봇 이용 가능 -->
                                <c:if test="${cafe24_usr_yn eq 'Y'}">
                                    <td class="blank"></td>
                                    <td class="bankbot">
                                        <!-- TODO 2021.07.26 무료 프로모션으로 임시 주석처리
                                        <h1 class="original_price"><fmt:formatNumber value="${goodsInfo.addn_goods[0].GOODS_BAS_PRC}" pattern="#,###" />원</h1>
                                        <h2 class="discount_price"><fmt:formatNumber value="${goodsInfo.addn_goods[0].FNA_PRC}" pattern="#,###" />원</h2> 
                                        -->
                                        <h2 class="discount_price">free</h2> 
                                        <c:set var="now" value="<%=new java.util.Date()%>" />
                                        <c:set var="today"><fmt:formatDate value="${now}" pattern="yyyyMMdd" /></c:set> 
                                        (프로모션 진행중)
                                        <c:if test="${empty addn_goods_req_seq_no}">
                                            <button style="background: #85b86f; 
                                                            color: #fff; 
                                                            width: 12.5rem; 
                                                            height: 3rem; 
                                                            font-size: 1.125rem; 
                                                            font-weight: bold; 
                                                            border-radius: 3px;" onclick="pressedBtnPromotion('bankbot');">신청하기</button>
                                        </c:if>
                                    </td>
                                </c:if>
                            </tr>
                            <tr>
                                <td colspan="2" class="b_gray">데이터 제공량</td>
                                <td class="freebot">등록만 가능</td>
                                <td class="ronibot">
                                    <div class="data_div">
                                        <span class="label">판매몰ID</span>
                                        <strong>
                                            <c:choose>
                                                <c:when test="${goodsInfo.basic_goods_info.RNB.SALE_MALL_REG_ID_PSB_CNT ne 0}">${goodsInfo.basic_goods_info.RNB.SALE_MALL_REG_ID_PSB_CNT}개</c:when>
                                                <c:otherwise>무제한</c:otherwise>
                                            </c:choose>
                                        </strong>
                                    </div>
                                    <div class="data_div">
                                        <span class="label">계좌</span>
                                        <strong>
                                            <c:choose>
                                                <c:when test="${goodsInfo.basic_goods_info.RNB.SETT_ACC_ACCT_REG_PSB_CNT ne 0}">${goodsInfo.basic_goods_info.RNB.SETT_ACC_ACCT_REG_PSB_CNT}개</c:when>
                                                <c:otherwise>무제한</c:otherwise>
                                            </c:choose>
                                        </strong>
                                    </div>
                                    <div class="data_div">
                                        <strong><br>'스마트스토어는 동일 계정(주계정 + 매니저 계정)에 한해서 API ID가 다를 경우 하나의 계정으로 취급합니다.'</strong>
                                    </div>
                                </td>
                                <td class="paibot">
                                    <div class="data_div">
                                        <span class="label">판매몰ID</span>
                                        <strong>
                                            <c:choose>
                                                <c:when test="${goodsInfo.basic_goods_info.PYB.SALE_MALL_REG_ID_PSB_CNT ne 0}">${goodsInfo.basic_goods_info.PYB.SALE_MALL_REG_ID_PSB_CNT}개</c:when>
                                                <c:otherwise>무제한</c:otherwise>
                                            </c:choose>
                                        </strong>
                                    </div>
                                    <div class="data_div">
                                        <span class="label">계좌</span>
                                        <strong>
                                            <c:choose>
                                                <c:when test="${goodsInfo.basic_goods_info.PYB.SETT_ACC_ACCT_REG_PSB_CNT ne 0}">${goodsInfo.basic_goods_info.PYB.SETT_ACC_ACCT_REG_PSB_CNT}개</c:when>
                                                <c:otherwise>무제한</c:otherwise>
                                            </c:choose>
                                        </strong>
                                    </div>
                                </td>
                                <!-- Cafe24 고객만 뱅크봇 이용 가능 -->
                                <c:if test="${cafe24_usr_yn eq 'Y'}">
                                    <td class="blank" rowspan="11"></td>
                                    <td class="bankbot" rowspan="11">
                                        <!-- TODO 2021.07.26 무료 프로모션으로 임시 주석처리
                                        <div class="data_div event">
                                            <strong>파격 혜택<br>1년 결제 시 가격 인하</strong>
                                            <div class="display_price">
                                                <h1 class="original_price"><fmt:formatNumber value="${goodsInfo.addn_goods[1].GOODS_BAS_PRC}" pattern="#,###" />원</h1>
                                                <h2 class="discount_price"><fmt:formatNumber value="${goodsInfo.addn_goods[1].FNA_PRC}" pattern="#,###" />원/1년</h2>
                                            </div>
                                        </div>
                                        <div class="data_div">
                                            <strong>정기 결제</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>자동대사 서비스</strong>
                                            <p>상품 주문의 무통장 입금과<br>계좌 입금 내역을 자동으로 매칭하여<br>즉시 입금 완료 체크 서비스</p>
                                        </div>
                                        <div class="data_div">
                                            <strong>계좌 조회 주기 5분</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>계좌 등록 수량 무제한</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>자동대사 통계<br>카카오 알림톡 발송 서비스</strong>
                                        </div> 
                                        -->
                                        <div class="data_div">
                                            <strong>Cafe24회원전용</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>자동대사 서비스</strong>
                                            <p>입금자/입금액확인서비스</p>
                                        </div>
                                        <div class="data_div">
                                            <strong>■서비스 소개■<br>계좌 조회 주기 5분</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>계좌 등록 수량 무제한</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>자동대사 통계<br>카카오 알림톡 발송 서비스</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>■이용 방법■<br>1. 정산계좌통합관리 메뉴에서<br>‘정산계좌’를 선택하고<br>‘계좌등록 하기</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>2. ‘계좌등록’에서<br>‘입금확인계좌’ 선택하기</strong>
                                        </div>
                                        <div class="data_div">
                                            <strong>3. 대사서비스에서<br>‘카페24’의 주문건과 대사확인</strong>
                                        </div>
                                        <c:if test="${addn_goods_req_seq_no > 0}">
                                        <div class="data_div">
                                            <strong>■ 이용 해지 방법■<br>마이페이지 > 결제정보 메뉴에서<br>‘해지’ 하기</strong>
                                        </div>
                                        </c:if>
                                    </td>
                                </c:if>
                            </tr>
                            <tr>
                                <td class="label bold_black">인덱스</td>
                                <td class="label">
                                    정산예정금,오늘 정산금,판매예치금,<br>
                                    통장잔액 합계, 판매 통계 요약
                                </td>
                                <td class="freebot">
                                    <img class="check_icon" src="/assets/images/icon/check_black.png" alt="">
                                </td>
                                <td class="ronibot">
                                    <img class="check_icon" src="/assets/images/icon/check_green.png" alt="">
                                </td>
                                <td class="paibot">
                                    <img class="check_icon" src="/assets/images/icon/check_green.png" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td class="label bold_black" rowspan="2">
                                    정산예정금 통합관리
                                </td>
                                <td class="label">
                                    한 눈에 보기,<br>
                                    판매몰에서 제공하는 정산항목 상세보기,<br>
                                    정산예정금 달력으로보기,<br>
                                    판매몰별 관리 팁
                                </td>
                                <td class="freebot">
                                    <img class="class_icon" src="/assets/images/icon/x_black.png" alt="">
                                </td>
                                <td class="ronibot">
                                    <img class="check_icon" src="/assets/images/icon/check_green.png " alt="">
                                </td>
                                <td class="paibot">
                                    <img class="check_icon" src="/assets/images/icon/check_green.png " alt="">
                                </td>
                            </tr>
                            <tr id="top_line">
                                <td>
                                    일자별/판매몰별/배송상태별 <br>
                                    알림톡 리포트
                                </td>
                                <td>
                                    월 1회
                                </td>
                                <td onclick="ronibot()">
                                    매일
                                </td>
                                <td onclick="paibot()">
                                    매일
                                </td>
                            </tr>
                            <tr>
                                <td class="label bold_black">정산계좌 통합관리</td>
                                <td class="label">
                                    판매몰별 입금내역 <br>
                                    정산계좌 입출금 상세내역
                                </td>
                                <td class="freebot"><img class="class_icon" src="/assets/images/icon/x_black.png" alt=""></td>
                                <td class="ronibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                                <td class="paibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                            </tr>
                            <tr>
                                <td class="label bold_black">매출 통계</td>
                                <td class="label">
                                    매출 현황 요약 보고 <br>
                                    3년간 매출 통계 & 분석 <br>
                                    매출 분석 그래프
                                </td>
                                <td class="freebot"><img class="class_icon" src="/assets/images/icon/x_black.png" alt=""></td>
                                <td class="ronibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                                <td class="paibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                            </tr>
                            <tr>
                                <td class="label bold_black">과거 정산금 통계</td>
                                <td class="label">
                                    정산금 현황 요약보고 <br>
                                    3년간 정산금 통계&분석<br>
                                    매출 vs 정산금 비교 그래프<br>
                                    정산금 vs 입금 비교 그래프
                                </td>
                                <td class="freebot"><img class="class_icon" src="/assets/images/icon/x_black.png" alt=""></td>
                                <td class="ronibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                                <td class="paibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                            </tr>
                            <tr>
                                <td class="label bold_black">반품 통계</td>
                                <td class="label">
                                    반품 현황 요약보고 <br>
                                    3년간 반품 통계&분석<br>
                                    반품 분석 그래프
                                </td>
                                <td class="freebot"><img class="class_icon" src="/assets/images/icon/x_black.png" alt=""></td>
                                <td class="ronibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                                <td class="paibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                            </tr>
                            <tr>
                                <td class="label bold_black">동종업계 매출추이</td>
                                <td class="label">
                                    나의 매출랭킹<br>
                                    매출랭킹 변동추이<br>
                                    분야별 랭킹 top10
                                </td>
                                <td class="freebot"><img class="class_icon" src="/assets/images/icon/x_black.png" alt=""></td>
                                <td class="ronibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                                <td class="paibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                            </tr>
                            <tr>
                                <td class="label bold_black">점프 서비스</td>
                                <td class="label">
                                    판매몰부터 금융사, 쇼핑몰관리, 매입채널, 관공서까지<br>
                                    로그인 절차 없이 필요한 사이트로 바로 이동
                                </td>
                                <td class="freebot"><img class="class_icon" src="/assets/images/icon/x_black.png" alt=""></td>
                                <td class="ronibot"><img class="check_icon" src="/assets/images/icon/check_green.png"
                                        alt=""></td>
                                        <td class="paibot"><img class="check_icon" src="/assets/images/icon/check_green.png"
                                        alt=""></td>
                            </tr>
                            <tr>
                                <td class="label bold_black">금융 서비스</td>
                                <td class="label">
                                    내게 맞는 금융상품 추천 받기
                                </td>
                                <td class="freebot"><img class="class_icon" src="/assets/images/icon/check_black.png" alt=""></td>
                                <td class="ronibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                                <td class="paibot"><img class="check_icon" src="/assets/images/icon/check_green.png" alt=""></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="ticket_select_button_container">
                    <h1 id="select_ticket">
                        <div class="ticket_name"></div>
                        <span class="selected_msg">이용권을 선택해주세요.</span>
                    </h1>
                    <div id="row">
                        <button type="button" id="next_btn_bottom" onclick="pressedBtnNextForPayment();">다음</button>
                    </div>
                </div>
                <!-- //20200706_10 :: 이용권 안내 End -->
            </div>
            <!-- section2_pc end -->

            <!-- sec2_mb start -->
            <div id="sec2_mb">
                <div class="vid_btn2">
                    서비스 소개 영상
                </div>
                <div class="swiper-container_ticket_free">
                    <div class="swiper-wrapper swiper_border">
                        <div class="swiper-slide">
                            <div class="select_ticket">
                                <img src="/assets/images/icon/check_mb.png" alt="">
                            </div>
                            <div class="mb_ticket_header">
                                <h1>프리봇</h1>
                                <h2>무료</h2>
                            </div>
                            <div class="mb_slider_group">
                                <div class="data_div_box">
                                    <div class="data_div"></div>
                                    <h1 class="data_mb">데이터 제공량</h1>
                                    <h2 class="data_mb2">-</h2>
                                </div>
                                <div class="slide_info">
                                    <div class="mb_one_line">
                                        <h1 class="title">인덱스</h1>
                                        <p class="mb_underliner">
                                            정산예정금,오늘 정산금,판매 예치금,<br>
                                            통장잔액 합계, 판매 통계 요약
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_black.png" alt=""></h2>
                                    </div>
                                    <h1 class="title">정산예정금 통합관리</h1>
                                    <p>
                                        한 눈에 보기,
                                        판매몰에서 제공하는 정산항목 상세보기,
                                        정산예정금 달력으로 보기,
                                        판매몰별 관리 팁
                                    </p>
                                    <h2 class="mb_icon"><img src="/assets/images/icon/x_black.png" alt=""></h2>
                                    <p>
                                        일자별/판매몰별/배송상태별 <br>
                                        알림톡 리포트
                                    </p>
                                    <h2>월 1회</h2>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="select_ticket">
                                <img src="/assets/images/icon/check_mb.png" alt="">
                            </div>
                            <div class="mb_ticket_header">
                                <h1>프리봇</h1>
                                <h2>무료</h2>
                            </div>
                            <div class="mb_slider_group">
                                <div class="slide_info">
                                    <div class="mb_one_line">
                                        <h1 class="title">정산계좌 통합관리</h1>
                                        <p class="mb_underliner">
                                            판매몰별 입금내역
                                            정산계좌 입출금 상세내역
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/x_black.png" alt=""></h2>
                                    </div>
                                    <h1 class="title">매출 통계</h1>
                                    <p>
                                        매출 현황 요약 보고
                                        3년간 매출 통계 & 분석
                                        매출 분석 그래프
                                    </p>
                                    <h2 class="mb_icon"><img src="/assets/images/icon/x_black.png" alt=""></h2>
                                    <h1 class="title">과거 정산금 통계</h1>
                                    <p>
                                        정산금 현황 요약보고 <br>
                                        3년간 정산금 통계&분석
                                        매출 vs 정산금 비교 그래프
                                        정산금 vs 입금 비교 그래프
                                    </p>
                                    <h2 class="mb_icon"><img src="/assets/images/icon/x_black.png" alt=""></h2>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="select_ticket">
                                <img src="/assets/images/icon/check_mb.png" alt="">
                            </div>
                            <div class="mb_ticket_header">
                                <h1>프리봇</h1>
                                <h2>무료</h2>
                            </div>
                            <div class="mb_slider_group">
                                <div class="slide_info">
                                    <div class="mb_one_line">
                                        <h1 class="title">반품 통계</h1>
                                        <p class="mb_underliner">
                                            반품 현황 요약보고
                                            3년간 반품 통계&분석
                                            반품 분석 그래프
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/x_black.png" alt=""></h2>
                                    </div>
                                    <h1 class="title">동종업계 매출추이</h1>
                                    <p>
                                        나의 매출랭킹
                                        매출랭킹 변동추이
                                        분야별 랭킹 top10
                                    </p>
                                    <h2 class="mb_icon"><img src="/assets/images/icon/x_black.png" alt=""></h2>
                                    <h1 class="title">점프 서비스</h1>
                                    <p>
                                        판매몰부터 금융사, 쇼핑몰 관리,매입패널,관공서까지
                                        로그인 절차 없이 필요한 사이트로 바로 이동
                                    </p>
                                    <h2 class="mb_icon"><img src="/assets/images/icon/x_black.png" alt=""></h2>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="select_ticket">
                                <img src="/assets/images/icon/check_mb.png" alt="">
                            </div>
                            <div class="mb_ticket_header">
                                <h1>프리봇</h1>
                                <h2>무료</h2>
                            </div>
                            <div class="mb_slider_group">
                                <div class="slide_info">
                                    <h1 class="title">금융 서비스</h1>
                                    <p>
                                        내게 맞는 금융상품 추천 받기
                                    </p>
                                    <h2 class="mb_icon"><img src="/assets/images/icon/check_black.png" alt=""></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                <div class="mb_one_box">
                    <div class="swiper-container_ticket_free2">
                        <div class="swiper-wrapper swiper_border">
                            <div class="swiper-slide">
                                <div class="select_ticket">
                                    <img src="/assets/images/icon/check_mb.png" alt="">
                                </div>
                                <div class="mb_ticket_header">
                                    <h1 class="roni_one">로니봇</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                                <span class="original_price">
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.GOODS_BAS_PRC}" pattern="#,###" />원
                                                </span>
                                                <span class="discount_price">
                                                    <c:if test="${freeTrialYn eq 'N'}">
                                                        무료체험 이후
                                                    </c:if>
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" />원
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="discount_price">
                                                    ${goodsInfo.basic_goods_info.RNB.GOODS_DESC}
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </div>
                                <div class="mb_slider_group">
                                    <div class="data_div_box">
                                        <div class="data_div"></div>
                                        <h1 class="data_mb">데이터 제공량</h1>
                                        <h2 class="data_mb2">
                                            <span>판매몰 ID: 
                                                <c:choose>
                                                    <c:when test="${goodsInfo.basic_goods_info.RNB.SALE_MALL_REG_ID_PSB_CNT ne 0}">${goodsInfo.basic_goods_info.RNB.SALE_MALL_REG_ID_PSB_CNT}개</c:when>
                                                    <c:otherwise>무제한</c:otherwise>
                                                </c:choose>
                                                 / 계좌:
                                                <c:choose>
                                                    <c:when test="${goodsInfo.basic_goods_info.RNB.SETT_ACC_ACCT_REG_PSB_CNT ne 0}">${goodsInfo.basic_goods_info.RNB.SETT_ACC_ACCT_REG_PSB_CNT}개</c:when>
                                                    <c:otherwise>무제한</c:otherwise>
                                                </c:choose>
                                            </span>
                                        </h2>
                                        <h2 class="data_mb3"><br>'스마트스토어는 동일 계정(주계정 + 매니저 계정)에 한해서 <br> API ID가 다를 경우 하나의 계정으로 취급합니다.'</h2>
                                    </div>
                                    <div class="slide_info">
                                        <div class="mb_one_line">
                                            <h1 class="title">인덱스</h1>
                                            <p class="mb_underliner">
                                                정산예정금,오늘 정산금,판매 예치금,<br>
                                                통장잔액 합계, 판매 통계 요약
                                            </p>
                                            <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt="">
                                            </h2>
                                        </div>
                                        <h1 class="title">정산예정금 통합관리</h1>
                                        <p>
                                            한 눈에 보기,
                                            판매몰에서 제공하는 정산항목 상세보기,
                                            정산예정금 달력으로 보기,
                                            판매몰별 관리 팁
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                        <p>
                                            일자별/판매몰별/배송상태별 <br>
                                            알림톡 리포트
                                        </p>
                                        <h2>매일</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="select_ticket">
                                    <img src="/assets/images/icon/check_mb.png" alt="">
                                </div>
                                <div class="mb_ticket_header">
                                    <h1 class="roni_one">로니봇</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                                <span class="original_price">
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.GOODS_BAS_PRC}" pattern="#,###" />원
                                                </span>
                                                <span class="discount_price">
                                                    <c:if test="${freeTrialYn eq 'N'}">
                                                        무료체험 이후
                                                    </c:if>
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" />원
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="discount_price">
                                                    ${goodsInfo.basic_goods_info.RNB.GOODS_DESC}
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </div>
                                <div class="mb_slider_group">
                                    <div class="slide_info">
                                        <div class="mb_one_line">
                                            <h1 class="title">정산계좌 통합관리</h1>
                                            <p class="mb_underliner">
                                                판매몰별 입금내역
                                                정산계좌 입출금 상세내역
                                            </p>
                                            <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt="">
                                            </h2>
                                        </div>
                                        <h1 class="title">매출 통계</h1>
                                        <p>
                                            매출 현황 요약 보고
                                            3년간 매출 통계 & 분석
                                            매출 분석 그래프
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                        <h1 class="title">과거 정산금 통계</h1>
                                        <p>
                                            정산금 현황 요약보고 <br>
                                            3년간 정산금 통계&분석
                                            매출 vs 정산금 비교 그래프
                                            정산금 vs 입금 비교 그래프
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="select_ticket">
                                    <img src="/assets/images/icon/check_mb.png" alt="">
                                </div>
                                <div class="mb_ticket_header">
                                    <h1 class="roni_one">로니봇</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                                <span class="original_price">
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.GOODS_BAS_PRC}" pattern="#,###" />원
                                                </span>
                                                <span class="discount_price">
                                                    <c:if test="${freeTrialYn eq 'N'}">
                                                        무료체험 이후
                                                    </c:if>
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" />원
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="discount_price">
                                                    ${goodsInfo.basic_goods_info.RNB.GOODS_DESC}
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </div>
                                <div class="mb_slider_group">
                                    <div class="slide_info">
                                        <div class="mb_one_line">
                                            <h1 class="title">반품 통계</h1>
                                            <p class="mb_underliner">
                                                반품 현황 요약보고
                                                3년간 반품 통계&분석
                                                반품 분석 그래프
                                            </p>
                                            <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt="">
                                            </h2>
                                        </div>
                                        <h1 class="title">동종업계 매출추이</h1>
                                        <p>
                                            나의 매출랭킹
                                            매출랭킹 변동추이
                                            분야별 랭킹 top10
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                        <h1 class="title">점프 서비스</h1>
                                        <p>
                                            판매몰부터 금융사, 쇼핑몰 관리,매입패널,관공서까지
                                            로그인 절차 없이 필요한 사이트로 바로 이동
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="select_ticket">
                                    <img src="/assets/images/icon/check_mb.png" alt="">
                                </div>
                                <div class="mb_ticket_header">
                                    <h1 class="roni_one">로니봇</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                                <span class="original_price">
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.GOODS_BAS_PRC}" pattern="#,###" />원
                                                </span>
                                                <span class="discount_price">
                                                    <c:if test="${freeTrialYn eq 'N'}">
                                                        무료체험 이후
                                                    </c:if>
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" />원
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="discount_price">
                                                    ${goodsInfo.basic_goods_info.RNB.GOODS_DESC}
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </div>
                                <div class="mb_slider_group">
                                    <div class="slide_info">
                                        <h1 class="title">금융 서비스</h1>
                                        <p>
                                            내게 맞는 금융상품 추천 받기
                                        </p>
                                        <h2 class="mb_icon">
                                            <img src="/assets/images/icon/check_green.png" alt="">
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination2"></div>
                    </div>
                </div>
                <div class="rows">
                    <button class="buy" type="button" onclick="pressedBtnBuyForPayment();" id="btnRNB" disabled>구매하기</button>
                </div>
                <div class="mb_one_box">
                    <div class="swiper-container_ticket_free3">
                        <div class="swiper-wrapper swiper_border">
                            <div class="swiper-slide">
                                <div class="select_ticket">
                                    <img src="/assets/images/icon/check_mb.png" alt="">
                                </div>
                                <div class="mb_ticket_header">
                                    <h1 class="pai_one">파이봇</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                                <span class="original_price">
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.GOODS_BAS_PRC}" pattern="#,###" />원
                                                </span>
                                                <span class="discount_price">
                                                    <c:if test="${freeTrialYn eq 'N'}">
                                                        무료체험 이후
                                                    </c:if>
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" />원
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="discount_price">
                                                    ${goodsInfo.basic_goods_info.PYB.GOODS_DESC}
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </div>
                                <div class="mb_slider_group">
                                    <div class="data_div_box">
                                        <div class="data_div"></div>
                                        <h1 class="data_mb">데이터 제공량</h1>
                                        <h2 class="data_mb2">
                                            <span>판매몰 ID: 
                                                <c:choose>
                                                    <c:when test="${goodsInfo.basic_goods_info.PYB.SALE_MALL_REG_ID_PSB_CNT ne 0}">${goodsInfo.basic_goods_info.PYB.SALE_MALL_REG_ID_PSB_CNT}개</c:when>
                                                    <c:otherwise>무제한</c:otherwise>
                                                </c:choose>
                                                 / 계좌:
                                                <c:choose>
                                                    <c:when test="${goodsInfo.basic_goods_info.PYB.SETT_ACC_ACCT_REG_PSB_CNT ne 0}">${goodsInfo.basic_goods_info.PYB.SETT_ACC_ACCT_REG_PSB_CNT}개</c:when>
                                                    <c:otherwise>무제한</c:otherwise>
                                                </c:choose>
                                            </span>
                                        </h2>
                                    </div>
                                    <div class="slide_info">
                                        <div class="mb_one_line">
                                            <h1 class="title">인덱스</h1>
                                            <p class="mb_underliner">
                                                정산예정금,오늘 정산금,판매 예치금,<br>
                                                통장잔액 합계, 판매 통계 요약
                                            </p>
                                            <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt="">
                                            </h2>
                                        </div>
                                        <h1 class="title">정산예정금 통합관리</h1>
                                        <p>
                                            한 눈에 보기,
                                            판매몰에서 제공하는 정산항목 상세보기,
                                            정산예정금 달력으로 보기,
                                            판매몰별 관리 팁
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                        <p>
                                            일자별/판매몰별/배송상태별 <br>
                                            알림톡 리포트
                                        </p>
                                        <h2>매일</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="select_ticket">
                                    <img src="/assets/images/icon/check_mb.png" alt="">
                                </div>
                                <div class="mb_ticket_header">
                                    <h1 class="pai_one">파이봇</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                                <span class="original_price">
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.GOODS_BAS_PRC}" pattern="#,###" />원
                                                </span>
                                                <span class="discount_price">
                                                    <c:if test="${freeTrialYn eq 'N'}">
                                                        무료체험 이후
                                                    </c:if>
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" />원
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="discount_price">
                                                    ${goodsInfo.basic_goods_info.PYB.GOODS_DESC}
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </div>
                                <div class="mb_slider_group">
                                    <div class="slide_info">
                                        <div class="mb_one_line">
                                            <h1 class="title">정산계좌 통합관리</h1>
                                            <p class="mb_underliner">
                                                판매몰별 입금내역
                                                정산계좌 입출금 상세내역
                                            </p>
                                            <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt="">
                                            </h2>
                                        </div>
                                        <h1 class="title">매출 통계</h1>
                                        <p>
                                            매출 현황 요약 보고
                                            3년간 매출 통계 & 분석
                                            매출 분석 그래프
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                        <h1 class="title">과거 정산금 통계</h1>
                                        <p>
                                            정산금 현황 요약보고 <br>
                                            3년간 정산금 통계&분석
                                            매출 vs 정산금 비교 그래프
                                            정산금 vs 입금 비교 그래프
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="select_ticket">
                                    <img src="/assets/images/icon/check_mb.png" alt="">
                                </div>
                                <div class="mb_ticket_header">
                                    <h1 class="pai_one">파이봇</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                                <span class="original_price">
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.GOODS_BAS_PRC}" pattern="#,###" />원
                                                </span>
                                                <span class="discount_price">
                                                    <c:if test="${freeTrialYn eq 'N'}">
                                                        무료체험 이후
                                                    </c:if>
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" />원
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="discount_price">
                                                    ${goodsInfo.basic_goods_info.PYB.GOODS_DESC}
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </div>
                                <div class="mb_slider_group">
                                    <div class="slide_info">
                                        <div class="mb_one_line">
                                            <h1 class="title">반품 통계</h1>
                                            <p class="mb_underliner">
                                                반품 현황 요약보고
                                                3년간 반품 통계&분석
                                                반품 분석 그래프
                                            </p>
                                            <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt="">
                                            </h2>
                                        </div>
                                        <h1 class="title">동종업계 매출추이</h1>
                                        <p>
                                            나의 매출랭킹
                                            매출랭킹 변동추이
                                            분야별 랭킹 top10
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                        <h1 class="title">점프 서비스</h1>
                                        <p>
                                            판매몰부터 금융사, 쇼핑몰 관리,매입패널,관공서까지
                                            로그인 절차 없이 필요한 사이트로 바로 이동
                                        </p>
                                        <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="select_ticket">
                                    <img src="/assets/images/icon/check_mb.png" alt="">
                                </div>
                                <div class="mb_ticket_header">
                                    <h1 class="pai_one">파이봇</h1>
                                    <h2>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                                <span class="original_price">
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.GOODS_BAS_PRC}" pattern="#,###" />원
                                                </span>
                                                <span class="discount_price">
                                                    <c:if test="${freeTrialYn eq 'N'}">
                                                        무료체험 이후
                                                    </c:if>
                                                    <fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" />원
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="discount_price">
                                                    ${goodsInfo.basic_goods_info.PYB.GOODS_DESC}
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </h2>
                                </div>
                                <div class="mb_slider_group">
                                    <div class="slide_info">
                                        <h1 class="title">금융 서비스</h1>
                                        <p>
                                            내게 맞는 금융상품 추천 받기
                                        </p>
                                        <h2 class="mb_icon">
                                            <img src="/assets/images/icon/check_green.png" alt="">
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination3"></div>
                    </div>
                </div>
                <div class="rows">
                    <button class="buy" type="button" onclick="pressedBtnBuyForPayment();" id="btnPYB" disabled>구매하기</button>
                </div>

                <!-- Cafe24 고객만 뱅크봇 이용 가능 -->
                <c:if test="${cafe24_usr_yn eq 'Y'}">
                    <!-- //20200706_10 :: 모바일 뱅크봇 Start -->
                    <div class="mb_one_box">
                        <div class="swiper-container_ticket_bankbot">
                            <div class="swiper-wrapper swiper_border">
                                <div class="swiper-slide">
                                    <div class="select_ticket">
                                        <img src="/assets/images/icon/check_mb.png" alt="">
                                    </div>
                                    <div class="mb_ticket_header">
                                        <h1 class="bank_one">뱅크봇</h1>
                                        <h2>
                                            <span class="original_price">
                                                <fmt:formatNumber value="${goodsInfo.addn_goods[0].GOODS_BAS_PRC}" pattern="#,###" />원
                                            </span>
                                            <span class="discount_price">
                                                <fmt:formatNumber value="${goodsInfo.addn_goods[0].FNA_PRC}" pattern="#,###" />원
                                            </span>
                                        </h2>
                                    </div>
                                    <div class="mb_slider_group">
                                        <div class="data_div_box event">
                                            <div class="data_div"></div>
                                            <h1 class="data_mb">파격 혜택</h1>
                                            <h2 class="data_mb2">
                                                <span>1년 결제 시 가격 인하</span>
                                            </h2>
                                            <div class="event_desc">
                                                <span class="ori_price"><fmt:formatNumber value="${goodsInfo.addn_goods[1].GOODS_BAS_PRC}" pattern="#,###" />원</span>
                                                <span class="sale_price"><fmt:formatNumber value="${goodsInfo.addn_goods[1].FNA_PRC}" pattern="#,###" />/1년</span>
                                            </div>
                                        </div>
                                        <div class="slide_info">
                                            <div class="mb_one_line">
                                                <h1 class="title">한달 주기 결제</h1>
                                                <!-- <p class="mb_underliner">
                                                    정산예정금,오늘 정산금,판매 예치금,<br>
                                                    통장잔액 합계, 판매 통계 요약
                                                </p>
                                                <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt="">
                                                </h2> -->
                                            </div>
                                            <h1 class="title">자동대사 서비스</h1>
                                            <p>
                                                5분 주기 무제한 등록 가능<br>
                                                자동 대사 통계<br>
                                                카카오 알림톡 발송(일 1회)
                                            </p>
                                            <h2 class="mb_icon"><img src="/assets/images/icon/check_green.png" alt=""></h2>
                                            <!-- <p>
                                                일자별/판매몰별/배송상태별 <br>
                                                알림톡 리포트
                                            </p>
                                            <h2>매일</h2> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination_bankbot"></div>
                        </div>
                    </div>
                    <div class="rows">
                        <button class="buy" type="button" onclick="pressedBtnBuyForPayment();" id="btnBNKB" disabled>구매하기</button>
                    </div>
                    <!-- //20200706_10 :: 모바일 뱅크봇 End -->
                </c:if>

            </div>
            <!-- sec2_mb end -->
            <!-- section3 start -->
            <div id="section3">
                <h1 id="sec3_title">
                    <span>셀러봇캐시 유료 이용권</span>을 구매하면 무엇이 좋은가요?
                </h1>
                <ul id="bottom_slider_menu">
                    <li class="click_menu" value="0">정산예정금 통합관리</li>
                    <li value="1">정산예정금 알림톡</li>
                    <li value="2">정산계좌 통합관리</li>
                    <li value="3">매출 통계</li>
                    <li value="4">과거 정산금 통계</li>
                    <li value="5">반품 통계</li>
                    <li value="6">동종업계 매출추이</li>
                    <li value="7">점프 서비스</li>
                    <li value="8">금융 서비스</li>
                </ul>
                <div id="bottom_menu_mb">
                    <select id="menuSelect">
                        <option value="0">정산예정금 통합관리</option>
                        <option value="1">정산예정금 알림톡</option>
                        <option value="2">정산계좌 통합관리</option>
                        <option value="3">매출 통계</option>
                        <option value="4">과거 정산금 통계</option>
                        <option value="5">반품 통계</option>
                        <option value="6">동종업계 매출추이</option>
                        <option value="7">점프 서비스</option>
                        <option value="8">금융 서비스</option>
                    </select>
                </div>
                <div class="swiper-container_bottom" id="discArea">
                    <ul class="second_menu" id="subMenu">
                        <!-- <li class="secound_click_active">정산예정금 한 눈에 보기</li>
                        <li>정산예정금 상세보기</li>
                        <li>정산예정금 달력</li>
                        <li>
                            정산예정금을 오픈마켓과 비오픈마켓으로 나누어 배송상태별, 정산일자별로 요약해서 볼 수 있습니다.
                        </li> -->
                    </ul>
                    <div class="swiper_left_area"></div>
                    <div class="swiper_right_area"></div>
                    <div class="swiper-wrapper" id="discImgArea">
                        <!-- <div class="swiper-slide">
                            <img class="botttom_slide1" src="/assets/images/ticket_img/slide_sample1.png" alt="">
                        </div>
                        <div class="swiper-slide">Slide 2</div>
                        <div class="swiper-slide">Slide 3</div> -->
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev prev_bottom"></div>
                    <div class="swiper-button-next next_bottom"></div>
                </div>
                <script>
                    document.querySelectorAll(".swiper_left_area")[0].addEventListener("mouseover", function () {
                        document.querySelectorAll(".swiper-container_bottom .swiper-button-prev")[0].style.display = "flex";
                    });
                    document.querySelectorAll(".swiper_left_area")[0].addEventListener("mouseout", function () {
                        document.querySelectorAll(".swiper-container_bottom .swiper-button-prev")[0].style.display = "none";
                    });
                    document.querySelectorAll(".swiper_right_area")[0].addEventListener("mouseover", function () {
                        document.querySelectorAll(".swiper-container_bottom .swiper-button-next")[0].style.display = "flex";
                    });
                    document.querySelectorAll(".swiper_right_area")[0].addEventListener("mouseout", function () {
                        document.querySelectorAll(".swiper-container_bottom .swiper-button-next")[0].style.display = "none";
                    });

                    document.querySelectorAll(".swiper-container_bottom .swiper-button-prev")[0].addEventListener("mouseover", function () {
                        document.querySelectorAll(".swiper-container_bottom .swiper-button-prev")[0].style.display = "flex";
                    });

                    document.querySelectorAll(".swiper-container_bottom .swiper-button-next")[0].addEventListener("mouseover", function () {
                        document.querySelectorAll(".swiper-container_bottom .swiper-button-next")[0].style.display = "flex";
                    });

                </script>
            </div>
            <!-- section3 end -->
        </div>
    </div>
    <!-- ticket_type_popup start -->
    <div id="ticket_type_popup">
        <ul>
            <li>
                <img icon src="/assets/images/icon/x_black.png" alt="">
                고객님, 판매몰이 너무 많으셔서
                파이봇만 선택 가능해요~
            </li>
            <li>
                <button type="button">
                    확인
                </button>
            </li>
        </ul>
        <div id="bg">
        </div>
    </div>
    <!-- ticket_type_popup end -->
</div>
<div class="top_btn_container">
    <div class="edit_top_btn">
        <h1>TOP</h1>
    </div>
</div>
<div class="edit_video_popup" style="display: flex;">
    <div class="edit_video_popup_container">
        <p><img src="/assets/images/member/close.png" alt=""></p>
        <iframe width="100%" height="100%"
            src="https://www.youtube.com/embed/4ZruNOiU07A?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
    </div>
    <div class="background_black"></div>
</div>
<style>
    .swiper-container_ticket {
        height: 18.88rem;
        max-width: 1200px;
        width: 100%;
        overflow: hidden;
        position: relative;
        margin-bottom: 5rem;
    }

    .swiper-container_ticket>.swiper-wrapper>.swiper-slide#first_slider {
        background: #000000;
        height: 18.88rem;
        width: 50%;
        background: url("/assets/images/ticket_img/ticket_discount(PC)_20220407.jpg")no-repeat;
        background-size: cover;
        background-position: center;
    }

    .swiper-container_ticket>.swiper-wrapper>.swiper-slide#first_slider2 {
        background: #FFDE00;
        height: 18.88rem;
        width: 50%;
        background: url("/assets/images/ticket_img/ticket_free(PC)_20220407.jpg")no-repeat;
        background-size: cover;
        background-position: center;
        vertical-align: top;
    }

    .swiper-container_ticket>.swiper-button-next {
        right: 0;
        top: 9.44rem;
    }

    .swiper-container_ticket>.swiper-wrapper {
        max-width: 1300px;
        width: 100%;
    }

    .swiper-container_ticket>.swiper-button-prev {
        left: 0;
        top: 9.44rem;
    }
</style>

<script src="/assets/js/swiper-master/dist/js/swiper.min.js"></script>


<input type="hidden" id="rnbGoodOptSeqNo" value="${goodsInfo.basic_goods_info.RNB.GOODS_OPT_SEQ_NO}">
<input type="hidden" id="pybGoodOptSeqNo" value="${goodsInfo.basic_goods_info.PYB.GOODS_OPT_SEQ_NO}">

<script>
    window.onpageshow = function(event) {
        // BFCache로 인한 스크립트 오작동 처리
        if(event.persisted || (window.performance && window.performance.navigation.type == 2)) {
            location.reload(true);
        }
    }

    $(document).ready(function () {

        function disableNextBtn() {
            $("#next_btn_top").attr('disabled', true);
            $("#next_btn_top").css("background", '#AAAAAA');
            $("#next_btn_bottom").attr('disabled', true);
            $("#next_btn_bottom").css("background", '#AAAAAA');
        }

        function disableFRB() {
            $('[name="bot_selector"].freebot').attr('disabled', true);
            if($("#cafe24_usr_yn").val() == 'Y')
                $('.freebot_selector').addClass('disabled');
            else
                $('.freebot_selector_nb').addClass('disabled');
        }

        function disableRNB() {
            $('[name="bot_selector"].ronibot').attr('disabled', true);
            if($("#cafe24_usr_yn").val() == 'Y')
                $('.ronibot_selector').addClass('disabled');
            else
                $('.ronibot_selector_nb').addClass('disabled');
        }

        function disablePYB() {
            $('[name="bot_selector"].paibot').attr('disabled', true);
            if($("#cafe24_usr_yn").val() == 'Y')
                $('.paibot_selector').addClass('disabled');
            else
                $('.paibot_selector_nb').addClass('disabled');
        }

        function disableBNKB() {
            $('[name="bot_selector"].bankbot').attr('disabled', true);
            $('.bankbot_selector').addClass('disabled');
        }

        if ('${goodsInfo.eventNo}' == 0)  {
            $('.ticket_select_button_container').show();
            $('.table_container').show();

            /** 상품 disable 처리 */
            // 카페24 유입회원
            if ($("#cafe24_usr_yn").val() == 'Y') {
                if ($("#goodsTypCd").val() == "PYB") {
                    paibot();
                    // 기존 상품 + 뱅크봇 이용 중
                    if($("#basic_goods_req_seq_no").val() > 0 && $("#addn_goods_req_seq_no").val() > 0) {
                        disablePYB();
                        disableBNKB();
                        disableNextBtn();
                    } else if($("#basic_goods_req_seq_no").val() > 0) { // 기존 상품만 이용 중
                        disablePYB();
                    } else if($("#addn_goods_req_seq_no").val() > 0) { // 뱅크봇만 이용 중
                        disableFRB();
                        disableBNKB();
                    } else if($("#forced").val() == 'Y') {
                        disableRNB();
                    }
                } else if($("#goodsTypCd").val() == "RNB") {
                    ronibot();

                    // 기존 상품 + 뱅크봇 이용 중
                    if ($("#basic_goods_req_seq_no").val() > 0 && $("#addn_goods_req_seq_no").val() > 0) {
                        disableRNB();
                        disableBNKB();
                        disableNextBtn();
                    }
                    else if($("#basic_goods_req_seq_no").val() > 0) { // 기존 상품만 이용 중
                        disableRNB();
                    }
                } else {
                    // 뱅크봇만 이용 중
                    if ($("#addn_goods_req_seq_no").val() > 0) {
                        disableFRB();
                        disableBNKB();
                        displaySelectMsg();
                    }
                }
            }
            else { // 일반회원
                if ($("#goodsTypCd").val() == "PYB") {
                    paibot();

                    // 파이봇 이용 중
                    if ($("#basic_goods_req_seq_no").val() > 0) {
                        disablePYB();
                        disableNextBtn();
                    }
                } else if ($("#goodsTypCd").val() == "RNB") {
                    ronibot();

                    // 로니봇 이용 중
                    if ($("#basic_goods_req_seq_no").val() > 0) {
                        disableRNB();
                        disableNextBtn();
                    }
                }
            }
        }

        updateSubMenu(null);
        getSliderList();
    });

    function getSliderList() {
        var leftItem = document.querySelectorAll(".swiper-container_bottom .swiper-button-prev")[0];
        var rightItem = document.querySelectorAll(".swiper-container_bottom .swiper-button-next")[0];
        var textList = [];
        document.querySelectorAll(".second_menu li").forEach(function (item, idx) {
            textList.push(item.innerText);
        });
        textList.forEach(function (item, idx) {
            if (document.querySelectorAll(".second_menu li")[idx].classList.contains("secound_click_active")) {
                var activeItem = document.querySelectorAll(".second_menu li")[idx].innerText;
                if (textList[idx] == activeItem) {
                    var listLength = document.querySelectorAll(".second_menu li").length;
                    if (listLength > 2) {
                        leftItem.innerText = textList[listLength - 2]
                        rightItem.innerText = textList[idx + 1];
                    }
                    else {
                        leftItem.innerText = textList[0]
                        rightItem.innerText = textList[0]
                    }
                }
            }

        });
    }

    // window.addEventListener("load", function () {
    //     getSliderList();
    // });

    var mySwiper22 = new Swiper('.swiper-container_bottom', {
        slidesPerView: 1,
        // Navigation arrows
        navigation: {
            nextEl: '.next_bottom',
            prevEl: '.prev_bottom',
        },
        pagination: {
            el: '.swiper-pagination',
        },
        loop: true,
        on: {
            slideChangeTransitionEnd: function () {
                var index = 0;
                if($("#DeviceType").val() == 'PC')
                    index = $("#bottom_slider_menu").find(".click_menu").val();
                else
                    index = $("#menuSelect").val();

                $(".second_menu li").eq(this.realIndex).addClass("secound_click_active");
                $(".second_menu li").not($(".second_menu li").eq(this.realIndex)).removeClass("secound_click_active");

                $("#discTextArea").html(discList[index][this.realIndex].disc);

                // 좌/우 버튼에 다음 서브 메뉴명 설정
                var nextIdx = (this.realIndex == discList[index].length - 1) ? 0 : this.realIndex + 1;
                var prevIdx = (this.realIndex == 0) ? discList[index].length - 1 : this.realIndex - 1;

                $(".swiper-container_bottom .swiper-button-next").html("<span class='nav_text'>" + discList[index][nextIdx].title + "</span>");
                $(".swiper-container_bottom .swiper-button-prev").html("<span class='nav_text'>" + discList[index][prevIdx].title + "</span>");
            },
        }
    });

    var mySwiper = new Swiper('.swiper-container_ticket', {
        slidesPerView: 2,
        // Navigation arrows
        navigation: {
            nextEl: '.next_top',
            prevEl: '.prev_top',
        },
        loop: true,
        breakpoints: {
            880: {
                slidesPerView: 1,
                spaceBetween: 0
            },
        }
    });

    var mySwiper1_mb = new Swiper('.swiper-container_ticket_free ', {
        slidesPerView: 1,
        spaceBetween: 50,
        // Navigation arrows
        pagination: {
            el: '.swiper-pagination',
        },
    });

    var mySwiper1_mb2 = new Swiper('.swiper-container_ticket_free2', {
        slidesPerView: 1,
        spaceBetween: 50,
        // Navigation arrows
        pagination: {
            el: '.swiper-pagination2',
        },
    });

    var mySwiper1_mb3 = new Swiper('.swiper-container_ticket_free3', {
        slidesPerView: 1,
        spaceBetween: 50,
        // Navigation arrows
        pagination: {
            el: '.swiper-pagination3',
        },
    });

    var mySwiper1_mb3 = new Swiper('.swiper-container_ticket_bankbot', {
        slidesPerView: 1,
        spaceBetween: 50,
        // Navigation arrows
        pagination: {
            el: '.swiper-pagination_bankbot',
        },
    });
</script>

<script>
    // document.querySelector("#section2_pc > .table_container > table").addEventListener("click", function (e) {
    //     action_coords(event)
    // });
    $("#section2_pc > .table_container > table").click(function(){
        action_coords(event);
    });

    function action_coords(event) {
        var tableWidth = Number(window.getComputedStyle(document.querySelector("#section2_pc")).width.split("px")[0]);
        var x2 = event.offsetX / tableWidth;
        if (x2 > 0.57 && x2 < 0.78) {
            ronibot()
        }
        if (x2 > 0.78) {
            paibot()
        }
    }

    $("#menuSelect").change(function () {
        updateSubMenu(this);
    });

    $("#bottom_slider_menu li").click(function () {
        $(this).addClass("click_menu");
        $("#bottom_slider_menu li").not($(this)).removeClass("click_menu");
        updateSubMenu(this);
        getSliderList();
    });

    //클릭불가, 회색처리는 해당 함수를 조건문 안에 실행해주시면 됩니다.(함수를 실행하면 로니봇이 비활성화됩니다.)
    function ronibot() {
        $('[name="bot_selector"].ronibot').prop('checked', true);
        if($("#cafe24_usr_yn").val() == "Y")
            $('.ronibot_selector').addClass('is_active');
        else
            $('.ronibot_selector_nb').addClass('is_active');
        displaySelectMsg();
    }

    function paibot() {
        $('[name="bot_selector"].paibot').prop('checked', true);
        if($("#cafe24_usr_yn").val() == "Y")
            $('.paibot_selector').addClass('is_active');
        else
            $('.paibot_selector_nb').addClass('is_active');
        displaySelectMsg();
    }

    function selectRNB(isSelected) {
        if(isSelected == 'disabled') {
            $(".swiper-container_ticket_free2 .swiper-slide").addClass("mobile_selected");
            $(".swiper-container_ticket_free2 .select_ticket").css("display", "block");
            $("#btnRNB").css("background-color", "#0035E1");
            $("#btnRNB").attr('disabled', false);
        }
        else {
            $(".swiper-container_ticket_free2 .swiper-slide").removeClass("mobile_selected");
            $(".swiper-container_ticket_free2 .select_ticket").css("display", "none");
            $("#btnRNB").css("background-color", "#AAAAAA");
            $("#btnRNB").attr('disabled', true);
        }
    }

    function selectPYB(isSelected) {
        if(isSelected == 'disabled') {
            $(".swiper-container_ticket_free3 .swiper-slide").addClass("mobile_selected");
            $(".swiper-container_ticket_free3 .select_ticket").css("display", "block");
            $("#btnPYB").css("background-color", "#0035E1");
            $("#btnPYB").attr('disabled', false);
        }
        else {
            $(".swiper-container_ticket_free3 .swiper-slide").removeClass("mobile_selected");
            $(".swiper-container_ticket_free3 .select_ticket").css("display", "none");
            $("#btnPYB").css("background-color", "#AAAAAA");
            $("#btnPYB").attr('disabled', true);
        }
    }

    function selectBNKB(isSelected) {
        if(isSelected == 'disabled') {
            $(".swiper-container_ticket_bankbot .swiper-slide").addClass("mobile_selected");
            $(".swiper-container_ticket_bankbot .select_ticket").css("display", "block");
            $("#btnBNKB").css("background-color", "#0035E1");
            $("#btnBNKB").attr('disabled', false);
        }
        else {
            $(".swiper-container_ticket_bankbot .swiper-slide").removeClass("mobile_selected");
            $(".swiper-container_ticket_bankbot .select_ticket").css("display", "none");
            $("#btnBNKB").css("background-color", "#AAAAAA");
            $("#btnBNKB").attr('disabled', true);
        }
    }

    $(".swiper-container_ticket_free2 .swiper-slide").click(function () {
        if($("#basic_goods_req_seq_no").val() > 0) {
            showAlert(getUsingGoodsNames());
            return;
        }

        if(($("#forced").val() == "Y" && $("#goodsTypCd").val() == "PYB")) {
            showAlert("서비스 정책으로 인해 해당 상품을 선택할 수 없습니다.");
            return;
        }

        var isSelected = $("#btnRNB").attr('disabled');
        selectRNB(isSelected);
        
        if(isSelected == 'disabled' && $("#btnPYB").attr('disabled') != 'disabled')
            selectPYB($("#btnPYB").attr('disabled'));
    });

    $(".swiper-container_ticket_free3 .swiper-slide").click(function () {
        if($("#basic_goods_req_seq_no").val() > 0) {
            showAlert(getUsingGoodsNames());
            return;
        }

        if(($("#forced").val() == "Y" && $("#goodsTypCd").val() == "RNB")) {
            showAlert("서비스 정책으로 인해 해당 상품을 선택할 수 없습니다.");
            return;
        }

        var isSelected = $("#btnPYB").attr('disabled');
        selectPYB(isSelected);

        if(isSelected == 'disabled' && $("#selectRNB").attr('disabled') != 'disabled')
            selectRNB($("#selectRNB").attr('disabled'));
    });

    $(".swiper-container_ticket_bankbot .swiper-slide").click(function () {
        if($("#addn_goods_req_seq_no").val() > 0) {
            showAlert(getUsingGoodsNames());
            return;
        }

        var isSelected = $("#btnBNKB").attr('disabled');
        selectBNKB(isSelected);
    });
</script>
<div>
    <form id="goPaymentFrm" action="/sub/payment/payment" method="POST">
        <input type="hidden" name="type">
        <input type="hidden" name="seq">
        <!-- <input type="hidden" name="forced"> -->
        <input type="hidden" name="isSelectedBNKB">
    </form>
</div>
<script>
    $(function () {
        var conWidth = $(".jump_con").width();
        var winWidth = $(window).width();
        var calValue = (winWidth - conWidth) / 2 - 80;
        $(".edit_top_btn").css("right", calValue);

        $(window).resize(function () {
            var conWidth = $(".my_wra").width();
            var winWidth = $(window).width();
            var calValue = (winWidth - conWidth) / 2 - 80;
            $(".edit_top_btn").css("right", calValue);
        });
        $(".edit_top_btn").click(function () {
            $('html, body').stop().animate({
                scrollTop: 0
            }, 500);
        });

        $(window).scroll(function () {
            var y = $(window).scrollTop();
            if (y > 200) {
                $(".edit_top_btn").show();
            } else {
                $(".edit_top_btn").hide();
            }
        });
    });

    if ('NodeList' in window && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = function (callback, thisArg) {
            thisArg = thisArg || window;
            for (var i = 0; i < this.length; i++) {
                callback.call(thisArg, this[i], i, this);
            }
        };
    }

    //영상 팝업 스크립트
    document.querySelectorAll(".edit_video_popup")[0].style.display = "none";
    document.querySelectorAll(".vid_btn")[0].addEventListener("click", function () {
        document.querySelectorAll(".edit_video_popup")[0].style.display = "flex";
    });
    document.querySelectorAll(".edit_video_popup_container p")[0].addEventListener("click", function () {
        document.querySelectorAll(".edit_video_popup")[0].style.display = "none";
        document.querySelectorAll(".edit_video_popup iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });
    // document.querySelectorAll(".vid_btn2")[0].addEventListener("click", function () {
    //     document.querySelectorAll(".edit_video_popup")[0].style.display = "flex";
    // });
    $(".vid_btn2").click(function(){
        document.querySelectorAll(".edit_video_popup")[0].style.display = "flex";
    })
</script>

<style>
    .my_wra {
        overflow: hidden;
    }

    .color_disable_2 {
        background-color: #99a0a4 !important;
    }

    .vid_btn {
        width: 136px;
    }

    .forced_notice {
        text-align: center;
        font-size: 1.125rem;
        margin-top: 2rem;
    }

    swiper-container_ticket_free {
        width: 100%;
        overflow: hidden;
    }
</style>

<!-- //20200706_10 :: 이용권 안내 셀렉트 컨트롤 Start -->
<script>
    //(function($){
    $(document).ready(function(){
        if($("#forced").val() == "Y" && $("#basic_goods_req_seq_no").val() == "") {
            $("#forced_notice").text("서비스 정책으로 인해 로니봇을 선택할 수 없습니다.");
        }

        $('[name="bot_selector"]').on('change', function(){
            if( $(this).parent().hasClass('disabled') ) {
                if ($("#basic_goods_req_seq_no").val() > 0) {
                    return;
                }
                else if ($("#forced").val() === "Y") {
                    showAlert("서비스 정책으로 인해 상품을 변경할 수 없습니다.");
                    return;
                }
            }

            if( $(this).prop('checked') ) {
                if( $(this).hasClass('freebot') || $(this).hasClass('ronibot') || $(this).hasClass('paibot') ) {
                    $('[name="bot_selector"].freebot').prop('checked', false).parent().removeClass('is_active');
                    $('[name="bot_selector"].ronibot').prop('checked', false).parent().removeClass('is_active');
                    $('[name="bot_selector"].paibot').prop('checked', false).parent().removeClass('is_active');
                    $(this).prop('checked', true).parent().addClass('is_active');
                }
                else {
                    $(this).parent().addClass('is_active');
                }
            }
            else {
                $(this).parent().removeClass('is_active');
            }

            displaySelectMsg();

        });
    });
    //})(jQuery);

    function displaySelectMsg() {
        // Cafe24 회원인 경우
        if($("#cafe24_usr_yn").val() == "Y") {
            // 일반 서비스와 부가 서비스 모두 이용 중인 경우
            if($("#basic_goods_req_seq_no").val() > 0 && $("#addn_goods_req_seq_no").val() > 0) {
                if($("#goodsTypCd").val() == 'RNB')
                    $('#select_ticket .ticket_name').append('<span class="ronibot">로니봇</span>');
                else if($("#goodsTypCd").val() == 'PYB')
                    $('#select_ticket .ticket_name').append('<span class="paibot">파이봇</span>');

                $('#select_ticket .ticket_name').append('<span class="bankbot">뱅크봇</span>');
                $('.selected_msg').text('을 이용 중입니다.');
            }
            else if($("#basic_goods_req_seq_no").val() > 0) { // 일반 서비스만 이용 중인 경우
                $('#select_ticket .ticket_name').empty();

                var appendHtml = "";
                if( $('[name="bot_selector"]:checked').length > 0 ) {
                    $('[name="bot_selector"]:checked').each(function(){
                        if(!$(this).parent().hasClass('disabled') && $(this).hasClass('bankbot')) {
                            const productLabel = $(this).val(),
                                    productClass = $(this).attr('class');
                            appendHtml += '<span class="' + productClass + '">' + productLabel + '</span>';
                        }
                    });
                }

                if(appendHtml == "") {
                    if($("#goodsTypCd").val() == 'RNB')
                        $('#select_ticket .ticket_name').append('<span class="ronibot">로니봇</span>');
                    else if($("#goodsTypCd").val() == 'PYB')
                        $('#select_ticket .ticket_name').append('<span class="paibot">파이봇</span>');

                    $('.selected_msg').text('을 이용 중입니다.');
                }
                else {
                    $('#select_ticket .ticket_name').append(appendHtml);                    
                    $('.selected_msg').text('선택 완료');
                }
            }
            else if($("#addn_goods_req_seq_no").val() > 0) { // 부가 서비스만 이용 중인 경우
                $('#select_ticket .ticket_name').empty();

                var appendHtml = "";
                if( $('[name="bot_selector"]:checked').length > 0 ) {
                    $('[name="bot_selector"]:checked').each(function(){
                        if(!$(this).parent().hasClass('disabled') && !$(this).hasClass('bankbot')) {
                            const productLabel = $(this).val(),
                                    productClass = $(this).attr('class');
                            appendHtml += '<span class="' + productClass + '">' + productLabel + '</span>';
                        }
                    });
                }

                if(appendHtml == "") {
                    $('#select_ticket .ticket_name').append('<span class="freebot">프리봇</span>');
                    $('#select_ticket .ticket_name').append('<span class="bankbot">뱅크봇</span>');
                    $('.selected_msg').text('을 이용 중입니다.');
                }
                else {
                    $('#select_ticket .ticket_name').append(appendHtml);                    
                    $('.selected_msg').text('선택 완료');
                }
            }
            else { // 이용 중인 서비스가 없는 경우
                displayMessage();
            }
        }
        else { // 일반 회원인 경우
            // 이용 중인 서비스가 있는 경우
            if ($("#basic_goods_req_seq_no").val() > 0) {
                if($("#goodsTypCd").val() == 'RNB')
                    $('#select_ticket .ticket_name').append('<span class="ronibot">로니봇</span>');
                else if($("#goodsTypCd").val() == 'PYB')
                    $('#select_ticket .ticket_name').append('<span class="paibot">파이봇</span>');

                $('.selected_msg').text('을 이용 중입니다.');
            }
            else { // 이용 중인 서비스가 없는 경우
                displayMessage();
            }
        }
    }

    // 기존의 메시지 표시용 함수
    function displayMessage() {
        $('#select_ticket .ticket_name').empty();
        if( $('[name="bot_selector"]:checked').length > 0 ) {
            $('[name="bot_selector"]:checked').each(function(){
                if(!$(this).parent().hasClass('disabled') ) {
                    const productLabel = $(this).val(),
                            productClass = $(this).attr('class');
                    $('#select_ticket .ticket_name').append('<span class="' + productClass + '">' + productLabel + '</span>');
                }
            });
            $('.selected_msg').text('선택 완료');
        } else {
            $('.selected_msg').text('이용권을 선택해주세요.');
        }
    }
</script>
<!-- //20200706_10 :: 이용권 안내 셀렉트 컨트롤 End -->

<!-- 20201105 :: 이벤트 팝업 추가 -->
<c:if test="${goodsInfo.eventNo == 52}">
    <input type="hidden" name="evtGoodsType" id="evtGoodsType" value="" />
    <input type="hidden" name="evtOptSeqNo" id="evtOptSeqNo" value="" />
    <div class="event_popup_container">
        <div class="event_popup">
            <div class="popup_header">
                <h2><strong>셀러봇 캐시</strong> 인터파크 광고머니 이벤트</h2>
                <button class="pop_close"><img src="/assets/images/common/event/pop_close.png" alt="팝업 닫기"></button>
            </div>
            <div class="popup_body">
                <div class="event_msg_box">
                    <p>인터파크를 통해 접속하신 경우, 이용권 구매 시 인터파크 광고머니를 지급받을 수 있어요.</p>
                </div>
                <div class="event_table">
                    <table>
                        <thead>
                            <tr>
                                <th class="type">이용권</th>
                                <th class="period">이용기간</th>
                                <th class="price">결제금액<br>(VAT 별도)</th>
                                <th class="event_msg">&nbsp;</th>
                                <th class="event_money">광고머니</th>
                            </tr>
                        </thead>
                        <tbody class="ronibot">
                            <tr data-goods_opt_seq_no="5">
                                <th class="type" rowspan="3">로니봇</th>
                                <td class="period">
                                    <div class="inner_wrap">
                                        3개월
                                    </div>
                                </td>
                                <td class="price">
                                    <div class="inner_wrap">
                                        <ins>30,000원</ins>
                                    </div>
                                </td>
                                <td class="event_msg" rowspan="3">
                                    <div class="inner_wrap">
                                        <img src="/assets/images/common/event/event_arrow.png" alt="광고 머니 지급">
                                    </div>
                                </td>
                                <td class="event_money">
                                    <div class="inner_wrap">
                                        <ins>60,000원</ins>
                                    </div>
                                </td>
                            </tr>
                            <tr data-goods_opt_seq_no="7">
                                <td class="period">
                                    <div class="inner_wrap">
                                        6개월
                                    </div>
                                </td>
                                <td class="price">
                                    <div class="inner_wrap">
                                        <del>60,000원</del>
                                        <ins>57,000원</ins>
                                    </div>
                                </td>
                                <td class="event_money">
                                    <div class="inner_wrap">
                                        <ins>114,000원</ins>
                                    </div>
                                </td>
                            </tr>
                            <tr data-goods_opt_seq_no="9">
                                <td class="period">
                                    <div class="inner_wrap">
                                        12개월
                                    </div>
                                </td>
                                <td class="price">
                                    <div class="inner_wrap">
                                        <del>120,000원</del>
                                        <ins>108,000원</ins>
                                    </div>
                                </td>
                                <td class="event_money">
                                    <div class="inner_wrap">
                                        <ins>216,000원</ins>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody class="paibot">
                            <tr data-goods_opt_seq_no="6">
                                <th class="type" rowspan="3">파이봇</th>
                                <td class="period">
                                    <div class="inner_wrap">
                                        3개월
                                    </div>
                                </td>
                                <td class="price">
                                    <div class="inner_wrap">
                                        <ins>60,000원</ins>
                                    </div>
                                </td>
                                <td class="event_msg" rowspan="3">
                                    <div class="inner_wrap">
                                        <img src="/assets/images/common/event/event_arrow_2.png" alt="광고 머니 지급">
                                    </div>
                                </td>
                                <td class="event_money">
                                    <div class="inner_wrap">
                                        <ins>120,000원</ins>
                                    </div>
                                </td>
                            </tr>
                            <tr data-goods_opt_seq_no="8">
                                <td class="period">
                                    <div class="inner_wrap">
                                        6개월
                                    </div>
                                </td>
                                <td class="price">
                                    <div class="inner_wrap">
                                        <del>120,000원</del>
                                        <ins>114,000원</ins>
                                    </div>
                                </td>
                                <td class="event_money">
                                    <div class="inner_wrap">
                                        <ins>228,000원</ins>
                                    </div>
                                </td>
                            </tr>
                            <tr data-goods_opt_seq_no="10">
                                <td class="period">
                                    <div class="inner_wrap">
                                        12개월
                                    </div>
                                </td>
                                <td class="price">
                                    <div class="inner_wrap">
                                        <del>240,000원</del>
                                        <ins>216,000원</ins>
                                    </div>
                                </td>
                                <td class="event_money">
                                    <div class="inner_wrap">
                                        <ins>432,000원</ins>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="selected_bot">
                        <span class="bot_name"></span>
                        <!--span class="t">선택 완료</span-->
                    </div>
                    <div class="event_guide">
                        <ul>
                            <li>※ 이용기간 종료 후 기존 이용권, 이용기간만큼 자동 정기결제됩니다.</li>
                            <li>※ 정확한 인터파크 광고머니 지급 일정은 셀러봇캐시 이벤트 페이지를 참조해주세요.</li>
                            <li>※ 광고머니 소진 시 조기 종료될 수 있으며, 이용권과 광고머니 지급 후 환불은 불가능합니다.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="popup_footer">
                <button class="popup_button next_step">다음</button>
            </div>
        </div>
    </div>

    <script>
        var botType = null;
        var usePeriod = null;
        var goodsOptSeqNo = 0;

        $(document).ready(function(){
            $('.event_popup_container').show();

            goodsOptSeqNo = '${basic_goods_opt_seq_no}';
            if (goodsOptSeqNo > 0) {
                let elt = $('.event_table table tbody').find('tr[data-goods_opt_seq_no=' + goodsOptSeqNo + ']');
                botType = elt.parent().attr('class');
                usePeriod = elt.find('.period').text().trim();
                elt.find('td:not(.event_msg)').addClass('on');

                if (botType === 'ronibot') {
                    let nm = ( usePeriod === '' ? '로니봇' : ( '로니봇(' + usePeriod + ')' ) );
                    $('.selected_bot').addClass('on').find('.bot_name').removeClass('paibot').addClass('ronibot').text(nm + ' 이용 중');
                }
                else {
                    let nm = ( usePeriod === '' ? '파이봇' : ( '파이봇(' + usePeriod + ')' ) ); 
                    $('.selected_bot').addClass('on').find('.bot_name').removeClass('ronibot').addClass('paibot').text(nm + ' 이용 중');
                }

                $('.next_step').text('닫기');
                $('.next_step').click(() => {
                    location.href = '/';
                });

            } else {
                $('.event_table td').click(function() {
                    botType = $(this).parents('tbody').attr('class');
                    usePeriod = $(this).parent().find('.period').text().trim();

                    $('.event_table tbody td').removeClass('on');

                    if (botType === 'ronibot') {
                        if ($('#goodsTypCd').val() == 'PYB') {
                            $('.selected_bot').addClass('on').find('.bot_name').removeClass('paibot').removeClass('ronibot').css('color','red')
                                              .text('서비스 정책으로 인해 해당 상품을 선택할 수 없습니다.');
                            botType = null;
                            return;
                        }
                        $('.selected_bot').addClass('on').find('.bot_name').css('color','')
                                          .removeClass('paibot').addClass('ronibot').text('로니봇(' + usePeriod + ') 선택완료');
                    }
                    else {
                        $('.selected_bot').addClass('on').find('.bot_name').css('color','')
                                          .removeClass('ronibot').addClass('paibot').text('파이봇(' + usePeriod + ') 선택완료');
                    }

                    $(this).parent().find('td:not(.event_msg)').addClass('on');
                    goodsOptSeqNo = $(this).parent().attr('data-goods_opt_seq_no');
                });

                $('.next_step').click(() => {
                    if (botType === null) {
                        alert("상품을 선택해 주세요.");
                        return;
                    }
                    $('.event_popup_container').hide();

                    $("#forced").val("N");
                    if (botType === 'ronibot') {
                        $("#evtGoodsType").val("RNB");
                    } else {
                        $("#evtGoodsType").val("PYB");
                    }
                    $("#evtOptSeqNo").val(goodsOptSeqNo);

                    pressedBtnBuyForPayment_52();
                });
            }

            $('.pop_close').click(function(){
                $('.event_popup_container').hide();
            });
        });

        var pressedBtnBuyForPayment_52 = () => {
            var type = $("#evtGoodsType").val();
            var seq = $("#evtOptSeqNo").val();
            if (type != null) {
                movePaymentView(type, seq, false);
            } else {
                alert("상품 유형이 선택되지 않았습니다.");
            }
        }
    </script>
</c:if>
<!-- 52 -->
<!-- //20201105 :: 이벤트 팝업 추가 -->