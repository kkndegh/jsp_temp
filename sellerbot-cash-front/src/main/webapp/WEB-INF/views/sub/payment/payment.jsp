<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="/assets/js/paymentForInicis.js?ver=20211116_01"></script>

<input type="hidden" id="DeviceType" value="${DeviceType}">
<input type="hidden" name="goodsTypCd" id="goodsTypCd" value="${goodsTypCd}">
<input type="hidden" name="goodsList" id="goodsList">
<input type="hidden" name="freeTrialYn" id="freeTrialYn" value="${freeTrialYn}">
<input type="hidden" name="forced" id="forced" value="${forced}">
<input type="hidden" name="forcedType" id="forcedType" value="${forcedType}">
<input type="hidden" name="isSelectedBNKB" id="isSelectedBNKB" value="${isSelectedBNKB}">
<input type="hidden" name="custPointReserv" id="custPointReserv" value="${custPointReserv}">
<input type="hidden" name="successYN" id="successYN" value="${successYN}">
<input type="hidden" name="errMsg" id="errMsg" value="${errMsg}">
<input type="hidden" name="rnbFnaPrc" id="rnbFnaPrc" value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}">
<input type="hidden" name="pybFnaPrc" id="pybFnaPrc" value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}">
<input type="hidden" name="rnbGoodsOptSeqNo" id="rnbGoodsOptSeqNo" value="${goodsInfo.basic_goods_info.RNB.GOODS_OPT_SEQ_NO}">
<input type="hidden" name="pybGoodsOptSeqNo" id="pybGoodsOptSeqNo" value="${goodsInfo.basic_goods_info.PYB.GOODS_OPT_SEQ_NO}">
<input type="hidden" name="rnbGoodsNm" id="rnbGoodsNm" value="${goodsInfo.basic_goods_info.RNB.GOODS_NM}">
<input type="hidden" name="pybGoodsNm" id="pybGoodsNm" value="${goodsInfo.basic_goods_info.PYB.GOODS_NM}">
<input type="hidden" name="totalPrice" id="totalPrice" value="0">
<input type="hidden" name="basic_goods_req_seq_no" id="basic_goods_req_seq_no" value="${basic_goods_req_seq_no}">
<input type="hidden" name="addn_goods_req_seq_no" id="addn_goods_req_seq_no" value="${addn_goods_req_seq_no}">
<input type="hidden" name="addn_goods_opt_seq_no" id="addn_goods_opt_seq_no" value="${addn_goods_opt_seq_no}">
<input type="hidden" name="cafe24_usr_yn" id="cafe24_usr_yn" value="${cafe24_usr_yn}">
<input type="hidden" name="isRegAcct" id="isRegAcct" value="${isRegAcct}">
<input type="hidden" name="eventNo" id="eventNo" value="${goodsInfo.eventNo}">

<div class="container">
    <div class="container_wrapper">
        <div class="my_wra">
            <!-- menu_top start -->
            <div class="menu_top">
                <p class="menu_name">이용요금 안내</p>
                <div class="gnb_topBox">
                    <ul class="gnb_top clearfix">
                        <li><a href="/sub/payment/product">이용권 소개</a></li>
                        <li class="focus"><a href="/sub/payment/payment">결제하기</a></li>
                    </ul>
                </div>
            </div>
            <!-- menu_top end -->
            <!-- buy_titles start -->
            <div class="buy_titles">
                <h1>결제하기</h1>
                <span></span>
            </div>
            <!-- buy_titles end -->
            <!-- section01 start -->
            <div id="section01">
                <!-- //20200706_10 :: 결제 화면 선택 상품 표시 Start -->
                <div id="ticket_types2">
                    <div class="ticket_type_header">
                        <h1>선택 요금제</h1>
                    </div>
                    <div class="ticket_type_body">
                        <c:if test="${cafe24_usr_yn eq 'Y'}">
                            <c:if test="${forced eq 'Y'}">
                                <c:if test="${basic_goods_req_seq_no > 0 && addn_goods_req_seq_no > 0}">
                                    <!-- 상품 Start -->
                                <c:choose>
                                <c:when test="${goodsTypCd eq 'FRB'}">
                                    <div class="ticket_type_box freebot" id="frbArea">
                                </c:when>
                                <c:otherwise>
                                    <div class="ticket_type_box freebot" id="frbArea" style="display: none;">
                                </c:otherwise>
                                </c:choose>
                                        <h2><span>프리봇</span></h2>
                                        <div class="btn_line">
                                            <div class="current_selected_ticket">
                                                <span class="freebot active"></span>
                                                <c:if test="${forcedType eq 'RNB'}">
                                                    <span class="ronibot"></span>
                                                </c:if>
                                                <c:if test="${forcedType eq 'PYB'}">
                                                    <span class="paibot"></span>
                                                </c:if>
                                            </div>
                                            <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                        </div>
                                    </div>
                                    <c:choose>
                                        <c:when test="${goodsTypCd eq 'RNB'}">
                                            <div class="ticket_type_box ronibot" id="rnbArea">
                                        </c:when>
                                        <c:otherwise>
                                            <div class="ticket_type_box ronibot" id="rnbArea" style="display: none;">
                                        </c:otherwise>
                                    </c:choose>
                                        <h2><span id="roni">로니봇</span></h2>
                                        <h3>
                                            <c:if test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                                <c:if test="${freeTrialYn eq 'N'}">무료체험 종료 후 </c:if>매월 <strong><fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" /> 원</strong><span class="vat">(VAT 별도)</span>
                                            </c:if>
                                            <c:if test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'N'}">
                                                ${goodsInfo.basic_goods_info.RNB.GOODS_DESC}                                                
                                            </c:if>
                                        </h3>
                                        <div class="btn_line">
                                            <div class="current_selected_ticket">
                                                <!-- <span class="freebot"></span> -->
                                                <span class="ronibot active"></span>
                                                <!-- <span class="paibot"></span> -->
                                            </div>
                                            <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                        </div>
                                    </div>
                                    <!-- 상품 Start -->
                                    <c:choose>
                                        <c:when test="${goodsTypCd eq 'PYB'}">
                                            <div class="ticket_type_box paibot" id="pybArea">
                                        </c:when>
                                        <c:otherwise>
                                            <div class="ticket_type_box paibot" id="pybArea" style="display: none;">
                                        </c:otherwise>
                                    </c:choose>
                                        <h2><span id="pai">파이봇</span></h2>
                                        <h3>
                                            <c:if test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                                <c:if test="${freeTrialYn eq 'N'}">무료체험 종료 후 </c:if>매월 <strong><fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" /> 원</strong><span class="vat">(VAT 별도)</span>
                                            </c:if>
                                            <c:if test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'N'}">
                                                ${goodsInfo.basic_goods_info.PYB.GOODS_DESC}                                                
                                            </c:if>
                                        </h3>
                                        <div class="btn_line">
                                            <div class="current_selected_ticket">
                                                <span class="freebot"></span>
                                                <c:if test="${forcedType eq 'RNB'}">
                                                    <span class="ronibot"></span>
                                                </c:if>
                                                <c:if test="${forcedType eq 'PYB'}">
                                                    <span class="paibot active"></span>
                                                </c:if>
                                            </div>
                                            <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                        </div>
                                    </div>
                                    <!-- 상품 End -->
                                    <!-- 상품 Start -->
                                    <div class="ticket_type_box bankbot">
                                        <h2><span id="bank">뱅크봇</span></h2>
                                        <div class="btn_line">
                                            <select id="selectBox_bankbot" disabled>
                                                <option>선택안함</option>
                                                <c:forEach var="addn" items="${goodsInfo.addn_goods}" varStatus="status">
                                                    <c:choose>
                                                        <c:when test="${addn.PERIOD eq 12}">
                                                            <option value="${addn.GOODS_OPT_SEQ_NO}" data-seq="${addn.GOODS_OPT_SEQ_NO}" data-prc="${addn.FNA_PRC}" data-period="${addn.PERIOD}"><fmt:formatNumber value="${addn.FNA_PRC}" pattern="#,###" />원/년</option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${addn.GOODS_OPT_SEQ_NO}" data-seq="${addn.GOODS_OPT_SEQ_NO}" data-prc="${addn.FNA_PRC}" data-period="${addn.PERIOD}"><fmt:formatNumber value="${addn.FNA_PRC}" pattern="#,###" />원/월</option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- 상품 End -->
                                </c:if>
                                <c:if test="${empty basic_goods_req_seq_no}">
                                    <!-- 상품 Start -->
                                    <c:choose>
                                        <c:when test="${goodsTypCd eq 'FRB'}">
                                            <div class="ticket_type_box freebot" id="frbArea">
                                        </c:when>
                                        <c:otherwise>
                                            <div class="ticket_type_box freebot" id="frbArea" style="display: none;">
                                        </c:otherwise>
                                    </c:choose>
                                        <h2><span>프리봇</span></h2>
                                        <div class="btn_line">
                                            <div class="current_selected_ticket">
                                                <span class="freebot active"></span>
                                                <c:if test="${forcedType eq 'RNB'}">
                                                    <span class="ronibot"></span>
                                                </c:if>
                                                <c:if test="${forcedType eq 'PYB'}">
                                                    <span class="paibot"></span>
                                                </c:if>
                                            </div>
                                            <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                        </div>
                                    </div>
                                    <c:choose>
                                        <c:when test="${goodsTypCd eq 'RNB'}">
                                            <div class="ticket_type_box ronibot" id="rnbArea">
                                        </c:when>
                                        <c:otherwise>
                                            <div class="ticket_type_box ronibot" id="rnbArea" style="display: none;">
                                        </c:otherwise>
                                    </c:choose>
                                        <h2><span id="roni">로니봇</span></h2>
                                        <h3>
                                            <c:if test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                                <c:if test="${freeTrialYn eq 'N'}">무료체험 종료 후 </c:if>매월 <strong><fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" /> 원</strong><span class="vat">(VAT 별도)</span>
                                            </c:if>
                                            <c:if test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'N'}">
                                                ${goodsInfo.basic_goods_info.RNB.GOODS_DESC}                                                
                                            </c:if>
                                        </h3>
                                        <div class="btn_line">
                                            <div class="current_selected_ticket">
                                                <span class="freebot"></span>
                                                <span class="ronibot active"></span>
                                                <span class="paibot"></span>
                                            </div>
                                            <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                        </div>
                                    </div>
                                    <!-- 상품 Start -->
                                    <c:choose>
                                        <c:when test="${goodsTypCd eq 'PYB'}">
                                            <div class="ticket_type_box paibot" id="pybArea">
                                        </c:when>
                                        <c:otherwise>
                                            <div class="ticket_type_box paibot" id="pybArea" style="display: none;">
                                        </c:otherwise>
                                    </c:choose>
                                        <h2><span id="pai">파이봇</span></h2>
                                        <h3>
                                            <c:if test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                                <c:if test="${freeTrialYn eq 'N'}">무료체험 종료 후 </c:if>매월 <strong><fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" /> 원</strong><span class="vat">(VAT 별도)</span>
                                            </c:if>
                                            <c:if test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'N'}">
                                                ${goodsInfo.basic_goods_info.PYB.GOODS_DESC}                                                
                                            </c:if>
                                        </h3>
                                        <div class="btn_line">
                                            <div class="current_selected_ticket">
                                                <c:if test="${empty addn_goods_req_seq_no}">
                                                    <span class="freebot"></span>
                                                </c:if>
                                                <c:if test="${forcedType eq 'RNB'}">
                                                    <span class="ronibot"></span>
                                                </c:if>
                                                <c:if test="${forcedType eq 'PYB'}">
                                                    <span class="paibot active"></span>
                                                </c:if>
                                            </div>
                                            <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                        </div>
                                    </div>
                                    <!-- 상품 End -->
                                </c:if>
                                <c:if test="${empty addn_goods_req_seq_no}">
                                    <!-- 상품 Start -->
                                    <div class="ticket_type_box bankbot">
                                        <h2><span id="bank">뱅크봇</span></h2>
                                        <div class="btn_line">
                                            <select id="selectBox_bankbot">
                                                <option>선택안함</option>
                                                <c:forEach var="addn" items="${goodsInfo.addn_goods}" varStatus="status">
                                                    <c:choose>
                                                        <c:when test="${addn.PERIOD eq 12}">
                                                            <option value="${status.index}" data-seq="${addn.GOODS_OPT_SEQ_NO}" data-prc="${addn.FNA_PRC}" data-period="${addn.PERIOD}"><fmt:formatNumber value="${addn.FNA_PRC}" pattern="#,###" />원/년</option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${status.index}" data-seq="${addn.GOODS_OPT_SEQ_NO}" data-prc="${addn.FNA_PRC}" data-period="${addn.PERIOD}"><fmt:formatNumber value="${addn.FNA_PRC}" pattern="#,###" />원/월</option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- 상품 End -->
                                </c:if>
                            </c:if>
                            <c:if test="${forced eq 'N'}">
                                <!-- 상품 Start -->
                                <c:choose>
                                    <c:when test="${goodsTypCd eq 'FRB'}">
                                        <div class="ticket_type_box freebot" id="frbArea">
                                    </c:when>
                                    <c:otherwise>
                                        <div class="ticket_type_box freebot" id="frbArea" style="display: none;">
                                    </c:otherwise>
                                </c:choose>
                                    <h2><span>프리봇</span></h2>
                                    <div class="btn_line">
                                        <div class="current_selected_ticket">
                                            <span class="freebot active"></span>
                                            <span class="ronibot"></span>
                                            <span class="paibot"></span>
                                        </div>
                                        <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                    </div>
                                </div>
                                <c:choose>
                                    <c:when test="${goodsTypCd eq 'RNB'}">
                                        <div class="ticket_type_box ronibot" id="rnbArea">
                                    </c:when>
                                    <c:otherwise>
                                        <div class="ticket_type_box ronibot" id="rnbArea" style="display: none;">
                                    </c:otherwise>
                                </c:choose>
                                    <h2><span id="roni">로니봇</span></h2>
                                    <h3>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                                <c:if test="${freeTrialYn eq 'N'}">무료체험 종료 후 </c:if>매월 <strong><fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" /> 원</strong><span class="vat">(VAT 별도)</span>
                                            </c:when>
                                            <c:otherwise>
                                                ${selectedGoods.GOODS_DESC}
                                            </c:otherwise>
                                        </c:choose>
                                    </h3>
                                    <div class="btn_line">
                                        <div class="current_selected_ticket">
                                            <c:if test="${empty addn_goods_req_seq_no}">
                                                <span class="freebot"></span>
                                            </c:if>
                                            <span class="ronibot active"></span>
                                            <span class="paibot"></span>
                                        </div>
                                        <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                    </div>
                                </div>
                                <!-- 상품 Start -->
                                <c:choose>
                                    <c:when test="${goodsTypCd eq 'PYB'}">
                                        <div class="ticket_type_box paibot" id="pybArea">
                                    </c:when>
                                    <c:otherwise>
                                        <div class="ticket_type_box paibot" id="pybArea" style="display: none;">
                                    </c:otherwise>
                                </c:choose>
                                    <h2><span id="pai">파이봇</span></h2>
                                    <h3>
                                        <c:choose>
                                            <c:when test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                                <c:if test="${freeTrialYn eq 'N'}">무료체험 종료 후 </c:if>매월 <strong><fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" /> 원</strong><span class="vat">(VAT 별도)</span>
                                            </c:when>
                                            <c:otherwise>
                                                ${selectedGoods.GOODS_DESC}
                                            </c:otherwise>
                                        </c:choose>
                                    </h3>
                                    <div class="btn_line">
                                        <div class="current_selected_ticket">
                                            <c:if test="${empty addn_goods_req_seq_no}">
                                                <span class="freebot"></span>
                                            </c:if>
                                            <span class="ronibot"></span>
                                            <span class="paibot active"></span>
                                        </div>
                                        <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                    </div>
                                </div>
                                <!-- 상품 End -->
                                <c:if test="${empty addn_goods_req_seq_no}">
                                    <!-- 상품 Start -->
                                    <div class="ticket_type_box bankbot">
                                        <h2><span id="bank">뱅크봇</span></h2>
                                        <div class="btn_line">
                                            <select id="selectBox_bankbot">
                                                <option>선택안함</option>
                                                <c:forEach var="addn" items="${goodsInfo.addn_goods}" varStatus="status">
                                                    <c:choose>
                                                        <c:when test="${addn.PERIOD eq 12}">
                                                            <option value="${status.index}" data-seq="${addn.GOODS_OPT_SEQ_NO}" data-prc="${addn.FNA_PRC}" data-period="${addn.PERIOD}"><fmt:formatNumber value="${addn.FNA_PRC}" pattern="#,###" />원/년</option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${status.index}" data-seq="${addn.GOODS_OPT_SEQ_NO}" data-prc="${addn.FNA_PRC}" data-period="${addn.PERIOD}"><fmt:formatNumber value="${addn.FNA_PRC}" pattern="#,###" />원/월</option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- 상품 End -->
                                </c:if>
                            </c:if>
                        </c:if>

                        <c:if test="${cafe24_usr_yn eq 'N'}">
                            <c:choose>
                            <c:when test="${goodsTypCd eq 'RNB'}">
                                <div class="ticket_type_box ronibot" id="rnbArea">
                            </c:when>
                            <c:otherwise>
                                <div class="ticket_type_box ronibot" id="rnbArea" style="display: none;">
                            </c:otherwise>
                            </c:choose>
                            <h2><span id="roni">로니봇</span></h2>
                            <h3>
                            <c:choose>
                                <c:when test="${goodsInfo.basic_goods_info.RNB.DEF_YN eq 'Y'}">
                                    <c:if test="${freeTrialYn eq 'N'}">무료체험 종료 후 </c:if> 매월 <strong><fmt:formatNumber value="${goodsInfo.basic_goods_info.RNB.FNA_PRC}" pattern="#,###" /> 원</strong><span class="vat">(VAT 별도)</span>
                                </c:when>
                                <c:otherwise>
                                    <span id="roni_desc">${selectedGoods.GOODS_DESC}</span>
                                </c:otherwise>
                            </c:choose>
                            </h3>
                            <div class="btn_line">
                                <div class="current_selected_ticket">
                                    <!--span class="freebot"></span-->
                                    <span class="ronibot active"></span>
                                    <span class="paibot"></span>
                                </div>
                                <c:choose>
                                <c:when test="${goodsInfo.eventNo == 52}">
                                    <select id="selectBox_ronibot">
                                        <c:forEach var="goods" items="${goodsInfo.basic_goods}" varStatus="status">
                                            <c:if test="${goods.GOODS_TYP_CD eq 'RNB'}">
                                                <option value="${status.index}" ${goods.GOODS_OPT_SEQ_NO == basic_goods_opt_seq_no ? 'selected':''} data-seq="${goods.GOODS_OPT_SEQ_NO}" data-prc="${goods.FNA_PRC}" data-period="${goods.PERIOD}" ><fmt:formatNumber value="${goods.FNA_PRC}" pattern="#,###" />원/${goods.PERIOD}개월</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </c:when>
                                <c:otherwise>
                                    <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                </c:otherwise>
                                </c:choose>
                            </div>
                            </div>
                                <c:choose>
                                    <c:when test="${goodsTypCd eq 'PYB'}">
                                        <div class="ticket_type_box paibot" id="pybArea">
                                    </c:when>
                                    <c:otherwise>
                                        <div class="ticket_type_box paibot" id="pybArea" style="display: none;">
                                    </c:otherwise>
                                </c:choose>
                                <h2><span id="pai">파이봇</span></h2>
                                <h3>
                                    <c:choose>
                                    <c:when test="${goodsInfo.basic_goods_info.PYB.DEF_YN eq 'Y'}">
                                        <c:if test="${freeTrialYn eq 'N'}">무료체험 종료 후 </c:if>매월 <strong><fmt:formatNumber value="${goodsInfo.basic_goods_info.PYB.FNA_PRC}" pattern="#,###" /> 원</strong><span class="vat">(VAT 별도)</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span id="pai_desc">${selectedGoods.GOODS_DESC}</span>
                                    </c:otherwise>
                                    </c:choose>
                                </h3>
                                <div class="btn_line">
                                    <div class="current_selected_ticket">
                                        <!--span class="freebot"></span-->
                                        <span class="ronibot"></span>
                                        <span class="paibot active"></span>
                                    </div>
                                    <c:choose>
                                    <c:when test="${goodsInfo.eventNo == 52}">
                                        <select id="selectBox_paibot">
                                            <c:forEach var="goods" items="${goodsInfo.basic_goods}" varStatus="status">
                                                <c:if test="${goods.GOODS_TYP_CD eq 'PYB'}">
                                                    <option value="${status.index}" data-seq="${goods.GOODS_OPT_SEQ_NO}" data-prc="${goods.FNA_PRC}" data-period="${goods.PERIOD}" ${goods.GOODS_OPT_SEQ_NO == basic_goods_opt_seq_no ? 'selected':''} ><fmt:formatNumber value="${goods.FNA_PRC}" pattern="#,###" />원/${goods.PERIOD}개월</option>
                                                </c:if>
                                            </c:forEach>
                                        </select>
                                    </c:when>
                                    <c:otherwise>
                                        <button type="button" id="change_ticket" onclick="pressedBtnChangeTicket();">변경하기</button>
                                    </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <!-- 상품 End -->
                        </c:if>

                    </div>

                    <div class="ticket_type_footer">
                        <div id="ticket_total_price">
                            <span class="total_price_label">총 결제금액</span>
                            <span class="total_price" id="total_price">0원</span>
                        </div>
                    </div>
                </div>
                <!-- //20200706_10 :: 결제 화면 선택 상품 표시 End -->
            </div>
            <!-- section01 end -->
            <!-- section02 start -->
            <div id="section02">
                <!--
                <input type="checkbox" class="checkbox" id="all"> <label for="all"></label>
                <h1 id="check_title">전체 선택</h1>
                -->
                <div id="checked">
                    <div class="checked_row">
                        <input type="checkbox" class="checkbox" id="agree1"> <label for="agree1"></label>
                        <h1>셀러봇캐시 <span class="blue_active" id="agree_script_div">유료 서비스 약관</span>에 동의합니다.</h1>
                        <!-- <h1>셀러봇캐시 <span class="blue_active" onclick="pressedBtnTerm();">유료 서비스 약관</span>에 동의합니다.</h1>
                    </div>
                    <div class="checked_row">
                        <input type="checkbox" class="checkbox" id="agree2"> <label for="agree2"></label>
                        <h1>결제정보를 <span class="blue_active" onclick="pressedBtnService();">결제 서비스 업체</span>에 제공을 동의합니다.</h1> -->
                    </div>
                </div>
            </div>
            <!-- section02 end -->
            <!-- alert_agrees_div start -->
            <div id="alert_agrees_div">
                <h1 id="disagree_text">
                    결제 서비스 진행을 위해 약관 동의가 필수입니다.
                </h1>
                <!-- 무료 체험시 -->
                <!-- <h2 id="payed_title">무료체험 종료 후 사용할 결제 수단을 선택하세요</h2> -->
                <!-- 일반 결제 -->
                <!-- <h2 id="nomal_payed_title">사용할 결제 수단을 선택하세요</h2> -->
                <h2 id="payed_title">
                    <c:choose>
                        <c:when test="${freeTrialYn eq 'Y'}">
                            사용할 결제 수단을 선택하세요
                        </c:when>
                        <c:otherwise>
                            무료체험 종료 후 사용할 결제 수단을 선택하세요
                        </c:otherwise>
                    </c:choose>
                </h2>
            </div>
            <!-- alert_agrees_div end --> 
            <!-- text_align_center start -->
            <div class="text_align_center">
                <button type="button" id="payed_card">신용카드 <span>결제</span></button>
                <button type="button" id="next_step">다음</button>
            </div>
            <!-- text_align_center end -->
             <!-- 신용카드 결제 수단을 선택하지 않고 다음을 눌렀을 경우 -->
            <!-- <h1 id="disagree_text2">
                    결제 수단을 선택해주세요
            </h1> -->
        </div>
    </div>
    <div class="agree_text_popup_wrap">
        <div class="display_none_back">
            <div id="title">
                <div class="add_pooup_close">
                <div></div>
                <div></div>
            </div>
                <h1>셀러봇캐시 유료 서비스 약관</h1>
            </div>
            <h1>
                제 1장 총칙
                <br><br>
                제 1 조 (목적)<br>
                이 약관은 ㈜온리원(이하 “회사”)이 제공하는 셀러봇캐시 서비스 (이하 “서비스”) 내에서 제공되는 셀러봇캐시 유료서비스 (이하 “유료서비스”) 의 이용과 관련하여 회사와 회원의 권리, 의무 및 책임사항, 기타 필요한 사항을 규정함을 목적으로 합니다.<br>
                <br>
                제2조 (이용약관의 효력 및 변경)<br>
                1. 본 약관은 회원이 서비스 내에서 유료 서비스를 이용하고자 하는 경우, 본 약관에 별도로 동의한 회원에게 적용됩니다.<br>
                2. 회사는 합리적인 사유가 발생될 경우에는 이 약관을 변경할 수 있으며, 약관을 변경한 경우에는 지체없이 이를 사전에 공지합니다.<br>
                3. 이 약관에 동의하는 것은 정기적으로 웹을 방문하여 약관의 변경사항을 확인하는 것에 동의함을 의미합니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 회원의 피해는 회사에서 책임지지 않습니다.<br>
                4. 회원은 변경된 약관에 동의하지 않을 경우 회원탈퇴(해지)를 요청할 수 있으며 변경된 약관의 효력 발생일로부터 7일 이후에도 거부의사를 표시하지 아니하고 서비스를 계속 사용할 경우 약관의 변경사항에 동의한 것으로 간주됩니다.<br>
                <br>
                제3조 (약관의 해석)<br>
                이 약관에서 정하지 아니한 사항과 이 약관의 해석에 관하여는 ‘콘텐츠진흥법’, ‘전자상거래등에서의소비자보호에관한법률’, ‘약관의규제에관한법률’, 문화체육관광부장관이 정하는 ‘콘텐츠이용자보호지침’, 기타 관계법령, ‘셀러봇캐시 이용약관’ 또는 상관례에 따릅니다.<br>
                <br>
                제4조 (용어의 정의)<br>
                1. 이 약관에서 사용하는 용어의 정의는 다음과 같습니다.<br>
                <br>
                ① '유료회원'이라 함은 회사의 서비스 이용을 신청한 이용고객으로서 유료 결제를 신청하고 회사가 정한 결제 절차를 완료한 자를 말합니다.<br>
                ② '환불'이라 함은 결제자가 회사가 지정한 환불 가능 기간내에, 유료 서비스 이용을 철회한 경우 회사의 환불 정책에 따라 결제자에게 환불 가능한 금액을 지불한 수단으로 돌려주는 행위를 말합니다.<br>
                ③ '결제'라 함은 회사의 특화된 유료서비스를 이용하기 위하여 회원이 일정 금액을 지불 하는 행위를 말합니다.<br>
                ④ ‘실결제금액’이라 함은 쿠폰이나 포인트가 아닌 현금, 신용카드 등 회사가 현금으로 정산받을 수 있는 결제수단을 통해 결제한 금액을 말합니다.<br>
                <br>
                2. 이 약관에서 사용하는 용어의 정의는 제1항에서 정하는 것을 제외하고는 관계법령 및 서비스별 안내에서 정하는 바에 의합니다.<br>
                <br>
                제5조 (회원에 대한 통지)<br>
                1. 회사가 회원에 대한 통지를 하는 경우 이 약관에 별도 규정이 없는 한 서비스 내 전자우편주소, 전자쪽지 등으로 할 수 있습니다.<br>
                2. 회사는 회원 전체에 대한 통지의 경우 7일 이상 서비스의 게시판에 게시함으로써 제1항의 통지에 갈음할 수 있습니다. 다만, 회원 본인의 거래와 관련하여 중대한 영향을 미치는 사항에 대하여는 제1항의 통지를 합니다.<br>
                <br>
                제6조 (유료서비스의 내용 등의 게시)<br>
                1. 회사는 다음 사항을 해당 셀러봇캐시 유료서비스의 이용 초기화면이나 FAQ 등에 회원이 알기 쉽게 표시합니다.<br>
                ① 셀러봇캐시 유료서비스의 명칭 또는 제호<br>
                ② 셀러봇캐시 유료서비스 제공자의 성명(법인인 경우에는 법인의 명칭), 주소, 전화번호<br>
                ③ 셀러봇캐시 유료서비스의 내용, 이용방법, 이용료, 기타 이용조건<br>
                2. 회사는 유료서비스를 제공함에 있어 유료서비스의 대금 환불의 조건 및 절차에 관한 사항을 FAQ 등을 통해 제공합니다.<br>
                <br>
                제7조 (이용계약의 성립 등)<br>
                1. 회원은 회사가 제공하는 다음 또는 이와 유사한 절차에 의하여 이용신청을 합니다. 회사는 계약 체결 전에 각 호의 사항에 관하여 회원이 정확하게 이해하고 실수 또는 착오 없이 거래할 수 있도록 정보를 제공합니다.<br>
                ① 유료서비스의 확인 및 선택<br>
                ② 결제방법의 선택 및 결제정보의 입력<br>
                ③ 유료서비스의 이용신청에 관한 확인 또는 회사의 확인에 대한 동의<br>
                2. 회사는 회원의 이용신청이 다음 각 호에 해당하는 경우에는 승낙하지 않거나 승낙을 유보할 수 있습니다.<br>
                ① 실명이 아니거나 타인의 명의를 이용한 경우<br>
                ② 허위의 정보를 기재하거나, 회사가 제시하는 내용을 기재하지 않은 경우<br>
                ③ 미성년자가 ‘청소년보호법’ 등 관련 법령에 의해서 이용이 금지되는 유료서비스를 이용하고자 하는 경우<br>
                ④ 서비스 관련 설비의 여유가 없거나, 기술상 또는 업무상 문제가 있는 경우<br>
                3. 신용불량자로 등록된 회원이나 대금 지급을 연체하는 회원에 대하여 유료 서비스 이용을 제한할 수 있습니다.<br>
                4. 이용계약의 성립시기는 ‘결제완료’ 또는 ‘구매완료’를 신청절차 상에서 표시한 시점으로 합니다.<br>
                5. 회원이 유료서비스를 이용하기 위해서는 이 약관에 동의 후 각 서비스에 따른 이용조건에 따라 이용요금을 지급하여야 합니다.<br>
                <br>
                제 8 조 (유료서비스의 이용관련)<br>
                <br>
                1. 당사 서비스의 중대한 하자에 의하여 서비스의 기능을 상실하였거나 불량했을 경우 장애시간에 따른 사용기간 연장 혹은 해당 기능 복원으로 조치를 받을 수 있습니다. 단, 사용기간 연장 시 최대 15일까지만 연장이 가능하며 다음 각 호에 해당하는 경우는 제외합니다.<br>
                ① 천재지변 또는 이에 준하는 불가항력의 상태가 있는 경우<br>
                ② 서비스의 효율적인 제공을 위한 시스템 개선, 정기점검, 장비 증설 등 계획된 서비스 중지 일정을 제 5 조 각 항을 통해 사전에 공지한 경우<br>
                ③ 그 외 회사의 고의 과실이 없는 사유로 인한 경우<br>
                ④ 회사에게 회선, 통신망, 전용선을 제공하고 있는 이동통신사 또는 부가통신사업자 측의 장애·귀책사유로 인한 서비스의 불완전 또는 불능으로 회원 또는 제3자에게 손해가 야기된 경우<br>
                ⑤ 판매몰 계정 정보나 계좌 정보, 연락처 등 회원이 입력해야하는 정보를 제대로 입력하지 않거나 최신화하지 않아서 발생한 조회 오류<br>
                ⑥ 쇼핑몰 사이트 내의 회원정보와 셀러봇캐시 내의 회원정보의 사업자정보가 서로 불일치하여 발생한 조회 오류<br>
                ⑦ 그 외 회원의 귀책사유로 서비스 이용에 장애가 있는 경우<br>
                ⑧ 쇼핑몰 사이트의 구조적 변경 (페이지, 소스코드, API 변경 등)이 발생했거나, 쇼핑몰의 점검으로 인해 데이터 수집에 근본적인 문제가 발생한 경우<br>
                ⑨ 금융사 (은행 등) 측에서 계좌 조회 서비스 오류가 나타났거나 서비스 중단한 경우<br>
                <br>
                2. 당사 유료서비스 이용 시 셀러봇캐시 이용약관에 위반한 행위를 하여 일시 사용정지되는 경우, 해당 회원의 사용정지기간이 끝나면 다시 사용할 수 있습니다.<br>
                <br>
                <br>
                제9조 (유료서비스의 중단 및 변경)<br>
                1. 회사는 사업 종목의 전환, 사업의 포기, 업체 간의 통합 등의 이유로 셀러봇캐시 유료서비스를 제공할 수 없게 되는 경우에는 회사는 이 약관에서 정한 방법으로 회원에게 통지하고 제8조에 따라 조치를 취합니다.<br>
                <br>
                2. 회사는 상당한 이유가 있는 경우에 운영상, 기술상의 필요에 따라 제공하고 있는 전부 또는 일부의 셀러봇캐시 유료서비스를 변경할 수 있고, 변경 전 해당 서비스 초기 화면에 관련 사항을 게시합니다. 다만, 변경된 내용이 중대하거나 회원에게 불리한 경우에는 이 약관에서 정한 방법으로 통지하고, 중대하거나 회원에게 불리한 변경 내용에 동의하지 않는 회원은 회원탈퇴(해지)를 요청할 수 있습니다.<br>
                <br>
                제10조 (유료서비스의 환불)<br>
                1. 회원은 다음 각 호의 사유가 있으면 적법한 절차를 거친 후 회원 자신이 직접 유료 서비스 이용을 하기 위해 소비한 금액을 본 조 2항과 같이 회사로부터 환불받을 수 있습니다.<br>
                ① 유료 서비스 신청을 했으나 사용할 수 있는 서비스가 전무하며 그에 대한 책임이 전적으로 당사에 있을 경우 (단, 시스템 정기점검 등 불가피한 경우를 제외)<br>
                ② 유료 서비스 신청을 하여 사용료를 결제 하였으나, 유료서비스가 3일 이내에 실시되지 않아 회원이 환불을 요청한 경우<br>
                ③ 결제 후 14일 이내 환불을 신청한 경우<br>
                ④ 기타 소비자 보호를 위하여 당사에서 따로 정하는 경우<br>
                <br>
                2. 회사는 결제일 이후 지난 기간에 따라 환불 수수료 및 위약금 등을 고려하여 해지 시 환불 금액을 조정할 수 있습니다.<br>
                ① 결제 당일 해지 시 실결제금액의 100% 환불 <br>
                ② 결제 후 14일 이내 해지 시 실결제금액의 50% 환불<br>
                ③ 결제 후 14일 이후 (15일째부터) 해지 시 실결제금액의 0% 환불 (환불 불가능)<br>
                <br>
                3. 단, 회원이 유료서비스를 무료/무상으로 취득하거나 포인트를 통해 이용권을 결제하는 등 회원이 직접 현금 등을 통해 결제하여 비용을 지불하지 아니한 서비스에 대하여서는 회사가 환불 의무를 부담하지 않습니다.<br> 
                <br>
                4. 서비스 환불 시 적립금 처리 관련하여 다음과 같이 정의합니다.<br>
                ① 환불 시 해당 상품 구매로 인해 회사가 부여한 적립금은 회수합니다.<br>
                ② 결제 시 사용한 적립금은 환불이 불가합니다.<br>
                ③ 적립금은 현금으로 환불받을 수 없습니다.<br>
                <br>
                5. 환불절차는 다음과 같습니다.<br>
                ① 10조 1항 각 호에 의한 이유로 환불을 원하는 회원은 유료서비스 신청을 한 후 14일 이내에 회사로 연락하여 환불을 신청해야 하며, 당사는 환불 신청이 정당함을 심사한 후, 정당한 이유가 있음으로 판명된 회원에게 환불합니다. 유료 서비스 신청 후 14일이 지난 후 (15일째부터) 에는 환불신청을 하여도 환불을 받으실 수 없습니다.<br>
                <br>
                제 2 장 기타<br>
                <br>
                제 1 조 (세금계산서 관련)<br>
                <br>
                1. 회사는 실시간 계좌이체와 가상계좌로 결제한 이용금액에 대하여 매월 말일 기준으로 합계 액에 대한 세금계산서를 발행합니다.<br>
                <br>
                2. 회원은 세금계산서의 발행을 위해 회원의 사업자정보를 서비스 화면을 통해 입력해야 합니다. (마이페이지 -> 회원정보)<br>
                <br>
                제 2 조 (적립금)<br>
                <br>
                1. 적립 후 사용하지 않고 유효기간이 경과된 적립금은 유효기간 경과분에 한해 기간 만료일 다음날 자동 소멸됩니다.<br>
                ① 적립금의 유효기간은 적립된 시점으로부터 1년(365일)입니다.<br>
                <br>
                제 3 조 (무료체험)<br>
                <br>
                1. 회사는 회원이 유료서비스를 결제하기 전 일정 기간동안 유료서비스를 체험 사용할 수 있는 ‘무료체험’을 제공할 경우, 회원은 아래의 절차와 규정을 준수할 것을 본 약관을 통해 동의한 후 신청할 수 있습니다.<br>
                ① 사업자번호 당 1회 신청 가능<br>
                ② 월 단위 정기결제 수단을 미리 등록하여 체험 종료 후 이용권을 매월 정기결제<br>
                ③ 체험 종료 전 이용권을 해지할 수 있으며, 정기결제 이후 해지할 경우 제 10 조 2항에 따라 환불 진행<br>
                <br>
                제 4 조 (기타)<br>
                <br>
                1. 미납금액이 있는 회원이 탈퇴신청 할 경우 미납액을 해결하기 전에 탈퇴가 불가능하며, 회사는 미납액에 대해 탈퇴를 신청한 회원에게 청구할 권리와 의무가 있습니다.<br>
                <br>
                2. 회사는 당 서비스와 관련하여 회원이 제기하는 정당한 의견이나 불만을 반영하여 우선적으로 그 사항을 처리합니다. 다만, 신속한 처리가 곤란한 경우에는 회원에게 그 사유와 처리일정을 즉시 통보해 드립니다. 회사와 회원간에 발생한 분쟁은 전자거래기본법 제28조 및 동 시행령 제 15조에 의하여 설치된 전자거래분쟁조정위원회의 조정에 따를 수 있습니다.<br>
                <br>
                ■ 부칙<br>
                <br>
                이 약관은 2020년 5월 14일부터 시행합니다.<br>
                <br>
            </h1>
            <button type="button" id="check_script"
                class="btn_confirm_terms join_step_popup_close">약관확인</button>
        </div>
        <div id="bg"></div>
    </div>
</div>
<div>
<c:choose>
    <c:when test="${DeviceType eq 'PC'}">
        <c:choose>
            <c:when test="${mid eq 'INIBillTst'}">
                <script language='javascript' type='text/javascript' src='https://stgstdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>
            </c:when>
            <c:otherwise>
                <script language='javascript' type='text/javascript' src='https://stdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>
            </c:otherwise>
        </c:choose>
        <form id="SendPayForm_id" method="POST" >
            <input type="hidden" name="version" value="1.0" >
            <input type="hidden" name="mid" value="${mid}">
            <input type="hidden" name="goodname">
            <input type="hidden" name="oid">
            <input type="hidden" name="price">
            <input type="hidden" name="currency" value="WON">
            <input type="hidden" name="buyername" value=${buyername}>
            <input type="hidden" name="buyertel" value="${buyertel}">
            <input type="hidden" name="buyeremail" value="${buyeremail}">
            <input type="hidden" name="timestamp">
            <input type="hidden" name="signature">
            <input type="hidden" name="returnUrl" value="${siteDomain}/sub/payment/payresponse">
            <input type="hidden" name="closeUrl" value="${siteDomain}/sub/payment/close">
            <input type="hidden" name="mKey" value="${mKey}">
            <input type="hidden" name="gopaymethod" value="">
            <input type="hidden" name="acceptmethod" value="BILLAUTH(card):FULLVERIFY"> 
            <input type="hidden" name="merchantData"> 
            <input type="hidden" name="offerPeriod" value="M2">
        </form>
    </c:when>
    <c:otherwise>
        <form id="SendPayForm_id" name="SendPayForm_id" method="POST" action="https://inilite.inicis.com/inibill/inibill_card.jsp">
            <input type="hidden" name="mid" value="${mid}">
            <input type="hidden" name="buyername" value=${buyername}>            
            <input type="hidden" name="goodname">
            <input type="hidden" name="price">
            <input type="hidden" name="orderid">
            <input type="hidden" name="returnurl" value="${siteDomain}/sub/payment/mobilepayresponse">            
            <input type="hidden" name="timestamp">
            <input type="hidden" name="period" value="M2">
            <input type="hidden" name="p_noti">
            <input type="hidden" name="hashdata">
            <input type="hidden" name="buyername" value=${buyername}>
            <input type="hidden" name="buyertel" value="${buyertel}">
            <input type="hidden" name="buyeremail" value="${buyeremail}">
        </form>
    </c:otherwise>
</c:choose>
</div>
<script>
    // document.querySelectorAll("#checked input").forEach(function (item, idx) {
    //     item.addEventListener('change', function () {
    //         // if (document.querySelectorAll("#checked input")[0].checked == true && document.querySelectorAll("#checked input")[1].checked == true) {
    //         if (document.querySelectorAll("#checked input")[0].checked == true) {
    //             document.querySelector("#agree1").checked = true;
    //         } else {
    //             document.querySelector("#agree1").checked = false;
    //         }
    //     });
    // });

    document.querySelector("#agree1").addEventListener("change", function () {
        if (document.querySelector("#agree1").checked == true) {
            // document.querySelectorAll("#checked input").forEach(function (item) {
            //     item.checked = true;
            // });

            $("#disagree_text").css("display", "none");
        } else {
            // document.querySelectorAll("#checked input").forEach(function (item) {
            //     item.checked = false;
            // });

            $("#disagree_text").css("display", "block");
        }
    });
</script>
<script>
    $("#payed_card").click(function () {
        if ($("#agree1").is(":checked") == true) {
            $(this).css("border", "2px solid #2DABE8");
            $("#next_step").css("background","#2DABE8");
            $("#payed_title").css("display", "none");
            $("#nomal_payed_title").css("display", "none");
        }    
        else{
            $("#disagree_text").css("display", "block");
        }
    });

    $("#next_step").click(function() {
        paymentForInicis();
    });

    $("#selectBox_bankbot").change( function() {
        changeBankbotGoods();
    });
</script>
<script>
    $("#agree_script_div").click(function () {
        $(".agree_text_popup_wrap").css("display", "flex");
        $("html,body").css("overflowY","hidden");
    });

    $("#check_script").click(function () {
        $(".agree_text_popup_wrap").css("display", "none");
        $("html,body").css("overflowY","visible");
    });

    $(".agree_text_popup_wrap div.display_none_back #title .add_pooup_close").click(function() {
        $(".agree_text_popup_wrap").css("display", "none");
        $("html,body").css("overflowY","visible");
    })
</script>
<style>
    #next_step {
        background: #888888;
        border-radius: 3px;
        color: #fff;
        width: 10rem;
        height: 2.8rem;
        margin-top: 3rem;
        margin-bottom: 3rem;
    }

    .my_wra {
        padding-top: 5em;
        min-height: calc(100vh - 5.625em);
        overflow-x: hidden;
        margin-bottom: 3rem;
    }
</style>

<c:if test="${goodsInfo.eventNo == 0}">
    <script>
        $(document).ready(function () {
            if($("#successYN").val() == "N") {
                showAlert("결제 요청이 실패하였습니다.<br>다시 시도해주세요.<br>(" + $("#errMsg").val() + ")");
            }
            initPayment();
        });
    </script>
</c:if>

<c:if test="${goodsInfo.eventNo == 52}">
    <script>
        $(document).ready(function () {
            if($("#successYN").val() == "N") {
                showAlert("결제 요청이 실패하였습니다.<br>다시 시도해주세요.<br>(" + $("#errMsg").val() + ")");
            }
            updateSelectedGoodsInfo_52();
        });

        $(".freebot").click(() => {
            return false;
        });

        $(".ronibot").click(() => {
            if ($("#forced").val() === "Y" && $("#forcedType").val() === "PYB") {
                showAlert('서비스 정책으로 인해 상품을 변경할 수 없습니다.');
            } else {
                selectBasicGoods("RNB");
            }
            return false;
        });

        $(".paibot").click(() => {
            if ($("#forced").val() === "Y" && $("#forcedType").val() === "RNB") {
                showAlert('서비스 정책으로 인해 상품을 변경할 수 없습니다.');
            } else {
                selectBasicGoods("PYB");
            }
            return false;
        });

        let selectBasicGoods = (type) => {
            switch (type) {
                case "RNB":
                    $("#goodsTypCd").val(type);
                    $("#rnbArea").css("display", "block");            
                    $("#pybArea").css("display", "none");
                    break;
                case "PYB":
                    $("#goodsTypCd").val(type);
                    $("#rnbArea").css("display", "none");
                    $("#pybArea").css("display", "block");
                    break;
                default:
                    break;
            }
            updateSelectedGoodsInfo_52();
        }

        $("#selectBox_ronibot").click(() => {
            return false;
        });

        $("#selectBox_paibot").click(() => {
            return false;
        });

        $("#selectBox_ronibot").change(() => {
            updateSelectedGoodsInfo_52();
            return false;
        });

        $("#selectBox_paibot").change(() => {
            updateSelectedGoodsInfo_52();
            return false;
        });

        var updateSelectedGoodsInfo_52 = function() {
            let total = 0;
            let typeCd = $("#goodsTypCd").val();
            let goodsList = [];
            let elt = $("#selectBox_ronibot");
            let nm = $("#rnbGoodsNm").val();

            if (typeCd == "PYB") {
                elt = $("#selectBox_paibot");                
                nm = $("#pybGoodsNm").val();
            }

            let info = getGoodsBySelect(elt);
            if (info != null) {
                let price = info[0];
                let period = info[1];
                nm = nm + " " + period + "개월";

                goodsList.push({
                    p: price * 1.1 ,
                    o: info[2],
                    n: encodeURI(nm),
                    d: period
                });
                total += price;
                //console.log('>> price = ' + price);
                let desc = "정기결제 - " + period + "개월마다 " + fnAddComma(price) +"(VAT별도)";
                if (typeCd == "RNB")
                    $("#roni_desc").text(desc);
                else if (typeCd == "PYB")
                    $("#pai_desc").text(desc);
            }

            $("#totalPrice").val(total);
            $("#total_price").html(fnAddComma(total) + '원');

            $('#SendPayForm_id [name="goodname"]').val("셀로봇이용권(" + nm + ")");
            $('#SendPayForm_id [name="price"]').val(total + (total * 0.1));
            $('#goodsList').val(JSON.stringify(goodsList));
        };

        var getGoodsBySelect = (e) => {
            let opt = e.find("option:selected");
            let price = opt.data("prc");
            if (typeof(price) == 'number' && price > 0) {
                return [price, opt.data("period"), opt.data("seq")];
            }
        };
    </script>
</c:if>
