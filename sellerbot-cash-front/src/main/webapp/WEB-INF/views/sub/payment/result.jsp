<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />

<link rel="stylesheet" href="/assets/css/ticket.css">

<div class="container">
    <div class="container_wrapper">
        <div class="my_wra">
            <!-- menu_top start -->
            <div class="menu_top">
                <p class="menu_name">이용요금 안내</p>
                <div class="gnb_topBox">
                    <ul class="gnb_top clearfix">
                        <li><a href="/sub/payment/product">이용권 안내</a></li>
                        <li class="focus"><a href="/sub/payment/payment">결제하기</a></li>
                    </ul>
                </div>
            </div>
            <!-- menu_top end -->
            <!-- buy_titles2 start -->
            <div class="buy_titles2">
                <h1>이용해주셔서 감사합니다.</h1>
                <span></span>
            </div>
            <c:forEach var="product" items="${products}" varStatus="status">
                <div id="section01">
                    <div id="ticket_types">
                        <div class="result">
                            <div class="header">
                                <c:if test="${product.goodsTyp eq 'RNB'}">
                                    <div class="ribons" id="y_ticket"></div>
                                </c:if>
                                <c:if test="${product.goodsTyp eq 'PYB'}">
                                    <div class="ribons" id="blue_ticket"></div>
                                </c:if>                                
                            </div>
                            <div class="body">
                                <h2>요금제 - <span id="roni">${product.name}</span></h2>
                                <h2>
                                    정기결제 - ${product.duration}개월마다 <fmt:formatNumber value="${product.price}" pattern="#,###" />원(VAT포함)
                                </h2>
                                <div class="footer"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <h1 id="thanks_text">
                    셀러봇캐시(${product.name}) 요금제 이용이 시작되었습니다. <br>
                    <c:if test="${freeYN == 'Y'}">
                        ${dateUtilBean.getDateTimeKrFormatYMD(product.nextBillDate)} 전에 멤버쉽을 해지하시면 요금이 청구되지 않습니다. <br>
                    </c:if>
                    <c:if test="${!empty product.nextBillDate}">
                        다음 정기결제일은 ${dateUtilBean.getDateTimeKrFormatYMD(product.nextBillDate)}입니다.
                    </c:if>                    
                </h1>
            </c:forEach>

            <h2 id="payed_title2">결제 정보</h2>
            <div class="text_align_center">
                <div id="card_info">
                    <div id="card_icons"></div>
                    <h1>신용카드 <span>${cardNum}</span></h1>
                </div>
            </div>
            <div class="text_align_center" style="max-width: 30rem;">
                <button type="button" id="next_step" style="background: #2DABE8;display: inline-block;">메인으로 가기</button>
                <button type="button" id="btnPaymentInfo" style="display: inline-block;">결제정보</button>
            </div>
        </div>
    </div>
</div>
<style>
    .my_wra {
        margin-bottom:7rem;
    }
</style>
<script>
    $(document).ready(function () {
    });

    $("#next_step").click(function () {
        location.replace("/");
    });

    $("#btnPaymentInfo").click(function () {
        location.replace("/sub/my/paidInfo");
    });
</script>
