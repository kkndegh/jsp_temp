<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />

<!--container_start-->
<div class="container">
	<div class="peer_wrap">
		<div class="menu_top">
			<p class="menu_name">동종업계 매출추이</p>
			<ul class="gnb_top clearfix">
				<li><a href="/sub/peersale/state">나의 매출 랭킹</a></li>
				<li><a href="/sub/peersale/state_change">매출 랭킹 변동추이</a></li>
				<li class="focus"><a href="/sub/peersale/rank_top">분야별 랭킹 Top10</a></li>
			</ul>
		</div>
		<div class="peer_title_top">
			<div>조회하실 분야를 선택하세요</div>
		</div>
		<ul class="peer_rank_top_sel">
			<li class="peer_rank_top_sel_all peer_rank_top_sel_on" data-goods_cate_seq_no="all">
				<h1 class="peer_rank_top_sel_text">전체</h1>
			</li>
			<c:forEach items="${categoryList }" var="category">
				<li data-goods_cate_seq_no="${category.goods_cate_seq_no }">
					<div class="peer_rank_top_sel_flexBox">
						<div class="peer_rank_top_sel_flexBox_inner">
							<img class="peer_rank_top_sel_img"
								src="/assets/images/peersale/category_img_${category.goods_cate_seq_no }.png"
								alt="${category.cate_nm }" style="width: 100%; height: 100%;">
							<h1 class="peer_rank_top_sel_text">${category.cate_nm }</h1>
						</div>
					</div>
				</li>
			</c:forEach>
		</ul>

		<div class="peer_rank">
			<div class="peer_rank_title">
				<h1>${dateUtilBean.parseKrDateString(lastMonth)}</h1> <span class="peer_product_category"><span
						class="itmeName">전체</span> 분야 랭킹Top10 </span>
				<h3>셀러들의 전체 분야 합산결과 매출랭킹 입니다</h3>
			</div>
			<div class="peer_rank_title_plus">
				'<p class="ca_data"></p>'+'<p class="ca_data_2"></p>'<span class="peer_product_category">매출랭킹Top10
				</span>
				<h3>셀러들의 전체 분야 합산결과 매출랭킹 입니다</h3>
			</div>
			<ul class="peer_rank_list">
				<c:forEach items="${rankList }" var="rank" varStatus="status">
					<c:if test="${status.count <= 3 }">
						<li class="peer_rank_list_rank rank_${rank.sales_rank }st">
							<h1>${rank.sales_rank }위</h1>
							<h2 class="peer_rank_pr">
								<fmt:formatNumber value="${rank.sales_prc}" minFractionDigits="0" pattern="#,###" />
							</h2><span>원</span>
							<div class="pointer_rank">
								<h4>${rank.sales_rank }</h4>
							</div>
						</li>
					</c:if>
				</c:forEach>
				<c:if test="${fn:length(rankList) < 1 }">
					<li class="peer_rank_list_rank rank_1st peer_rank_list_rank_nodata">
						<h1>1위</h1>
						<h2 class="peer_rank_pr">데이터가 없습니다.</h2>
						<div class="pointer_rank pointer_rank_2">
							<h4>2</h4>
						</div>
					</li>
				</c:if>

				<c:if test="${fn:length(rankList) < 2 }">
					<li class="peer_rank_list_rank rank_2st peer_rank_list_rank_nodata">
						<h1>2위</h1>
						<h2 class="peer_rank_pr">데이터가 없습니다.</h2>
						<div class="pointer_rank pointer_rank_2">
							<h4>2</h4>
						</div>
					</li>
				</c:if>

				<c:if test="${fn:length(rankList) < 3 }">
					<li class="peer_rank_list_rank rank_3st peer_rank_list_rank_nodata">
						<h1>3위</h1>
						<h2 class="peer_rank_pr">데이터가 없습니다.</h2>
						<div class="pointer_rank pointer_rank_2">
							<h4>3</h4>
						</div>
					</li>
				</c:if>


			</ul>
			<div class="peer_mon_sel">
				<div class="peer_mon_sel_prev">
					<span>1</span>월
				</div>
				<div class="peer_mon_sel_state" for="datepicker-value">
					<span class="peer_mon_sel_year">2019</span>
					<h1>년</h1>
					<span class="peer_mon_sel_mon">03</span>
					<h1>월</h1>
				</div>
				<div class="peer_mon_sel_next"><span>3</span>월 ></div>
				<input type="hidden" name="datepicker-value" value="${lastMonth }">
				<div class="datepicker-cell datepicker-center" id="datepicker-month" style="margin-top: -1px;"></div>

			</div>
			<div class="peer_rank_board">
				<div class="peer_rank_board_wrap">
					<ul class="peer_rank_board_title">
						<li>분야순위</li>
						<li>매출액</li>
						<li>판매 수량</li>
						<li>전월 대비</li>
						<li>전월 대비(%)</li>
					</ul>
					<c:forEach items="${rankList }" var="rank" varStatus="status">
						<c:if test="${status.count == 11 }">
							<div class="state_ellipsis"></div>
						</c:if>
						<ul
							class="peer_rank_board_con ${rank.self_rank_yn eq 'Y' ? 'peer_rank_board_con_border peer_rank_board_con_my' : '' }">
							<li>${rank.sales_rank }위</li>
							<li data-sales_prc="${rank.sales_prc}">
								<fmt:formatNumber value="${rank.sales_prc}" pattern="#,###" />
							</li>
							<li>
								<fmt:formatNumber value="${rank.sales_num}" pattern="#,###" />
							</li>
							<li class="peer_rank_more">
								<fmt:formatNumber value="${rank.sales_prc_MOM}" pattern="#,###" />
							</li>
							<li>
								<fmt:formatNumber value="${rank.sales_prc_rto_MOM}" pattern="#,###" />
							</li>
							<li class="pointer_my_rank">내 위치<div class="tri_my_rank"></div>
							</li>
						</ul>
					</c:forEach>

				</div>
			</div>
		</div>

	</div>
	<!--account_wra_end -->
	<div class="banner_payment" style="display: none;">
		<div class="area_banner_payment">
			<div class="bf_hand_tlt">
				<img src="/assets/images/payment/question_Icon.png" alt="">
				<p><b>미리 정산받을 수 있는 금액</b><br>
					내가 지원받을 수 있는 금액
				</p>
			</div>
			<div class="price_bfhand">
			</div>
			<a href="/sub/pre_calculate/pre_calculate" class="more_bfhand">자세히보기</a>
		</div>
	</div>
</div>
<!--container_end-->

<script>

	'use strict';
	var _category = [];
	$(document).ready(function () {

		var defaultDate = moment($("[name=datepicker-value]").val(), "YYYY-MM").toDate();
		var _picker = new tui.DatePicker('#datepicker-month', {
			date: defaultDate,
			language: 'ko',
			type: 'month',
			input: {
				element: '[name="datepicker-value"]',
				format: 'yyyy-MM'
			},
			selectableRanges: [[new Date(1900, 1, 1), defaultDate]]
		});

		_picker.on("change", function () {
			setSearchDate($(".peer_mon_sel"));
			getRankList();
		});

		// 4.10.2 월별 카테고리별 매출 순위 목록 요청
		function getRankList() {
			var month = $("[name=datepicker-value]").val();
			var param = {
				"sales_dt": month,
				"top_count": 10
			};

			if (_category.length > 0) {
				param.goods_cate_seq_no = _category.join(",");
			};

			$.get("/sub/peersale/category/rank", $.param(param), function (res) {
				var target = $(".peer_rank");

				// 제목 날짜 변경
				target.find(".peer_rank_title h1").text(moment(month, "YYYY-MM").format("YYYY년 MM월"));

				var rankList = target.find(".peer_rank_list");
				var rankHtml = [];
				// 순위 변경
				for (var index = 0; index < 3; index++) {
					var data = res[index];
					if (nonNull(data)) {
						rankHtml.push("<li class=\"peer_rank_list_rank rank_" + (index + 1) + "st\">");
						rankHtml.push("	<h1>" + data.sales_rank + "위</h1>");
						rankHtml.push("	<h2 class=\"peer_rank_pr\">" + fnAddComma(data.sales_prc) + "</h2><span>원</span>");
						rankHtml.push("	<div class=\"pointer_rank\">");
						rankHtml.push("		<h4>" + nvl(data.sales_rank, "") + "</h4>");
						rankHtml.push("	</div>");
						rankHtml.push("</li>");
					} else {
						rankHtml.push("<li class=\"peer_rank_list_rank rank_" + (index + 1) + "st peer_rank_list_rank_nodata\">");
						rankHtml.push("	<h1>" + (index + 1) + "위</h1>");
						rankHtml.push("	<h2 class=\"peer_rank_pr\">데이터가 없습니다.</h2>");
						rankHtml.push("	<div class=\"pointer_rank\">");
						rankHtml.push("		<h4>" + (index + 1) + "</h4>");
						rankHtml.push("	</div>");
						rankHtml.push("</li>");
					}
				}

				rankList.empty();
				rankList.html(rankHtml.join(""));

				// 테이블 처리
				var board = target.find(".peer_rank_board");
				var title = board.find(".peer_rank_board_title").clone();
				var tbHtml = [];
				$.each(res, function (index) {
					// ... 표시
					if (index == 10) { tbHtml.push("<div class=\"state_ellipsis\"></div>"); }

					// 내용
					var ulClass = "peer_rank_board_con";
					ulClass += index == 0 ? " peer_rank_board_con_border" : "";
					ulClass += this.self_rank_yn == 'Y' ? " peer_rank_board_con_my" : "";
					tbHtml.push("<ul class=\"" + ulClass + "\">");
					tbHtml.push("  <li>" + this.sales_rank + "위</li>");
					tbHtml.push("  <li data-sales_prc=" + this.sales_prc + ">" + fnAddComma(this.sales_prc) + "</li>");
					tbHtml.push("  <li>" + fnAddComma(this.sales_num) + "</li>");
					tbHtml.push("  <li class=\"peer_rank_more\">" + fnAddComma(this.sales_prc_MOM) + "</li>");
					tbHtml.push("  <li>" + Math.round(nvl(this.sales_prc_rto_MOM, "")) + "</li>");
					tbHtml.push("  <li class=\"pointer_my_rank\">내 위치<div class=\"tri_my_rank\"></div>");
					tbHtml.push("  </li>");
					tbHtml.push("</ul>");

				});
				board.empty();

				if (tbHtml.length == 0) {
					tbHtml.push("<div class=\"peer_nodata_wrapper peer_nodata_wrapper_type_2\">");
					tbHtml.push("	<div class=\"grid_noData\">");
					tbHtml.push("		<h1>데이터가 없습니다.</h1>");
					tbHtml.push("	</div>");
					tbHtml.push("</div>");
				} else {
					board.append(title);
				}

				board.append(title);
				board.append(tbHtml.join(""));
			});
		}

		// 날짜 셋팅
		function setSearchDate(jqObj) {
			var lastDay = moment(new Date()).subtract(1, 'month').format("YYYY-MM");
			var month = $("[name=datepicker-value]").val();
			var mMonth = moment($("[name=datepicker-value]").val(), "YYYY-MM");
			_picker.setDate(mMonth.toDate());
			jqObj.find(".peer_mon_sel_year").text(mMonth.format("YYYY"));
			jqObj.find(".peer_mon_sel_mon").text(mMonth.format("MM"));
			jqObj.find(".peer_mon_sel_prev span").text(mMonth.subtract(1, 'month').format("MM"));
			jqObj.find(".peer_mon_sel_next span").text(mMonth.add(2, 'month').format("MM"));

			if (lastDay == month) {
				jqObj.find(".peer_mon_sel_next").addClass("peer_btn_disable");
			} else {
				jqObj.find(".peer_mon_sel_next").removeClass("peer_btn_disable");
			}
		}

		setSearchDate($(".peer_mon_sel"));
		// 월 변경
		// 전달
		$(".peer_mon_sel_prev").on("click", function () {
			if ($(this).hasClass("peer_btn_disable")) { return false; }
			var jqObj = $("[name=datepicker-value]");
			var month = jqObj.val();
			jqObj.val(moment(month, "YYYY-MM").subtract(1, 'month').format("YYYY-MM"));

			setSearchDate($(".peer_mon_sel"));
			getRankList();
		});
		// 다음달
		$(".peer_mon_sel_next").on("click", function () {
			if ($(this).hasClass("peer_btn_disable")) { return false; }
			var jqObj = $("[name=datepicker-value]");
			var month = jqObj.val();
			jqObj.val(moment(month, "YYYY-MM").add(1, 'month').format("YYYY-MM"));
			setSearchDate($(".peer_mon_sel"));
			getRankList();
		});
		// 달력
		$("[for=datepicker-value]").on("click", function () {
			$("[name=datepicker-value]").click();
		});

		// 전체
		$(".peer_rank_top_sel_all").click(function () {
			$(".peer_rank_top_sel li").removeClass("peer_rank_top_sel_on");
			$(".peer_rank_top_sel_all").addClass("peer_rank_top_sel_on");
			_category = [];
			fnCategoryView(_category);
			getRankList();
		});

		// 카테고리 세부항목 선택 최대 2개 까지
		$(".peer_rank_top_sel li:not(.peer_rank_top_sel_all)").click(function () {
			var jqThis = $(this);
			if (jqThis.hasClass("peer_rank_top_sel_on")) {
				_category = fnArrayRemove(_category, Number(jqThis.data("goods_cate_seq_no")));
			} else {
				if (_category.length < 2) {
					_category.push(jqThis.data("goods_cate_seq_no"));
				} else {
					_category.splice(0, 1);
					_category.push(jqThis.data("goods_cate_seq_no"));
				}
			}
			fnCategoryView(_category);
			getRankList();
		});

		function fnCategoryView(category) {
			$("li.peer_rank_top_sel_on").removeClass("peer_rank_top_sel_on");
			if (category.length == 0) {
				$(".peer_rank_top_sel_all").addClass("peer_rank_top_sel_on");
			} else {
				for (var i in category) {
					$("li[data-goods_cate_seq_no=" + category[i] + "]").addClass("peer_rank_top_sel_on");
				}
			}

			var title = [];
			$("li.peer_rank_top_sel_on").each(function () {
				title.push($(this).text())
			});
			$(".itmeName").text(title.join("+"));
		}

		function fnArrayRemove(arr, val) {
			var result = [];
			var target = 9999;
			for (var i in arr) {
				if (arr[i] != val) {
					result.push(arr[i]);
				}
			}

			return result;
		}

		// 20230830 미리정산가능금액
		$.ajax({
			url: '/sub/finGood/preSett'
			, type: 'get'
			, async: false
			, success: function (res) {
				$(".banner_payment").attr('style', 'display: block;')
				$(".price_bfhand").html("<p>최대 <b>" + formatNumber(res) + "</b>원</p>");
			}
			, error: function (request, status, error) {
				console.error("미리정산가능금액 가져오기 실패");
			}
		});
	});


</script>