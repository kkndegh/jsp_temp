<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script src="/assets/amchart/core.js"></script>
<script src="/assets/amchart/charts.js"></script>
<script src="/assets/amchart/animated.js"></script>
<script src="/assets/amchart/lang/ko_KR.js"></script>

<!--container_start-->
<div class="container">
	<div class="peer_wrap">
		<div class="menu_top">
			<p class="menu_name">동종업계 매출추이</p>
			<ul class="gnb_top clearfix">
				<li><a href="/sub/peersale/state">나의 매출 랭킹</a></li>
				<li class="focus"><a href="/sub/peersale/state_change">매출 랭킹 변동추이</a></li>
				<li><a href="/sub/peersale/rank_top">분야별 랭킹 Top10</a></li>
			</ul>
		</div>
		<div class="peer_title_top">
			<div>매출 랭킹 변동추이</div>
		</div>

		<c:if test="${not empty myRankList }">
			<div class="peer_change_board_all">
				<div class="peer_change_board">
					<ul class="peer_change_board_wrap peer_change_board_title">
						<li>항목/월</li>
						<c:forEach items="${monthList }" var="month">
							<li>${month }</li>
						</c:forEach>
					</ul>
					<c:forEach items="${myRankList }" var="row">
						<ul class="peer_change_board_wrap peer_change_board_con">
							<c:forEach items="${row }" var="col" varStatus="status">
								<c:if test="${status.count == 1 }">
									<li>${col.cate_nm }</li>
								</c:if>
								<li>
									<c:choose>
										<c:when test="${empty col.rank}">
											<h1 class="peer_change_rank">-</h1>
											<h2 class="peer_change_pr">-</h2>
										</c:when>
										<c:otherwise>
											<h1 class="peer_change_rank">
												<fmt:formatNumber value="${col.rank }" minFractionDigits="0"
													pattern="#,###" />위</h1>
											<h2 class="peer_change_pr">
												<fmt:formatNumber value="${col.sales_prc }" minFractionDigits="0"
													pattern="#,###" />
											</h2>
										</c:otherwise>
									</c:choose>
								</li>
							</c:forEach>
						</ul>
					</c:forEach>
				</div>
			</div>
		</c:if>
		<c:if test="${empty myRankList }">
			<div class="peer_grid_noData_type_3">
				<div class="grid_noData">
					<h1>데이터가 없습니다.</h1>
				</div>
			</div>
		</c:if>

		<h1 class="peer_change_chart_title">카테고리별 매출 랭킹 변동 추이</h1>
		<!-- <h2 class="chart_top_pr">(단위:천 원)</h2> -->
		<div class="peer_change_chart" style="height: 700px;">
			<div id="my_legend" class="my_legend">
				<ul></ul>
			</div>
			<!--  p class="peer_change_chartCaption">(단위:천 원)</p -->
			<div id="chartdiv" style="width: 100%; height: 100%"></div>
		</div>

	</div>
	<!--account_wra_end -->
	<div class="banner_payment" style="display: none;">
		<div class="area_banner_payment">
			<div class="bf_hand_tlt">
				<img src="/assets/images/payment/question_Icon.png" alt="">
				<p><b>미리 정산받을 수 있는 금액</b><br>
					내가 지원받을 수 있는 금액
				</p>
			</div>
			<div class="price_bfhand">
			</div>
			<a href="/sub/pre_calculate/pre_calculate" class="more_bfhand">자세히보기</a>
		</div>
	</div>
</div>
<!--container_end-->



<script type="text/javascript">

	'use strict';

	var _jsonList = [];
	<c:if test="${not empty jsonMyRankList }">
		_jsonList = ${jsonMyRankList}
	</c:if>


		$(document).ready(function () {

			try {

				am4core.useTheme(am4themes_animated);
				//Themes end

				am4core.options.commercialLicense = true;

				//Create chart instance
				var chart = am4core.create("chartdiv", am4charts.XYChart);
				chart.language.locale = am4lang_ko_KR;
				chart.responsive.enabled = false;

				//Add data
				chart.data = generateChartData();

				//Create axes
				var dateAxisX = chart.xAxes.push(new am4charts.DateAxis());

				dateAxisX.baseInterval = { timeUnit: "month", count: 1 }
				dateAxisX.renderer.grid.template.location = 0;
				dateAxisX.renderer.minGridDistance = 1;
				dateAxisX.renderer.grid.template.disabled = false;
				dateAxisX.renderer.fullWidthTooltip = true;
				dateAxisX.dateFormatter = new am4core.DateFormatter();
				dateAxisX.dateFormats.setKey("month", "yyyy'-'MMM");
				dateAxisX.periodChangeDateFormats.setKey("month", "yyyy'-'MMM");

				var rankAxisY = chart.yAxes.push(new am4charts.ValueAxis());
				rankAxisY.renderer.inversed = true;
				rankAxisY.title.text = "순위";
				rankAxisY.renderer.grid.template.disabled = false;
				rankAxisY.renderer.opposite = false;
				rankAxisY.renderer.grid.template.location = 1;
				rankAxisY.numberFormatter.numberFormat = "#,###";
				rankAxisY.extraMin = 0;
				rankAxisY.extraMax = 0.5;

				// Create series
				$.each(_jsonList, function () {
					var json = this[0];
					var rankSeries = chart.series.push(new am4charts.LineSeries());
					rankSeries.dataFields.valueY = "rank" + json.goods_cate_seq_no;
					rankSeries.dataFields.prcY = "sales_prc" + json.goods_cate_seq_no;
					rankSeries.dataFields.dateX = "date";
					rankSeries.yAxis = rankAxisY;
					rankSeries.name = json.cate_nm;
					rankSeries.tooltipText = "{name}\n순위: {valueY.formatNumber('#,###')}\n금액: {prcY.formatNumber('#,###')}원 ";
					rankSeries.strokeWidth = 2;
					rankSeries.propertyFields.strokeDasharray = "dashLength";
					rankSeries.sequencedInterpolation = true;
					var bullet = rankSeries.bullets.push(new am4charts.CircleBullet());

				});

				//Add legend
				chart.legend = new am4charts.Legend();

				//Add cursor
				chart.cursor = new am4charts.XYCursor();
				chart.cursor.fullWidthLineX = true;
				chart.cursor.xAxis = dateAxisX;
				chart.cursor.lineX.strokeOpacity = 0;
				chart.cursor.lineX.fill = am4core.color("#000");
				chart.cursor.lineX.fillOpacity = 0.1;

				//generate data, quite different range
				function generateChartData() {
					var chartData = [];
					var categoryLenth = _jsonList.length;
					if (categoryLenth > 0) {
						var firstRow = _jsonList[0];
						for (var i in firstRow) {
							var json = {};
							for (var j in _jsonList) {
								var data = _jsonList[j][i];
								var cate_no = data.goods_cate_seq_no;
								json.date = moment(data.year_month + "01", "YYYYMMDD").toDate();
								json["sales_num" + cate_no] = data.sales_num;
								json["sales_prc" + cate_no] = data.sales_prc;
								json["rank" + cate_no] = data.rank;
							}
							chartData.push(json);
						}

					}
					return chartData;
				}
			} catch (e) {
				console.log(e);
			}

			// 20230830 미리정산가능금액
			setTimeout(function () {
				$.ajax({
					url: '/sub/finGood/preSett'
					, type: 'get'
					, async: false
					, success: function (res) {
						$(".banner_payment").attr('style', 'display: block;')
						$(".price_bfhand").html("<p>최대 <b>" + formatNumber(res) + "</b>원</p>");
					}
					, error: function (request, status, error) {
						console.error("미리정산가능금액 가져오기 실패");
					}
				});
			}, 2000);
		});


</script>

<style>
	#chartdiv {
		width: 100%;
		max-height: 600px;
		height: 100vh;
	}
</style>