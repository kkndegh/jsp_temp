<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />


<script src="/assets/amchart/core.js"></script>
<script src="/assets/amchart/charts.js"></script>
<script src="/assets/amchart/animated.js"></script>
<script src="/assets/amchart/lang/ko_KR.js"></script>

<!--container_start-->
<div class="container">
	<div class="peer_wrap">
		<div class="menu_top">
			<p class="menu_name">동종업계 매출추이</p>
			<ul class="gnb_top clearfix">
				<li class="focus"><a href="/sub/peersale/state">나의 매출 랭킹</a></li>
				<li><a href="/sub/peersale/state_change">매출 랭킹 변동추이</a></li>
				<li><a href="/sub/peersale/rank_top">분야별 랭킹 Top10</a></li>
			</ul>
		</div>
		<div class="peer_title_top">
			<div><span class="cl_name">
					<c:out value="${sessionScope.cust.biz_nm}" />고객님</span>이 판매중인 분야의 매출순위 입니다.</div>
		</div>
		<c:if test="${empty categoryList }">
			<div class="peer_nodata_wrapper_big">
				<div class="grid_noData">
					<h1>데이터가 없습니다.</h1>
				</div>
			</div>
		</c:if>

		<ul class="peer_product_list_wrap text-center">
			<c:forEach items="${categoryList }" var="category">
				<li title="${category.cate_nm }" for="${category.goods_cate_seq_no }">
					<div class="peer_product_list_cir peer_product_list_cir_${category.goods_cate_seq_no }"></div>
					<div class="peer_product_list_name">
						<h1>${category.cate_nm }</h1>
					</div>
					<div class="peer_product_list_rank">
						<h1 class="peer_product_list_rank_num">${category.sales_rank }</h1>위
					</div>
				</li>
			</c:forEach>
		</ul>
		<!--rank-->
		<div class="peer_rank_wrapper">

			<c:forEach items="${categoryList }" var="category">
				<div title="${category.cate_nm }" class="peer_rank" id="${category.goods_cate_seq_no }">
					<div class="peer_rank_title">
						<h1>${dateUtilBean.parseKrDateString(lastMonth)}</h1>
						<span class="peer_product_category"> ${category.cate_nm}분야 </span>
						<h2>매출금액</h2>
						<h3>셀러들의 전체 매출현황입니다.</h3>
					</div>
					<ul class="peer_rank_list">

						<c:forEach items="${category.rankList }" var="rank" varStatus="status">
							<c:if test="${status.count <= 3 }">
								<li class="peer_rank_list_rank rank_${rank.sales_rank }st">
									<h1>${rank.sales_rank }위</h1>
									<h2 class="peer_rank_pr">
										<fmt:formatNumber value="${rank.sales_prc}" minFractionDigits="0"
											pattern="#,###" />
									</h2><span>원</span>
									<div class="pointer_rank">
										<h4>${rank.sales_rank }</h4>
									</div>
								</li>
							</c:if>
						</c:forEach>

						<c:if test="${fn:length(category.rankList) < 1 }">
							<li class="peer_rank_list_rank rank_1st peer_rank_list_rank_nodata">
								<h1>1위$</h1>
								<h2 class="peer_rank_pr">데이터가 없습니다.</h2>
								<div class="pointer_rank pointer_rank_2">
									<h4>2</h4>
								</div>
							</li>
						</c:if>

						<c:if test="${fn:length(category.rankList) < 2 }">
							<li class="peer_rank_list_rank rank_2st peer_rank_list_rank_nodata">
								<h1>2위</h1>
								<h2 class="peer_rank_pr">데이터가 없습니다.</h2>
								<div class="pointer_rank pointer_rank_2">
									<h4>2</h4>
								</div>
							</li>
						</c:if>

						<c:if test="${fn:length(category.rankList) < 3 }">
							<li class="peer_rank_list_rank rank_3st peer_rank_list_rank_nodata">
								<h1>3위</h1>
								<h2 class="peer_rank_pr">데이터가 없습니다.</h2>
								<div class="pointer_rank pointer_rank_2">
									<h4>3</h4>
								</div>
							</li>
						</c:if>

					</ul>
					<div class="peer_mon_sel" data-month="${lastMonth }"
						data-goods_cate_seq_no="${category.goods_cate_seq_no }">
						<div class="peer_mon_sel_prev">
							<span>1</span>월
						</div>
						<div class="peer_mon_sel_state" for="datepicker-value_${category.goods_cate_seq_no }">
							<span class="peer_mon_sel_year">2019</span>
							<h1>년</h1>
							<span class="peer_mon_sel_mon">03</span>
							<h1>월</h1>
						</div>
						<div class="peer_mon_sel_next"><span>3</span>월 ></div>
						<input type="hidden" name="datepicker-value"
							id="datepicker-value_${category.goods_cate_seq_no }" value="${lastMonth }">
						<div class="datepicker-cell datepicker-center"
							id="datepicker-month_${category.goods_cate_seq_no }" style="margin-top: -1px;"></div>
					</div>
					<div class="peer_rank_board">
						<div class="peer_rank_board_wrap">
							<ul class="peer_rank_board_title">
								<li>분야순위</li>
								<li>매출액</li>
								<li>판매 수량</li>
								<li>전월 대비</li>
								<li>전월 대비(%)</li>
							</ul>
							<c:forEach items="${category.rankList }" var="rank" varStatus="status">
								<c:if test="${status.count == 11 }">
									<div class="state_ellipsis"></div>
								</c:if>
								<ul
									class="peer_rank_board_con ${rank.self_rank_yn eq 'Y' ? 'peer_rank_board_con_border peer_rank_board_con_my' : '' }">
									<li>${rank.sales_rank }위</li>
									<li data-sales_prc="${rank.sales_prc}">
										<fmt:formatNumber value="${rank.sales_prc}" pattern="#,###" />
									</li>
									<li>
										<fmt:formatNumber value="${rank.sales_num}" pattern="#,###" />
									</li>
									<li class="peer_rank_more">
										<fmt:formatNumber value="${rank.sales_prc_MOM}" pattern="#,###" />
									</li>
									<li>
										<fmt:formatNumber value="${rank.sales_prc_rto_MOM}" pattern="#,###" />
									</li>
									<li class="pointer_my_rank">내 위치<div class="tri_my_rank"></div>
									</li>
								</ul>
							</c:forEach>
						</div>
					</div>

					<div class="rank_pry_1">
						<div id="chartdiv_${category.goods_cate_seq_no}"
							data-goods_cate_seq_no="${category.goods_cate_seq_no}"
							style="min-height: 400px; width: 100%"></div>
					</div>
				</div>
			</c:forEach>


			<div class="peer_top_btn">
				<h1>TOP</h1>
			</div>
		</div>
	</div>
	<!--account_wra_end -->
	<div class="banner_payment" style="display: none;">
		<div class="area_banner_payment">
			<div class="bf_hand_tlt">
				<img src="/assets/images/payment/question_Icon.png" alt="">
				<p><b>미리 정산받을 수 있는 금액</b><br>
					내가 지원받을 수 있는 금액
				</p>
			</div>
			<div class="price_bfhand">
			</div>
			<a href="/sub/pre_calculate/pre_calculate" class="more_bfhand">자세히보기</a>
		</div>
	</div>
</div>
<!--container_end-->
<script>

	'use strict';
	$(document).ready(function () {
		// 차트 셋팅
		am4core.useTheme(am4themes_animated);
		am4core.options.commercialLicense = true;
		var _datepicker = {};

		// 4.10.2 월별 카테고리별 매출 순위 목록 요청
		function getRankList(goods_cate_seq_no, month) {

			var param = {
				"sales_dt": month,
				"goods_cate_seq_no": goods_cate_seq_no,
				"top_count": 10
			};

			$.get("/sub/peersale/category/rank", $.param(param), function (res) {
				var target = $(".peer_mon_sel[data-goods_cate_seq_no=" + goods_cate_seq_no + "]").parents(".peer_rank");

				// 제목 날짜 변경
				target.find(".peer_rank_title h1").text(moment(month, "YYYY-MM").format("YYYY년 MM월"));

				var rankList = target.find(".peer_rank_list");
				var rankHtml = [];
				// 순위 변경

				for (var index = 0; index < 3; index++) {
					var data = res[index];
					if (nonNull(data)) {
						rankHtml.push("<li class=\"peer_rank_list_rank rank_" + (index + 1) + "st\">");
						rankHtml.push("	<h1>" + data.sales_rank + "위</h1>");
						rankHtml.push("	<h2 class=\"peer_rank_pr\">" + fnAddComma(data.sales_prc) + "</h2><span>원</span>");
						rankHtml.push("	<div class=\"pointer_rank\">");
						rankHtml.push("		<h4>" + nvl(data.sales_rank, "") + "</h4>");
						rankHtml.push("	</div>");
						rankHtml.push("</li>");
					} else {
						rankHtml.push("<li class=\"peer_rank_list_rank rank_" + (index + 1) + "st peer_rank_list_rank_nodata\">");
						rankHtml.push("	<h1>" + (index + 1) + "위</h1>");
						rankHtml.push("	<h2 class=\"peer_rank_pr\">데이터가 없습니다.</h2>");
						rankHtml.push("	<div class=\"pointer_rank\">");
						rankHtml.push("		<h4>" + (index + 1) + "</h4>");
						rankHtml.push("	</div>");
						rankHtml.push("</li>");
					}
				}

				rankList.empty();
				rankList.html(rankHtml.join(""));

				// 테이블 처리
				var board = target.find(".peer_rank_board");
				var title = board.find(".peer_rank_board_title").clone();
				var tbHtml = [];
				$.each(res, function (index) {
					// ... 표시
					if (index == 10) { tbHtml.push("<div class=\"state_ellipsis\"></div>"); }
					// 내용
					var ulClass = "peer_rank_board_con";
					ulClass += index == 0 ? " peer_rank_board_con_border" : "";
					ulClass += this.self_rank_yn == 'Y' ? " peer_rank_board_con_my" : "";
					tbHtml.push("<ul class=\"" + ulClass + "\">");
					tbHtml.push("  <li>" + this.sales_rank + "위</li>");
					tbHtml.push("  <li data-sales_prc=" + this.sales_prc + ">" + fnAddComma(this.sales_prc) + "</li>");
					tbHtml.push("  <li>" + fnAddComma(this.sales_num) + "</li>");
					tbHtml.push("  <li class=\"peer_rank_more\">" + fnAddComma(this.sales_prc_MOM) + "</li>");
					tbHtml.push("  <li>" + nvl(this.sales_prc_rto_MOM, "") + "</li>");
					tbHtml.push("  <li class=\"pointer_my_rank\">내 위치<div class=\"tri_my_rank\"></div>");
					tbHtml.push("  </li>");
					tbHtml.push("</ul>");
				});

				board.empty();

				if (tbHtml.length == 0) {
					tbHtml.push("<div class=\"peer_nodata_wrapper peer_nodata_wrapper_type_2\">");
					tbHtml.push("	<div class=\"grid_noData\">");
					tbHtml.push("		<h1>데이터가 없습니다.</h1>");
					tbHtml.push("	</div>");
					tbHtml.push("</div>");
				} else {
					board.append(title);
				}

				board.append(tbHtml.join(""));

				getAreaList(goods_cate_seq_no, month);
			});
		}
		// 4.10.4 월별 카테고리 매출 분포 목록 요청
		function getAreaList(goods_cate_seq_no, month) {
			var param = {
				"sta_dt": month,
				"end_dt": month,
				"goods_cate_seq_no": goods_cate_seq_no
			};
			$.get("/sub/peersale/category/area", $.param(param), function (res) {
				var data = [];
				var row = {};
				var myPrc = $("#" + goods_cate_seq_no).find(".peer_rank_board_con_my [data-sales_prc]").data("sales_prc");

				if (res.thous_5_up > 0) {

					var json = {
						"name": "5,000만 이상",
						"value": res.thous_5_up
					};
					if (myPrc >= 50000000) {
						json.showTooltip = true;
						json.disabled = false;
					}
					data.push(json);
				}
				if (res.thous_2_up > 0) {
					var json = {
						"name": "2,000만 이상",
						"value": res.thous_2_up
					};
					if (myPrc >= 20000000 && myPrc < 50000000) {
						json.showTooltip = true;
						json.disabled = false;
					}
					data.push(json);
				}
				if (res.hund_5_up > 0) {
					var json = {
						"name": "500만 이상",
						"value": res.hund_5_up
					};
					if (myPrc >= 5000000 && myPrc < 20000000) {
						json.showTooltip = true;
						json.disabled = false;
					}
					data.push(json);
				}
				if (res.hund_4_up > 0) {
					var json = {
						"name": "400만 이상",
						"value": res.hund_4_up
					};
					if (myPrc >= 4000000 && myPrc < 5000000) {
						json.showTooltip = true;
						json.disabled = false;
					}
					data.push(json);
				}
				if (res.hund_3_up > 0) {
					var json = {
						"name": "300만 이상",
						"value": res.hund_3_up
					};
					if (myPrc >= 3000000 && myPrc < 4000000) {
						json.showTooltip = true;
						json.disabled = false;
					}
					data.push(json);
				}

				if (res.hund_2_up > 0) {
					var json = {
						"name": "200만 이상",
						"value": res.hund_2_up
					};
					if (myPrc >= 2000000 && myPrc < 3000000) {
						json.showTooltip = true;
						json.disabled = false;
					}
					data.push(json);
				}

				if (res.hund_2_down > 0) {
					var json = {
						"name": "200만 미만",
						"value": res.hund_2_down
					};
					if (myPrc < 2000000) {
						json.showTooltip = true;
						json.disabled = false;
					}
					data.push(json);
				}

				fnDrowChart(goods_cate_seq_no, data);
			});
		}

		// 날짜 셋팅
		function setSearchDate(jqObj) {
			var lastDay = moment(new Date()).subtract(1, 'month').format("YYYY-MM");
			var month = jqObj.data("month");
			var mMonth = moment(month, "YYYY-MM");
			jqObj.find(".peer_mon_sel_year").text(mMonth.format("YYYY"));
			jqObj.find(".peer_mon_sel_mon").text(mMonth.format("MM"));
			jqObj.find(".peer_mon_sel_prev span").text(mMonth.subtract(1, 'month').format("MM"));
			jqObj.find(".peer_mon_sel_next span").text(mMonth.add(2, 'month').format("MM"));

			if (lastDay == month) {
				jqObj.find(".peer_mon_sel_next").addClass("peer_btn_disable");
			} else {
				jqObj.find(".peer_mon_sel_next").removeClass("peer_btn_disable");
			}
		}

		// 피라미드 차트
		function fnDrowChart(goods_cate_seq_no, data) {
			var chartId = "chartdiv_" + goods_cate_seq_no;

			if (data.length == 0) {
				$("#" + chartId).hide();
			} else {
				$("#" + chartId).show();
			}
			var chart = am4core.create(chartId, am4charts.SlicedChart);
			chart.data = data;
			var series = chart.series.push(new am4charts.PyramidSeries());
			//series.colors.step = 1;
			series.dataFields.value = "value";
			series.dataFields.category = "name";
			series.alignLabels = true;
			series.labelsContainer.width = 200;
			series.labelsContainer.paddingLeft = 10;
			series.alwaysShowTooltip = "always";

			series.colors.list = [
				am4core.color("#F3EFF0"),
				am4core.color("#F8DFE3"),
				am4core.color("#F8CED2"),
				am4core.color("#FEAC97"),
				am4core.color("#FF6C6C"),
				am4core.color("#ED5A46")
			];

			// bullet
			var bullet = series.bullets.push(new am4charts.CircleBullet());
			bullet.disabled = true;
			bullet.propertyFields.disabled = "disabled";
			bullet.circle.stroke = am4core.color("#027736");
			bullet.circle.strokeWidth = 1;
			bullet.circle.horizontalCenter = "middle";

			// Tooltip
			bullet.tooltipText = "내 위치";
			bullet.propertyFields.alwaysShowTooltip = "showTooltip";
			series.tooltip.label.textAlign = "middle";
			series.tooltip.pointerOrientation = "down";
			series.tooltip.dy = -10;
			series.tooltip.propertyFields.pointerOrientation = "orientation";
			series.tooltip.propertyFields.dy = "offset";
			bullet.locationY = 0.5;

			series.tooltip.getFillFromObject = false;
			series.tooltip.background.fill = am4core.color("#CEB1BE");
			series.cursorTooltipEnabled = false;
			series.slices.template.tooltipText = "";

		}

		$(".peer_mon_sel").each(function () {
			var jqThis = $(this);
			var month = jqThis.data("month");
			var goods_cate_seq_no = jqThis.data("goods_cate_seq_no");
			setSearchDate(jqThis);
			getAreaList(goods_cate_seq_no, month);

			var picker = new tui.DatePicker('#datepicker-month_' + goods_cate_seq_no, {
				date: moment(month, "YYYY-MM").toDate(),
				language: 'ko',
				type: 'month',
				input: {
					element: '#datepicker-value_' + goods_cate_seq_no,
					format: 'yyyy-MM'
				},
				selectableRanges: [[new Date(1900, 1, 1), moment(month, "YYYY-MM").toDate()]]
			});

			picker.on("change", function () {
				var month = jqThis.find("[name=datepicker-value]").val();
				jqThis.data("month", month);
				setSearchDate(jqThis);
				getRankList(goods_cate_seq_no, month);
			});

			_datepicker[goods_cate_seq_no] = picker;

		});

		// 월 변경
		// 전달
		$(".peer_mon_sel_prev").on("click", function () {
			if ($(this).hasClass("peer_btn_disable")) { return false; }

			var jqObj = $(this).parents(".peer_mon_sel");
			var month = moment(jqObj.data("month"), "YYYY-MM").subtract(1, 'month').format("YYYY-MM");
			var goods_cate_seq_no = jqObj.data("goods_cate_seq_no");
			jqObj.data("month", month);
			setSearchDate(jqObj);
			getRankList(goods_cate_seq_no, month);
			_datepicker[goods_cate_seq_no].setDate(moment(month, "YYYY-MM").toDate());
		});

		// 다음달
		$(".peer_mon_sel_next").on("click", function () {
			if ($(this).hasClass("peer_btn_disable")) { return false; }
			var jqObj = $(this).parents(".peer_mon_sel");
			var month = moment(jqObj.data("month"), "YYYY-MM").add(1, 'month').format("YYYY-MM");
			var goods_cate_seq_no = jqObj.data("goods_cate_seq_no");
			jqObj.data("month", month);
			setSearchDate(jqObj);
			getRankList(goods_cate_seq_no, month);
			_datepicker[goods_cate_seq_no].setDate(moment(month, "YYYY-MM").toDate());
		});

		// 달력
		$(".peer_mon_sel_state").on("click", function () {
			$("#" + $(this).attr("for")).click();
		});

		// 디지인 스크립트
		$(".peer_product_list_wrap li").click(function () {
			$(this).addClass("peer_product_list_wrap_active");
			$(".peer_product_list_wrap li").not(this).removeClass("peer_product_list_wrap_active");
		});

		$(".text_n").click(function () {
			var notice = $(this).parents(".title_notice");
			if (notice.hasClass("active") == true) {
				notice.removeClass("active");
			} else {
				$(".title_notice").removeClass("active");
				notice.addClass("active");
			}
			$(".close_btn_notice").click(function () {
				$(this).parents(".title_notice").removeClass("active");
			});
		});

		$(".msg_payday .close_msg_pay").click(function () {
			$(this).parents(".msg_payday").hide();
		});

		var swiper = new Swiper('.slide3', {
			slidesPerView: 3,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		});

		$(".rank_pry").mouseover(function () {
			$(this).children(".rank_pry_rank_info").css("display", "block");
		});
		$(".rank_pry").mouseout(function () {
			$(".rank_pry_rank_info").css("display", "none");
		});

		var conWidth = $(".peer_rank").width();
		var winWidth = $(window).width();
		var calValue = (winWidth - conWidth) / 2 - 80;
		$(".peer_top_btn").css("right", calValue);
		$(window).resize(function () {
			var conWidth = $(".peer_rank").width();
			var winWidth = $(window).width();
			var calValue = (winWidth - conWidth) / 2 - 80;
			$(".peer_top_btn").css("right", calValue);
		});
		$(".peer_top_btn").click(function () {
			$('html, body').stop().animate({
				scrollTop: 0
			}, 500);
		});
		$(window).scroll(function () {
			var y = $(window).scrollTop();
			if (y > 200) {
				$(".peer_top_btn").show();
			} else {
				$(".peer_top_btn").hide();
			}
		});
		$(".peer_product_list_wrap li").click(function () {
			var targetId = $(this).attr("for");
			var offset = $("#" + targetId).offset().top;
			offset -= $(".header_area").height();
			$('html, body').stop().animate({
				scrollTop: offset
			}, 350);
		});

		// 20230830 미리정산가능금액
		setTimeout(function () {
			$.ajax({
				url: '/sub/finGood/preSett'
				, type: 'get'
				, async: false
				, success: function (res) {
					$(".banner_payment").attr('style', 'display: block;')
					$(".price_bfhand").html("<p>최대 <b>" + formatNumber(res) + "</b>원</p>");
				}
				, error: function (request, status, error) {
					console.error("미리정산가능금액 가져오기 실패");
				}
			});
		}, 2500);
	});

</script>

<style>
	::-webkit-scrollbar {
		height: 5px;
	}
</style>