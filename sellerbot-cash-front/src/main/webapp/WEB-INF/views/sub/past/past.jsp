<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script src="/assets/js/Chart.bundle.min.js"></script>

<script src="/assets/js/common/common.js"></script>
<script src="/assets/js/common/util.js"></script>
<script src="/assets/js/past.js?ver=20230221_01"></script>

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-pagination.js"></script>
<script src="/assets/js/tui-grid.js"></script>

<div class="container">
    <div class="sales_wra">
        <div class="sales_area">
        <div class="sub_tab_menu">
            <p class="name">과거정산금 통계</p>
            <div class="tab_menu">
            <a href="/sub/past/past" class="focus">정산금 현황</a>
            <a href="/sub/past/compare">매출/정산 비교</a>
            </div>
        </div>
        <div class="summary_report_section_wrap">
            <div class="summary_report_section">
            <p class="title_summary open">[과거 정산금현황 요약보고]</p>
            <div class="summary_section_card">
                <div id="dataFirst" class="summary_card first" style="display: none;">
                <div class="title_highlight">
                    <p>${prevMonth} 정산금</p>
                </div>
                <div class="graph_summary type1">
                    <canvas id="spGh1" height="150"></canvas>
                    <div class="gh_line"></div>
                    </div>
                <p class="desc_summary">
                        ${prevMonth} 정산금은<br>
                        <c:if test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > 0}">
                        전월 대비 <span class="up">${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}% 증가</span><c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto ne 0}">하였고,<br></c:if>
                        <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto eq 0}">
                            하였습니다.
                        </c:if>
                        </c:if>
                        <c:if test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < 0}">
                        전월 대비 <span class="down">${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}% 감소</span><c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto ne 0}">하였고,<br></c:if>
                        <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto eq 0}">
                            하였습니다.
                        </c:if>
                        </c:if>
                        <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto > 0}">
                        전년 동월대비 <span class="up">${settAccSummaryLast.last_year_same_month_sett_acc_rto}% 증가</span>하였습니다.
                        </c:if>
                        <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto < 0}">
                        전년 동월대비 <span class="down">${settAccSummaryLast.last_year_same_month_sett_acc_rto}% 감소</span>하였습니다.
                        </c:if>
                </p>
                </div>
                <!--nodata_1-->
                <div id="no_dataFirst" class="summary_card first first_nodata">
                    <div class="title_highlight">
                    <p class="first_nodata_title">${prevMonth} 정산금</p>
                    </div>
                    <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                    </div>
                </div>
                <div id="dataSecond" class="summary_card second" style="display: none;">
                <div class="title_highlight">
                    <p>최근 1년 정산금</p>
                </div>
                <div class="graph_summary type2">
                    <canvas id="spGh2" height="150"></canvas>
                </div>
                <p class="desc_summary">최근 1년 중 정산금은<br>
                        <span class="up"><script>document.write(convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_max_month}', "년 ", "월"));</script></span>이
                        <span class="up"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_max_prc}" pattern="#,###" />원</span>으로 가장 높았고,<br>
                        <span class="down"><script>document.write(convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_min_month}', "년 ", "월"));</script></span>이
                        <span class="down"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_min_prc}" pattern="#,###" />원</span>으로 가장 낮았으며,<br>
                        <span class="average">월 평균 정산금</span>은
                        <span class="average"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_avg_prc}" pattern="#,###" />원</span> 입니다.
                </p>
                </div>
                <!--nodata_2-->
                <div id="no_dataSecond" class="summary_card second first_nodata">
                    <div class="title_highlight">
                    <p class="second_nodata_title">최근 1년 정산금</p>
                    </div>
                    <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                    </div>
                </div>
                <div id="dataThird" class="summary_card third" style="display: none;">
                </div>
                <!--nodata_3-->
                <div id="no_dataThird" class="summary_card third first_nodata">
                    <div class="title_highlight">
                    <p id="thirdDateRange" class="third_nodata_title">조회기간 기준 (${searchDateRangeStr})</p>
                    </div>
                    <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                    </div>
                </div>
                <div class="summary_card last">
                <div class="talk_sale_box">
                    <p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
                    <c:choose>
                    <c:when test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto >= 50}">
                        <div class="talk_item first">
                        <p class="emotion_talk">
                            <img src="/assets/images/main/jungsan_verygood.png" alt="">
                        </p>
                        <p class="lb_emotion">정산금 지역</p>
                        </div>
                        <div class="talk_item">
                        <p class="txt_talk">매출이 엄~청<br>
                            늘었어요! 야호^^</p>
                        </div>
                    </c:when>
                    <c:when
                        test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > 20 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < 50}">
                        <div class="talk_item first">
                        <p class="emotion_talk">
                            <img src="/assets/images/main/jungsan_good.png" alt="">
                        </p>
                        <p class="lb_emotion">정산금 지역</p>
                        </div>
                        <div class="talk_item">
                        <p class="txt_talk">이대로만 가즈아,<br>
                            쭉~쭉쭉쭉!</p>
                        </div>
                    </c:when>
                    <c:when
                        test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto >= -20 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto <= 20}">
                        <div class="talk_item first">
                        <p class="emotion_talk">
                            <img src="/assets/images/main/jungsan_normal.png" alt="">
                        </p>
                        <p class="lb_emotion">정산금 지역</p>
                        </div>
                        <div class="talk_item">
                        <p class="txt_talk">여기서 만족할 순<br>
                            없겠죠?!</p>
                        </div>
                    </c:when>
                    <c:when
                        test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > -50 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < -20}">
                        <div class="talk_item first">
                        <p class="emotion_talk">
                            <img src="/assets/images/main/jungsan_bad.png" alt="">
                        </p>
                        <p class="lb_emotion">정산금 지역</p>
                        </div>
                        <div class="talk_item">
                        <p class="txt_talk">에구, 좀 더<br>
                            분발하셔야겠어요</p>
                        </div>
                    </c:when>
                    <c:when
                        test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto <= -50}">
                        <div class="talk_item first">
                        <p class="emotion_talk">
                            <img src="/assets/images/main/jungsan_terrible.png" alt="">
                        </p>
                        <p class="lb_emotion">정산금 지역</p>
                        </div>
                        <div class="talk_item">
                        <p class="txt_talk">으쌰으쌰!<br>
                            힘을내요, 슈퍼파월~!</p>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="talk_item first">
                        <p class="emotion_talk">
                            <img src="/assets/images/main/jungsan_normal.png" alt="">
                        </p>
                        <p class="lb_emotion">정산금 지역</p>
                        </div>
                        <div class="talk_item">
                        <p class="txt_talk">과거 정산금 데이터를<br>분석하고 있습니다~</p>
                        </div>
                    </c:otherwise>
                    </c:choose>
                </div>
                <div class="talk_advice_box">
                    <p class="lb_advice">[도움말]</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>정산금 산출기준</b><br>
                    판매몰에서 제공하는 주문,정산관리에서 집계합니다.
                    </p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로 가능합니다.</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>용어정리</b><br>
                    <b class="advice_margin">공제금액:</b> 판매자가 부담한모든 수수료와 비용입니다.<br />
                    <b class="advice_margin">유보금:</b> 쇼핑몰이 지급보류한 정산금입니다.<br />
                    <b class="advice_margin">배송비:</b> 쇼핑몰이 구매자로부터 수취한 배송비입니다
                    </p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">공제금액과 유보금은 <b>판매대비 비율</b>입니다.</p>
                </div>
                </div>
            </div>
            </div>
        </div>
        <!-- 모바일버전을 위한 상단 swiper 추가 -->
        <!--960 반응형-->
        <div class="summary_report_section_m_Box" style="display:none;">
            <div class="swiper-container summary_report_section_m">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[과거 정산금현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div id="dataFirst_m_Box" class="summary_card first" style="display: none;">
                        <div class="title_highlight">
                        <p>${prevMonth} 정산금</p>
                        </div>
                        <div class="graph_summary type1">
                            <canvas id="spGh1_m" height="150"></canvas>
                            <div class="gh_line"></div>
                        </div>
                        <p class="desc_summary">
                            ${prevMonth} 정산금은<br>
                            <c:if test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > 0}">
                                전월 대비 <span class="up">${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}% 증가</span><c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto ne 0}">하였고,<br></c:if>
                                <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto eq 0}">
                                하였습니다.
                                </c:if>
                            </c:if>
                            <c:if test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < 0}">
                                전월 대비 <span class="down">${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}% 감소</span><c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto ne 0}">하였고,<br></c:if>
                                <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto eq 0}">
                                하였습니다.
                                </c:if>
                            </c:if>
                            <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto > 0}">
                                전년 동월대비 <span class="up">${settAccSummaryLast.last_year_same_month_sett_acc_rto}% 증가</span>하였습니다.
                            </c:if>
                            <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto < 0}">
                                전년 동월대비 <span class="down">${settAccSummaryLast.last_year_same_month_sett_acc_rto}% 감소</span>하였습니다.
                            </c:if>
                        </p>
                    </div>
                    <!--nodata_1-->
                    <div id="no_dataFirst_m_Box" class="summary_card first first_nodata">
                        <div class="title_highlight">
                        <p class="first_nodata_title">${prevMonth} 정산금</p>
                        </div>
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>
                    <div id="dataSecond_m_Box" class="summary_card second" style="display: none;">
                        <div class="title_highlight">
                        <p>최근 1년 정산금</p>
                        </div>
                        <div class="graph_summary type2">
                        <canvas id="spGh2_m" height="150"></canvas>
                        </div>
                        <p class="desc_summary">최근 1년 중 정산금은<br>
                        <span class="up"><script>document.write(convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_max_month}', "년 ", "월"));</script></span>이
                        <span class="up"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_max_prc}" pattern="#,###" />원</span>으로 가장 높았고,<br>
                        <span class="down"><script>document.write(convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_min_month}', "년 ", "월"));</script></span>이
                        <span class="down"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_min_prc}" pattern="#,###" />원</span>으로 가장 낮았으며,<br>
                        <span class="average">월 평균 정산금</span>은
                        <span class="average"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_avg_prc}" pattern="#,###" />원</span> 입니다.
                        </p>
                    </div>
                    <!--nodata_2-->
                    <div id="no_dataSecond_m_Box" class="summary_card second first_nodata">
                        <div class="title_highlight">
                            <p class="second_nodata_title">최근 1년 정산금</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[과거 정산금현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div id="dataThird_m_Box" class="summary_card third" style="display: none;">
                    </div>
                    <!--nodata_3-->
                    <div id="no_dataThird_m_Box" class="summary_card third first_nodata">
                        <div class="title_highlight">
                            <p id="thirdDateRange" class="third_nodata_title">조회기간 기준 (${searchDateRangeStr})</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    <div class="summary_card last">
                        <div class="talk_sale_box">
                        <p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
                        <c:choose>
                            <c:when test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto >= 50}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_verygood.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">매출이 엄~청<br>
                                늘었어요! 야호^^</p>
                            </div>
                            </c:when>
                            <c:when
                            test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > 20 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < 50}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_good.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">이대로만 가즈아,<br>
                                쭉~쭉쭉쭉!</p>
                            </div>
                            </c:when>
                            <c:when
                            test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto >= -20 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto <= 20}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_normal.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">여기서 만족할 순<br>
                                없겠죠?!</p>
                            </div>
                            </c:when>
                            <c:when
                            test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > -50 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < -20}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_bad.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">에구, 좀 더<br>
                                분발하셔야겠어요</p>
                            </div>
                            </c:when>
                            <c:when test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto <= -50}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_terrible.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">으쌰으쌰!<br>
                                힘을내요, 슈퍼파월~!</p>
                            </div>
                            </c:when>
                            <c:otherwise>
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_normal.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">과거 정산금 데이터를<br>분석하고 있습니다~</p>
                            </div>
                            </c:otherwise>
                        </c:choose>
                        </div>
                        <div class="talk_advice_box">
                        <p class="lb_advice">[도움말]</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>정산금 산출기준</b><br>
                            판매몰에서 제공하는 주문,정산관리에서 집계합니다.
                        </p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로
                            가능합니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>용어정리</b><br>
                            <b class="advice_margin">공제금액:</b> 판매자가 부담한모든 수수료와 비용입니다.<br />
                            <b class="advice_margin">유보금:</b> 쇼핑몰이 지급보류한 정산금입니다.<br />
                            <b class="advice_margin">배송비:</b> 쇼핑몰이 구매자로부터 수취한 배송비입니다
                        </p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">공제금액과 유보금은 <b>판매대비
                            비율</b>입니다.</p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="swiper-pagination1"></div>
        </div>
        <!--320 반응형-->
        <div class="summary_report_section_m_Box_2" style="display:none;">
            <div class="swiper-container summary_report_section_m_2">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[과거 정산금현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div id="dataFirst_m_Box_2" class="summary_card first" style="display: none;">
                        <div class="title_highlight">
                        <p>${prevMonth} 정산금</p>
                        </div>
                        <div class="graph_summary type1">
                            <canvas id="spGh1_m_1" height="150"></canvas>
                            <div class="gh_line"></div>
                        </div>
                        <p class="desc_summary">
                            ${prevMonth} 정산금은<br>
                            <c:if test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > 0}">
                                전월 대비 <span class="up">${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}% 증가</span><c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto ne 0}">하였고,<br></c:if>
                                <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto eq 0}">
                                하였습니다.
                                </c:if>
                            </c:if>
                            <c:if test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < 0}">
                                전월 대비 <span class="down">${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}% 감소</span><c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto ne 0}">하였고,<br></c:if>
                                <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto eq 0}">
                                하였습니다.
                                </c:if>
                            </c:if>
                            <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto > 0}">
                                전년 동월대비 <span class="up">${settAccSummaryLast.last_year_same_month_sett_acc_rto}% 증가</span>하였습니다.
                            </c:if>
                            <c:if test="${settAccSummaryLast.last_year_same_month_sett_acc_rto < 0}">
                                전년 동월대비 <span class="down">${settAccSummaryLast.last_year_same_month_sett_acc_rto}% 감소</span>하였습니다.
                            </c:if>
                        </p>
                    </div>
                    <!--nodata_1-->
                    <div id="no_dataFirst_m_Box_2" class="summary_card first first_nodata">
                        <div class="title_highlight">
                        <p class="first_nodata_title">${prevMonth} 정산금</p>
                        </div>
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>

                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[과거 정산금현황 요약보고]</p>
                    <div class="summary_section_card summary_section_card_display">
                    <div id="dataSecond_m_Box_2" class="summary_card second" style="display: none;">
                        <div class="title_highlight">
                        <p>최근 1년 정산금</p>
                        </div>
                        <div class="graph_summary type2">
                        <canvas id="spGh2_m_2" height="150"></canvas>
                        </div>
                        <p class="desc_summary">최근 1년 중 정산금은<br>
                        <span class="up"><script>document.write(convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_max_month}', "년 ", "월"));</script></span>이
                        <span class="up"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_max_prc}" pattern="#,###" />원</span>으로 가장 높았고,<br>
                        <span class="down"><script>document.write(convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_min_month}', "년 ", "월"));</script></span>이
                        <span class="down"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_min_prc}" pattern="#,###" />원</span>으로 가장 낮았으며,<br>
                        <span class="average">월 평균 정산금</span>은
                        <span class="average"><fmt:formatNumber value="${settAccSummaryLast.last_year_sett_acc_avg_prc}" pattern="#,###" />원</span> 입니다.
                        </p>
                    </div>
                    <!--nodata_2-->
                    <div id="no_dataSecond_m_Box_2" class="summary_card second first_nodata">
                        <div class="title_highlight">
                            <p class="second_nodata_title">최근 1년 정산금</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[과거 정산금현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div id="dataThird_m_Box_2" class="summary_card third" style="display: none;">
                    </div>
                    <!--nodata_3-->
                    <div id="no_dataThird_m_Box_2" class="summary_card third first_nodata">
                        <div class="title_highlight">
                            <p id="thirdDateRange" class="third_nodata_title">조회기간 기준 (${searchDateRangeStr})</p>
                        </div>
                        <div class="nodata_flexBox">
                            <h1>데이터가 없습니다.</h1>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[과거 정산금현황 요약보고]</p>
                    <div class="summary_section_card summary_section_card_display">
                    <div class="summary_card last">
                        <div class="talk_sale_box">
                        <p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
                        <c:choose>
                            <c:when test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto >= 50}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_verygood.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">매출이 엄~청<br>
                                늘었어요! 야호^^</p>
                            </div>
                            </c:when>
                            <c:when
                            test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > 20 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < 50}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_good.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">이대로만 가즈아,<br>
                                쭉~쭉쭉쭉!</p>
                            </div>
                            </c:when>
                            <c:when
                            test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto >= -20 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto <= 20}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_normal.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">여기서 만족할 순<br>
                                없겠죠?!</p>
                            </div>
                            </c:when>
                            <c:when
                            test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto > -50 && settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto < -20}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_bad.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">에구, 좀 더<br>
                                분발하셔야겠어요</p>
                            </div>
                            </c:when>
                            <c:when test="${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto <= -50}">
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_terrible.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">으쌰으쌰!<br>
                                힘을내요, 슈퍼파월~!</p>
                            </div>
                            </c:when>
                            <c:otherwise>
                            <div class="talk_item first">
                                <p class="emotion_talk">
                                <img src="/assets/images/main/jungsan_normal.png" alt="">
                                </p>
                                <p class="lb_emotion">정산금 지역</p>
                            </div>
                            <div class="talk_item">
                                <p class="txt_talk">과거 정산금 데이터를<br>분석하고 있습니다~</p>
                            </div>
                            </c:otherwise>
                        </c:choose>
                        </div>
                        <div class="talk_advice_box">
                        <p class="lb_advice">[도움말]</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>정산금 산출기준</b><br>
                            판매몰에서 제공하는 주문,정산관리에서 집계합니다.
                        </p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로
                            가능합니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>용어정리</b><br>
                            <b class="advice_margin">공제금액:</b> 판매자가 부담한모든 수수료와 비용입니다.<br />
                            <b class="advice_margin">유보금:</b> 쇼핑몰이 지급보류한 정산금입니다.<br />
                            <b class="advice_margin">배송비:</b> 쇼핑몰이 구매자로부터 수취한 배송비입니다
                        </p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">공제금액과 유보금은 <b>판매대비
                            비율</b>입니다.</p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="swiper-pagination2"></div>
        </div>
        <!--swiper모바일-->
        <script>
            $(function() {
            var swiper = new Swiper('.summary_report_section_m', {
                pagination: {
                el: '.swiper-pagination1',
                },

            });
            var swiper = new Swiper('.summary_report_section_m_2', {
                pagination: {
                el: '.swiper-pagination2',
                },
            });
            });
        </script>

        <div class="summary_graph_section">
            <div class="period_graph_box">
            <div class="choice_mall_type">
                <span class="lb">쇼핑몰 선택</span>
                <select id="mallSelect" class="sctbox_mall">
                <option value="">전체</option>
                <c:forEach var="mall" items="${custMallList}" varStatus="status">
                    <option value="${mall.mall_cd}">${mall.mall_cd_nm}</option>
                </c:forEach>
                </select>
            </div>
            <!-- datepicker -->
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="datepicker-input-start" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
            </div>
            <div class="datepicker-cell" id="datepicker-month-start" style="margin-top: -1px;"></div>
            <h1 style="display:inline-block;display: inline-block;vertical-align: middle;font-size: 1rem;"> ~ </h1>
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="datepicker-input-end" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
            </div>
            <div class="datepicker-cell" id="datepicker-month-end" style="margin-top: -1px;"></div>
            <!-- datepicker -->
            <div class="btn_period_group">
                <button id="searchBtn">조회하기</button>
                <button id="lastYearBtn" type="button" class="btn_gray">최근 1년보기</button>
            </div>
            </div>
            <p class="unit">(단위: 천 원)</p>
            <div class="graph_bar_box_wrap">
            <div class="contents_title">
                <h1>과거 정산금현황 그래프</h1>
                <h2>(단위: 천 원)</h2>
            </div>
            <div class="graph_bar_box">
                <!-- <div class="line_average">
                <span>2,000</span>
                <p></p>
                </div> -->
                <div class="label_gh">
                <div class="item_label">
                    <span class="color"></span>
                    <span class="txt_lb">정산금액</span>
                </div>
                </div>
                <div class="barGh1_scroll">
                <canvas id="barGh1" width="1400" height="420"></canvas>
                <!-- <div class="bar_chart_noData">
                    <p>데이터가 없습니다.</p>
                </div> -->
                </div>
            </div>
            </div>
        </div>
        <div class="status_gridArea">
            <div class="status_grid_wrapper">
            <div class="contents_title">
                <h1>과거 정산금현황 요약</h1>
                <h2>(단위: 천 원)</h2>
            </div>
            <div id="status_grid_1" style="display: none;"></div>
            <div id="status_grid_1_noData" class="grid_noData">
                <h1>데이터가 없습니다.</h1>
                </div>

            </div>
            <div class="status_grid_wrapper">
            <div class="contents_title">
                <h1>과거 정산금현황 월별 상세보기</h1>
                <h2>(단위: 천 원)</h2>
            </div>
            <div id="status_grid_2" style="display: none;"></div>
            <div id="status_grid_2_noData" class="grid_noData">
                <h1>데이터가 없습니다.</h1>
                </div>
            </div>
            <div id="grid3Wrapper" class="status_grid_wrapper">
            <div id="status_grid_3"></div>
            <!-- <div class="grid_noData">
                <h1>데이터가 없습니다.</h1>
            </div> -->
            <div id="grid3-pagination-container" class="tui-pagination"></div>
            <!-- <div class="tui-pagination tui-grid-pagination">
                <span class="tui-page-btn tui-is-disabled tui-first">
                <span class="tui-ico-first">first</span>
                </span>
                <span class="tui-page-btn tui-is-disabled tui-prev">
                <span class="tui-ico-prev">prev</span>
                </span>
                <strong class="tui-page-btn tui-is-selected tui-first-child">1</strong>
                <a href="#" class="tui-page-btn">2</a>
                <a href="#" class="tui-page-btn">3</a>
                <a href="#" class="tui-page-btn tui-last-child">4</a>
                <a href="#" class="tui-page-btn tui-next"><span class="tui-ico-next">next</span></a>
                <a href="#" class="tui-page-btn tui-last"><span class="tui-ico-last">last</span></a>
            </div> -->
            </div>
        </div>
        </div>
    </div>
    </div>
<style>
        /* #status_grid_1 .tui-grid-cell-header:nth-of-type(1) {
    text-align: center;
    }
    #status_grid_2 .tui-grid-cell-header:nth-of-type(1) {
    text-align: center;
    }
    #status_grid_3 .tui-grid-cell-header:nth-of-type(1) {
    text-align: center;
    } */
    .tui-grid-row-even td:nth-of-type(1) div {
        color:#000 !important;
    }
    .tui-grid-row-odd td:nth-of-type(1) div {
        color:#000 !important;
    }

    .swiper-pagination1,
    .swiper-pagination2 {
        margin: 0 auto;
        text-align: center;
        width: 100%;
        bottom: 2.5rem;
        position: absolute;
        z-index: 6;
    }

    .swiper-pagination-bullet {
        margin-right: 0.5rem;
    }

    .swiper-pagination-bullet:last-of-type {
        margin-right: 0;
    }

    .tui-grid-body-area {
        height: inherit !important;
    }

    .tui-grid-cell-header {
        background: none;
    }

    .tui-grid-cell {
        background-color: #f8f8f8;
    }

    /*paging*/
    .tui-pagination {
        padding-top: 1rem;
    }

    .tui-pagination .tui-is-selected,
    .tui-pagination strong {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-is-selected:hover {
        background: #a4a4a4;
        border-color: #a4a4a4;
    }

    .tui-pagination .tui-first-child.tui-is-selected {
        border: 1px solid #a4a4a4;
    }

    .tui-grid-container * {
        box-sizing: content-box;
        text-align: right;
    }

    /* .tui-grid-cell-header {
    text-align: center;
    } */

    #status_grid_2 .tui-grid-row-odd td:nth-of-type(1) div {
        text-align: left !important;
    }

    #status_grid_2 .tui-grid-row-even td:nth-of-type(1) div {
        text-align: left !important;
    }
    #status_grid_3 .tui-grid-row-odd td:nth-of-type(1) div {
        text-align: left !important;
    }

    #status_grid_3 .tui-grid-row-even td:nth-of-type(1) div {
        text-align: left !important;
    }

    .tui-datepicker-input>input {
        width: 100%;
        height: 100%;
        padding: 6px 27px 6px 10px;
        font-size: 12px;
        line-height: 14px;
        vertical-align: top;
        border: 0;
        color: #333;
        border-radius: 3rem;
    }

    .tui-datepicker-input {
        position: relative;
        display: inline-block;
        width: 120px;
        height: 2.5rem;
        vertical-align: top;
        border: 1px solid #ddd;
        border-radius: 3rem;
    }

    .datepicker-cell {
        display: inline-block;
    }

    .tui-datepicker-input.tui-has-focus {
        vertical-align: middle;
        border: 0;
    }

    .tui-datepicker {
        border: 1px solid #aaa;
        background-color: white;
        position: absolute;
        margin-left: -11rem;
        margin-top: 1rem;
        z-index: 999;
    }
</style>
<!-- <script>
$(document).ready(function(){
    var contentLength = $(".tui-grid-cell-content").length;
    for (i = 0; i < contentLength - 1; i++) {
    if ($(".tui-grid-cell-content").eq(i).html() == "합계" || $(".tui-grid-cell-content").eq(i).html() == "평균") {
        $(".tui-grid-cell-content").eq(i).css("textAlign", "center");
    }
    }
    });
</script> -->
<!--그리드 추가-->
<script>
    //첫번째
    var monthInfo = JSON.parse('${monthInfo}');
    const grid2 = new tui.Grid({
    el: document.getElementById('status_grid_1'),
    data: createStatusData(monthInfo),
    scrollX: true,
    scrollY: false,
    columns: [{
        header: '전체',
        name: 'title1',
        },
        {
        header: '판매금액',
        name: 'contents1',
        },
        {
        header: '(-) 공제금액',
        name: 'contents2',
        },
        {
        header: '(+) 유보금',
        name: 'contents3',
        },
        {
        header: '(-) 유보금',
        name: 'contents4',
        },
        {
        header: '(+) 배송비',
        name: 'contents5',
        },
        {
        header: '정산금액',
        name: 'contents6',
        },
    ]
    });
    //세번째
    gridData3 = [{
        title1: '정산예정금액<br />(정산완료)',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    }, {
        title1: '판매금액',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    },
    {
        title1: '공제합계<br />(①+②+③+④+⑤)',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    },
    {
        title1: '배송비금액',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    },
    {
        title1: '서비스이용료<br />①',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    },
    {
        title1: '판매자쿠폰할인<br />②',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    },
    {
        title1: '쿠폰적용금액<br />③',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    },
    {
        title1: '특수구매할인<br />④',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    },
    {
        title1: '스마일캐시적립<br />⑤',
        contents1: '-',
        contents2: '-',
        contents3: '-',
        contents4: '15,035',
        contents5: '15,035',
        contents6: '15,035',
        contents7: '15,035',
        contents8: '15,035',
        contents9: '15,035',
        contents10: '15,035',
        contents11: '15,035',
        contents12: '15,035',
    },
    ];
    var monthList = JSON.parse('${monthList}');
    const grid = new tui.Grid({
    el: document.getElementById('status_grid_2'),
    rowHeight: 60,
    data: makeMonthGridData(monthList),
    scrollX: true,
    scrollY: false,
    columns: makeMonthGridColumns(monthList),
    });
    const grid3 = new tui.Grid({
    el: document.getElementById('status_grid_3'),
    rowHeight: 60,
    data: gridData3,
    scrollX: true,
    scrollY: true,
    pagination: true,
    columnOptions: {
        resizable: true
    },
    columns: [{
        header: '전체',
        name: 'title1',
        whiteSpace: 'normal',
        },
        {
        header: '2018-04',
        name: 'contents1'
        },
        {
        header: '2018-05',
        name: 'contents2'
        },
        {
        header: '2018-06',
        name: 'contents3'
        },
        {
        header: '2018-07',
        name: 'contents4'
        },
        {
        header: '2018-08',
        name: 'contents5'
        },
        {
        header: '2018-09',
        name: 'contents6'
        }, {
        header: '2018-10',
        name: 'contents7'
        },
        {
        header: '2018-11',
        name: 'contents8'
        },
        {
        header: '2018-12',
        name: 'contents9'
        },
        {
        header: '2019-01',
        name: 'contents10'
        },
        {
        header: '2019-02',
        name: 'contents11'
        },
        {
        header: '2019-03',
        name: 'contents12'
        },
    ]
    });
    //grid mouseover
    $(".tui-grid-row-odd, .tui-grid-row-even").mouseover(function() {
        $(this).children("td").addClass("grid_background_color_1");
    });
    $(".tui-grid-row-odd, .tui-grid-row-even").mouseout(function() {
        $(this).children("td").removeClass("grid_background_color_1");
    });
    
    var pagination = new tui.Pagination('grid3-pagination-container', {
        totalItems: 100,
        visiblePages: 5,
        itemsPerPage: 5,
        page: 1,
        centerAlign: false,
        firstItemClassName: 'tui-first-child',
        lastItemClassName: 'tui-last-child',
        usageStatistics: true
    });

    pagination.on('beforeMove', function (eventData) {
        var last = eventData.page * pagination._options.itemsPerPage;
        var start = last - pagination._options.itemsPerPage;

        if (last > pagination.totalItems) {
            last = pagination.totalItems;
        }

        grid3.resetData(detailGridData.slice(start, last));
    });
</script>    
<!-- 정산금 통계 화면용 스크립트 -->
<script>
// 초기화면 진입 시 몰별 상세보기 숨김처리
    $("#grid3Wrapper").hide();

    // 데이트피커 설정
    var today = Date.now();

    var startMonthPicker = new tui.DatePicker('#datepicker-month-start', {
        date: stringToDate_YYYYMM('${startDate}'),
        language: 'ko',
        type: 'month',
        input: {
            element: '#datepicker-input-start',
            format: 'yyyy-MM'
        },
        selectableRanges: [
            [new Date(0), moment(today).subtract(11, 'month').toDate()]
        ]
    });

    var endMonthPicker = new tui.DatePicker('#datepicker-month-end', {
        date: stringToDate_YYYYMM('${endDate}'),
        language: 'ko',
        type: 'month',
        input: {
            element: '#datepicker-input-end',
            format: 'yyyy-MM'
        },
        selectableRanges: [
            [new Date(0), today]
        ]
    });

    // 현황 표시
    // 지난달 정산금
    var last_month_per_before_last_month_sett_acc_rto = parseFloat(nvl('${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}', "0.0"));
    var last_year_same_month_sett_acc_rto = parseFloat(nvl('${settAccSummaryLast.last_year_same_month_sett_acc_rto}', "0.0"));

    if (last_month_per_before_last_month_sett_acc_rto != 0 || last_year_same_month_sett_acc_rto != 0) {
        visibleStatus("dataFirst", true)
    }

    // 최근 1년 정산금
    var last_year_sett_acc_max_month = '${settAccSummaryLast.last_year_sett_acc_max_month}';
    var last_year_sett_acc_min_month = '${settAccSummaryLast.last_year_sett_acc_min_month}';

    if (last_year_sett_acc_max_month != last_year_sett_acc_min_month) {
        visibleStatus("dataSecond", true)
    }

    // 조회기간 기준
    var settAccSummaryPeriod = JSON.parse('${settAccSummaryPeriod}');

    if ((settAccSummaryPeriod.max_sales_prc_per_sett_acc_dedu_prc_rto_mall_cd != settAccSummaryPeriod.min_sales_prc_per_sett_acc_dedu_prc_rto_mall_cd)
    || (settAccSummaryPeriod.max_sales_prc_per_sett_acc_defer_prc_sum_rto_mall_cd != settAccSummaryPeriod.min_sales_prc_per_sett_acc_defer_prc_sum_rto_mall_cd)
    || (settAccSummaryPeriod.max_sales_prc_per_sett_acc_dlv_prc_sum_rto_mall_cd != settAccSummaryPeriod.min_sales_prc_per_sett_acc_dlv_prc_sum_rto_mall_cd)) {
        visibleStatus("dataThird", true)
        createSummaryPeriod(settAccSummaryPeriod);
    }

    // 조회하기 이벤트
    $("#searchBtn").click(function () {
        search();
    });

    // 최근 1년보기
    $("#lastYearBtn").click(function () {
        endMonthPicker.setDate(getAddMonth(new Date(), -1));
    });

    // 조회 시작일 변경 이벤트
    // 조회 시작일 기준 12개월 후 혹은 금월까지 범위 설정
    startMonthPicker.on('change', function () {
    var today = new Date();
    var end = getAddMonth(startMonthPicker.getDate(), 11);

    if (end > today)
        endMonthPicker.setDate(today);
    else
        endMonthPicker.setDate(end);
    });

    // 조회 종료일 변경 이벤트
    // 조회 종료일 기준 12개월 전으로 범위 설정
    endMonthPicker.on('change', function () {
        startMonthPicker.setDate(getAddMonth(endMonthPicker.getDate(), -11));
    });
</script>

<script>
//전월대비, 전년 동월대비 
function getMaxValue(){
    var dataList =  ['${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}', '${settAccSummaryLast.last_year_same_month_sett_acc_rto}'];
    return Math.abs(dataList[0]) > Math.abs(dataList[1]) ? Math.abs(dataList[0]) : Math.abs(dataList[1]);
}
//전월대비, 전년 동월대비 
var _option_max = getMaxValue();

var ctx = document.getElementById('barGh1').getContext('2d');
var ctx1 = document.getElementById('spGh1').getContext('2d');
var ctx2 = document.getElementById('spGh2').getContext('2d');
var ctx5 = document.getElementById('spGh1_m').getContext('2d');
var ctx6 = document.getElementById('spGh2_m').getContext('2d');
var ctx7 = document.getElementById('spGh1_m_1').getContext('2d');
var ctx8 = document.getElementById('spGh2_m_2').getContext('2d');
var option1 = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
    padding: {
        top: 75,
        bottom: 20,
        right: 50,
        left: 90,
    }
    },
    legend: {
    display: false,
    },
    title: {
    display: false,
    },
    scales: {
    xAxes: [{
        barPercentage: 0.4,
        gridLines: {
        display: false,
        }
    }],
    yAxes: [{
        position: 'right',
        ticks: {
        maxTicksLimit: 8,
        beginAtZero: true,
        userCallback: function(value, index, values) {
                value = value.toString();
                value = value.split(/(?=(?:...)*$)/);
                value = value.join(',');
                return value;
            }
        },
        gridLines: {
        drawBorder: false,
        },
    }]
    },
    tooltips: {
    enabled: false,
    }
}
var maxSettAcc = '${settAccSummaryLast.last_year_sett_acc_max_prc}';
var option2 = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
    padding: {
        top: 10
    }
    },
    legend: {
    display: false
    },
    title: {
    display: false
    },
    scales: {
    xAxes: [{
        //barPercentage: 0.8,
        gridLines: {
        display: false,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 1000,
        color:"rgba(255,99,132,0)"
        }
    }],
    yAxes: [{
        ticks: {
        max: _option_max,
        min: (_option_max * -1),
        display: false,
        suggestedMin: 0,
        },
        gridLines: {
        display: true,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 0,
        color:"rgba(255,99,132,0)"
        },
    }]
    },
    tooltips: {
    enabled: false,
    }
}
var option_eidt_1 = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
    padding: {
        top: 10
    }
    },
    legend: {
    display: false
    },
    title: {
    display: false
    },
    scales: {
    xAxes: [{
        //barPercentage: 0.8,
        gridLines: {
        display: false,
        // drawBorder: false,
        beginAtZero: false,
        barThickness: 1000,
        color: "rgba(255,99,132,0)"
        }
    }],
    yAxes: [{
        ticks: {
        max: _option_max,
        min: (_option_max * -1),
        display: false,
        suggestedMin: 0,
        },
        gridLines: {
        display: true,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 0,
        color: "rgba(255,99,132,0)"
        },
    }]
    },
    tooltips: {
    enabled: false,
    }
}
var option_eidt_2 = {
    responsive: false,
    maintainAspectRatio: false,
    layout: {
    padding: {
        top: 10
    }
    },
    legend: {
    display: false
    },
    title: {
    display: false
    },
    scales: {
    xAxes: [{
        //barPercentage: 0.8,
        gridLines: {
        display: false,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 1000,
        color: "rgba(255,99,132,0)"
        }
    }],
    yAxes: [{
        ticks: {
        display: false,
        suggestedMin: 0,
        max: Math.ceil(maxSettAcc * 1.1)
        },
        gridLines: {
        display: true,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 0,
        color: "rgba(255,99,132,0)"
        },
    }]
    },
    tooltips: {
    enabled: false,
    }
}
var monthList = JSON.parse('${monthList}');
var monthBarChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: makeMonthChartColumns(monthList),
        datasets: [{
            data: makeMonthChartData(monthList, 'sett_acc_prc'),
            backgroundColor: "#4f69e8",
            hoverBackgroundColor: "#4f69e8",
            borderColor: "#4f69e8",
            hoverBorderColor: "#4f69e8"
        }]
    },
    options: option1
});

var color = [];

if( '${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}' > 0){ color.push("#75ced2"); } 
else { color.push("#f4715b"); }

if( '${settAccSummaryLast.last_year_same_month_sett_acc_rto}' > 0){ color.push("#75ced2"); } 
else { color.push("#f4715b"); }

var myChart = new Chart(ctx1, {
    type: 'bar',
    data: {
        labels: ['전월 대비', '전년 동월대비'],
        datasets: [{
            data: ['${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}', '${settAccSummaryLast.last_year_same_month_sett_acc_rto}'],
            backgroundColor: color,
            hoverBackgroundColor: color,
            borderColor: color,
            hoverBorderColor: color
        }]
    },
    options: option2
});
var myChart = new Chart(ctx2, {
    type: 'bar',
    data: {
        labels: [convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_max_month}', '-', '월', true), convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_min_month}', '-', '월', true), '월평균'],
        datasets: [{
            data: ['${settAccSummaryLast.last_year_sett_acc_max_prc}', '${settAccSummaryLast.last_year_sett_acc_min_prc}', '${settAccSummaryLast.last_year_sett_acc_avg_prc}'],
            backgroundColor: ["#75ced2", "#f4715b", "#d6a7d1"],
            hoverBackgroundColor: ["#75ced2", "#f4715b", "#d6a7d1"],
            borderColor: ["#75ced2", "#f4715b", "#d6a7d1"],
            hoverBorderColor: ["#75ced2", "#f4715b", "#d6a7d1"]
        }]
    },
    options: {
    layout: {
        padding: {
            top: 10
        }
    },
    legend: {
        display: false
    },
    title: {
        display: false
    },
    scales: {
    xAxes: [{
        //barPercentage: 0.8,
        gridLines: {
        display: false,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 1000,
        color:"rgba(255,99,132,0)"
        }
    }],
    yAxes: [{
        ticks: {
        display: false,
        suggestedMin: 0,
        max: Math.ceil(maxSettAcc * 1.1)
        },
        gridLines: {
        display: true,
        drawBorder: false,
        beginAtZero: false,
        barThickness: 0,
        color:"rgba(255,99,132,0)"
        },
    }]
    },
    tooltips: {
        enabled: false,
    }
}
});
var myChart = new Chart(ctx5, {
    type: 'bar',
    data: {
        labels: ['전월 대비', '전년 동월대비'],
        datasets: [{
            data: ['${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}', '${settAccSummaryLast.last_year_same_month_sett_acc_rto}'],
            backgroundColor: color,
            hoverBackgroundColor: color,
            borderColor: color,
            hoverBorderColor: color
        }]
    },
    options: option_eidt_1
});
var myChart = new Chart(ctx6, {
    type: 'bar',
    data: {
        labels: [convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_max_month}', '-', '월', true), convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_min_month}', '-', '월', true), '월평균'],
        datasets: [{
            data: ['${settAccSummaryLast.last_year_sett_acc_max_prc}', '${settAccSummaryLast.last_year_sett_acc_min_prc}', '${settAccSummaryLast.last_year_sett_acc_avg_prc}'],
            backgroundColor: color,
            hoverBackgroundColor: color,
            borderColor: color,
            hoverBorderColor: color
        }]
    },
    options: option_eidt_2
});
var myChart = new Chart(ctx7, {
    type: 'bar',
    data: {
        labels: ['전월 대비', '전년 동월대비'],
        datasets: [{
            data: ['${settAccSummaryLast.last_month_per_before_last_month_sett_acc_rto}', '${settAccSummaryLast.last_year_same_month_sett_acc_rto}'],
            backgroundColor: color,
            hoverBackgroundColor: color,
            borderColor: color,
            hoverBorderColor: color
        }]
    },
    options: option_eidt_1
});
var myChart = new Chart(ctx8, {
    type: 'bar',
    data: {
        labels: [convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_max_month}', '-', '월', true), convertDate_YYYYMM('${settAccSummaryLast.last_year_sett_acc_min_month}', '-', '월', true), '월평균'],
        datasets: [{
            data: ['${settAccSummaryLast.last_year_sett_acc_max_prc}', '${settAccSummaryLast.last_year_sett_acc_min_prc}', '${settAccSummaryLast.last_year_sett_acc_avg_prc}'],
            backgroundColor: ["#75ced2", "#f4715b", "#d6a7d1"],
            hoverBackgroundColor: ["#75ced2", "#f4715b", "#d6a7d1"],
            borderColor: ["#75ced2", "#f4715b", "#d6a7d1"],
            hoverBorderColor: ["#75ced2", "#f4715b", "#d6a7d1"]
        }]
    },
    options: option_eidt_2
});
//custom tooltip
Chart.plugins.register({
    afterDatasetsDraw: function(chartInstance, easing) {
    // To only draw at the end of animation, check for easing === 1
    var ctx = chartInstance.chart.ctx;
    chartInstance.data.datasets.forEach(function(dataset, i) {
        var meta = chartInstance.getDatasetMeta(i);
        var chartID = chartInstance.chart.canvas.id;
        if (!meta.hidden) {
        if (chartID.indexOf("spGh2") > -1) {
            meta.data.forEach(function(element, index) {
            // Draw the text in black, with the specified font
            ctx.fillStyle = '#212121';
            var fontSize = 12;
            var fontStyle = 'normal';
            var fontFamily = 'NanumSquare';
            var backgroundColor = 'rgba(79,105,232,0.2)';
            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
            // Just naively convert to string for now
            var dataString = dataset.data[index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            // Make sure alignment settings are correct
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var padding = 5;
            var position = element.tooltipPosition();
            ctx.fillText(dataString + "원", position.x, position.y - (fontSize / 2) - padding);
            });
        } else if (chartID.indexOf("spGh1") > -1) {
            meta.data.forEach(function(element, index) {
            // Draw the text in black, with the specified font
            ctx.fillStyle = '#000';
            var fontSize = 12;
            var fontStyle = 'normal';
            var fontFamily = 'NanumSquare';
            var backgroundColor = 'rgba(79,105,232,0.2)';
            ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
            // Just naively convert to string for now
            var dataNum = dataset.data[index];
            var dataString = dataset.data[index].toString();
            // Make sure alignment settings are correct
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            var padding = 5;
            var position = element.tooltipPosition();
            if (dataNum > 0) {
                if (matchMedia("screen and (max-width: 5000px) and (min-width:751px)").matches) {
                    ctx.fillText("▲" + dataString + "%", position.x, position.y + (fontSize / 2) + padding);
                }
                if (matchMedia("screen and (max-width: 750px)").matches) {
                    ctx.fillText(dataString + "%", position.x, position.y + (fontSize / 2) + padding);
                }
            } else {
                // dataNum = dataNum.replace(/[^0-9]/g, '');
                dataNum = Math.abs(dataNum);
                if (matchMedia("screen and (max-width: 5000px) and (min-width:751px)").matches) {
                    ctx.fillText("▼" + dataNum + "%", position.x, position.y - (fontSize / 2) - padding);
                }
                if (matchMedia("screen and (max-width: 750px)").matches) {
                    // dataNum = dataNum.replace(/[^0-9]/g, '');
                    dataNum = Math.abs(dataNum);
                    ctx.fillText(dataNum + "%", position.x, position.y - (fontSize / 2) - padding);
                }
            }
            });
        } else {
            meta.data.forEach(function(element, index) {
                // Draw the text in black, with the specified font
                ctx.fillStyle = '#212121';
                var fontSize = 12;
                var fontStyle = 'normal';
                var fontFamily = 'NanumSquare';
                var backgroundColor = 'rgba(79,105,232,0.2)';
                ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                // Just naively convert to string for now
                var dataString = dataset.data[index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                // Make sure alignment settings are correct
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                var padding = 5;
                var position = element.tooltipPosition();
                ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
            });
        }

        }
    });
    }
});
</script>