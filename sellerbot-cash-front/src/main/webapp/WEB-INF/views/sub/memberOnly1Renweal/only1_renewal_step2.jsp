<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_sodeone/css/sodeOne_02.css">
    <script type="module" src="/assets/new_sodeone/js/sodeOne_02.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <section>
            <button type="button" class="fixed2" id="skipStep">
                <span>건너뛰기</span>
            </button>
            <button type="button" class="fixed _70" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>통합 회원가입</h1>
                    <img src="/assets/new_join/img/sellerbotLogo_sodeOnePage.png" alt="sellerbot">
                    <img src="/assets/new_join/img/sodeOneLogo_sodeOnePage.png" alt="sodeOneLogo">
                </div>
                <div class="processBar">
                    <div class="first step">
                        <span class="line"></span>
                        <p>약관동의</p>
                    </div>
                    <div class="second step active">
                        <span class="line"></span>
                        <p>가입여부 조회</p>
                    </div>
                    <div class="third step">
                        <span class="line"></span>
                        <p>이메일 본인인증</p>
                    </div>
                    <div class="forth step">
                        <span class="line"></span>
                        <p>사업자정보입력</p>
                    </div>
                    <div class="fifth step">
                        <span class="line"></span>
                        <p>결제수단 등록</p>
                    </div>
                </div>
            </article>
            <form onsubmit="return false;" id="form" name="form" method="post" autocomplete="off" novalidate="novalidate">
                <input type="hidden" value="${sodeOneTerm }" name="sodeOneTerm" />
                <input type="hidden" value="${sellerbotTerm }" name="sellerbotTerm" />
                <input type="hidden" value="${smtTerm }" name="smtTerm" />
                <input type="hidden" id="step" name="step" value="pre_join_step2" />
                <input type="hidden" id="svc_id" name="svc_id" value="${svc_id}" />
                <input type="hidden" id="password" name="password"/>
                <input type="hidden" id="req_cust_id" name="req_cust_id"/>
                
                <article id="joinYN">
                    <h2>가입여부 조회</h2>
                    <h3>(소드원 계정이 있으시면 셀러봇캐시 통합 아이디로 이용할 수 있습니다. 없으실 경우, 건너뛰기를 눌러주세요)</h3>
                    <div class="grayBox">
                        <div class="center">
                            <p>소드원 아이디(이메일)</p>
                            <div class="flexRow emailRow">
                                <div class="left">
                                    <input type="text" id="email" placeholder="이메일 입력" targetId="email">
                                    <span>@</span>
                                    <select id="domain">
                                        <option value="manual" selected>직접입력</option>
                                        <option value="naver.com">naver.com</option>
                                        <option value="daum.net">daum.net</option>
                                        <option value="hanmail.net">hanmail.net</option>
                                        <option value="gmail.com">gmail.com</option>
                                        <option value="nate.com">nate.com</option>
                                        <option value="me.com">me.com</option>
                                        <option value="icloud.com">icloud.com</option>
                                        <option value="hotmail.com">hotmail.com</option>
                                        <option value="yahoo.com">yahoo.com</option>
                                    </select>
                                    <input type="text" id="manualDomain" targetId="email">
                                </div>
                                <span id="emailMsg" class="underMsg">이메일을 입력해주세요</span>
                            </div>
                            <p>비밀번호</p>
                            <div class="row">
                                <input type="password" id="pw" placeholder="비밀번호 입력">
                                <span class="underMsg">영문, 숫자, 특수문자를 조합하여 8자 이상 ~20자 이내로 입력 ( 사용가능한 특수문자 : !@#$%^&*+=-[] )</span>
                                <span id="passwdMsg" class="errMsg">비밀번호가 형식에 맞지 않습니다.</span>
                            </div>
                        </div>
                    </div>
                </article>
            </form>
        </section>
    </main>
</body>