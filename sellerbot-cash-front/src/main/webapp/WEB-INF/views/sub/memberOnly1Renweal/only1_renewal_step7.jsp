<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_sodeone/css/sodeOne_07.css">
    <script type="module" src="/assets/new_sodeone/js/sodeOne_07.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <section>
            <button type="button" class="fixed active" id="nextStep">
                <span>통합하기</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>통합 회원가입</h1>
                    <img src="/assets/new_join/img/sellerbotLogo_sodeOnePage.png" alt="sellerbot">
                    <img src="/assets/new_join/img/sodeOneLogo_sodeOnePage.png" alt="sodeOneLogo">
                </div>
            </article>
            <article id="idUnion">
                <h2>셀러봇캐시 아이디 통합 <small>(이용하고 계신 서비스의 아이디를 셀러봇캐시 아이디로 통합할 수 있습니다.)</small></h2>
                <table>
                    <thead>
                        <tr>
                            <td>구분</td>
                            <td>아이디</td>
                            <td>사업자번호</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>셀러봇캐시</td>
                            <td class="grayText">${extCustDTO.cust_id }</td>
                            <td class="grayText">${extCustDTO.biz_no }</td>
                        </tr>
                    <%--<tr>
                            <td>소드원</td>
                            <td class="grayText">${value.cust_id}</td>
                            <td class="grayText">${value.biz_no}</td>
                        </tr>--%>
                    </tbody>
                </table>
            </article>
        </section>
    </main>
    <form id="fm" name="fm" action="/pub/member_only1/integrate" method="post">
        <input type="hidden" name="sodeOneTerm" value="${sodeOneTerm }">
        <input type="hidden" name="sellerbotTerm" value="${sellerbotTerm }">
        <input type="hidden" name="svc_biz_no" value="${svc_biz_no }">
        <input type="hidden" name="svc_cust_id" value="${svc_cust_id }">
        <input type="hidden" name="cust_id" value="${cust_id }">
    </form>
</body>