<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_sodeone/css/sodeOne_04.css">
    <script type="module" src="/assets/new_sodeone/js/sodeOne_04.js?ver=20231122_01" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <section>
            <button type="button" class="fixed" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <button type="button" class="fixed3" id="talkInfo">
                <img class="normal" src="/assets/new_join/img/talkInfo.png" alt="talkInfo">
            </button>
            <article id="top">
                <div class="title">
                    <h1>통합 회원가입</h1>
                    <img src="/assets/new_join/img/sellerbotLogo_sodeOnePage.png" alt="sellerbot">
                    <img src="/assets/new_join/img/sodeOneLogo_sodeOnePage.png" alt="sodeOneLogo">
                    <img id="talkInfo" class="mini" src="/assets/new_join/img/talkInfo_mini.png" alt="talkInfo">
                </div>
                <div class="processBar">
                    <div class="first step">
                        <span class="line"></span>
                        <p>약관동의</p>
                    </div>
                    <div class="second step">
                        <span class="line"></span>
                        <p>가입여부 조회</p>
                    </div>
                    <div class="third step">
                        <span class="line"></span>
                        <p>이메일 본인인증</p>
                    </div>
                    <div class="forth step active">
                        <span class="line"></span>
                        <p>사업자정보입력</p>
                    </div>
                    <div class="fifth step">
                        <span class="line"></span>
                        <p>결제수단 등록</p>
                    </div>
                </div>
            </article>
            </article>
            <form onsubmit="return false;" id="form1" name="form1" method="post" autocomplete="off" novalidate="novalidate">
                <input type="hidden" value="${svc_biz_no }" name="svc_biz_no" />
                <input type="hidden" id="auth_token" name="auth_token" />
                <input type="hidden" value="${sodeOneTerm }" name="sodeOneTerm" />
                <input type="hidden" value="${sellerbotTerm }" name="sellerbotTerm" />
                <input type="hidden" value="${smtTerm }" name="smtTerm" />
                <input type="hidden" value="${svc_cust_id }" id="svc_cust_id" name="svc_cust_id" />
                <article id="bizInfo">
                    <h2>사업자정보 입력</h2>
                    <div class="bg">
                        <div class="wrapper">
                            <p class="essential">아이디 (이메일)</p>
                            <div class="row">
                                <input type="text" id="cust_id" name="cust_id" placeholder="이메일 입력" value="${cust_id}" readonly
                                style="background-color: #eeeeee;
                                color: #bbb;">
                            </div>
                            <p class="essential">비밀번호</p>
                            <div class="row marginBottomMore">
                                <input type="password" id="passwd"  name="passwd" placeholder="비밀번호 입력">
                                <span class="underMsg">영문, 숫자, 특수문자를 조합하여 8자 이상 ~20자 이내로 입력 ( 사용가능한 특수문자 : !@#$%^&*+=-[] )</span>
                                <span id="msg3" class="errMsg">비밀번호가 형식에 맞지 않습니다.</span>
                            </div>
                            <p class="essential">비밀번호 확인</p>
                            <div class="row">
                                <input type="password" id="passwd_same" name="passwd_same" placeholder="비밀번호 입력">
                                <!-- 20230310 수정 -->
                                <span class="underMsg">비밀번호를 한 번 더 입력해주세요.</span>
                                <!-- 20230310 수정 -->
                                <span id="msg4" class="errMsg">비밀번호가 일치하지 않습니다.</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg">
                        <div class="wrapper">
                            <p class="essential">상호명</p>
                            <div class="row">
                                <input type="text" id="biz_nm" name="biz_nm" placeholder="상호명 입력">
                                <span id="msg1" class="underMsg red">상호명을 입력해주세요.</span>
                            </div>
                            <p class="essential">사업자등록번호</p>
                            <div class="flexRow">
                                <input class="onlyNumber" type="text" id="biz_no" name="biz_no" placeholder="사업자등록번호 입력" maxlength="10">
                                <button type="button" id="bizNumCheck">중복확인</button>
                                <span class="underMsg">('-'없이 숫자만 입력)</span>
                                <span id="biz_noMsg" class="errMsg">사업자번호를 입력해주세요.</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg">
                        <div class="wrapper">
                            <p class="essential">대표자명</p>
                            <div class="row">
                                <input type="text" id="ceo_nm" name="ceo_nm" placeholder="대표자명 입력">
                                <span id="msg3" class="underMsg red">대표자명을 입력해주세요.</span>
                            </div>
                            <p class="essential">대표자 연락처</p>
                            <div class="flexRow">
                                <select id="ceo_no_svc" name="ceo_no_svc">
                                    <option value="010" selected>010</option>
                                </select>
                                <input class="onlyNumber" type="text" id="ceo_no" name="ceo_no" maxlength="8">
                                <span class="underMsg marginLeft">('-'없이 숫자입력 / 입력하신 연락처로 중요한 공지 및 혜택을 안내드리거나 알림톡
                                    리포트를 보내드리고 있어 정확히 입력해주세요.)</span>
                                <span id="msg4" class="errMsg">대표자 연락처를 입력해주세요.</span>
                            </div>
                            <p class="question">알림톡 신청 시간 <img id="talkInfo" src="/assets/new_join/img/questionBtn.svg" alt="manual open"></p>
                            <div class="row">
                                <select id="trs_hop_hour1" name="trs_hop_hour1">
                                    <option value="">알림톡 신청시간 선택</option>
                                    <option value="08">08:00</option>
                                    <option value="09">09:00</option>
                                    <option value="10" selected>10:00</option>
                                    <option value="11">11:00</option>
                                    <option value="12">12:00</option>
                                    <option value="13">13:00</option>
                                    <option value="14">14:00</option>
                                    <option value="15">15:00</option>
                                    <option value="16">16:00</option>
                                    <option value="17">17:00</option>
                                    <option value="18">18:00</option>
                                    <option value="19">19:00</option>
                                    <option value="20">20:00</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="bg">
                        <div class="wrapper">
                            <p>담당자명(선택)</p>
                            <div class="row">
                                <input type="text" id="chrg_nm" name="chrg_nm" placeholder="담당자명 입력">
                            </div>
                            <p>담당자 연락처(선택)</p>
                            <div class="flexRow">
                                <select id="charg_no_svc" name="charg_no_svc">
                                    <option value="010" selected>010</option>
                                </select>
                                <input class="onlyNumber" type="text" id="chrg_no" name="chrg_no" maxlength="8">
                                <span class="underMsg marginLeft">('-'없이 숫자입력 / 입력하신 연락처로 중요한 공지 및 혜택을 안내드리거나 알림톡
                                    리포트를 보내드리고 있어 정확히 입력해주세요.)</span>
                            </div>
                            <p class="question">알림톡 신청 시간 <img id="talkInfo" src="/assets/new_join/img/questionBtn.svg" alt="manual open"></p>
                            <div class="row">
                                <select id="trs_hop_hour2" name="trs_hop_hour2">
                                    <option value="" selected>알림톡 신청시간 선택</option>
                                    <option value="08">08:00</option>
                                    <option value="09">09:00</option>
                                    <option value="10">10:00</option>
                                    <option value="11">11:00</option>
                                    <option value="12">12:00</option>
                                    <option value="13">13:00</option>
                                    <option value="14">14:00</option>
                                    <option value="15">15:00</option>
                                    <option value="16">16:00</option>
                                    <option value="17">17:00</option>
                                    <option value="18">18:00</option>
                                    <option value="19">19:00</option>
                                    <option value="20">20:00</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="bg">
                        <div class="wrapper">
                            <p class="essential">유입경로</p>
                            <div class="row">
                                <select class="sctbox" id="inf_path_cd" name="inf_path_cd" required="required">
                                    <option value="">경로선택</option>
                                    <c:forEach items="${codeList }" var="code" varStatus="state">
                                        <option value="${code.com_cd}">${code.cd_nm}</option>
                                    </c:forEach>
                                </select>
                                <span id="msg5" class="underMsg red">유입경로를 선택해주세요.</span>
                            </div>
                        </div>
                    </div>
                </article>
            </form>
        </section>
    </main>
</body>