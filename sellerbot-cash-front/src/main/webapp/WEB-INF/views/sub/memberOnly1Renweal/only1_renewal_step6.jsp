<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_sodeone/css/sodeOne_06.css">
    <script type="module" src="/assets/new_sodeone/js/sodeOne_06.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <section>
            <button type="button" class="fixed" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>통합 회원가입</h1>
                    <img src="/assets/new_join/img/sellerbotLogo_sodeOnePage.png" alt="sellerbot">
                    <img src="/assets/new_join/img/sodeOneLogo_sodeOnePage.png" alt="sodeOneLogo">
                </div>
            </article>
            <form id="form" onsubmit="return false;" action="/pub/member_only1/step4" method="post" name="form">
                <input type="hidden" value="${svc_biz_no }" name="svc_biz_no" />
                <input type="hidden" value="${cust_id }" name="svc_cust_id" />
                <input type="hidden" value="${sodeOneTerm }" name="sodeOneTerm" />
                <input type="hidden" value="${sellerbotTerm }" name="sellerbotTerm" />
                <article id="emailLogin">
                    <h2>셀러봇캐시 이메일 로그인 인증 필요 <small>(셀러봇캐시 통합 이메일 계정으로 소드원 서비스를 이용할 수 있습니다.)</small></h2>
                    <div class="grayBox">
                        <div class="center">
                            <p>셀러봇캐시 아이디(이메일)</p>
                            <div class="flexRow emailRow">
                                <div class="left">
                                    <input type="text" placeholder="셀러봇캐시 이메일아이디를 입력해주세요."
										id="cust_id" name="cust_id" value="${cust_id }" readonly="readonly"
										required="required" data-valitype="email" style="width: 100%;"/>
                                    <!-- <input type="text" id="email" placeholder="이메일 입력">
                                    <span>@</span>
                                    <select id="domain">
                                        <option value="manual" selected>직접입력</option>
                                        <option value="naver.com">naver.com</option>
                                        <option value="daum.net">daum.net</option>
                                        <option value="hanmail.net">hanmail.net</option>
                                        <option value="gmail.com">gmail.com</option>
                                        <option value="nate.com">nate.com</option>
                                        <option value="me.com">me.com</option>
                                        <option value="icloud.com">icloud.com</option>
                                        <option value="hotmail.com">hotmail.com</option>
                                        <option value="yahoo.com">yahoo.com</option>
                                    </select>
                                    <input type="text" id="manualDomain"> -->
                                </div>
                                <!-- <span id="msg1" class="underMsg">이메일을 입력해주세요</span> -->
                            </div>
                            <p>비밀번호</p>
                            <div class="row">
                                <input type="password" id="pw" placeholder="비밀번호 입력">
                                <span class="underMsg">영문, 숫자, 특수문자를 조합하여 8자 이상 ~20자 이내로 입력 ( 사용가능한 특수문자 : !@#$%^&*+=-[] )</span>
                            </div>
                        </div>
                    </div>
                </article>
            </form>
        </section>
    </main>
</body>