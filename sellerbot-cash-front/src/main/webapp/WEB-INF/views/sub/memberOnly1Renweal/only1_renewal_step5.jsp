<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_sodeone/css/sodeOne_05.css">
    <script type="module" src="/assets/new_sodeone/js/sodeOne_05.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<script>
	window.onpageshow = function(event) {
		if ( event.persisted || (window.performance && window.performance.navigation.type == 2)) {
		// Back Forward Cache로 브라우저가 로딩될 경우 혹은 브라우저 뒤로가기 했을 경우
            location.href="/pub/member_only1/step5";
		}
	}
</script>
<body>
    <main>
        <input type="hidden" id="DeviceType" value="${DeviceType}">
        <!-- 상품정보 가지고 올거임  -->
        <input type="hidden" name="goodsList"  id="goodsList"    value="">
        <!-- 두개 값 모르겟음 -->
        <input type="hidden" name="freeTrialYn" id="freeTrialYn"   value="Y"/>
        <input type="hidden" name="eventNo" id="eventNo" value="0">
        <!-- price, goodsName, typeCd, optNo -->
        <input type="hidden" name="price" id="price" value="${goodsInfo.FNA_PRC}">
        <input type="hidden" name="goodsName" id="goodsName" value="${goodsInfo.GOODS_NM}">
        <input type="hidden" name="typeCd" id="typeCd" value="${goodsInfo.GOODS_TYP_CD}">
        <input type="hidden" name="optNo" id="optNo" value="${goodsInfo.GOODS_OPT_SEQ_NO}">
    
        <!-- 결제 실패시 결과 값 -->
        <input type="hidden" name="successYN" id="successYN" value="${successYN}">
        <input type="hidden" name="errMsg" id="errMsg" value="${errMsg}" />

        <section>
            <button type="button" class="fixed" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>통합 회원가입</h1>
                    <img src="/assets/new_join/img/sellerbotLogo_sodeOnePage.png" alt="sellerbot">
                    <img src="/assets/new_join/img/sodeOneLogo_sodeOnePage.png" alt="sodeOneLogo">
                </div>
                <div class="processBar">
                    <div class="first step">
                        <span class="line"></span>
                        <p>약관동의</p>
                    </div>
                    <div class="second step">
                        <span class="line"></span>
                        <p>가입여부 조회</p>
                    </div>
                    <div class="third step">
                        <span class="line"></span>
                        <p>이메일 본인인증</p>
                    </div>
                    <div class="forth step">
                        <span class="line"></span>
                        <p>사업자정보입력</p>
                    </div>
                    <div class="fifth step active">
                        <span class="line"></span>
                        <p>결제수단 등록</p>
                    </div>
                </div>
            </article>
            <article id="payInfo">
                <h2>이용권 결제정보</h2>
                <div class="ticketInfo">
                    <div class="top">소드원 쇼핑몰론 고객전용 이용권</div>
                    <div class="flexArea">
                        <div class="left">
                            <div class="flexRow">
                                <div class="title">월 이용료</div>
                                <div class="content">
                                    <span class="circle check"></span>
                                    <p class="grayText">정상가 월  
                                        <fmt:formatNumber value="${goodsInfo.GOODS_BAS_PRC}" pattern="#,###" />
                                          원
                                    </p>
                                    <p class="bigText">할인가 월 
                                        <fmt:formatNumber value="${goodsInfo.FNA_PRC}" pattern="#,###" />
                                        원
                                    </p>
                                </div>
                            </div>
                            <div class="flexRow">
                                <div class="title">가입비</div>
                                <div class="content">
                                    <span class="circle check"></span>
                                    <p class="grayText">3만원</p>
                                    <p class="bigText">무료</p>
                                </div>
                            </div>
                            <div class="flexRow">
                                <div class="title">판매몰 계좌</div>
                                <div class="content">
                                    <span class="check"></span>
                                    <p>무제한등록</p>
                                </div>
                            </div>
                            <div class="flexRow">
                                <div class="title">판매몰 ID</div>
                                <div class="content">
                                    <span class="check"></span>
                                    <p>무제한등록</p>
                                </div>
                            </div>
                        </div>
                        <div class="right">
                            <div class="flexRow">
                                <div class="title">데이터 조회</div>
                                <div class="content column">
                                    <div class="flexText">
                                        <span class="check"></span>
                                        <p>일자별 상세 정산 내역</p>
                                    </div>
                                    <div class="flexText">
                                        <span class="check"></span>
                                        <p>판매통계분석 및 그래프</p>
                                    </div>
                                </div>
                            </div>
                            <div class="flexRow">
                                <div class="title">리포팅 서비스</div>
                                <div class="content">
                                    <span class="check"></span>
                                    <p>매일 정산예정금 알림톡리포트 서비스</p>
                                </div>
                            </div>
                            <div class="flexRow">
                                <div class="title">전용 상담 서비스</div>
                                <div class="content">
                                    <span class="check"></span>
                                    <p>고객상담 우선지원서비스 제공</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grayBox">
                    <p>가입을 위해서는 결제수단 등록이 필수이며, 결제수단 등록 시 무료체험 14일이 제공됩니다.<br>해지는 언제든지 가능하며 체험 종료 후 매월 자동 정기결제 됩니다.</p>
                </div>
            </article>
            <article id="terms">
                <h2>약관동의</h2>
                <div class="accordion">
                    <div class="title">
                        <div class="left">
                            <div class="checkBoxDiv">
                                <input type="checkbox" id="terms_01" class="chkBox">
                                <label class="checkBox" for="terms_01"></label>
                                <label class="text" for="terms_01" style="color:black;"></label>
                            </div>
                            <p class="text_checkBox"><b>[필수]</b> 셀러봇캐시 유료결제 약관</p>
                        </div>
                        <span><img src="/assets/new_join/img/right-chevron.png" alt="right-chevron"></span>
                    </div>
                    <div class="panel">
                        <div class="paddingBox">
                            <pre id="termContent"></pre>
                        </div>
                    </div>
                </div>
            </article>
            <article id="payment">
                <h2>결제수단</h2>
                <button type="button" id="creditCard">신용카드</button>
            </article>
            <article id="caution">
                <h2>유의사항</h2>
                <div class="grayBox">
                    <pre>셀러봇캐시 가입을 위해서는 이용권 정기 결제를 위한 결제 수단 등록이 필수입니다.

첫 결제수단 등록 시, 무료체험 14일이 제공되며, 체험 종료 후 매월 정기결제가 진행됩니다.
결제 7일 전 알림을 보내드리고 있는 점 참고해주세요.

해지는 언제든지 가능하며, 해지 경로는 아래와 같습니다.
- [셀러봇캐시 홈페이지 → 마이페이지 → 결제정보 → (이용권 우측) 해지]
<!-- 20230323 문구 추가 -->
가상계좌 무통장입금 등 다른 결제 방식을 원하시는 경우, 우측 하단의 '1:1 상담톡' 을 통해 문의 해주세요.
                    </pre>
                </div>
            </article>
        </section>
        <c:choose>
            <c:when test="${DeviceType eq 'PC'}">
                <c:choose>
                    <c:when test="${mid eq 'INIBillTst'}">
                        <script language='javascript' type='text/javascript' src='https://stgstdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>
                    </c:when>
                    <c:otherwise>
                        <script language='javascript' type='text/javascript' src='https://stdpay.inicis.com/stdjs/INIStdPay.js' charset='UTF-8'></script>
                    </c:otherwise>
                </c:choose>
                <form id="SendPayForm_id" method="POST">
                    <input type="hidden" name="version" value="1.0" >
                    <input type="hidden" name="mid" value="${mid}">
                    <input type="hidden" name="goodname">
                    <input type="hidden" name="oid">
                    <input type="hidden" name="price">
                    <input type="hidden" name="currency" value="WON">
                    <input type="hidden" name="buyername" value=${buyername}>
                    <input type="hidden" name="buyertel" value="${buyertel}">
                    <input type="hidden" name="buyeremail" value="${buyeremail}">
                    <input type="hidden" name="timestamp">
                    <input type="hidden" name="signature">
                    <input type="hidden" name="returnUrl" value="${siteDomain}/pub/member_only1/payment/payresponse">
                    <input type="hidden" name="closeUrl" value="${siteDomain}/pub/payment/pay_close">
                    <input type="hidden" name="mKey" value="${mKey}">
                    <input type="hidden" name="gopaymethod" value="">
                    <input type="hidden" name="acceptmethod" value="BILLAUTH(card):FULLVERIFY"> 
                    <input type="hidden" name="merchantData"> 
                    <input type="hidden" name="offerPeriod" value="M2">
                </form>
            </c:when>
            <c:otherwise>
                <form id="SendPayForm_id" name="SendPayForm_id" method="POST" action="https://inilite.inicis.com/inibill/inibill_card.jsp">
                    <input type="hidden" name="mid" value="${mid}">
                    <input type="hidden" name="buyername" value=${buyername}>            
                    <input type="hidden" name="goodname">
                    <input type="hidden" name="price">
                    <input type="hidden" name="orderid">
                    <input type="hidden" name="returnurl" value="${siteDomain}/pub/member_only1/payment/mobilepayresponse">
                    <input type="hidden" name="timestamp">
                    <input type="hidden" name="period" value="M2">
                    <input type="hidden" name="p_noti">
                    <input type="hidden" name="hashdata">
                    <input type="hidden" name="buyername" value=${buyername}>
                    <input type="hidden" name="buyertel" value="${buyertel}">
                    <input type="hidden" name="buyeremail" value="${buyeremail}">
                </form>
            </c:otherwise>
        </c:choose>
    </main>
</body>