<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_sodeone/css/sodeOne_01.css">
    <script type="module" src="/assets/new_sodeone/js/sodeOne_01.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <form id="form" name="form" action="/pub/member_only1/step2" method="post">
            <input type="hidden" id="svc_cust_id" name="svc_cust_id" value="${svc_cust_id }" />
            <input type="hidden" id="svc_biz_no" name="svc_biz_no" value="${svc_biz_no}" />
            <input type="hidden" id="svc_id" name="svc_id" value="${svc_id}" />
            <input type="hidden" id="step" name="step" value="join_step1" />    
            <section>
                <button type="button" class="fixed" id="nextStep">
                    <span>다음</span>
                    <img src="/assets/new_join/img/rightArrow.svg" />
                </button>
                <article id="top">
                    <div class="title">
                        <h1>통합 회원가입</h1>
                        <img src="/assets/new_join/img/sellerbotLogo_sodeOnePage.png" alt="sellerbot">
                        <img src="/assets/new_join/img/sodeOneLogo_sodeOnePage.png" alt="sodeOneLogo">
                    </div>
                    <div class="processBar">
                        <div class="first step active">
                            <span class="line"></span>
                            <p>약관동의</p>
                        </div>
                        <div class="second step">
                            <span class="line"></span>
                            <p>가입여부 조회</p>
                        </div>
                        <div class="third step">
                            <span class="line"></span>
                            <p>이메일 본인인증</p>
                        </div>
                        <div class="forth step">
                            <span class="line"></span>
                            <p>사업자정보입력</p>
                        </div>
                        <div class="fifth step">
                            <span class="line"></span>
                            <p>결제수단 등록</p>
                        </div>
                    </div>
                </article>
                <article id="termsAgree">
                    <h2>통합 약관 동의</h2>
                    <div class="formBg">
                        <div class="formDiv">
                            <div class="checkAll">
                                <div class="checkBoxDiv">
                                    <input type="checkbox" id="checkAll" class="chkBox">
                                    <label class="checkBox" for="checkAll"></label>
                                    <label class="text" for="checkAll" style="color:black;"></label>
                                </div>
                                <!-- 20230310 수정 -->
                                <p class="text_checkBox">셀러봇캐시 약관 전체 동의</p>
                            </div>
                            <div class="grayBox">
                                <pre>전체 동의에는 필수 및 선택 정보에 대한 동의가 포함되어 있으며, 개별적으로 동의를 선택 하실 수 있습니다. 선택 항목에 대한 동의를 거부하시는 경우에도 서비스 이용이 가능합니다.</pre>
                            </div>
                            <c:forEach var="titem" items="${sellerbotTerms}" varStatus="status">
                                <c:set var="termsId" value="${titem.terms_id}" />
                                <div class="accordion">
                                    <div class="title">
                                        <div class="left">
                                            <div class="checkBoxDiv">
                                                <input type="checkbox" id="terms_0${status.count}"
                                                name="sellerbotTerm" value="${termsId}" term_esse_yn="${titem.terms_esse_yn}"
                                                term_type_cd="${titem.terms_typ_cd}" class="chkBox"  />
                                                <label class="checkBox" for="terms_0${status.count}"></label>
                                                <label class="text" for="terms_0${status.count}" style="color:black;"></label>
                                            </div>
                                            <p class="label">
                                                <c:if test="${titem.terms_esse_yn == 'Y'}">
                                                    <b>[필수]</b>
                                                </c:if>
                                                <c:if test="${titem.terms_esse_yn != 'Y'}">
                                                    [선택]
                                                </c:if>
                                                ${titem.terms_title}
                                            </p>
                                        </div>
                                        <span><img src="/assets/new_join/img/right-chevron.png" alt="right-chevron"></span>
                                    </div>
                                    <c:choose>
                                        <c:when test="${titem.terms_typ_cd == 'SMT'}">
                                            <div class="panel marketing">
                                                <div class="paddingBox">
                                                    <pre>${titem.terms_cont}</pre>
                                                </div>
                                            </div>
                                            <div class="checkBoxRow">
                                                    <c:forEach items="${smtTermsList}" var="smt">
                                                    <div class="checkBoxDiv square">
                                                        <input type="checkbox" id="${smt.com_cd}" name="smtTerm" value="${smt.com_cd}" class="chkBox">
                                                        <label class="checkBox" for="${smt.com_cd}"></label>
                                                        <label class="text" for="${smt.com_cd}" style="color:black;">${smt.cd_nm}</label>
                                                    </div>
                                                </c:forEach>   
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="panel">
                                                <div class="paddingBox">
                                                    <pre>
                                                        ${titem.terms_cont}
                                                    </pre>
                                                </div>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="formBg">
                        <div class="formDiv">
                            <div class="checkAll">
                                <div class="checkBoxDiv">
                                    <input type="checkbox" id="checkAll2" class="chkBox">
                                    <label class="checkBox" for="checkAll2"></label>
                                    <label class="text" for="checkAll2" style="color:black;"></label>
                                </div>
                                <!-- 20230310 수정 -->
                                <p class="text_checkBox">소드원 약관 전체 동의</p>
                            </div>
                            <div class="grayBox">
                                <pre>전체 동의에는 필수 및 선택 정보에 대한 동의가 포함되어 있으며, 개별적으로 동의를 선택 하실 수 있습니다. 선택 항목에 대한 동의를 거부하시는 경우에도 서비스 이용이 가능합니다.</pre>
                            </div>
                            <c:forEach var="titem" items="${sodeOneTerms}" varStatus="status">
                                <c:set var="termsId" value="${titem.terms_id}" />
                                <div class="accordion">
                                    <div class="title">
                                        <div class="left">
                                            <div class="checkBoxDiv">
                                                <input type="checkbox" id="sode_terms_0${status.count}"
                                                name="sodeOneTerm" value="${termsId}" term_esse_yn="${titem.terms_esse_yn}"
                                                term_type_cd="${titem.terms_typ_cd}" class="chkBox sode"  />
                                                <label class="checkBox" for="sode_terms_0${status.count}"></label>
                                                <label class="text" for="sode_terms_0${status.count}" style="color:black;"></label>
                                            </div>
                                            <p class="label">
                                                <c:if test="${titem.terms_esse_yn == 'Y'}">
                                                    <b>[필수]</b>
                                                </c:if>
                                                <c:if test="${titem.terms_esse_yn != 'Y'}">
                                                    [선택]
                                                </c:if>
                                                ${titem.terms_title}
                                            </p>
                                        </div>
                                        <span><img src="/assets/new_join/img/right-chevron.png" alt="right-chevron"></span>
                                    </div>
                                    <c:choose>
                                        <c:when test="${titem.terms_typ_cd == 'SMT'}">
                                            <div class="panel marketing">
                                                <div class="paddingBox">
                                                    <pre>${titem.terms_cont}</pre>
                                                    <img src="/assets/new_join/img/step_02_marketing.png" alt="table">
                                                </div>
                                            </div>
                                            <div class="checkBoxRow">
                                                    <c:forEach items="${smtTermsList}" var="smt">
                                                    <div class="checkBoxDiv square">
                                                        <input type="checkbox" id="${smt.com_cd}" name="smtTerm" value="${smt.com_cd}" class="chkBox">
                                                        <label class="checkBox" for="${smt.com_cd}"></label>
                                                        <label class="text" for="${smt.com_cd}" style="color:black;">${smt.cd_nm}</label>
                                                    </div>
                                                </c:forEach>   
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="panel">
                                                <div class="paddingBox">
                                                    <pre>
                                                        ${titem.terms_cont}
                                                    </pre>
                                                </div>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </article>
            </section>
        </form>
    </main>
</body>

