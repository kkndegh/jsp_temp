<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<head>
    <link rel="stylesheet" href="/assets/new_sodeone/css/sodeOne_03.css">
    <script type="module" src="/assets/new_sodeone/js/sodeOne_03.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>
<body>
    <main>
        <section>
            <button type="button" class="fixed" id="nextStep">
                <span>다음</span>
                <img src="/assets/new_join/img/rightArrow.svg" />
            </button>
            <article id="top">
                <div class="title">
                    <h1>통합 회원가입</h1>
                    <img src="/assets/new_join/img/sellerbotLogo_sodeOnePage.png" alt="sellerbot">
                    <img src="/assets/new_join/img/sodeOneLogo_sodeOnePage.png" alt="sodeOneLogo">
                </div>
                <div class="processBar">
                    <div class="first step">
                        <span class="line"></span>
                        <p>약관동의</p>
                    </div>
                    <div class="second step">
                        <span class="line"></span>
                        <p>가입여부 조회</p>
                    </div>
                    <div class="third step active">
                        <span class="line"></span>
                        <p>이메일 본인인증</p>
                    </div>
                    <div class="forth step">
                        <span class="line"></span>
                        <p>사업자정보입력</p>
                    </div>
                    <div class="fifth step">
                        <span class="line"></span>
                        <p>결제수단 등록</p>
                    </div>
                </div>
            </article>
            <form onsubmit="return false;" id="form1" name="form1" method="post" autocomplete="off" novalidate="novalidate">
                <input type="hidden" value="${svc_biz_no }" name="svc_biz_no" />
                <input type="hidden" id="auth_token" name="auth_token" />
                <input type="hidden" value="${sodeOneTerm }" name="sodeOneTerm" />
                <input type="hidden" value="${sellerbotTerm }" name="sellerbotTerm" />
                <input type="hidden" value="${smtTerm }" name="smtTerm" />
                <input type="hidden" value="${svc_cust_id }" id="svc_cust_id" name="svc_cust_id" />
                <input type="hidden" value=""  id="cust_id" name="cust_id" />
                <input type="hidden" value="" id="sendEmailHidInfo" name="sendEmailHidInfo" />
                <article id="joinYN">
                    <h2>이메일 본인인증</h2>
                    <div class="grayBox">
                        <div class="center">
                            <p>셀러봇캐시 아이디(이메일)*</p>
                            <div class="flexRow emailRow">
                                <div class="left">
                                    <input type="text" id="email" placeholder="이메일 입력" targetId="email">
                                    <span>@</span>
                                    <select id="domain">
                                        <option value="manual" selected>직접입력</option>
                                        <option value="naver.com">naver.com</option>
                                        <option value="daum.net">daum.net</option>
                                        <option value="hanmail.net">hanmail.net</option>
                                        <option value="gmail.com">gmail.com</option>
                                        <option value="nate.com">nate.com</option>
                                        <option value="me.com">me.com</option>
                                        <option value="icloud.com">icloud.com</option>
                                        <option value="hotmail.com">hotmail.com</option>
                                        <option value="yahoo.com">yahoo.com</option>
                                    </select>
                                    <input type="text" id="manualDomain" targetId="email">
                                </div>
                                <button id="reqVerificationCode" style="margin-top: 5px;">인증번호 요청</button>
                                <span id="emailMsg" class="underMsg">이메일을 입력해주세요</span>
                            </div>
                            <p class="essential">인증번호</p>
                            <div class="flexRow">
                                <div class="left">
                                    <input class="onlyNumber" type="text" id="verificationCode" placeholder="인증번호 입력">
                                </div>
                                <button id="confirmVerificationCode">확인</button>
                                <span id="msg2" class="underMsg red">인증번호를 다시 확인해주세요.</span>
                            </div>
                        </div>
                    </div>
                </article>
            </form>
        </section>
    </main>
</body>