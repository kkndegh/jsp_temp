<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link rel="stylesheet" href="/assets/css/page.css">

<div class="my_wra cafe24_wra">
    <div class="con_wra">
        <div class="site_logo"><img src="/assets/images/cafe24/intro_logo.png" alt=""></div>
        <img src="/assets/images/cafe24/01.png" alt="">
        <form action="/cafe24/launch" id="frm_auth" name="frm_auth" method="post" target="_blank">
            <input type="hidden" name="app" value="${app}">
            <input type="hidden" name="mall_id" value="${mall_id}">
            <input type="hidden" name="state" value="${state}">
            <input type="hidden" name="code" value="${code}">
            <div class="button_container">
                <button class="button primary">
                    <c:choose>
                        <c:when test="${app eq 'bankbot'}">뱅크봇 접속</c:when>
                        <c:otherwise>셀러봇캐시 접속</c:otherwise>
                    </c:choose>
                </button>
            </div>
        </form>
    </div>
</div>
