<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>셀러봇캐시 뱅크봇 회원가입</title>
    <link rel="stylesheet" href="/assets/css/bankbot_cafe24_01.css">
    <script type="module" src="/assets/js/bankbot_cafe24_01.js" defer></script>
    <script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
</head>

<body>
    <main>
        <div class="head">
            <h5>뱅크봇 자동입금 대사서비스 회원가입</h5>
            <img src="/assets/images/cafe24/sellerbotLogo.svg" alt="sellerbot cash">
        </div>
        <div class="body">
            <p>자동입금 대사 서비스 이용을 위해 약관 동의 후 가입을 해주세요.</p>
            <h5>이용약관 동의</h5>
            <div class="accordionWrap">
                <c:forEach var="titem" items="${bankbotTerms}" varStatus="status">
                    <c:set var="termsId" value="${titem.terms_id}" />
                    <div class="accordion">
                        <div class="title">
                            <div class="left">
                                <div class="checkBoxDiv">
                                    <input type="checkbox" id="terms_0${status.count}"
                                    name="sellerbotTerm" value="${termsId}" term_esse_yn="${titem.terms_esse_yn}"
                                    term_type_cd="${titem.terms_typ_cd}" class="chkBox"  />
                                    <label class="checkBox" for="terms_0${status.count}"></label>
                                    <label class="text" for="terms_0${status.count}" style="color:black;"></label>
                                </div>
                                <!-- 20230310 수정 -->
                                <p class="text_checkBox">
                                    <c:if test="${titem.terms_esse_yn == 'Y'}">
                                        <b>[필수]</b>
                                    </c:if>
                                    <c:if test="${titem.terms_esse_yn != 'Y'}">
                                        [선택]
                                    </c:if>
                                    ${titem.terms_title}
                                </p>
                            </div>
                            <span>자세히보기 <img src="/assets/images/cafe24/right-chevron.png" alt="right-chevron"></span>
                        </div>
                        <div class="panel">
                            <div class="paddingBox">
                                <pre>
                                    ${titem.terms_cont}
                                </pre>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <span class="line"></span>
            <h5>가입정보</h5>
            <p class="input_title">법인명(상호)</p>
            <input type="text" class="preval" id="cp_name" value="${company_name}" placeholder="법인명 입력" readonly="readonly">
            <p class="input_title">대표자</p>
            <input type="text" class="preval" id="owner_name" value="${president_name}" placeholder="대표자명 입력" readonly="readonly">
            <p class="input_title">사업자 번호</p>
            <input type="text" class="preval" id="cp_num" value="${company_registration_no}" placeholder="사업자번호 입력" class="onlyNumber" readonly="readonly">
            <p class="input_title">이메일</p>
            <input type="text" class="preval" id="email" value="${email}" placeholder="이메일 입력" readonly="readonly">
            <p class="input_title">휴대폰 번호</p>
            <input type="text" id="phone" placeholder="휴대폰 번호 입력(숫자만 입력해주세요)" class="onlyNumber">
            <p class="dot" style="margin-top:30px;">이메일은 자사 웹사이트(셀러봇캐시)의 ID로 사용됩니다.</p>
            <p class="dot">회원가입 안내, 주요정보는 이메일과 휴대폰번호로 발송해 드립니다.</p>
            <button id="confirm" disabled>가입하기</button>

            <input type="hidden" id="encodeVal" value="${encodeVal}"/>
        </div>
    </main>
</body>

</html>
