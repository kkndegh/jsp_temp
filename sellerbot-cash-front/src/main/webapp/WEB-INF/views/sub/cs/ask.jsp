<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:eval expression="@environment.getProperty('internal.redirect.base')" var="redirectBase"/>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 

<div class="container">
    <div class="cs_wra">
        <div class="menu_top">
            <p class="menu_name">고객센터</p>
            <div class="gnb_topBox">
                <ul class="gnb_top clearfix">
					<li><a href="/pub/cs/notice">공지사항</a></li>
					<li><a href="/pub/cs/event">이벤트</a></li>
					<li><a href="/pub/cs/faq">FAQ</a></li>
					<li class="focus"><a href="/pub/cs/ask">문의하기</a></li>
				</ul>
            </div>
        </div>
        <div class="title_bg">
            <p class="title">문의하기</p>
            <p class="desc">셀러봇캐시에게 문의하시면 사람인척하는 AI가 답하거나, 사람다운 사람들이 모여서 일하는 온리원의 진짜 사람이 답해드립니다. </p>
        </div>
        <div class="cs_area">
            <!-- <a href="/sub/service/guide" class="p_icon"><img src="/assets/images/member/premiumService_button.png" alt=""></a> -->
            <div class="cs_section">
                <section class="ask_type">
                    <a href="javascript:clickKaKao();" class="type_ask_box">
                        <div class="imgbox"><img src="/assets/images/cs/sns_img.png" alt=""></div>
                        <p class="name">카카오톡 문의하기</p>
                        <p class="desc">셀러봇캐시 서비스의<br>
                            플러스 친구가 되어주세요.<br>
                            언제, 어디서든 1:1 문의가 가능합니다.</p>
                        <button class="kakao" onclick="clickKaKao();return false;"><img src="/assets/images/cs/kakao_Icon.png" alt="">카카오톡 문의하기</button>
                    </a>
                    <a href="javascript:clickEmail();" class="type_ask_box">
                        <div class="imgbox"><img src="/assets/images/cs/img_1-1.png" alt=""></div>
                        <p class="name">1:1 문의하기</p>
                        <p class="desc">셀러봇캐시를 이용하시면서<br>
                        궁금하거나 어려운 사항이 생기시면<br>
                        무엇이든지 물어보세요.</p>
                        <button class="email" onclick="clickEmail();return false;">1:1문의 등록하기</button>
                    </a>
                </section>
            </div>
        </div>
    </div>
</div>
<script>
    function clickKaKao(){
        window.open('http://pf.kakao.com/_kfjyC/chat');
    }

    function clickEmail(){
        <security:authorize access="!isAuthenticated()">
            showConfirm("로그인 후 이용 가능 합니다.", function(){
                location.href='/login';
            }, {btnOK: "로그인하기",btnCancel: "취소"});
        </security:authorize>
        <security:authorize access="isAuthenticated()">
            location.href='/sub/my/ask';
        </security:authorize>
    }
</script>