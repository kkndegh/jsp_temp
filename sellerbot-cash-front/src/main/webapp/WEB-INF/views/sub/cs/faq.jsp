<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:eval expression="@environment.getProperty('internal.redirect.base')" var="redirectBase"/>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" /> 

<div class="container">
	<div class="cs_wra">
		<div class="menu_top">
			<p class="menu_name">고객센터</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<li><a href="/pub/cs/notice">공지사항</a></li>
					<li><a href="/pub/cs/event">이벤트</a></li>
					<li class="focus"><a href="/pub/cs/faq">FAQ</a></li>
					<li><a href="/pub/cs/ask">문의하기</a></li>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">FAQ</p>
			<p class="desc">셀러봇캐시가 궁금하신가요? 셀러봇 캐시에게 자주 묻는 질문을 모아서 보여드립니다.</p>
		</div>
		<div class="cs_area">
			<!-- <a href="/sub/service/guide.html" class="p_icon"><img src="/assets/images/member/premiumService_button.png" alt=""></a> -->
			<div class="cs_section">
				<section class="faq">
					<div class="sh_cs">
						<!-- <select class="sctbox">
                  <option value="">통합검색</option>
                </select> -->
						<div class="sh_box">
							<form name="faqForm" action="/pub/cs/faq" method="get">
								<input type="text" name="title" class="textbox_mb" placeholder="검색어를 입력하세요"
									value="${title }">
								<input name="page" type="hidden" value="${paging.pageNo}">
								<input id="faq_typ_cd" name="faq_typ_cd" type="hidden" value="${faq_typ_cd}">
								<button><img src="/assets/images/cs/search.png" alt=""></button>
							</form>
						</div>
					</div>
					<div class="cate_cs">
						<ul class="clearfix cate_cs_list_wrap">
							<li class="${empty faq_typ_cd ? 'focus':''}"><span>전체</span></li>
							<c:set var="cnt" value="1" />
							<c:set var="rowCnt" value="1" />
							<c:set var="codeLength" value="${fn:length(codeList)}" />
							<c:set var="noLiCnt" value="${(codeLength + 1) % 6}" />
							<c:set var="className" value="" />

							<c:forEach items="${codeList }" var="code" varStatus="status">
								<c:set var="cnt" value="${cnt + 1}" />

								<c:choose>
									<c:when test="${cnt lt 6}">
									</c:when>
									<c:when test="${cnt eq (6 * rowCnt)}">
										<c:set var="className" value="last" />
										<c:if test="${rowCnt gt 1}">
											<c:set var="className" value="line2 last" />
										</c:if>
										<c:set var="rowCnt" value="${rowCnt + 1}" />
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${cnt ne codeLength}">
												<c:set var="className" value="line2" />
											</c:when>
										</c:choose>
									</c:otherwise>
								</c:choose>

								<li ${faq_typ_cd eq code.com_cd ? 'class="focus"' : '' } data-faq_typ_cd="${code.com_cd}" class="${className}">
									<span>${code.cd_nm}</span>
								</li>
							</c:forEach>
							<c:if test="${noLiCnt ne '0'}">
								<c:set var="noCnt" value="${6-noLiCnt}" />
								<c:forEach var="i" begin="1" end="${noCnt}">
									<c:choose>
										<c:when test="${i eq noCnt}">
											<li class="line2 last noLi"><span></span></li>
										</c:when>
										<c:otherwise>
											<li class="line2 noLi"><span></span></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:if>
						</ul>
					</div>
					<div class="faq_question">
						<p class="title">자주하는 질문</p>
						<c:forEach items="${faqList.dataList}" var="list" varStatus="status">
							<div class="faq_q_box">
								<div class="lb"><span>${list.faq_typ_cd_nm}</span></div>
								<div class="q_text">
									<span class="icon"><img src="/assets/images/cs/circleQ.png" alt=""></span>
									<p>${list.title } <img src="/assets/images/cs/circleArrows.png" alt=""></p>
								</div>
							</div>

							<div class="faq_a_box">
								<div class="a_text">
									<c:if
										test="${not empty list.faq_file && list.faq_file.stor_file_loc_cd eq 'CUP' }">
										<p class="imgbox">
											<img src="/imagefile/${list.faq_file.stor_path}${list.faq_file.stor_file_nm}"
												alt="" style="width: 100%">
										</p>
									</c:if>
									<span class="icon"><img src="/assets/images/cs/circleA.png" alt=""></span>
									${list.asw_cont}
									<c:if
										test="${not empty list.faq_file && list.faq_file.stor_file_loc_cd eq 'CDW' }">
										<p class="imgbox">
											<img src="/imagefile/${list.faq_file.stor_path}${list.faq_file.stor_file_nm}"
												alt="" style="width: 100%">
										</p>
									</c:if>
								</div>
							</div>
						</c:forEach>
					</div>

					<div class="faq_question faq_question_m" style="display:none;">
						<p class="title">자주하는 질문</p>
						<c:forEach items="${faqList.dataList}" var="list" varStatus="status">
							<div class="faq_q_box">
								<div class="q_text">
									<span class="icon"><img src="/assets/images/cs/circleQ.png" alt=""></span>
									<div class="faq_q_box_right_wrap">
										<div class="lb" style="width: 90%"><span>${list.faq_typ_cd_nm}</span></div>
										<p style="width: 80%">${list.title } <img
												src="/assets/images/cs/circleArrows.png" alt=""></p>
									</div>
								</div>
							</div>
							<div class="faq_a_box">
								<div class="a_text">
									<c:if
										test="${not empty list.faq_file && list.faq_file.stor_file_loc_cd eq 'CUP' }">
										<p class="imgbox">
											<img src="/imagefile/${list.faq_file.stor_path}${list.faq_file.stor_file_nm}"
												alt="" style="width: 100%">
										</p>
									</c:if>
									<span class="icon"><img src="/assets/images/cs/circleA.png" alt=""></span>
									<p>${list.asw_cont }</p>
									<c:if
										test="${not empty list.faq_file && list.faq_file.stor_file_loc_cd eq 'CDW' }">
										<p class="imgbox">
											<img src="/imagefile/${list.faq_file.stor_path}${list.faq_file.stor_file_nm}"
												alt="" style="width: 100%">
										</p>
									</c:if>
								</div>
							</div>
						</c:forEach>
					</div>

					<jsp:include page="/WEB-INF/jsputils/pagination.jsp">
						<jsp:param name="firstPageNo" value="${paging.firstPageNo}" />
						<jsp:param name="prevPageNo" value="${paging.prevPageNo}" />
						<jsp:param name="startPageNo" value="${paging.startPageNo}" />
						<jsp:param name="pageNo" value="${paging.pageNo}" />
						<jsp:param name="endPageNo" value="${paging.endPageNo}" />
						<jsp:param name="nextPageNo" value="${paging.nextPageNo}" />
						<jsp:param name="finalPageNo" value="${paging.finalPageNo}" />
					</jsp:include>
				</section>
			</div>
		</div>
	</div>
</div>
<script>
	$(".faq_q_box .q_text p").click(function () {
		var faq = $(this).parents(".faq_q_box");
		if (faq.hasClass("active") == true) {
			faq.removeClass("active");
		} else {
			$(".faq_q_box").removeClass("active");
			faq.addClass("active");
		}

	});

	// 분류 선택
	$(".cate_cs li").click(function () {
		$(".cate_cs li").removeClass("focus");
		$(this).addClass("focus");
		var faq_typ_cd = $(this).data('faq_typ_cd');
		$("#faq_typ_cd").val(faq_typ_cd);
		document.faqForm.page.value = 1;
		document.faqForm.submit();
	});

	function goPage(p) {
		if (document.faqForm.page.value != p) {
			document.faqForm.page.value = p;
			document.faqForm.submit();
		}
	};
</script>