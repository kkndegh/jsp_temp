<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>

<link rel="stylesheet" type="text/css" href="/assets/css/jump_down.css" />
<script src="/assets/js/jump.js"></script>

<div class="container">
	<div class="jd_wra">
		<div class="menu_top">
			<p class="menu_name">점프 서비스</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<li><a href="/sub/jump/service">점프서비스</a></li>
					<li class="focus"><a href="/sub/jump/download">프로그램 설치안내</a></li>
				</ul>
			</div>
		</div>
		<div class="title_bg">
			<p class="title">프로그램 설치안내</p>
			<p class="desc">점프서비스 이용 고객님들은, 꼭 한번 읽어봐주세요</p>
		</div>
		<div class="jd_area">
			<div class="info_area">
				<p class="sub_title">버전 정보</P>
				<div class="sub_contents">

					<span class="app_down"> v1.1.7 정식버전 출시(2023-08-04)</span>
					<span class="app_down"><img onclick="javascript:download();"
							style="margin-left: 1em; cursor:pointer;" src="/assets/images/jump/download_ico.png"
							width="16" /></span>
					<ul style="margin-left: 2em">
						<li style="list-style-type: disc">SellerBot Cash v1.1.7 출시</li>
					</ul>
				</div>

			</div>

			<div class="info_area">
				<p class="sub_title">사용 환경</P>
				<div class="sub_contents">Microsoft Windows 10 (32비트 및 64비트 지원)</div>
			</div>

			<div class="info_area">
				<p class="sub_title">설치 안내</p>
				<div class="slideshow-container">
					<div class="mySlides fade">
						<div class="numbertext">1 / 3</div>
						<img src="/assets/images/jump/dw_welcome.png" style="width:100%">
						<div class="text">설치창이 뜨면 다음 버튼을 눌러서 다음단계로 진행해 주세요.</div>
					</div>

					<div class="mySlides fade">
						<div class="numbertext">2 / 3</div>
						<img src="/assets/images/jump/dw_install.png" style="width:100%">
						<div class="text">설치가 등록되고 있습니다. 설치마무리까지 잠시만 기다려 주세요.</div>
					</div>

					<div class="mySlides fade">
						<div class="numbertext">3 / 3</div>
						<img src="/assets/images/jump/dw_complete.png" style="width:100%">
						<div class="text">설치가 완료되었습니다.</div>
					</div>

					<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
					<a class="next" onclick="plusSlides(1)">&#10095;</a>

				</div>
				<br>

				<div style="text-align:center">
					<span class="dot" onclick="currentSlide(1)"></span>
					<span class="dot" onclick="currentSlide(2)"></span>
					<span class="dot" onclick="currentSlide(3)"></span>
				</div>

			</div>








		</div>
	</div>
</div>

<script>

	$(document).ready(function () {
		var noti_seq_no = '${noti_seq_no}';
		if (noti_seq_no != "") {
			$("#seq" + noti_seq_no).addClass("active");
		}
	});

	function goPage(p) {
		if (document.noticeForm.page.value != p) {
			document.noticeForm.page.value = p;
			document.noticeForm.submit();
		}
	};

</script>


<script>

	var slideIndex = 1;
	showSlides(slideIndex);

	function plusSlides(n) {
		showSlides(slideIndex += n);
	}

	function currentSlide(n) {
		showSlides(slideIndex = n);
	}

	function showSlides(n) {
		var i;
		var slides = document.getElementsByClassName("mySlides");
		var dots = document.getElementsByClassName("dot");
		if (n > slides.length) { slideIndex = 1 }
		if (n < 1) { slideIndex = slides.length }
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace(" active", "");
		}
		slides[slideIndex - 1].style.display = "block";
		dots[slideIndex - 1].className += " active";
	}

</script>