<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!--s:2200213 추가-->
<!-- <script src="/assets/js/jquery-3.3.1.min.js"></script> -->
<script src="/assets/js/jquery-confirm.min.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/jquery-confirm.min.css" />
<!--e:2200213 추가-->

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-pagination.js"></script>
<script src="/assets/js/tui-grid.js"></script>

<!--script src="/assets/js/common/lws.js"></script-->
<!-- 2020-09-17 wss 프로토콜 적용, DNS 미적용으로 주석처리 -->
<script src="/assets/js/common/lws.js?ver=20211122_01"></script>
<script src="/assets/js/jump.js"></script>

<script>
	// 금융사 사이트 추가
	function pop_add_bank(idx) {
		var width = $(window).width();
		var height = $(window).height();

		var bw = '600';
		var bh = '50%';
		if (width < 600) bw = '98%';
		else bw = '600';

		$.confirm({
			theme: 'modal_box_wrap',
			buttons: {
				취소: {
					btnClass: 'btn btn_gray2',
				},
				추가하기: {
					btnClass: 'btn btn_blue',
					action: function (e) {
						return addJumpSite(e, "FINA");
					}
				}
			},
			closeIcon: function () {
				return 'aRandomButton';
			},
			//	columnClass: 'col-md-6 col-md-offset-3',    				
			boxWidth: bw,
			boxHeight: bh,
			useBootstrap: false,
			title: '금융사 사이트 추가',
			content: createAddModalHtml("FINA")
					/* '<div class="modal_popup position_r">' +
											//'<span class="pop_tit_txt">셀러봇캐시에 점프하실 사이트를 추가합니다.</span>'+
											'<div class="modal_con pop_setting">' +
													// start
                                                    // '<p class="mt05 mb02 txt_s12"><input type="checkbox"> 전체 선택 <input type="checkbox" class="ml20"> 전체 선택 해제</p>'+
													'<div class="scroll">'+
															'<ul class="fl_wrap">'+
																'<li class="current">KEB하나은행은행은행</li>'+
																'<li class="add_click">SC은행</li>'+
																'<li class="add_click">경남은행</li>'+
																'<li>광주은행</li>'+
																'<li class="add_click">국민은행</li>'+
																'<li class="current">기업은행</li>'+
																'<li>농협은행</li>'+
																'<li class="current">대구은행</li>'+
																'<li>부산은행</li>'+
																'<li>국민은행</li>'+
																'<li>기업은행</li>'+
																'<li>농협은행</li>'+
																'<li class="current">KEB하나은행</li>'+
																'<li>국민은행</li>'+
																'<li>기업은행</li>'+
																'<li>농협은행</li>'+
															'</ul>'+
													'</div>' +
													'<p class="mt05 mb02">원하시는 사이트가 목록에 없을 경우, <a href="/sub/my/ask">1:1문의</a>를 통해 요청해주시길 바랍니다.</p>'+
													'<div class="setting_select"><b>1건</b> 선택완료</div>'+
													//사이트 추가 end
											'</div>' +
									'</div>' */,
		});
	}

	//$.document().re
	// 쇼핑몰관리 사이트 추가
	function pop_add_mall(idx) {

		var width = $(window).width();
		var height = $(window).height();

		var bw = '600';
		var bh = '50%';
		if (width < 600) bw = '98%';
		else bw = '600';

		$.confirm({
			theme: 'modal_box_wrap',
			buttons: {
				취소: {
					btnClass: 'btn btn_gray2',
				},
				추가하기: {
					btnClass: 'btn btn_blue',
					action: function (e) {
						return addJumpSite(e, "MAMG");
					}
				}
			},
			closeIcon: function () {
				return 'aRandomButton';
			},
			//	columnClass: 'col-md-6 col-md-offset-3',    				
			boxWidth: bw,
			boxHeight: bh,
			useBootstrap: false,
			title: '쇼핑몰관리 사이트 추가',
			content: createAddModalHtml("MAMG")
					/* '<div class="modal_popup position_r">' +
											//'<span class="pop_tit_txt">셀러봇캐시에 점프하실 사이트를 추가합니다.</span>'+
											'<div class="modal_con pop_setting">' +
													// start
                                                    // '<p class="mt05 mb02 txt_s12"><input type="checkbox"> 전체 선택 <input type="checkbox" class="ml20"> 전체 선택 해제</p>'+
													'<div class="scroll">'+
															'<ul class="fl_wrap">'+
																'<li class="current">사방넷</li>'+
																'<li class="add_click">이지어드민</li>'+
																'<li class="add_click">샵링커</li>'+
																'<li>카페24</li>'+
																'<li class="add_click">이셀러스</li>'+
																'<li class="current">사방넷</li>'+
																'<li>사방넷</li>'+
																'<li class="current">사방넷</li>'+
																'<li>이지어드민</li>'+
																'<li>이셀러스</li>'+
																'<li>이셀러스</li>'+
																'<li>이셀러스</li>'+
																'<li class="current">이지어드민</li>'+
																'<li>이셀러스</li>'+
																'<li>이셀러스</li>'+
																'<li>이셀러스</li>'+
															'</ul>'+
													'</div>' +
													'<p class="mt05 mb02">원하시는 사이트가 목록에 없을 경우, <a href="/sub/my/ask">1:1문의</a>를 통해 요청해주시길 바랍니다.</p>'+
													'<div class="setting_select"><b>1건</b> 선택완료</div>'+
													//사이트 추가 end
											'</div>' +
									'</div>' */,
		});
	}

	//$.document().re
	// 매입채널 사이트 추가
	function pop_add_domai(idx) {

		var width = $(window).width();
		var height = $(window).height();

		var bw = '600';
		var bh = '50%';
		if (width < 600) bw = '98%';
		else bw = '600';

		$.confirm({
			theme: 'modal_box_wrap',
			buttons: {
				취소: {
					btnClass: 'btn btn_gray2',
				},
				추가하기: {
					btnClass: 'btn btn_blue',
					action: function (e) {
						return addJumpSite(e, "PUCH");
					}
				}
			},
			closeIcon: function () {
				return 'aRandomButton';
			},
			//	columnClass: 'col-md-6 col-md-offset-3',    				
			boxWidth: bw,
			boxHeight: bh,
			useBootstrap: false,
			title: '매입채널 사이트 추가',
			content: createAddModalHtml("PUCH")
					/* '<div class="modal_popup position_r">' +
											//'<span class="pop_tit_txt">셀러봇캐시에 점프하실 사이트를 추가합니다.</span>'+
											'<div class="modal_con pop_setting">' +
													// start
                                                    // '<p class="mt05 mb02 txt_s12"><input type="checkbox"> 전체 선택 <input type="checkbox" class="ml20"> 전체 선택 해제</p>'+
													'<div class="scroll">'+
															'<ul class="fl_wrap">'+
																'<li class="current">사방넷</li>'+
																'<li class="add_click">이지어드민</li>'+
																'<li class="add_click">샵링커</li>'+
																'<li>카페24</li>'+
																'<li class="add_click">이셀러스</li>'+
																'<li class="current">사방넷</li>'+
																'<li>사방넷</li>'+
																'<li class="current">사방넷</li>'+
																'<li>이지어드민</li>'+
																'<li>이셀러스</li>'+
																'<li>이셀러스</li>'+
																'<li>이셀러스</li>'+
																'<li class="current">이지어드민</li>'+
																'<li>이셀러스</li>'+
																'<li>이셀러스</li>'+
																'<li>이셀러스</li>'+
															'</ul>'+
													'</div>' +
													'<p class="mt05 mb02">원하시는 사이트가 목록에 없을 경우, <a href="/sub/my/ask">1:1문의</a>를 통해 요청해주시길 바랍니다.</p>'+
													'<div class="setting_select"><b>1건</b> 선택완료</div>'+
													//사이트 추가 end
											'</div>' +
									'</div>' */,
		});
	}
</script>

<input type="hidden" id="deviceType" value="${DeviceType }">

<!--container_start-->
<div class="container">
	<div class="peer_wrap">
		<div class="menu_top">
			<p class="menu_name">점프 서비스</p>
			<div class="gnb_topBox">
				<ul class="gnb_top clearfix">
					<li class="focus"><a href="/sub/jump/service">점프서비스</a></li>
					<li><a href="/sub/jump/download">프로그램 설치안내</a></li>
				</ul>
			</div>
		</div>
		<div class="peer_title_top">
			<div>셀러봇캐시랑 지금 바로 <b>점프</b>해볼까요?</div>
		</div>
		<div class="jump_tab fl_wrap">
			<!-- tab -->
			<a href="#1" class="jump_store">판매몰</a>
			<a href="#2" class="jump_bank">금융사</a>
			<a href="#3" class="jump_care">쇼핑몰관리</a>
			<a href="#4" class="jump_domai">매입채널</a>
			<a href="#5" class="jump_public">관공서</a>
			<!-- //tab -->
		</div>
		<p class="edit_video_btn4"><span>영상가이드</span></p>
		<style>
			.edit_video_btn4 {
				max-width: 1000px;
    width: 100%;
    margin: 0 auto;
    margin-top: 0.7rem;

			}
			.edit_video_btn4 span {
				font-size: 1rem;
    color: #fff;
    background-color: #009aca;
    display: block;
    width: 106px;
    text-align: center;
    padding: 0.3rem 0;
    border-radius: 3rem;
    float: right;
    cursor: pointer;

			}

		</style>
		<!--판매몰-->
		<a name="1"></a>
		<div class="jump_con">
			<h3 class="h_store">
				주로 이용하시는 <b>판매몰</b>로 점프 합니다.
				<p><a href="/sub/my/market" style="text-decoration:underline; color:#009aca">판매몰 관리</a>에 등록된 판매몰로 바로
					점프하실 수 있어요.</p>
			</h3>
			<section id="mallList" class="jump_store_list fl_wrap">
				<!-- <ul>
							<li class="fl_wrap">
								<dl class="th_line">
									<dt>마켓유형</dt>
									<dt>판매몰명</dt>
									<dt>아이디</dt>
									<dt>상태<p class="btn_info">?<span class="over_text">상태에 대한 설명이 나옴니다. 셀러봇 서비스 상태에 대한 설명입니다.</span></p></dt>
									<dt></dt>
								</dl>
							</li>
							<li class="fl_wrap">
								<dl>
									<dd>오픈마켓</dd>
									<dd class="text_center"><img src="/assets/images/member/logo/logo001.png" alt="Gmarket" class="logo"></dd>
									<dd class="text_center">jump_mall@naver.com</dd>
									<dd><p class="store_con"><span class="bullet_gry"></span>점검</p></dd>
									<dd class="text_right pr30"><a href="#">자동로그인 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></dd>
								</dl>
							</li>
							<li class="fl_wrap">
								<dl>
									<dd>오픈마켓</dd>
									<dd class="text_center"><img src="/assets/images/member/logo/logo001.png" alt="Gmarket" class="logo"></dd>
									<dd class="text_center">jump_mall@naver.com</dd>
									<dd><p class="store_con"><span class="bullet_blu"></span>셀러봇 서비스</p></dd>
									<dd class="text_right pr30"><a href="#">자동로그인 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></dd>
								</dl>
							</li>
							<li class="fl_wrap">
								<dl>
									<dd>종합몰</dd>
									<dd class="text_center"><img src="/assets/images/member/logo/logo010.png" alt="Gmarket" class="logo"></dd>
									<dd class="text_center">jump_mall@naver.com</dd>
									<dd><p class="store_con"><span class="bullet_blu"></span>셀러봇 서비스</p></dd>
									<dd class="text_right pr30"><a href="#">자동로그인 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></dd>
								</dl>
							</li>
							<li class="fl_wrap">
								<dl>
									<dd>소셜커머스</dd>
									<dd class="text_center"><img src="/assets/images/member/logo/logo052.png" alt="Gmarket" class="logo"></dd>
									<dd class="text_center">jump_mall@naver.com</dd>
									<dd><p class="store_con"><span class="bullet_blu"></span>셀러봇 서비스</p></dd>
									<dd class="text_right pr30"><a href="#">자동로그인 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></dd>
								</dl>
							</li>
							<li class="fl_wrap">
								<dl>
									<dd>종합몰</dd>
									<dd class="text_center"><img src="/assets/images/member/logo/logo003.png" alt="Gmarket" class="logo"></dd>
									<dd class="text_center">jump_mall@naver.com</dd>
									<dd><p class="store_con"><span class="bullet_red"></span>오류</p></dd>
									<dd class="text_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></dd>
								</dl>
							</li>
						</ul> -->
			</section>
			<div id="mallListPagingWrapper" class="status_grid_wrapper">
				<div id="grid-pagination-container" class="tui-pagination"></div>
			</div>
		</div>
		<!--금융사-->
		<a name="2"></a>
		<div class="jump_con">
			<h3 class="h_bank">
				가까이 하기에는 너무 먼, <b>금융사 사이트</b>
				<p>평소 이용하시는 금융사로 바로 이동하실 수 있고 사이트를 직접 등록하실 수도 있어요.</p>
			</h3>
			<div class="text_right setting_area"><button class="btn btn_setting"
					onclick="pop_add_bank(); return false;"><img src="/assets/images/jump/icon_add.png"
						alt="추가"></button> <button class="btn btn_setting"
					onclick="javascript:deleteJumpSite('FINA');"><img src="/assets/images/jump/icon_del.png"
						alt="삭제"></button></div>
			<section class="jump_list fl_wrap">
				<ul id="CUST_FINA_LIST">
					<li class="text_center">데이터가 없습니다.</li>
					<!-- <li class="fl_wrap">
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>신한은행 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>IBK기업은행 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>신한은행 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
							</li>
							<li class="fl_wrap">
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>SC은행 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>소드원 쇼핑몰론 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>신한은행 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
							</li> -->
				</ul>
			</section>
		</div>
		<!--쇼핑몰관리 사이트-->
		<a name="3"></a>
		<div class="jump_con">
			<h3 class="h_care">
				자주 활용하시는, <b>쇼핑몰관리 사이트</b>
				<p>평소 이용하시거나 등록하신 쇼핑몰 관리 사이트로 편리하게 접속하실 수 있어요.</p>
			</h3>
			<div class="text_right setting_area"><button class="btn btn_setting"
					onclick="pop_add_mall(); return false;"><img src="/assets/images/jump/icon_add.png"
						alt="추가"></button> <button class="btn btn_setting"
					onclick="javascript:deleteJumpSite('MAMG');"><img src="/assets/images/jump/icon_del.png"
						alt="삭제"></button></div>
			<section class="jump_list fl_wrap">
				<ul id="CUST_MAMG_LIST">
					<li class="text_center">데이터가 없습니다.</li>
					<!-- <li class="fl_wrap">
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>사방넷 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>이지어드민 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>샵링커 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
							</li>
							<li class="fl_wrap">
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>카페24 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>이셀러스 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
							</li> -->
				</ul>
			</section>
		</div>
		<!--매입채널 사이트-->
		<a name="4"></a>
		<div class="jump_con">
			<h3 class="h_domai">
				필요할 때 바로바로! <b>매입채널 사이트</b>
				<p>평소 이용하시거나 등록하신 매일채널 사이트로 편리하게 접속하실 수 있어요.</p>
			</h3>
			<div class="text_right setting_area"><button class="btn btn_setting"
					onclick="pop_add_domai(); return false;"><img src="/assets/images/jump/icon_add.png"
						alt="추가"></button> <button class="btn btn_setting"
					onclick="javascript:deleteJumpSite('PUCH');"><img src="/assets/images/jump/icon_del.png"
						alt="삭제"></button></div>
			<section class="jump_list fl_wrap">
				<ul id="CUST_PUCH_LIST">
					<li class="text_center">데이터가 없습니다.</li>
					<!-- <li class="fl_wrap">
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>사방넷 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>이지어드민 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>샵링커 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
							</li>
							<li class="fl_wrap">
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>카페24 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>이셀러스 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
							</li> -->
				</ul>
			</section>
		</div>
		<!--관공서 사이트-->
		<a name="5"></a>
		<div class="jump_con pb50">
			<h3 class="h_public">
				쇼핑몰 판매와 관련된 <b>관공서 사이트</b>
				<p>평소 이용하시거나 등록하신 관공서 사이트로 편리하게 접속하실 수 있어요.</p>
			</h3>
			<section class="jump_list fl_wrap">
				<ul id="GOOF_LIST">
					<li class="text_center">데이터가 없습니다.</li>
					<!-- <li class="fl_wrap">
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>국세청 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>민원24 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
								<dl class="fl_wrap">
									<dt><input type="checkbox"></dt>
									<dd>대법원 등기소 <span class="fl_right pr30"><a href="#">이동하기 <img src="/assets/images/main/icon_arrow.png" class="inline_block ml10"></a></span></dd>
								</dl>
							</li> -->
				</ul>
			</section>
		</div>
		<!-- <section class="notice pb50 pt50">
						<div class="new_owl_slider_wrap">
							<div class="new_owl_slider_num_wrap new_owl_slider_1_wrap">
								<h1>파트너사</h1>
								<div class="new_owl_slider_1">
									<img src="/assets/images/notice/icon_partner1.png" alt="" />
									<img src="/assets/images/notice/icon_partner2.png" alt="" />
									<img src="/assets/images/notice/icon_partner3.png" alt="" />
									<img src="/assets/images/notice/icon_partner4.png" alt="" />
								</div>
							</div>
							<div class="new_owl_slider_num_wrap new_owl_slider_2_wrap">
								<h1>특허/이력</h1>
								<div class="new_owl_slider_2">
									<img src="/assets/images/notice/icon_patent1.png" alt="" />
									<img src="/assets/images/notice/icon_patent2.png" alt="" />
									<img src="/assets/images/notice/icon_patent3.png" alt="" />
									<img src="/assets/images/notice/icon_patent4.png" alt="" />
								</div>
							</div>
						</div>
					</section> -->
	</div>
	<!--account_wra_end -->
</div>
<!--container_end-->

<div class="peer_top_btn">
	<h1>TOP</h1>
</div>
<div class="edit_video_popup">
    <div class="edit_video_popup_container">
      <p><img src="/assets/images/member/close.png" alt=""></p>
      <iframe width="100%" height="100%" src="https://www.youtube.com/embed/RrqtkfTESzg?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="background_black"></div>
</div>
<script>
	var conWidth = $(".jump_con").width();
	var winWidth = $(window).width();
	var calValue = (winWidth - conWidth) / 2 - 80;
	$(".peer_top_btn").css("right", calValue);

	$(window).resize(function () {
		var conWidth = $(".jump_con").width();
		var winWidth = $(window).width();
		var calValue = (winWidth - conWidth) / 2 - 80;
		$(".peer_top_btn").css("right", calValue);
	});

	$(".peer_top_btn").click(function () {
		$('html, body').stop().animate({
			scrollTop: 0
		}, 500);
	});

	$(window).scroll(function () {
		var y = $(window).scrollTop();
		if (y > 200) {
			$(".peer_top_btn").show();
		} else {
			$(".peer_top_btn").hide();
		}
	});

	$(".peer_product_list_wrap li").click(function () {
		var targetId = $(this).attr("for");
		var offset = $("#" + targetId).offset().top;
		offset -= $(".header_area").height();
		$('html, body').stop().animate({
			scrollTop: offset
		}, 350);
	});
</script>

<script>
	window.onbeforeunload = function (e) {
		if ($("#deviceType").val() == "PC")
			closeWS();
	};

	$(document).ready(function () {
		var noti_seq_no = '${noti_seq_no}';
		if (noti_seq_no != "") {
			$("#seq" + noti_seq_no).addClass("active");
		}

		if ($("#deviceType").val() == "PC")
			connectWS();

		// 등록된 몰 리스트
		var mallListString = '${mallListString}';
		if (mallListString != null && mallListString != "")
			mallList = JSON.parse(mallListString);
		createMallList();

		// 고객 점프 리스트
		var custJumpMapString = '${custJumpMapString}';
		if (custJumpMapString != null && custJumpMapString != "")
			custJumpMap = JSON.parse(custJumpMapString);
		createCustJumpList();

		// 점프 리스트
		var jumpMapString = '${jumpMapString}';
		if (jumpMapString != null && jumpMapString != "")
			jumpMap = JSON.parse(jumpMapString);

		// 관공서 리스트 생성
		createGOOFList();
	});

	function goPage(p) {
		if (document.noticeForm.page.value != p) {
			document.noticeForm.page.value = p;
			document.noticeForm.submit();
		}
	};
			//영상 팝업 스크립트
			document.querySelectorAll(".edit_video_btn4")[0].addEventListener("click",function(){
			document.querySelectorAll(".edit_video_popup")[0].style.display = "flex";
		});
		document.querySelectorAll(".edit_video_popup_container p")[0].addEventListener("click",function(){
			document.querySelectorAll(".edit_video_popup")[0].style.display = "none";
			document.querySelectorAll(".edit_video_popup iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
		});
</script>

<script type="text/javascript">
// 디자인 스크립트
// <!--
// $(document).ready(function(){

// 	$(".text_n").click(function() {
// 		console.log("text_n");
// 		console.log(this);
// 		console.log(this);
// 	  var notice = $(this).parents(".title_notice");
// 	  if (notice.hasClass("active") == true) {
// 	    notice.removeClass("active");
// 	  } else {
// 	    $(".title_notice").removeClass("active");
// 	    notice.addClass("active");
// 	  }
// 	  $(".close_btn_notice").click(function() {
// 	    $(this).parents(".title_notice").removeClass("active");
// 	  });
// 	});


// 	var owl = $('.new_owl_slider_1');
//     owl.owlCarousel({
//       items: 3,
//       loop: true,
//       autoplay: true,
//       margin:60,
//       autoplayTimeout: 2000,
//       autoplayHoverPause: false,

//     });
//     var owl2 = $('.new_owl_slider_2');
//     owl2.owlCarousel({
//       items: 3,
//       margin:60,
//       loop: true,
//       autoplay: true,
//       autoplayTimeout: 2000,
//       autoplayHoverPause: false,
//     });


// });
//-->
</script>

<style>
	/*paging*/
	.tui-pagination {
		padding-top: 1rem;
	}

	.tui-pagination .tui-is-selected,
	.tui-pagination strong {
		background: #a4a4a4;
		border-color: #a4a4a4;
	}

	.tui-pagination .tui-is-selected:hover {
		background: #a4a4a4;
		border-color: #a4a4a4;
	}

	.tui-pagination .tui-first-child.tui-is-selected {
		border: 1px solid #a4a4a4;
	}

	.jconfirm {
		z-index: 9998;
	}
</style>