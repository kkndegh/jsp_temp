<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<div class="container">
    <div class="member_wra">
        <div class="left_title_area">
            <div class="step_join_tab">
                <span class="active">01</span>
                <span>02</span>
                <span>03</span>
            </div>
            <div class="left_title_item">
                <div class="step_box">
                    <div class="title_txt">환영합니다.<br>기다리고 있었어요</div>
                    <p class="sub_title_txt">회원가입을 위해<br>약관동의를 먼저 진행해주세요.</p>
                </div>
            </div>
        </div>
        <div class="right_contents_area">
            <div class="join_section">
                <form id="form" name="form" onsubmit="return false;" action="/pub/member/join_step1" method="post"
                    autocomplete="off" novalidate="novalidate">
                    <div class="member_right_section">
                        <div class="member_frm_box">
                            <div class="use_agree">
                                <div class="agree_all">
                                    <input type="checkbox" class="chkbox" id="joinAll">
                                    <label for="joinAll">셀러봇캐시 전체동의</label>
                                </div>
                                <c:forEach var="titem" items="${sellerbotTerms}">
                                    <c:set var="termsId" value="${titem.terms_id}" />
                                    <div class="item_agree_join">
                                        <input type="checkbox" class="chkbox" id="joinAgree${termsId}"
                                            name="sellerbotTerm" value="${termsId}" term_esse="${titem.terms_esse_yn}"
                                            term_type_cd="${titem.terms_typ_cd}"
                                        >
                                        <label for="joinAgree${termsId}">
                                            [${titem.terms_esse_yn == 'Y' ? '필수':'선택' }]
                                            <c:out value="${titem.terms_title}" />
                                        </label>
                                        <a href="" termid="${termsId}" class="link_agree">자세히보기</a>
                                    </div>
                                    <c:if test="${titem.terms_typ_cd == 'SMT'}">
                                        <c:forEach var="smtTermsItem" items="${smtTermsList}">
                                            <div class="item_agree_smt" style="padding-left: 3.5em;">
                                                <input type="checkbox" class="chkbox" id="smtTermsAgree${smtTermsItem.com_cd}"
                                                    name="smtTerm"
                                                    value="${smtTermsItem.com_cd}"
                                                >
                                                <label for="smtTermsAgree${smtTermsItem.com_cd}" 
                                                    style="transform: scale(.9); transform-origin: center; color: #999999;"
                                                >
                                                    <c:out value="${smtTermsItem.cd_nm}" />
                                                </label>
                                            </div>
                                        </c:forEach>
                                    </c:if>
                                </c:forEach>

                            </div>
                        </div>
                    </div>
                    <div class="btn_mb_group">
                        <button class="btn_cicle">다음</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- 약관 정보 -->
    <!--팝업-->
    <div class="new_popup_wrap">
        <div class="display_none_back"></div>

        <c:forEach var="titem" items="${sellerbotTerms}">
            <div id="termpopup${titem.terms_id}" class="pop_up_1 pop_up_all">
                <div class="popup_top_title">
                    <h1>
                        [${titem.terms_esse_yn == 'Y' ? '필수':'선택' }]
                        <c:out value="${titem.terms_title }" />
                    </h1>
                    <div class="add_pooup_close">
                        <div></div>
                        <div></div>
                    </div>
                </div>
                <h1 class="popup_title_2">
                    <c:choose>
                        <c:when test="${titem.terms_esse_yn eq 'Y'}">
                            서비스를 이용하시려면 약관동의가 필요합니다.
                        </c:when>
                        <c:otherwise>
                            다음의 약관은 선택사항입니다.
                        </c:otherwise>
                    </c:choose>
                </h1>
                <div class="pop_up_text_wrap">
                    ${sf:textToHtml(titem.terms_cont)}
                </div>
                <button type="button" class="btn_confirm_terms join_step_popup_close">약관확인</button>
            </div>

        </c:forEach>

    </div>
    <!-- 약관 정보 종료 -->

</div>
<script>
    $(document).ready(function () {

        $(".agree_all .chkbox").click(function () {
            var allchk = $(this);
            if (allchk.is(":checked") == true) {
                allchk.parents(".use_agree").find(".item_agree_join .chkbox").prop("checked", true);
                allchk.parents(".use_agree").find(".item_agree_smt .chkbox").prop("checked", true);
            } else {
                allchk.parents(".use_agree").find(".item_agree_join .chkbox").prop("checked", false);
                allchk.parents(".use_agree").find(".item_agree_smt .chkbox").prop("checked", false);
            }
        });
        $(".item_agree_join .chkbox, .item_agree_smt .chkbox").click(function () {
            var chkThis = $(this);
            var itemAllCnt = chkThis.parents(".use_agree").find(".item_agree_join").length;
            var smtItemAllCnt = chkThis.parents(".use_agree").find(".item_agree_smt").length;

            itemAllCnt = itemAllCnt + smtItemAllCnt;

            // 전체 약관 선택시 셀러봇캐시 전체동의 체크 여부 표시
            var itemChk = chkThis.parents(".use_agree").find(".item_agree_join .chkbox:checked").length;
            var smtItemChk = chkThis.parents(".use_agree").find(".item_agree_smt .chkbox:checked").length;
            itemChk = itemChk + smtItemChk;

            if (itemAllCnt == itemChk) {
                chkThis.parents(".use_agree").find(".agree_all .chkbox").prop("checked", true);
            } else {
                chkThis.parents(".use_agree").find(".agree_all .chkbox").prop("checked", false);
            }

            // 셀러봇캐시 마케팅 활용 항목 전체 동의시 항목 전체 체크 여부 표시
            if (chkThis.attr("term_type_cd") == 'SMT') {
                if (chkThis.is(":checked") == true) {
                    chkThis.parents(".use_agree").find(".item_agree_smt .chkbox").prop("checked", true);
                } else {
                    chkThis.parents(".use_agree").find(".item_agree_smt .chkbox").prop("checked", false);
                }
            } else {
                if (smtItemChk > 0) {
                    chkThis.parents(".use_agree").find(".item_agree_join").children("[term_type_cd=SMT]").prop("checked", true);
                } else {
                    chkThis.parents(".use_agree").find(".item_agree_join").children("[term_type_cd=SMT]").prop("checked", false);
                }
            }
            
        });

        // 공통 script.js 이벤트 삭제
        $(".item_agree_join .link_agree").off("click");
        // 각각 약관 마다 모달 호출
        $(".item_agree_join .link_agree").click(function () {
            var termid = $(this).attr("termid");
            $(".new_popup_wrap").css("display", "flex");
            $(".display_none_back").css("display", "block");
            $("#termpopup" + termid).show();
            return false;
        });

        // 약관 닫기
        $(".join_step_popup_close").click(function () {
            $(".new_popup_wrap").css("display", "none");
            $(".pop_up_all").css("display", "none");
            $(".display_none_back").css("display", "none");
        });
        $(".add_pooup_close").click(function () {
            $(".new_popup_wrap").css("display", "none");
            $(".pop_up_all").css("display", "none");
            $(".display_none_back").css("display", "none");
        });

        $(".btn_cicle").on("click", function (e) {
            e.preventDefault();
            if (onSubmitJoinStep1()) {
                document.form.submit();
            }

        });

    });

    function onSubmitJoinStep1() {
        var terms = document.getElementsByName("sellerbotTerm");
        var terms = $("[name=sellerbotTerm]");

        // 필수 선택 중 선택하지 않은  약관이 있는지 확인
        var checkessterm = true;

        // 금융서비스 특정 url로 유입시 제3자 정보제공 동의 확인
        var checkIT3Term = true;
        var infPath = "${fn:toUpperCase(sessionScope.infPath)}";
        terms.each(function (idx) {
            var ess = $(this).attr("term_esse");
            if (ess == 'Y' && !$(this).is(":checked")) {
                checkessterm = false;
            }

            var typeCd = $(this).attr("term_type_cd");
            if (infPath == 'SCB' && typeCd == 'IT3' && !$(this).is(":checked")) {
                checkIT3Term = false;
            }
        });

        if (!checkIT3Term) {
            showAlert("제휴(금융)서비스 이용을 위하여 제3자정보제공동의를 <br>선택해 주시기 바랍니다.");
        }

        if (!checkessterm) {
            showAlert("필수 약관을 선택해주세요.");
        }

        return (checkessterm && checkIT3Term);
    }

</script>