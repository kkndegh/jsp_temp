<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf" %>
<script src="/assets/js/mngMall.js?ver=20221117_01"></script>

<div class="container">
    <div class="member_wra long_member_wra step_4">
        <div class="left_title_area">
            <div class="left_title_longArea">
                <div class="step_join_tab col3">
                    <span class="active">04</span>
                    <span>05</span>
                    <span>06</span>
                </div>
                <div class="left_title_item">
                    <div class="step_box">
                        <div class="title_txt">판매몰을<br>등록해볼까요?</div>
                        <p class="sub_title_txt">현재 판매하고 있는<br>쇼핑몰을 선택해주세요.</p>
                    </div>
                </div>
                <!-- <div class="cs_center_info">
                    <p class="cs-info-none">고객센터 : 1666-8216<br>
                    s-cash@only1fs.com</p>
                </div> -->
            </div>
        </div>
        <div class="right_contents_area">
            <div class="complete_box">
                <div class="complete_item">
                    <form action="/sub/member/join_step5" onsubmit="return false;">
                        <div class="add_join_section">
                            <p class="title_step">판매몰 선택 (복수선택 가능)</p>
                            <p class="desc_step">
                            1개 이상 필수등록해야하며<br>
                            복수선택이 가능합니다.
                            </p>
                            <style>
                                /* 툴팁 balloon */
                                .msg_time_alarm .bt-tooltip {
                                    position: absolute;
                                    display: inline-block;
                                }
            
                                .msg_time_alarm .bt-tooltip:hover[data-title]:before,
                                .msg_time_alarm .bt-tooltip:hover[data-title]:after {
                                    visibility: visible;
                                    pointer-events: auto
                                }
            
                                .msg_time_alarm .bt-tooltip[data-title]:before,
                                .msg_time_alarm .bt-tooltip[data-title]:after {
                                    pointer-events: none;
                                    visibility: hidden;
                                }
            
                                .msg_time_alarm .bt-tooltip[data-title]:after {
                                    content: attr(data-title);
                                    position: absolute;
                                    top: -75%;
                                    right: -55%;
                                    transform: translateX(50%);
                                    margin-left: 50%;
                                    text-transform: none;
                                    text-align: left;
                                    white-space: pre-line;
                                    font-size: 1rem;
                                    border: 1px solid #d4d4d5;
                                    line-height: 1.4285em;
                                    max-width: none;
                                    background: #fff;
                                    padding: .633em 1em;
                                    font-weight: 100;
                                    font-style: normal;
                                    color: rgba(0, 0, 0, .87);
                                    border-radius: .48571429rem;
                                    -webkit-box-shadow: 0 2px 4px 0 rgba(34, 36, 38, .12), 0 2px 10px 0 rgba(34, 36, 38, .15);
                                    box-shadow: 0 2px 4px 0 rgba(34, 36, 38, .12), 0 2px 10px 0 rgba(34, 36, 38, .15);
                                    z-index: 1;
                                }
                            </style>
                            <c:choose>
                                <c:when test="${not empty loanBatchProTgtInfo.batchProTgt_list }">
                                    <c:set var="loanSvcDescp" value="${loanBatchProTgtInfo.batchProTgt_list[0].svc_descp }" />
                                </c:when>
                                <c:otherwise>
                                    <c:set var="loanSvcDescp" value="신한 퀵정산" />
                                </c:otherwise>
                            </c:choose>
                            <p class="msg_time_alarm">
                                <strong style="cursor:pointer" class="tooltipEvent bt-tooltip" data-title="${finMallList}">${loanSvcDescp}을 이용하기 위해서는<br> 쇼핑몰 등록이 필요합니다.<img src="/assets/images/icon/icon_smRlavy.png" style="width: 23px;height: 20px;display: inline-block; margin-left: 10px;" alt=""></strong><br>
                            </p>
                            <div class="market_choice clearfix">
                                <div class="market_box_find">
                                    <div class="ui-widget">
                                        <input placeholder="판매몰 검색" class="market_box_find_text_area">
                                    </div>
                                </div>
                                <div class="market_box">
                                <div class="search_market">
                                <!--<input type="text" class="sh_box" id="autoText">-->
                                <!-- <button type="button" class="sh_mall_btn">검색</button> -->
                                </div>
                                <div class="group_list_mall" data-type-mk="mk_type1">
                                    <p class="type_market open"><span>오픈마켓</span> <img src="/assets/images/member/btn_arrows.png" alt="">
                                        
                                    </p>
                                    <ul class="scrollblack mall_lists_1">
                                    <c:set var="cnt" value="1"/>
                                        <c:forEach items="${extMallDTOList}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'OPM' && mall.mall_sts_cd eq 'NOR' }">
                                                <li name="open_${cnt }" id="mallList${mall.mall_cd}">
                                                    <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                                    </c:if>
                                                    <span name="open_${cnt }_tag">${mall.mall_nm }</span>
                                                    <input type="hidden" value="${mall.sub_mall_cert_1st_nm }" class="sub_mall_cert_1st_nm"/>                			
                                                    <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }" class="sub_mall_cert_2nd_nm"/>                			
                                                    <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }" class="mall_cert_2nd_passwd_nm"/>
                                                    <input type="hidden" value="${mall.sub_mall_cert_step }" class="sub_mall_cert_step"/>
                                                    <input type="hidden" value="${mall.mall_cd }" class="mall_cd"/>
                                                    <input type="hidden" value="${mall.passwd_step }" class="passwd_step"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }" class="sub_mall_auth_1st_esse_yn"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }" class="sub_mall_auth_2nd_esse_yn"/>
                                                    <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                                    <input type="hidden" value="${mall.mall_nm }" class="mall_nm"/>
                                                    <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                                    <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                                    <input type="hidden" value="${mall.help_info }" class="help_info" />
                                                    <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                                    <input type="hidden" value="${mall.video_url }" class="video_url" />
                                                    <c:forEach items="${mall.mall_cert_typ}" var="mallTyp" varStatus="typStatus">
                                                        <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }" value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ"/>
                                                    </c:forEach>                			
                                                </li>
                                                <c:set var="cnt" value="${cnt + 1 }"/>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_list_mall" data-type-mk="mk_type2">
                                    <p class="type_market"><span>소셜커머스</span> <img src="/assets/images/member/btn_arrows.png" alt=""></p>
                                    <ul class="scrollblack mall_lists_2">
                                    <c:set var="cnt" value="1"/>
                                        <c:forEach items="${extMallDTOList}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'SNM' && mall.mall_sts_cd eq 'NOR' }">
                                                <li name="soci_${cnt }" id="mallList${mall.mall_cd}">
                                                    <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                                    </c:if>
                                                    <span name="soci_${cnt }_tag">${mall.mall_nm }</span>
                                                    <input type="hidden" value="${mall.sub_mall_cert_1st_nm }" class="sub_mall_cert_1st_nm"/>                			
                                                    <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }" class="sub_mall_cert_2nd_nm"/>
                                                    <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }" class="mall_cert_2nd_passwd_nm"/>
                                                    <input type="hidden" value="${mall.sub_mall_cert_step }" class="sub_mall_cert_step"/>
                                                    <input type="hidden" value="${mall.mall_cd }" class="mall_cd"/>
                                                    <input type="hidden" value="${mall.passwd_step }" class="passwd_step"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }" class="sub_mall_auth_1st_esse_yn"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }" class="sub_mall_auth_2nd_esse_yn"/>
                                                    <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                                    <input type="hidden" value="${mall.mall_nm }" class="mall_nm"/>
                                                    <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                                    <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                                    <input type="hidden" value="${mall.help_info }" class="help_info" />
                                                    <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                                    <input type="hidden" value="${mall.video_url }" class="video_url" />
                                                    <c:forEach items="${mall.mall_cert_typ}" var="mallTyp" varStatus="typStatus">
                                                        <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }" value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ"/>
                                                    </c:forEach>   
                                                </li>
                                                <c:set var="cnt" value="${cnt + 1 }"/>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_list_mall" data-type-mk="mk_type3">
                                    <p class="type_market"><span>종합몰(백화점)</span> <img src="/assets/images/member/btn_arrows.png" alt=""></p>
                                    <ul class="scrollblack mall_lists_3">
                                        <c:set var="cnt" value="1"/>
                                        <c:forEach items="${extMallDTOList}" var="mall" varStatus="status">
                                            <!-- 2020-09-10 롯데ON 노출 조건 반영, 롯데ON은 오픈마켓, 종합몰(백화점) 모두 표시 -->
									        <c:if test="${(mall.mall_typ_cd eq 'COM' || (mall.mall_typ_cd eq 'OPM' && mall.mall_cd eq '060')) && mall.mall_sts_cd eq 'NOR'}">
                                                <li name="syn_${cnt }" id="mallList${mall.mall_cd}">
                                                    <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                                    </c:if>
                                                    <span name="syn_${cnt }_tag">${mall.mall_nm }</span>
                                                    <input type="hidden" value="${mall.sub_mall_cert_1st_nm }" class="sub_mall_cert_1st_nm"/>                			
                                                    <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }" class="sub_mall_cert_2nd_nm"/>
                                                    <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }" class="mall_cert_2nd_passwd_nm"/>
                                                    <input type="hidden" value="${mall.sub_mall_cert_step }" class="sub_mall_cert_step"/>
                                                    <input type="hidden" value="${mall.mall_cd }" class="mall_cd"/>
                                                    <input type="hidden" value="${mall.passwd_step }" class="passwd_step"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }" class="sub_mall_auth_1st_esse_yn"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }" class="sub_mall_auth_2nd_esse_yn"/>
                                                    <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                                    <input type="hidden" value="${mall.mall_nm }" class="mall_nm"/>
                                                    <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                                    <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                                    <input type="hidden" value="${mall.help_info }" class="help_info" />
                                                    <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                                    <input type="hidden" value="${mall.video_url }" class="video_url" />
                                                    <c:forEach items="${mall.mall_cert_typ}" var="mallTyp" varStatus="typStatus">
                                                        <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }" value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ"/>
                                                    </c:forEach>   
                                                </li>
                                                <c:set var="cnt" value="${cnt + 1 }"/>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_list_mall" data-type-mk="mk_type4">
                                    <p class="type_market"><span>PG사/VAN사</span> <img src="/assets/images/member/btn_arrows.png" alt=""></p>
                                    <ul class="scrollblack mall_lists_4">
                                        <c:set var="cnt" value="1"/>
                                        <c:forEach items="${extMallDTOList}" var="mall" varStatus="status">
                                            <c:if test="${(mall.mall_typ_cd eq 'PMM' || mall.mall_typ_cd eq 'VAM') && mall.mall_sts_cd eq 'NOR' }">
                                                <li name="pg_${cnt }" id="mallList${mall.mall_cd}">
                                                    <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                                    </c:if>
                                                    <span name="pg_${cnt }_tag">${mall.mall_nm }</span>
                                                    <input type="hidden" value="${mall.sub_mall_cert_1st_nm }" class="sub_mall_cert_1st_nm"/>                			
                                                    <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }" class="sub_mall_cert_2nd_nm"/>
                                                    <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }" class="mall_cert_2nd_passwd_nm"/>
                                                    <input type="hidden" value="${mall.sub_mall_cert_step }" class="sub_mall_cert_step"/>
                                                    <input type="hidden" value="${mall.mall_cd }" class="mall_cd"/>
                                                    <input type="hidden" value="${mall.passwd_step }" class="passwd_step"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }" class="sub_mall_auth_1st_esse_yn"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }" class="sub_mall_auth_2nd_esse_yn"/>
                                                    <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                                    <input type="hidden" value="${mall.mall_nm }" class="mall_nm"/>
                                                    <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                                    <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                                    <input type="hidden" value="${mall.help_info }" class="help_info" />
                                                    <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                                    <input type="hidden" value="${mall.video_url }" class="video_url" />
                                                    <c:forEach items="${mall.mall_cert_typ}" var="mallTyp" varStatus="typStatus">
                                                        <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }" value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ"/>
                                                    </c:forEach>   
                                                </li>
                                                <c:set var="cnt" value="${cnt + 1 }"/>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_list_mall" data-type-mk="mk_type5">
                                    <p class="type_market"><span>전문몰</span> <img src="/assets/images/member/btn_arrows.png" alt=""></p>
                                    <ul class="scrollblack mall_lists_5">
                                        <c:set var="cnt" value="1"/>
                                        <c:forEach items="${extMallDTOList}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'SPM' && mall.mall_sts_cd eq 'NOR'}">
                                                <li name="spe_${cnt }" id="mallList${mall.mall_cd}">
                                                    <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                                    </c:if>
                                                    <span name="spe_${cnt }_tag">${mall.mall_nm }</span>
                                                    <input type="hidden" value="${mall.sub_mall_cert_1st_nm }" class="sub_mall_cert_1st_nm"/>                			
                                                    <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }" class="sub_mall_cert_2nd_nm"/>
                                                    <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }" class="mall_cert_2nd_passwd_nm"/>
                                                    <input type="hidden" value="${mall.sub_mall_cert_step }" class="sub_mall_cert_step"/>
                                                    <input type="hidden" value="${mall.mall_cd }" class="mall_cd"/>
                                                    <input type="hidden" value="${mall.passwd_step }" class="passwd_step"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }" class="sub_mall_auth_1st_esse_yn"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }" class="sub_mall_auth_2nd_esse_yn"/>
                                                    <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                                    <input type="hidden" value="${mall.mall_nm }" class="mall_nm"/>
                                                    <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                                    <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                                    <input type="hidden" value="${mall.help_info }" class="help_info" />
                                                    <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                                    <input type="hidden" value="${mall.video_url }" class="video_url" />
                                                    <c:forEach items="${mall.mall_cert_typ}" var="mallTyp" varStatus="typStatus">
                                                        <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }" value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ"/>
                                                    </c:forEach>   
                                                </li>
                                                <c:set var="cnt" value="${cnt + 1 }"/>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_list_mall" data-type-mk="mk_type6">
                                    <p class="type_market"><span>3PL물류(상품자산)</span> <img src="/assets/images/member/btn_arrows.png" alt=""></p>
                                    <ul class="scrollblack mall_lists_6">
                                        <c:set var="cnt" value="1"/>
                                        <c:forEach items="${extMallDTOList}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'OFM' && mall.mall_sts_cd eq 'NOR'}">
                                                <li name="off_${cnt }" id="mallList${mall.mall_cd}">
                                                    <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                                    </c:if>
                                                    <span name="off_${cnt }_tag">${mall.mall_nm }</span>
                                                    <input type="hidden" value="${mall.sub_mall_cert_1st_nm }" class="sub_mall_cert_1st_nm"/>                			
                                                    <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }" class="sub_mall_cert_2nd_nm"/>
                                                    <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }" class="mall_cert_2nd_passwd_nm"/>
                                                    <input type="hidden" value="${mall.sub_mall_cert_step }" class="sub_mall_cert_step"/>
                                                    <input type="hidden" value="${mall.mall_cd }" class="mall_cd"/>
                                                    <input type="hidden" value="${mall.passwd_step }" class="passwd_step"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }" class="sub_mall_auth_1st_esse_yn"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }" class="sub_mall_auth_2nd_esse_yn"/>
                                                    <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                                    <input type="hidden" value="${mall.mall_nm }" class="mall_nm"/>
                                                    <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                                    <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                                    <input type="hidden" value="${mall.help_info }" class="help_info" />
                                                    <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                                    <input type="hidden" value="${mall.video_url }" class="video_url" />
                                                    <c:forEach items="${mall.mall_cert_typ}" var="mallTyp" varStatus="typStatus">
                                                        <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }" value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ"/>
                                                    </c:forEach>   
                                                </li>
                                                <c:set var="cnt" value="${cnt + 1 }"/>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <div class="group_list_mall" data-type-mk="mk_type9">
                                    <p class="type_market"><span>기타</span> <img src="/assets/images/member/btn_arrows.png" alt=""></p>
                                    <ul class="scrollblack mall_lists_9">
                                        <c:set var="cnt" value="1"/>
                                        <c:forEach items="${extMallDTOList}" var="mall" varStatus="status">
                                            <c:if test="${mall.mall_typ_cd eq 'ETC' && mall.mall_sts_cd eq 'NOR'}">
                                                <li name="etc_${cnt }" id="mallList${mall.mall_cd}">
                                                    <c:if test="${mall.mallLG_file.stor_file_nm != null }">
                                                        <img src="/imagefile/${mall.mallLG_file.stor_path}${mall.mallLG_file.stor_file_nm}" alt="몰 로고">
                                                    </c:if>
                                                    <c:if test="${mall.mallLG_file.stor_file_nm == null }">
                                                        <img src="/imagefile/WEB-INF/defaultImage/image-not-found.png" alt="몰 로고">
                                                    </c:if>
                                                    <span name="etc_${cnt }_tag">${mall.mall_nm }</span>
                                                    <input type="hidden" value="${mall.sub_mall_cert_1st_nm }" class="sub_mall_cert_1st_nm"/>                			
                                                    <input type="hidden" value="${mall.sub_mall_cert_2nd_nm }" class="sub_mall_cert_2nd_nm"/>
                                                    <input type="hidden" value="${mall.mall_cert_2nd_passwd_nm }" class="mall_cert_2nd_passwd_nm"/>
                                                    <input type="hidden" value="${mall.sub_mall_cert_step }" class="sub_mall_cert_step"/>
                                                    <input type="hidden" value="${mall.mall_cd }" class="mall_cd"/>
                                                    <input type="hidden" value="${mall.passwd_step }" class="passwd_step"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_1st_esse_yn }" class="sub_mall_auth_1st_esse_yn"/>
                                                    <input type="hidden" value="${mall.sub_mall_auth_2nd_esse_yn }" class="sub_mall_auth_2nd_esse_yn"/>
                                                    <input type="hidden" value="${mall.otp_auth_use_yn }" class="otp_auth_use_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_esse_yn }" class="otp_auth_esse_yn" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd }" class="otp_auth_typ_cd" />
                                                    <input type="hidden" value="${mall.otp_auth_typ_cd_nm }" class="otp_auth_typ_cd_nm" />
                                                    <input type="hidden" value="${mall.mall_nm }" class="mall_nm"/>
                                                    <input type="hidden" value="${mall.help_use_yn }" class="help_use_yn" />
                                                    <input type="hidden" value="${mall.help_open_yn }" class="help_open_yn" />
                                                    <input type="hidden" value="${mall.help_info }" class="help_info" />
                                                    <input type="hidden" value="${mall.video_use_yn }" class="video_use_yn" />
                                                    <input type="hidden" value="${mall.video_url }" class="video_url" />
                                                    <c:forEach items="${mall.mall_cert_typ}" var="mallTyp" varStatus="typStatus">
                                                        <input type="hidden" data-type_nm="${mallTyp.mall_cert_typ_nm }" value="${mallTyp.mall_cert_typ_cd }" class="mall_cert_typ"/>
                                                    </c:forEach>   
                                                </li>
                                                <c:set var="cnt" value="${cnt + 1 }"/>
                                            </c:if>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                            <div class="choiced_mk_box">
                                <div class="status">
                                    <p class="all">전체 <span class="all_num">0</span></p>
                                </div>
                                <div class="list_choiced_mk">
                                    <p class="title_list_choiced">입력항목</p>
                                    <!--리스트 없을때-->
                                    <div class="list_choiced_empty_box">
                                        <p>좌측에서<br>판매몰을 선택해주세요.</p>
                                    </div>
                                    <!--리스트 있을때-->
                                    <div class="list_choiced_mk_box scrollblack">
                                        <div class="group_mall" data-choice-mk="mk_type1">
                                            <p class="type_market_choiced">오픈마켓</p>
                                            <ul class="open_append">
                                            </ul>
                                        </div>
                                        <div class="group_mall" data-choice-mk="mk_type2">
                                            <p class="type_market_choiced">소셜커머스</p>
                                            <ul class="open_append_2">
                                            </ul>
                                        </div>
                                        <div class="group_mall" data-choice-mk="mk_type3">
                                            <p class="type_market_choiced">종합몰(백화점)</p>
                                            <ul class="open_append_3">
                                            </ul>
                                        </div>
                                        <div class="group_mall" data-choice-mk="mk_type4">
                                            <p class="type_market_choiced">PG사/VAN사</p>
                                            <ul class="open_append_4">
                                            </ul>
                                        </div>
                                        <div class="group_mall" data-choice-mk="mk_type5">
                                            <p class="type_market_choiced">전문몰</p>
                                            <ul class="open_append_5">
                                            </ul>
                                        </div>
                                        <div class="group_mall" data-choice-mk="mk_type6">
                                            <p class="type_market_choiced">3PL물류(상품자산)</p>
                                            <ul class="open_append_6">
                                            </ul>
                                        </div>
                                        <div class="group_mall" data-choice-mk="mk_type8">
                                            <p class="type_market_choiced">기타</p>
                                            <ul class="open_append_8">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                    <!--
                                    <button class="check_account" type="button">등록결과 보기</button>
                -->
                                <!--등록결과 에러-->
                                <p class="error_account_msg" style="display: none;">계정 입력 오류 항목이 있습니다. 다시 확인해주세요.</p>
                            </div>
                        </div>
                    </div>
                    <div class="btn_mb_group">
                        <button class="btn_cicle">다음</button>
                    </div>
                    </form>
                </div>

                <div id="usable_store">
					<div class="usable_store_header">
						<h3>이용 가능 판매몰</h3>
					</div>
					<div class="usable_store_body">
						<ul></ul>
					</div>
					<div class="usable_store_footer">
						<p>판매몰은 계속 추가되고 있으며, 원하시는 판매몰이 있으실 경우 <a href="/sub/my/ask" target="_blank">1:1문의</a>를 통해 요청해주시기 바랍니다.</p>
					</div>
				</div>

            </div>
            <!--계정 입력 팝업-->
            <div class="popup_account type1">
                <div class="pop_account">
                    <div class="pop_head">
                        <p>계정입력</p>
                        <p class="popup_gui_btn">?</p>
                        <div class="popup_gui_container">
                            <span id="helpInfoText">
                                업체번호와 공급계약번호는
                                인터파크 판매자 매니저의 좌측메뉴
                                판매자 정보 관리의 관리 계정(ID)
                                관리에서 확인할 수 있습니다.
                            </span>
                            <img class="popup_gui_close" src="/assets/images/member/close.png"
                                alt="" />
                        </div>
                        <a href="" class="close_pop"><img src="/assets/images/member/close.png" alt=""></a>
                    </div>
                    <div class="pop_body">
                        <p class="edit_video_btn2"><span>영상가이드</span></p>
                        <div class="logo_area_account">
                            <img src="/assets/images/member/logo/logo011.png" alt="">
                        </div>
                        <div class="m_pop_scroll">
                            <form class="frm_account" name="frmAccount" onsubmit="return false;">
                                <div class="input_account" id="div_mall_cert_typ_cd" style="display: none;">
                                    <p class="lb">몰 인증 유형</p>
                                    <select class="sctbox" id="i_mall_cert_typ_cd" name="mall_cert_typ_cd">
                                    </select>
                                </div>
                                <div class="input_account">
                                    <p class="lb">ID</p>
                                    <input type="text" class="textbox_mb" name="mall_cert_1st_id">
                                </div>
                                <div class="input_account">
                                    <p class="lb">PW</p>
                                    <input type="password" class="textbox_mb" name="mall_cert_1st_passwd">
                                </div>
                                <div class="input_account" style="display: none;" id="div_sub_mall_cert_1st_nm">
                                    <p class="lb" id="t_sub_mall_cert_1st_nm"></p>
                                    <input type="text" class="textbox_mb" name="sub_mall_cert_1st" id="i_sub_mall_cert_1st_nm">
                                </div>
                                <div class="input_account" style="display: none;" id="div_mall_cert_2nd_passwd_nm">
                                    <p class="lb" id="t_mall_cert_2nd_passwd_nm"></p>
                                    <input type="password" class="textbox_mb" name="mall_cert_2nd_passwd" >
                                </div>
                                <div class="input_account"  style="display: none;" id="div_sub_mall_cert_2nd_nm">
                                    <p class="lb" id="t_sub_mall_cert_2nd_nm"></p>
                                    <input type="text" class="textbox_mb" name="sub_mall_cert_2nd" id="i_sub_mall_cert_2nd_nm">
                                    <input type="hidden" class="textbox_mb" name="cert_step" id="cert_step">
                                    <input type="hidden" class="textbox_mb" name="mall_cd" id="mall_cd">
                                    <input type="hidden" class="textbox_mb" name="mall_nm" id="mall_nm">
                                    <input type="hidden" class="textbox_mb" name="passwd_step" id="passwd_step">
                                    <input type="hidden" class="textbox_mb" name="sub_mall_auth_1st_esse_yn" id="sub_mall_auth_1st_esse_yn">
                                    <input type="hidden" class="textbox_mb" name="sub_mall_auth_2nd_esse_yn" id="sub_mall_auth_2nd_esse_yn">
                                    <input type="hidden" name="goods_cate_seq_no" id="goods_cate_seq_no">
                                    <input type="hidden" id="btn_type">
                                </div>

                                <!-- QR코드 인증 -->
                                <div class="input_account" style="display: none;" id="div_mall_auth_otp_key">
                                    <p class="lb" id="t_mall_auth_otp_key">OTP인증</p>
                                    <div class="input_otp file">
                                        <input type="password" class="textbox_otp_file" name="mall_auth_otp_key">
                                        <label for="fileUpload">QR등록</label>
                                        <input type="file" id="fileUpload" name="qr_code_file" class="hidden_file">
                                        <div class="ask_sub_text_area error" id="qr_code_file_err" style="display: none;">
                                            <h1 class="ask_sub_text_area_text">
                                                파일 크기는 10MB 이하의 파일만 업로드 가능합니다.<br />
                                                파일 유형은 JPG,PNG 파일만 업로드 가능합니다.
                                            </h1>
                                        </div>
                                        <input type="hidden" class="textbox_mb" name="otp_auth_use_yn" id="otp_auth_use_yn">
                                        <input type="hidden" class="textbox_mb" name="otp_auth_esse_yn" id="otp_auth_esse_yn">
                                        <input type="hidden" class="textbox_mb" name="otp_auth_typ_cd" id="otp_auth_typ_cd">
                                        <input type="hidden" class="textbox_mb" name="otp_auth_typ_cd_nm" id="otp_auth_typ_cd_nm">
                                        <p id="mall_auth_otp_key_error" class="error"></p>
                                    </div>
                                </div>

                                <!-- 카테고리 추가 -->
                                <div class="input_account" id="div_goods_cate_seq_no">
                                    <p class="lb">카테고리</p>
                                    <select class="sctbox" id="goods_cate_seq_no" name="goods_cate_seq_no">
                                    <option value="">선택</option>
                                    <c:forEach items="${categoryList }" var="category">
                                        <option value="${category.goods_cate_seq_no }">${category.cate_nm }</option>
                                    </c:forEach>
                                    </select>
                                </div>
                                <br>
                            </form>
                        </div>
                    </div>
                    <div class="pop_foot">
                        <button type="button" class="frm_input_btn">입력완료</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--팝업 확인-->
    <div class="popup popup_certify">
        <div class="pop">
            <span class="close_pop"><img src="/assets/images/member/close_gray.png" alt=""></span>
            <div class="pop_body">
                <p class="desc_pop">‘등록결과 보기’ 를 눌러<br>
                    계정 등록 오류 사항을 확인해주세요.</p>
            </div>
            <div class="pop_footer">
                <button type="button" class="close_btn">확인</button>
            </div>
        </div>
    </div>
    <div class="body_cover"></div>
</div>
<div class="edit_video_popup">
    <div class="edit_video_popup_container">
      <p><img src="/assets/images/member/close.png" alt=""></p>
      <iframe id="iframeVideoPopup" width="100%" height="100%" src="https://www.youtube.com/embed/gr1TWqaR3Rs?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="background_black"></div>
</div>
<script src="/assets/js/select2/select2.min.js"></script>
<script>
    if (!('remove' in Element.prototype)) {
        Element.prototype.remove = function() {
            if (this.parentNode) {
                this.parentNode.removeChild(this);
            }
        };
    }

    // 영상 팝업 스크립트
    document.querySelectorAll(".edit_video_btn2")[0].addEventListener("click",function(){
        document.querySelectorAll(".edit_video_popup")[0].style.display = "flex";
    });
    document.querySelectorAll(".edit_video_popup_container p")[0].addEventListener("click",function(){
        document.querySelectorAll(".edit_video_popup")[0].style.display = "none";
        document.querySelectorAll(".edit_video_popup iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });
    //?버튼
	document.querySelectorAll(".popup_gui_close")[0].addEventListener("click",function(){
		document.querySelectorAll(".popup_gui_container")[0].style.display = "none";
	});
	document.querySelectorAll(".popup_gui_btn")[0].addEventListener("click",function(){
		document.querySelectorAll(".popup_gui_container")[0].style.display = "block";
	});
    var clickM;
    var img_mall_name;
    var img_mall_text;

    function fn_edit(ts){
        clickM = $(ts);
        $("#btn_type").val("modify");
        
        $("input[name='mall_nm']").val("");
        $("input[name='mall_cd']").val("");
        $("input[name='cert_step']").val("");
        $("input[name='mall_cert_1st_id']").val("");
        $("input[name='mall_cert_1st_passwd']").val("");
        $("input[name='sub_mall_cert_1st']").val("");
        $("input[name='sub_mall_cert_2nd']").val("");
        $("input[name='passwd_step']").val("");
        $("#i_mall_cert_typ_cd").val("");
        $("input[name='mall_cert_2nd_passwd']").val("");

        var currentLi = $(ts).parents("li");
        var mall_nm = $(currentLi).data("mall_nm");
        var mall_cd = $(currentLi).data("mall_cd");
        var cert_step = $(currentLi).data("cert_step");
        var mall_cert_1st_id = $(currentLi).data("mall_cert_1st_id");
        var mall_cert_1st_passwd = $(currentLi).data("mall_cert_1st_passwd");
        var sub_mall_cert_1st = $(currentLi).data("sub_mall_cert_1st");
        var sub_mall_cert_2nd = $(currentLi).data("sub_mall_cert_2nd");
        var otp_auth_use_yn = $(currentLi).data("otp_auth_use_yn");
        var otp_auth_esse_yn = $(currentLi).data("otp_auth_esse_yn");
        var otp_auth_typ_cd = $(currentLi).data("otp_auth_typ_cd");
        var otp_auth_typ_cd_nm = $(currentLi).data("otp_auth_typ_cd_nm");
        var passwd_step = $(currentLi).data("passwd_step");
        var mall_cert_typ_cd = $(currentLi).data("mall_cert_typ_cd");
        var mall_cert_2nd_passwd = $(currentLi).data("mall_cert_2nd_passwd");
        
        // 카테고리
        var goods_cate_seq_no = $(currentLi).data("goods_cate_seq_no");
        
        var oriClickM = $("#mallList"+mall_cd);
        
        var sub_mall_cert_1st_nm = oriClickM.children(".sub_mall_cert_1st_nm").val();
        var sub_mall_cert_2nd_nm = oriClickM.children(".sub_mall_cert_2nd_nm").val();
        var mall_cert_2nd_passwd_nm = oriClickM.children(".mall_cert_2nd_passwd_nm").val();
        var imgLogo = oriClickM.find("img").attr("src");
        var mallImg = oriClickM.find("img");
        img_mall_name = oriClickM.attr("name");
        
        $(".popup_account.type1 .logo_area_account img").attr('src', imgLogo);     
        $(".popup_account.type1 .logo_area_account img").attr('src', imgLogo);

        if(oriClickM.children(".sub_mall_cert_1st_nm").val() == "" || sub_mall_cert_1st_nm == null){
            $("#div_sub_mall_cert_1st_nm").hide();
        }else{
            $("#t_sub_mall_cert_1st_nm").html(sub_mall_cert_1st_nm);
            $("#div_sub_mall_cert_1st_nm").show();
        }
        
        if(oriClickM.children(".sub_mall_cert_2nd_nm").val() == "" || sub_mall_cert_2nd_nm == null){
            $("#div_sub_mall_cert_2nd_nm").hide();
        }else{
            $("#t_sub_mall_cert_2nd_nm").html(sub_mall_cert_2nd_nm);
            $("#div_sub_mall_cert_2nd_nm").show();
        }
        
        if(oriClickM.children(".mall_cert_2nd_passwd_nm").val() == "" || mall_cert_2nd_passwd_nm == null){
            $("#div_mall_cert_2nd_passwd_nm").hide();
        }else{
            $("#t_mall_cert_2nd_passwd_nm").html(mall_cert_2nd_passwd_nm);
            $("#div_mall_cert_2nd_passwd_nm").show();
        }

        if (clickM.children(".otp_auth_use_yn").val() == "Y" || otp_auth_use_yn == "Y") {
            if (otp_auth_typ_cd == "OTP001") {
                $(".input_otp.file > input[name=mall_auth_otp_key]").attr("disabled", true);
                $(".input_otp.file > input[name=mall_auth_otp_key]").attr("class", "textbox_otp_file");
                $(".input_otp.file > label[for=fileUpload]").show();
            } else if (otp_auth_typ_cd == "OTP002") {
                $(".input_otp.file > input[name=mall_auth_otp_key]").attr("disabled", false);
                $(".input_otp.file > input[name=mall_auth_otp_key]").attr("class", "textbox_mb");
                $(".input_otp.file > label[for=fileUpload]").hide();
            }

            $("#div_mall_auth_otp_key").show();
        } else {
            $("#div_mall_auth_otp_key").hide();
        }
        
        if(oriClickM.children(".mall_cert_typ").length == 0){
            $("#div_mall_cert_typ_cd").hide();
        }else{
            var html = "";
            oriClickM.children(".mall_cert_typ").each(function(i,v){
                html += "<option value='"+$(this).val()+"'>"+$(this).data("type_nm")+"</option>";
            });
            $("#i_mall_cert_typ_cd").html(html);
            $("#div_mall_cert_typ_cd").show();
        }

        /**
         * 2020-09-10 기존에 몰 등록 시 데이터 중복 확인하는 부분이 없기 때문에 몰 정보 수정 시 아이디 수정 불가로 변경
         */
        $("input[name='mall_cert_1st_id']").attr("disabled", "disabled");

        $("input[name='mall_nm']").val(mall_nm);
        $("input[name='mall_cd']").val(mall_cd);
        $("input[name='cert_step']").val(cert_step);
        $("input[name='mall_cert_1st_id']").val(mall_cert_1st_id);
        $("input[name='mall_cert_1st_passwd']").val(mall_cert_1st_passwd);
        $("input[name='sub_mall_cert_1st']").val(sub_mall_cert_1st);
        $("input[name='sub_mall_cert_2nd']").val(sub_mall_cert_2nd);
        $("input[name='passwd_step']").val(passwd_step);
        $("#i_mall_cert_typ_cd").val(mall_cert_typ_cd);
        $("input[name='mall_cert_2nd_passwd']").val(mall_cert_2nd_passwd);
        $("select[name='goods_cate_seq_no']").val( goods_cate_seq_no );
        $(".popup_account.type1").addClass("active");
    }

    /**
     * 2020-09-10 
     * id 규칙 : {몰코드}_{아이디}_{카테고리 구분값}
     */
     var addMallData = function(cate_cls_nm, id, mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, sub_mall_cert_1st
                        , sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no
                        , otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key) {
        var new_li = document.createElement('li');
        $(new_li).attr("id", id);
        $(new_li).attr("data-mall_nm", mall_nm);
        $(new_li).attr("data-mall_cd", mall_cd);
        $(new_li).attr("data-cert_step", cert_step);
        $(new_li).attr("data-mall_cert_1st_id", mall_cert_1st_id);
        $(new_li).attr("data-mall_cert_1st_passwd", mall_cert_1st_passwd);
        $(new_li).attr("data-sub_mall_cert_1st", sub_mall_cert_1st);
        $(new_li).attr("data-sub_mall_cert_2nd", sub_mall_cert_2nd);
        $(new_li).attr("data-passwd_step", passwd_step);
        $(new_li).attr("data-mall_cert_typ_cd", mall_cert_typ_cd);
        $(new_li).attr("data-mall_cert_2nd_passwd", mall_cert_2nd_passwd);
        $(new_li).attr("data-otp_auth_use_yn", otp_auth_use_yn);
        $(new_li).attr("data-otp_auth_esse_yn", otp_auth_esse_yn);
        $(new_li).attr("data-mall_auth_otp_key", mall_auth_otp_key);
        $(new_li).data("goods_cate_seq_no", goods_cate_seq_no);
        $(new_li).attr("class", "paramData");

        var append_con = "<p class='mk_name'>" + mall_nm + "</p><p class='mk_id'>"+mall_cert_1st_id+"</p>"+
        "<p class='mk_sub_id' style='display:none'>"+sub_mall_cert_1st+"</p>"+"<div class='account_btn'><p class='btn_edit' onclick='fn_edit(this)'><img src='/assets/images/member/icon_2.png' alt=''></p><p class='btn_remove' onclick='deleteMallData(\"" + id + "\")'><img src='/assets/images/member/icon_1.png' alt=''></p></div>";
        new_li.innerHTML = append_con;
        $(cate_cls_nm).append(new_li);
    };

    var updateMallData = function(item, mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, sub_mall_cert_1st, sub_mall_cert_2nd
                            , passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no
                            , otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key) {
        $(item).attr("data-mall_nm", mall_nm);
        $(item).attr("data-mall_cd", mall_cd);
        $(item).attr("data-cert_step", cert_step);
        $(item).attr("data-mall_cert_1st_id", mall_cert_1st_id);
        $(item).attr("data-mall_cert_1st_passwd", mall_cert_1st_passwd);
        $(item).attr("data-sub_mall_cert_1st", sub_mall_cert_1st);
        $(item).attr("data-sub_mall_cert_2nd", sub_mall_cert_2nd);
        $(item).attr("data-passwd_step", passwd_step);
        $(item).attr("data-mall_cert_typ_cd", mall_cert_typ_cd);
        $(item).attr("data-mall_cert_2nd_passwd", mall_cert_2nd_passwd);
        $(item).attr("data-otp_auth_use_yn", otp_auth_use_yn);
        $(item).attr("data-otp_auth_esse_yn", otp_auth_esse_yn);
        $(item).attr("data-mall_auth_otp_key", mall_auth_otp_key);
        $(item).data("goods_cate_seq_no", goods_cate_seq_no);
    };

    var deleteMallData = function(obj) {
        var modalId = showConfirm("등록한 계정을<br>삭제하시겠습니까?", function() {
            var id = obj.split("_")[0] + "_" + obj.split("_")[1];
            $("li[id*='" + id + "']").each(function(index, item) {
                item.remove();
            });                
        
            all_length = $(".list_choiced_mk_box li").length;
            $(".all_num").html(all_length);
            if(all_length < 1) {
                $(".list_choiced_mk_box").css("display","none");
                $(".list_choiced_empty_box").css("display","block");
            }
            else{
                $(".list_choiced_empty_box").css("display","none");
                $(".list_choiced_mk_box").css("display","block");
            }
            removeModal(modalId);
        });
    };

$(document).ready(function() {
    // 2019-07-17 스크롤 유지
    $(".modal_alarm, .market_box").off("mouseover");
    $(".modal_alarm, .market_box").off("mouseout");


    /**
	 * 이용 가능 판매몰 목록 추가
	 */
	if( $('#usable_store').length > 0 ) {
		$('.market_box li').each(function(){
			const marketBoxLi = $(this).clone();
			$('#usable_store .usable_store_body ul').append(marketBoxLi);
		});
	}

    
    $(function() {
        var all_mall = [];
        $(".market_choice .mall_nm").each(function(){ 
            all_mall.push($(this).val()) 
        });
        
        $(".market_box_find_text_area").autocomplete({
            source: all_mall,
            select: function(event, ui) {
                
                for(var i = 0; i < $('.group_list_mall li').length; i++){  
                    if(ui.item.value == $('.group_list_mall li span').eq(i).text()){	
                        var img_tags = $('.group_list_mall li span').eq(i).prev('img').attr('src');
                        clickM = $('.group_list_mall li').eq(i);
                        img_mall_name = clickM.attr("name");
                        $(".popup_account.type1 .logo_area_account img").attr('src',img_tags);
                        clickM.click();
                    }
                }
            },
            focus: function(event, ui) {
                return false;
            }
        });
    });
    
    $(".group_list_mall li, #usable_store li").on("click", function() {
    
        $("#btn_type").val("add");
        clickM = $(this);
        var imgLogo = clickM.find("img").attr("src");
        var mallImg = clickM.find("img");
        img_mall_name = clickM.attr("name");
        var open_leng = $(".mall_lists_1 li").length;
        $(".popup_account.type1").addClass("active");
        $(".popup_account.type1 .logo_area_account img").attr('src', imgLogo);
        
        var sub_mall_cert_1st_nm = clickM.children(".sub_mall_cert_1st_nm").val();
        var sub_mall_cert_2nd_nm = clickM.children(".sub_mall_cert_2nd_nm").val();
        var mall_cert_2nd_passwd_nm = clickM.children(".mall_cert_2nd_passwd_nm").val();
        var sub_mall_cert_step = clickM.children(".sub_mall_cert_step").val();
        
        var passwd_step = clickM.children(".passwd_step").val(); 
        var sub_mall_auth_1st_esse_yn = clickM.children(".sub_mall_auth_1st_esse_yn").val(); 
        var sub_mall_auth_2nd_esse_yn = clickM.children(".sub_mall_auth_2nd_esse_yn").val(); 
        var otp_auth_use_yn = clickM.children(".otp_auth_use_yn").val();
        var otp_auth_esse_yn = clickM.children(".otp_auth_esse_yn").val();
        var otp_auth_typ_cd = clickM.children(".otp_auth_typ_cd").val();
        var otp_auth_typ_cd_nm = clickM.children(".otp_auth_typ_cd_nm").val();
        var mall_nm = clickM.children(".mall_nm").val(); 
        var mall_cd = clickM.children(".mall_cd").val();
        
        /**
         * 2020-09-10 몰 정보 등록 시 아이디 입력 가능 처리
         */
        $("input[name='mall_cert_1st_id']").attr("disabled", null);

        $("input[name='mall_nm']").val(mall_nm);
        $("input[name='mall_cd']").val(mall_cd);
        $("input[name='cert_step']").val(sub_mall_cert_step);
        $("input[name='mall_cert_1st_id']").val('');
        $("input[name='mall_cert_1st_passwd']").val('');
        $("input[name='sub_mall_cert_1st']").val('');
        $("input[name='sub_mall_cert_2nd']").val('');
        $("input[name='passwd_step']").val(passwd_step);
        $("#i_mall_cert_typ_cd").html('<option value="A00"></option>');
        $("input[name='mall_cert_2nd_passwd']").val('');
        $("input[name='sub_mall_auth_1st_esse_yn']").val(sub_mall_auth_1st_esse_yn);
        $("input[name='sub_mall_auth_2nd_esse_yn']").val(sub_mall_auth_2nd_esse_yn);
        // 카테고리 추가
        $("select[name='goods_cate_seq_no']").val( $("select[name='goods_cate_seq_no'] option:first").val() );

        $("input[name='mall_auth_otp_key']").val('');
        $("input[name='qr_code_file']").val('');
        $("input[name='otp_auth_use_yn']").val(otp_auth_use_yn);
        $("input[name='otp_auth_esse_yn']").val(otp_auth_esse_yn);
        $("input[name='otp_auth_typ_cd']").val(otp_auth_typ_cd);
        $("input[name='otp_auth_typ_cd_nm']").val(otp_auth_typ_cd_nm);
        
        if(clickM.children(".sub_mall_cert_1st_nm").val() == "" || sub_mall_cert_1st_nm == null){
            $("#div_sub_mall_cert_1st_nm").hide();
        }else{
            $("#t_sub_mall_cert_1st_nm").html(sub_mall_cert_1st_nm);
            $("#div_sub_mall_cert_1st_nm").show();
        }
        
        if(clickM.children(".sub_mall_cert_2nd_nm").val() == "" || sub_mall_cert_2nd_nm == null){
            $("#div_sub_mall_cert_2nd_nm").hide();
        }else{
            $("#t_sub_mall_cert_2nd_nm").html(sub_mall_cert_2nd_nm);
            $("#div_sub_mall_cert_2nd_nm").show();
        }
        
        if(clickM.children(".mall_cert_2nd_passwd_nm").val() == "" || mall_cert_2nd_passwd_nm == null){
            $("#div_mall_cert_2nd_passwd_nm").hide();
        }else{
            $("#t_mall_cert_2nd_passwd_nm").html(mall_cert_2nd_passwd_nm);
            $("#div_mall_cert_2nd_passwd_nm").show();
        }

        if (clickM.children(".otp_auth_use_yn").val() == "Y" || otp_auth_use_yn == "Y") {
            if (otp_auth_typ_cd == "OTP001") {
                $(".input_otp.file > input[name=mall_auth_otp_key]").attr("disabled", true);
                $(".input_otp.file > input[name=mall_auth_otp_key]").attr("class", "textbox_otp_file");
                $(".input_otp.file > label[for=fileUpload]").show();
            } else if (otp_auth_typ_cd == "OTP002") {
                $(".input_otp.file > input[name=mall_auth_otp_key]").attr("disabled", false);
                $(".input_otp.file > input[name=mall_auth_otp_key]").attr("class", "textbox_mb");
                $(".input_otp.file > label[for=fileUpload]").hide();
            }

            $("#div_mall_auth_otp_key").show();
        } else {
            $("#div_mall_auth_otp_key").hide();
        }
        
        if(clickM.children(".mall_cert_typ").length == 0){
            $("#div_mall_cert_typ_cd").hide();
        }else{
            var html = "";
            clickM.children(".mall_cert_typ").each(function(i,v){
                html += "<option value='"+$(this).val()+"'>"+$(this).data("type_nm")+"</option>";
            });
            $("#i_mall_cert_typ_cd").html(html);
            $("#div_mall_cert_typ_cd").show();
        }

        /**
         * 2020-06-22 판매몰별 도움말 & 영상 설정
         */
        setHelpInfo(clickM);
        setVideoInfo(clickM);
            
        $(".popup_account.active .close_pop").click(function() {
            $(this).parents(".popup_account").removeClass("active");
            return false;
        });
    });

    // 입력완료
    $(".frm_input_btn").click(function() {
        var ck = img_mall_name.split('open');
        var ck2 = img_mall_name.split('soci');
        var ck3 = img_mall_name.split('syn');
        var ck4 = img_mall_name.split('pg');
        var ck5 = img_mall_name.split('spe');
        var ck6 = img_mall_name.split('off');
        var ck8 = img_mall_name.split('etc');
        var open_leng = $(".mall_lists_1 li").length;
        
        var mall_cd = $("input[name='mall_cd']").val();
        var mall_nm = $("input[name='mall_nm']").val();
        var cert_step = $("input[name='cert_step']").val();
        var mall_cert_1st_id = $("input[name='mall_cert_1st_id']").val();
        var mall_cert_1st_passwd = $("input[name='mall_cert_1st_passwd']").val();
        var sub_mall_cert_1st = $("input[name='sub_mall_cert_1st']").val();
        var sub_mall_cert_2nd = $("input[name='sub_mall_cert_2nd']").val();
        var passwd_step = $("input[name='passwd_step']").val();
        var mall_cert_typ_cd = $("#i_mall_cert_typ_cd").val();
        var mall_cert_2nd_passwd = $("input[name='mall_cert_2nd_passwd']").val();
        var sub_mall_auth_1st_esse_yn = $("input[name='sub_mall_auth_1st_esse_yn']").val();
        var sub_mall_auth_2nd_esse_yn = $("input[name='sub_mall_auth_2nd_esse_yn']").val();
        var goods_cate_seq_no = $("select[name='goods_cate_seq_no']").val();
        
        // OTP 인증 추가
        var otp_auth_use_yn = $("input[name='otp_auth_use_yn']").val();
        var otp_auth_esse_yn = $("input[name='otp_auth_esse_yn']").val();
        var mall_auth_otp_key = $("input[name='mall_auth_otp_key']").val();
        
        if(mall_cert_1st_id == ""){
            showAlert("ID를 입력해 주세요");
            return;
        }
        
        if(mall_cert_1st_passwd == ""){
            showAlert("패스워드를 입력해 주세요");
            return;
        }
        
        if(sub_mall_auth_1st_esse_yn == "Y" && sub_mall_cert_1st == ""){
            showAlert($("#t_sub_mall_cert_1st_nm").html()+"를 입력해 주세요");
            return;
        }
        if(sub_mall_auth_2nd_esse_yn == "Y" && sub_mall_cert_2nd == ""){
            showAlert($("#t_sub_mall_cert_2nd_nm").html()+"를 입력해 주세요");
            return;
        }
        
        if(passwd_step == 1){
            if(mall_cert_2nd_passwd == ""){
                showAlert($("#t_mall_cert_2nd_passwd_nm").html()+"를 입력해 주세요");
                return;	
            }	
        }

        if (otp_auth_esse_yn == "Y" && otp_auth_use_yn == "Y" && mall_auth_otp_key == "") {
            showAlert($("#t_mall_auth_otp_key").html() + "를 입력해 주세요");
            $("#mall_auth_otp_key_error").text("*" + $("#t_mall_auth_otp_key").text() + "를 입력해 주세요");
            $("#mall_auth_otp_key_error").show();
            return;
        } else {
            $("#mall_auth_otp_key_error").hide();
        }

        if(isNull(goods_cate_seq_no)){
            showAlert("카테고리를 선택해 주세요");
            return;
        }

        var id = mall_cd + "_" + mall_cert_1st_id;
        if($("#btn_type").val() == "modify") {
            $("li[id*='" + id + "']").each(function(index, item){
                updateMallData(item, mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key
                );
            });
        }
        else {

            var cnt=0;
            $("li[id*='" + id + "']").each(function(index, item){
                cnt++
            });

            // 네이버페이 등록시 가맹점ID(046, 047)까지 비교하여 중복 체크
            $("li[id*='" + id + "']").each(function(index, item){
                if(mall_cd == "046" || mall_cd == "047"){
                    if(sub_mall_cert_1st == $(this).find(".mk_sub_id").text()){
                        cnt++;
                    }
                }else{
                    cnt++;
                }
            });

            if(cnt > 0) {
                showAlert("동일한 정보의 몰 정보가 존재합니다.");
                return;
            }

            /**
             * 2020-09-10 롯데ON 노출 조건 반영, 롯데ON은 오픈마켓, 종합몰(백화점) 모두 표시 
             */
            if(mall_cd == "060") {
                // 오픈마켓
                addMallData(".open_append", id+"_1", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
                // 종합몰(백화점)
                addMallData(".open_append_3", id+"_3", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
            }
            else if (ck.length > 1) {
                addMallData(".open_append", id+"_1", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
            }
            else if (ck2.length > 1) {
                addMallData(".open_append_2", id+"_2", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
            }
            else if (ck3.length > 1) {
                addMallData(".open_append_3", id+"_3", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
            }
            else if (ck4.length > 1) {
                addMallData(".open_append_4", id+"_4", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
            }
            else if (ck5.length > 1) {
                addMallData(".open_append_5", id+"_5", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
            }
            else if (ck6.length > 1) {
                addMallData(".open_append_6", id+"_6", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
            }
            else if (ck8.length > 1) {
                addMallData(".open_append_8", id+"_8", mall_nm, mall_cd, cert_step, mall_cert_1st_id, mall_cert_1st_passwd, 
                sub_mall_cert_1st, sub_mall_cert_2nd, passwd_step, mall_cert_typ_cd, mall_cert_2nd_passwd, goods_cate_seq_no,
                otp_auth_use_yn, otp_auth_esse_yn, mall_auth_otp_key);
            }
        }
                
        $(this).parents(".popup_account").removeClass("active");
        
        var all_length = $(".list_choiced_mk_box li").length;
        $(".all_num").html(all_length);
        if(all_length < 1) {
            $(".list_choiced_mk_box").css("display","none");
            $(".list_choiced_empty_box").css("display","block");
        }
        else{
            $(".list_choiced_empty_box").css("display","none");
            $(".list_choiced_mk_box").css("display","block");
        }
    });
    
    //select2
    function format(option) {
        var originalOption = option.element;
        var valOption = $(originalOption).val();
        if (!option.id) return option.text; // optgroup
        return "<div class='mall_logo'><img src='/assets/images/member/logo/" + valOption + ".png'/>" + option.text + "</div>";
    }
    $(".select_mall").select2({
        placeholder: "판매몰 검색",
        allowClear: true,
        templateResult: format,
        templateSelection: format,
        escapeMarkup: function(m) {
        return m;
        }
    });

    $(".select_mall").on("select2:select", function(e) {
        var selectMImg = $(".select2-selection__rendered .mall_logo img").attr("src");
        $(".sh_mall_btn").click(function() {
            $(".popup_account.type1").addClass("active");
            $(".popup_account.type1 .logo_area_account img").attr('src', selectMImg);
            $(".popup_account.type1 .close_pop").click(function() {
                $(".popup_account.type1").removeClass("active");
                return false;
            });
        });
    });
    $(".btn_cicle").click(function(){
        var paramArray = new Array();
        $(".paramData").each(function(){
            var mall_cd = $(this).data("mall_cd");
            var pushData  = {
                mall_nm :$(this).data("mall_nm"),
                mall_cd :mall_cd,
                cert_step : $(this).data("cert_step"),
                mall_cert_1st_id : $(this).data("mall_cert_1st_id"),
                mall_cert_1st_passwd : $(this).data("mall_cert_1st_passwd"),
                sub_mall_cert_1st : $(this).data("sub_mall_cert_1st"),
                sub_mall_cert_2nd : $(this).data("sub_mall_cert_2nd"),
                passwd_step : $(this).data("passwd_step"),
                mall_cert_typ_cd : $(this).data("mall_cert_typ_cd"),
                mall_cert_2nd_passwd : $(this).data("mall_cert_2nd_passwd"),
                goods_cate_seq_no: $(this).data("goods_cate_seq_no")
            };

            /**
             * 2020-09-10 롯데ON은 오픈마켓과 종합몰(백화점)에 모두 표시해야되기 때문에 몰 등록 시 중복된 데이터는 제외시키기
             */
            if(mall_cd == "060") {
                var valiItem = paramArray.filter(function(param){
                    if( pushData.mall_cd == param.mall_cd && pushData.mall_cert_1st_id == param.mall_cert_1st_id ){
                        return param;
                    }
                });
                
                if(valiItem.length > 0) {
                    // 롯데ON의 중복된 데이터 추가안함
                }
                else {
                    paramArray.push(pushData);
                }
            }
            else {
                paramArray.push(pushData);
            }
        });

        if(paramArray.length == 0){
            showAlert("판매몰을 등록하여 주시기 바랍니다.");
            return false;
        }
        
        for( var i in paramArray ){
            var item = paramArray[i];
            var valiItem = paramArray.filter(function(param){
                // 네이버페이 등록시 가맹점ID(046, 047)까지 비교하여 중복 체크
                if(param.mall_cd == '046' || param.mall_cd == '047'){
                    if( item.mall_cd == param.mall_cd && item.mall_cert_1st_id == param.mall_cert_1st_id && item.sub_mall_cert_1st == param.sub_mall_cert_1st ){
                        return param;
                    }
                }else{
                    if( item.mall_cd == param.mall_cd && item.mall_cert_1st_id == param.mall_cert_1st_id ){
                        return param;
                    }
                }
            });
            
            if(valiItem.length > 1){
                showAlert(item.mall_nm + "의 " + item.mall_cert_1st_id + " 아이디가 중복해서 입력되어있습니다.");
                return false;
            }
        }
        
        $.ajax({
            url: '/sub/member/create_mall'
            , type: 'post'
            , async : false
            , dataType: 'json'   
            , contentType: 'application/json'
            , data: JSON.stringify(paramArray)
            , success: function(response) {
                if(response.result == "OK"){
                    showAlert("입력하신 판매몰 정보가<br>등록되었습니다.", function(){
                        location.href="/sub/member/join_step5";	
                    });
                }else{
                    showAlert("저장에 실패 하였습니다.");
                }
            }
            , error : function(error){
                showAlert("저장실패 하였습니다.");
            }
        });
    });

    //20210720. 금융상품 사용가능 쇼핑몰 정보 말풍선
    $('.tooltipEvent').on({
        mouseover: function(){
            var c = $(this).attr("class");
            if (c.indexOf("bt-tooltip") > 0) {
            } else {
                $(this).addClass('bt-tooltip');
            }
        },
        mouseout: function(){
            $(this).removeClass('bt-tooltip');
        },
        click: function(){
            var c = $(this).attr("class");
            if (c.indexOf("bt-tooltip") > 0) {
                $(this).removeClass('bt-tooltip');
            } else {
                $(this).addClass('bt-tooltip');
            }
        }
    });
});
</script>