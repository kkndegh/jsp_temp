<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<div class="container">
	<div class="member_wra long_member_wra">
		<div class="left_title_area">
			<div class="left_title_longArea">
				<div class="step_join_tab col3">
					<span>04</span>
					<span class="active">05</span>
					<span>06</span>
				</div>
				<div class="left_title_item">
					<div class="step_box">
						<div class="title_txt">알림톡까지<br>신청해보세요</div>
						<p class="sub_title_txt">매일 원하는 시간에<br>
							셀러봇캐시가 보고해드려요.</p>
					</div>
				</div>
				<!-- <div class="cs_center_info">
					<p class="cs-info-none">고객센터 : 1666-8216<br>
						s-cash@only1fs.com</p>
				</div> -->
			</div>
		</div>
		<div class="right_contents_area">
			<div class="complete_box">
				<div class="complete_item">
					<form action="/" onsubmit="return false;" id="form">
						<div class="alarm_setting_box">
							<p class="imgbox"><img src="/assets/images/intro/intro_ph.png" alt=""></p>
							<div class="contents_alarm">
								<p class="title_alarm"><span>셀러봇캐시</span>에게<br>
									아침마다 리포트 받기</p>
								<p class="desc_alarm">매일 차곡차곡 쌓이는 정산내역!<br>
									하루도 빠짐없이 밤새 열일하는<br>
									셀러봇캐시 알림톡서비스를 받아보실래요?
								</p>
								<div class="admin_choice">
									<div class="item_admin">
										<input type="checkbox" class="chk_admin" id="admin1" checked name="admin1">
										<label for="admin1">대표자명 :&nbsp;</label>
										<div class="sub_admin_info">
											<div class="item_sub_input txt1"> ${cust.ceo_nm}</div> <span
												class="m_block">|</span> <span class="admin_label_caption">대표자 연락처 :
											</span>
											<div class="item_sub_input txt2">${cust.ceo_no}</div>
											<input type="hidden" value="${cust.ceo_nm}" name="ceo_nm" id="ceo_nm" />
											<input type="hidden" value="${cust.ceo_no}" name="ceo_no" id="ceo_no" />
										</div>
										<div class="time_alarm">
											<span class="lb">수신시간 : </span>
											<select class="sctbox" name="trs_hop_hour1" id="trs_hop_hour1">
												<option value="08">08시</option>
												<option value="09">09시</option>
												<option value="10">10시</option>
												<option value="11">11시</option>
												<option value="12">12시</option>
												<option value="13">13시</option>
												<option value="14">14시</option>
												<option value="15">15시</option>
												<option value="16">16시</option>
												<option value="17">17시</option>
												<option value="18">18시</option>
												<option value="19">19시</option>
												<option value="20">20시</option>
											</select>
										</div>
									</div>
									<div class="item_admin chrg-area">
										<input type="checkbox" class="chk_admin" id="admin2" name="admin2">
										<label for="admin2">담당자명 :</label>
										<div class="sub_admin_info">
											<div class="item_sub_input txt1">
												<input type="text" value="${cust.chrg_nm}"
													class="admin_txt input_color_gray" readonly name="chrg_nm"
													id="chrg_nm">
												<!-- <img src="/assets/images/member/icon_edit_Off.png" alt=""> -->
											</div>
											<span class="m_block">|</span> <span class="admin_label_caption">담당자 연락처 :
											</span>
											<div class="item_sub_input txt2">
												<input type="text" value="${cust.chrg_no}"
													class="admin_txt input_color_gray" readonly name="chrg_no"
													id="chrg_no">
												<!-- <img src="/assets/images/member/icon_edit_Off.png" alt=""> -->
											</div>
										</div>
										<div class="time_alarm">
											<span class="lb">수신시간 : </span>
											<select class="sctbox" name="trs_hop_hour2" id="trs_hop_hour2">
												<option value="08">08시</option>
												<option value="09">09시</option>
												<option value="10">10시</option>
												<option value="11">11시</option>
												<option value="12">12시</option>
												<option value="13">13시</option>
												<option value="14">14시</option>
												<option value="15">15시</option>
												<option value="16">16시</option>
												<option value="17">17시</option>
												<option value="18">18시</option>
												<option value="19">19시</option>
												<option value="20">20시</option>
											</select>
										</div>
									</div>
								</div>
								<div class="talk_alarm">
									<input type="checkbox" class="chk_admin_set" id="chk_admin_set" checked
										name="chk_admin_set">
									<label for="chk_admin_set">네, 알림톡을 받아볼래요.</label>
								</div>
							</div>
						</div>
						<div class="btn_mb_group">
							<!-- <button class="btn_cicle btn_gray" type="button" onclick="history.go(-1)">이전</button> -->
							<button class="btn_cicle" onclick="fn_save()">완료</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

	function fn_save() {

		if (!$("#chk_admin_set").is(":checked")) {
			location.href = "/sub/member/join_step6";
			return;
		}

		var chrg_no = $("#chrg_no").val();

		if ($("#admin2").is(":checked")) {

			if (!isNumber(chrg_no)) {
				showAlert("담당자 번호는 숫자만 입력 가능합니다.");
				return;
			}
			if (!isMobile(chrg_no)) {
				showAlert("담당자 번호를 확인해 주세요");
				return;
			}
		}

		var formData = [];

		if ($("#admin1").is(":checked")) {
			var param = {};
			param.cust_posi_cd = "CEO";
			param.tgt_cust_nm = $("#ceo_nm").val();
			param.tgt_cust_no = $("#ceo_no").val();
			param.req_dt = fnGetToDay("YYYYMMDD");
			param.trs_hop_hour = $("#trs_hop_hour1").val();
			formData.push(param);
		}

		if ($("#admin2").is(":checked")) {
			var param = {};
			param.cust_posi_cd = "CRG";
			param.tgt_cust_nm = $("#chrg_nm").val();
			param.tgt_cust_no = $("#chrg_no").val();
			param.req_dt = fnGetToDay("YYYYMMDD");
			param.trs_hop_hour = $("#trs_hop_hour2").val();
			formData.push(param);
		}


		$.ajax({
			url: '/sub/my/alarm_input',
			data: JSON.stringify(formData),
			contentType: "application/json",
			processData: false,
			async: true,
			type: 'POST',
			success: function (data) {
				showAlert("입력하신 알림톡 신청정보가<br>등록되었습니다.", function () {
					location.href = "/sub/member/join_step6";
				});
				return false;
			},
			error: function (request, status, error) {
				showAlert("등록에 실패하였습니다.");
			}
		});
	}

	$("#admin2").on("click", function () {

		if ($("#admin2").is(":checked")) {
			$("#chk_admin_set").prop("checked", true);
			$("#admin1").prop("checked", true);
		} else {
			$(".item_sub_input ").removeClass("edit");
			$(".item_sub_input ").find("input").prop("disabled", true);
			$(".item_sub_input img").attr("src", "/assets/images/member/icon_edit_Off.png");
			$("#chrg_no").attr("readonly", true);
		}

	});

	$(".item_sub_input img").on("click", function () {
		if ($("#admin2").is(":checked")) {
			var icon = $(this);
			if (icon.parents(".item_sub_input").hasClass("edit") == false) {
				icon.parents(".item_sub_input").addClass("edit");
				icon.parents(".item_sub_input").find("input").prop("disabled", false);
				icon.attr("src", "/assets/images/member/icon_edit_On.png");
				icon.siblings("input").attr("readonly", false);
			} else {
				icon.siblings(".item_sub_input").removeClass("edit");
				icon.siblings(".item_sub_input").find("input").prop("disabled", true);
				icon.attr("src", "/assets/images/member/icon_edit_Off.png");
				icon.parents("input").attr("readonly", true);
			}
		}
	});


	$("#admin1").on("click", function () {
		if ($(this).is(":checked")) {
			$("#chk_admin_set").prop("checked", true);
		} else {
			$("#chk_admin_set").prop("checked", false);
		}
	});

	$("#chk_admin_set").on("click", function () {
		if ($(this).is(":checked")) {
			$("#admin1").prop("checked", true);
		} else {
			$("#admin1").prop("checked", false);
			$("#admin2").prop("checked", false);
		}
	});


	$(document).ready(function () {

		if (isNull($("#chrg_nm").val()) || isNull($("#chrg_no").val())) {
			$(".chrg-area").hide();
		}


	});


</script>