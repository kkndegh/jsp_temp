<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<div class="container">
	<div class="member_wra">
		<div class="left_title_area">
			<div class="step_join_tab">
				<span>01</span>
				<span class="active">02</span>
				<span>03</span>
			</div>
			<div class="left_title_item">
				<div class="step_box">
					<div class="title_txt">본인확인이<br>필요해요.</div>
					<p class="sub_title_txt">본인확인을 위해<br>이메일과 인증번호를 입력해주세요.</p>
				</div>
			</div>
		</div>
		<div class="right_contents_area">
			<div class="join_section">
				<form onsubmit="return false;" novalidate="novalidate" autocomplete="off">
					<div class="member_right_section">
						<div class="member_frm_box">
							<div class="item_mb">
								<span class="lb">이메일(ID)</span>
								<div class="input_box">
									<c:set var="hasCustId" value="${custDto != null}" />
									<c:set var="hasCafeStore" value="${cafeStore != null}" />
									<input id="custId" type=text class="textbox_mb"
										value="${hasCustId ? custDto.cust_id : hasCafeStore? cafeStore.email:'' }" ${hasCustId ? ' readonly' :'' }
										autocomplete="new-cust-id" required="required" data-valitype="email">
									<p id="custId_error" class="error">*이메일(ID@E-mail.com)을 정확히 입력해주세요.</p>
								</div>
							</div>
							<div class="item_mb last">
								<span class="lb">인증번호</span>
								<div class="input_box">
									<input type="text" id="authNo" class="textbox_mb" required="required"
										data-valitype="number">
									<p id="authNo_error" class="error">*인증번호를 입력해주세요.</p>
									<div class="alert_send" style="display: none">입력해주신 이메일(<span
											class="data_email"></span>)로 인증번호가 <span id="sendTxt">전송</span>되었습니다.</div>
								</div>
							</div>
						</div>
					</div>
					<div class="btn_mb_group">
						<button class="btn_cicle" id="checkAuthNo">다음</button>
						<button type="button" class="btn_cicle btn_gray" id="sendSertNum">인증번호<br>발송</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<script>
	$(document).ready(function () {

		$('input[type="text"]').keydown(function (e) {
			if (e.keyCode === 13) {
				e.preventDefault();
				if ($(this).attr("id") == "custId") {
					$("#sendSertNum").click();
				} else {
					$("#checkAuthNo").click();
				}

			}
		});

		// 인증 번호 발송전 validation 체크
		function isValiSendNum() {
			$(".error").hide();

			var custId = $("#custId").val();

			if (custId != null && custId != "") {
				custId = custId.trim();
			}

			if (!isEmail(custId)) {
				$("#custId_error").show();
				$("#custId").focus();
				return false;
			}
			return true;
		}

		// 인증 번호 발송
		$("#sendSertNum").click(function (event) {
			event.preventDefault();
			if (!isValiSendNum()) {
				return false;
			}

			var custId = $("#custId").val().trim();

			$.post("/pub/api/member/join/confirmCust", { custId: custId }, function (data, status) {
				$('#custId').attr('readonly', true);
				$("#sendSertNum").html("인증번호<br>재발송");

				if (nonNull($(".data_email").text())) { $("#sendTxt").text("재전송"); }
				$(".data_email").text(custId);
				$(".alert_send").show();
				$("#authNo").focus();

			}).fail(function (response, data) {
				if (response.status == 410) { // 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
					location.reload();
				} else if (response.status == 409) { // 등록된 아이디가 있음
					showAlert(custId + " 이메일은 <br>이미 등록되어 있습니다.<br> 아이디 찾기를 진행 하세요.");
				} else if (response.status == 4490) { // 이미 ID에 인증번호가 발송된 상태
					showAlert("인증 번호가 발송된 상태 입니다.");
					$(".data_email").text(custId);
					$(".alert_send").show();
					$("#authNo").focus();
				} else {
					// showAlert("Error: " + response.status);
					showAlert(data.error.comment);
				}
			});

			return false;
		});

		// 인증번호 검증후 다음 스템 진행
		$("#checkAuthNo").click(function (event) {

			if (!isVali()) { return false; }

			var authNo = $("#authNo").val();

			if ($('#custId').attr('readonly')) {
				$.post("/pub/api/member/join/confirmAuthNo", { authNo: authNo }, function (data, status) {
					window.location.href = "/pub/member/join_step3";
				})
					.fail(function (response) {
						if (response.status == 410) { // 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
							location.reload();
						} else if (response.status == 400) { // 인증번호 오류
							showAlert("인증번호를 다시 확인하시기 바랍니다.", function () {
								$("#authNo").focus();
							});
						} else if (response.status == 4490) { // 인증 번호 발송을 요청 하지 않은 상태
							showAlert("인증번호 발송을 먼저 하세요.");
							$(".data_email").text($("#custId").val());
							$(".alert_send").show();
							$("#authNo").focus();
						} else {
							showAlert("Error: " + response.status);
						}
					});
			} else {
				showAlert("인증번호 발송을 먼저 하세요.");
			}
		});



	});

</script>