<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<div class="container">
	<div class="member_wra">
		<div class="left_title_area">
			<div class="left_title_item">
				<div class="step_box">
					<div class="title_txt">판매몰을<br>등록해주세요.</div>
					<p class="sub_title_txt">현재 판매하고 있는<br>쇼핑몰을 선택해주세요.</p>
				</div>
			</div>
		</div>
		<div class="right_contents_area">
			<div class="complete_box">
				<div class="complete_item">
					<img src="/assets/images/member/img_signup.png">
					<p class="complete_msg">
						<c:choose>
							<c:when test="${ empty usingGoods}">
								<span>판매몰 등록 </span>후 서비스 이용이 가능해요!<br>
								<!-- <span>판매몰을 등록</span>해볼까요? -->
								<button class="btn_reg" onclick="window.location.href='${goUrl}'">등록하기</button>
							</c:when>
							<c:otherwise>
								<span style="font-size: 2.0em;">회원가입이 완료되었습니다</span><br><br>
								지금 <span>판매몰 정보를 등록</span>하시면, <span>정산예정금 정보</span>를 확인하실 수 있어요!<br><br>
								<span style="color: #0070C0;">${usingGoods.addn_pay_typ_cd_nm}</span> 지급완료! 
								<span style="color: #0070C0;">${usingGoods.addn_goods_nm}</span>
								<font style="color: #0070C0;">(${usingGoods.addn_use_stt_dt}~${usingGoods.addn_use_end_dt})</font><br>
								<!-- <span>판매몰을 등록</span>해볼까요? -->
								<button class="btn_reg" style="float:left; margin-left:5.5em" onclick="window.location.href='${goUrl}'">등록하기</button>
							</c:otherwise>
						</c:choose>
					</p>
				</div>
			</div>
		</div>
	</div>

</div>

<c:if test="${pageContext.request.serverName eq 'www.sellerbot.co.kr'}">
	<!-- 구글 분석기 전환페이지 설정 -->
	<script type="text/javascript">
		var _nasa = {};
		_nasa["cnv"] = wcs.cnv("2", "10"); // 전환유형, 전환가치 설정해야함. 설치매뉴얼 참고
	</script>

	<!-- 페이스북 전환페이지 설정 -->
	<script>
		fbq('track', 'CompleteRegistration');	
	</script>

	<!-- 구글 ADS 전환페이지 설정 -->
	<script>
		gtag('event', 'conversion', { 'send_to': 'AW-691347026/DEKjCK_f2LYBENK81MkC' });
	</script>

	<!-- 네이버 전환페이지 설정 -->
	<script type="text/javascript"> 
		var _nasa={};
		_nasa["cnv"] = wcs.cnv("2","1"); // 전환유형, 전환가치 설정해야함. 설치매뉴얼 참고
	</script>
</c:if>
	<script type="text/javascript"> 
		setTimeout(function () {
			window.location.href='${goUrl}'
		}, 3000);
	</script>