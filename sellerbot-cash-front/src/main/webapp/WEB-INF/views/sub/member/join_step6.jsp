<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<script src="/assets/js/mngAcct.js?ver=20211122_01"></script>
<input type="hidden" name="delConfirm" id="delConfirm" value="${delConfirm}">
<input type="hidden" id="regAcctTestSuccYN" name="regAcctTestSuccYN" value="N">
<input type="hidden" id="usingGoodFlag" name="usingGoodFlag" value="${usingGoodFlag}">

<div class="container">
    <div class="member_wra long_member_wra">
        <div class="left_title_area">
            <div class="left_title_longArea">
                <div class="step_join_tab col3">
                    <span>04</span>
                    <span>05</span>
                    <span class="active">06</span>
                </div>
                <div class="left_title_item">
                    <div class="step_box">
                        <div class="title_txt">정산계좌를<br>등록하세요</div>
                        <p class="sub_title_txt">판매몰별 입금내역과 계좌 입출금 내역을<br> 한 곳에서 한 번에 볼 수 있어요.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="right_contents_area">
            <div class="complete_box">
                <!-- 정산계좌 등록 안내 Start -->
                <section class="account_first">
                    <p class="icon_account">
                        <img src="/assets/images/my/img_account_register.png" alt="">
                    </p>
                    <div class="txt_account_first">
                        <p>나도 모르는 정산금 입금내역,<br>
                            <span>등록 한 번</span>이면 쉽고 빠르게 볼 수 있어요.</p>
                        <a class="step_6_enroll" style="cursor: pointer;">정산계좌 등록</a>
                    </div>
                </section>
                <!-- 정산계좌 등록 안내 End -->
                <!-- 정산계좌 등록 Start -->
                <div class="my_section">
                    <div class="my_section_wrapper">
                        <div class="add_pooup_close add_pooup_close_step">
                            <div></div>
                            <div></div>
                        </div>
                        <div class="my_section_inner">
                            <div class="guide_register">
                                <h1>
                                    <p>정산계좌 통합관리 도움말</p>
                                    <p class="edit_video_btn2"><span>영상가이드</span></p>
                                </h1>

                                <style>
                                    .edit_video_btn2 {
                                        text-align: right;
                                    }

                                    .edit_video_btn2 span {
                                        font-size: 1rem;
                                        color: #fff;
                                        background-color: #009aca;
                                        display: block;
                                        width: 106px;
                                        text-align: center;
                                        padding: 0.3rem 0;
                                        border-radius: 3rem;
                                        float: right;
                                        cursor: pointer;
                                    }

                                    .guide_register h1 {
                                        font-size: 0;
                                    }

                                    .guide_register h1 p {
                                        display: inline-block;
                                        width: 50%;
                                        font-size: 1.5rem;
                                        vertical-align: middle;
                                        position: static;
                                    }

                                    @media(max-width:720px) {
                                        .btn_mb_group {
                                            position: static;
                                            margin: 40px auto 0;
                                            text-align: center;
                                            margin-bottom: 172px;
                                        }

                                        .btn_mb_group button:first-child {
                                            margin: 0;
                                            float: right;
                                            clear: both;
                                            margin-right: 20px;
                                            margin-bottom: 40px;
                                        }

                                        .item_input_acct {
                                            padding-left: 0;
                                        }

                                        section.account_first {
                                            display: block;
                                        }

                                        .account_first .icon_account img {
                                            display: block;
                                            width: 150px;
                                            margin: 0 auto;
                                            margin-bottom: 2rem;
                                        }

                                        .icon_account {
                                            margin-right: 0 !important;
                                            display: block;
                                        }

                                        .txt_account_first {
                                            max-width: 100%;
                                            display: block;
                                        }

                                        .my_section_wrapper {
                                            max-height: auto !important;
                                        }

                                        .my_section_wrapper {
                                            text-align: left;
                                            position: static !important;
                                            max-width: 60rem;
                                            max-height: 50rem;
                                            border-radius: 1rem;
                                            box-shadow: none !important;
                                            padding: 1rem;
                                            padding-top: 5rem;
                                            right: 11rem;
                                            background-color: #fff;
                                            width: 100% !important;
                                            height: auto !important;
                                            padding: 0 !important;
                                        }

                                        .long_member_wra {
                                            overflow: visible !important;
                                        }

                                        .my_section_wrapper {
                                            max-height: inherit !important;
                                        }

                                        .my_section {
                                            max-height: inherit !important;
                                            height: auto !important;
                                        }

                                        .long_member_wra .complete_box {
                                            padding-left: 0 !important;
                                            padding-right: 0 !important;
                                        }

                                        .right_contents_area {
                                            padding-left: 0 !important;
                                            padding-right: 0 !important;
                                        }

                                        .guide_register span {
                                            padding-left: 0;
                                        }
                                    }
                                </style>
                                <span>
                                    정산계좌 통합관리 서비스를 이용하시려면 각 은행별 홈페이지에서 빠른조회서비스 등록을 진행하신 후<br>
                                    계좌정보를 입력하셔야 합니다. 하단의 은행명을 선택해주시면 은행별 빠른조회서비스 등록 절차를 확인하실 수 있습니다.
                                </span>
                                <span class="guide_register_detail">
                                    <b>산업은행</b><br>
                                    ▣ 개인뱅킹<br>
                                    ① 산업은행 홈페이지 (http://www.kdb.co.kr)에 접속<br>
                                    ② 공인인증서를 통하여 인터넷뱅킹 로그인<br>
                                    ③ [뱅킹관리 → 계좌관리 → 계좌관리] 에서 등록
                                    <br>
                                    <br>
                                    ▣ 개인뱅킹<br>
                                    ① 산업은행 홈페이지 (http://www.kdb.co.kr)에 접속<br>
                                    ② 공인인증서를 통하여 인터넷뱅킹 로그인<br>
                                    ③ - A. 기업뱅킹 (USB 1개 사용) : [뱅킹관리 → 계좌관리 → 빠른조회대상계좌설정]에서 등록<br>
                                    ③ - B. 기업뱅킹 (USB 2개 이상) : [대표관리자 인터넷뱅킹 접속 → 뱅킹관리 → 계좌관리 → 빠른조회대상계좌설정] 에서 등록
                                </span>
                            </div>
                            <div class="frm_account_my">
                                <p class="account_lb">계좌정보 입력</p>
                                <div class="item_input_acct bank_cert_m_seq_no_use_yn">
                                    <p class="lb_input">은행명</p>
                                    <div class="box_input_acct">
                                        <select class="sctbox" id="bank_cert_m_seq_no" required="required">
                                        </select>
                                    </div>
                                </div>
                                <div class="item_input_acct bank_id_use_yn">
                                    <p class="lb_input">빠른조회<br>은행아이디</p>
                                    <div class="box_input_acct">
                                        <input id="bank_id" type="text" class="textbox_mb" maxlength="25" required="required" onchange="changeRegModalData();">
                                        <p id="bank_id_error" class="error" style="display:none;">*은행아이디를 입력해주세요.</p>
                                    </div>
                                </div>
                                <div class="item_input_acct bank_passwd_use_yn">
                                    <p class="lb_input">빠른조회<br>은행비밀번호</p>
                                    <div class="box_input_acct">
                                        <input id="bank_passwd" type="password" class="textbox_mb" maxlength="20" required="required" onchange="changeRegModalData();">
                                        <p id="bank_passwd_error" class="error" style="display:none;">*은행비밀번호 입력해주세요.</p>
                                    </div>
                                </div>
                                <div class="item_input_acct dpsi_nm_use_yn">
                                    <p class="lb_input">예금주</p>
                                    <div class="box_input_acct">
                                        <input id="dpsi_nm" type="text" class="textbox_mb" maxlength="20" required="required" onchange="changeRegModalData();">
                                        <p id="dpsi_nm_error" class="error" style="display:none;">*예금주를 입력해주세요.</p>
                                    </div>
                                </div>
                                <div class="item_input_acct acct_no_use_yn">
                                    <p class="lb_input" id="acctNoTitle">계좌번호</p>
                                    <div class="box_input_acct">
                                        <input id="acct_no" type="text" class="textbox_mb" maxlength="20" autocomplete="off" required="required" data-valitype="acctNo" onchange="changeRegModalData();">
                                        <p id="acct_no_error" class="error" style="display:none;">*계좌번호를 바르게 입력해주세요.</p>
                                    </div>
                                </div>
                                <div class="item_input_acct acct_passwd_use_yn">
                                    <p class="lb_input" id="acctPwdTitle">계좌비밀번호</p>
                                    <div class="box_input_acct">
                                        <input id="acct_passwd" type="password" autocomplete="new-password" class="textbox_mb" maxlength="20" required="required" onchange="changeRegModalData();">
                                        <p id="acct_passwd_error" class="error" style="display:none;">*계좌비밀번호 입력해주세요.</p>
                                    </div>
                                </div>
                                <div class="item_input_acct ctz_biz_no_use_yn">
                                    <p class="lb_input">주민등록<br>(사업자)번호</p>
                                    <div class="box_input_acct">
                                        <input id="ctz_biz_no" type="text" class="textbox_mb" maxlength="10"
                                            placeholder="주민번호 앞 6자리 또는 사업자번호10자리만 입력" required="required" data-valitype="ctzBizno" onchange="changeRegModalData();">
                                        <p id="ctz_biz_no_error" class="error" style="display:none;">*주민등록(사업자) 번호를 바르게 입력해주세요.</p>
                                    </div>
                                </div>
                                <div class="item_input_acct crc_use_cd">
                                    <p class="lb_input">통화</p>
                                    <div class="box_input_acct">
                                        <select id="crc" class="sctbox">
                                            <option value="">선택</option>
                                            <c:forEach items="${codeList }" var="code" varStatus="index">
                                                <option value="${code.com_cd }">${code.cd_nm }</option>
                                            </c:forEach>
                                        </select>
                                        <p id="crc_error" class="error" style="display:none;">*통화를 선택해주세요.</p>
                                    </div>
                                </div>
                                <div class="btn_frm_group">
                                    <button class="enroll_step_2" id="btnTest">계정 테스트</button>
                                    <button class="enroll_step_2" id="btnSave">입력완료</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- 정산계좌 등록 End -->
                <!-- 정산계좌 목록 Start -->
                <section class="account_my">
                    <div class="status_my_account" id="acctStatus">
                        <p class="all">전체 5 /</p>
                        <p class="normal">정상 4 /</p>
                        <p class="error">오류 1</p>
                        <a class="register_acct">계좌 등록</a>
                    </div>
                    <ul class="account_my" id="acctListArea">
                    </ul>
                </section>
                <!-- 정산계좌 목록 End -->
            </div>
        </div>
    </div>
</div>
<div class="btn_mb_group">
    <button type="button" class="btn_cicle" onclick="moveToIndex();">다음</button>
</div>
<div class="edit_video_popup">
    <div class="edit_video_popup_container">
        <p><img src="/assets/images/member/close.png" alt=""></p>
        <iframe width="100%" height="100%"
            src="https://www.youtube.com/embed/OzaIAVrgQEo?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="background_black"></div>
</div>
<script>
    $("#admin2").on("click", function () {
        if ($("#admin2").is(":checked") == true) {
            $(".item_sub_input img").on("click", function () {
                var icon = $(this);
                if (icon.parents(".item_sub_input").hasClass("edit") == false) {
                    icon.parents(".item_sub_input").addClass("edit");
                    icon.parents(".item_sub_input").find("input").prop("disabled", false);
                    icon.attr("src", "/assets/images/member/icon_edit_On.png");
                } else {
                    icon.parents(".item_sub_input").removeClass("edit");
                    icon.parents(".item_sub_input").find("input").prop("disabled", true);
                    icon.attr("src", "/assets/images/member/icon_edit_Off.png");
                }
            });
        } else {
            $(".item_sub_input ").removeClass("edit");
            $(".item_sub_input ").find("input").prop("disabled", true);
            $(".item_sub_input img").attr("src", "/assets/images/member/icon_edit_Off.png");
        }
    });
    //disable input and select//
    $("#chk_admin_set").click(function () {
        if ($(".chk_admin_set").is(":checked") == true) {
            enable()
        } else {
            disable()
        }
    });

    function disable() {
        $(".chk_admin").attr("disabled", true).attr("readonly", false);
        $(".admin_choice").css("opacity", "0.5");
        $(".sctbox").attr("disabled", true);
    }

    function enable() {
        $(".chk_admin").attr("disabled", false).attr("readonly", false);
        $(".admin_choice").css("opacity", "1");
        $(".sctbox").attr("disabled", false);
    }
    $("#admin2").click(function () {
        if ($("#admin2").is(":checked") == true) {
            $(".sub_admin_info").eq(1).show();
            $(".time_alarm").eq(1).css("display", "block");
        } else {
            $(".sub_admin_info").eq(1).hide();
            $(".time_alarm").eq(1).css("display", "none");
        }
    });
</script>
<style>
    .frm_account_my {}

    .account_first {}

    .my_section {
        max-width: 50rem;
        max-height: 50rem;
        width: 90%;
        height: 80%;
    }

    .my_section_close {}

    .my_section_close img {}

    .add_pooup_close_step {
        z-index: 9;
        margin: 1rem;
        right: 0;
    }

    .add_pooup_close_step div {
        background-color: #000;
    }

    .my_section_inner {
        overflow: auto;
        width: 100%;
        height: 100%;
    }

    section.account_my {
        padding: 3em 2em;
        height: 80%;
        margin-top: 7%;
        overflow: auto;
        width: 70%;
    }

    .account_my img {
        width: auto !important;
    }

    div.edit_video_popup {
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        display: none;
        z-index: 9999;
    }

    div.edit_video_popup div.edit_video_popup_container {
        position: relative;
        z-index: 6;
        max-width: 50rem;
        width: 90%;
        height: 30rem;
    }

    .background_black {
        width: 100%;
        height: 100%;
        position: absolute;
        left: 0;
        top: 0;
        background: #000;
        opacity: 0.5;
        z-index: 5;
    }

    div.edit_video_popup div.edit_video_popup_container p {}

    div.edit_video_popup div.edit_video_popup_container p img {
        float: right;
        margin-bottom: 0.5rem;
        cursor: pointer;
    }

    div.edit_video_popup div.edit_video_popup_container iframe {}

    @media(max-width:720px) {
        section.account_my {
            padding: 3em 2em;
            height: 80%;
            margin-top: 7%;
            overflow: auto;
            width: 100%;
        }

        .my_section_inner {
            text-align: left;
            position: static;
            overflow: auto;
            max-width: 50rem;
            max-height: initial;
            width: 100%;
            height: auto;
            border-radius: 1rem;
            padding: 1rem;
            padding-top: 5rem;
            margin-bottom: 3rem;
        }

        .my_section {
            height: 50rem;
        }

        body {
            overflow: visible !important;
        }
    }

    .account_my_title2 {
        float: left;
        width: auto !important;
        margin-top: 0.5rem;
        font-size: 1.5rem;
        border: 0 !important;
    }

    .account_my li {}

    .border_none {
        border: 0 !important;
    }

    .account_my {
        display: none;
    }

    .register_acct {
        cursor: pointer;
    }

    .my_section_wrapper {
        text-align: left;
        position: fixed;
        max-width: 60rem;
        max-height: 50rem;
        width: 90%;
        height: 80%;
        border-radius: 1rem;
        box-shadow: 0px 0px 21px -10px #000;
        padding: 1rem;
        padding-top: 5rem;
        right: 11rem;
        background-color: #fff;
    }
</style>
<script>
    var _bankList;
	<c:if test="${not empty bankList }">
		_bankList = ${bankList};
 	</c:if>
 
    $(document).ready(function () {
        $("#bank_cert_m_seq_no").on("change", function () {
            fnViewChange(this.value, _bankList);
        });

        $("#btnTest").on("click", function () {
            var bankInfo = fnGetJsonData($("#bank_cert_m_seq_no").val(), _bankList);
            btnPressedTestAccount(bankInfo);
        });

        $("#btnSave").on("click", function () {
            regAcctForJoinStep6();
        });

        $("#btnGoFaq").on("click", function () {
            location.href = "/pub/cs/faq?title=" + $("#bank_cert_m_seq_no option:selected").text() + "&page=1&faq_typ_cd=F07";
        });

        fnInit(_bankList);
    });


    $(function () {
        window.addEventListener("resize", function () {


        });

        function resizeStyle() {
            var winWidth = $(window).width();
            if (winWidth >= 720) {
                $(".add_pooup_close_step").click(function () {
                    $(".my_section").hide();

                    var acctCount = $("#acctListArea .clearfix").length;
                    if(acctCount > 0)
                        $(".account_my").show();
                    else {
                        $(".account_first").css("display", "flex")
                        $("body").css("overflow", "visible");
                    }
                });

            } else {
                $(".add_pooup_close_step").click(function () {
                    $(".my_section").hide();

                    var acctCount = $("#acctListArea .clearfix").length;
                    if(acctCount > 0)
                        $(".account_my").show();
                    else {
                        $(".account_first").css("display", "block")
                        $("body").css("overflow", "visible");
                    }
                });
            }
        }
        $(window).resize(function () {
            resizeStyle();
        });
        resizeStyle();
        $(".my_section").hide();
        $(".step_6_enroll").click(function () {
            $("#bank_cert_m_seq_no").val(18);
            fnViewChange($("#bank_cert_m_seq_no").val(), _bankList);

            $(".account_first").css("display", "none");
            $(".my_section").show();
            $("body").css("overflow", "hidden");
        });

        $(".edit_video_popup_container p img").click(function () {
            $(".edit_video_popup").hide();
            document.querySelectorAll(".edit_video_popup iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        });
        $(".edit_video_btn2 span").click(function () {
            $(".edit_video_popup").css("display", "flex")
        });
    });

    // 계좌 목록의 계좌등록 버튼
    var pressedBtnRegAcct = function() {
        // $("#crc option:eq(0)").prop("selected", true);
        $("#bank_cert_m_seq_no").val(18);
        fnViewChange($("#bank_cert_m_seq_no").val(), _bankList);

        $(".my_section").show();
        $(".account_my").hide();
    }
</script>
