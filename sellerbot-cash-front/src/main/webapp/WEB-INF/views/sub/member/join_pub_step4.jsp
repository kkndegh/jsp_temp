<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/assets/css/ticket.css">
<script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
<script type="module" src="/assets/pubMarket/js/index.js" defer></script>
<link rel="stylesheet" href="/assets/pubMarket/css/index.css">

<link rel="stylesheet" href="https://uicdn.toast.com/editor/latest/toastui-editor-viewer.css" />
<script src="https://uicdn.toast.com/editor/latest/toastui-editor-viewer.js"></script>

<input type="hidden" id="currentRegCount" value="0">
<input type="hidden" id="maxRegCount" value="0">

<style>
	#wrap\ container{
		display: grid;
		grid-template-columns: 1fr 3fr;
	}

	#wrap\ container > div.member_wra.long_member_wra.step_4{
		position: sticky;
		top: 0;
	}

	#wrap\ container > div.member_wra.long_member_wra.step_4 > div.left_title_area{
		float: none;
		width: 100%
	}

	#wrap\ container > div.btn_mb_group{
		position: fixed;
		bottom: 5rem;
		margin-bottom: 110px;
	}

	#wrap\ container > div #article_0{
		margin-top: 120px;
	}

	@media (max-width: 1024px){
		#wrap\ container > div.member_wra.long_member_wra.step_4 {
			overflow: hidden;
			height: auto;
			position: initial;
		}
	}

	@media (max-width: 960px){
		.left_title_area {
			width: 100%;
		}
	}

	@media (max-width: 720px){
		#wrap\ container{
			display: block;
		}
	}
</style>

<div id="wrap container">
	<div class="member_wra long_member_wra step_4">
		<div class="left_title_area">
			<div class="left_title_longArea">
				<div class="step_join_tab col3">
					<span class="active">04</span>
					<span>05</span>
					<span>06</span>
				</div>
				<div class="left_title_item">
					<div class="step_box">
						<div class="title_txt">판매몰을<br>등록해볼까요?</div>
						<p class="sub_title_txt">현재 판매하고 있는<br>쇼핑몰을 선택해주세요.</p>
					</div>
				</div>
				<!-- <div class="cs_center_info">
					<p class="cs-info-none">고객센터 : 1666-8216<br>
					s-cash@only1fs.com</p>
				</div> -->
			</div>
		</div>
	</div>
    <div class="btn_mb_group">
        <button class="btn_cicle step4_next">다음</button>
    </div>
	<button id="goTop">
		<i class="fa-solid fa-caret-up"></i>
		<p>TOP</p>
	</button>
	<div class="myModal">
		<div class="content">
			<div class="top">
				<p class="modalTitle">제목</p>
				<button id="title_closeModal"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
			</div>
			<button id="closeModal"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
			<div class="main">
				<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quidem quo distinctio esse laudantium
					vero
					dolores quasi, magnam tenetur ratione repellat delectus ipsam, recusandae vel soluta amet
					consectetur voluptatum? Veniam, adipisci?</p>
			</div>
			<div class="bottom">
			</div>
		</div>
	</div>
	<div>

	
	<article id="article_0">
		<p class="subTitle">등록한 판매몰</p>
		<span class="line"></span>
		<div class="listTopFlex">
			<div class="statusBtns">
				<button class="color_black on" id="ALL">전체<btn-num id="all_cnt">0</btn-num></button>
				<button class="color_blue" id="NR">정상<btn-num id="nr_cnt">0</btn-num></button>
				<button class="color_gray" id="PAU">수집중<btn-num id="">0</btn-num></button>
				<button class="color_silver" id="REG">대기<btn-num id="reg_cnt">0</btn-num></button>
				<button class="color_red" id="ERR">오류<btn-num id="err_cnt">0</btn-num></button>
				<button class="color_yellow" id="INS">점검<btn-num id="ins_cnt">0</btn-num></button>
				<button class="color_purple" id="OCM">오픈예정<btn-num id="ocm_cnt">0</btn-num></button>
			</div>
		</div>
		<div class="listHead">
			<dt>상태</dt>
			<dt>판매몰</dt>
			<dt>아이디</dt>
			<dt>부가정보</dt>
			<dt>제휴서비스</dt>
			<dt>TIP</dt>
			<dt>관리</dt>
		</div>
		<div class="listWrap scrollable">
			<ul></ul>
			<ol></ol>
		</div>
		<div class="listBottom">
			<button id="showListBtn"><b>전체보기</b></button>
		</div>
	</article>
	<article id="article_1">
		<p class="subTitle">지원 판매몰 리스트</p>
		<span class="line"></span>
		<div class="gridTopFlex">
			<div class="left">
				<input type="text" placeholder="판매몰 검색" class="market_box_find_text_area">
				<button id="searchText"><img src="/assets/pubMarket/img/magnifier.svg" alt="search"></button>
			</div>
			<div class="right">
				<button id="beOpenedBtn" class="black">오픈예정몰(사전등록)</button>
				<a id="guideBtn" href="https://www.sellerbot.co.kr/static/guide/KB_B/index.html " target="_blank">
					<button class="black">등록가이드</button>
				</a>
			</div>
		</div>
		<div id="top10" class="grayBox scrollable">
			<p class="gridTitle">TOP10</p>
			<div class="gridArea"></div>
		</div>
		<div id="allMalls" class="grayBox scrollable">
			<p class="gridTitle">이름순</p>
			<div class="gridArea">
				
			</div>
		</div>
	</article>
	<article id="article_2">
		<p class="subTitle">판매몰 등록/수정</p>
		<span class="line"></span>
		<div class="flexArea">
			<div class="left">
				<div class="contents scrollable">
					<h3 class="noDataText"> 등록할 판매몰을<br>[ 지원 판매몰 리스트 ] 에서 선택해주세요. </h3>
					<div class="modifyArea">

					</div>
					<div class="registerArea">
						
					</div>
				</div>
				<div class="btns">
					<button id="initialize">초기화</button>
					<button id="updateAll">한 번에 업데이트</button>
				</div>
			</div>
			<div class="right">
				<div class="topTitle">
					<p>판매몰 도움말이 없습니다.</p>
                    <button class="reg" style="display:none;">등록 방법</button>
				</div>
				<!-- <div class="contents scrollable" id="helpInfo"></div> -->
				<div class="contents scrollable" id="viewer"></div>
				<div class="btns">
					<button id="videoGuide">영상가이드</button>
					<button id="showDetail">상세보기</button>
				</div>
			</div>
		</div>
	</article>
</div>
	<div class="bottomBar">
		<div class="closeBtnBox">
			<button id="closeBottomBar"><img src="/assets/pubMarket/img/xmark.svg" alt="xmark"></button>
		</div>
		<div class="centerBox">
			<p>현재 선택한 판매몰은 <b id="total">0</b>건 입니다. (<text-highlight>등록 <b id="reg">0</b>건 / 수정 <b id="mod">0</b>건</text-highlight>)<br>아래 판매몰 등록/수정에서 입력을 완료해주세요.</p>
			<div class="btns">
				<button id="hideBottomBar">닫기</button>
				<button id="goto">바로가기</button>
			</div>
		</div>
	</div>
</div>


<div class="new_popup_wrap">
    <div class="display_none_back"></div>
    <div class="pop_up_1 pop_up_all">
        <div class="popup_top_title">
            <h1>인증절차 필요 - 네이버페이</h1>
            <div class="add_pooup_close">
                <div></div>
                <div></div>
            </div>
        </div>
        <h1 class="popup_title_2">네이버페이 업데이트를 위해선,</br>
            아래의 이메일 인증 절차가 필요해요~</br>
            <a style="color: #2DABE8;">인증 이메일 : <span id="user_mail"></span></a>
        </h1>
        <div class="pop_up_text_wrap" style="height:15rem;">
            <span class="naver_text">
                <p>&lt;인증 절차&gt;</p>
                <p>1. 아래 업데이트 클릭</p>
                <p>2. <a style="color: #2DABE8;">인증 이메일</a>로 로그인 후 받은 메일함 확인</p>
                <p>3. 네이버페이에서 발송된 인증용 메일 클릭</p>
                <p>4. ‘인증하기’ 클릭</p>
                <p>(업데이트 클릭 후 3분 안에 메일 ‘인증하기‘ 해주셔야 해요~)</p>
            </span>
        </div>
        <div class="button_row">
            <button type="button" class="close">닫기</button>
            <button type="button" class="chg_ticket" data-cust_mall_seq_no="">업데이트</button>
        </div>
    </div>
</div>

<script>
	new toastui.Editor({
		el: document.querySelector('#viewer'),
		initialValue: ""
	});
</script>