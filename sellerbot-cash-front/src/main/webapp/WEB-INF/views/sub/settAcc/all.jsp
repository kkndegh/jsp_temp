<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<style>
	.chart_wrap_top {
		margin-bottom: 3em;
		position: relative;
	}

	.tip_ {
		display: inline-block;
		width: 30px;
		padding: 0px 7px;
		margin-right: 10px;

		position: relative;

	}

	.line_ {
		height: 100%;
		width: 60px;

		position: absolute;
		display: inline-block;
		border-right: 1px solid #333;
	}

	.tip_ label {
		background: #19cddd;
		padding: 2px 5px;
		border-radius: 7px;
		color: #fff;
		font-size: 0.9em;
	}

	.tip_ span {
		display: block;
		position: absolute;
		right: -17px;
		top: 0px;
		color: #fff;
		font-size: 0.4em;
		background: red;
		border-radius: 6px;
		width: 20px;
		height: 12px;
		line-height: 12px;
		text-align: center;

	}

	.custom_chart_text label {
		display: inline-block;
		min-width: 85px;
	}

	.tooltip_wrap {
		min-height: 98px;
	}

	.custom_chart_des4 {
		position: absolute;
		right: 10px;
		bottom: 5px;
	}

	.custom_bar_chart {
		display: inline-block;
		width: 46%;
	}

	#chart_1 {
		text-align: right;
	}

	#chart_2 {
		float: right;
	}

	@media (max-width:5000px) and (min-width:960px) {
		#chartjs-tooltip {
			position: relative;
			width: 14rem;
		}

		#chartjs-tooltip img {
			position: absolute;
			top: -0.7rem;
			right: -0.5rem;
		}

		.tooltip_item_1 {
			font-size: 1.2em;
			padding-top: 0rem;
			padding-left: 0.3rem;
			color: #333333;
			font-weight: bold;
		}

		.tooltip_item_2 {
			padding-top: 0.3em;
			font-size: 1.2em;
			color: #888888;
			margin-left: 0.3rem;
		}

		.tooltip_item_3 {
			font-size: 1.4em;
			margin-left: 0.3rem;
			margin-top: 0.5rem;
			display: inline-block;
			text-align: left;
			width: 48%;
		}

		.tooltip_item_4 {
			font-size: 1.4em;
			margin-top: 0.5rem;
			display: inline-block;
			text-align: right;
			width: 49%;
		}
	}

	@media (max-width:959px) {
		#chartjs-tooltip table {
			position: relative;
		}

		#mobile_for_tool_2 ul,
		#mobile_for_tool_1 ul {
			width: 100%;
			background-color: #fff;
			border-radius: 1rem;
			padding: 1rem;
			margin-top: 2rem;
			box-shadow: 1px 1px 5px #aaa;
			position: relative;
		}

		#mobile_for_tool_2 ul:last-child,
		#mobile_for_tool_1 ul:last-child {
			margin-bottom: 2rem;
		}

		#chartjs-tooltip {
			position: relative;
			width: 17rem;
			height: 6.3rem;
			position: static !important;
			margin: 0 auto;
			width: 100%;
			margin: 1rem 0;
			opacity: 1 !important;
		}

		#mobile_for_tool_2 img,
		#mobile_for_tool_1 img {
			position: absolute;
			top: -1rem;
			right: 0rem;
		}

		#chartjs-tooltip img {
			position: absolute;
			top: -1.5rem;
			right: -1.5rem;
		}

		.tooltip_item_1 {
			font-size: 1.2em;
			padding-top: 0.5rem;
			padding-left: 0.3rem;
			color: #333333;
			font-weight: bold;
		}

		.tooltip_item_2 {
			padding-top: 0.3em;
			font-size: 1.2em;
			color: #888888;
			margin-left: 0.3rem;
		}

		.tooltip_item_3 {
			font-size: 1.4em;
			margin-left: 0.3rem;
			margin-top: 0.5rem;
			display: inline-block;
			width: 48%;
		}

		.tooltip_item_4 {
			font-size: 1.4em;
			margin-top: 0.5rem;
			display: inline-block;
			width: 48%;
			text-align: right;
		}
	}

	@media (max-width:1370px) and (min-width:1000px) {
		#chartjs-tooltip {
			width: 18rem;
		}
	}
</style>

<div class="container">
	<div class="payment_wra">
		<div class="layout_payment">
			<!-- 상단 메뉴 Start -->
			<div class="menu_top">
				<p class="menu_name">정산예정금 확인</p>
				<div class="gnb_topBox">
					<ul class="gnb_top clearfix">
						<li class="focus"><a href="/sub/settAcc/all">정산예정금 한 눈에 보기</a></li>
						<li><a href="/sub/settAcc/detail">정산예정금 상세보기</a></li>
						<li><a href="/sub/settAcc/calendar">정산예정금 달력</a></li>
					</ul>
				</div>
			</div>
			<!-- 상단 메뉴 End -->
			<!-- 정산예정금 현황 Start -->
			<div class="area_all_payment pb1">
				<div class="calendar_guide">GUIDE</div>
				<!-- 가이드 문구 팝업 Start -->
				<div class="calendar_guide_pop">
					<div class="calendar_guide_pop_header">
						<div class="calendar_guide_pop_shp">GUIDE</div>
						<p>도움말</p>
						<div class="calendar_guide_pop_close">
							<div></div>
							<div></div>
						</div>
					</div>
					<ul class="calendar_guide_pop_con">
						<li class="calendar_guide_pop_con_title">배송상태 기준<br />정산예정금
						</li>
						<li>정산예정금 오픈마켓에서는 정산예정금을 배송상태별로 구분하며 구매 확정 후 1~2 영업일 내에 정산금이 지급됩니다.</li>
					</ul>
					<ul class="calendar_guide_pop_con calendar_guide_pop_con_last">
						<li class="calendar_guide_pop_con_title">정산일자 기준<br />정산예정금
						</li>
						<li>비오픈마켓에서는 정산예정금을 정산일자별로 구분하며 지정된 정산일자에 정산금이 지급됩니다.</li>
					</ul>
				</div>
				<!-- 가이드 문구 팝업 End -->
				<div class="status_box">
					<span class="all">전체 ${mallInfo.all_mall_cnt}</span> /
					<span class="normal">정상 ${mallInfo.nor_mall_cnt}</span> /
					<span class="warning">점검 ${mallInfo.ins_mall_cnt}</span> /
					<span class="error">오류 ${mallInfo.err_mall_cnt}</span>
				</div>
				<div class="update_info">
					<a href="javascript:return false;" style="cursor:default;"><img
							src="/assets/images/payment/refresh.png" alt=""></a>
					<span>마지막 업데이트일자 ${settAccSche.scra_ts }</span>
				</div>
				<div class="calc_payment_box">
					<div class="item_calc">
						<p class="lb"><b>오픈마켓</b> (배송상태 기준)</p>
						<p class="num"><b>
								<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum}" pattern="#,###" /></b>원
						</p>
					</div>
					<div class="icon">+</div>
					<div class="item_calc">
						<p class="lb"><b>비오픈마켓</b> (정산일자 기준)</p>
						<p class="num"><b>
								<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum}" pattern="#,###" /></b>원
						</p>
					</div>
					<div class="icon icon_2">=</div>
					<div class="item_calc total">
						<p class="lb">정산예정금 합계</p>
						<p class="num"><b>
								<fmt:formatNumber value="${settAccSche.sett_acc_pln_sum}" pattern="#,###" /></b>원</p>
						<div onclick="location.href='/sub/pre_calculate/pre_calculate'" class="btn_tip"></div>
					</div>
				</div>
			</div>
			<!-- 정산예정금 현황 End -->
			<!-- 가장 가까운/먼 정산일 Start -->
			<c:if test="${not empty tipFirstVo || not empty tipLastVo}">
				<div class="pay_day_preview clearfix">
					<c:if test="${fn:length(custSettAccScheTipVo.mall_tip) > 0}">
						<div onclick="location.href='/sub/settAcc/calendar'" class="pay_day_preview_tipbox">
							TIP
							<div class="pay_day_preview_tipbox_num">
								${tipCount }
							</div>
						</div>
					</c:if>
					<c:choose>
						<c:when test="${not empty tipFirstVo }">
							<fmt:parseDate value="${tipFirstVo.sett_acc_pln_dt}" var="sett_acc_pln_dt"
								pattern="yyyyMMdd" />
							<div class="item_payday">
								<p class="txt_payday">가장 <span>가까운</span> 정산일</p>
								<p class="txt_payday">
									<fmt:formatDate value="${sett_acc_pln_dt}" pattern="yyyy-MM-dd" />
								</p>
								<p class="txt_payday">${tipFirstVo.mall_cd_nm}</p>
								<p class="txt_payday money"><b>
										<fmt:formatNumber value="${tipFirstVo.sett_acc_pln}" pattern="#,###" /></b>원</p>
							</div>
						</c:when>
						<c:otherwise>
							<div class="item_payday">
								<p class="txt_payday">가장 <span>가까운</span> 정산예정금 데이터가 없습니다.</p>
							</div>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${not empty tipLastVo }">
							<fmt:parseDate value="${tipLastVo.sett_acc_pln_dt}" var="sett_acc_pln_dt"
								pattern="yyyyMMdd" />
							<div class="item_payday">
								<p class="txt_payday">가장 <span class="color2">먼</span> 정산일</p>
								<p class="txt_payday">
									<fmt:formatDate value="${sett_acc_pln_dt}" pattern="yyyy-MM-dd" />
								</p>
								<p class="txt_payday">${tipLastVo.mall_cd_nm}</p>
								<p class="txt_payday money"><b>
										<fmt:formatNumber value="${tipLastVo.sett_acc_pln}" pattern="#,###" /></b>원</p>
							</div>
						</c:when>
						<c:otherwise>
							<div class="item_payday">
								<p class="txt_payday">가장 <span>먼</span> 정산예정금 데이터가 없습니다.</p>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</c:if>
			<!-- 가장 가까운/먼 정산일 End -->
			<div class="payment_all_contents_wrap">
				<!-- 오픈마켓 그래프 Start -->
				<div class="payment_all_contents_left">
					<div class="am_chart_title">
						<h1 class="am_chart_title_color">오픈마켓</h1>
						<span>판매몰별 비중</span>
					</div>
					<div class="am_chart_wrap">
						<div id="left_chart">
							<div class="payment_nodata" style="display:none;">
								<img src="/assets/images/common/icon_nodata.png" alt="" />
								<p>데이터가 없습니다.</p>
							</div>
						</div>
					</div>
					<!-- 오픈마켓 정산예정금 Start -->
					<div class="layout_col2 layout_col2_pc">
						<div class="data_section">
							<div class="title">
								<img src="/assets/images/payment/openMarket_Icon.png" alt="">
								<p class="mall_type">오픈마켓</p>
								<span class="percent">
									<c:choose>
										<c:when test="${settAccSche.sett_acc_pln_open_sum_rto eq 0 }">
											0%
										</c:when>
										<c:otherwise>
											<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum_rto }"
												pattern="0.00" />%
										</c:otherwise>
									</c:choose>
								</span>
								<p class="price">
									<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum}" pattern="#,###" />원
								</p>
							</div>
							<c:choose>
								<c:when test="${fn:length(settAccSche.sett_acc_pln_open_market_l) == 0}">
									<div class="payment_nodata">
										<img src="/assets/images/common/icon_nodata.png" alt="" />
										<p>데이터가 없습니다.</p>
									</div>
								</c:when>
								<c:otherwise>
									<ul class="data_mall">
										<c:forEach items="${settAccSche.sett_acc_pln_open_market_l }" var="market1"
											varStatus="status">
											<c:if test="${market1.sett_acc_pln != 0}">
												<li class="data-li">
													<p class="name_m">${market1.mall_nm }</p>
													<p class="price"><b>
															<fmt:formatNumber value="${market1.sett_acc_pln}"
																pattern="#,###" /></b>원</p>
												</li>
											</c:if>
										</c:forEach>
									</ul>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<!-- 오픈마켓 정산예정금 End -->
				</div>
				<!-- 오픈마켓 그래프 End -->
				<!-- 비오픈마켓 그래프 Start -->
				<div class="payment_all_contents_right">
					<div class="am_chart_title">
						<h1>비오픈마켓</h1>
						<span>판매몰별 비중</span>
					</div>
					<div class="am_chart_wrap">
						<div id="left_chart_2">
							<div class="payment_nodata" style="display: none">
								<img src="/assets/images/common/icon_nodata.png" alt="" />
								<p>데이터가 없습니다.</p>
							</div>
						</div>
					</div>
					<!-- 비오픈마켓 정산예정금 Start -->
					<div class="layout_col2 layout_col2_pc">
						<div class="data_section data_section_tooltips">
							<div class="title tooltip_after">
								<img src="/assets/images/payment/noneOpenMarket_Icon.png" alt="">
								<p class="mall_type">비오픈마켓</p> <span class="percent percent2">
									<c:choose>
										<c:when test="${settAccSche.sett_acc_pln_nopen_sum_rto eq 0 }">
											0%
										</c:when>
										<c:otherwise>
											<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum_rto }"
												pattern="0.00" />%
										</c:otherwise>
									</c:choose>
								</span>
								<p class="price">
									<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum}" pattern="#,###" />원
								</p>
							</div>
							<c:choose>
								<c:when test="${settAccSche.sett_acc_pln_nopen_sum == 0}">
									<div class="payment_nodata">
										<img src="/assets/images/common/icon_nodata.png" alt="" />
										<p>데이터가 없습니다.</p>
									</div>
								</c:when>
								<c:otherwise>
									<ul class="data_mall">
										<c:forEach items="${settAccSche.sett_acc_pln_nopen_market_l }" var="market2"
											varStatus="status">
											<c:if test="${market2.sett_acc_pln!=0 }">
												<li class="data-li">
													<div class="item_list">
														<p class="name_m">${market2.mall_nm }</p>
														<p class="price"><b>
																<fmt:formatNumber value="${market2.sett_acc_pln}"
																	pattern="#,###" /></b>원</p>
													</div>
												</li>
											</c:if>
										</c:forEach>
									</ul>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<!-- 비오픈마켓 정산예정금 End -->
				</div>
				<!-- 비오픈마켓 그래프 End -->
				<!-- 모바일용 오픈 마켓  -->
				<div class="layout_col2 layout_col2_m">
					<div class="data_section">
						<div class="title">
							<img src="/assets/images/payment/openMarket_Icon.png" alt="">
							<p class="mall_type">오픈마켓</p>
							<span class="percent">
								<c:choose>
									<c:when test="${settAccSche.sett_acc_pln_open_sum_rto eq 0 }">
										0%
									</c:when>
									<c:otherwise>
										<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum_rto }"
											pattern="0.00" />%
									</c:otherwise>
								</c:choose>
							</span>
							<p class="price">
								<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum}" pattern="#,###" />원</p>
						</div>
						<c:choose>
							<c:when test="${fn:length(settAccSche.sett_acc_pln_open_market_l) == 0}">
								<div class="payment_nodata">
									<img src="/assets/images/common/icon_nodata.png" alt="" />
									<p>데이터가 없습니다.</p>
								</div>
							</c:when>
							<c:otherwise>
								<ul class="data_mall">
									<c:forEach items="${settAccSche.sett_acc_pln_open_market_l }" var="market1"
										varStatus="status">
										<li class="data-li">
											<p class="name_m">${market1.mall_nm }</p>
											<!-- <p class="id_m"></p> -->
											<p class="price"><b>
													<fmt:formatNumber value="${market1.sett_acc_pln}" pattern="#,###" />
													</b>원</p>
										</li>
									</c:forEach>
								</ul>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<!-- 모바일용 오픈 마켓  종료 -->
				<div class="payment_all_contents_bottom">
					<!-- 배송상태별 정산예정금 Start -->
					<div class="payment_all_contents_left">
						<div class="layout_col2">
							<div class="data_section">
								<div id="m_tolltipBox_fir"></div>
								<div class="title">
									<img src="/assets/images/payment/delievery_Icon.png" alt="">
									<p class="">배송상태별 정산예정금 합계</p>
									<p class="price" id="opmSelectTotal">
										<fmt:formatNumber value="${settAccScheDlv.dlv_sett_acc_pln_sum}"
											pattern="#,###" />원</p>
								</div>
							</div>
							<div class="payment_total">
								<c:choose>
									<c:when test="${settAccScheDlv.dlv_sett_acc_pln_sum > 0}">
										<select class="sctbox_lg sctbox_lg_1" onchange="allChange()">
											<option value="all">전체</option>
											<c:forEach items="${settAccSche.sett_acc_pln_open_market_l }" var="market1"
												varStatus="status">
												<c:if test="${market1.sett_acc_pln > 0 }">
													<option value="${market1.mall_cd }">${market1.mall_nm }</option>
												</c:if>
											</c:forEach>
										</select>
										<ul class="list_pay list_pay_left" data-total1="all">
											<c:forEach items="${settAccScheDlv.dlv }" var="dlv" varStatus="status">
												<c:choose>
													<c:when test="${dlv.dvl_sts_cd eq 'RDY' }">
														<li>
															<div class="item_list clearfix">
																<p class="col col1">발송대상</p>
																<p class="col col2">${dlv.dvl_num}건</p>
																<p class="col col3">
																	<fmt:formatNumber value="${dlv.dvl_sett_acc_pln}"
																		pattern="#,###" />원</p>
															</div>
														</li>
													</c:when>
													<c:when test="${dlv.dvl_sts_cd eq 'DOI' }">
														<li>
															<div class="item_list clearfix">
																<p class="col col1">배송중</p>
																<p class="col col2">${dlv.dvl_num}건</p>
																<p class="col col3">
																	<fmt:formatNumber value="${dlv.dvl_sett_acc_pln}"
																		pattern="#,###" />원</p>
															</div>
														</li>
													</c:when>
													<c:when test="${dlv.dvl_sts_cd eq 'DON' }">
														<li>
															<div class="item_list clearfix">
																<p class="col col1">배송완료</p>
																<p class="col col2">${dlv.dvl_num}건</p>
																<p class="col col3">
																	<fmt:formatNumber value="${dlv.dvl_sett_acc_pln}"
																		pattern="#,###" />원</p>
															</div>
														</li>
													</c:when>
													<c:when test="${dlv.dvl_sts_cd eq 'CON' }">
														<li>
															<div class="item_list clearfix">
																<p class="col col1">구매확정</p>
																<p class="col col2">${dlv.dvl_num}건</p>
																<p class="col col3">
																	<fmt:formatNumber value="${dlv.dvl_sett_acc_pln}"
																		pattern="#,###" />원</p>
															</div>
														</li>
													</c:when>
													<c:otherwise>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</ul>
									</c:when>
									<c:otherwise>
										<div class="payment_nodata">
											<img src="/assets/images/common/icon_nodata.png" alt="" />
											<p>데이터가 없습니다.</p>
										</div>
									</c:otherwise>
								</c:choose>
								<c:forEach items="${settAccSche.sett_acc_pln_open_market_l }" var="market1"
									varStatus="status">
									<div class="new_list_pay_detail new_list_pay_detail_1 mallCd${market1.mall_cd}">
										<c:forEach items="${settAccScheDlvMall.mall }" varStatus="status" var="mall">
											<c:if
												test="${market1.mall_cd eq mall.mall_cd and mall.mall_dvl_sett_acc_pln_sum > 0}">
												<div class="new_list_pay_detail_1_box">
													<div class="new_list_pay_detail_1_box_top">
														<div class="new_list_pay_detail_1_box_top_left">
															<h1>${mall.mall_nm }</h1>
															<span>${mall.mall_cert_1st_id }</span>
														</div>
														<div class="new_list_pay_detail_1_box_top_right">
															<h1>
																<fmt:formatNumber value="${mall.mall_dvl_num_sum }"
																	pattern="#,###" />건</h1>
															<h2>
																<fmt:formatNumber
																	value="${mall.mall_dvl_sett_acc_pln_sum }"
																	pattern="#,###" />원</h2>
														</div>
													</div>
													<div class="new_list_pay_detail_1_box_bottom">
														<div class="new_list_pay_detail_1_box_bottom_left">
															<img src="/imagefile/${mall.stor_path}${mall.stor_file_nm}" alt="몰 로고">
														</div>
														<div class="new_list_pay_detail_1_box_bottom_right_wrap">
															<ul class="new_list_pay_detail_1_box_bottom_right">
																<li>발송대상</li>
																<c:forEach var="detail"
																	items="${mall.sett_acc_pln_open_dlv_l}">
																	<c:if test="${detail.dvl_sts_cd eq 'RDY' }">
																		<li>
																			<fmt:formatNumber value="${detail.dvl_num }"
																				pattern="#,###" />건</li>
																		<li>
																			<fmt:formatNumber
																				value="${detail.dvl_sett_acc_pln }"
																				pattern="#,###" />원</li>
																	</c:if>
																</c:forEach>
															</ul>
															<ul class="new_list_pay_detail_1_box_bottom_right">
																<li>배송중</li>
																<c:forEach var="detail"
																	items="${mall.sett_acc_pln_open_dlv_l}">
																	<c:if test="${detail.dvl_sts_cd eq 'DOI' }">
																		<li>
																			<fmt:formatNumber value="${detail.dvl_num }"
																				pattern="#,###" />건</li>
																		<li>
																			<fmt:formatNumber
																				value="${detail.dvl_sett_acc_pln }"
																				pattern="#,###" />원</li>
																	</c:if>
																</c:forEach>
															</ul>
															<ul class="new_list_pay_detail_1_box_bottom_right">
																<li>배송완료</li>
																<c:forEach var="detail"
																	items="${mall.sett_acc_pln_open_dlv_l}">
																	<c:if test="${detail.dvl_sts_cd eq 'DON' }">
																		<li>
																			<fmt:formatNumber value="${detail.dvl_num }"
																				pattern="#,###" />건</li>
																		<li>
																			<fmt:formatNumber
																				value="${detail.dvl_sett_acc_pln }"
																				pattern="#,###" />원</li>
																	</c:if>
																</c:forEach>
															</ul>
															<ul class="new_list_pay_detail_1_box_bottom_right">
																<li>구매확정</li>
																<c:forEach var="detail"
																	items="${mall.sett_acc_pln_open_dlv_l}">
																	<c:if test="${detail.dvl_sts_cd eq 'CON' }">
																		<li>
																			<fmt:formatNumber value="${detail.dvl_num }"
																				pattern="#,###" />건</li>
																		<li>
																			<fmt:formatNumber
																				value="${detail.dvl_sett_acc_pln }"
																				pattern="#,###" />원</li>
																	</c:if>
																</c:forEach>
															</ul>
														</div>
													</div>
												</div>
											</c:if>
										</c:forEach>
									</div>
								</c:forEach>
							</div>
						</div>
					</div>
					<!-- 배송상태별 정산예정금 Start -->
					<!-- 모바일용 비오픈 마켓 -->
					<div class="layout_col2 layout_col2_m">
						<div class="data_section data_section_tooltips">
							<div class="title tooltip_after">
								<img src="/assets/images/payment/noneOpenMarket_Icon.png" alt="">
								<p class="mall_type">비오픈마켓</p> <span class="percent percent2">
									<c:choose>
										<c:when test="${settAccSche.sett_acc_pln_nopen_sum_rto eq 0 }">
											0%
										</c:when>
										<c:otherwise>
											<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum_rto }"
												pattern="0.00" />%
										</c:otherwise>
									</c:choose>
								</span>
								<p class="price">
									<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum}" pattern="#,###" />원
								</p>
							</div>
							<c:choose>
								<c:when test="${settAccSche.sett_acc_pln_nopen_sum == 0}">
									<div class="payment_nodata">
										<img src="/assets/images/common/icon_nodata.png" alt="" />
										<p>데이터가 없습니다.</p>
									</div>
								</c:when>
								<c:otherwise>
									<ul class="data_mall">
										<c:forEach items="${settAccSche.sett_acc_pln_nopen_market_l }" var="market2"
											varStatus="status">
											<c:if test="${market2.sett_acc_pln!=0 }">
												<li class="data-li">
													<div class="item_list">
														<p class="name_m">${market2.mall_nm }</p>
														<!-- <p class="id_m"></p> -->
														<p class="price"><b>
																<fmt:formatNumber value="${market2.sett_acc_pln}"
																	pattern="#,###" /></b>원</p>
													</div>
												</li>
											</c:if>
										</c:forEach>
									</ul>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<!-- 모바일용 비오픈 마켓 종료 -->
					<!-- 월별 정산예정금 Start -->
					<div class="payment_all_contents_right">
						<div class="layout_col2">
							<div class="data_section">
								<div class="title">
									<img src="/assets/images/payment/calendar_Icon.png" alt="">
									<p class="">월별 정산예정금 합계</p>
									<p class="price" id="nOpmSelectTotal">
										<fmt:formatNumber value="${settAccScheMon.mon_sett_acc_pln_sum}"
											pattern="#,###" />원</p>
								</div>
							</div>
							<div class="payment_total">
								<c:choose>
									<c:when test="${settAccScheMon.mon_sett_acc_pln_sum > 0 }">
										<select class="sctbox_lg sctbox_lg_2" onchange="allChange2()">
											<option value="all">전체</option>
											<c:forEach items="${settAccSche.sett_acc_pln_nopen_market_l}"
												varStatus="status" var="mallName">
												<c:if test="${mallName.sett_acc_pln != 0 }">
													<option value="${mallName.mall_cd }">${mallName.mall_nm}</option>
												</c:if>
											</c:forEach>
										</select>
										<ul class="list_pay list_pay_month list_pay_right" data-total2="all">
											<c:forEach items="${settAccScheMon.mon_sett_acc_pln_l }" var="pln"
												varStatus="status">
												<li>
													<div class="item_list clearfix">
														<p class="col col1">${fn:substring(pln.sett_acc_pln_dt_mon,0,4)}-${fn:substring(pln.sett_acc_pln_dt_mon,4,6)}월</p>
														<p class="col col2">
															<fmt:formatNumber value="${pln.mon_sett_acc_pln}"
																pattern="#,###" />원</p>
													</div>
												</li>
											</c:forEach>
										</ul>
									</c:when>
									<c:otherwise>
										<div class="payment_nodata">
											<img src="/assets/images/common/icon_nodata.png" alt="" />
											<p>데이터가 없습니다.</p>
										</div>
									</c:otherwise>
								</c:choose>
								<c:forEach items="${settAccScheDayMall.dayMallNameList}" varStatus="status" var="mallName">
									<div
									class="new_list_pay_detail new_list_pay_detail_2 mallCd${settAccScheDayMall.dayMallCdList[status.index]}">
									<div class="new_list_pay_detail_top">
										<h1>${mallName }</h1>
										<p class="new_list_pay_pri mallTotalPrice${settAccScheDayMall.dayMallCdList[status.index]}">
											76,989,983원</p>
										</div>
										<div class="new_list_pay_detail_middle">
											<img src="/imagefile/${settAccScheDayMall.dayMallLogoList[status.index]}" alt="몰 로고">
											<ul class="new_list_pay_detail_middle_con">
												<c:forEach items="${settAccScheDayMall.sett_acc_pln_mall_day_l}"
													varStatus="idx" var="daymall">
													<c:if
														test="${settAccScheDayMall.dayMallCdList[status.index] eq daymall.mall_cd and daymall.day_sett_acc_pln != 0}">
														<li>
															<h1>${daymall.day}
															</h1>
															<h2>${daymall.mall_cd_nm}</h2>
															<h3>(${daymall.mall_cert_1st_id })</h3>
															<span>
																<fmt:formatNumber value="${daymall.day_sett_acc_pln}"
																	pattern="#,###" />원</span>
															<input type="hidden"
																class="day_sett_acc_pln${daymall.mall_cd}"
																value="${daymall.day_sett_acc_pln}" />
														</li>
													</c:if>
												</c:forEach>
											</ul>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
					</div>
					<!-- 월별 정산예정금 End -->
				</div>
			</div>
		</div>
		<%@include file="../../widgets/banner.jsp" %>
	</div>
</div>
<script src="/assets/amchart/core.js"></script>
<script src="/assets/amchart/charts.js"></script>
<script src="/assets/amchart/animated.js"></script>
<script>
	//오픈마켓 몰 코드에 대한 정산예정금 MAP
	var opmSettMap = {};
	//비오픈마켓 몰 코드에 대한 정산예정금 MAP
	var nOpmSettMap = {};

	$(document).ready(function () {

		$(".calendar_guide").click(function () {
			$(".calendar_guide_pop").addClass("calendar_guide_pop_active");
		});
		$(".calendar_guide_pop_close").click(function () {
			$(".calendar_guide_pop").removeClass("calendar_guide_pop_active");
		});

		//비오픈마켓 합계 스크립트
    	var cdList = new Array();
    	    	
	  	<c:forEach items="${settAccScheDayMall.dayMallCdList}" varStatus="status" var="cdList">
	  		cdList.push('${cdList}');
	  	</c:forEach>
	  	  
	  	for (var i = 0; i < cdList.length; i++) {
			var total = 0;
			$(".day_sett_acc_pln"+cdList[i]).each(function(p,i){
				total += parseInt($(this).val());
			});
	  		nOpmSettMap[cdList[i]] = total; 
	  		$(".mallTotalPrice"+cdList[i]).html(comma(total)+"원");
		}
	  	  
		$(".status_box").css("cursor","pointer");
		$(".status_box").click(function(){
			location.href="/sub/my/market";
		});
	  	
	  	<c:forEach items="${settAccSche.sett_acc_pln_open_market_l}" varStatus="status" var="opmCdList">
	  		opmSettMap["${opmCdList.mall_cd}"] = ${opmCdList.sett_acc_pln};
		</c:forEach>
	  	
    	var chart1Array=[];
    	var chart2Array=[];
		<c:forEach var="opmData" items="${settAccSche.sett_acc_pln_open_market_l}">
			<c:if test="${opmData.mall_cd eq '003'}">
				var opmCustomColor="#FFE3EE";
			</c:if>
			<c:if test="${opmData.mall_cd eq '001'}">
				var opmCustomColor="#d2ffd2";
			</c:if>
			<c:if test="${opmData.mall_cd eq '002'}">
				var opmCustomColor="#F5AF64";
			</c:if>
			<c:if test="${opmData.mall_cd eq '005'}">
				var opmCustomColor="#FF9DFF";
			</c:if>
			<c:if test="${opmData.mall_cd eq '008'}">
				var opmCustomColor="#CBFF75";
			</c:if>
			<c:if test="${opmData.mall_cd eq '046'}">
				var opmCustomColor="#82F9B7";
			</c:if>
			<c:if test="${opmData.mall_cd eq '051'}">
				var opmCustomColor="#FFEB46";
			</c:if>
			<c:if test="${opmData.sett_acc_pln > 0}">
				chart1Array.push({"오픈마켓":"${opmData.mall_nm}","litres":${opmData.sett_acc_pln},"color":opmCustomColor});
			</c:if>
		</c:forEach>
		<c:forEach var="nOpmData" items="${settAccSche.sett_acc_pln_nopen_market_l}">
			<c:if test="${nOpmData.sett_acc_pln > 0}">
				chart2Array.push({"비오픈마켓":"${nOpmData.mall_nm}","litres":${nOpmData.sett_acc_pln}});
			</c:if>
		</c:forEach>
		
   		am4core.useTheme(am4themes_animated);
		// 챠트 상용화 적용
		am4core.options.commercialLicense = true;
		if (chart1Array.length > 0) {
			var chart = am4core.create("left_chart", am4charts.PieChart);
			chart.data = chart1Array;
			var pieSeries = chart.series.push(new am4charts.PieSeries());
			pieSeries.dataFields.value = "litres";
			pieSeries.dataFields.category = "오픈마켓";
			pieSeries.slices.template.propertyFields.fill = "color";
			pieSeries.slices.template.stroke = am4core.color("#fff");
			pieSeries.slices.template.strokeWidth = 2;
			pieSeries.slices.template.strokeOpacity = 1;
			// This creates initial animation
			pieSeries.hiddenState.properties.opacity = 1;
			pieSeries.hiddenState.properties.endAngle = -90;
			pieSeries.hiddenState.properties.startAngle = -90;
			pieSeries.fontSize = 10;
			// 차트 반응형
			chart.responsive.enabled = false;
		} else {
			fnNoDataChart("left_chart");
		}
		if (chart2Array.length > 0) {
			var chart2 = am4core.create("left_chart_2", am4charts.PieChart);
			chart2.data = chart2Array;
			var pieSeries2 = chart2.series.push(new am4charts.PieSeries());

			pieSeries2.dataFields.value = "litres";
			pieSeries2.dataFields.category = "비오픈마켓";
			pieSeries2.slices.template.stroke = am4core.color("#fff");
			pieSeries2.slices.template.strokeWidth = 2;
			pieSeries2.slices.template.strokeOpacity = 1;
			// This creates initial animation
			pieSeries2.hiddenState.properties.opacity = 1;
			pieSeries2.hiddenState.properties.endAngle = -90;
			pieSeries2.hiddenState.properties.startAngle = -90;
			pieSeries2.fontSize = 10;

			// 차트 반응형
			chart2.responsive.enabled = false;
		} else {
			fnNoDataChart("left_chart_2");
		}
	});

	function comma(num) {
		var len, point, str;
		num = num + "";
		point = num.length % 3;
		len = num.length;

		str = num.substring(0, point);
		while (point < len) {
			if (str != "") str += ",";
			str += num.substring(point, point + 3);
			point += 3;
		}
		return str;
	}

	//select//
	function allChange() {
		var nowSel = $(".sctbox_lg_1 option:selected").val();
		if (nowSel == "all") {
			$(".list_pay_left").css("display", "block");
			$(".new_list_pay_detail_1").css("display", "none");

			var keys = Object.keys(opmSettMap);
			var totalSett = 0;
			for (var i = 0; i < keys.length; i++) {
				totalSett += opmSettMap[keys[i]];
			}
			$("#opmSelectTotal").html(comma(totalSett) + "원");
		} else {
			$("#opmSelectTotal").html(comma(opmSettMap[nowSel]) + "원");
			$(".new_list_pay_detail_1").hide();
			$(".new_list_pay_detail_1").each(function () {
				if ($(this).hasClass("mallCd" + nowSel)) {
					$(this).show();
				}
			});
			$(".list_pay_left").css("display", "none");
		}
	}

	function allChange2() {
		var nowSel = $(".sctbox_lg_2 option:selected").val();
		if (nowSel == "all") {
			$(".list_pay_right").css("display", "block");
			$(".new_list_pay_detail_2").css("display", "none");
			var keys = Object.keys(nOpmSettMap);
			var totalSett = 0;
			for (var i = 0; i < keys.length; i++) {
				totalSett += nOpmSettMap[keys[i]];
			}
			$("#nOpmSelectTotal").html(comma(totalSett) + "원");
			console.log(comma(totalSett) + "원");
		} else {
			$("#nOpmSelectTotal").html(comma(nOpmSettMap[nowSel]) + "원");
			$(".new_list_pay_detail_2").hide();
			$(".new_list_pay_detail_2").each(function () {
				if ($(this).hasClass("mallCd" + nowSel)) {
					$(this).show();
				}
			});
			$(".list_pay_right").css("display", "none");
			console.log(comma(nOpmSettMap[nowSel]) + "원");
		}
	}

	// No data Chart
	function fnNoDataChart(id) {
		var chart = am4core.create(id, am4charts.PieChart);
		chart.data = [{
			"country": "Dummy",
			"disabled": true,
			"value": 1000,
			"color": am4core.color("#dadada"),
			"opacity": 0.3,
			"strokeDasharray": "4,4",
			"tooltip": ""
		}];

		var label = chart.createChild(am4core.Label);
		label.text = "데이터가 없습니다.";
		label.fontSize = 12;
		label.fill = "#b5b5b6";
		label.isMeasured = false;
		label.x = $("#" + id).width() / 2 - 45;
		label.y = $("#" + id).height() / 2 - 10;

		/* Create series */
		var series = chart.series.push(new am4charts.PieSeries());
		series.dataFields.value = "value";
		series.dataFields.category = "country";

		/* Set tup slice appearance */
		var slice = series.slices.template;
		slice.propertyFields.fill = "color";
		slice.propertyFields.fillOpacity = "opacity";
		slice.propertyFields.stroke = "color";
		slice.propertyFields.strokeDasharray = "strokeDasharray";
		slice.propertyFields.tooltipText = "tooltip";
		series.labels.template.propertyFields.disabled = "disabled";
		series.ticks.template.propertyFields.disabled = "disabled";
	}
</script>