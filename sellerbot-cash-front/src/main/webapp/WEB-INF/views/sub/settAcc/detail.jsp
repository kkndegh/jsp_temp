<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="/assets/js/settAcc.js"></script>
<input type="hidden" name="modal_mall_cd" id="modal_mall_cd" value="">

<style>
	.tui-grid-body-area {
		overflow: auto !important;
	}

	.select_mall_sizeFix {
		width: 9rem;
		border: 0;
		display: inline-block;
		background-color: #fff;
		border-radius: 3rem;
		margin-left: 1.5rem;
	}

	.new_payment_detail_top_board_title span {
		display: inline-block;
		font-size: 1.125rem;
		color: #555555;
		margin-top: 0.1rem;
		vertical-align: middle;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
		padding-top: 1.5rem;
	}

	.select2-search--dropdown {
		display: none !important;
	}

	.select2-container .select2-selection--single .select2-selection__rendered {
		font-size: 1rem;
	}

	.select2-container--default .select2-selection--single .select2-selection__rendered {
		line-height: 22px !important;
		width: 100%;
	}

	.select2-container--default .select2-selection--single {
		width: 8rem;
	}

	.select2-container--open .select2-dropdown--below {
		border-radius: 10px;
	}

	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		margin-top: 0.5rem;
	}

	::-webkit-scrollbar {
		height: 10px;
		/* 가로축 스크롤바 길이 */
	}

	.tuiGridCustomColor {
		color: #d00017;
	}
</style>

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-code-snippet.js"></script>
<script src="/assets/js/tui-pagination.js"></script>
<script src="/assets/js/tui-grid.js"></script>

<div class="container">
	<div class="payment_wra payment_bg_wra">
		<div class="layout_payment layout_table_pay">
			<!-- 상단 메뉴 Start -->
			<div class="menu_top">
				<p class="menu_name">정산예정금 확인</p>
				<div class="gnb_topBox">
					<ul class="gnb_top clearfix">
						<li><a href="/sub/settAcc/all">정산예정금 한 눈에 보기</a></li>
						<li class="focus"><a href="/sub/settAcc/detail">정산예정금 상세보기</a></li>
						<li><a href="/sub/settAcc/calendar">정산예정금 달력</a></li>
					</ul>
				</div>
			</div>
			<!-- 상단 메뉴 End -->
			<!-- 정산예정금 현황 Start -->
			<div class="area_all_payment">
				<div class="calendar_guide">GUIDE</div>
				<!-- 가이드 문구 팝업 strat -->
				<div class="calendar_guide_pop">
					<div class="calendar_guide_pop_header">
						<div class="calendar_guide_pop_shp">GUIDE</div>
						<p>도움말</p>
						<div class="calendar_guide_pop_close">
							<div></div>
							<div></div>
						</div>
					</div>
					<ul class="calendar_guide_pop_con">
						<li class="calendar_guide_pop_con_title">배송상태 기준<br />정산예정금
						</li>
						<li>정산예정금 오픈마켓에서는 정산예정금을 배송상태별로 구분하며 구매 확정 후 1~2 영업일 내에 정산금이 지급됩니다.</li>
					</ul>
					<ul class="calendar_guide_pop_con calendar_guide_pop_con_last">
						<li class="calendar_guide_pop_con_title">정산일자 기준<br />정산예정금
						</li>
						<li>비오픈마켓에서는 정산예정금을 정산일자별로 구분하며 지정된 정산일자에 정산금이 지급됩니다.</li>
					</ul>
				</div>
				<!-- 가이드 문구 팝업 end -->
				<div class="status_box">
					<c:choose>
						<c:when test="${ not empty mallInfo}">
							<span class="all">전체 ${mallInfo.all_mall_cnt - mallInfo.del_mall_cnt}</span> /
							<span class="normal">정상 ${mallInfo.nor_mall_cnt}</span> /
							<span class="warning">점검 ${mallInfo.ins_mall_cnt}</span> /
							<span class="error">오류 ${mallInfo.err_mall_cnt}</span>
						</c:when>
						<c:otherwise>
							<span class="all">전체 0</span> /
							<span class="normal">정상 0</span> /
							<span class="error">오류 0</span>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="update_info">
					<a href="javascript:return false;" style="cursor:default;"><img
							src="/assets/images/payment/refresh.png" alt=""></a>
					마지막 업데이트일자 ${settAccSche.scra_ts }
				</div>
				<div class="calc_payment_box">
					<div class="item_calc">
						<p class="lb">
							<b>오픈마켓</b> (배송상태 기준)
						</p>
						<p class="num">
							<b>
								<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum}" pattern="#,###" /></b>원
						</p>
					</div>
					<div class="icon">+</div>
					<div class="item_calc">
						<p class="lb">
							<b>비오픈마켓</b> (정산일자 기준)
						</p>
						<p class="num">
							<b>
								<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum}" pattern="#,###" /></b>원
						</p>
					</div>
					<div class="icon icon_2">=</div>
					<div class="item_calc total">
						<p class="lb">정산예정금 합계</p>
						<p class="num">
							<b>
								<fmt:formatNumber value="${settAccSche.sett_acc_pln_sum}" pattern="#,###" /></b>원
						</p>
						<div onclick="location.href='/sub/pre_calculate/pre_calculate'" class="btn_tip"></div>
					</div>
				</div>
			</div>
			<!-- 정산예정금 현황 End -->
			<!-- 가장 가까운/먼 정산일 Start -->
			<c:if test="${not empty tipFirstVo || not empty tipLastVo}">
				<div class="pay_day_preview clearfix">
					<c:if test="${fn:length(custSettAccScheTipVo.mall_tip) > 0}">
						<div onclick="location.href='/sub/settAcc/calendar'" class="pay_day_preview_tipbox">
							TIP
							<div class="pay_day_preview_tipbox_num">
								${tipCount }
							</div>
						</div>
					</c:if>
					<c:choose>
						<c:when test="${not empty tipFirstVo }">
							<fmt:parseDate value="${tipFirstVo.sett_acc_pln_dt}" var="sett_acc_pln_dt"
								pattern="yyyyMMdd" />
							<div class="item_payday">
								<p class="txt_payday">가장 <span>가까운</span> 정산일</p>
								<p class="txt_payday">
									<fmt:formatDate value="${sett_acc_pln_dt}" pattern="yyyy-MM-dd" />
								</p>
								<p class="txt_payday">${tipFirstVo.mall_cd_nm}</p>
								<p class="txt_payday money"><b>
										<fmt:formatNumber value="${tipFirstVo.sett_acc_pln}" pattern="#,###" /></b>원</p>
							</div>
						</c:when>
						<c:otherwise>
							<div class="item_payday">
								<p class="txt_payday">가장 <span>가까운</span> 정산예정금 데이터가 없습니다.</p>
							</div>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${not empty tipLastVo }">
							<fmt:parseDate value="${tipLastVo.sett_acc_pln_dt}" var="sett_acc_pln_dt"
								pattern="yyyyMMdd" />
							<div class="item_payday">
								<p class="txt_payday">가장 <span class="color2">먼</span> 정산일</p>
								<p class="txt_payday">
									<fmt:formatDate value="${sett_acc_pln_dt}" pattern="yyyy-MM-dd" />
								</p>
								<p class="txt_payday">${tipLastVo.mall_cd_nm}</p>
								<p class="txt_payday money"><b>
										<fmt:formatNumber value="${tipLastVo.sett_acc_pln}" pattern="#,###" /></b>원</p>
							</div>
						</c:when>
						<c:otherwise>
							<div class="item_payday">
								<p class="txt_payday">가장 <span>먼</span> 정산예정금 데이터가 없습니다.</p>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
			</c:if>
			<!-- 가장 가까운/먼 정산일 End -->
		</div>
		<!--테이블 시작-->
		<div class="account_sum_date_wrap_title">
			<a href="/sub/settAcc/detail/excel" class="btn-account-excel">엑셀다운로드</a>
		</div>
		<!--테이블 1-->
		<div class="grid_sizeFix">
			<!--테이블 타이들(셀렉트박스)-->
			<div class="new_payment_detail_top_board_title">
				<h1>오픈마켓</h1>
				<span>쇼핑몰 선택</span>
				<div class="select_mall_sizeFix">
					<select class="select_mall select_mall_1" name="mall" id="opmSelectBox">
						<option value="">전체</option>
						<c:if test="${opmDataMalList!=null}">
							<c:forEach var="opmData" items="${opmDataMalList}" varStatus="status">
								<option value="${opmData.cust_mall_seq_no }" data-mall="${opmData.mall_nm }"
									data-id="${opmData.mall_cert_1st_id }" data-mallCd="${opmData.mall_cd }">
									${opmData.mall_nm }(${opmData.mall_cert_1st_id })</option>
							</c:forEach>
						</c:if>
					</select>
				</div>
			</div>
			<div id="grid_1" class="div-gird">
				<!--grid-->
			</div>
		</div>
		<!--그리드 팝업 1-->
		<div class="grid_1_popup">
			<!--그리드 팝업-->
			<div class="grid_popup_wrapper">
				<div class="grid_popup_header grid_popup_header_1">
					<h1>정산예정금 상세정보</h1>
					<h2>
						옥션<b>(pigeonmall)</b>
					</h2>
					<div class="pay_popup_header_close_1">
						<div></div>
						<div></div>
					</div>
				</div>
				<div id="grid_popup_1" class="div-gird"></div>
				<div id="grid_popup_1-tui-pagination-container" class="tui-pagination"></div>
			</div>
			<div class="grid_background"></div>
		</div>
		<!--테이블 2-->
		<div class="grid_sizeFix_2">
			<div class="new_payment_detail_top_board_title">
				<h1>비오픈마켓</h1>
				<span>쇼핑몰 선택</span>
				<div class="select_mall_sizeFix">
					<select class="select_mall select_mall_2" name="mall" id="nOpmSelectBox">
						<option value="" data-mall="" data-id="">전체</option>
						<c:if test="${nOpMallList!=null}">
							<c:forEach var="nOpmData" items="${nOpMallList}" varStatus="status">
								<option value="${nOpmData.cust_mall_seq_no}" data-mall="${nOpmData.mall_cd_nm }"
									data-id="${nOpmData.mall_cert_1st_id }" data-mallCd="${nOpmData.mall_cd }">
									${nOpmData.mall_cd_nm }(${nOpmData.mall_cert_1st_id})
								</option>
							</c:forEach>
						</c:if>
					</select>
				</div>
			</div>
			<!--그리드 플러그인-->
			<div id="grid_2" class="div-gird"></div>
			<div id="grid_2-tui-pagination-container" class="tui-pagination"></div>
			<!--그리드 팝업 2-->
			<div class="grid_2_popup">
				<!--그리드 팝업-->
				<div class="grid_popup_wrapper">
					<div class="grid_popup_header grid_popup_header_2">
						<h1>정산예정금 상세정보</h1>
						<h2>
							티켓몬스터<b>(쿨앤쿨1)</b>
						</h2>
						<div class="pay_popup_header_close_1 header_close_2">
							<div></div>
							<div></div>
						</div>
					</div>
					<div id="grid_popup_2" class="div-gird"></div>
					<div id="grid_popup_2-tui-pagination-container" class="tui-pagination"></div>
				</div>
				<div class="grid_background"></div>
			</div>
		</div>
	</div>
	<%@include file="../../widgets/banner.jsp" %>
</div>
<script>
	var _grids = {};
	var _opmDataList;
	var _nOpmDataList;
	var _codeList = [
		{ "code": "SUM", "name": "합계" },
		{ "code": "RDY", "name": "발송대상" },
		{ "code": "DOI", "name": "배송 중" },
		{ "code": "DON", "name": "배송완료" },
		{ "code": "CON", "name": "구매확정" }
	];
	var _mallColumns = [];
	var _fullDataSet = []; // 비오픈 마켓 데이터 수량

	var _excelData = { "grid_1": [], "grid_2": [] };
	<c:if test="${not empty opmDataList }">
		_opmDataList =  ${opmDataList};
	</c:if>

	<c:if test="${not empty nOpmDataList }">
		_nOpmDataList =  ${nOpmDataList};
	</c:if>

	/**
	* 그리드 그리기
	*/
	function fnDrowGrid(id, columns, complexColumns, dataset, isPage, color) {
		//  메인화면: true, 모달: false
		var isCheck = true;
		if (id == "grid_1" || id == "grid_2") {
			isCheck = false;
		}

		var jsonSum = {};
		var json = {};
		var isDataset = dataset.length > 0 ? true : false;
		var keys = [];

		var gridOption = fnGetGridOption();

		// 모달인 경우
		if (isCheck) {
			gridOption.columnOptions.frozenCount = 3;
		}

		var colWidth = fnGetGridColwidth();

		$.each(columns, function (i) {
			this.width = colWidth;
		});

		if (isDataset) {
			// 함계
			jsonSum = fnGetSumData(dataset, isCheck, columns);

			var sortColumns = fnColumSort(id, jsonSum, columns, complexColumns);
			columns = sortColumns.columns;

			$.each(columns, function (i) {
				if (i == 0) {
					json[this.name] = "합계";
					this.align = "left";
				} else {
					// 예외처리 추가 정산요율 수수료 좌측 정렬 추가
					if (isNull(jsonSum[this.name]) && ("SETT_ACC_RATE" != this.name && "FEE_RTO" != this.name)) {
						json[this.name] = "-";
						this.align = "left";
						this.formatter = function (jsonValue) {
							return jsonValue.value;
						}
					} else {
						json[this.name] = jsonSum[this.name];
						this.align = "right";
						this.formatter = function (jsonValue) {
							return fnAddComma(jsonValue.value);
						}
					}
				}
			});

			for (var i = 0; i < gridOption.columnOptions.frozenCount; i++) {
				keys.push(columns[i].name);
			}

			$.each(dataset, function () {
				this._attributes = { "className": { column: {} } };
				for (var i in keys) {
					this._attributes.className.column[keys[i]] = color;
				}
			});

			json._attributes = { "className": { column: {} } };

			for (var i in keys) {
				json._attributes.className.column[keys[i]] = color;
			}
		}

		var target = $("#" + id);
		if (columns.length == 0) {
			columns.push({ header: "구분", name: "day", align: "left", width: colWidth });
			columns.push({ header: "계", name: "total", align: "left", width: colWidth });
		}

		gridOption.el = document.getElementById(id);
		gridOption.columns = columns;
		gridOption.scrollX = isDataset;

		if (complexColumns.length > 0) {
			gridOption.header.complexColumns = complexColumns;
		}

		_excelData[id] = {
			"dataset": dataset,
			"columns": columns
		};

		// 모달인 경우 일부 데이터 하이라이트
		if (isCheck)
			highlightForGridData($("#modal_mall_cd").val(), dataset, json, columns);	

		if (isPage) {
			if (isDataset) {
				gridOption.data = dataset.slice(0, 10);
				gridOption.data.unshift(json);
				gridOption.pagination = true;
			} else {
				gridOption.data = [];
			}

		} else {
			if (isDataset) {
				gridOption.data = dataset;
				gridOption.data.unshift(json);
			} else {
				gridOption.data = [];
			}
		}

		var grid = new tui.Grid(gridOption);
		tui.Grid.applyTheme('default', {
			cell: {
				header: {
					background: '#f8f8f8',
					border: '#eaeaea',
				},
				normal: {
					background: '#f8f8f8',
					border: '#eaeaea',
				}
			}
		});

		if(!isCheck) {
			grid.on('mouseover', function (ev) {
				var evGrid = ev.instance;
				var rowKey = ev.rowKey;
				var id = $(evGrid.el).attr("id");
				var fixedBackgroundColor = "grid_background_color_1_left";
				if (id == "grid_2" || id == "grid_popup_2") {
					fixedBackgroundColor = "grid_background_color_2_left";
				}
				evGrid.addRowClassName(rowKey, fixedBackgroundColor);
			});

			grid.on('mouseout', function (ev) {
				var evGrid = ev.instance;
				var rowKey = ev.rowKey;
				var id = $(evGrid.el).attr("id");
				var fixedBackgroundColor = "grid_background_color_1_left";
				if (id == "grid_2" || id == "grid_popup_2") {
					fixedBackgroundColor = "grid_background_color_2_left";
				}
				evGrid.removeRowClassName(rowKey, fixedBackgroundColor);
			});
		}

		if (isPage && isDataset) {
			var container = document.getElementById(id + "-" + "tui-pagination-container");
			var Pageoption = {
				totalItems: dataset.length,
				itemsPerPage: 10,
				visiblePages: 5,
				page: 1,
				centerAlign: false,
				firstItemClassName: 'tui-first-child',
				lastItemClassName: 'tui-last-child',
				usageStatistics: true
			};
			var pagination = new tui.Pagination(container, Pageoption);

			pagination.on('beforeMove', function (ev) {
				var page = ev.page;
				var last = Pageoption.itemsPerPage * page;
				var start = last - Pageoption.itemsPerPage;
				if (last > Pageoption.totalItems) {
					last = Pageoption.totalItems;
				}

				var cnt = grid.getRowCount();
				for (var i = 0; i < cnt; i++) {
					grid.removeRow(i);
				}
				var dataList = dataset.slice(start, last);
				var pagingGridColunm = {};
				for (var i in keys) {
					var key = keys[i];
					pagingGridColunm[key] = color;
				}

				// 합계 행
				json._attributes = { "className": { column: {} } };
				json._attributes.className.column = pagingGridColunm;
				grid.appendRow(json);

				for (var i in dataList) {
					dataList[i]._attributes = { "className": { column: {} } };
					var gridColunm = {};
					for (var j in keys) {
						gridColunm[keys[j]] = color;
					}

					// body 행
					dataList[i]._attributes.className.column = pagingGridColunm;
					grid.appendRow(dataList[i]);
				}
			});
		}

		_grids[id] = grid;
		// 모달인 경우 일부 header 하이라이트
		if (isCheck)
			highlightForGridHeader($("#modal_mall_cd").val(), id, dataset, gridOption);
	}

	//오픈 마켓 테이블  
	function fnTbOpenMarket() {
		var id = "grid_1";
		var columns = [];
		var complexColumns = [];
		var dataSet = [];
		var isPage = false;

		columns.push({
			header: '구분',
			name: 'label',
			align: 'left'
		},
			{
				header: '계',
				name: 'sum',
				align: 'right'
			});

		if (typeof _opmDataList != "undefined" && _opmDataList.mall.length > 0) {
			var beforeMallNm = "";

			$.each(_opmDataList.mall, function () {
				columns.push({
					header: this.mall_cert_1st_id,
					name: this.cust_mall_seq_no,
					code: this.mall_cd,
					align: 'right'
				});

				if (this.mall_nm != beforeMallNm) {
					complexColumns.push({
						header: this.mall_nm,
						name: "mergeColumn" + this.cust_mall_seq_no,
						code: this.mall_cd,
						childNames: [this.cust_mall_seq_no]
					});
				} else {
					complexColumns[complexColumns.length - 1].childNames.push(this.cust_mall_seq_no);
				}

				beforeMallNm = this.mall_nm;
			});

			$.each(_codeList.slice(1), function (key) {
				var objCode = this;
				var vo = { "label": objCode.name };
				vo.sum = 0;

				$.each(_opmDataList.mall, function () {
					vo[this.cust_mall_seq_no] = fnGetStsCdMoney(this.sett_acc_pln_open_dlv_l, objCode.code);
					vo.sum = vo.sum + vo[this.cust_mall_seq_no];
				});

				dataSet.push(vo);
			});
		}

		fnDrowGrid(id, columns, complexColumns, dataSet, isPage, "bg-blue");
	}

	//비오픈 마켓 테이블  
	function fnTbNotOpenMarket() {
		var id = "grid_2";
		var columns = [];
		var complexColumns = [];
		var dataSet = [];
		var isPage = false;

		fnSetDataSet();
		var mallList = fnGetMallList();

		columns.push({
			header: '구분',
			name: 'day',
			align: 'left'
		},
			{
				header: '계',
				name: 'total',
				align: 'right'
			});

		var beforeMallNm = "";

		$.each(mallList, function () {
			columns.push({
				header: this.mall_cert_1st_id,
				name: this.cust_mall_seq_no,
				code: this.mall_cd,
				align: 'right'
			});

			if (this.mall_cd_nm != beforeMallNm) {
				complexColumns.push({
					header: this.mall_cd_nm,
					name: "mergeColumn" + this.cust_mall_seq_no,
					code: this.mall_cd,
					childNames: [this.cust_mall_seq_no]
				});
			} else {
				complexColumns[complexColumns.length - 1].childNames.push(this.cust_mall_seq_no);
			}

			beforeMallNm = this.mall_cd_nm;
		});

		dataSet = _fullDataSet;
		fnDrowGrid(id, columns, complexColumns, dataSet, true, "bg-karry");
	}

	//발송 대상 별 몰별 상세 금액 취득
	function fnGetStsCdMoney(sett_acc_pln_open_dlv_l, dvl_sts_cd) {
		var target = sett_acc_pln_open_dlv_l.filter(function (json) {
			if (dvl_sts_cd == json.dvl_sts_cd) {
				return json;
			}
		});

		if (typeof target[0] == "undefined") {
			return 0;
		}

		return target[0].dvl_sett_acc_pln;
	}

	// 일자 별 정렬
	function fnSort() {
		if (typeof _nOpmDataList == "undefined") {
			return false;
		}
		_nOpmDataList = _nOpmDataList.sett_acc_pln_mall_day_l.sort(JsonArraySort("day"));
	}

	// 중복 제거 한 일자 데이터
	function fnGetDays() {
		var result = []; // 일자

		if (typeof _nOpmDataList == "undefined") {
			return false;
		}
		// 일자별 정렬
		var beforeDay = "";

		$.each(_nOpmDataList.sett_acc_pln_mall_day_l, function () {
			if (beforeDay != this.day) { result.push(this.day); }
			beforeDay = this.day;
		});

		return result;
	}

	//비오픈 몰 갯수
	function fnGetMallList() {
		var mallList = [];
		// 상점갯수
		$.each($("#nOpmSelectBox option"), function () {
			if (nonNull($(this).val())) {
				mallList.push({
					"cust_mall_seq_no": $(this).val(),
					"mall_cd_nm": $(this).data("mall"),
					"mall_cert_1st_id": $(this).data("id"),
					"mall_cd": $(this).data("mallcd")
				});
			}
		});
		return mallList;
	}

	//테이블 표기 형  데이터
	function fnSetDataSet() {
		var days = fnGetDays(); // 일자
		var mallList = fnGetMallList();
		var rowData = { "day": "", "total": 0 };
		var beforeDay = "";
		for (var i in days) {
			if (beforeDay != days[i]) {
				rowData = { "day": days[i], "total": fnGetDaySumMoney(days[i]) };
				$.each(mallList, function (item) {
					rowData[this.cust_mall_seq_no] = fnGetMallDayMoney(days[i], this.cust_mall_seq_no);
				});
				_fullDataSet.push(rowData);
			} else {
				days;
			};
			beforeDay = days[i];
		}
	}

	//일별 합계 값
	function fnGetDaySumMoney(day) {
		var sum = 0;
		var dayList = _nOpmDataList.sett_acc_pln_mall_day_l.filter(function (item) {
			if (day == item.day) {
				return item;
			}
		});
		$.each(dayList, function () {
			sum = sum + this.day_sett_acc_pln;
		});
		return sum;
	}

	//상점, 일 별 금액 취득
	function fnGetMallDayMoney(day, seq_no) {
		var result = 0;

		var dayList = _nOpmDataList.sett_acc_pln_mall_day_l.filter(function (item) {
			if (day == item.day && item.cust_mall_seq_no == seq_no) {
				return item;
			}
		});

		$.each(dayList, function () {
			result = result + this.day_sett_acc_pln;
		});

		return result;
	}

	/********************************************************
	 *  모달 영역
	 ********************************************************/
	//모달 상세 정보 조회
	function fnGetDetailPaymentData(id, callback) {
		var target = $("#" + id);
		var mall_cd = $("#" + id + " option:selected").data("mallcd");
		var paymentParam = { "custMallSeqNo": target.val() };

		showLoadingModal();
		$.get("/sub/settAcc/detail/ajax", $.param(paymentParam), function (payments) {
			var mallParam = { "mallCd": mall_cd };
			$.get("/sub/settAcc/detail/ajax/cul", $.param(mallParam), function (mallList) {
				_mallColumns = mallList;
				callback(payments, mallList);
				hideLoadingModal();
			});
		});
	}

	// 오픈 마켓 모달
	function fnShowOpenMarketModal() {
		fnGetDetailPaymentData("opmSelectBox", function (payments, mallList) {

			var sortPayments = [];
			$.each(_codeList, function () {
				var code = this;
				$.each(payments, function () {
					if (code.code == this.COM_DLV_STS) {
						this.COM_DLV_STS = code.name;
						sortPayments.push(this);
					}
				});
			});

			var id = "grid_popup_1";
			var columns = [];
			var complexColumns = []
			var dataSet = sortPayments;

			var selected = $("#opmSelectBox option:selected");
			$(".grid_popup_header_1").find("h2").html(selected.data("mall") + "<b>(" + selected.data("id") + ")</b>");

			$.each(mallList, function () {
				columns.push({
					header: this.KOR_CUL,
					name: this.ENG_CUL,
					align: 'right'
				});
			});

			$("#modal_mall_cd").val(selected.data("mallcd"));
			fnDrowGrid(id, columns, complexColumns, dataSet, true, "bg-blue");

			$(".grid_1_popup").addClass("grid_1_popup_active");
			$("body").css("overflowY", "hidden");
		});
	}

	//비오픈 마켓 모달
	function fnShowNotOpenMarketModal() {
		fnGetDetailPaymentData("nOpmSelectBox", function (payments, mallList) {
			var id = "grid_popup_2";
			var columns = [];
			var complexColumns = []
			var dataSet = payments;

			$.each(mallList, function () {
				columns.push({
					header: this.KOR_CUL,
					name: this.ENG_CUL,
					align: 'right'
				});
			});

			var selected = $("#nOpmSelectBox option:selected");
			$(".grid_popup_header_2").find("h2").html(selected.data("mall") + "<b>(" + selected.data("id") + ")</b>");

			$("#modal_mall_cd").val(selected.data("mallcd"));
			fnDrowGrid(id, columns, complexColumns, dataSet, true, "bg-karry");
			
			$(".grid_2_popup").addClass("grid_1_popup_active");
			$("body").css("overflowY", "hidden");
		});
	}

	//합계 연산
	function fnGetSumData(dataList, isCheck, columns) {
		var result = {};
		var json = dataList[0];
		var column = {};
		if (isCheck) {
			for (var i in columns) {
				column = columns[i];
				if (fnIsSumCol(column.name)) {
					result[column.name] = 0;
				}
			}
		} else {
			for (var i in columns) {
				column = columns[i];
				result[column.name] = 0;
			}
		}

		Object.keys(result).forEach(function (key) {
			$.each(dataList, function () {
				result[key] = nvl(this[key], 0) + nvl(result[key], 0);
			});
		});

		return result;
	}

	//합계 대상 컬럼
	function fnIsSumCol(name) {
		var col = {};

		// 정산 요율 합계 제외 요청 smp관리페이지 개발 후 삭제
		if ("SETT_ACC_RATE" == name || "FEE_RTO" == name) {
			return false;
		}

		for (var index in _mallColumns) {
			col = _mallColumns[index]
			if (col.ENG_CUL == name && (col.DATA_TYP == "int" || col.DATA_TYP == "float")) {
				return true;
			}
		}

		// smp관리페이지 개발 후 삭제
		var sumTarget = [
			'svc_use_fee', 'seller_cpn_disc', 'pch_cpn_apply_prc', 'prime_cust_disc', 'plur_pch_disc'
			, 'smile_cash_sav', 'sale_prc_sum', 'dedu_prc_sum', 'maoi_dlv_prc', 'sale_unit_prc', 'sum_prc'
			, 'sum_num', 'card_num', 'card_prc', 'acct_trs_num', 'acct_trs_prc', 'tel_bank_num', 'tel_bank_prc'
			, 'vir_acct_num', 'vir_acct_prc', 'kmoney_num', 'kmoney_prc', 'pay_hld', 'pay_hld_rel', 'ext_iad'
			, 'u_pay_remai', 't_day_u_pay_prc', 'remit_fee', 'remit_fee_stax', 'sett_acc_pln', 'orde_prc'
			, 'sale_cnt', 'no_dlv_num', 'goods_purc_tot_prc', 'pay_tgt_tot_prc', 'n_s_sales_link_fee'
			, 'fna_rel_pay_prc', 'orde_dlv_prc', 'can_dlv_prc', 'rtn_dlv_prc', 'orde_free_dlv_prc', 'rel_oub_num'
			, 'rel_pay_prc', 'sout_rew', 'oub_dely_rew', 'temp_invo', 'temp_invo_plt_exemp', 'sms_use'
			, 'sav_prc', 'ad_laun_dbkeep_cost', 'defer_forb_prc', 'smon_defer_prc', 'b_pay_a_remai', 'dedu_b_tot_sum_40_per'
			, 'dedu_b_tot_sum_60_per', 'ch_div', 'cons_sale_prc', 'thi_cl_fee', 'cj_sum_prc', 'sale_prc', 'tra_cl_disc_buen_prc'
			, 'ssg_disc_buen_prc', 'disc_buen_prc_sum', 'pure_sale_prc', 'tari', 'stax', 'tot_dlv_prc', 'tra_cl_dlv_prc'
			, 'ssg_dlv_prc', 'tra_cl_dlv_prc_cpn_prc', 'ssg_dlv_prc_cpn_prc', 'cust_buen_dlv_prc', 'succ_prc'
			, 'succ_num', 'all_num', 'all_prc', 'can_num', 'can_prc', 'sale_num', 'pch_tot_prc', 'bas_sale_fee'
			, 'cpn_disc_prc', 'cust_rel_orde_prc', 'sett_acc_num', 'cpn_biz_sote', 'rel_sale_tot_prc', 'cpn_disc_tot_prc'
			, 'tax_bill_pub_prc', 'biz_depo_pln_prc', 'wiwd_sales_prc_prc', 'glob_sales_prc_prc', 'sub_mt_purc_prc'
			, 'pln_sett_acc_prc', 'disc_cpn_use_fee', 'plt_dedu_prc', 'dedu_etc', 'sales_num', 'wiwd_goods_prc_pay_prc'
			, 'glob_goods_purc', 'supp_prc', 'oub_cnt', 'rtn_cnt', 'rel_oub_cnt', 'sales_prc_tot_prc', 'fna_pay_pln_fee'
			, 'prio_fee_refund_prc', 'sett_acc_tgt_prc', 'seller_disc_cpn', 'chrg_logi_prc', 'seller_svc_use_fee'
			, 'plt', 'sett_acc_dedu', 'cu_use_fee', 'cu_rew', 'rel_sales_prc', 'pay_tgt_prc', 'logi_cons_dlv_prc'
			, 'rtn_swap_dlv_prc', 'dlv_dely_plt', 'svc_fee', 'ad_fee', 'pic_pict_cost', 'cont_prou_cost'
			, 'ajst_fee', 'pay_pln_fee', 'pre_pay_prc', 'acm_pay_prc', 'sout_rew_sav_prc', 'dedu_refund_prc'
			, 'partn_buen_cpn', 'all_dlv_done_prc', 'wmp_buen_cpn', 'owner_buen_goods_cpn', 'dlv_prc'
			, 'bill_tgt_ajst_prc', 'bill_no_tgt_ajst_prc', 'redu_fee', 'deal_reg_cost', 'svr_use_fee', 'dsg_filming_cost'
			, 'lck_fee', 'pre_sett_acc_dedu', 'ad_cost', 'tot_prc', 'dedu_prc', 'sales_prc_sum', 'orde_cnt'
			, 'orde_sup_prc', 'orde_sales_prc', 'can_sup_prc', 'can_sales_prc', 'can_cnt', 'sup_prc_sum'
			, 'pch_prc', 'tot_cnt', 'sale_tot_prc', 'fplus_buen_cpn_prc', 'sale_fee', 'biz_buen_cpn_prc'
			, 'fore_pay_prc', 'tot_sales_prc', 'sales_p_cost', 'purc_p_cost', 'pay_pln_prc', 'purc_prc'
			, 'on_ln_sales_prc', 'on_ln_orde_num', 'goods_unit_prc', 'cnt', 'disc_prc', 'sales_prc'
			, 'fee', 'cpn_prc', 'cpn_prc_biz_buen_prc', 'sett_acc_pln_prc', 'goods_tot_orde_prc', 'num'
			, 'goods_prc', 'opt_prc', 'goods_disc_prc', 'stor_buen_disc_prc', 'dlv_prc_sum', 'dlv_prc_disc_prc'
			, 'pay_fee', 'sale_sett_acc_prc', 'goods_orde_prc', 'p_pay_dlv_prc', 'seller_disc_prc', 'bas_fee'
			, 'show_add_fee', 'fee_sum', 'com_sett_acc_pln_prc', 'com_num', 'remai', 'sett_acc_prc', 'bil_net_prc'
			, 'pay_prc', 'sett_acc_tgt_dlv_prc', 'dlv_done_prc', 'dlv_done_cnt', 'dlv_done_refund_cnt'
			, 'dlv_done_refund_prc', 'sale_agen_fee', 'wmp_buen_goods_cpn', 'card_corp_buen_goods_cpn', 'sale_biz_buen_goods_cpn'
			, 'wmp_buen_basket_cpn', 'card_corp_buen_basket_cpn', 'sale_biz_buen_basket_cpn', 'hld_prc', 'pre_sett_acc_dedu_prc'
		];

		for (var i in sumTarget) {
			if (sumTarget[i] == name.toLowerCase()) {
				return true;
			}
		}

		return false;
	}

	$(document).ready(function () {
		$(".status_box").css("cursor", "pointer");
		$(".status_box").click(function () {
			location.href = "/sub/my/market";
		});

		// 오픈 마켓 테이블
		fnTbOpenMarket();

		// 비오픈 마켓  데이터 날짜별 정렬
		if (typeof _nOpmDataList != "undefined" && _nOpmDataList.sett_acc_pln_mall_day_l.length > 0) {
			_nOpmDataList.sett_acc_pln_mall_day_l = _nOpmDataList.sett_acc_pln_mall_day_l.sort(JsonArraySort("day"));
		}

		fnTbNotOpenMarket();

		$(".calendar_guide").click(function () {
			$(".calendar_guide_pop").addClass("calendar_guide_pop_active");
		});

		$(".calendar_guide_pop_close").click(function () {
			$(".calendar_guide_pop").removeClass("calendar_guide_pop_active");
		});

		//select2
		$(".select_mall_1").select2();
		$(".select_mall_2").select2();

		$('.select_mall_1').on("change", function (e) {
			if (nonNull($(this).val())) {
				fnShowOpenMarketModal();
			}
		});

		$('.select_mall_2').on("change", function (e) {
			if (nonNull($(this).val())) {
				$('body').loadingModal({ text: 'Showing loader...' });
				fnShowNotOpenMarketModal();
			}
		});

		//닫기버튼
		$(".pay_popup_header_close_1").click(function () {
			$(".grid_1_popup").removeClass("grid_1_popup_active");
			$("body").css("overflowY", "visible");
			$("#grid_popup_1").empty();
			$("#grid_popup_1-tui-pagination-container").empty();
			$('#opmSelectBox').val("").trigger('change.select2');
			delete _grids.grid_popup_1;
		});

		//닫기버튼
		$(".header_close_2").click(function () {
			$(".grid_2_popup").removeClass("grid_1_popup_active");
			$("body").css("overflowY", "visible");
			$("#grid_popup_2").empty();
			$("#grid_popup_2-tui-pagination-container").empty();
			$('#nOpmSelectBox').val("").trigger('change.select2');
			delete _grids.grid_popup_2;
		});

		// 애큐온페이지 이동
		$(".bnr_acuon .area_bnr_acuon").click(function() {
			location.href = '/sub/pre_calculate/detail?finGoodSeqNo=19';
		});
	});

	function fnGetGridOption() {
		var gridOption = {
			'scrollX': false,
			'scrollY': false,
			'minBodyHeight': 30,
			'width': 1200,
			//		    'maxBodyWidth': 1200,
			'minBodyHeight': 30,
			'header': {
				'width': 150,
				'height': 100
			},
			'columnOptions': {
				'frozenCount': 2,
				'frozenBorderWidth': 1
			}
		};
		var width = $(window).width();
		if (width > 1200) { width = 1200; }
		var colWidth = fnGetGridColwidth();
		// 모바일
		if (width < 768) {
			gridOption.width = width;
			gridOption.maxBodyWidth = width;
			gridOption.header.width = colWidth;
		} else if (width < 1200) {
			gridOption.width = width;
			gridOption.maxBodyWidth = width;
			gridOption.header.width = colWidth;
		}
		return gridOption;
	}

	function fnGetGridColwidth() {
		var width = $(window).width();
		if (width > 1200) { width = 1200; }
		var colWidth = 150;
		if (width < 768) {
			colWidth = width / 4;
		} else if (width < 1200) {
			colWidth = width / 6;
		}
		return colWidth;
	}

	function fnColumSort(id, jsonSum, columns, complexColumns) {
		var result = {
			"columns": [],
			"complexColumns": [],
		};

		var mallList = [];
		if (id == "grid_1") {
			mallList = _opmDataList.mall;
		} else if (id == "grid_2") {
			mallList = fnGetMallList();
		} else {
			// 대상이 아니면 컬럼 정렬을 하지 않는다.
			result.columns = columns;
			result.complexColumns = complexColumns;
			return result;
		}

		var sum_mallList = [];
		var selectList = [];

		$.each(complexColumns, function () {
			var sum = {
				"mall_cd": this.code,
				"val": 0
			};

			$.each(this.childNames, function () {
				sum.val += jsonSum[this];
			});
			sum_mallList.push(sum);
		});

		// 정렬
		var sum_mallList = sum_mallList.sort(JsonArraySort("val", true));

		// 컬럼 위치 변경
		$.each(sum_mallList, function () {
			var mall_cd = this.mall_cd;
			var targetList = columns.filter(function (col) {
				if (col.code == mall_cd) { return col; }
			});

			$.each(targetList, function () {
				this.sum = jsonSum[this.name];
			});

			// 정렬
			targetList.sort(JsonArraySort("sum", true));

			// 최종 값 셋팅
			$.each(targetList, function () {
				var target = this;
				result.columns.push(target);
				if (id == "grid_1") {
					$('.select_mall_1 option').each(function (i) {
						if (target.name == this.value) {
							selectList.push(this);
						}
					});
				} else if (id == "grid_2") {
					$('.select_mall_2 option').each(function (i) {
						if (target.name == this.value) {
							selectList.push(this);
						}
					});
				}
			});
		});

		if (id == "grid_1") {
			selectList.unshift($('.select_mall_1 option')[0]);
			$('.select_mall_1').empty();
			$('.select_mall_1').append(selectList);
			$('.select_mall_1').val("").trigger('change.select2');


		} else if (id == "grid_2") {
			selectList.unshift($('.select_mall_2 option')[0]);
			$('.select_mall_2').empty();
			$('.select_mall_2').append(selectList);
			$('.select_mall_2').val("").trigger('change.select2');

		}

		// 구분 / 계
		result.columns.unshift(columns[1]);
		result.columns.unshift(columns[0]);

		return result;
	}

	$(window).resize(function () {
		var width = $(window).width();
		if (width > 1200) { width = 1200; }
		var colWidth = 150;
		// 모바일
		if (width < 768) {
			colWidth = width / 4;
		} else if (width < 1200) {
			colWidth = width / 6;
		}

		var widths = [];
		$.each(_grids, function () {
			var grid = this;
			var target = $(grid.el);
			$.each(grid.getColumns(), function () {
				widths.push(colWidth);
			});
			grid.setWidth(width);
			grid.resetColumnWidths(widths);
			setTimeout(function () {
				grid.refreshLayout();
				target.find(".tui-grid-lside-area").find(".tui-grid-body-container").width(colWidth * 2);
			}, 300);
		});
	});
</script>