<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf" %>
<jsp:useBean id="dateUtilBean" class="onlyone.sellerbotcash.web.util.DateUtil" />

<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">
<script src="/assets/js/tui-pagination.js"></script>
<script src="/assets/js/tui-grid.js"></script>

<script src="/assets/js/common/common.js"></script>
<!--script src="/assets/js/common/lws.js"></script-->
<!-- 2020-09-17 wss 프로토콜 적용, DNS 미적용으로 주석처리 -->
<script src="/assets/js/common/lws.js?ver=20211122_01"></script>
<script src="/assets/js/settAcc.js?ver=20200828_01"></script>

<input type="hidden" id="deviceType" value="${DeviceType }">

<div class="container">
	<div class="payment_wra">
		<div class="layout_payment">
			<!-- 상단 메뉴 Start -->
			<div class="menu_top">
				<p class="menu_name">정산예정금 확인</p>
				<div class="gnb_topBox">
					<ul class="gnb_top clearfix">
						<li><a href="/sub/settAcc/all">정산예정금 한 눈에 보기</a></li>
						<li><a href="/sub/settAcc/detail">정산예정금 상세보기</a></li>
						<li class="focus"><a href="/sub/settAcc/calendar">정산예정금 달력</a></li>
					</ul>
				</div>
			</div>
			<!-- 상단 메뉴 End -->
			<!-- 정산예정금 현황 Start -->
			<div class="area_all_payment pb1">
				<div class="calendar_guide">GUIDE</div>
				<!-- 가이드 문구 팝업 strat -->
				<div class="calendar_guide_pop">
					<div class="calendar_guide_pop_header">
						<div class="calendar_guide_pop_shp">GUIDE</div>
						<p>도움말</p>
						<div class="calendar_guide_pop_close">
							<div></div>
							<div></div>
						</div>
					</div>
					<ul class="calendar_guide_pop_con">
						<li class="calendar_guide_pop_con_title">배송상태 기준<br />정산예정금
						</li>
						<li>정산예정금 오픈마켓에서는 정산예정금을 배송상태별로 구분하며 구매 확정 후 1~2 영업일 내에 정산금이 지급됩니다.</li>
					</ul>
					<ul class="calendar_guide_pop_con calendar_guide_pop_con_last">
						<li class="calendar_guide_pop_con_title">정산일자 기준<br />정산예정금
						</li>
						<li>비오픈마켓에서는 정산예정금을 정산일자별로 구분하며 지정된 정산일자에 정산금이 지급됩니다.</li>
					</ul>
				</div>
				<!-- 가이드 문구 팝업 end -->
				<div class="status_box">
					<c:choose>
						<c:when test="${ not empty mallInfo}">
							<span class="all">전체 ${mallInfo.all_mall_cnt - mallInfo.del_mall_cnt}</span> /
							<span class="normal">정상 ${mallInfo.nor_mall_cnt}</span> /
							<span class="warning">점검 ${mallInfo.ins_mall_cnt}</span> /
							<span class="error">오류 ${mallInfo.err_mall_cnt}</span>
						</c:when>
						<c:otherwise>
							<span class="all">전체 0</span> /
							<span class="normal">정상 0</span> /
							<span class="error">오류 0</span>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="update_info">
					<a href="javascript:return false;"><img src="/assets/images/payment/refresh.png" alt=""></a>
					<span>마지막 업데이트일자 ${custSettAccScheVo.scra_ts }</span>
				</div>
				<div class="calc_payment_box">
					<div class="item_calc">
						<p class="lb">
							<b>오픈마켓</b> (배송상태 기준)
						</p>
						<p class="num">
							<b>
								<fmt:formatNumber value="${custSettAccScheVo.sett_acc_pln_open_sum}" pattern="#,###" />
							</b>원
						</p>
					</div>
					<div class="icon">+</div>
					<div class="item_calc">
						<p class="lb">
							<b>비오픈마켓</b> (정산일자 기준)
						</p>
						<p class="num">
							<b>
								<fmt:formatNumber value="${custSettAccScheVo.sett_acc_pln_nopen_sum}" pattern="#,###" />
							</b>원
						</p>
					</div>
					<div class="icon icon_2">=</div>
					<div class="item_calc total">
						<p class="lb">정산예정금 합계</p>
						<p class="num">
							<b>
								<fmt:formatNumber value="${custSettAccScheVo.sett_acc_pln_sum}" pattern="#,###" /></b>원
						</p>
						<div onclick="location.href='/sub/pre_calculate/pre_calculate'" class="btn_tip"></div>
					</div>
				</div>
			</div>
			<!-- 정산예정금 현황 End -->
			<!-- 가장 가까운/먼 정산일 Start -->
			<c:if test="${not empty tipFirstVo || not empty tipLastVo}">
				<div class="pay_day_preview clearfix">
					<c:if test="${not empty tipFirstVo.sett_acc_pln_dt }">
						<div class="item_payday">
							<p class="txt_payday">
								가장 <span>가까운</span> 정산일
							</p>
							<p class="txt_payday">
								${dateUtilBean.makeNonFormatToFormat(tipFirstVo.sett_acc_pln_dt) }
							</p>
							<p class="txt_payday">
								${tipFirstVo.mall_cd_nm}
							</p>
							<p class="txt_payday money">
								<b><fmt:formatNumber value="${tipFirstVo.sett_acc_pln}" pattern="#,###" /></b>원
							</p>
						</div>
					</c:if>
					<c:if test="${not empty tipLastVo.sett_acc_pln_dt }">
						<div class="item_payday">
							<p class="txt_payday">
								가장 <span class="color2">먼</span> 정산일
							</p>
							<p class="txt_payday">
								${dateUtilBean.makeNonFormatToFormat(tipLastVo.sett_acc_pln_dt) }
							</p>
							<p class="txt_payday">
								${tipLastVo.mall_cd_nm}
							</p>
							<p class="txt_payday money">
								<b><fmt:formatNumber value="${tipLastVo.sett_acc_pln}" pattern="#,###" /></b>원
							</p>
						</div>
					</c:if>

				</div>
			</c:if>
			<!-- 가장 가까운/먼 정산일 End -->
			<!--care_tip 20200219 추가-->
			<!-- 정산예정금 TIP Start -->
			<div id="care_tip" class="care_tip">
			</div>
			<!-- 정산예정금 TIP End -->
			<div class="section_calendar_pay">
				<div class="section_calendar_pay_title">
					<h1>&#9654; 정산예정금 달력</h1>
					<span>오늘 이후의 정산일자별 정산예정금은 비오픈마켓만 제공됩니다.</span>
				</div>
				<!-- 정산예정금 달력 Start -->
				<div class="full_calendar_box">
					<div class="total_month_pay">
						<p class="money">113,743,056원</p>
					</div>
					<div id="calendar" class="calendarData"></div>
				</div>
				<!-- 정산예정금 달력 End -->
				<!-- 연월별 정산예정금 예정일 Start -->
				<c:if test="${not empty custSettAccScheDayMallMonth }">
					<!-- 연간 리스트 시작  -->
					<div class="month_pay_detail active">
						<c:forEach var="year" items="${yearList}" varStatus="status">
							<c:set var="monthData" value="${custSettAccScheDayMallMonth['year'] }"></c:set>

							<div class="year_sales_box">
								<!-- 제목 -->
								<c:if test="${status.count == 1}"><span class="year_sales_box_title">&#9654; 연월별 정산예정금
										예정일</span></c:if>
								<!-- 연도 -->
								<p class="year">
									${year}년
								</p>
								<!-- 월 -->
								<c:forEach var="monthSummary" items="${custSettAccScheDayMallMonth[year]}">
									<div class="month_pay_item"
										data-month="${year}${monthSummary['sett_acc_pln_dt_mon']}">
										<p class="month">
											${monthSummary['sett_acc_pln_dt_mon']}월
										</p>
										<p class="price" data-price="${monthSummary['mon_sett_acc_pln']}">
											<b>
												<fmt:formatNumber value="${monthSummary['mon_sett_acc_pln']}"
													pattern="#,###" /></b>원
										</p>
									</div>
								</c:forEach>
							</div>
						</c:forEach>
					</div>
				</c:if>
				<c:if test="${empty custSettAccScheDayMallMonth }">
					<div class="calen_nodata_wrap">
						<span class="year_sales_box_title year_sales_box_title_nodata">&#9654; 연월별 정산예정금 예정일</span>
						<div class="month_pay_detail_no_data calen_month_pay_detail_no_data">
							<img src="/assets/images/common/icon_nodata.png" alt="" />
							<p>데이터가 없습니다.</p>
						</div>
					</div>
				</c:if>
				<!-- 연월별 정산예정금 예정일 End -->
				<!-- 월별 상세 정보 Start -->
				<c:forEach var="yyyymm" items="${yyyymmList }">
					<c:set var="totalValue" value="0" />
					<div class="month_pay_detail item month_pay_detail-${yyyymm}">
						<p class="back_list">목록으로</p>
						<!-- 연월  -->
						<p class="year">
							${dateUtilBean.parseKrDateString(yyyymm)}
						</p>
						<!-- 일별 -->
						<div class="list_detail_day">
							<c:forEach var="dayEntity" items="${custSettAccScheDayMallDay[yyyymm] }">
								<c:set var="totalValue" value="${totalValue+dayEntity.day_sett_acc_pln}" />
								<div class="item_day_sales">
									<div class="col">
										${dayEntity.day}일
									</div>
									<div class="col">
										<img src="/imagefile/${dayEntity.stor_path}${dayEntity.stor_file_nm}" alt="몰 로고">
									</div>
									<div class="col">
										${dayEntity.mall_cd_nm}
									</div>
									<div class="col col-4">
										<b>
											<fmt:formatNumber value="${dayEntity.day_sett_acc_pln}" pattern="#,###" />
										</b>원
									</div>
								</div>
							</c:forEach>
							<div class="month_total_sale">
								<span>합계</span>
								<b>
									<fmt:formatNumber value="${totalValue}" pattern="#,###" />원</b>
							</div>
						</div>
					</div>
				</c:forEach>
				<!-- 월별 상세 정보 End -->
			</div>
		</div>
	</div>
	<%@include file="../../widgets/banner.jsp" %>
</div>

<script src="/assets/js/fullcalendar/core/main.js" charset="euc-kr"></script>
<script src="/assets/js/fullcalendar/daygrid/main.js" charset="euc-kr"></script>
<script src="/assets/js/fullcalendar/locales-all.js"></script>
<script src="/assets/js/monthpicker/monthPicker.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/tooltip.min.js"></script>

<script>
	var _calendarData = {};

	<c:if test="${not empty custSettAccScheDayMallCalendar }">
		_calendarData =  ${custSettAccScheDayMallCalendar};
	</c:if>

		function fnGetEvents() {
			var events = [];

			if (isNull(_calendarData)) {
				return events;
			}

			var title;
			var isPastday = false;
			var isToday = false;
			var isMobile = false;
			var today = moment(Date.now()).format("YYYYMMDD");

			if (matchMedia("screen and (max-width:5000px) and (min-width:700px)").matches) {
				isMobile = false;
				data_space = "\n";
			}

			if (matchMedia("screen and (max-width:699px)").matches) {
				isMobile = true;
			}

			$.each(_calendarData, function (key) {

				if (isMobile) {
					title = this.count + "건" + " "
				} else {
					title = fnAddComma(this.payment) + "\n" + " " + this.count + "건" + " "
				}

				// 과거 데이터 여부 확인
				var day = moment(key).format("YYYYMMDD");
				if (moment.duration(moment(today).diff(day)).asDays() == 1)
					isPastday = true;
				else
					isPastday = false;

				// 오늘 정산금 여부 확인
				if (today == key)
					isToday = true;
				else
					isToday = false;

				events.push({
					'title': title,
					'description': this.desc,
					'start': moment(key, "YYYYMMDD").format("YYYY-MM-DD"),
					'isPastday': isPastday,
					'isToday': isToday
				});
			});

			return events;
		}

	function fnSetCalenderTotalMonth(calDate) {
			var result = "0" + "원";
			var yyyymm = moment(calDate).format("YYYYMM");
			var jqThis;
			$(".month_pay_item").each(function (i) {
				jqThis = $(this);
				if (jqThis.data("month") == yyyymm) {
					result = fnAddComma(jqThis.find(".price").data("price")) + "원";
					return false;
				}
			});

			$("p.money:not(.txt_payday)").text(result);
		}

	window.onbeforeunload = function (e) {
		if ($("#deviceType").val() == "PC")
			closeWS();
	};

	$(document).ready(function () {
		if ($("#deviceType").val() == "PC")
			connectWS();

		createTipList(null, 0);

		$(".status_box").css("cursor", "pointer");
		$(".status_box").click(function () {
			location.href = "/sub/my/market";
		});

		$(".calendar_guide").click(function () {
			$(".calendar_guide_pop").addClass("calendar_guide_pop_active");
		});

		$(".calendar_guide_pop_close").click(function () {
			$(".calendar_guide_pop").removeClass("calendar_guide_pop_active");
		});

		/*full calendar*/
		var calendarEl = document.getElementById('calendar');
		var calendar = new FullCalendar.Calendar(calendarEl, {
			plugins: ['dayGrid'],
			locale: 'ko',
			header: {
				left: '',
				center: 'prev title next',
				right: ''
			},
			eventBackgroundColor: 'rgba(0,0,0,0)',
			eventBorderColor: 'rgba(0,0,0,0)',
			eventTextColor: '#23262d',
			defaultView: 'dayGridMonth',
			defaultDate: fnGetToDay("YYYY-MM-DD"),
			eventRender: function (info) {
				// 어제 날짜에 정산금 데이터가 있는 경우 회색 처리
				if (info.event.extendedProps.isPastday) {
					if ($("#calendar").find(".fc-today").length > 0) {
						var todayArea = $("#calendar").find(".fc-today")[0];
						var yesterdayArea = todayArea.previousSibling;
						$(yesterdayArea).addClass("fc-past_cash");
					}
				}

				// 오늘 정산금 아이콘
				if (info.event.extendedProps.isToday) {
					if ($("#calendar").find(".fc-today").length > 1) {
						var todayTopArea = $("#calendar").find(".fc-today")[1];
						var todayIconHtml = '<span class="tick_today"></span>';
						todayTopArea.innerHTML = todayTopArea.innerHTML + todayIconHtml;
					}
				}

				var tooltip = new Tooltip(
					info.el,
					{
						title: info.event.extendedProps.description,
						placement: 'left',
						trigger: 'hover',
						html: true,
						container: 'body'
					});
			},
			'events': fnGetEvents()
		});
		calendar.render();

		// start the hack for the date picker
		$(".fc-header-toolbar .fc-center h2").attr('id', 'fc-datepicker-header').before('<input size="1" style="height: 0px; width:0px; border: 0px;" id="fc-datepicker" value="" />');
		$("#fc-datepicker").MonthPicker({
			Button: false,
			OnAfterMenuClose: function () {
				var data = $("#fc-datepicker").MonthPicker('Validate')
				if (data !== null) {
					calendar.gotoDate(data);
					fnSetCalenderTotalMonth(data);
				}
			}
		});
		$("#fc-datepicker-header").click(function () {
			$('#fc-datepicker').MonthPicker('Open');
		});

		$(".close_msg_pay").click(function () {
			$(this).parents(".msg_payday").remove();
		});

		$(".month_pay_item").click(function () {
			var month = $(this).data('month');
			$(this).parents(".month_pay_detail").removeClass("active");
			$(".month_pay_detail-" + month).addClass("active");
			$(".month_pay_detail.item .back_list").click(function () {
				$(".month_pay_detail").addClass("active");
				$(".month_pay_detail.item").removeClass("active");
			});
		});

		fnSetCalenderTotalMonth(calendar.getDate());

		// 왼쪽 버튼을 클릭하였을 경우
		$("button.fc-prev-button").click(function () {
			fnSetCalenderTotalMonth(calendar.getDate());
		});

		// 오른쪽 버튼을 클릭하였을 경우
		$("button.fc-next-button").click(function () {
			fnSetCalenderTotalMonth(calendar.getDate());
		});

		// 애큐온페이지 이동
		$(".bnr_acuon .area_bnr_acuon").click(function() {
			location.href = '/sub/pre_calculate/detail?finGoodSeqNo=19';
		});
	});
</script>
<style>
	/*paging*/
	.tui-pagination {
		padding-top: 1rem;
	}

	.tui-pagination .tui-is-selected,
	.tui-pagination strong {
		background: #a4a4a4;
		border-color: #a4a4a4;
	}

	.tui-pagination .tui-is-selected:hover {
		background: #a4a4a4;
		border-color: #a4a4a4;
	}

	.tui-pagination .tui-first-child.tui-is-selected {
		border: 1px solid #a4a4a4;
	}

	.ui-state-default,
	.ui-widget-content .ui-state-default,
	.ui-widget-header .ui-state-default,
	.ui-button,
	html .ui-button.ui-state-disabled:hover,
	html .ui-button.ui-state-disabled:active {
		border: none;
		background: none;

	}

	.ui-state-hover,
	.ui-widget-content .ui-state-hover,
	.ui-widget-header .ui-state-hover,
	.ui-state-focus,
	.ui-widget-content .ui-state-focus,
	.ui-widget-header .ui-state-focus,
	.ui-button:hover,
	.ui-button:focus {
		/* border: 1px solid #cccccc; */
		background: #598ae2;
		font-weight: normal;
		color: #2b2b2b;
		color: #fff !important;
		border-radius: 0 !important;
		margin: 0 !important;
		border: 0 !important;
	}

	.ui-state-highlight,
	.ui-widget-content .ui-state-highlight,
	.ui-widget-header .ui-state-highlight {
		background: #598ae2;
		color: #fff !important;
		border: 0 !important;
		border-radius: 0 !important;
	}

	.ui-widget-header {
		background: none !important;
		border: 0;
		border-bottom: 1px solid #eaeaea;
		padding: 0;
		margin: 0 !IMPORTANT;
		padding: 9px 0;
	}

	.month-picker-year-table .month-picker-title .ui-button {
		color: #000 !important;
		margin: 0 !important;
		padding: 0 !important;
		font-weight: normal;

	}

	.month-picker-year-table .month-picker-title .ui-button:hover {
		background: none !important;
		color: #000 !important;
		padding: 0 !important;
		margin: 0 !important;

	}

	.month-picker-month-table .ui-button {}

	.month-picker table {}

	#MonthPicker_fc-datepicker {
		background: white !important;
	}

	.ui-state-hover a,
	.ui-state-hover a:hover,
	.ui-state-hover a:link,
	.ui-state-hover a:visited,
	.ui-state-focus a,
	.ui-state-focus a:hover,
	.ui-state-focus a:link,
	.ui-state-focus a:visited,
	a.ui-button:hover,
	a.ui-button:focus {
		color: #2b2b2b;
		text-decoration: none;
	}

	.ui-visual-focus {
		box-shadow: 0 0 3px 1px rgb(94, 158, 214);
	}

	.ui-state-active,
	.ui-widget-content .ui-state-active,
	.ui-widget-header .ui-state-active,
	a.ui-button:active,
	.ui-button:active,
	.ui-button.ui-state-active:hover {
		border: 1px solid #003eff;
		background: #007fff;
		font-weight: normal;
		color: #ffffff;
	}

	.ui-icon-background,
	.ui-state-active .ui-icon-background {
		border: #003eff;
		background-color: #ffffff;
	}

	.ui-state-active a,
	.ui-state-active a:link,
	.ui-state-active a:visited {
		color: #ffffff;
		text-decoration: none;
	}
</style>
