<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<div class="container">
	<div class="member_wra">
		<div class="left_title_area">
			<div class="only1_path">
				<p>[<img src="/assets/images/member/icon_only1.png" alt="">에서 페이지가 이동되었습니다]</p>
			</div>
			<div class="left_title_item">
				<div class="step_box">
					<div class="title_txt">가입이<br>완료되었어요.</div>
					<p class="sub_title_txt">셀러봇캐시의 회원이<br>
						되어주셔서 감사합니다.</p>
				</div>
			</div>
		</div>
		<div class="right_contents_area">
			<div class="complete_box">
				<div class="complete_item">
					<img src="/assets/images/member/img_signup.png">
					<p class="complete_msg">
						<span>가입이 완료</span>되었어요!<br>
						로그인 후<br>
						<span>판매몰을 등록</span>해 볼까요?
					</p>
				</div>
			</div>
			<div class="btn_mb_group">
				<button class="btn_cicle" onclick="window.location.href='/login'">로그인<br>하기</button>
			</div>
		</div>
	</div>
</div>

<c:if test="${pageContext.request.serverName eq 'www.sellerbot.co.kr'}">
	<!-- 구글 분석기 전환페이지 설정 -->
	<script type="text/javascript">
		var _nasa = {};
		_nasa["cnv"] = wcs.cnv("2", "10"); // 전환유형, 전환가치 설정해야함. 설치매뉴얼 참고
	</script>

	<!-- 페이스북 전환페이지 설정 -->
	<script>
		fbq('track', 'CompleteRegistration');	
	</script>

	<!-- 구글 ADS 전환페이지 설정 -->
	<script>
		gtag('event', 'conversion', { 'send_to': 'AW-691347026/DEKjCK_f2LYBENK81MkC' });
	</script>

	<!-- 네이버 전환페이지 설정 -->
	<script type="text/javascript"> 
		var _nasa={};
		_nasa["cnv"] = wcs.cnv("2","1"); // 전환유형, 전환가치 설정해야함. 설치매뉴얼 참고
	</script>
</c:if>