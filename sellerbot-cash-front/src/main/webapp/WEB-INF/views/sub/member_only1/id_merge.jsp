<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
    
<div class="container">
    <div class="member_wra">
        <div class="left_title_area">
            <div class="left_title_item">
                <div class="step_box">
                    <div class="title_txt">셀러봇캐시<br>아이디 통합하기</div>
                    <p class="sub_title_txt">이용하고 계신 서비스의 아이디를<br>셀러봇캐시 아이디로 통합할 수 있습니다.</p>
                    <!-- <p class="sub_title_txt sub_title_txt_2 cs-info-none">고객센터 : 1666-8216<br>s-cash@only1fs.com</p> -->
                </div>
            </div>
        </div>
        <div class="right_contents_area">
            <div class="join_section">
                <form>
                    <div class="member_right_section">
                        <div class="id_merge_con">
                            <h1 class="id_merge_con_text">
                                셀러봇캐시 통합아이디를 통해 다양한<br />
                                서비스(<span class="id_merge_con_text_2_color">소드원, 셀러봇캐시</span>)를 편리하게 이용하실 수 있습니다.
                            </h1>
                            <div class="id_merge_board">
                            <ul class="id_merge_board_con">
                                <li class="id_merge_board_title">
                                    <ul class="id_merge_board_con_inner_wrap">
                                        <li style="display: none;">서비스</li>
                                        <li>아이디</li>
                                        <li>사업자번호</li>
                                    </ul>
                                </li>
                            <%-- <c:forEach items="${list}" var="value" varStatus="status">
                                <li>
                                    <ul class="id_merge_board_con_inner_wrap">
                                        <li>소드원</li>
                                        <li>${value.cust_id}</li>
                                        <li>${value.biz_no}</li>
                                    </ul>
                                </li>
                            </c:forEach> --%>
                            <li>
                                <ul class="id_merge_board_con_inner_wrap">
                                    <li style="display: none;">셀러봇캐시</li>
                                    <li>${extCustDTO.cust_id }</li>
                                    <li>${extCustDTO.biz_no }</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- <h1 class="id_merge_con_text_2">
                        셀러봇캐시 운영 관리자<br />
                        <b class="id_merge_con_text_2_color">hgu013@onlyonloan.co.kr</b> (1666-8216)
                    </h1> -->
                </div>
            </div>
            <div class="btn_mb_group">
                <button type="button" id="btnIntegrate" class="btn_cicle btn_cicle_merge">아이디<br>통합하기</button>
            </div>
        </form>
    </div>
    </div>
</div>
<div class="new_popup_wrap">
    <div class="display_none_back"></div>
    <!--아이디 통합 오류 팝업-->
    <div class="pop_up_1 pop_up_all">
        <h1 class="merge_popup_text_1">아이디 통합 오류</h1>
        <h1 class="merge_popup_text_2">관리자에게 문의해주시면 빠른 시일 내에 처리해드리겠습니다.</h1>
        <button type="button" class="btn_confirm_terms merge_close">닫기</button>
    </div>
</div>
</div>
<form id="fm" name="fm" action="/pub/member_only1/join_integrate" method="post">
    <input type="hidden" name="sodeOneTerm" value="${sodeOneTerm }">
    <input type="hidden" name="sellerbotTerm" value="${sellerbotTerm }">
    <input type="hidden" name="svc_biz_no" value="${svc_biz_no }">
    <input type="hidden" name="svc_cust_id" value="${svc_cust_id }">
    <input type="hidden" name="cust_id" value="${cust_id }">
</form>
<script>
    $(document).ready(function () {
        $("#btnIntegrate").on("click", function(){
            document.fm.submit();
        });
    });
</script>