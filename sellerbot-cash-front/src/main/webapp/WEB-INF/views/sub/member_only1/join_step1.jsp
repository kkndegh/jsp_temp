<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>


<form id="form" onsubmit="return false;" action="/pub/member_only1/join_step2" method="post" name="form"
	novalidate="novalidate" autocomplete="off">
	<div class="container">
		<div class="member_wra">
			<div class="left_title_area">
				<div class="only1_path">
					<p>[<img src="/assets/images/member/icon_only1.png" alt="">에서 페이지가 이동되었습니다]</p>
				</div>
				<div class="step_join_tab">
					<span class="active">01</span>
					<span>02</span>
					<span>03</span>
				</div>
				<div class="left_title_item">
					<div class="step_box">
						<div class="title_txt">환영합니다.<br>기다리고 있었어요</div>
						<p class="sub_title_txt">회원가입을 위해<br>약관동의를 먼저 진행해주세요.</p>
					</div>
				</div>
			</div>
			<div class="right_contents_area">
				<div class="join_section">
					<!--<div class="step_join_tab_txt">
                        <span class="focus">약관동의</span>
                        <span>본인확인</span>
                        <span>사업자정보</span>
                        <span>사업자 인적정보</span>
                    </div>-->
					<div class="member_right_section">
						<div class="member_frm_box">
							<div class="use_agree">
								<div class="agree_all">
									<input type="checkbox" class="chkbox" id="joinAll2">
									<label for="joinAll2">셀러봇캐시 전체 약관동의</label>
								</div>
								<c:forEach var="titem" items="${sellerbotTerms}">
									<c:set var="termsId" value="${titem.terms_id}" />
									<div class="item_agree_join">
										<input type="checkbox"
											class="chkbox ${titem.terms_esse_yn == 'Y' ? 'required' : ''}"
											id="joinAgree${termsId}" name="sellerbotTerm" value="${termsId}"
											term_type_cd="${titem.terms_typ_cd}"
										>
										<label for="joinAgree${termsId}">
											[${titem.terms_esse_yn == 'Y' ? '필수':'선택' }]
											<c:out value="${titem.terms_title}" />
										</label>
										<a href="#" onclick="link_agree('${titem.svc_id}${titem.terms_id}');"
											termid="${termsId}" class="link_agree">자세히보기</a>
									</div>
									<c:if test="${titem.terms_typ_cd == 'SMT'}">
                                        <c:forEach var="smtTermsItem" items="${smtTermsList}">
                                            <div class="item_agree_smt" style="padding-left: 3.5em">
                                                <input type="checkbox" class="chkbox" id="smtTermsAgree${smtTermsItem.com_cd}"
                                                    name="smtTerm"
                                                    value="${smtTermsItem.com_cd}"
                                                >
                                                <label for="smtTermsAgree${smtTermsItem.com_cd}">
                                                    <c:out value="${smtTermsItem.cd_nm}" />
                                                </label>
                                            </div>
                                        </c:forEach>
                                    </c:if>
								</c:forEach>
							</div>
							<div class="use_agree">
								<div class="agree_all">
									<input type="checkbox" class="chkbox" id="joinAll">
									<label for="joinAll">소드원 전체 약관동의</label>
								</div>
								<c:forEach var="titem" items="${sodeOneTerms}">
									<c:set var="termsId" value="${titem.terms_id}" />
									<div class="item_agree_join">
										<input type="checkbox"
											class="chkbox ${titem.terms_esse_yn == 'Y' ? 'required' : ''}"
											id="joinAgree${termsId}" name="sodeOneTerm" value="${termsId}">
										<label for="joinAgree${termsId}">
											[${titem.terms_esse_yn == 'Y' ? '필수':'선택' }]
											<c:out value="${titem.terms_title}" />
										</label>
										<a href="#" onclick="link_agree('${titem.svc_id}${titem.terms_id}');"
											termid="${termsId}" class="link_agree">자세히보기</a>
									</div>
								</c:forEach>
							</div>
						</div>
					</div>
					<div class="btn_mb_group">
						<button class="btn_cicle" onclick="step2()">다음</button>
					</div>
				</div>
			</div>
		</div>


		<!-- 약관 정보 -->
		<!--팝업-->
		<div class="new_popup_wrap">
			<div class="display_none_back"></div>

			<c:forEach var="titem" items="${sellerbotTerms}">
				<div id="termpopup${titem.terms_id}" class="pop_up_1 pop_up_all pop_up_font-size">
					<div class="popup_top_title">
						<h1>
							[${titem.terms_esse_yn == 'Y' ? '필수':'선택' }]
							<c:out value="${titem.terms_title }" />
						</h1>
						<div class="add_pooup_close">
							<div></div>
							<div></div>
						</div>
					</div>
					<h1 class="popup_title_2">
						<c:choose>
							<c:when test="${titem.terms_esse_yn eq 'Y'}">
								서비스를 이용하시려면 약관동의가 필요합니다.
							</c:when>
							<c:otherwise>
								다음의 약관은 선택사항입니다.
							</c:otherwise>
						</c:choose>
					</h1>
					<div class="pop_up_text_wrap">
						${sf:textToHtml(titem.terms_cont)}
					</div>
					<button type="button" class="btn_confirm_terms join_step_popup_close">약관확인</button>
				</div>
			</c:forEach>

			<c:forEach var="titem" items="${sodeOneTerms}">
				<div id="termpopup${titem.terms_id}" class="pop_up_1 pop_up_all pop_up_font-size">
					<div class="popup_top_title">
						<h1>
							[${titem.terms_esse_yn == 'Y' ? '필수':'선택' }]
							<c:out value="${titem.terms_title }" />
						</h1>
						<div class="add_pooup_close">
							<div></div>
							<div></div>
						</div>
					</div>
					<h1 class="popup_title_2">
						<c:choose>
							<c:when test="${titem.terms_esse_yn eq 'Y'}">
								서비스를 이용하시려면 약관동의가 필요합니다.
							</c:when>
							<c:otherwise>
								다음의 약관은 선택사항입니다.
							</c:otherwise>
						</c:choose>
					</h1>
					<div class="pop_up_text_wrap">
						${sf:textToHtml(titem.terms_cont)}
					</div>
					<button type="button" class="btn_confirm_terms join_step_popup_close">약관확인</button>
				</div>
			</c:forEach>
		</div>
		<!-- 약관 정보 종료 -->
		<!--팝업 종료-->

	</div>
	<input type="hidden" id="svc_cust_id" name="svc_cust_id" value="${svc_cust_id }" />
	<input type="hidden" id="svc_biz_no" name="svc_biz_no" value="${svc_biz_no}" />
	<input type="hidden" id="svc_id" name="svc_id" value="${svc_id}" />
	<input type="hidden" id="step" name="step" value="join_step1" />

</form>
<script>

	$(document).ready(function () {

		$(".agree_all .chkbox").click(function () {
			var allchk = $(this);
			if (allchk.is(":checked") == true) {
				allchk.parents(".use_agree").find(".item_agree_join .chkbox").prop("checked", true);
				allchk.parents(".use_agree").find(".item_agree_smt .chkbox").prop("checked", true);
			} else {
				allchk.parents(".use_agree").find(".item_agree_join .chkbox").prop("checked", false);
				allchk.parents(".use_agree").find(".item_agree_smt .chkbox").prop("checked", false);
			}
		});
		$(".item_agree_join .chkbox, .item_agree_smt .chkbox").click(function () {
			var chkThis = $(this);
			var itemAllCnt = chkThis.parents(".use_agree").find(".item_agree_join").length;
            var smtItemAllCnt = chkThis.parents(".use_agree").find(".item_agree_smt").length;

			itemAllCnt = itemAllCnt + smtItemAllCnt;

			// 전체 약관 선택시 셀러봇캐시 전체동의 체크 여부 표시
            var itemChk = chkThis.parents(".use_agree").find(".item_agree_join .chkbox:checked").length;
            var smtItemChk = chkThis.parents(".use_agree").find(".item_agree_smt .chkbox:checked").length;
            itemChk = itemChk + smtItemChk;

			if (itemAllCnt == itemChk) {
                chkThis.parents(".use_agree").find(".agree_all .chkbox").prop("checked", true);
            } else {
                chkThis.parents(".use_agree").find(".agree_all .chkbox").prop("checked", false);
            }

            // 셀러봇캐시 마케팅 활용 항목 전체 동의시 항목 전체 체크 여부 표시
            if (chkThis.attr("term_type_cd") == 'SMT') {
                if (chkThis.is(":checked") == true) {
                    chkThis.parents(".use_agree").find(".item_agree_smt .chkbox").prop("checked", true);
                } else {
                    chkThis.parents(".use_agree").find(".item_agree_smt .chkbox").prop("checked", false);
                }
            } else {
                if (smtItemChk > 0) {
                    chkThis.parents(".use_agree").find(".item_agree_join").children("[term_type_cd=SMT]").prop("checked", true);
                } else {
                    chkThis.parents(".use_agree").find(".item_agree_join").children("[term_type_cd=SMT]").prop("checked", false);
                }
            }
		});

		// 공통 script.js 이벤트 삭제
		$(".item_agree_join .link_agree").off("click");
		// 각각 약관 마다 모달 호출
		$(".item_agree_join .link_agree").click(function () {
			var termid = $(this).attr("termid");
			$(".new_popup_wrap").css("display", "flex");
			$(".display_none_back").css("display", "block");
			$("#termpopup" + termid).show();
			return false;
		});

		// 약관 닫기
		$(".join_step_popup_close").click(function () {
			$(".new_popup_wrap").css("display", "none");
			$(".pop_up_all").css("display", "none");
			$(".display_none_back").css("display", "none");
		});
		$(".add_pooup_close").click(function () {
			$(".new_popup_wrap").css("display", "none");
			$(".pop_up_all").css("display", "none");
			$(".display_none_back").css("display", "none");
		});

	});

	function link_agree(cls) {
		$("." + cls).show();
		$(".btn_confirm_terms").click(function () {
			$("." + cls).hide();
		});
	}


	function step2() {
		var joinAgreeCheck = false;
		$(".required").each(function (i, v) {
			if (!$(v).is(":checked")) {
				joinAgreeCheck = true;
			}
		});

		if (joinAgreeCheck) {
			showAlert("필수 약관을 선택해주세요.");
			return;
		}
		document.form.submit();
	}
</script>