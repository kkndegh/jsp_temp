<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<form onsubmit="return false;" id="form1" name="form1" method="post" autocomplete="off" novalidate="novalidate">
	<div class="container">
		<div class="member_wra">
			<div class="left_title_area">
				<div class="only1_path">
					<p>[<img src="/assets/images/member/icon_only1.png" alt="">에서 페이지가 이동되었습니다]</p>
				</div>
				<div class="step_join_tab">
					<span>01</span>
					<span class="active">02</span>
					<span>03</span>
				</div>
				<div class="left_title_item">
					<div class="step_box">
						<div class="title_txt">가입을 위해<br>본인확인이<br>필요해요.</div>
						<p class="sub_title_txt">
							셀러봇캐시 통합가입을 위해<br>이메일 본인인증이 필요합니다.
						</p>
					</div>
				</div>
			</div>
			<div class="right_contents_area">
				<div class="join_section">
					<div class="member_right_section">
						<div class="member_frm_box">
							<div class="item_mb">
								<span class="lb">이메일(ID)</span>
								<div class="input_box">
									<input type="text" class="textbox_mb" value="${svc_cust_id }" id="cust_id"
										name="cust_id" ${not empty svc_cust_id ? 'readonly' : '' }
										autocomplete="new-cust_id" required="required" data-valitype="email">
									<p id="cust_id_error" class="error">*이메일(ID@E-mail.com)을 정확히 입력해주세요.</p>
								</div>
							</div>
							<div class="item_mb last item-auth-no">
								<span class="lb">인증번호</span>
								<div class="input_box">
									<input type="text" class="textbox_mb" id="auth_no" name="auth_no"
										required="required" data-valitype="number">
									<p id="auth_no_error" class="error">*인증번호를 입력해주세요.</p>
									<div class="alert_send" style="display: none">입력해주신 이메일(<span
											class="data_email"></span>)로 인증번호가 <sapn id="sendTxt">전송</sapn>되었습니다.</div>
								</div>
							</div>
							<div class="find_mb">
								<a href="/pub/member/find_id" target="_blank">아이디</a>
								<span class="bar">|</span>
								<a href="/pub/member/find_pw" target="_blank">비밀번호</a>
								<span>찾기</span>
							</div>

						</div>
					</div>
					<div class="btn_mb_group">
						<button class="btn_cicle" onclick="step3()">다음</button>
						<button id="btnSend" type="button" class="btn_cicle btn_gray"
							onclick="idCheck(this)">인증번호<br>발송</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="${svc_biz_no }" name="svc_biz_no" />
	<input type="hidden" id="auth_token" name="auth_token" />
	<input type="hidden" value="${sodeOneTerm }" name="sodeOneTerm" />
	<input type="hidden" value="${sellerbotTerm }" name="sellerbotTerm" />
	<input type="hidden" value="${smtTerm }" name="smtTerm" />
	<input type="hidden" value="${svc_cust_id }" name="svc_cust_id" />
</form>
<script>

	$(document).ready(function () {

		$('input[type="text"]').keydown(function (e) {
			if (e.keyCode === 13) {
				e.preventDefault();

				if (nonNull($("#cust_id").val()) && nonNull($("#auth_no").val()) && nonNull($("#auth_token").val())) {
					step3();
				} else {
					idCheck(this);
				}

			}
		});

	});

	//인증 번호 발송전 validation 체크
	function isValiSendNum() {
		$(".error").hide();
		if (!isEmail($("#cust_id").val())) {
			$("#cust_id_error").show();
			$("#cust_id").focus();
			return false;
		}
		return true;
	}

	// 다음 단계 이동
	function step3() {

		if (!isVali()) { return false; }
		if (isNull($("#auth_token").val())) {
			$(".data_email").text($("#cust_id").val());
			$("#auth_no").focus();
			return false;
		}

		$.ajax({
			url: '/pub/member_only1/confirmCust',
			data: {
				cust_id: $("#cust_id").val(),
				authNo: $("#auth_no").val(),
				auth_token: $("#auth_token").val(),
				password: $("#password").val(),
				auth_type: $("#auth_type").val()
			},
			async: true,
			type: 'POST',
			success: function (data) {
				$("#form1").attr("action", "/pub/member_only1/join_step3");
				document.form1.submit();
			},
			error: function (response, status, error) {

				// 인증 번호 오류
				if (response.status == 400) {
					showAlert("인증번호가 틀립니다.");
					return false;
				} else if (response.status == 4490) {
					// 인증 번호 발송을 요청 하지 않은 상태 
					showAlert("인증번호 발송을 먼저 하세요.", function () {
						$("#auth_no").focus();
					});
					return false;
				}

				showAlert("Error: " + response.status);
			}
		}); //
	}

	// 인증 번호 전송
	function idCheck(ele) {

		if (!isValiSendNum()) {
			return false;
		}

		var ajaxParam = { cust_id: $("#cust_id").val(), "svc_id": "sodeone" };

		$.ajax({
			url: '/pub/member_only1/join_id_check',
			data: ajaxParam,
			async: false,
			type: 'POST',
			success: function (data) {
				if (nonNull($(".data_email").text())) { $("#sendTxt").text("재전송"); }
				$(".data_email").text($("#cust_id").val());
				$("#auth_token").val(data);
				$("#btnSend").html("인증번호<br>재발송");
				$(".alert_send").show();
				$("#auth_no").focus();
			},
			error: function (response, status, error) {

				if (response.status == 400) {
					var modalId = showConfirm("통합계정입니다.<br>비밀번호 찾기를 진행하시겠습니까?", function () {
						location.href = "/pub/member/find_pw?" + $.param(ajaxParam);
					});
				} else if (isNull('${svc_cust_id}') && response.status == 409) {
					// svc_cust_id not null and cust_id duplication
					// 아이디가 중복되었습니다. (아이디 재입력, 셀러봇캐시 가입확인)
					var modalId = showConfirm("해당 이메일(ID)는 셀러봇캐시에 가입정보가 존재합니다.<br>통합회원 가입을 진행하시겠습니까?", function () {
						var fm = document.form1;
						fm.action = "/pub/member_only1/cash_cust_confirm";
						fm.submit();
					});
				} else if (nonNull('${svc_cust_id}') && response.status == 409) {
					// svc_cust_id not null and cust_id duplication
					// 통합가입 대상
					showAlert("해당 이메일(ID)는 소드원과 셀러봇캐시에<br>이미 가입이 되어있습니다.<br>셀러봇캐시 회원 통합을 진행합니다.", function () {
						$("#form1").attr("action", "/pub/member_only1/cash_cust_confirm");
						document.form1.submit();
					});
				} else {
					alert("Error: " + response.status);
				}
			}
		});
	}

</script>