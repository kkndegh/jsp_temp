<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>

<form id="form" onsubmit="return false;" action="/pub/member_only1/join_step4" method="post" name="form">
	<div class="container">
		<div class="member_wra">
			<div class="left_title_area">
				<div class="only1_path">
					<p>[<img src="/assets/images/member/icon_only1.png" alt="">에서 페이지가 이동되었습니다]</p>
				</div>
				<div class="step_join_tab d-none">
					<span>01</span>
					<span>02</span>
					<span class="active">03</span>
				</div>
				<div class="left_title_item">
					<div class="step_box">
						<div class="title_txt">셀러봇캐시 통합을<br>위해 계정 인증이 필요합니다.</div>
						<p class="sub_title_txt">셀러봇캐시 통합 계정으로<br>소드원 서비스를 이용하실 수 있습니다.</p>
					</div>
				</div>
			</div>
			<div class="right_contents_area">
				<div class="join_section">
					<div class="member_right_section">

						<div class="member_frm_box">
							<div class="item_mb">
								<span class="lb">이메일(ID)</span>
								<div class="input_box">
									<input type="text" class="textbox_mb" placeholder="셀러봇캐시 이메일아이디를 입력해주세요."
										id="cust_id" name="cust_id" value="${cust_id }" readonly="readonly"
										required="required" data-valitype="email" />
									<p id="cust_id_error" class="error">*이메일(ID@E-mail.com)을 정확히 입력해주세요.</p>
								</div>
							</div>
							<div class="item_mb last">
								<span class="lb">비밀번호</span>
								<div class="input_box">
									<input type="password" class="textbox_mb" placeholder="비밀번호를 입력해주세요." id="password"
										name="password" autocomplete="new-password" required="required" />
									<p id="password_error" class="error">*비밀번호를 입력해주세요.</p>
								</div>
							</div>
						</div>

					</div>
					<div class="btn_mb_group">
						<button class="btn_cicle" onclick="step4()">다음</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="${svc_biz_no }" name="svc_biz_no" />
	<input type="hidden" value="${cust_id }" name="svc_cust_id" />
	<input type="hidden" value="${sodeOneTerm }" name="sodeOneTerm" />
	<input type="hidden" value="${sellerbotTerm }" name="sellerbotTerm" />
</form>
<script>

	$(document).ready(function () {

		$('input[type="text"], input[type="password"]').keydown(function (e) {
			if (e.keyCode === 13) {
				e.preventDefault();
				step4();
			}
		});

	});


	function step4() {

		if (!isVali()) { return false; }

		$.ajax({
			url: '/pub/member_only1/seller_join_check',
			data: { password: $("#password").val(), cust_id: $("#cust_id").val() },
			async: false,
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				if (data.result) {
					document.form.submit();
				} else {
					var errorCode = data.errorCode;
					if (errorCode == "AUTH-4303") {
						showAlert("해당 사용자가 존재하지 않습니다.");
					} else if (errorCode == "AUTH-4304") {
						showAlert("아이디나 비밀번호를 다시 확인해주세요.");
					} else {
						showAlert("로그인 에러");
					}
				}
			},
			error: function (response, status, error) {
				showAlert("서버 에러.");
			}
		});
	}
</script>