<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<form id="form" onsubmit="return false;" action="/pub/member_only1/svc_cust_confirm" method="post" name="form"
	autocomplete="off" novalidate="novalidate">
	<div class="container">
		<div class="member_wra">
			<div class="left_title_area">
				<div class="only1_path">
					<p>[<img src="/assets/images/member/icon_only1.png" alt="">에서 페이지가 이동되었습니다]</p>
				</div>
				<div class="step_join_tab d-none">
					<span>01</span>
					<span class="active">02</span>
					<span>03</span>
				</div>
				<div class="left_title_item">
					<div class="step_box">
						<div class="title_txt">소드원 가입여부를<br>조회합니다.</div>
						<p class="sub_title_txt">소드원 계정이 있으시다면 <br>셀러봇캐시 통합아이디로 이용하실 수 있습니다. <br>없으시다면 건너뛰기를 눌러주세요.</p>
					</div>
				</div>
			</div>
			<div class="right_contents_area">
				<div class="join_section">
					<div class="member_right_section">

						<div class="member_frm_box">
							<div class="item_mb">
								<span class="lb">소드원 이메일(ID)</span>
								<div class="input_box">
									<input type="text" class="textbox_mb" placeholder="소드원 이메일(ID)를 입력해주세요."
										id="req_cust_id" name="req_cust_id" autocomplete="new-req-cust-id"
										required="required" data-valitype="email" />
									<p id="req_cust_id_error" class="error">*이메일(ID@E-mail.com)을 정확히 입력해주세요.</p>
								</div>
							</div>
							<div class="item_mb last">
								<span class="lb">비밀번호</span>
								<div class="input_box">
									<input type="password" class="textbox_mb" placeholder="비밀번호를 입력해주세요." id="password"
										name="password" autocomplete="new-password" required="required" />
									<p id="password_error" class="error">*비밀번호를 입력해주세요.</p>
								</div>
							</div>
						</div>

					</div>
					<div class="btn_mb_group">
						<button class="btn_cicle btn_gray" onclick="skip()">건너뛰기</button>
						<button class="btn_cicle" onclick="step3()">다음</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" value="${sodeOneTerm }" name="sodeOneTerm" />
	<input type="hidden" value="${sellerbotTerm }" name="sellerbotTerm" />
	<input type="hidden" value="${smtTerm }" name="smtTerm" />
	<input type="hidden" id="step" name="step" value="pre_join_step2" />
	<input type="hidden" id="svc_id" name="svc_id" value="${svc_id}" />
	<div class="popup popup_certify">
		<div class="pop">
			<span class="close_pop"><img src="/assets/images/member/close_gray.png" alt=""></span>
			<div class="pop_body">
				<p class="desc_pop">
					입력한 정보를 확인하세요.<br />
					아이디가 존재하지 않습니다.
				</p>
			</div>
			<div class="pop_footer">
				<button type="button" class="close_btn">확인</button>
			</div>
		</div>
	</div>
</form>
<script>

	$(document).ready(function () {

		$('input[type="text"], input[type="password"]').keydown(function (e) {
			if (e.keyCode === 13) {
				e.preventDefault();
				step3();
			}
		});

	});


	function skip() {


		var opt = {
			btnOK: "가입하기",
			btnCancel: "취소"
		};

		showConfirm("소드원 회원 정보가 확인되지 않습니다.<br>셀러봇캐시 통합회원 가입을 하시겠습니까?", function () {
			$("#form").attr("action", "/pub/member_only1/join_step2");
			document.form.submit();
		}, opt);

	}

	function step3() {

		if (!isVali()) { return false; }

		if (isNull($("#req_cust_id").val())) {
			showAlert("소드원 이메일(ID)를 입력해주세요.");
			return false;
		}

		if (!isEmail($("#req_cust_id").val())) {
			showAlert("이메일 형식이 아닙니다.");
			return false;
		}

		if (isNull($("#password").val())) {
			showAlert("비밀번호를 입력해주세요.");
			return false;
		}


		$.ajax({
			url: '/pub/member_only1/service_join_check',
			data: { password: $("#password").val(), svc_cust_id: $("#req_cust_id").val() },
			async: false,
			type: 'POST',
			dataType: 'json',
			success: function (data) {

				if (data.sodeone_member) {
					showConfirm("소드원 회원 정보가 존재합니다.<br>셀러봇캐시 계정으로 통합하시겠습니까?", function () {
						$("#form").attr("action", "/pub/member_only1/svc_cust_confirm");
						document.form.submit();
					}, {
						btnOK: "통합하기",
						btnCancel: "취소"
					});
				} else if (!data.sodeone_member) {
					showConfirm("소드원에 가입된 정보가 없습니다. <br> 신규 가입으로 진행하시겠습니까?", function () {
						$("#form").attr("action", "/pub/member_only1/svc_cust_confirm");
						document.form.submit();
					});
				}
			},
			error: function (response, status, error) {
				var error = response.responseJSON.error;
				if (response.status == 403) {
					if (error.code == "AUTH-4302") {

						var modalId = showConfirm("회원정보가 존재하지 않습니다.", function () {
							removeModal(modalId);
							$("#form").attr("action", "/pub/member_only1/join_step2");
							document.form.submit();
						}, {
							btnOK: "건너뛰기",
							btnCancel: "확인"
						}
						);

					} else if (error.code == "AUTH-4303") {
						var modalId = showConfirm("해당 이메일(ID)는 소드원의 계정이 아닙니다.<br>소드원 계정이 없으시다면 건너뛰기를 눌러주세요.", function () {
							removeModal(modalId);
							$("#form").attr("action", "/pub/member_only1/join_step2");
							document.form.submit();
						}, {
							btnOK: "건너뛰기",
							btnCancel: "확인"
						}
						);
					}


				} else {
					showAlert(error.comment);
				}
			}
		});
	}
</script>