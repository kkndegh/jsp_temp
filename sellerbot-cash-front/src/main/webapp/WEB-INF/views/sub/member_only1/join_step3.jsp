<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"%>
<div class="container">
	<div class="member_wra">
		<div class="left_title_area">
			<div class="only1_path">
				<p>[<img src="/assets/images/member/icon_only1.png" alt="">에서 페이지가 이동되었습니다]</p>
			</div>
			<div class="step_join_tab">
				<span>01</span>
				<span>02</span>
				<span class="active">03</span>
			</div>
			<div class="left_title_item">
				<div class="step_box">
					<div class="title_txt">사업자정보가<br>필요해요</div>
					<p class="sub_title_txt">현재 운영하고 있는<br>사업자정보를 입력해주세요.</p>
				</div>
			</div>
		</div>
		<div class="right_contents_area">
			<div class="join_section">
				<form name="fm" action="/pub/member_only1/join_step_end" onsubmit="return false;" method="post"
					novalidate="novalidate" autocomplete="off">
					<div class="member_right_section">
						<div class="member_frm_box mb_company scrollblack">
							<div class="item_mb">
								<span class="lb">이메일(ID)</span>
								<div class="input_box">
									<input type="text" class="textbox_mb" value="${cust_id}" id="cust_id" name="cust_id"
										readonly="readonly">
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">비밀번호</span>
								<div class="input_box">
									<input type="password" id="passwd" name="passwd" class="textbox_mb" maxlength="20"
										autocomplete="new-password" required="required" data-valitype="password">
									<p class="desc_mb">
										영문, 숫자, 특수문자를 조합하여 8자 이상 ~ 20자 이내로 입력해야 합니다.<br>( 사용가능한 특수문자 : !@#$%^&*+=-[] )
									</p>
									<p id="passwd_rror" class="error">
										*비밀번호 형식이 맞지 않습니다.
									</p>
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">비밀번호 확인</span>
								<div class="input_box">
									<input type="password" id="passwd_same" name="passwd_same" class="textbox_mb"
										maxlength="20" required="required" data-valitype="equalsTo" for="passwd"
										autocomplete="new-re-password">
									<p id="passwd_same_error" class="error">*입력하신 비밀번호가 맞지 않습니다.</p>
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">상호명</span>
								<div class="input_box">
									<input type="text" id="biz_nm" name="biz_nm" class="textbox_mb" value=""
										required="required">
									<p id="biz_nm_error" class="error">*상호명을 입력하여 주세요.</p>
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">사업자 등록번호</span>
								<div class="input_box">
									<input type="text" id="biz_no" name="biz_no" class="textbox_mb" required="required"
										data-valitype="bizNo" maxlength="10">
									<button id="checkBizNo" class="confirm_btn" type="button">중복확인</button>
									<p class="desc_mb">('-'없이 숫자입력)</p>
									<p id="biz_no_error" class="error">*사업자 등록번호를 정확히 입력하여 주세요.</p>
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">대표자명</span>
								<div class="input_box">
									<input type="text" id="ceo_nm" name="ceo_nm" class="textbox_mb" required="required">
									<p id="ceo_nm_error" class="error">*대표자명을 정확히 입력해주세요.</p>
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">대표자 연락처</span>
								<div class="input_box">
									<select class="sctbox sctbox_tel" id="ceo_no_svc" name="ceo_no_svc">
										<option value="010">010</option>
										<option value="011">011</option>
										<option value="016">016</option>
										<option value="017">017</option>
										<option value="018">018</option>
										<option value="019">019</option>
									</select>
									<input type="text" id="ceo_no" name="ceo_no" class="textbox_mb textbox_mb_tel"
										required="required" data-valitype="mobile" for="ceo_no_svc" maxlength="8">
									<p class="desc_mb">('-'없이 숫자입력)</p>
									<p id="ceo_no_error" class="error">*대표자 연락처를 정확히 입력해주세요.</p>
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">담당자명<br>(선택)</span>
								<div class="input_box">
									<input type="text" id="chrg_nm" name="chrg_nm" class="textbox_mb">
									<p id="chrg_nm_error" class="error">*담당자명을 입력하세요.</p>
								</div>
							</div>
							<div class="item_mb">
								<span class="lb">담당자 연락처<br>(선택)</span>
								<div class="input_box">
									<select class="sctbox sctbox_tel" id="charg_no_svc" name="charg_no_svc">
										<option value="010">010</option>
										<option value="011">011</option>
										<option value="016">016</option>
										<option value="017">017</option>
										<option value="018">018</option>
										<option value="019">019</option>
									</select>
									<input type="text" id="chrg_no" name="chrg_no" class="textbox_mb textbox_mb_tel"
										maxlength="8">
									<p class="desc_mb">('-'없이 숫자입력)</p>
									<p id="chrg_no_error" class="error">*담당자 연락처를 정확히 입력해주세요</p>
								</div>
							</div>
							<div class="item_mb line2">
								<span class="lb">유입경로</span>
								<div class="input_box">
									<select class="sctbox" id="inf_path_cd" name="inf_path_cd" required="required">
										<option value="">경로선택</option>
										<c:forEach items="${codeList }" var="code" varStatus="state">
											<option value="${code.com_cd}">${code.cd_nm}</option>
										</c:forEach>
									</select>
									<p id="inf_path_cd_error" class="error">*유입경로를 선택해주세요.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="btn_mb_group">
						<button class="btn_cicle">다음</button>
						<button id="findIdBtn" type="button" class="btn_cicle btn_gray">ID찾기</button>
					</div>
					<input type="hidden" value="${sodeOneTerm }" name="sodeOneTerm" />
					<input type="hidden" value="${sellerbotTerm }" name="sellerbotTerm" />
					<input type="hidden" value="${smtTerm }" name="smtTerm" />
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	var checkedBizNo = false;

	$(document).ready(function () {


		setInputElement($("input[name='biz_nm']"), '${cust_info.biz_nm}'); // 상호명
		setInputElement($("input[name='biz_no']"), '${cust_info.biz_no}'); // 사업자 번호
		setInputElement($("input[name='ceo_nm']"), '${cust_info.ceo_nm}'); // 대표자 이름
		setInputElement($("input[name='chrg_nm']"), '${cust_info.chrg_nm}'); // 담당자 이름

		var ceo_no = '${cust_info.ceo_no}';
		var chrg_no = '${cust_info.chrg_no}';

		$("#biz_no").val('${cust_info.biz_no}');

		if (nonNull(ceo_no) && ceo_no.length > 9) {
			$("#ceo_no_svc").val(ceo_no.substring(0, 3));
			$("#ceo_no").val(ceo_no.substring(3));
		}
		if (nonNull(chrg_no) && chrg_no.length > 9) {
			$("#charg_no_svc").val(chrg_no.substring(0, 3));
			$("#chrg_no").val(chrg_no.substring(3));
		}

		$("#passwd").keyup(function (evt) { $("#passwd_error").hide(); });
		$("#passwd_same").keyup(function (evt) { $("#passwd_same_error").hide(); });
		$("#ceo_no").keyup(function (evt) { $("#ceo_no_error").hide() });
		$("#chrg_nm").keyup(function (evt) { $("#chrg_nm_error").hide() });
		$("#chrg_no").keyup(function (evt) { $("#chrg_no_error").hide() });
		$("#inf_path_cd").change(function (evt) { $("#inf_path_cd_error").hide() });

		$("#biz_no").keyup(function () { $("#biz_no_error").hide(); });

		$("#checkBizNo").click(function (evt) {
			evt.preventDefault();

			if ($(this).hasClass("disabled")) return false;

			var bizNo = $("#biz_no").val();
			bizNo = (bizNo).replace(/\D/g, '');
			$("#biz_no").val(bizNo);
			if (bizNo.length == 0) {
				$("#biz_no_error").html("*사업자 번호를 입력하세요.");
				$("#biz_no_error").show();
			} else if (validateBizNo(bizNo)) {
				$.post("/pub/member_only1/checkBizNo", { bizNo: bizNo }, function (data, status) {
					checkedBizNo = true;
					$("#biz_no_error").hide();
					showAlert("사용가능한 사업자 번호 입니다.");
					$("#biz_no").prop("readonly", true);
					$("#checkBizNo").addClass("disabled");
				})
					.fail(function (response) {
						checkedBizNo = false;
						if (response.status == 410) { // 가입용 session 정보가 없을 경우 발생됨 안내가 필요 할 수도 있음.
							location.reload();
						} else if (response.status == 409) { // 사업자 번호 확인 오류
							if("AUTH-4003" == response.responseText) {
								$("#biz_no_error").html("*사업자 번호의 ID가 존재합니다. <span>ID찾기</span>를 이용해주세요.");
								$("#biz_no_error").show();
							} else if ("AUTH-4307" == response.responseText) {
								// $("#biz_no_error").html("*탈퇴한 회원입니다. 고객센터로 문의해주세요. 1666-8216");
								$("#biz_no_error").html("*탈퇴한 회원입니다. <a href='https://9p9j3.channel.io/lounge' target='_blank' style='color: blue'>챗봇으로 문의하기</a>");
								$("#biz_no_error").show();
							} else {
								$("#biz_no_error").html("*" + response.responseText);
								$("#biz_no_error").show();
							}
						} else {
							showAlert("Error: " + response.status);
						}
					});
			} else {
				$("#biz_no_error").html("*" + "사업자 번호가 정확하지 않습니다.");
				$("#biz_no_error").show();
			}
		});

		$("#findIdBtn").click(function (evt) {
			var ok = confirm("정말 아이디 찾기로 이동 하시겠습니까?");
			if (ok) {
				window.location.href = "/pub/member/find_id";
			}
		});


		$(".btn_cicle").on("click", function () {

			if (!isVali()) { return false; }

			if (!$("#biz_no").prop("readonly")) {
				$("#biz_no_error").text("사업자 번호 중복 여부를 확인하여 주세요.");
				$("#biz_no").focus();
				return false;
			}

			if (isNull($("#chrg_nm").val()) && nonNull($("#chrg_no").val())) {
				$("#chrg_nm_error").show();
				$("#chrg_nm").focus();
				return false;
			}

			if (isNull($("#chrg_no").val()) && nonNull($("#chrg_nm").val())) {
				$("#chrg_no_error").show();
				$("#chrg_no").focus();
				return false;
			}

			if (nonNull($("#chrg_no").val())) {
				var mobile = $("#charg_no_svc").val() + $("#chrg_no").val();
				if (!isMobile(mobile)) {
					$("#chrg_no_error").show();
					$("#chrg_no").focus();
					return false;
				}
			}

			document.fm.submit();

		});

	});

	function validateBizNo(bizID) {
		bizID = bizID + "";
		// bizID는 숫자만 10자리로 해서 문자열로 넘긴다. 
		var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
		var tmpBizID, i, chkSum = 0, c2, remander;
		bizID = bizID.replace(/-/gi, '');

		for (i = 0; i <= 7; i++) chkSum += checkID[i] * bizID.charAt(i);
		c2 = "0" + (checkID[8] * bizID.charAt(8));
		c2 = c2.substring(c2.length - 2, c2.length);
		chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
		remander = (10 - (chkSum % 10)) % 10;

		if (Math.floor(bizID.charAt(9)) == remander) return true; // OK! 
		return false;
	}

	function setInputElement(jqObj, val) {
		jqObj.val(val);
		if (nonNull(val)) { jqObj.prop("readonly", true); }
	}
</script>