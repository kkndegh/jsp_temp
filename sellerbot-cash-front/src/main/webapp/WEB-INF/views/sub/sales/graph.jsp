<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<!--amChart-->
<script src="/assets/amchart/core.js"></script>
<script src="/assets/amchart/charts.js"></script>
<script src="/assets/amchart/animated.js"></script>

<script src="/assets/js/Chart.bundle.min.js"></script>

<style>
    .swiper-pagination1,
    .swiper-pagination2 {
        margin: 0 auto;
        text-align: center;
        width: 100%;
        bottom: 2.5rem;
        position: absolute;
        z-index: 6;
    }

    .swiper-pagination-bullet {
        margin-right: 0.5rem;
    }

    .swiper-pagination-bullet:last-of-type {
        margin-right: 0;
    }

    .tui-datepicker-input>input {
        width: 100%;
        height: 100%;
        padding: 6px 27px 6px 10px;
        font-size: 12px;
        line-height: 14px;
        vertical-align: top;
        border: 0;
        color: #333;
        border-radius: 3rem;
    }

    .tui-datepicker-input {
        position: relative;
        display: inline-block;
        width: 120px;
        height: 2.5rem;
        vertical-align: top;
        border: 1px solid #ddd;
        border-radius: 3rem;
    }

    .datepicker-cell {
        display: inline-block;
    }

    .tui-datepicker-input.tui-has-focus {
        vertical-align: middle;
        border: 0;
    }

    .tui-datepicker {
        border: 1px solid #aaa;
        background-color: white;
        position: absolute;
        margin-left: -11rem;
        margin-top: 1rem;
        z-index: 999;
    }
	.cv-spinner {
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;  
	}
	.spinner {
		width: 40px;
		height: 40px;
		border: 4px #ddd solid;
		border-top: 4px #2e93e6 solid;
		border-radius: 50%;
		animation: sp-anime 0.8s infinite linear;
	} 
    @keyframes sp-anime {
        100% { 
            transform: rotate(360deg); 
        } 
    } 
</style>

<div class="container">
    <div class="sales_wra">
        <div class="sales_area">
        <div class="menu_top">
            <p class="menu_name">매출통계</p>
            <div class="gnb_topBox">
            <ul class="gnb_top clearfix">
                <li><a href="/sub/sales/sales">매출 현황</a></li>
                <li class="focus"><a href="/sub/sales/analysis">매출분석 그래프</a></li>
            </ul>
            </div>
        </div>
        <div class="summary_report_section_wrap">
            <div class="summary_report_section">
            <p class="title_summary open">[매출현황 요약보고]</p>
            <div class="summary_section_card">
                <div class="summary_card first" >
                <div class="title_highlight">
                    <p>당월 매출</p>
                </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                
                <div class="graph_summary type1">
                    <canvas id="spGh1" height="150"></canvas>
                </div>
<!--                 <p class="desc_summary"> -->
<!--                     19년 3월 매출은<br> -->
<!--                     전월 대비 <span class="up">15% 증가</span>하였고,<br> -->
<!--                     전년 동월대비 <span class="down">50% 감소</span>하였습니다. -->
<!--                 </p> -->
                </div>

                <!--nodata_1-->
                <div class="summary_card first first_nodata top_nodata">
                <div class="title_highlight">
                    <p class="first_nodata_title">당월 매출</p>
                </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                
                <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                </div>
                </div>

                <div class="summary_card second">
                <div class="title_highlight">
                    <p>최근 1년 매출</p>
                </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
`                
                <div class="graph_summary type2">
                    <canvas id="spGh2" height="150"></canvas>
                </div>
                <p class="desc_summary"></p>
<!--                 <p class="desc_summary">최근 1년 중 매출은<br> -->
<!--                     <span class="up">2018년 12월</span>이 <span class="up">3,629,349원</span>으로 가장 높았고,<br> -->
<!--                     <span class="down">2018년 4월</span>이 <span class="down">258,913원</span>으로 가장 낮았으며,<br> -->
<!--                     <span class="average">월 평균 매출</span>은 <span class="average">1,286,072원</span> 입니다.</p> -->
                </div>
                <!--nodata_2-->
                <div class="summary_card second first_nodata top_nodata">
                <div class="title_highlight">
                    <p class="second_nodata_title">최근 1년 매출</p>
                </div>
                <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                </div>
                </div>
                <div class="summary_card third">
                <div class="title_highlight">
                    <p>조회기간 기준 </p>
                </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                
<!--                 <div class="table_summary"> -->
<!--                     <div class="high_tb"> -->
<!--                     <p class="lb">가장 높음</p> -->
<!--                     <div class="mall_box mb2"> -->
<!--                         <p>G마켓</p> -->
<!--                     </div> -->
<!--                     <div class="mall_box"> -->
<!--                         <p>G마켓 <br><span class="percent">12%</span></p> -->
<!--                     </div> -->
<!--                     <div class="mall_box"> -->
<!--                         <p>G마켓 <br><span class="percent">15%</span></p> -->
<!--                     </div> -->
<!--                     </div> -->
<!--                     <div class="text_info_tb tb2"> -->
<!--                     <div class="item_text_info"> -->
<!--                         <p>판매몰별 매출</p> -->
<!--                     </div> -->
<!--                     <div class="item_text_info"> -->
<!--                         <p>마켓수수료 공제율<br>(매출대비)</p> -->
<!--                     </div> -->
<!--                     <div class="item_text_info"> -->
<!--                         <p>광고성 수수료 공제율<br>(매출대비)</p> -->
<!--                     </div> -->
<!--                     </div> -->
<!--                     <div class="low_tb"> -->
<!--                     <p class="lb">가장 낮음</p> -->
<!--                     <div class="mall_box mb2"> -->
<!--                         <p>쿠팡</p> -->
<!--                     </div> -->
<!--                     <div class="mall_box"> -->
<!--                         <p>쿠팡 <br><span class="percent">6%</span></p> -->
<!--                     </div> -->
<!--                     <div class="mall_box"> -->
<!--                         <p>쿠팡 <br><span class="percent">3%</span></p> -->
<!--                     </div> -->
<!--                     </div> -->

<!--                 </div> -->
<!--                 <p class="desc_summary desc_summary_fontSize"> -->
<!--                     - 판매몰별 매출은 <span class="up_style">G마켓</span>에서 가장 높았고, <span class="down_style">쿠팡</span>에서 가장 낮았습니다.<br /> -->
<!--                     - 마켓유형별 판매는 <span class="color_style">오픈마켓</span>에서 주력하고 계십니다.<br /> -->
<!--                     - 마켓수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span class="down_style">12%</span>로 가장 높았고,<br /> -->
<!--                     <span class="up_style">쿠팡</span>에서 <span class="up_style">6%</span>로 가장 낮았습니다.<br /> -->
<!--                     - 광고성 수수료는 매출액 그럼 어대비 <span class="down_style">G마켓</span>에서 <span class="down_style">15%</span>로 가장 높았고,<br /> -->
<!--                     <span class="up_style">쿠팡</span>에서 <span class="up_style">3%</span>로 가장 낮았습니다. -->
<!--                 </p> -->
                </div>
                <!--nodata_3-->
                <div class="summary_card third first_nodata top_nodata">
                <div class="title_highlight">
                    <p class="third_nodata_title">조회기간 기준</p>
                </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                
                <div class="nodata_flexBox">
                    <h1>데이터가 없습니다.</h1>
                </div>
                </div>
                <div class="summary_card last">
                <div class="talk_sale_box">
                    <p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
                    <div class="talk_item first">
                    	<p class="emotion_talk " id="emotion_talk_setting" >
                    		<img src="/assets/images/main/sales_normal.png" alt="">
<%--                         <c:choose> --%>
<%--                             <c:when test="${empty salesRto}"> --%>
<!--                                 <img src="/assets/images/main/sales_normal.png" alt="">		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto >= 50 }"> --%>
<!--                                 <img src="/assets/images/main/sales_verygood.png" alt="매우 좋음">		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto <= 49 && salesRto >= 21  }"> --%>
<!--                                 <img src="/assets/images/main/sales_good.png" alt="좋음">		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto <= 20 && salesRto >= -20  }"> --%>
<!--                                 <img src="/assets/images/main/sales_normal.png" alt="보통">		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto <= -21 && salesRto >= -49  }"> --%>
<!--                                 <img src="/assets/images/main/sales_bad.png" alt="나쁨">		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto <= -50 }"> --%>
<!--                                 <img src="/assets/images/main/sales_terrible.png" alt="매우 나쁨">		 -->
<%--                             </c:when> --%>
<%--                         </c:choose> --%>
                    </p>
                    <p class="lb_emotion">매출 지역</p>
                    </div>
                    <div class="talk_item" id="talk_item_setting">
                    	<p class="txt_talk">매출 통계 데이터를<br>분석하는 중이에요.</p>
<%--                         <c:choose> --%>
<%--                             <c:when test="${empty salesRto}"> --%>
<!--                                 <p class="txt_talk">매출 통계 데이터를<br>분석하는 중이에요.</p>		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto >= 50 }"> --%>
<!--                                 <p class="txt_talk">매출이 엄~청<br>늘었어요! 야호^^</p>		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto <= 49 && salesRto >= 21  }"> --%>
<!--                                 <p class="txt_talk">이대로만 가즈아,<br>쭉~쭉쭉쭉!</p>		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto <= 20 && salesRto >= -20  }"> --%>
<!--                                 <p class="txt_talk">여기서 만족할 순<br>없겠죠?!</p>		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto <= -21 && salesRto >= -49  }"> --%>
<!--                                 <p class="txt_talk">에구, 좀 더<br>분발하셔야겠어요</p>		 -->
<%--                             </c:when> --%>
<%--                             <c:when test="${salesRto <= -50 }"> --%>
<!--                                 <p class="txt_talk">으쌰으쌰!<br>힘을내요, 슈퍼파월~!</p>		 -->
<%--                             </c:when> --%>
<%--                         </c:choose> --%>
                    </div>
                </div>
                <div class="talk_advice_box">
                    <p class="lb_advice">[도움말]</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>매출 산출기준</b><br>
                    판매몰에서 제공하는 부가매출신고내역에서 집계합니다.
                    (단, 부가세매출신고내역에서 조회되지 않는 당월 매출은
                    주문관리에서 집계합니다.)</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>공제 산출기준</b><br>
                    판매몰에서 제공하는 매입공제내역에서 집계합니다.</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로 가능합니다.</p>
                    <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>공제합계의 비율(%)</b><br>
                    공제합계, 마켓수수료,광고성수수료, 기타공제는
                    모두 <b>매출금액 대비 비율</b>입니다.
                    </p>
                </div>
                </div>

            </div>
            </div>
        </div>
        <!-- 모바일버전을 위한 상단 swiper 추가 -->
        <!--960 반응형-->
        <div class="summary_report_section_m_Box" style="display: none;">
            <div class="swiper-container summary_report_section_m">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[매출현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div class="summary_card first">
                        <div class="title_highlight">
                        <p>당월 매출</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
                        <div class="graph_summary type1">
                        <canvas id="spGh1_m" height="150"></canvas>
                        </div>
<!--                         <p class="desc_summary"> -->
<!--                         19년 3월 매출은<br> -->
<!--                         전월 대비 <span class="up">15% 증가</span>하였고,<br> -->
<!--                         전년 동월대비 <span class="down">50% 감소</span>하였습니다. -->
<!--                         </p> -->
                    </div>

                    <!--nodata_1-->
                    <div class="summary_card first first_nodata top_nodata">
                        <div class="title_highlight">
                        <p class="first_nodata_title">당월 매출</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>
                    <div class="summary_card second">
                        <div class="title_highlight">
                        <p>최근 1년 매출</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
                        <div class="graph_summary type2">
                        <canvas id="spGh2_m" height="150"></canvas>
                        </div>
                        <p class="desc_summary"></p>
<!--                         <p class="desc_summary">최근 1년 중 매출은<br> -->
<!--                         <span class="up">2018년 12월</span>이 <span class="up">3,629,349원</span>으로 가장 높았고,<br> -->
<!--                         <span class="down">2018년 4월</span>이 <span class="down">258,913원</span>으로 가장 낮았으며,<br> -->
<!--                         <span class="average">월 평균 매출</span>은 <span class="average">1,286,072원</span> 입니다.</p> -->
                    </div>
                    <!--nodata_2-->
                    <div class="summary_card second first_nodata top_nodata">
                        <div class="title_highlight">
                        <p class="second_nodata_title">최근 1년 매출</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[매출현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div class="summary_card third">
                        <div class="title_highlight">
                        <p>조회기간 기준</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
<!--                         <div class="table_summary"> -->
<!--                         <div class="high_tb"> -->
<!--                             <p class="lb">가장 높음</p> -->
<!--                             <div class="mall_box mb2"> -->
<!--                             <p>G마켓</p> -->
<!--                             </div> -->
<!--                             <div class="mall_box"> -->
<!--                             <p>G마켓 <br><span class="percent">12%</span></p> -->
<!--                             </div> -->
<!--                             <div class="mall_box"> -->
<!--                             <p>G마켓 <br><span class="percent">15%</span></p> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                         <div class="text_info_tb tb2"> -->
<!--                             <div class="item_text_info"> -->
<!--                             <p>판매몰별 매출</p> -->
<!--                             </div> -->
<!--                             <div class="item_text_info"> -->
<!--                             <p>마켓수수료 공제율<br>(매출대비)</p> -->
<!--                             </div> -->
<!--                             <div class="item_text_info"> -->
<!--                             <p>광고성 수수료 공제율<br>(매출대비)</p> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                         <div class="low_tb"> -->
<!--                             <p class="lb">가장 낮음</p> -->
<!--                             <div class="mall_box mb2"> -->
<!--                             <p>쿠팡</p> -->
<!--                             </div> -->
<!--                             <div class="mall_box"> -->
<!--                             <p>쿠팡 <br><span class="percent">6%</span></p> -->
<!--                             </div> -->
<!--                             <div class="mall_box"> -->
<!--                             <p>쿠팡 <br><span class="percent">3%</span></p> -->
<!--                             </div> -->
<!--                         </div> -->

<!--                         </div> -->
<!--                         <p class="desc_summary desc_summary_fontSize"> -->
<!--                         - 판매몰별 매출은 <span class="up_style">G마켓</span>에서 가장 높았고, <span class="down_style">쿠팡</span>에서 가장 낮았습니다.<br /> -->
<!--                         - 마켓유형별 판매는 <span class="color_style">오픈마켓</span>에서 주력하고 계십니다.<br /> -->
<!--                         - 마켓수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span class="down_style">12%</span>로 가장 높았고,<br /> -->
<!--                         <span class="up_style">쿠팡</span>에서 <span class="up_style">6%</span>로 가장 낮았습니다.<br /> -->
<!--                         - 광고성 수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span class="down_style">15%</span>로 가장 높았고,<br /> -->
<!--                         <span class="up_style">쿠팡</span>에서 <span class="up_style">3%</span>로 가장 낮았습니다. -->
<!--                         </p> -->
                    </div>
                    <!--nodata_3-->
                    <div class="summary_card third first_nodata top_nodata">
                        <div class="title_highlight">
                        <p class="third_nodata_title">조회기간 기준 (2018-04~2019-03)</p>
                        </div>
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>
                    <div class="summary_card last">
                        <div class="talk_sale_box">
                        <p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
                        <div class="talk_item first">
                            <p class="emotion_talk">
                            <img src="/assets/images/main/sales_normal.png" alt="">
                            </p>
                            <p class="lb_emotion">매출 지역</p>
                        </div>
                        <div class="talk_item">
                            <p class="txt_talk">여기서 만족할 순<br>
                            없겠죠?!</p>
                        </div>
                        </div>
                        <div class="talk_advice_box">
                        <p class="lb_advice">[도움말]</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>매출 산출기준</b><br>
                            판매몰에서 제공하는 부가매출신고내역에서 집계합니다.
                            (단, 부가세매출신고내역에서 조회되지 않는 당월 매출은
                            주문관리에서 집계합니다.)</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>공제 산출기준</b><br>
                            판매몰에서 제공하는 매입공제내역에서 집계합니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로 가능합니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>공제합계의 비율(%)</b><br>
                            공제합계, 마켓수수료,광고성수수료, 기타공제는
                            모두 <b>매출금액 대비 비율</b>입니다.
                        </p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="swiper-pagination1">
            </div>
        </div>
        <!--320 반응형-->
        <div class="summary_report_section_m_Box_2" style="display: none;">
            <div class="swiper-container summary_report_section_m_2">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[매출현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div class="summary_card first">
                        <div class="title_highlight">
                        <p>당월 매출</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
                        <div class="graph_summary type1">
                        <canvas id="spGh1_m_1" height="150"></canvas>
                        </div>
<!--                         <p class="desc_summary"> -->
<!--                         19년 3월 매출은<br> -->
<!--                         전월 대비 <span class="up">15% 증가</span>하였고,<br> -->
<!--                         전년 동월대비 <span class="down">50% 감소</span>하였습니다. -->
<!--                         </p> -->
                    </div>
                    <!--nodata_1-->
                    <div class="summary_card first first_nodata top_nodata">
                        <div class="title_highlight">
                        <p class="first_nodata_title">당월 매출</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[매출현황 요약보고]</p>
                    <div class="summary_section_card summary_section_card_display">
                    <div class="summary_card second">
                        <div class="title_highlight">
                        <p>최근 1년 매출</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
                        <div class="graph_summary type2">
                        <canvas id="spGh2_m_2" height="150"></canvas>
                        </div>
                        <p class="desc_summary"></p>                        
<!--                         <p class="desc_summary">최근 1년 중 매출은<br> -->
<!--                         <span class="up">2018년 12월</span>이 <span class="up">3,629,349원</span>으로 가장 높았고,<br> -->
<!--                         <span class="down">2018년 4월</span>이 <span class="down">258,913원</span>으로 가장 낮았으며,<br> -->
<!--                         <span class="average">월 평균 매출</span>은 <span class="average">1,286,072원</span> 입니다.</p> -->
                    </div>
                    <!--nodata_2-->
                    <div class="summary_card second first_nodata top_nodata">
                        <div class="title_highlight">
                        <p class="second_nodata_title">최근 1년 매출</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[매출현황 요약보고]</p>
                    <div class="summary_section_card">
                    <div class="summary_card third">
                        <div class="title_highlight">
                        <p>조회기간 기준</p>
                        </div>
				<div class="cv-spinner">
					<span class="spinner"></span>
				</div>							
                        
<!--                         <div class="table_summary"> -->
<!--                         <div class="high_tb"> -->
<!--                             <p class="lb">가장 높음</p> -->
<!--                             <div class="mall_box mb2"> -->
<!--                             <p>G마켓</p> -->
<!--                             </div> -->
<!--                             <div class="mall_box"> -->
<!--                             <p>G마켓 <br><span class="percent">12%</span></p> -->
<!--                             </div> -->
<!--                             <div class="mall_box"> -->
<!--                             <p>G마켓 <br><span class="percent">15%</span></p> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                         <div class="text_info_tb tb2"> -->
<!--                             <div class="item_text_info"> -->
<!--                             <p>판매몰별 매출</p> -->
<!--                             </div> -->
<!--                             <div class="item_text_info"> -->
<!--                             <p>마켓수수료 공제율<br>(매출대비)</p> -->
<!--                             </div> -->
<!--                             <div class="item_text_info"> -->
<!--                             <p>광고성 수수료 공제율<br>(매출대비)</p> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                         <div class="low_tb"> -->
<!--                             <p class="lb">가장 낮음</p> -->
<!--                             <div class="mall_box mb2"> -->
<!--                             <p>쿠팡</p> -->
<!--                             </div> -->
<!--                             <div class="mall_box"> -->
<!--                             <p>쿠팡 <br><span class="percent">6%</span></p> -->
<!--                             </div> -->
<!--                             <div class="mall_box"> -->
<!--                             <p>쿠팡 <br><span class="percent">3%</span></p> -->
<!--                             </div> -->
<!--                         </div> -->

<!--                         </div> -->
<!--                         <p class="desc_summary desc_summary_fontSize"> -->
<!--                         - 판매몰별 매출은 <span class="up_style">G마켓</span>에서 가장 높았고, <span class="down_style">쿠팡</span>에서 가장 낮았습니다.<br /> -->
<!--                         - 마켓유형별 판매는 <span class="color_style">오픈마켓</span>에서 주력하고 계십니다.<br /> -->
<!--                         - 마켓수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span class="down_style">12%</span>로 가장 높았고,<br /> -->
<!--                         <span class="up_style">쿠팡</span>에서 <span class="up_style">6%</span>로 가장 낮았습니다.<br /> -->
<!--                         - 광고성 수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span class="down_style">15%</span>로 가장 높았고,<br /> -->
<!--                         <span class="up_style">쿠팡</span>에서 <span class="up_style">3%</span>로 가장 낮았습니다. -->
<!--                         </p> -->
                    </div>
                    <!--nodata_3-->
                    <div class="summary_card third first_nodata top_nodata">
                        <div class="title_highlight">
                        <p class="third_nodata_title">조회기간 기준 (2018-04~2019-03)</p>
                        </div>
                        <div class="nodata_flexBox">
                        <h1>데이터가 없습니다.</h1>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div class="swiper-slide">
                <div class="summary_report_section">
                    <p class="title_summary open">[매출현황 요약보고]</p>
                    <div class="summary_section_card summary_section_card_display">
                    <div class="summary_card last">
                        <div class="talk_sale_box">
                        <p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
                        <div class="talk_item first">
                            <p class="emotion_talk">
                            <img src="/assets/images/main/sales_normal.png" alt="">
                            </p>
                            <p class="lb_emotion">매출 지역</p>
                        </div>
                        <div class="talk_item">
                            <p class="txt_talk">여기서 만족할 순<br>
                            없겠죠?!</p>
                        </div>
                        </div>
                        <div class="talk_advice_box">
                        <p class="lb_advice">[도움말]</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>매출 산출기준</b><br>
                            판매몰에서 제공하는 부가매출신고내역에서 집계합니다.
                            (단, 부가세매출신고내역에서 조회되지 않는 당월 매출은
                            주문관리에서 집계합니다.)</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>공제 산출기준</b><br>
                            판매몰에서 제공하는 매입공제내역에서 집계합니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은 <b>1년단위</b>로 가능합니다.</p>
                        <p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>공제합계의 비율(%)</b><br>
                            공제합계, 마켓수수료,광고성수수료, 기타공제는
                            모두 <b>매출금액 대비 비율</b>입니다.
                        </p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="swiper-pagination2">
            </div>
        </div>
        <!--end-->
        <div class="summary_graph_section summary_doughnut_section">
            <div class="period_graph_box">
            <div class="choice_mall_type" style="display: none;">
            <select class="sctbox_mall" id="mall_cd">
                <option value="">전체</option>
                <c:forEach items="${mallList}" var="mall" varStatus="status">
                    <option value="${mall.mall_cd }">${mall.mall_cd_nm }</option>
                </c:forEach>
            </select>
            </div>
            
            <!--monthpicker-->
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="sales_find_stt_dt" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
            </div>
            <div class="datepicker-cell" id="datepicker-sales_find_stt_dt" style="margin-top: -1px;"></div>
            <h1 style="display:inline-block;display: inline-block;vertical-align: middle;font-size: 1rem;"> ~ </h1>
            <!--monthpicker-->
            <div class="tui-datepicker-input tui-datetime-input tui-has-focus">
                <input type="text" id="sales_find_end_dt" aria-label="Year-Month">
                <span class="tui-ico-date"></span>
            </div>
            <div class="datepicker-cell" id="datepicker-sales_find_end_dt" style="margin-top: -1px;"></div>
            
            <div class="btn_period_group">
            <button id="btnSearch">조회하기</button>
            <button id="btnRecentlyYear" type="button" class="btn_gray">최근 1년보기</button>
            </div>
        </div>
            <!--new_chart-->
            <!--top-->
            <div class="new_chart_top new_chart_wrapper">
            <div class="chart_wrapper">
                <span class="chart_wrapper_title">
                판매몰별 매출
                </span>
                <div id="top_chart_1">
                </div>
                <div class="chart_text_wrap">
                <h1>합계</h1>
                <h2><span>15,432,866</span>원</h2>
                </div>
            </div>
            
            <div id="top_chart_1_nodata" class="chart_wrapper" style="display: none;">
                <span class="chart_wrapper_title">
                판매몰별 매출
                </span>
                <div id="graph_no_data">데이터가 없습니다.</div>
            </div>
            
            <div class="chart_wrapper">
                <span class="chart_wrapper_title">
                판매몰별 공제
                </span>
                <div id="top_chart_2"></div>
                <div class="chart_text_wrap">
                <h1>합계</h1>
                <h2><span>15,432,866</span>원</h2>
                </div>
            </div>
            
            <div id="top_chart_2_nodata" class="chart_wrapper" style="display: none;">
                <span class="chart_wrapper_title">
                판매몰별 매출
                </span>
                <div id="graph_no_data">데이터가 없습니다.</div>
            </div>
            
            </div>

            <!--가이드-->
            <div class="guide_box_graph">
            <div class="guide_txt">
                <img src="/assets/images/sale/btn_guide.png" alt="">
                <p>마켓유형별 도움말</p>
            </div>
            <div class="guide_pop_detail">
                <p class="close_guide"><img src="/assets/images/sale/btn_guideX.png" alt=""></p>
                <div class="head_guide">
                <img src="/assets/images/sale/btn_guide.png" alt="">
                <p>마켓유형별 도움말</p>
                </div>
                <div class="body_guide">
                <div class="item_guide">
                    <p class="lb">오픈마켓</p>
                    <div class="contents_guide">
                    <p class="desc">판매자와 구매자에게 모두 열려있는<br>
                        인터넷 중개몰(온라인장터)을 말합니다.</p>
                    <div class="list_mall_guide">
                        <p>11번가</p>
                        <p>G마켓</p>
                        <p>옥션</p>
                        <p>스마트스토어</p>
                        <p>네이버페이</p>
                        <p>인터파크OM</p>
                    </div>
                    </div>
                </div>
                <div class="item_guide">
                    <p class="lb">소셜<br>(커머스)</p>
                    <div class="contents_guide">
                    <p class="desc">소셜네크워크서비스(SNS)를 활용하여 이루어지는<br>
                        전자상거래의 일정으로, 일정 수 이상의 구매자가 모일 경우<br>
                        파격적인 할인가로 상품을 제공하는 판매 방식을 말합니다.
                    </p>
                    <div class="list_mall_guide">
                        <p>쿠팡</p>
                        <p>위메프</p>
                        <p>티켓몬스터</p>
                        <p>위메프2.0</p>
                        <p>쿠팡(매입)</p>
                        <p>-</p>
                    </div>
                    </div>
                </div>
                <div class="item_guide">
                    <p class="lb">종합몰</p>
                    <div class="contents_guide">
                    <p class="desc">다양한 제품을 파는 쇼핑몰을 의미하며, 오픈마켓과는 다르게<br>
                        인증을 거쳐 판매를 개시할 수 있습니다. 어느 정도 통제가 있고<br>
                        판매 수수료가 비싼 편에 속합니다.
                    </p>
                    <div class="list_mall_guide">
                        <p>GS홈쇼핑</p>
                        <p>신세계몰</p>
                        <p>롯데닷컴</p>
                        <p>CJ오쇼핑</p>
                        <p>롯데홈쇼핑</p>
                        <p>NS쇼핑</p>
                        <p>현대홈쇼핑</p>
                        <p>인터파크MD</p>
                        <p>-</p>
                    </div>
                    </div>
                </div>
                <div class="item_guide">
                    <p class="lb">PG&VAN사</p>
                    <div class="contents_guide">
                    <p class="desc">신용카드 결제 및 지불을 대행한 뒤 하부 쇼핑몰에서 수수료를 받는 업체를 말합니다.</p>
                    <div class="list_mall_guide">
                        <p>이니시스</p>
                        <p>KCP</p>
                        <p>모빌페이(올앳)</p>
                        <p>모빌리언스</p>
                        <p>KSIMS</p>
                        <p>토스페이먼츠</p>
                        <p>여신금융협회</p>
                        <p>-</p>
                        <p>-</p>
                    </div>
                    </div>
                </div>
                <div class="item_guide">
                    <p class="lb">전문몰</p>
                    <div class="contents_guide">
                    <p class="desc">특정 분야의 전문적인 제품을 취급하는 쇼핑몰을 말합니다.</p>
                    <div class="list_mall_guide">
                        <p>하프클럽</p>
                        <p>10x10</p>
                        <p>아이스타일24</p>
                        <p>위즈위드</p>
                        <p>패션플러스</p>
                        <p>어라운드더코너</p>
                        <p>무신사</p>
                        <p>지그재그</p>
                        <p>-</p>
                    </div>
                    </div>
                </div>
                <div class="item_guide">
                    <p class="lb">오프라인몰</p>
                    <div class="contents_guide">
                    <p class="desc">백화점, 대형마트 등의 오프라인 판매채널로서 매출, 정산 등의 판매정보를 온라인으로 제공하는 몰을 말합니다.</p>
                    <div class="list_mall_guide">
                        <p>현대백화점EDI</p>
                        <p>롯데백화점EDI</p>
                        <p>홈플러스</p>
                        <p>스타필드</p>
                        <p>-</p>
                        <p>-</p>
                    </div>
                    </div>
                </div>
                <!-- <div class="item_guide">
                    <p class="lb">소셜<br>(커머스)</p>
                    <div class="contents_guide">
                    <p class="desc">소셜네크워크서비스(SNS)를 활용하여 이루어지는<br>
                        전자상거래의 일정으로, 일정 수 이상의 구매자가 모일 경우<br>
                        파격적인 할인가로 상품을 제공하는 판매 방식을 말합니다.
                    </p>
                    <div class="list_mall_guide">
                        <p>쿠팡</p>
                        <p>위메프</p>
                        <p>티켓몬스터</p>
                        <p>위메프2.0</p>
                        <p>쿠팡(매입)</p>
                        <p>-</p>
                    </div>
                    </div>
                </div> -->

                </div>
            </div>
            </div>
            <!--bottom-->
            <div class="new_chart_bottom new_chart_wrapper">
            <div id="bottom_chart_1_wrapper" class="chart_wrapper">
                <span class="chart_wrapper_title">
                마켓유형별 매출
                </span>
                <div id="bottom_chart_1"></div>
                <div class="chart_text_wrap">
                <h1>합계</h1>
                <h2><span>15,432,866</span>원</h2>
                </div>
            </div>
            
            <div id="bottom_chart_1_nodata" class="chart_wrapper" style="display: none;">
                <span class="chart_wrapper_title">
                마켓유형별 매출
                </span>
                <div id="graph_no_data">데이터가 없습니다.</div>
            </div>
            
            
            
            <div class="chart_wrapper">
                <span class="chart_wrapper_title">
                마켓유형별 공제
                </span>
                <div id="bottom_chart_2"></div>
                <div class="chart_text_wrap">
                <h1>합계</h1>
                <h2><span>15,432,866</span>원</h2>
                </div>
            </div>
            
            
            <div id="bottom_chart_2_nodata" class="chart_wrapper" style="display: none;">
                <span class="chart_wrapper_title">
                마켓유형별 매출
                </span>
                <div id="graph_no_data">데이터가 없습니다.</div>
            </div>
            </div>

        </div>
        </div>
    </div>
    </div>

<script>
    var _summaryLast;
    var _yearMarket;
    var _monthMarket;
    var _mallGraph;
    <c:if test="${not empty summaryLast }">
    _summaryLast = ${summaryLast};
    </c:if>
    <c:if test="${not empty yearMarket }">
    _yearMarket = ${yearMarket};
    </c:if>
    <c:if test="${not empty monthMarket }">
    _monthMarket = ${monthMarket};
    </c:if>
    <c:if test="${not empty mallGraph }">
    _mallGraph = ${mallGraph};
    </c:if>

    var _charts = {};


    // 월 단위 차트 데이터
    function drawLastMonthView(id, dataList){
        var div_summary_card = $("#"+id).parents(".summary_card.first");
        div_summary_card.empty();
                
        // 월
        var lastKrYymm = moment(new Date()).subtract(1, 'month').format("YY년 MMM"); 
        var html = [];
        html.push("  <div class=\"title_highlight\">");
        html.push("		<p>"+ lastKrYymm +" 매출</p>");
        html.push("  </div>");
        html.push("  <div class=\"graph_summary type1\">");
        html.push("		<canvas id=\""+ id +"\" height=\"150\"></canvas>");
        html.push("  </div>");
        html.push("  <p class=\"desc_summary\">");
        html.push("	"+lastKrYymm+" 매출은<br>");

        var endText = dataList[1] == 0 ? "하였습니다." : "하였고,"
            
        if(dataList[0] > 0 ){
            html.push("	전월 대비 <span class=\"up\">"+ dataList[0] +"% 증가</span>"+endText+"<br>");	
        } else if(dataList[0] < 0 ){
            html.push("	전월 대비 <span class=\"down\">"+ dataList[0] +"% 감소</span>"+endText+"<br>");
        } 
        if( dataList[1] > 0){
            html.push("	전년 동월대비 <span class=\"up\">"+dataList[1]+"% 증가</span>하였습니다.");
        } else if(dataList[1] < 0){
            html.push("	전년 동월대비 <span class=\"down\">"+dataList[1]+"% 감소</span>하였습니다.");
        }
        
        html.push("  </p>");
        html.push("</div>");

        div_summary_card.html(html.join(""));
        
        var color = [];

        if( dataList[0] > 0){ color.push("#75ced2"); } 
        else { color.push("#f4715b"); }

        if( dataList[1] > 0){ color.push("#75ced2"); } 
        else { color.push("#f4715b"); }

        var max = Math.abs(dataList[0]) > Math.abs(dataList[1]) ? Math.abs(dataList[0]) : Math.abs(dataList[1]);

        // 차트 생성
        var ctx = document.getElementById(id).getContext('2d');
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['전월 대비', '전년 동월대비'],
            datasets: [{
            data: dataList,
            backgroundColor: color,
            hoverBackgroundColor: color,
            borderColor: color,
            hoverBorderColor: color
            }]
        },
        options: {
            responsive: false,
            maintainAspectRatio: false,
            layout: {
                padding: {
                top: 10
                }
            },
            legend: {
                display: false
            },
            title: {
                display: false
            },
            scales: {
                xAxes: [{
                
                gridLines: {
                    display: false,
                    beginAtZero: false,
                    barThickness: 1000,
                    color:"rgba(255,99,132,0)"

                }
                }],
                yAxes: [{
                ticks: {
                    "max": max,
                    "min": (max * -1),
                    display: false,
                    suggestedMin: 0,
                },
                gridLines: {
                    display: true,
                    drawBorder: false,
                    beginAtZero: false,
                    barThickness: 0,
                    color:"rgba(255,99,132,0)"
                },
                }]
            },
            tooltips: {
                enabled: false,
            }
            }
        });

        var noData = $(".summary_card.first.first_nodata.top_nodata");
        if( dataList[0] == "0" && dataList[1] == "0" ){
            div_summary_card.hide();
            noData.find(".title_highlight p").html(lastKrYymm + " 매출");
            noData.show();	
        } else {
            div_summary_card.show();
            noData.hide();
        }
    }
    // 최근 1년 매출
    function drawLastYearView(id, labelList, dataList){
        var div_summary_card = $("#"+id).parents(".summary_card.second");
        div_summary_card.empty();

        var moneyList = [];
        for(var i in dataList){
            moneyList.push(fnAddComma(dataList[i]));
        }

        var html = [];
        html.push(" <div class=\"title_highlight\">");
        html.push("		<p>최근 1년 매출</p>");
        html.push(" </div>");
        html.push(" <div class=\"graph_summary type2\">");
        html.push("		<canvas id=\""+id+"\" height=\"150\"></canvas>");
        html.push(" </div>");
        html.push(" <p class=\"desc_summary\">최근 1년 중 매출은<br>");

        if( (nonNull(dataList[0])) && (nonNull(dataList[1])) && labelList[0] != labelList[1] ){
            html.push("	<span class=\"up\">"+ moment(labelList[0], "YYYYMM").format("YYYY년 MMM") +"</span>이 <span class=\"up\">"+moneyList[0]+"원</span>으로 가장 높았고,<br>");
            html.push("	<span class=\"down\">"+ moment(labelList[1], "YYYYMM").format("YYYY년 MMM")  +"</span>이 <span class=\"down\">"+moneyList[1]+"원</span>으로 가장 낮았으며,<br>");
            html.push("	<span class=\"average\">월 평균 매출</span>은 <span class=\"average\">"+moneyList[2]+"원</span> 입니다.</p>");
        }
        
        div_summary_card.html(html.join(""));

        // 데이터 날짜 형식 변환
        labelList[0] = moment(labelList[0], "YYYYMM").format("YYYY-MMM");
        labelList[1] = moment(labelList[1], "YYYYMM").format("YYYY-MMM");

        var ctx = document.getElementById(id).getContext('2d');
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labelList,
                datasets: [{
                data: dataList,
                backgroundColor: ["#75ced2", "#f4715b", "#d6a7d1"],
                hoverBackgroundColor: ["#75ced2", "#f4715b", "#d6a7d1"],
                borderColor: ["#75ced2", "#f4715b", "#d6a7d1"],
                hoverBorderColor: ["#75ced2", "#f4715b", "#d6a7d1"]
                }]
            },
            options: {
                responsive: false,
                maintainAspectRatio: false,
                layout: {
                    padding: {
                    top: 10
                    }
                },
                legend: {
                    display: false
                },
                title: {
                    display: false
                },
                scales: {
                    xAxes: [{
                    gridLines: {
                        display: false,
                    }
                    }],
                    yAxes: [{
                    ticks: {
                        display: false,
                        max: Math.ceil(dataList[0] * 1.1)
                        
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false,
                    },
                    }]
                },
                tooltips: {
                    enabled: false,
                }
                }
            });	

        if( (dataList[0] == 0 && dataList[1] == 0) || (labelList[0] == labelList[1]) ){
            div_summary_card.hide();
            $(".summary_card.second.first_nodata.top_nodata").show();	
        } else {
            div_summary_card.show();
            $(".summary_card.second.first_nodata.top_nodata").hide();
        }
    }

    // 조회기간 기준
    function drawSeachView(yearMarket){
        var isNoData = true;
        var div_summary_card = $(".summary_card.third:not(.top_nodata)");
        div_summary_card.empty();
        
        var html = [];
        html.push("  <div class=\"title_highlight\">");
        html.push("	<p>조회기간 기준 ("+$("#sales_find_stt_dt").val()+"~"+$("#sales_find_end_dt").val()+")</p>");
        html.push("  </div>");
        html.push("  <div class=\"table_summary\">");
        html.push("	<div class=\"high_tb\">");
        html.push("	  <p class=\"lb\">가장 높음</p>");

        // 매출
        if( nonNull(yearMarket.max_sales_mall_cd_nm) && nonNull(yearMarket.min_sales_mall_cd_nm) 
                && yearMarket.max_sales_mall_cd_nm != yearMarket.min_sales_mall_cd_nm ){
            html.push("	  <div class=\"mall_box mb2\">");
            html.push("		<p>"+ nvl(yearMarket.max_sales_mall_cd_nm, "")+"</p>");
            html.push("	  </div>");
            isNoData = false;
        }

        // 마켓수수료
        if( nonNull(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) 
                && yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm
                ){
            html.push("	  <div class=\"mall_box \">");
            html.push("		<p>"+ nvl(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm, "") +" <br><span class=\"percent\">"+ nvl(yearMarket.max_sales_prc_per_sales_mk_fee_rto, 0)+"%</span></p>");
            html.push("	  </div>");
            isNoData = false;
        }

        // 광고성
        if( nonNull(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) 
                && yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm
                ){
            html.push("	  <div class=\"mall_box \">");
            html.push("		<p>"+ nvl(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm, "")+" <br><span class=\"percent\">"+ nvl(yearMarket.max_sales_prc_per_sales_ad_fee_rto, 0)+"%</span></p>");
            html.push("	  </div>");
            isNoData = false;
        }
        
        html.push("	</div>");
        html.push("	<div class=\"text_info_tb tb2\">");

        // 매출
        if( nonNull(yearMarket.max_sales_mall_cd_nm) && nonNull(yearMarket.min_sales_mall_cd_nm) 
                && yearMarket.max_sales_mall_cd_nm != yearMarket.min_sales_mall_cd_nm
                ){
            html.push("	  <div class=\"item_text_info\">");
            html.push("		<p>판매몰별 매출</p>");
            html.push("	  </div>");
        }

        // 마켓수수료
        if( nonNull(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) 
                && yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm
                ){
            html.push("	  <div class=\"item_text_info\">");
            html.push("		<p>마켓수수료 공제율<br>(매출대비)</p>");
            html.push("	  </div>");
        }

        // 광고성
        if( nonNull(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) 
                && yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm
                ){
            html.push("	  <div class=\"item_text_info\">");
            html.push("		<p>광고성 수수료 공제율<br>(매출대비)</p>");
            html.push("	  </div>");
        }
        
        html.push("	</div>");
        html.push("	<div class=\"low_tb\">");
        html.push("	  <p class=\"lb\">가장 낮음</p>");

        // 매출
        if( nonNull(yearMarket.max_sales_mall_cd_nm) && nonNull(yearMarket.min_sales_mall_cd_nm) 
                && yearMarket.max_sales_mall_cd_nm != yearMarket.min_sales_mall_cd_nm 
                ){
            html.push("	  <div class=\"mall_box mb2\">");
            html.push("		<p>"+ nvl(yearMarket.min_sales_mall_cd_nm, "") +"</p>");
            html.push("	  </div>");
        }
    
        // 마켓수수료
        if( nonNull(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) 
                && yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm
                ){
            html.push("	  <div class=\"mall_box \">");
            html.push("		<p>"+ nvl(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm, "") +" <br><span class=\"percent\">"+ nvl(yearMarket.min_sales_prc_per_sales_mk_fee_rto, 0) +"%</span></p>");
            html.push("	  </div>");
        }

        // 광고성
        if( nonNull(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) 
                && yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm
                ){
            html.push("	  <div class=\"mall_box \">");
            html.push("		<p>"+ nvl(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm, "") +" <br><span class=\"percent\">"+ nvl(yearMarket.min_sales_prc_per_sales_ad_fee_rto, 0) +"%</span></p>");
            html.push("	  </div>");
        }
        
        html.push("	</div>");
        html.push("  </div>");
        html.push("  <p class=\"desc_summary desc_summary_fontSize\">");
        // 매출
        if( nonNull(yearMarket.max_sales_mall_cd_nm) && nonNull(yearMarket.min_sales_mall_cd_nm) 
                && yearMarket.max_sales_mall_cd_nm != yearMarket.min_sales_mall_cd_nm ){
            html.push("	- 판매몰별 매출은 <span class=\"up_style\">" + nvl(yearMarket.max_sales_mall_cd_nm, "") + "</span>에서 가장 높았고, <span class=\"down_style\">"+ nvl(yearMarket.min_sales_mall_cd_nm, "") +"</span>에서 가장 낮았습니다.<br />");
        }

        // 마켓 유형별 판매
        if( nonNull(yearMarket.max_sales_prc_market_typ_nm) ){
            html.push("	- 마켓유형별 판매는 <span class=\"color_style\">"+ nvl(yearMarket.max_sales_prc_market_typ_nm, "") +"</span>에서 주력하고 계십니다.<br />");
        }
        
        // 마켓수수료
        if( nonNull(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) 
                && yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm
                ){
            html.push("	- 마켓수수료는 매출액 대비 <span class=\"down_style\">"+ nvl(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm, "")  +"</span>에서 <span class=\"down_style\">"+ nvl(yearMarket.max_sales_prc_per_sales_mk_fee_rto, 0)  +"%</span>로 가장 높았고,<br />");
            html.push("	<span class=\"up_style\">"+ nvl(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm, "") +"</span>에서 <span class=\"up_style\">"+ nvl(yearMarket.min_sales_prc_per_sales_mk_fee_rto, 0)+"%</span>로 가장 낮았습니다.<br />");	
        }

        // 광고 수수료
        if( nonNull(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) 
                && yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm
                ){
            html.push("	- 광고성 수수료는 매출액 대비 <span class=\"down_style\">"+ nvl(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm, "")+"</span>에서 <span class=\"down_style\">"+ nvl(yearMarket.max_sales_prc_per_sales_ad_fee_rto, 0) +"%</span>로 가장 높았고,<br />");
            html.push("	<span class=\"up_style\">"+ nvl(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm, "")+"</span>에서 <span class=\"up_style\">"+ nvl(yearMarket.min_sales_prc_per_sales_ad_fee_rto, 0)+"%</span>로 가장 낮았습니다.");
        }

        html.push("  </p>");

        if( isNoData  ){
            $(".third_nodata_title").html("조회기간 기준 ("+$("#sales_find_stt_dt").val()+"~"+$("#sales_find_end_dt").val()+")");
            div_summary_card.hide();
            $(".summary_card.third.first_nodata.top_nodata").show();	
        } else{
            div_summary_card.show();
            div_summary_card.html(html.join(""));
            $(".summary_card.third.first_nodata.top_nodata").hide();
        }
    }

    // 파이차트 생성
    function drawPieChart(id, dataList){
        am4core.useTheme(am4themes_animated);
        am4core.options.commercialLicense = true;
        var chart = am4core.create(id, am4charts.PieChart);
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "value";
        pieSeries.dataFields.category = "title";
        chart.innerRadius = am4core.percent(50);
        pieSeries.alignLabels = true;
        pieSeries.fontSize = 10;
        
        var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
        shadow.opacity = 0;
        var hoverState = pieSeries.slices.template.states.getKey("hover");
        var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
        hoverShadow.opacity = 0.7;
        hoverShadow.blur = 5;
        chart.data = dataList;

        var sum = 0;
        for(var i in dataList){
            sum += dataList[i].value;
        }

        if(sum == 0){
            var target = $("#"+id+"_nodata");
            target.show();
            target.prev().hide();
        } else {
            var target = $("#"+id).next();
            target.empty();
            var html = [];
            html.push("<h1>합계</h1>");
            html.push("<h2><span>"+ fnAddComma(sum) +"</span>원</h2>");
            target.html( html.join("") );
        }

        // if(typeof _charts[id] == "undefined"){
        //     _charts[id] = chart;
        // } else {
        //     _charts[id] = null;
        //     _charts[id] = chart;
        // }
        
        // return chart;
    }

    // 차트용 데이터 파싱
    function chartDataParse(mallGraph){
        var result = { 
            "mall_prc": [],
            "mall_dedu": [],
            "type_prc": [],
            "type_dedu": []
        };

        var mallList = mallGraph.sales_mall;
        var typeList = mallGraph.sales_market_typ;
        $.each(mallList, function(){
            result.mall_prc.push( {"title": this.mall_cd_nm, "value":  nvl(this.sales_prc, 0) } );
            result.mall_dedu.push( {"title": this.mall_cd_nm, "value": nvl(this.dedu_sum, 0) } );
        });

        $.each(typeList, function(){
            result.type_prc.push( {"title": this.market_typ_cd_nm, "value": nvl(this.sales_prc, 0) } );
            result.type_dedu.push( {"title": this.market_typ_cd_nm, "value": nvl(this.dedu_sum, 0) } );
        });

        return result;
    }

    function fnSearch(){
        var sales_find_stt_dt = $("#sales_find_stt_dt").val();
        var sales_find_end_dt = $("#sales_find_end_dt").val();
        var param = {
                "sales_find_stt_dt": sales_find_stt_dt,
                "sales_find_end_dt": sales_find_end_dt,
        };
        if($("#mall_cd").val() != ""){
            param.mall_cd = $("#mall_cd").val(); 
        }
        
        $.get("/sub/sales/year", $.param(param), function(res){
            // 조회기간 기준
            drawSeachView(res);
        });

        $.get("/sub/sales/graph", $.param(param), function(res){
            var chartData = chartDataParse(res);
            setTimeout(function () {
                drawPieChart("top_chart_1", chartData.mall_prc);
                drawPieChart("top_chart_2", chartData.mall_dedu);
                drawPieChart("bottom_chart_1", chartData.type_prc);
                drawPieChart("bottom_chart_2", chartData.type_dedu);
            }, 200);
        });
    }
    /*
     * 날짜포맷 yyyy-MM-dd 변환
     */
    function getFormatDate(date){
        var year = date.getFullYear();
        var month = (1 + date.getMonth());
        month = month >= 10 ? month : '0' + month;
        var day = date.getDate();
        day = day >= 10 ? day : '0' + day;
        // return year + '-' + month + '-' + day;
      return year + '-' + month;
    }

    $(document).ready(function() {
	    var now = new Date();
	    var prevYearDate, prevMonthDate, toDay;

	    prevYearDate = new Date(now.getFullYear(), now.getMonth()-12, 1);
	    prevYearDate = getFormatDate(prevYearDate);
	    prevMonthDate = new Date(now.getFullYear(), now.getMonth(), 0);
	    prevMonthDate = getFormatDate(prevMonthDate);
	    
		$("#sales_find_stt_dt").val(prevYearDate);
		$("#sales_find_end_dt").val(prevMonthDate);
		
        
		fnSearch();
		
        var today = new Date();
        
        var opt1 = {"selectableRanges": [
                [ new Date(2014, 12 ,1) , moment(today).subtract(1, 'month').toDate()]]
        };
        var opt2 = {"selectableRanges": 
            [[ new Date(2014, 1, 1), moment(today).subtract(1, 'month').toDate()]]
        };
        
        var pickers = createRangeMonthPicker(null, opt1, null, opt2);

        function fnInit(){
            pickers.startpicker.on("change", function(){
                
                if( (+pickers.startpicker.getDate()) > (+pickers.endpicker.getDate()) ){
                    pickers.endpicker.setDate(moment(pickers.startpicker.getDate()).toDate());	
                }
            });

            pickers.endpicker.on("change", function(){
                if( (+pickers.startpicker.getDate()) > (+pickers.endpicker.getDate()) ){
                    pickers.startpicker.setDate(moment(pickers.endpicker.getDate()).toDate());	
                }
            });
            
            //custom tooltip
            Chart.plugins.register({
            afterDatasetsDraw: function(chartInstance, easing) {

                // To only draw at the end of animation, check for easing === 1
                var ctx = chartInstance.chart.ctx;
                chartInstance.data.datasets.forEach(function(dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                var chartID = chartInstance.chart.canvas.id;
                if (!meta.hidden) {
                    
                    if (chartID.indexOf("spGh1") > -1) {
                        
                    meta.data.forEach(function(element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = '#000';
                        var fontSize = 12;
                        var fontStyle = 'normal';
                        var fontFamily = '';
                        var backgroundColor = 'rgba(79,105,232,0.2)';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                        // Just naively convert to string for now
                        var dataNum = dataset.data[index];
                        var dataString = dataset.data[index].toString();
                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        var padding = 5;
                        var position = element.tooltipPosition();
                        if (dataNum > 0) {
                            ctx.fillText("▲" + dataString + "%", position.x, position.y + (fontSize / 2) + padding);
                        } else {
                        dataNum = dataNum.replace(/[^0-9]/g, '');
                        ctx.fillText("▼" + dataNum + "%", position.x, position.y - (fontSize / 2) - padding);
                        }
                    });
                    } else if (chartID.indexOf("spGh2") > -1) {
                        // 최근 1년 매출
                    meta.data.forEach(function(element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = '#000';
                        var fontSize = 12;
                        var fontStyle = 'normal';
                        var fontFamily = '';
                        var backgroundColor = 'rgba(79,105,232,0.2)';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        var padding = 0;
                        var position = element.tooltipPosition();

                        ctx.fillText(dataString + "원", position.x, position.y - (fontSize / 2) - padding);
                    });
                    } else {
                    meta.data.forEach(function(element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = '#000';
                        var fontSize = 12;
                        var fontStyle = 'normal';
                        var fontFamily = '';
                        var backgroundColor = 'rgba(79,105,232,0.2)';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        var padding = 10;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                    }
                }
                });
            }
            });
            //custom tooltip end
            
            $(function() {
                var swiper = new Swiper('.summary_report_section_m', {
                pagination: {
                    el: '.swiper-pagination1',
                },
                });
                var swiper = new Swiper('.summary_report_section_m_2', {
                pagination: {
                    el: '.swiper-pagination2',
                },
                });
            });
            
             // 월
             var dataList = [];
             dataList.push( nvl(_summaryLast.last_month_per_before_last_month_sales_rto+"", "0") );
             dataList.push( nvl(_summaryLast.last_year_same_month_sales_rto+"", "0") );
             drawLastMonthView("spGh1", dataList);
             drawLastMonthView("spGh1_m", dataList);
             drawLastMonthView("spGh1_m_1", dataList);

             // 년
             dataList = [];
             var labelList = [];
             labelList.push(_summaryLast.last_year_sales_max_month);
             labelList.push(_summaryLast.last_year_sales_min_month);
             labelList.push("월평균");
             dataList.push(nvl(_summaryLast.last_year_sales_max_prc, 0));
             dataList.push(nvl(_summaryLast.last_year_sales_min_prc, 0));
             dataList.push(nvl(_summaryLast.last_year_sales_avg_prc, 0));
             drawLastYearView("spGh2", labelList, dataList);
             drawLastYearView("spGh2_m", labelList, dataList);
             drawLastYearView("spGh2_m_2", labelList, dataList);
        }

        // 조회하기
        $("#btnSearch").on("click",function(){
            fnSearch();
        });

        // 최근 1년보기
        $("#btnRecentlyYear").on("click",function(){
            var today = new Date();
            pickers.startpicker.setDate(moment(today).subtract(12, 'month').toDate());
            pickers.endpicker.setDate(moment(today).subtract(1, 'month').toDate());
            
        });

        // 실행
        fnInit();
    });
</script>