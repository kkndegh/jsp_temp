<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<!--tui-grid-->
<link rel="stylesheet" href="/assets/css/tui-grid.css">
<link rel="stylesheet" href="/assets/css/paging.css">

<script src="/assets/js/Chart.bundle.min.js"></script>

<style>
	.swiper-pagination1,
	.swiper-pagination2 {
		margin: 0 auto;
		text-align: center;
		width: 100%;
		bottom: 2.5rem;
		position: absolute;
		z-index: 6;
	}

	.swiper-pagination-bullet {
		margin-right: 0.5rem;
	}

	.swiper-pagination-bullet:last-of-type {
		margin-right: 0;
	}

	.tui-grid-body-area {
		height: auto !important;
	}

	.tui-grid-cell {
		background-color: #f8f8f8;
	}

	#grid_2 {
		padding-bottom: 4rem;
	}

	.tui-grid-container * {
		box-sizing: content-box;
		text-align: right;
	}

	.tui-grid-cell-header {
		text-align: right;
	}


	#grid_2 .tui-grid-row-odd td:nth-of-type(1) div {
		text-align: left !important;
	}

	#grid_2 .tui-grid-row-even td:nth-of-type(1) div {
		text-align: left !important;
	}

	.tui-datepicker-input>input {
		width: 100%;
		height: 100%;
		padding: 6px 27px 6px 10px;
		font-size: 12px;
		line-height: 14px;
		vertical-align: top;
		border: 0;
		color: #333;
		border-radius: 3rem;
	}

	.tui-datepicker-input {
		position: relative;
		display: inline-block;
		width: 120px;
		height: 2.5rem;
		vertical-align: top;
		border: 1px solid #ddd;
		border-radius: 3rem;
	}

	.datepicker-cell {
		display: inline-block;
	}

	.tui-datepicker-input.tui-has-focus {
		vertical-align: middle;
		border: 0;
	}

	.tui-datepicker {
		border: 1px solid #aaa;
		background-color: white;
		position: absolute;
		margin-left: -11rem;
		margin-top: 1rem;
		z-index: 999;
	}

	#spGh1 {
		height: 150px;
	}

	.cv-spinner {
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;  
	}
	.spinner {
		width: 40px;
		height: 40px;
		border: 4px #ddd solid;
		border-top: 4px #2e93e6 solid;
		border-radius: 50%;
		animation: sp-anime 0.8s infinite linear;
	}
	@keyframes sp-anime {
		100% { 
			transform: rotate(360deg); 
		}
	}
	.is-hide{
		display:none;
	}	
</style>

<script src="/assets/js/tui-grid.js"></script>

<div class="container">
	<div class="sales_wra">
		<div class="sales_area">
			<div class="menu_top">
				<p class="menu_name">매출통계</p>
				<div class="gnb_topBox">
					<ul class="gnb_top clearfix">
						<li class="focus"><a href="/sub/sales/sales">매출 현황</a></li>
						<li><a href="/sub/sales/analysis">매출분석 그래프</a></li>
					</ul>
				</div>
			</div>
			<div class="summary_report_section_wrap">
				<div class="summary_report_section">
					<p class="title_summary open">[매출현황 요약보고]</p>
					<div class="summary_section_card">
						<div class="summary_card first">
							<div class="title_highlight">
								<p>당월 매출</p>
							</div>
							<div class="cv-spinner">
								<span class="spinner"></span>
							</div>							
							<div class="graph_summary type1">
								<canvas id="spGh1" height="150"></canvas>
							</div>
<!-- 							<p class="desc_summary"> -->
<!-- 								19년 3월 매출은<br> -->
<!-- 								전월 대비 <span class="up">15% 증가</span>하였고,<br> -->
<!-- 								전년 동월대비 <span class="down">50% 감소</span>하였습니다. -->
<!-- 							</p> -->
						</div>

						<!--nodata_1-->
						<div class="summary_card first first_nodata top_nodata">
							<div class="title_highlight">
								<p class="first_nodata_title">당월 매출</p>
							</div>
							<div class="nodata_flexBox">
								<h1>데이터가 없습니다.</h1>
							</div>
						</div>

						<div class="summary_card second">
							<div class="title_highlight">
								<p>최근 1년 매출</p>
							</div>
							<div class="cv-spinner">
								<span class="spinner"></span>
							</div>
							<div class="graph_summary type2">
								<canvas id="spGh2"></canvas>
							</div>
<!-- 							<p class="desc_summary">최근 1년 중 매출은<br> -->
<!-- 								<span class="up">2018년 12월</span>이 <span class="up">3,629,349원</span>으로 가장 높았고,<br> -->
<!-- 								<span class="down">2018년 4월</span>이 <span class="down">258,913원</span>으로 가장 낮았으며,<br> -->
<!-- 								<span class="average">월 평균 매출</span>은 <span class="average">1,286,072원</span> 입니다.</p> -->
						</div>

						<!--nodata_2-->
						<div class="summary_card second first_nodata top_nodata">
							<div class="title_highlight">
								<p class="second_nodata_title">최근 1년 매출</p>
							</div>
							<div class="cv-spinner">
								<span class="spinner"></span>
							</div>
							<div class="nodata_flexBox">
								<h1>데이터가 없습니다.</h1>
							</div>
						</div>

						<div class="summary_card third">
							<div class="title_highlight">
								<p>조회기간 기준 </p>
							</div>
							<div class="cv-spinner">
								<span class="spinner"></span>
							</div>
							
<!-- 							<div class="table_summary"> -->
<!-- 								<div class="high_tb"> -->
<!-- 									<p class="lb">가장 높음</p> -->
<!-- 									<div class="mall_box mb2"> -->
<!-- 										<p>G마켓</p> -->
<!-- 									</div> -->
<!-- 									<div class="mall_box"> -->
<!-- 										<p>G마켓 <br><span class="percent">12%</span></p> -->
<!-- 									</div> -->
<!-- 									<div class="mall_box"> -->
<!-- 										<p>G마켓 <br><span class="percent">15%</span></p> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="text_info_tb tb2"> -->
<!-- 									<div class="item_text_info"> -->
<!-- 										<p>판매몰별 매출</p> -->
<!-- 									</div> -->
<!-- 									<div class="item_text_info"> -->
<!-- 										<p>마켓수수료 공제율<br>(매출대비)</p> -->
<!-- 									</div> -->
<!-- 									<div class="item_text_info"> -->
<!-- 										<p>광고성 수수료 공제율<br>(매출대비)</p> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 								<div class="low_tb"> -->
<!-- 									<p class="lb">가장 높음</p> -->
<!-- 									<div class="mall_box mb2"> -->
<!-- 										<p>쿠팡</p> -->
<!-- 									</div> -->
<!-- 									<div class="mall_box"> -->
<!-- 										<p>쿠팡 <br><span class="percent">6%</span></p> -->
<!-- 									</div> -->
<!-- 									<div class="mall_box"> -->
<!-- 										<p>쿠팡 <br><span class="percent">3%</span></p> -->
<!-- 									</div> -->
<!-- 								</div> -->

<!-- 							</div> -->
<!-- 							<p class="desc_summary desc_summary_fontSize"> -->
<!-- 								- 판매몰별 매출은 <span class="up_style">G마켓</span>에서 가장 높았고, <span -->
<!-- 									class="down_style">쿠팡</span>에서 가장 낮았습니다.<br /> -->
<!-- 								- 마켓유형별 판매는 <span class="color_style">오픈마켓</span>에서 주력하고 계십니다.<br /> -->
<!-- 								- 마켓수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span -->
<!-- 									class="down_style">12%</span>로 가장 높았고,<br /> -->
<!-- 								<span class="up_style">쿠팡</span>에서 <span class="up_style">6%</span>로 가장 낮았습니다.<br /> -->
<!-- 								- 광고성 수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span -->
<!-- 									class="down_style">15%</span>로 가장 높았고,<br /> -->
<!-- 								<span class="up_style">쿠팡</span>에서 <span class="up_style">3%</span>로 가장 낮았습니다. -->
<!-- 							</p> -->
						</div>

						<!--nodata_3-->
						<div class="summary_card third first_nodata top_nodata">
							<div class="title_highlight">
								<p class="third_nodata_title">조회기간 기준</p>
							</div>
							<div class="cv-spinner">
								<span class="spinner"></span>
							</div>
							
							<div class="nodata_flexBox">
								<h1>데이터가 없습니다.</h1>
							</div>
						</div>

						<div class="summary_card last">
							<div class="talk_sale_box">
								<p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
								<div class="talk_item first">
									<p class="emotion_talk " id="emotion_talk_setting" >
										<img src="/assets/images/main/sales_normal.png" alt="">
<%-- 										<c:choose> --%>
<%-- 											<c:when test="${empty salesRto}"> --%>
<!-- 												<img src="/assets/images/main/sales_normal.png" alt=""> -->
<%-- 											</c:when> --%>
<%-- 											<c:when test="${salesRto >= 50 }"> --%>
<!-- 												<img src="/assets/images/main/sales_verygood.png" alt="매우 좋음"> -->
<%-- 											</c:when> --%>
<%-- 											<c:when test="${salesRto <= 49 && salesRto >= 21  }"> --%>
<!-- 												<img src="/assets/images/main/sales_good.png" alt="좋음"> -->
<%-- 											</c:when> --%>
<%-- 											<c:when test="${salesRto <= 20 && salesRto >= -20  }"> --%>
<!-- 												<img src="/assets/images/main/sales_normal.png" alt="보통"> -->
<%-- 											</c:when> --%>
<%-- 											<c:when test="${salesRto <= -21 && salesRto >= -49  }"> --%>
<!-- 												<img src="/assets/images/main/sales_bad.png" alt="나쁨"> -->
<%-- 											</c:when> --%>
<%-- 											<c:when test="${salesRto <= -50 }"> --%>
<!-- 												<img src="/assets/images/main/sales_terrible.png" alt="매우 나쁨"> -->
<%-- 											</c:when> --%>
<%-- 										</c:choose> --%>
									</p>
									<p class="lb_emotion">매출 지역</p>
								</div>
								<div class="talk_item " id="talk_item_setting">
									<p class="txt_talk">매출 통계 데이터를<br>분석하는 중이에요.</p>
<%-- 									<c:choose> --%>
<%-- 										<c:when test="${empty salesRto}"> --%>
<!-- 											<p class="txt_talk">매출 통계 데이터를<br>분석하는 중이에요.</p> -->
<%-- 										</c:when> --%>
<%-- 										<c:when test="${salesRto >= 50 }"> --%>
<!-- 											<p class="txt_talk">매출이 엄~청<br>늘었어요! 야호^^</p> -->
<%-- 										</c:when> --%>
<%-- 										<c:when test="${salesRto <= 49 && salesRto >= 21  }"> --%>
<!-- 											<p class="txt_talk">이대로만 가즈아,<br>쭉~쭉쭉쭉!</p> -->
<%-- 										</c:when> --%>
<%-- 										<c:when test="${salesRto <= 20 && salesRto >= -20  }"> --%>
<!-- 											<p class="txt_talk">여기서 만족할 순<br>없겠죠?!</p> -->
<%-- 										</c:when> --%>
<%-- 										<c:when test="${salesRto <= -21 && salesRto >= -49  }"> --%>
<!-- 											<p class="txt_talk">에구, 좀 더<br>분발하셔야겠어요</p> -->
<%-- 										</c:when> --%>
<%-- 										<c:when test="${salesRto <= -50 }"> --%>
<!-- 											<p class="txt_talk">으쌰으쌰!<br>힘을내요, 슈퍼파월~!</p> -->
<%-- 										</c:when> --%>
<%-- 									</c:choose> --%>
								</div>
							</div>
							<div class="talk_advice_box">
								<p class="lb_advice">[도움말]</p>
								<p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>매출
										산출기준</b><br>
									판매몰에서 제공하는 부가매출신고내역에서 집계합니다.
									(단, 부가세매출신고내역에서 조회되지 않는 당월 매출은
									주문관리에서 집계합니다.)</p>
								<p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>공제
										산출기준</b><br>
									판매몰에서 제공하는 매입공제내역에서 집계합니다.</p>
								<p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간 선택은
									<b>1년단위</b>로 가능합니다.</p>
								<p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt=""><b>공제합계의
										비율(%)</b><br>
									공제합계, 마켓수수료,광고성수수료, 기타공제는
									모두 <b>매출금액 대비 비율</b>입니다.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 모바일버전을 위한 상단 swiper 추가 -->
			<!--960 반응형-->
			<div class="summary_report_section_m_Box" style="display:none;">
				<div class="swiper-container summary_report_section_m">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<div class="summary_report_section">
								<p class="title_summary open">[매출현황 요약보고]</p>
								<div class="summary_section_card">
									<div class="summary_card first">
										<div class="title_highlight">
											<p>당월 매출</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>
										
										<div class="graph_summary type1">
											<canvas id="spGh1_m" height="150"></canvas>
										</div>
<!-- 										<p class="desc_summary"> -->
<!-- 											19년 3월 매출은<br> -->
<!-- 											전월 대비 <span class="up">15% 증가</span>하였고,<br> -->
<!-- 											전년 동월대비 <span class="down">50% 감소</span>하였습니다. -->
<!-- 										</p> -->
									</div>

									<!--nodata_1-->
									<div class="summary_card first first_nodata top_nodata">
										<div class="title_highlight">
											<p class="first_nodata_title">당월 매출</p>
										</div>
										<div class="nodata_flexBox">
											<h1>데이터가 없습니다.</h1>
										</div>
									</div>

									<div class="summary_card second">
									
										<div class="title_highlight">
											<p>최근 1년 매출  </p>											
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>										
										<div class="graph_summary type2">
											<canvas id="spGh2_m" height="150"></canvas>
										</div>
<!-- 										<p class="desc_summary">최근 1년 중 매출은<br> -->
<!-- 											<span class="up">2018년 12월</span>이 <span class="up">3,629,349원</span>으로 가장 -->
<!-- 											높았고,<br> -->
<!-- 											<span class="down">2018년 4월</span>이 <span class="down">258,913원</span>으로 가장 -->
<!-- 											낮았으며,<br> -->
<!-- 											<span class="average">월 평균 매출</span>은 <span -->
<!-- 												class="average">1,286,072원</span> 입니다.</p> -->
									</div>

									<!--nodata_2-->
									<div class="summary_card second first_nodata top_nodata">
										<div class="title_highlight">
											<p class="second_nodata_title">최근 1년 매출</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>										
										<div class="nodata_flexBox">
											<h1>데이터가 없습니다.</h1>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="summary_report_section">
								<p class="title_summary open">[매출현황 요약보고]</p>
								<div class="summary_section_card">
									<div class="summary_card third">
										<div class="title_highlight">
											<p>조회기간 기준</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>
										
										<div class="table_summary">
											<div class="high_tb">
												<p class="lb">가장 높음</p>
												<div class="mall_box mb2">
													<p>G마켓</p>
												</div>
												<div class="mall_box">
													<p>G마켓 <br><span class="percent">12%</span></p>
												</div>
												<div class="mall_box">
													<p>G마켓 <br><span class="percent">15%</span></p>
												</div>
											</div>
											<div class="text_info_tb tb2">
												<div class="item_text_info">
													<p>판매몰별 매출</p>
												</div>
												<div class="item_text_info">
													<p>마켓수수료 공제율<br>(매출대비)</p>
												</div>
												<div class="item_text_info">
													<p>광고성 수수료 공제율<br>(매출대비)</p>
												</div>
											</div>
											<div class="low_tb">
												<p class="lb">가장 높음</p>
												<div class="mall_box mb2">
													<p>쿠팡</p>
												</div>
												<div class="mall_box">
													<p>쿠팡 <br><span class="percent">6%</span></p>
												</div>
												<div class="mall_box">
													<p>쿠팡 <br><span class="percent">3%</span></p>
												</div>
											</div>

										</div>
										<p class="desc_summary desc_summary_fontSize">
											- 판매몰별 매출은 <span class="up_style">G마켓</span>에서 가장 높았고, <span
												class="down_style">쿠팡</span>에서 가장 낮았습니다.<br />
											- 마켓유형별 판매는 <span class="color_style">오픈마켓</span>에서 주력하고 계십니다.<br />
											- 마켓수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span
												class="down_style">12%</span>로 가장 높았고,<br />
											<span class="up_style">쿠팡</span>에서 <span class="up_style">6%</span>로 가장
											낮았습니다.<br />
											- 광고성 수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span
												class="down_style">15%</span>로 가장 높았고,<br />
											<span class="up_style">쿠팡</span>에서 <span class="up_style">3%</span>로 가장
											낮았습니다.
										</p>
									</div>

									<!--nodata_3-->
									<div class="summary_card third first_nodata top_nodata">
										<div class="title_highlight">
											<p class="third_nodata_title">조회기간 기준 </p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>										
										<div class="nodata_flexBox">
											<h1>데이터가 없습니다.</h1>
										</div>
									</div>

									<div class="summary_card last">
										<div class="talk_sale_box">
											<p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
											<div class="talk_item first">
												<p class="emotion_talk">
													<img src="/assets/images/main/sales_normal.png" alt="">
												</p>
												<p class="lb_emotion">매출 지역</p>
											</div>
											<div class="talk_item">
												<p class="txt_talk">여기서 만족할 순<br>
													없겠죠?!</p>
											</div>
										</div>
										<div class="talk_advice_box">
											<p class="lb_advice">[도움말]</p>
											<p class="txt_advice"><img src="/assets/images/main/icon_tip.png"
													alt=""><b>매출 산출기준</b><br>
												판매몰에서 제공하는 부가매출신고내역에서 집계합니다.
												(단, 부가세매출신고내역에서 조회되지 않는 당월 매출은
												주문관리에서 집계합니다.)</p>
											<p class="txt_advice"><img src="/assets/images/main/icon_tip.png"
													alt=""><b>공제 산출기준</b><br>
												판매몰에서 제공하는 매입공제내역에서 집계합니다.</p>
											<p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간
												선택은 <b>1년단위</b>로 가능합니다.</p>
											<p class="txt_advice"><img src="/assets/images/main/icon_tip.png"
													alt=""><b>공제합계의 비율(%)</b><br>
												공제합계, 마켓수수료,광고성수수료, 기타공제는
												모두 <b>매출금액 대비 비율</b>입니다.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-pagination1">
				</div>
			</div>
			<!--320 반응형-->
			<div class="summary_report_section_m_Box_2" style="display:none;">
				<div class="swiper-container summary_report_section_m_2">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<div class="summary_report_section">
								<p class="title_summary open">[매출현황 요약보고]</p>
								<div class="summary_section_card">
									<div class="summary_card first">
										<div class="title_highlight">
											<p>당월 매출</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>
										
										<div class="graph_summary type1">
											<canvas id="spGh1_m_1" height="150"></canvas>
										</div>
<!-- 										<p class="desc_summary"> -->
<!-- 											19년 3월 매출은<br> -->
<!-- 											전월 대비 <span class="up">15% 증가</span>하였고,<br> -->
<!-- 											전년 동월대비 <span class="down">50% 감소</span>하였습니다. -->
<!-- 										</p> -->
									</div>

									<!--nodata_1-->
									<div class="summary_card first first_nodata top_nodata">
										<div class="title_highlight">
											<p class="first_nodata_title">당월 매출</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>										
										<div class="nodata_flexBox">
											<h1>데이터가 없습니다.</h1>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="summary_report_section">
								<p class="title_summary open">[매출현황 요약보고]</p>
								<div class="summary_section_card summary_section_card_display">

									<div class="summary_card second">
										<div class="title_highlight">
											<p>최근 1년 매출</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>
										
										<div class="graph_summary type2">
											<canvas id="spGh2_m_2" height="150"></canvas>
										</div>
<!-- 										<p class="desc_summary">최근 1년 중 매출은<br> -->
<!-- 											<span class="up">2018년 12월</span>이 <span class="up">3,629,349원</span>으로 가장 -->
<!-- 											높았고,<br> -->
<!-- 											<span class="down">2018년 4월</span>이 <span class="down">258,913원</span>으로 가장 -->
<!-- 											낮았으며,<br> -->
<!-- 											<span class="average">월 평균 매출</span>은 <span -->
<!-- 												class="average">1,286,072원</span> 입니다.</p> -->
									</div>

									<!--nodata_2-->
									<div class="summary_card second first_nodata top_nodata">
										<div class="title_highlight">
											<p class="second_nodata_title">최근 1년 매출</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>										
										<div class="nodata_flexBox">
											<h1>데이터가 없습니다.</h1>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="summary_report_section">
								<p class="title_summary open">[매출현황 요약보고]</p>
								<div class="summary_section_card">
									<div class="summary_card third">
										<div class="title_highlight">
											<p>조회기간 기준</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>
										<div class="table_summary">
											<div class="high_tb">
												<p class="lb">가장 높음</p>
												<div class="mall_box mb2">
													<p>G마켓</p>
												</div>
												<div class="mall_box">
													<p>G마켓 <br><span class="percent">12%</span></p>
												</div>
												<div class="mall_box">
													<p>G마켓 <br><span class="percent">15%</span></p>
												</div>
											</div>
											<div class="text_info_tb tb2">
												<div class="item_text_info">
													<p>판매몰별 매출</p>
												</div>
												<div class="item_text_info">
													<p>마켓수수료 공제율<br>(매출대비)</p>
												</div>
												<div class="item_text_info">
													<p>광고성 수수료 공제율<br>(매출대비)</p>
												</div>
											</div>
											<div class="low_tb">
												<p class="lb">가장 높음</p>
												<div class="mall_box mb2">
													<p>쿠팡</p>
												</div>
												<div class="mall_box">
													<p>쿠팡 <br><span class="percent">6%</span></p>
												</div>
												<div class="mall_box">
													<p>쿠팡 <br><span class="percent">3%</span></p>
												</div>
											</div>

										</div>
										<p class="desc_summary desc_summary_fontSize">
											- 판매몰별 매출은 <span class="up_style">G마켓</span>에서 가장 높았고, <span
												class="down_style">쿠팡</span>에서 가장 낮았습니다.<br />
											- 마켓유형별 판매는 <span class="color_style">오픈마켓</span>에서 주력하고 계십니다.<br />
											- 마켓수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span
												class="down_style">12%</span>로 가장 높았고,<br />
											<span class="up_style">쿠팡</span>에서 <span class="up_style">6%</span>로 가장
											낮았습니다.<br />
											- 광고성 수수료는 매출액 대비 <span class="down_style">G마켓</span>에서 <span
												class="down_style">15%</span>로 가장 높았고,<br />
											<span class="up_style">쿠팡</span>에서 <span class="up_style">3%</span>로 가장
											낮았습니다.
										</p>
									</div>

									<!--nodata_3-->
									<div class="summary_card third first_nodata top_nodata">
										<div class="title_highlight">
											<p class="third_nodata_title">조회기간 기준</p>
										</div>
										<div class="cv-spinner">
											<span class="spinner"></span>
										</div>										
										<div class="nodata_flexBox">
											<h1>데이터가 없습니다.</h1>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="summary_report_section">
								<p class="title_summary open">[매출현황 요약보고]</p>
								<div class="summary_section_card summary_section_card_display">
									<div class="summary_card last">
										<div class="talk_sale_box">
											<p class="title_talk">오늘의 셀러봇캐시 예보 소식입니다.</p>
											<div class="talk_item first">
												<p class="emotion_talk">
													<img src="/assets/images/main/sales_normal.png" alt="">
												</p>
												<p class="lb_emotion">매출 지역</p>
											</div>
											<div class="talk_item">
												<p class="txt_talk">여기서 만족할 순<br>
													없겠죠?!</p>
											</div>
										</div>
										<div class="talk_advice_box">
											<p class="lb_advice">[도움말]</p>
											<p class="txt_advice"><img src="/assets/images/main/icon_tip.png"
													alt=""><b>매출 산출기준</b><br>
												판매몰에서 제공하는 부가매출신고내역에서 집계합니다.
												(단, 부가세매출신고내역에서 조회되지 않는 당월 매출은
												주문관리에서 집계합니다.)</p>
											<p class="txt_advice"><img src="/assets/images/main/icon_tip.png"
													alt=""><b>공제 산출기준</b><br>
												판매몰에서 제공하는 매입공제내역에서 집계합니다.</p>
											<p class="txt_advice"><img src="/assets/images/main/icon_tip.png" alt="">기간
												선택은 <b>1년단위</b>로 가능합니다.</p>
											<p class="txt_advice"><img src="/assets/images/main/icon_tip.png"
													alt=""><b>공제합계의 비율(%)</b><br>
												공제합계, 마켓수수료,광고성수수료, 기타공제는
												모두 <b>매출금액 대비 비율</b>입니다.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-pagination2">
				</div>
			</div>

			<!--end-->
			<div class="period_graph_box">
				<div class="choice_mall_type">
					<!-- <span class="lb">쇼핑몰 선택</span> -->
					<select class="sctbox_mall" id="mall_cd">
						<option value="">전체</option>
						<c:forEach items="${mallList}" var="mall" varStatus="status">
							<option value="${mall.mall_cd }">${mall.mall_cd_nm }</option>
						</c:forEach>
					</select>
				</div>


				<div class="tui-datepicker-input tui-datetime-input tui-has-focus">
					<input type="text" id="sales_find_stt_dt" aria-label="Year-Month">
					<span class="tui-ico-date"></span>
				</div>
				<div class="datepicker-cell" id="datepicker-sales_find_stt_dt" style="margin-top: -1px;"></div>

				<h1 style="display:inline-block;display: inline-block;vertical-align: middle;font-size: 1rem;"> ~ </h1>

				<div class="tui-datepicker-input tui-datetime-input tui-has-focus">
					<input type="text" id="sales_find_end_dt" aria-label="Year-Month">
					<span class="tui-ico-date"></span>
				</div>
				<div class="datepicker-cell" id="datepicker-sales_find_end_dt" style="margin-top: -1px;"></div>

				<div class="btn_period_group">
					<button id="btnSearch">조회하기</button>
					<button id="btnRecentlyYear" type="button" class="btn_gray">최근 1년보기</button>
				</div>
			</div>
			<div class="contents_title">
				<h1>매출현황 그래프</h1>
				<h2>(단위: 건/천 원)</h2>
			</div>
			<div class="graph_bar_group">
				<div class="toggle_type_gh">
					<div class="toggle_type_box clearfix">
						<span class="focus">매출</span>
						<span>공제</span>
					</div>
				</div>
				<div class="label_gh label_gh_m" stlye="display:none;">
					<div class="item_label">
						<span class="circle"></span>
						<span class="txt_lb">매출건수</span>
					</div>
					<div class="item_label">
						<span class="color"></span>
						<span class="txt_lb">매출금액</span>
					</div>
					<div class="item_label">
						<span class="color cr2"></span>
						<span class="txt_lb">공제합계</span>
					</div>

				</div>
				<div class="graph_bar_box active bar_2">

					<div class="line_average" style="top: 65%;">
						<span>1,687,867원</span>
						<p></p>
					</div>
					<div class="line_average line_a2" style="top: 60%;">
						<span>6건</span>
						<p></p>
					</div>

					<div class="label_gh">
						<div class="item_label">
							<span class="circle"></span>
							<span class="txt_lb">매출건수</span>
						</div>
						<div class="item_label">
							<span class="color"></span>
							<span class="txt_lb">매출금액</span>
						</div>
						<div class="item_label">
							<span class="color cr2"></span>
							<span class="txt_lb">공제합계</span>
						</div>

					</div>


					<div class="barGh1_size_m_scroll_wrapper">
						<canvas class="barGh1_size_m" id="barGh1" width="1400" height="420"></canvas>
					</div>
				</div>
				<div class="graph_bar_box bar_2">
					<div class="label_gh">
						<div class="item_label">
							<span class="color cr3"></span>
							<span class="txt_lb">마켓 수수료</span>
						</div>
						<div class="item_label">
							<span class="color cr4"></span>
							<span class="txt_lb">광고성 수수료</span>
						</div>
						<div class="item_label">
							<span class="color cr5"></span>
							<span class="txt_lb">기타 공제</span>
						</div>
					</div>
					<div class="label_gh label_gh_m" style="display:none;">
						<div class="item_label">
							<span class="color cr3"></span>
							<span class="txt_lb">마켓 수수료</span>
						</div>
						<div class="item_label">
							<span class="color cr4"></span>
							<span class="txt_lb">광고성 수수료</span>
						</div>
						<div class="item_label">
							<span class="color cr5"></span>
							<span class="txt_lb">기타 공제</span>
						</div>
					</div>
					<canvas id="barGh2" width="1400" height="420"></canvas>
				</div>
			</div>
			<!--table start-->
			<div class="sale_table_wrap">
				<!--상단-->
				<div class="contents_title">
					<h1>매출현황 요약</h1>
					<h2>(단위: 건/천 원)</h2>
				</div>
				<div id="grid_1"></div>
				<div class="grid_noData" style="display: none;">
					<h1>데이터가 없습니다.</h1>
				</div>
				<!--하단-->
				<div class="contents_title">
					<h1>매출현황 월별 상세보기</h1>
					<h2>(단위: 건/천 원)</h2>
				</div>
				<div id="grid_2"></div>
				<div class="grid_noData_padding" style="display: none;">
					<div class="grid_noData">
						<h1>데이터가 없습니다.</h1>
					</div>
				</div>
			</div>
			<!--table end-->
		</div>
	</div>
</div>
<script>
	//var _summaryLast;
	var _summaryLast;
	var _yearMarket;
	var _monthMarket;
	// <c:if test="${not empty summaryLast }">
	// 	_summaryLast = ${summaryLast};
	// </c:if>
	// <c:if test="${not empty yearMarket }">
	// 	_yearMarket = ${yearMarket};
	// </c:if>
	// <c:if test="${not empty monthMarket }">
	// 	_monthMarket = ${monthMarket};
	// </c:if>

	var _salesChart;
	var _deductChart;

	// 2019-11-07 매출 건수를 지원하지 않는 몰
	// (030)쿠팡 (031)티몬(과거매출) (041)현대EDI (053)위메프2.0(과거매출
	var _not_sales_num_mall = ['030', '031', '041', '053'];
	function fnIsNotSalesNumMall() {
		var mall_cd = $("#mall_cd").val();
		if (isNull(mall_cd)) { return false; }

		for (var i in _not_sales_num_mall) {
			if (_not_sales_num_mall[i] == mall_cd) {
				return true;
			}
		}
		return false;
	}

	// 월 단위 차트 데이터
	function drawLastMonthView(id, dataList) {
		var div_summary_card = $("#" + id).parents(".summary_card.first");
		div_summary_card.empty();

		// 월
		var lastKrYymm = moment(new Date()).subtract(1, 'month').format("YY년 MMM");
		var html = [];
		html.push("  <div class=\"title_highlight\">");
		html.push("		<p>" + lastKrYymm + " 매출</p>");
		html.push("  </div>");
		html.push("  <div class=\"graph_summary type1\">");
		html.push("		<canvas id=\"" + id + "\" height=\"150\"></canvas>");
		html.push("  </div>");
		html.push("  <p class=\"desc_summary\">");
		html.push("	" + lastKrYymm + " 매출은<br>");

		var endText = dataList[1] == 0 ? "하였습니다." : "하였고,"

		if (dataList[0] > 0) {
			html.push("	전월 대비 <span class=\"up\">" + dataList[0] + "% 증가</span>" + endText + "<br>");
		} else if (dataList[0] < 0) {
			html.push("	전월 대비 <span class=\"down\">" + dataList[0] + "% 감소</span>" + endText + "<br>");
		}
		if (dataList[1] > 0) {
			html.push("	전년 동월대비 <span class=\"up\">" + dataList[1] + "% 증가</span>하였습니다.");
		} else if (dataList[1] < 0) {
			html.push("	전년 동월대비 <span class=\"down\">" + dataList[1] + "% 감소</span>하였습니다.");
		}

		html.push("  </p>");
		html.push("</div>");

		div_summary_card.html(html.join(""));

		var color = [];

		if (dataList[0] > 0) { color.push("#75ced2"); }
		else { color.push("#f4715b"); }

		if (dataList[1] > 0) { color.push("#75ced2"); }
		else { color.push("#f4715b"); }

		var max = Math.abs(dataList[0]) > Math.abs(dataList[1]) ? Math.abs(dataList[0]) : Math.abs(dataList[1]);

		// 차트 생성
		var ctx = document.getElementById(id).getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ['전월 대비', '전년 동월대비'],
				datasets: [{
					data: dataList,
					backgroundColor: color,
					hoverBackgroundColor: color,
					borderColor: color,
					hoverBorderColor: color
				}]
			},
			options: {
				responsive: false,
				maintainAspectRatio: false,
				layout: {
					padding: {
						top: 10
					}
				},
				legend: {
					display: false
				},
				title: {
					display: false
				},
				scales: {
					xAxes: [{
						gridLines: {
							display: false,
							beginAtZero: false,
							barThickness: 1000,
							color: "rgba(255,99,132,0)"
						}
					}],
					yAxes: [{
						ticks: {
							"max": max,
							"min": (max * -1),
							display: false,
							suggestedMin: 0,
						},
						gridLines: {
							display: true,
							drawBorder: false,
							beginAtZero: false,
							barThickness: 0,
							color: "rgba(255,99,132,0)"
						},
					}]
				},
				tooltips: {
					enabled: false,
				}
			}
		});

		var noData = $(".summary_card.first.first_nodata.top_nodata");
		if (dataList[0] == "0" && dataList[1] == "0") {
			div_summary_card.hide();
			noData.find(".title_highlight p").html(lastKrYymm + " 매출");
			noData.show();
		} else {
			div_summary_card.show();
			noData.hide();
		}
	}

	// 최근 1년 매출
	function drawLastYearView(id, labelList, dataList) {
		var div_summary_card = $("#" + id).parents(".summary_card.second");
		div_summary_card.empty();

		var moneyList = [];
		for (var i in dataList) {
			moneyList.push(fnAddComma(dataList[i]));
		}

		var html = [];
		html.push(" <div class=\"title_highlight\">");
		html.push("		<p>최근 1년 매출</p>");
		html.push(" </div>");
		html.push(" <div class=\"graph_summary type2\">");
		html.push("		<canvas id=\"" + id + "\" height=\"150\"></canvas>");
		html.push(" </div>");
		html.push(" <p class=\"desc_summary\">최근 1년 중 매출은<br>");

		if ((nonNull(dataList[0])) && (nonNull(dataList[1])) && labelList[0] != labelList[1]) {
			html.push("	<span class=\"up\">" + moment(labelList[0], "YYYYMM").format("YYYY년 MMM") + "</span>이 <span class=\"up\">" + moneyList[0] + "원</span>으로 가장 높았고,<br>");
			html.push("	<span class=\"down\">" + moment(labelList[1], "YYYYMM").format("YYYY년 MMM") + "</span>이 <span class=\"down\">" + moneyList[1] + "원</span>으로 가장 낮았으며,<br>");
			html.push("	<span class=\"average\">월 평균 매출</span>은 <span class=\"average\">" + moneyList[2] + "원</span> 입니다.</p>");
		}

		div_summary_card.html(html.join(""));

		// 데이터 날짜 형식 변환
		labelList[0] = moment(labelList[0], "YYYYMM").format("YYYY-MMM");
		labelList[1] = moment(labelList[1], "YYYYMM").format("YYYY-MMM");

		var ctx = document.getElementById(id).getContext('2d');
		var chart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: labelList,
				datasets: [{
					data: dataList,
					backgroundColor: ["#75ced2", "#f4715b", "#d6a7d1"],
					hoverBackgroundColor: ["#75ced2", "#f4715b", "#d6a7d1"],
					borderColor: ["#75ced2", "#f4715b", "#d6a7d1"],
					hoverBorderColor: ["#75ced2", "#f4715b", "#d6a7d1"]
				}]
			},
			options: {
				responsive: false,
				maintainAspectRatio: false,
				layout: {
					padding: {
						top: 10
					}
				},
				legend: {
					display: false
				},
				title: {
					display: false
				},
				scales: {
					xAxes: [{
						gridLines: {
							display: false,
						}
					}],
					yAxes: [{
						ticks: {
							display: false,
							max: Math.ceil(dataList[0] * 1.1)
						},
						gridLines: {
							display: false,
							drawBorder: false,
						},
					}]
				},
				tooltips: {
					enabled: false,
				}
			}
		});

		if ((dataList[0] == 0 && dataList[1] == 0) || (labelList[0] == labelList[1])) {
			div_summary_card.hide();
			$(".summary_card.second.first_nodata.top_nodata").show();
		} else {
			div_summary_card.show();
			$(".summary_card.second.first_nodata.top_nodata").hide();
		}
	}

	// 조회기간 기준
	function drawSeachView(yearMarket) {
		var isNoData = true;
		var div_summary_card = $(".summary_card.third:not(.top_nodata)");
		div_summary_card.empty();

		var html = [];
		html.push("  <div class=\"title_highlight\">");
		html.push("	<p>조회기간 기준 (" + $("#sales_find_stt_dt").val() + "~" + $("#sales_find_end_dt").val() + ")</p>");
		html.push("  </div>");
		html.push("  <div class=\"table_summary\">");
		html.push("	<div class=\"high_tb\">");
		html.push("	  <p class=\"lb\">가장 높음</p>");

		// 매출
		if (nonNull(yearMarket.max_sales_mall_cd_nm) && nonNull(yearMarket.min_sales_mall_cd_nm)
			&& yearMarket.max_sales_mall_cd_nm != yearMarket.min_sales_mall_cd_nm) {
			html.push("	  <div class=\"mall_box mb2\">");
			html.push("		<p>" + nvl(yearMarket.max_sales_mall_cd_nm, "") + "</p>");
			html.push("	  </div>");
			isNoData = false;
		}

		// 마켓수수료
		if (nonNull(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm)
			&& yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm
		) {
			html.push("	  <div class=\"mall_box \">");
			html.push("		<p>" + nvl(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm, "") + " <br><span class=\"percent\">" + nvl(yearMarket.max_sales_prc_per_sales_mk_fee_rto, 0) + "%</span></p>");
			html.push("	  </div>");
			isNoData = false;
		}

		// 광고성
		if (nonNull(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm)
			&& yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm
		) {
			html.push("	  <div class=\"mall_box \">");
			html.push("		<p>" + nvl(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm, "") + " <br><span class=\"percent\">" + nvl(yearMarket.max_sales_prc_per_sales_ad_fee_rto, 0) + "%</span></p>");
			html.push("	  </div>");
			isNoData = false;
		}

		html.push("	</div>");
		html.push("	<div class=\"text_info_tb tb2\">");

		// 매출
		if (nonNull(yearMarket.max_sales_mall_cd_nm) && nonNull(yearMarket.min_sales_mall_cd_nm)
			&& yearMarket.max_sales_mall_cd_nm != yearMarket.min_sales_mall_cd_nm
		) {
			html.push("	  <div class=\"item_text_info\">");
			html.push("		<p>판매몰별 매출</p>");
			html.push("	  </div>");
		}

		// 마켓수수료
		if (nonNull(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm)
			&& yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm
		) {
			html.push("	  <div class=\"item_text_info\">");
			html.push("		<p>마켓수수료 공제율<br>(매출대비)</p>");
			html.push("	  </div>");
		}

		// 광고성
		if (nonNull(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm)
			&& yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm
		) {
			html.push("	  <div class=\"item_text_info\">");
			html.push("		<p>광고성 수수료 공제율<br>(매출대비)</p>");
			html.push("	  </div>");
		}

		html.push("	</div>");
		html.push("	<div class=\"low_tb\">");
		html.push("	  <p class=\"lb\">가장 낮음</p>");

		// 매출
		if (nonNull(yearMarket.max_sales_mall_cd_nm) && nonNull(yearMarket.min_sales_mall_cd_nm)
			&& yearMarket.max_sales_mall_cd_nm != yearMarket.min_sales_mall_cd_nm
		) {
			html.push("	  <div class=\"mall_box mb2\">");
			html.push("		<p>" + nvl(yearMarket.min_sales_mall_cd_nm, "") + "</p>");
			html.push("	  </div>");
		}

		// 마켓수수료
		if (nonNull(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm)
			&& yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm
		) {
			html.push("	  <div class=\"mall_box \">");
			html.push("		<p>" + nvl(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm, "") + " <br><span class=\"percent\">" + nvl(yearMarket.min_sales_prc_per_sales_mk_fee_rto, 0) + "%</span></p>");
			html.push("	  </div>");
		}

		// 광고성
		if (nonNull(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm)
			&& yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm
		) {
			html.push("	  <div class=\"mall_box \">");
			html.push("		<p>" + nvl(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm, "") + " <br><span class=\"percent\">" + nvl(yearMarket.min_sales_prc_per_sales_ad_fee_rto, 0) + "%</span></p>");
			html.push("	  </div>");
		}

		html.push("	</div>");
		html.push("  </div>");
		html.push("  <p class=\"desc_summary desc_summary_fontSize\">");

		// 매출
		if (nonNull(yearMarket.max_sales_mall_cd_nm) && nonNull(yearMarket.min_sales_mall_cd_nm)
			&& yearMarket.max_sales_mall_cd_nm != yearMarket.min_sales_mall_cd_nm) {
			html.push("	- 판매몰별 매출은 <span class=\"up_style\">" + nvl(yearMarket.max_sales_mall_cd_nm, "") + "</span>에서 가장 높았고, <span class=\"down_style\">" + nvl(yearMarket.min_sales_mall_cd_nm, "") + "</span>에서 가장 낮았습니다.<br />");
		} 
 
		// 마켓 유형별 판매
		if (nonNull(yearMarket.max_sales_prc_market_typ_nm)) {
			html.push("	- 마켓유형별 판매는 <span class=\"color_style\">" + nvl(yearMarket.max_sales_prc_market_typ_nm, "") + "</span>에서 주력하고 계십니다.<br />");
		}

		// 마켓수수료
		if (nonNull(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm)
			&& yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm
		) {
			html.push("	- 마켓수수료는 매출액 대비 <span class=\"down_style\">" + nvl(yearMarket.max_sales_prc_per_sales_mk_fee_rto_mall_cd_nm, "") + "</span>에서 <span class=\"down_style\">" + nvl(yearMarket.max_sales_prc_per_sales_mk_fee_rto, 0) + "%</span>로 가장 높았고,<br />");
			html.push("	<span class=\"up_style\">" + nvl(yearMarket.min_sales_prc_per_sales_mk_fee_rto_mall_cd_nm, "") + "</span>에서 <span class=\"up_style\">" + nvl(yearMarket.min_sales_prc_per_sales_mk_fee_rto, 0) + "%</span>로 가장 낮았습니다.<br />");
		}

		// 광고 수수료
		if (nonNull(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm) && nonNull(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm)
			&& yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm != yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm
		) {
			html.push("	- 광고성 수수료는 매출액 대비 <span class=\"down_style\">" + nvl(yearMarket.max_sales_prc_per_sales_ad_fee_rto_mall_cd_nm, "") + "</span>에서 <span class=\"down_style\">" + nvl(yearMarket.max_sales_prc_per_sales_ad_fee_rto, 0) + "%</span>로 가장 높았고,<br />");
			html.push("	<span class=\"up_style\">" + nvl(yearMarket.min_sales_prc_per_sales_ad_fee_rto_mall_cd_nm, "") + "</span>에서 <span class=\"up_style\">" + nvl(yearMarket.min_sales_prc_per_sales_ad_fee_rto, 0) + "%</span>로 가장 낮았습니다.");
		}

		html.push("  </p>");

		if (isNoData) {
			$(".third_nodata_title").html("조회기간 기준 (" + $("#sales_find_stt_dt").val() + "~" + $("#sales_find_end_dt").val() + ")");
			div_summary_card.hide();
			$(".summary_card.third.first_nodata.top_nodata").show();
		} else {
			div_summary_card.show();
			div_summary_card.html(html.join(""));
			$(".summary_card.third.first_nodata.top_nodata").hide();
		}
	}

	// 매출 공제 차트
	function drawGraph(monthMarket) {
		if (!isNull(_salesChart)) {
			_salesChart.destroy();
		}
		if (!isNull(_deductChart)) {
			_deductChart.destroy();
		}

		var labelList = [];
		var dataTable = {
			"sales_prc": [], //매출금액
			"dedu_sum": [], // 공제합계
			"sales_num": [], // 매출건수
			"mk_fee": [], // 마켓 수수료
			"ad_fee": [], // 광고성 수수료
			"etc_dedu": [] // 기타 수수료
		}

		// 차트용 데이터 파싱
		var sales_month = monthMarket.sales_month;
		// 매출 건수가 없는 몰
		var isNotSalesNumMall = fnIsNotSalesNumMall();
		if (isNotSalesNumMall) {
			for (var i in sales_month) {
				labelList.push(moment(sales_month[i].sales_dt, "YYYYMM").format("YYYY-MM"));
				dataTable.sales_prc.push(nvl(sales_month[i].sales_prc, 0));
				dataTable.dedu_sum.push(nvl(sales_month[i].dedu_sum, 0));
				dataTable.mk_fee.push(nvl(sales_month[i].mk_fee, 0));
				dataTable.ad_fee.push(nvl(sales_month[i].ad_fee, 0));
				dataTable.etc_dedu.push(nvl(sales_month[i].etc_dedu, 0));
			}
		} else {
			for (var i in sales_month) {
				labelList.push(moment(sales_month[i].sales_dt, "YYYYMM").format("YYYY-MM"));
				dataTable.sales_prc.push(nvl(sales_month[i].sales_prc, 0));
				dataTable.dedu_sum.push(nvl(sales_month[i].dedu_sum, 0));
				dataTable.sales_num.push(nvl(sales_month[i].sales_num, 0));
				dataTable.mk_fee.push(nvl(sales_month[i].mk_fee, 0));
				dataTable.ad_fee.push(nvl(sales_month[i].ad_fee, 0));
				dataTable.etc_dedu.push(nvl(sales_month[i].etc_dedu, 0));
			}
		}

		var datasets = [
			{
				type: 'bar',
				label: '매출금액',
				yAxisID: 'A',
				data: dataTable.sales_prc,
				backgroundColor: "#4f69e8",
				hoverBackgroundColor: "#4f69e8",
				borderColor: "#4f69e8",
				hoverBorderColor: "#4f69e8"
			}, {
				type: 'bar',
				label: '공제합계',
				yAxisID: 'A',
				data: dataTable.dedu_sum,
				backgroundColor: "#bcdde2",
				hoverBackgroundColor: "#bcdde2",
				borderColor: "#bcdde2",
				hoverBorderColor: "#bcdde2"
			}];

		if (!isNotSalesNumMall) {
			// 매출 건수가 존재하는 몰의 경우 차트에 표시한다.
			datasets.unshift({
				type: 'line',
				label: '매출건수',
				yAxisID: 'B',
				data: dataTable.sales_num,
				backgroundColor: "#ff8db0",
				hoverBackgroundColor: "#ff8db0",
				borderColor: "#ff8db0",
				hoverBorderColor: "#ff8db0",
				fill: false
			});
			// 매출 건수가 존재하지 않는 경우 차트에서 제외한다.
			$(".label_gh").each(function () {
				$(this).find(".item_label").eq(0).show();
			});
		} else {
			// 매출 건수가 존재하지 않는 경우 차트에서 제외한다.
			$(".label_gh").each(function () {
				$(this).find(".item_label").eq(0).hide();
			});
		}

		// 매출
		var ctx1 = document.getElementById('barGh1').getContext('2d');
		// 공제
		var ctx2 = document.getElementById('barGh2').getContext('2d');

		var option1 = {
			responsive: false,
			maintainAspectRatio: false,
			layout: {
				padding: {
					top: 90,
					bottom: 20,
					right: 50,
					left: 90,
				}
			},
			legend: {
				display: false,
			},
			title: {
				display: false,
			},
			scales: {
				xAxes: [{
					barPercentage: 0.5,
					gridLines: {
						display: false,
					}
				}],
				yAxes: [{
					id: 'A',
					position: 'right',
					ticks: {
						maxTicksLimit: 5,
						beginAtZero: true,
						min: 0,
						scaleStartValue: 0,
						userCallback: function (value, index, values) {
							value = value.toString();
							value = value.split(/(?=(?:...)*$)/);
							value = value.join(',');
							return value;
						}
					},
					gridLines: {
						drawBorder: false,
					},
				},
				{
					id: 'B',
					position: 'left',
					ticks: {
						maxTicksLimit: 5,
						beginAtZero: true,
						min: 0,
						scaleStartValue: 0,
						userCallback: function (value, index, values) {
							value = value.toString();
							value = value.split(/(?=(?:...)*$)/);
							value = value.join(',');
							return value;
						}
					},
					gridLines: {
						drawBorder: false,
						drawOnChartArea: false,
					},
				}
				]
			},
			elements: {
				line: {
					tension: 0
				}
			},
			tooltips: {
				enabled: false
			}
		};

		_salesChart = new Chart(ctx1, {
			type: 'bar',
			data: {
				labels: labelList,
				"datasets": datasets
			},
			options: option1
		});

		option1.scales.yAxes = option1.scales.yAxes.slice(0, 1);

		_deductChart = new Chart(ctx2, {
			type: 'bar',
			data: {
				labels: labelList,
				"datasets": [{
					label: '마켓 수수료',
					yAxisID: 'A',
					data: dataTable.mk_fee,
					backgroundColor: "#c6a379",
					hoverBackgroundColor: "#c6a379",
					borderColor: "#c6a379",
					hoverBorderColor: "#c6a379"
				}, {
					label: '광고성 수수료',
					yAxisID: 'A',
					data: dataTable.ad_fee,
					backgroundColor: "#d3e7f0",
					hoverBackgroundColor: "#d3e7f0",
					borderColor: "#d3e7f0",
					hoverBorderColor: "#d3e7f0"
				}, {
					label: '기타 공제',
					yAxisID: 'A',
					data: dataTable.etc_dedu,
					backgroundColor: "#7097a8",
					hoverBackgroundColor: "#7097a8",
					borderColor: "#7097a8",
					hoverBorderColor: "#7097a8",
				}]
			},
			options: option1
		});
	}

	// 메츨현황 요약
	function fnSetSalesSummaryTable(monthMarket) {
		$("#grid_1").empty();

		// grid_noData
		if (isNull(monthMarket.sales_sum_num) && isNull(monthMarket.sales_sum_dedu_sum) && isNull(monthMarket.sales_avg_prc)) {
			$(".grid_noData").show();
			$("#grid_1").hide();
		} else {

			$(".grid_noData").hide();
			$("#grid_1").show();

			// 매출 건수가 없는 몰
			var isNotSalesNumMall = fnIsNotSalesNumMall();

			//첫번째
			var gridData = [{
				title1: '합계',
				contents1: fnAddComma(monthMarket.sales_sum_num, 0),
				contents2: fnAddComma(monthMarket.sales_sum_prc, 0),
				contents3: isNull(monthMarket.sales_sum_dedu_sum) ? "-" : fnAddComma(monthMarket.sales_sum_dedu_sum) + "(" + fnAddComma(monthMarket.sales_sum_prc_per_sales_sum_dedu_sum_rto) + "%)",
				contents4: isNull(monthMarket.sales_sum_mk_fee) ? "-" : fnAddComma(monthMarket.sales_sum_mk_fee) + "(" + fnAddComma(monthMarket.sales_sum_prc_per_sales_sum_mk_fee_rto) + "%)",
				contents5: isNull(monthMarket.sales_sum_ad_fee) ? "-" : fnAddComma(monthMarket.sales_sum_ad_fee) + "(" + fnAddComma(monthMarket.sales_sum_prc_per_sales_sum_ad_fee_rto) + "%)",
				contents6: isNull(monthMarket.sales_sum_etc_dedu) ? "-" : fnAddComma(monthMarket.sales_sum_etc_dedu) + "(" + fnAddComma(monthMarket.sales_sum_prc_per_sales_sum_etc_dedu_rto) + "%)"
			}, {
				title1: '평균',
				contents1: fnAddComma(nvl(monthMarket.sales_avg_num, 0)),
				contents2: fnAddComma(nvl(monthMarket.sales_avg_prc, 0)),
				contents3: isNull(monthMarket.sales_avg_dedu_sum) ? "-" : fnAddComma(monthMarket.sales_avg_dedu_sum) + "(" + fnAddComma(monthMarket.sales_avg_prc_per_sales_avg_dedu_sum_rto) + "%)",
				contents4: isNull(monthMarket.sales_avg_mk_fee) ? "-" : fnAddComma(monthMarket.sales_avg_mk_fee) + "(" + fnAddComma(monthMarket.sales_avg_prc_per_sales_avg_mk_fee_rto) + "%)",
				contents5: isNull(monthMarket.sales_avg_ad_fee) ? "-" : fnAddComma(monthMarket.sales_avg_ad_fee) + "(" + fnAddComma(monthMarket.sales_avg_prc_per_sales_avg_ad_fee_rto) + "%)",
				contents6: isNull(monthMarket.sales_avg_etc_dedu) ? "-" : fnAddComma(monthMarket.sales_avg_etc_dedu) + "(" + fnAddComma(monthMarket.sales_avg_prc_per_sales_avg_etc_dedu_rto) + "%)"
			}];

			var columns = [
				{
					header: '매출금액',
					name: 'contents2',
					align: 'right'
				},
				{
					header: '공제합계',
					name: 'contents3',
					align: 'right'
				},
				{
					header: '마켓수수료',
					name: 'contents4',
					align: 'right'
				},
				{
					header: '광고성 수수료',
					name: 'contents5',
					align: 'right'
				},
				{
					header: '기타공제',
					name: 'contents6',
					align: 'right'
				}
			];

			if (!isNotSalesNumMall) {
				columns.unshift({
					header: '매출건수',
					name: 'contents1',
					align: 'right'
				});
			}

			columns.unshift({
				header: $("#mall_cd option:selected").text(),
				name: 'title1'
			});

			var grid2 = new tui.Grid({
				el: document.getElementById('grid_1'),
				data: gridData,
				scrollX: true,
				scrollY: false,
				"columns": columns,
				columnOptions: {
					resizable: true
				}
			});
		}
	}

	// 매출 현황 월별 상세 보기
	function fnSetSalesMonthTable(monthMarket) {
		$("#grid_2").empty();

		if (isNull(monthMarket.sales_sum_num) && isNull(monthMarket.sales_sum_dedu_sum) && isNull(monthMarket.sales_avg_prc)) {
			$("#grid_2").hide();
			$(".grid_noData_padding").show();
		} else {

			$("#grid_2").show();
			$(".grid_noData_padding").hide();

			var isNotSalesNumMall = fnIsNotSalesNumMall();

			var rowsBase = [
				{ "code": "sales_prc", "name": "매출금액" },
				{ "code": "last_month_per_sales_prc", "name": "└전월대비" },
				{ "code": "past_year_same_month_per_sales_prc", "name": "└전년 동월대비" },
				{ "code": "dedu_sum", "name": "공제합계" },
				{ "code": "mk_fee", "name": "└마켓수수료" },
				{ "code": "ad_fee", "name": "└광고성 수수료" },
				{ "code": "etc_dedu", "name": "└기타공제" }
			];

			if (!isNotSalesNumMall) {
				rowsBase.unshift({ "code": "sales_num", "name": "매출건수" });
			}

			var sales_month = monthMarket.sales_month;
			var columns = [];
			columns.push({ "header": nvl(monthMarket.mall_cd_nm, "전체"), "name": "title" });
			for (var i in sales_month) {
				var column = { "header": moment(sales_month[i].sales_dt, "YYYYMM").format("YYYY-MM"), "name": sales_month[i].sales_dt, "align": 'right' };
				column.formatter = function (data) {
					if (data.row.title == "└전월대비" || data.row.title == "└전년 동월대비") {
						if (data.value.indexOf("-") > -1) {
							return "<span class='text-red'>" + data.value + "<span>";
						} else {
							return "<span class='text-blue'>+" + data.value + "<span>";
						}

					}
					return data.value;
				};
				columns.push(column);
			}

			moment(new Date()).subtract(1, 'month').format("YY년 MMM");
			var gridData = [];
			for (var i in rowsBase) {
				var row = { "title": rowsBase[i].name };
				$.each(sales_month, function () {

					if (rowsBase[i].code == "mk_fee") {
						row[this.sales_dt] = isNull(this[rowsBase[i].code]) ? "-" : (fnAddComma(this.sales_prc_per_mk_fee_rto, 0) + "%<br>" + fnAddComma(this[rowsBase[i].code]));
					} else if (rowsBase[i].code == "ad_fee") {
						row[this.sales_dt] = isNull(this[rowsBase[i].code]) ? "-" : (fnAddComma(this.sales_prc_per_ad_fee_rto, 0) + "%<br>" + fnAddComma(this[rowsBase[i].code]));
					} else if (rowsBase[i].code == "etc_dedu") {
						row[this.sales_dt] = isNull(this[rowsBase[i].code]) ? "-" : (fnAddComma(this.sales_prc_per_etc_dedu_rto, 0) + "%<br>" + fnAddComma(this[rowsBase[i].code]));
					} else {
						row[this.sales_dt] = isNull(this[rowsBase[i].code]) ? "-" : fnAddComma(this[rowsBase[i].code]);
					}

				});
				gridData.push(row);
			}

			var grid = new tui.Grid({
				el: document.getElementById('grid_2'),
				rowHeight: 60,
				data: gridData,
				scrollX: true,
				scrollY: false,
				"columns": columns,
				columnOptions: {
					resizable: true
				}
			});

			//grid mouseover
			$(".tui-grid-row-odd, .tui-grid-row-even").mouseover(function () {
				$(this).children("td").addClass("grid_background_color_1");
			});

			$(".tui-grid-row-odd, .tui-grid-row-even").mouseout(function () {
				$(this).children("td").removeClass("grid_background_color_1");
			});
		}
	}

	// 조회
	function fnSearch() {
		var sales_find_stt_dt = $("#sales_find_stt_dt").val();
		var sales_find_end_dt = $("#sales_find_end_dt").val();
		var param = {
			"sales_find_stt_dt": sales_find_stt_dt,
			"sales_find_end_dt": sales_find_end_dt,
		};
// 		var param = {
// 				"sales_find_stt_dt": "2019-09",
// 				"sales_find_end_dt": "2020-08",
// 			};

		
		
		
		if ($("#mall_cd").val() != "") {
			param.mall_cd = $("#mall_cd").val();
		}

		$.get("/sub/sales/year", $.param(param), function (res) {
			// 조회기간 기준
			drawSeachView(res);
		});

		$.get("/sub/sales/month", $.param(param), function (res) {
			// 매출 현황 그래프
			drawGraph(res);
			// 매출 현황 요약
			fnSetSalesSummaryTable(res);
			// 매출 월별 상세 보기
			fnSetSalesMonthTable(res);
		});
	}
		
    /*
     * 날짜포맷 yyyy-MM-dd 변환
     */
    function getFormatDate(date){
        var year = date.getFullYear();
        var month = (1 + date.getMonth());
        month = month >= 10 ? month : '0' + month;
        var day = date.getDate();
        day = day >= 10 ? day : '0' + day;
        // return year + '-' + month + '-' + day;
      return year + '-' + month;
    }




	$(document).ready(function () {
		
		$.ajax({
			type: 'GET',
			url: '/sub/sales/summaryLast',
		    beforeSend : function(xhr, opts) {
		    	$('.cv-spinner').show();
		    },			
			success: function(data){
				//console.log(data);
				var __summaryLast = data;
				// 월
				var dataList = [];
				dataList.push(nvl(__summaryLast.last_month_per_before_last_month_sales_rto + "", "0"));
				dataList.push(nvl(__summaryLast.last_year_same_month_sales_rto + "", "0"));
				drawLastMonthView("spGh1", dataList);
				drawLastMonthView("spGh1_m", dataList);
				drawLastMonthView("spGh1_m_1", dataList);

				// 년
				dataList = [];
				var labelList = [];
				labelList.push(__summaryLast.last_year_sales_max_month);
				labelList.push(__summaryLast.last_year_sales_min_month);
				labelList.push("월평균");
				dataList.push(nvl(__summaryLast.last_year_sales_max_prc, 0));
				dataList.push(nvl(__summaryLast.last_year_sales_min_prc, 0));
				dataList.push(nvl(__summaryLast.last_year_sales_avg_prc, 0));
				drawLastYearView("spGh2", labelList, dataList);
				drawLastYearView("spGh2_m", labelList, dataList);
				drawLastYearView("spGh2_m_2", labelList, dataList);
				
				
				var salesRto = data.last_month_per_before_last_month_sales_rto;
				var emotion_talk_setting = "";
				var talk_item_setting = "";
				
				
				if(salesRto >= 50 ){
					emotion_talk_setting="<img src=\"/assets/images/main/sales_verygood.png\" alt=\"매우 좋음\">";
					talk_item_setting ="<p class=\"txt_talk\">매출이 엄~청<br>늘었어요! 야호^^</p>";
				}else if(salesRto <= 49 && salesRto >= 21 ){
					emotion_talk_setting="<img src=\"/assets/images/main/sales_good.png\" alt=\좋음\">";
					talk_item_setting ="<p class=\"txt_talk\">이대로만 가즈아,<br>쭉~쭉쭉쭉!</p>";
				}else if(salesRto <= 20 && salesRto >= -20 ){
					emotion_talk_setting="<img src=\"/assets/images/main/sales_normal.png\" alt=\"보통\">";
					talk_item_setting ="<p class=\"txt_talk\">여기서 만족할 순<br>없겠죠?!</p>";
				}else if(salesRto <= -21 && salesRto >= -49 ){
					emotion_talk_setting="<img src=\"/assets/images/main/sales_bad.png\" alt=\"나쁨\">";
					talk_item_setting ="<p class=\"txt_talk\">에구, 좀 더<br>분발하셔야겠어요</p>";
				}else if(salesRto <= -50 ){
					emotion_talk_setting="<img src=\"/assets/images/main/sales_terrible.png\" alt=\"매우 나쁨\">";
					talk_item_setting ="<p class=\"txt_talk\">으쌰으쌰!<br>힘을내요, 슈퍼파월~!</p>";
				}
				if(emotion_talk_setting.length>0){
					$('#emotion_talk_setting').html(emotion_talk_setting);
				}
				if(talk_item_setting.length>0){
					$('#talk_item_setting').html(talk_item_setting);
				}	
			}
		}).done(function() {
			// $('#.cv-spinner').hide();
		});
	    var now = new Date();
	    var prevYearDate, prevMonthDate, toDay;

	    prevYearDate = new Date(now.getFullYear(), now.getMonth()-12, 1);
	    prevYearDate = getFormatDate(prevYearDate);
	    prevMonthDate = new Date(now.getFullYear(), now.getMonth(), 0);
	    prevMonthDate = getFormatDate(prevMonthDate);
	    
		$("#sales_find_stt_dt").val(prevYearDate);
		$("#sales_find_end_dt").val(prevMonthDate);
		
		fnSearch();
		
		var pickers = createRangeMonthPicker();

		function fnInit() {

			pickers.startpicker.on("change", function () {
				pickers.endpicker.setDate(moment(pickers.startpicker.getDate()).add(11, 'month').toDate());
			});

			pickers.endpicker.on("change", function () {
				pickers.startpicker.setDate(moment(pickers.endpicker.getDate()).subtract(11, 'month').toDate());
			});

			//custom tooltip
			Chart.plugins.register({
				afterDatasetsDraw: function (chartInstance, easing) {

					// To only draw at the end of animation, check for easing === 1
					var ctx = chartInstance.chart.ctx;
					chartInstance.data.datasets.forEach(function (dataset, i) {
						var meta = chartInstance.getDatasetMeta(i);
						var chartID = chartInstance.chart.canvas.id;
						if (!meta.hidden) {

							if (chartID.indexOf("spGh1") > -1) {

								meta.data.forEach(function (element, index) {
									// Draw the text in black, with the specified font
									ctx.fillStyle = '#000';
									var fontSize = 12;
									var fontStyle = 'normal';
									var fontFamily = '';
									var backgroundColor = 'rgba(79,105,232,0.2)';
									ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
									// Just naively convert to string for now
									var dataNum = dataset.data[index];
									var dataString = dataset.data[index].toString();
									// Make sure alignment settings are correct
									ctx.textAlign = 'center';
									ctx.textBaseline = 'middle';
									var padding = 5;
									var position = element.tooltipPosition();
									if (dataNum > 0) {
										ctx.fillText("▲" + dataString + "%", position.x, position.y + (fontSize / 2) + padding);
									} else {
										dataNum = dataNum.replace(/[^0-9]/g, '');
										ctx.fillText("▼" + dataNum + "%", position.x, position.y - (fontSize / 2) - padding);
									}
								});
							} else if (chartID.indexOf("spGh2") > -1) {
								// 최근 1년 매출
								meta.data.forEach(function (element, index) {
									// Draw the text in black, with the specified font
									ctx.fillStyle = '#000';
									var fontSize = 12;
									var fontStyle = 'normal';
									var fontFamily = '';
									var backgroundColor = 'rgba(79,105,232,0.2)';
									ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
									// Just naively convert to string for now
									var dataString = dataset.data[index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									// Make sure alignment settings are correct
									ctx.textAlign = 'center';
									ctx.textBaseline = 'middle';
									var padding = 0;
									var position = element.tooltipPosition();

									ctx.fillText(dataString + "원", position.x, position.y - (fontSize / 2) - padding);
								});
							} else {

								meta.data.forEach(function (element, index) {
									// Draw the text in black, with the specified font
									ctx.fillStyle = '#000';
									var fontSize = 12;
									var fontStyle = 'normal';
									var fontFamily = '';
									var backgroundColor = 'rgba(79,105,232,0.2)';
									ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
									// Just naively convert to string for now
									var data = dataset.data[index];
									var dataString = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									// Make sure alignment settings are correct
									ctx.textAlign = 'center';
									ctx.textBaseline = 'middle';
									var padding = 10;
									var position = element.tooltipPosition();

									if (data < 0) {
										ctx.fillText(dataString, position.x, 356);
									} else {
										ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
									}
								});
							}
						}
					});
				},
				beforeDatasetsDraw: function (chartInstance, easing) {
					var ctx = chartInstance.chart.ctx;
				}
			});

// 			// 월
// 			var dataList = [];
// 			dataList.push(nvl(_summaryLast.last_month_per_before_last_month_sales_rto + "", "0"));
// 			dataList.push(nvl(_summaryLast.last_year_same_month_sales_rto + "", "0"));
// 			drawLastMonthView("spGh1", dataList);
// 			drawLastMonthView("spGh1_m", dataList);
// 			drawLastMonthView("spGh1_m_1", dataList);

// 			// 년
// 			dataList = [];
// 			var labelList = [];
// 			labelList.push(_summaryLast.last_year_sales_max_month);
// 			labelList.push(_summaryLast.last_year_sales_min_month);
// 			labelList.push("월평균");
// 			dataList.push(nvl(_summaryLast.last_year_sales_max_prc, 0));
// 			dataList.push(nvl(_summaryLast.last_year_sales_min_prc, 0));
// 			dataList.push(nvl(_summaryLast.last_year_sales_avg_prc, 0));
// 			drawLastYearView("spGh2", labelList, dataList);
// 			drawLastYearView("spGh2_m", labelList, dataList);
// 			drawLastYearView("spGh2_m_2", labelList, dataList);

// 			// 조회기간 기준
// 			drawSeachView(_yearMarket);

// 			// 매출 현황 그래프
// 			drawGraph(_monthMarket);

// 			// 매출 현황 요약
// 			fnSetSalesSummaryTable(_monthMarket);

// 			// 매출 월별 상세 보기
// 			fnSetSalesMonthTable(_monthMarket);

			// 요약 이벤트
			var swiper = new Swiper('.summary_report_section_m', {
				pagination: {
					el: '.swiper-pagination1',
				},

			});

			var swiper = new Swiper('.summary_report_section_m_2', {
				pagination: {
					el: '.swiper-pagination2',
				},
			});
		}

		// 조회하기
		$("#btnSearch").on("click", function () {
			fnSearch();
		});

		// 최근 1년보기
		$("#btnRecentlyYear").on("click", function () {
			var today = new Date();
			pickers.endpicker.setDate(moment(today).subtract(1, 'month').toDate());
		});
		fnInit();
	});    
</script>