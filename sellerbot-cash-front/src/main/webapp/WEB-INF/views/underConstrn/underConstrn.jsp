<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="index,follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="naver-site-verification" content="324e62a14d4c3a13a7f875040390999cc936dc83" />
    <meta property="og:title" content="셀러봇캐시" />
    <meta property="og:url" content="https://www.sellerbot.co.kr" />
    <meta property="og:image" content="https://www.sellerbot.co.kr/attachments/sellerbot_logo_meta_image.png" />
    <meta property="og:description" content="정산예정금 통합관리서비스! 셀러봇캐시" />
    <link rel="canonical" href="URL입력">
    <title>Sellerbot</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/moonspam/NanumSquare@1.0/nanumsquare.css">
    <link rel="stylesheet" href="/assets/css/common.css">
    <link rel="stylesheet" href="/assets/css/page.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body>
   <div class="wra">
        <div class="header_wra">
            <div class="header_area">
                <a href="/" class="logo"><img src="/assets/images/common/sellerbot_Logo.png" alt=""></a>
            </div>
        </div>
        <div class="container">
            <div class="error_wra">
                <div class="error_area">
                    <div class="text_error">
                        <p class="error_tlt">서비스 점검 안내</p>
                        <p class="error_desc">
                            안녕하세요~ 셀러봇캐시예요.&nbsp;
                            <br><br>현재 6월 29일 (목) 저녁 7시부터 셀러봇캐시를 안정적으로 제공해드리기 위해 점검중입니다..&nbsp;
                            <br>이에 따른 영향은 아래 내용을 참고해주세요!&nbsp;
                            <br><br>▶ 내용 : 셀러봇캐시 점검&nbsp;
                            <br>- 영향 : 점검시간 동안 셀러봇캐시 홈페이지 접속 불가&nbsp;
                            <br><br>▶ 일시 : 2023년 6월 29일 (목) 오후 7시 (~ 완료 시까지, 1~2시간 소요 예상)&nbsp;
                            <br><br>항상 더 나은 서비스를 제공해드리기 위해 노력하겠습니다.&nbsp;
                            <br><br>감사합니다!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_wra">
            <div class="footer_area">
                <a href="/" class="footer_logo footer_logo_m"><img src="/assets/images/common/footer_logo.png"
                        alt=""></a>
                <div class="footer_info footer_info_pc">
                    (주) 온리원 | 대표이사 최호식 | 사업자 등록번호 220-88-21645 | 특허번호 10-1089183<br>
                    사업자정보 소프트웨어개발 | 소재지 경기도 하남시 미사대로 520 현대지식산업센터 한강미사2차 D동 5층 545호 <!--| 고객센터: 1666-8216-->
                </div>
                <div class="footer_info footer_info_m">
                    (주) 온리원 | 대표이사 최호식 | 사업자 등록번호 220-88-21645<br> 특허번호 10-1089183 ㅣ
                    사업자정보 소프트웨어개발<br> 소재지 경기도 하남시 미사대로 520 현대지식산업센터 한강미사2차 D동 5층 545호 <!--<br>고객센터: 1666-8216-->
                </div>
                <a href="/" class="footer_logo footer_logo_pc"><img src="/assets/images/common/footer_logo.png" alt=""></a>
            </div>
        </div>
    </div>
</body>

</html>