<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!--s:2200213 추가-->
<!-- <script src="/assets/js/jquery-3.3.1.min.js"></script> -->
<script src="/assets/js/jquery-confirm.min.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/jquery-confirm.min.css" />
<!--e:2200213 추가-->
<script>
	var gTodayModal = null;
	var gCybMonyModal = null;

	function changeMadalToday() {
		if (gCybMonyModal != null)
			gCybMonyModal.close();

		pop_today();
	}

	function changeMadalCybMony() {
		if (gTodayModal != null)
			gTodayModal.close();

		pop_store_cash();
	}

	// 오늘 받는 정산금
	function pop_today(idx) {
		var todaySettAccPrc = "${todaySettAcc.today_sett_acc_prc_sum}";
		if (todaySettAccPrc == "" || todaySettAccPrc == "0") {
			showAlert("오늘 받는 정산금이 0원입니다.");
			return;
		}

		var width = $(window).width();
		var height = $(window).height();

		var bw = '600';
		var bh = '50%';
		if (width < 600) bw = '98%';
		else bw = '600';

		if (height < 600) bh = '98%';  //모바일용
		else bh = '50%';               //PC 

		gTodayModal = $.confirm({
			theme: 'modal_box_wrap',
			buttons: {
				닫기: {
					btnClass: 'btn btn_gray2',
					action: function () {
						gTodayModal = null;
					}
				}
			},
			//	columnClass: 'col-md-6 col-md-offset-3',    				
			boxWidth: bw,
			boxHeight: bh,
			useBootstrap: false,
			title: '오늘 받는 정산금',
			content: '<div class="modal_popup position_r">' +
				'<button class="btn_tab btn_yllw" onclick="javascript:changeMadalCybMony();">판매몰 예치금</button>' +
				'	<div class="modal_con">' +
				'		<div class="pop_header pop_today fl_wrap">' +
				'			<h3 class="txt_fff">' + moment(new Date()).format("YYYY년 MMM DD일") + ' 기준</h3>' +
				'			<dl class="txt_fff fl_wrap">' +
				'				<dt class="txt_fff fl_wrap"><span class="fl_left">오늘 받는 정산금</span><span class="fl_right"><fmt:formatNumber value="${todaySettAcc.today_sett_acc_prc_sum}" pattern="#,###" />원</span></dt>' +
				'				<dd class="txt_fff fl_wrap"><span class="fl_left">오픈마켓<c:if test="${not empty openMarketLastDtStr and openMarketLastDtStr != \"\"}"> (${openMarketLastDtStr} 기준)</c:if></span><span class="fl_right"><fmt:formatNumber value="${todaySettAcc.today_sett_acc_open_prc_sum}" pattern="#,###" />원</span></dd>' +
				'				<dd class="txt_fff fl_wrap"><span class="fl_left">비오픈마켓<c:if test="${not empty noOpenMarketLastDtStr and noOpenMarketLastDtStr != \"\"}"> (${noOpenMarketLastDtStr} 기준)</c:if></span><span class="fl_right"><fmt:formatNumber value="${todaySettAcc.today_sett_acc_nopen_prc_sum}" pattern="#,###" />원</span></dd>' +
				'			</dl>' +
				'			<p class="txt_fff txt_s12 w80 position_c">※ 실제 입금받으실 금액과는 다소 차이가 있을 수 있습니다.</p>' +
				'		</div>' +
				'		<div class="pop_list01 scroll">' +
				'			<table>' +
				'				<c:forEach var="mall" items="${todaySettAccMallPrc}" varStatus="status">' +
				'					<tr>' +
				'						<th>' +
				'							<li class="txt_s14 txt_666"><img src="/assets/images/common/logo/logo${mall.key}.png"></li>' +
				'							<li><fmt:formatNumber value="${mall.value}" pattern="#,###" />원</li>' +
				'						</th>' +
				'						<td>' +
				'							<c:forEach var="today_list" items="${todaySettAcc.today_list}" varStatus="status">' +
				'								<c:if test="${mall.key eq today_list.mall_cd}">' +
				'									<p class="fl_wrap"><span class="fl_left">${today_list.mall_cert_1st_id}</span><span class="fl_right"><fmt:formatNumber value="${today_list.today_sett_acc_prc}" pattern="#,###" />원</span></p>' +
				'								</c:if>' +
				'							</c:forEach>' +
				'						</td>' +
				'					</tr>' +
				'				</c:forEach>' +
				'			</table>' +
				'		</div>' +
				'	</div>' +
				'</div>',
		});
	}

	// 판매몰 예치금
	function pop_store_cash(idx) {
		var cybMony = "${todaySettAcc.cyb_mony_sum}";
		if (cybMony == "" || cybMony == "0") {
			showAlert("판매몰 예치금 잔액이 0원입니다.");
			return;
		}

		var width = $(window).width();
		var height = $(window).height();

		var bw = '600';
		var bh = '50%';
		if (width < 600) bw = '98%';
		else bw = '600';

		if (height < 600) bh = '98%';  //모바일용
		else bh = '50%';               //PC 

		gCybMonyModal = $.confirm({
			theme: 'modal_box_wrap',
			buttons: {
				닫기: {
					btnClass: 'btn btn_gray2',
					action: function () {
						gCybMonyModal = null;
					}
				}
			},
			//	columnClass: 'col-md-6 col-md-offset-3',    				
			boxWidth: bw,
			boxHeight: bh,
			useBootstrap: false,
			title: '판매몰 예치금',
			content: '<div class="modal_popup position_r">' +
				'	<button class="btn_tab btn_blu" onclick="javascript:changeMadalToday();">오늘 받는 정산금</button>' +
				'	<div class="modal_con">' +
				'		<div class="pop_header pop_store_cash fl_wrap">' +
				'			<h3>${openMarketLastDtStr} 기준</h3>' +
				'			<dl class="fl_wrap">' +
				'				<dt class="fl_wrap"><span class="fl_left">판매몰 예치금</span><span class="fl_right"><fmt:formatNumber value="${todaySettAcc.cyb_mony_sum}" pattern="#,###" />원</span></dt>' +
				'			</dl>' +
				'			<p class="txt_s12 w80 position_c">※ 판매몰예치금은 오픈마켓에 적립된 금액이며 각 판매몰에서 언제든지 활용하실 수 있습니다.</p>' +
				'			<p class="txt_s12 w80 position_c">※ 괄호 안의 명칭은 각 판매몰에서 칭하는 판매몰예치금 성격의 명칭입니다. </p>' +
				'		</div>' +
				'		<div class="pop_list01 scroll">' +
				'			<table>' +
				'				<c:forEach var="mall" items="${todaySettAccMallCybMony}" varStatus="status">' +
				'					<tr>' +
				'						<th>' +
				'							<li class="txt_s14 txt_666"><img src="/assets/images/common/logo/logo${mall.key}.png"><span>(<c:out value="${cybMonyCdMap[mall.key]}"/>)</span></li>' +
				'							<li><fmt:formatNumber value="${mall.value}" pattern="#,###" />원</li>' +
				'						</th>' +
				'						<td>' +
				'							<c:forEach var="today_list" items="${cybMonyDescList}" varStatus="status">' +
				'								<c:if test="${mall.key eq today_list.mall_cd}">' +
				'									<p class="fl_wrap"><span class="fl_left">${today_list.mall_cert_1st_id}</span><span class="fl_right"><fmt:formatNumber value="${today_list.cyb_mony}" pattern="#,###" />원</span></p>' +
				'								</c:if>' +
				'							</c:forEach>' +
				'						</td>' +
				'					</tr>' +
				'				</c:forEach>' +
				'			</table>' +
				'		</div>' +
				'	</div>' +
				'</div>',
		});
	}
</script>
<div class="container">
	<!-- 공지사항 Start -->
	<c:if test="${fn:length(notiList) > 0}">
		<div class="n_banner_notice slide_banner width-38" style="overflow:hidden">
			<div class="swiper-wrapper">
				<c:forEach var="noti" items="${notiList }">
					<div class="swiper-slide">
						<a href="/pub/cs/notice?noti_seq_no=${noti.noti_seq_no}" class="link_banner font-size-125"> <img
								src="/assets/images/member/alram_icon.png" alt=""><span>[공지사항]</span> ${noti.title }</a>
					</div>
				</c:forEach>
			</div>
			<p class="close_banner"><img src="/assets/images/main/announce_X.png" alt=""></p>
		</div>
	</c:if>
	<!-- 공지사항 End -->
	<div class="main_wra">
		<div class="dash_main">
			<div class="main_layout">
				<div class="layout_md">
					<!-- 정산예정금 요약 Start -->
					<div class="n_card_main n_line1">
						<a href="/sub/settAcc/all" class="more_info_btn"><img src="/assets/images/main/vtn_viewmore.png"
								alt=""></a>
						<h1>정산예정금 요약</h1>
						<div class="summary_payment total">
							<p class="lb">합계<span>(오픈마켓+비오픈마켓)</span></p>
							<p class="num"><b>
									<fmt:formatNumber value="${settAccSche.sett_acc_pln_sum}" pattern="#,###" /></b>원
							</p>
						</div>
						<div class="summary_payment fl_wrap mt05">
							<div class="calculate fl_left">
								<dl>
									<dt class="pb05">오픈마켓<span>전일 대비</span></dt>
									<c:choose>
										<c:when test="${settAccSche.sett_acc_pln_open_sum_per_day > 0}">
											<dd class="accounts_plus mt15"><b>
													<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span class="gap">▲
													<fmt:formatNumber
														value="${settAccSche.sett_acc_pln_open_sum_per_day}"
														pattern="#,###" />원 </span>
										</c:when>
										<c:when test="${settAccSche.sett_acc_pln_open_sum_per_day < 0}">
											<dd class="accounts_minus mt15"><b>
													<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span class="gap">▼
													<fmt:formatNumber
														value="${settAccSche.sett_acc_pln_open_sum_per_day * -1}"
														pattern="#,###" />원</span>
										</c:when>
										<c:otherwise>
											<dd class="mt15"><b>
													<fmt:formatNumber value="${settAccSche.sett_acc_pln_open_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
											<dd>-&nbsp;&nbsp;&nbsp;</dd>
										</c:otherwise>
									</c:choose>
									</dd>
								</dl>
							</div>
							<div class="calculate fl_left">
								<dl>
									<dt class="pb05">비오픈마켓<span>전일 대비</span></dt>
									<c:choose>
										<c:when test="${settAccSche.sett_acc_pln_nopen_sum_per_day > 0}">
											<dd class="accounts_plus mt15"><b>
													<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span class="gap">▲
													<fmt:formatNumber
														value="${settAccSche.sett_acc_pln_nopen_sum_per_day}"
														pattern="#,###" />원 </span>
										</c:when>
										<c:when test="${settAccSche.sett_acc_pln_nopen_sum_per_day < 0}">
											<dd class="accounts_minus mt15"><b>
													<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span class="gap">▼
													<fmt:formatNumber
														value="${settAccSche.sett_acc_pln_nopen_sum_per_day * -1}"
														pattern="#,###" />원</span>
										</c:when>
										<c:otherwise>
											<dd class="mt15"><b>
													<fmt:formatNumber value="${settAccSche.sett_acc_pln_nopen_sum}"
														pattern="#,###" /></b><span class="txt_won">원</span><br>
												<span>-&nbsp;&nbsp;</span>
										</c:otherwise>
									</c:choose>
									</dd>
								</dl>
							</div>
						</div>
						<div class="update_info_box">
							<a class="icon_update"><img src="/assets/images/main/refresh.png" alt=""></a>
							<p>마지막 업데이트일자 ${settAccSche.scra_ts}</p>
						</div>
					</div>
					<!-- 정산예정금 요약 End -->
					<!-- 정산예정금 관리팁 Start -->
					<div class="n_tip_area position_r">
						<span class="n_ico_new"><img src="/assets/images/main/icon_new.png" alt="new"></span>
						<p class="n_tip"><a href="/sub/settAcc/calendar" class="btn_go">
								<c:if test="${tipCount ne 0}"><img src="/assets/images/common/logo/logo031.png"
										class="logo"></c:if> 정산예정금에 관한 관리팁이 <b>${tipCount}건</b> 있습니다. <img
									src="/assets/images/main/icon_arrow.png">
							</a></p>
					</div>
					<!-- 정산예정금 관리팁 End -->
					<!-- 미리정산서비스 Start -->
					<div class="n_card_main n_line2">
						<a href="/sub/pre_calculate/pre_calculate" class="more_info_btn"><img
								src="/assets/images/main/vtn_viewmore.png" alt="미리정산서비스"></a>
						<h1><span class="txt_red">미리</span> 정산 서비스</h1>
						<div class="n_ad_service mt30">
							<dl>
								<dt class="text_left"><b>미리 활용할 수 있는</b><br><span class="txt_666">정산예정금</span></dt>
								<dd class="fl_right">최대 <b>
										<fmt:formatNumber value="${preSett}" pattern="#,###" /><span
											class="txt_won">원</span></b></dd>
							</dl>
						</div>
					</div>
					<!-- 미리정산서비스 End -->
				</div>
				<!-- 셀러봇캐시 예보 소식 Start -->
				<div class="layout_sm">
					<div class="card_center">
						<div class="center_txt_box">
							<p class="intro_txt">오늘도 반가워요,<br>
								<c:out value="${fn:replace(sessionScope.cust.biz_nm, '주식회사', '')}" />님!</p>
							<p class="intro_sub_txt">오늘의 셀러봇캐시 예보 소식입니다.</p>
							<!-- 정산예정금 -->
							<div class="sale_emotion">
								<div class="icon_emotion">
									<span class="img_icon">
										<c:if test="${ empty settScheRto}">
											<img src="/assets/images/main/jungsan_normal.png" alt="보통">
										</c:if>
										<c:if test="${settScheRto >= 50 }">
											<img src="/assets/images/main/jungsan_verygood.png" alt="매우 많았음">
										</c:if>
										<c:if test="${settScheRto <= 49 && settScheRto >= 21  }">
											<img src="/assets/images/main/jungsan_good.png" alt="많았음">
										</c:if>
										<c:if test="${settScheRto <= 20 && settScheRto >= -20  }">
											<img src="/assets/images/main/jungsan_normal.png" alt="보통">
										</c:if>
										<c:if test="${settScheRto <= -21 && settScheRto >= -49  }">
											<img src="/assets/images/main/jungsan_bad.png" alt="적었음">
										</c:if>
										<c:if test="${settScheRto <= -50 }">
											<img src="/assets/images/main/jungsan_terrible.png" alt="매우 적었음">
										</c:if>
									</span>
									<p class="lb">정산예정금</p>
								</div>
								<c:if test="${ empty settScheRto}">
									<div class="n_tooltip_txt" alt="보통">정산예정금 데이터를<br>분석하고 있어요~</div>
								</c:if>
								<c:if test="${settScheRto >= 50 }">
									<div class="n_tooltip_txt" alt="매우 많았음">정말 대단해요~<br>제대로 한 건 하셨네요!</div>
								</c:if>
								<c:if test="${settScheRto <= 49 && settScheRto >= 21  }">
									<div class="n_tooltip_txt" alt="많았음">아싸라비용~<br>오늘도 힘차게 출발!</div>
								</c:if>
								<c:if test="${settScheRto <= 20 && settScheRto >= -20  }">
									<div class="n_tooltip_txt" alt="보통">여기서 이대로<br>멈출 순 없겠죠?</div>
								</c:if>
								<c:if test="${settScheRto <= -21 && settScheRto >= -49  }">
									<div class="n_tooltip_txt" alt="적었음">좋은 날이 있겠죠~<br>오늘도 파이팅!</div>
								</c:if>
								<c:if test="${settScheRto <= -50 }">
									<div class="n_tooltip_txt" alt="매우 적었음">포기는 배추 셀 때나<br>쓰는 말이에요~</div>
								</c:if>
							</div>
							<!-- 매출 -->
							<div class="sale_emotion">
								<div class="icon_emotion">
									<span class="img_icon">
										<c:if test="${ empty salesRto }">
											<img src="/assets/images/main/sales_normal.png" alt="보통">
										</c:if>
										<c:if test="${salesRto >= 50 }">
											<img src="/assets/images/main/sales_verygood.png" alt="매우 좋음">
										</c:if>
										<c:if test="${salesRto <= 49 && salesRto >= 21  }">
											<img src="/assets/images/main/sales_good.png" alt="좋음">
										</c:if>
										<c:if test="${salesRto <= 20 && salesRto >= -20  }">
											<img src="/assets/images/main/sales_normal.png" alt="보통">
										</c:if>
										<c:if test="${salesRto <= -21 && salesRto >= -49  }">
											<img src="/assets/images/main/sales_bad.png" alt="나쁨">
										</c:if>
										<c:if test="${salesRto <= -50 }">
											<img src="/assets/images/main/sales_terrible.png" alt="매우 나쁨">
										</c:if>
									</span>
									<p class="lb">매출지역</p>
								</div>
								<c:if test="${ empty salesRto }">
									<div class="n_tooltip_txt" alt="매우 좋음">매출 통계 데이터를<br>분석하는 중이에요.</div>
								</c:if>
								<c:if test="${salesRto >= 50 }">
									<div class="n_tooltip_txt" alt="매우 좋음">매출이 엄~청<br>늘었어요! 야호^^</div>
								</c:if>
								<c:if test="${salesRto <= 49 && salesRto >= 21  }">
									<div class="n_tooltip_txt" alt="좋음">이대로만 가즈아,<br>쭉~쭉쭉쭉!</div>
								</c:if>
								<c:if test="${salesRto <= 20 && salesRto >= -20  }">
									<div class="n_tooltip_txt" alt="보통">여기서 만족할 순<br>없겠죠?!</div>
								</c:if>
								<c:if test="${salesRto <= -21 && salesRto >= -49  }">
									<div class="n_tooltip_txt" alt="나쁨">에구, 좀 더<br>분발하셔야겠어요</div>
								</c:if>
								<c:if test="${salesRto <= -50 }">
									<div class="n_tooltip_txt" alt="매우나쁨">으쌰으쌰!<br>힘을내요, 슈퍼파월~!</div>
								</c:if>
							</div>
							<!-- 알림톡 신청 -->
							<c:if test="${empty alarm }">
								<div class="sale_emotion">
									<div class="n_tooltip_txt n_tooltip_txt_last">
										<a href="/sub/my/alarm" class="last_icon_text">셀러봇캐시 알림톡</a><br>신청할래요?
									</div>
									<div class="icon_emotion icon_emotion_last">
										<span class="img_icon img_icon_last">
										</span>
									</div>
								</div>
							</c:if>
						</div>
					</div>
				</div>
				<!-- 셀러봇캐시 예보 소식 End -->
				<div class="layout_md2">
					<div class="n_line3 fl_wrap mb05">
						<!-- 오늘 받는 정산금 Start -->
						<div class="n_card_main n_card_half fl_left">
							<span class="n_ico_new"><img src="../../assets/images/main/icon_new.png" alt="new"></span>
							<a href="#" class="more_info_btn" onclick="pop_today(); return false;"><img
									src="../../assets/images/main/vtn_viewmore.png" alt=""></a>
							<h2>오늘 받는 정산금<span class="txt_s14 block">(오픈마켓+비오픈마켓)</span></h2>
							<p class="today_cash">
								<c:if
									test="${empty todaySettAcc.today_sett_acc_prc_sum or todaySettAcc.today_sett_acc_prc_sum eq 0}">
									<b class="txt_000">-</b>
								</c:if>
								<c:if
									test="${not empty todaySettAcc.today_sett_acc_prc_sum and todaySettAcc.today_sett_acc_prc_sum ne 0}">
									<b class="txt_000">
										<fmt:formatNumber value="${todaySettAcc.today_sett_acc_prc_sum}"
											pattern="#,###" /></b><span class="txt_won">원</span>
								</c:if>
							</p>
						</div>
						<!-- 오늘 받는 정산금 End -->
						<!-- 판매몰예치금 잔액 Start -->
						<div class="n_card_main n_card_half fl_right">
							<span class="n_ico_new"><img src="../../assets/images/main/icon_new.png" alt="new"></span>
							<a href="#" class="more_info_btn" onclick="pop_store_cash(); return false;"><img
									src="../../assets/images/main/vtn_viewmore.png" alt=""></a>
							<h2>판매몰예치금 잔액</h2>
							<p class="today_cash"><b class="txt_000">
									<fmt:formatNumber value="${todaySettAcc.cyb_mony_sum}" pattern="#,###" /></b><span
									class="txt_won">원</span></p>
						</div>
						<!-- 판매몰예치금 잔액 End -->
					</div>
					<!-- 계좌 조회 현황 Start -->
					<div class="n_card_main n_line2 mb05">
						<a href="/sub/account/account" class="more_info_btn"><img
								src="/assets/images/main/vtn_viewmore.png" alt=""></a>
						<h1>계좌 조회 현황</h1>
						<div class="n_main_status fl_wrap">
							<ul>
								<li class="error">오류 ${cusAcct.err_acct_cnt }</li>
								<li class="warning">점검 ${cusAcct.ins_acct_cnt}</li>
								<li class="normal">정상 ${cusAcct.nor_acct_cnt }</li>
								<li class="all">전체 ${cusAcct.all_acct_cnt - cusAcct.del_acct_cnt }</li>
							</ul>
						</div>
						<div class="fl_wrap">
							<p class="bullet_txt fl_left"><span></span>총 잔액</p>
							<c:set var="totalPrice" value="0" />
							<c:forEach items="${cusAcct.acct }" var="acct" varStatus="index">
								<c:if test="${acct.del_yn eq 'N' }">
									<c:set var="totalPrice" value="${acct.acct_prc + totalPrice }" />
								</c:if>
							</c:forEach>
							<p class="fl_right txt_s24"><b class="price">
									<fmt:formatNumber value="${totalPrice}" pattern="#,###" /></b><span
									class="txt_won">원</span></p>
						</div>
						<c:choose>
							<c:when test="${(cusAcct.all_acct_cnt - cusAcct.del_acct_cnt) == 0 }">
								<!--계좌 조회 현황 type1)가입 후 계좌등록 전-->
								<div class="n_tip_link">
									<a href="/sub/my/account"><img src="/assets/images/main/tip_Icon.png"
											alt=""><span>계좌 등록</span>을 하시면 통장잔액과 정산금 내역 등을 간편하게 조회하실 수 있습니다.</a>
								</div>
							</c:when>
							<c:otherwise>
								<!--계좌 조회 현황 type2)가입 후 계좌등록 완료-->
								<div class="update_info_box">
									<a class="icon_update"><img src="/assets/images/main/refresh.png" alt=""
											style="vertical-align: baseline;"></a>
									<p>마지막 업데이트일자 <c:if test="${fn:length(cusAcct.acct) > 0 }">
											${cusAcct.acct[0].acct_prc_mod_ts }</c:if>
									</p>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
					<!-- 계좌 조회 현황 End -->
					<!-- 판매몰 조회 현황 Start -->
					<div class="n_card_main n_line4 card_slide">
						<a href="/sub/my/market" class="more_info_btn"><img src="/assets/images/main/vtn_viewmore.png"
								alt=""></a>
						<h1>판매몰 조회 현황</h1>
						<div class="n_main_status fl_wrap">
							<ul>
								<li class="error">오류 ${mallInfo.err_mall_cnt}</li>
								<li class="warning">점검 ${mallInfo.ins_mall_cnt}</li>
								<li class="normal">정상 ${mallInfo.nor_mall_cnt}</li>
								<li class="all">전체 ${mallInfo.all_mall_cnt - mallInfo.del_mall_cnt}</li>
							</ul>
						</div>
						<c:choose>
							<c:when test="${mallInfo.err_mall_cnt == 0}">
								<!--오류 없을때-->
								<div class="slide_accountBox">
									<div class="swiper-container slide_account">
										<div class="swiper-wrapper">
											<div class="swiper-slide">
												<div class="bg_gray_box alone">
													<p class="msg_normal_account">
														모든 판매몰이 <span>정상조회</span> 되고있습니다.
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-button-prev"></div>
									<div class="swiper-button-next"></div>
								</div>
							</c:when>
							<c:when test="${mallInfo.err_mall_cnt < 2 }">
								<!--오류 2개 이하시-->
								<div class="slide_accountBox">
									<div class="swiper-container slide_account">
										<div class="swiper-wrapper">
											<div class="swiper-slide">
												<div class="n_bg_gray_box">
													<c:forEach items="${mallInfo.mall }" var="list" varStatus="status">
														<c:if test="${list.cust_mall_sts_cd eq 'ERR'}">
															<div class="item_inquiry">
																<p class="lb word-block" style="width: 55%">
																	${list.mall_cd_nm }<c:if
																		test="${ !empty list.mall_cert_1st_id }">
																		(${list.mall_cert_1st_id})</c:if>
																</p>
																<p class="main_text_hover status_inquiry status_inquiry_1 again_chk error"
																	data-cust_mall_seq_no="${list.cust_mall_seq_no }"
																	data-title="${list.scra_err_cd_desc }">${empty
																	list.scra_err_cd_nm ? list.cust_mall_sts_cd_nm :
																	list.scra_err_cd_nm }</p>
															</div>
															<div class="item_inquiry last">
																<p class="lb word-block" style="width: 55%"></p>
																<p
																	class="main_text_hover status_inquiry again_chk error">
																</p>
															</div>
															<div class="n_pop_chk n_pop_chk_1">
																<p><a class="n_pop_chk_link" href="/sub/my/market">링크입니다
																		문안을 작성해주세요.</a></p>
																<span class="n_pop_chk_close"><img
																		src="/assets/images/main/main_close_btn.png"
																		alt="닫기" /></span>
															</div>
														</c:if>
													</c:forEach>
												</div>
											</div>
										</div>
									</div>
									<div class="swiper-button-prev swiper-button-prev_main"></div>
									<div class="swiper-button-next swiper-button-next_main"></div>
								</div>
							</c:when>
							<c:otherwise>
								<!--오류 2개 초과시-->
								<div class="slide_accountBox">
									<div class="swiper-container slide_account">
										<div class="swiper-wrapper">
											<c:set var="mallLength" value="${fn:length(mallErrorList)}" />
											<c:set var="idx" value="0" />
											<c:forEach begin="0" end="${fn:length(mallErrorList)}" step="2" var="list"
												varStatus="status">
												<c:if test="${status.index <= mallLength-1}">
													<div class="swiper-slide">
														<div class="n_bg_gray_box">
															<div class="item_inquiry">
																<p class="lb word-block" style="width: 55%">
																	${mallErrorList[status.index].mall_cd_nm }<c:if
																		test="${ !empty mallErrorList[status.index].mall_cert_1st_id }">
																		(${mallErrorList[status.index].mall_cert_1st_id})
																	</c:if>
																</p>
																<p class="status_inquiry status_inquiry_2 error again_chk"
																	data-cust_mall_seq_no="${mallErrorList[status.index].cust_mall_seq_no }"
																	data-title="${mallErrorList[status.index].scra_err_cd_desc }">
																	${empty mallErrorList[status.index].scra_err_cd_nm ?
																	mallErrorList[status.index].cust_mall_sts_cd_nm :
																	mallErrorList[status.index].scra_err_cd_nm }</p>
															</div>

															<div class="item_inquiry last">
																<p class="lb word-block" style="width: 55%">
																	${mallErrorList[status.index+1].mall_cd_nm }<c:if
																		test="${ !empty mallErrorList[status.index+1].mall_cert_1st_id }">
																		(${mallErrorList[status.index+1].mall_cert_1st_id})
																	</c:if>
																</p>
																<p class="status_inquiry status_inquiry_2 error again_chk"
																	data-cust_mall_seq_no="${mallErrorList[status.index+1].cust_mall_seq_no }"
																	data-title="${mallErrorList[status.index+1].scra_err_cd_desc }">
																	${empty mallErrorList[status.index+1].scra_err_cd_nm
																	? mallErrorList[status.index+1].cust_mall_sts_cd_nm
																	: mallErrorList[status.index+1].scra_err_cd_nm }</p>
															</div>
															<div class="n_pop_chk n_pop_chk_2">
																<p><a class="n_pop_chk_link" href="/sub/my/market">링크입니다
																		문안을 작성해주세요.</a></p>
																<span class="n_pop_chk_close"><img
																		src="/assets/images/main/main_close_btn.png"
																		alt="닫기" /></span>
															</div>
														</div>
													</div>
												</c:if>
												<c:set var="idx" value="${idx + 1}" />
											</c:forEach>
										</div>
									</div>
									<div class="swiper-button-prev"></div>
									<div class="swiper-button-next"></div>
								</div>
							</c:otherwise>
						</c:choose>
						<p class="n_tip_link">
							<img src="/assets/images/main/icon_tip.png" alt="">판매몰 정보변경은 <a
								href="/sub/my/market"><span>My Page &gt; 판매몰 관리</span></a>로 이동하세요.
						</p>
					</div>
					<!-- 판매몰 조회 현황 End -->


				</div>
				<div class="layout_bt mt10 fl_wrap">
					<!-- 매출 통계 Start -->
					<div class="layout_bt_l fl_wrap">
						<div class="n_card_main n_line4 n_card_half">
							<a href="/sub/sales/sales" class="more_info_btn"><img
									src="/assets/images/main/vtn_viewmore.png" alt=""></a>
							<h1>매출 통계</h1>
							<div class="bt_stats">
								<dl class="fl_wrap">
									<dt><b>${oneMonths}</b><br><span>전월 대비</span></dt>
									<c:choose>
										<c:when test="${empty salesMonthMarket.sales_month[11].sales_prc }">
											<dd><b>0</b><span class="txt_won">원</span><br>
												<span class="gap">-&nbsp;&nbsp;&nbsp;</span></dd>
										</c:when>
										<c:otherwise>
											<c:if
												test="${salesMonthMarket.sales_month[11].last_month_per_sales_prc >= 0}">
												<dd class="accounts_plus"><b>
														<fmt:formatNumber
															value="${salesMonthMarket.sales_month[11].sales_prc}"
															pattern="#,###" /></b><span class="txt_won">원</span><br>
													<span class="gap">▲
														<fmt:formatNumber
															value="${salesMonthMarket.sales_month[11].last_month_per_sales_prc}"
															pattern="#,###" />원</span>
												</dd>
											</c:if>
											<c:if
												test="${salesMonthMarket.sales_month[11].last_month_per_sales_prc < 0}">
												<dd class="accounts_minus"><b>
														<fmt:formatNumber
															value="${salesMonthMarket.sales_month[11].sales_prc}"
															pattern="#,###" /></b><span class="txt_won">원</span><br>
													<span class="gap">▼
														<fmt:formatNumber
															value="${salesMonthMarket.sales_month[11].last_month_per_sales_prc * -1}"
															pattern="#,###" />원</span>
												</dd>
											</c:if>
										</c:otherwise>
									</c:choose>
								</dl>
								<dl class="fl_wrap">
									<dt><b>${twoMonths }</b><br><span>전월 대비</span></dt>
									<c:choose>
										<c:when test="${empty salesMonthMarket.sales_month[10].sales_prc }">
											<dd><b>0</b><span class="txt_won">원</span><br>
												<span class="gap">-&nbsp;&nbsp;&nbsp;</span>
											</dd>
										</c:when>
										<c:otherwise>
											<c:if
												test="${salesMonthMarket.sales_month[10].last_month_per_sales_prc >= 0}">
												<dd class="accounts_plus"><b>
														<fmt:formatNumber
															value="${salesMonthMarket.sales_month[10].sales_prc}"
															pattern="#,###" /></b><span class="txt_won">원</span><br>
													<span class="gap">▲
														<fmt:formatNumber
															value="${salesMonthMarket.sales_month[10].last_month_per_sales_prc}"
															pattern="#,###" />원</span>
												</dd>
											</c:if>
											<c:if
												test="${salesMonthMarket.sales_month[10].last_month_per_sales_prc < 0}">
												<dd class="accounts_minus"><b>
														<fmt:formatNumber
															value="${salesMonthMarket.sales_month[10].sales_prc}"
															pattern="#,###" /></b><span class="txt_won">원</span><br>
													<span class="gap">▼
														<fmt:formatNumber
															value="${salesMonthMarket.sales_month[10].last_month_per_sales_prc * -1}"
															pattern="#,###" />원</span>
												</dd>
											</c:if>
										</c:otherwise>
									</c:choose>
								</dl>
							</div>
						</div>
						<!-- 매출 통계 End -->
						<!-- 과거 정산금 통계 Start -->
						<div class="n_card_main n_line4 n_card_half fl_right">
							<a href="/sub/past/past" class="more_info_btn"><img
									src="/assets/images/main/vtn_viewmore.png" alt=""></a>
							<h1>과거 정산금 통계</h1>
							<div class="bt_stats">
								<dl class="fl_wrap">
									<dt><b>${oneMonths}</b><br><span>전월 대비</span></dt>
									<c:choose>
										<c:when test="${empty settAccStatus.sett_acc_month[11].sett_acc_prc }">
											<dd><b>0</b><span class="txt_won">원</span><br>
												<span class="gap">-&nbsp;&nbsp;&nbsp;</span></dd>
										</c:when>
										<c:otherwise>
											<c:if
												test="${settAccStatus.sett_acc_month[11].last_month_per_sett_acc_prc >= 0}">
												<dd class="accounts_plus"><b>
														<fmt:formatNumber
															value="${settAccStatus.sett_acc_month[11].sett_acc_prc}"
															pattern="#,###" /></b><span class="txt_won">원</span><br>
													<span class="gap">▲
														<fmt:formatNumber
															value="${settAccStatus.sett_acc_month[11].last_month_per_sett_acc_prc}"
															pattern="#,###" />원</span>
											</c:if>
											<c:if
												test="${settAccStatus.sett_acc_month[11].last_month_per_sett_acc_prc < 0}">
												<dd class="accounts_minus"><b>
														<fmt:formatNumber
															value="${settAccStatus.sett_acc_month[11].sett_acc_prc}"
															pattern="#,###" /></b><span class="txt_won">원</span><br>
													<span class="gap">▼
														<fmt:formatNumber
															value="${settAccStatus.sett_acc_month[11].last_month_per_sett_acc_prc * -1}"
															pattern="#,###" />원</span>
											</c:if>
											</dd>

										</c:otherwise>
									</c:choose>
								</dl>
								<dl class="fl_wrap">
									<dt><b>${twoMonths }</b><br><span>전월 대비</span></dt>
									<c:choose>
										<c:when test="${empty settAccStatus.sett_acc_month[10].sett_acc_prc }">
											<dd><b>0</b><span class="txt_won">원</span><br>
												<span class="gap">-&nbsp;&nbsp;&nbsp;</span></dd>
										</c:when>
										<c:otherwise>
											<c:if
												test="${settAccStatus.sett_acc_month[10].last_month_per_sett_acc_prc >= 0}">
												<dd class="accounts_plus"><b>
														<fmt:formatNumber
															value="${settAccStatus.sett_acc_month[10].sett_acc_prc}"
															pattern="#,###" /></b><span class="txt_won">원</span><br>
													<span class="gap">▲
														<fmt:formatNumber
															value="${settAccStatus.sett_acc_month[10].last_month_per_sett_acc_prc}"
															pattern="#,###" />원</span>
											</c:if>
											<c:if
												test="${settAccStatus.sett_acc_month[10].last_month_per_sett_acc_prc < 0}">
												<dd class="accounts_minus"><b>
														<fmt:formatNumber
															value="${settAccStatus.sett_acc_month[10].sett_acc_prc}"
															pattern="#,###" /></b><span class="txt_won">원</span><br>
													<span class="gap">▼
														<fmt:formatNumber
															value="${settAccStatus.sett_acc_month[10].last_month_per_sett_acc_prc * -1}"
															pattern="#,###" />원</span>
											</c:if>
											</dd>
										</c:otherwise>
									</c:choose>
								</dl>
							</div>
						</div>
						<!-- 과거 정산금 통계 End -->
					</div>
					<div class="layout_bt_r fl_right fl_wrap">
						<!-- 반품 통계 Start -->
						<div class="n_card_main n_line5 n_card_half">
							<a href="/sub/return/return" class="more_info_btn"><img
									src="../../assets/images/main/vtn_viewmore.png" alt=""></a>
							<h1>반품 통계</h1>
							<div class="bt_return mt20">
								<p class="bullet_txt fl_wrap"><span></span>지난달 반품률</p>
								<dl>
									<dt><b>${oneMonths }</b></dt>
									<dd>
										<c:if test="${empty rtnSummaryLast.last_month_rtn_num_rto}"><b>0</b><span
												class="txt_won">%</span></c:if>
										<c:if test="${not empty rtnSummaryLast.last_month_rtn_num_rto}">
											<b>${rtnSummaryLast.last_month_rtn_num_rto }</b><span
												class="txt_won">%</span></c:if>
									</dd>
								</dl>
							</div>
						</div>
						<!-- 반품 통계 End -->
						<!-- 점프 서비스 Start -->
						<div class="n_card_main n_line5 n_card_half fl_right bg_yllw">
							<span class="n_ico_new"><img src="../../assets/images/main/icon_new.png" alt="new"></span>
							<a href="/sub/jump/service" class="more_info_btn"><img
									src="../../assets/images/main/vtn_viewmore_w.png" alt=""></a>
							<h1 class="txt_000">점프 서비스</h1>
							<div class="bt_jump">
								<p>
									<a href="/sub/jump/service#1" class="jump_store">판매몰</a>
									<a href="/sub/jump/service#2" class="jump_bank">금융사</a>
									<a href="/sub/jump/service#3" class="jump_care">쇼핑몰관리</a>
									<a href="/sub/jump/service#4" class="jump_domai">매입채널</a>
									<a href="/sub/jump/service#5" class="jump_public">관공서</a>
								</p>
							</div>
						</div>
						<!-- 점프 서비스 End -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 이벤트 모달 Start -->
<c:if test="${fn:length(eventList) > 0}">
	<div class="modal modal_event" style="display: none;">
		<div class="open_event_modal">
			<div class="event_text_box">
				<p class="sub_title"><span>&nbsp;</span></p>
				<p class="title"><span>${eventList[0].event_title }</span></p>
				<div class="msg_event">
					<c:out value="${eventList[0].event_cont }" escapeXml="false"></c:out>
				</div>
			</div>
			<div class="manage_modal_box">
				<input type="checkbox" class="day_close" id="dayClose2">
				<label for="dayClose2">하루동안 이 창을 열지 않음</label>
				<p class="close_chk_modal">[닫기]</p>
			</div>
		</div>
	</div>
</c:if>
<!-- 이벤트 모달 End -->
<!-- 플레이오토 알림톡 팝업창 start -->
<c:if test="${isExternalPopUp}">
<div class="modal modal_pla" id="modal_pla_0" style="display: none;">
	<div class="open_event_modal">
		<div class="event_text_box">
			<img class="close_pla_modal" src="/assets/images/main/event_close_btn.png" 
				style="float: right;
						position: absolute;
						top: 0px;
						right: 0px;
						padding-top: 10px;
						padding-right: 10px;
						width: 43px;
						height: 43px;
						cursor: pointer;" alt="닫기">
			<img src="/assets/images/main/pla/pla_account_pop.jpg" usemap="#accounting">
			<map name="accounting">
				<area shape="poly" coords="75,428,430,428,430,480,75,480" alt="판매몰" href="/sub/account/account">
			</map>
		</div>
		<div class="manage_modal_box">
			<p class="inf_day_close" data-popup-id="modal_pla_0"
				style="display: inline-block;
						vertical-align: sub;
						margin-right: 0.5em;
						cursor: pointer;
						text-decoration: underline; ">오늘 하루 보지 않기</p> 
			<p class="close_pla_modal" 
				style="font-size: 1em;
						color: #313035;
						float: right;
						margin: 0.3em 1em 0;
						cursor: pointer;
						text-decoration: underline;">닫기</p>
		</div>
	</div>
</div>
<div class="modal modal_pla" id="modal_pla_1" style="display: none;">
	<div class="open_event_modal">
		<div class="event_text_box">
			<img class="close_pla_modal" src="/assets/images/main/event_close_btn.png"
				style="float: right;
					position: absolute;
					top: 0px;
					right: 0px;
					padding-top: 10px;
					padding-right: 10px;
					width: 43px;
					height: 43px;
					cursor: pointer;" alt="닫기">
			<img src="/assets/images/main/pla/pla_market_pop.jpg" usemap="#marketing">
			<map name="marketing">
				<area shape="poly" coords="75,428,430,428,430,480,75,480" alt="판매몰" href="/sub/my/market">
			</map>
		</div>
		<div class="manage_modal_box">
			<p class="inf_day_close" data-popup-id="modal_pla_1"
				style="display: inline-block;
						vertical-align: sub;
						margin-right: 0.5em;
						cursor: pointer;
						text-decoration: underline; ">오늘 하루 보지 않기</p> 
			<p class="close_pla_modal" 
				style="font-size: 1em;
						color: #313035;
						float: right;
						margin: 0.3em 1em 0;
						cursor: pointer;
						text-decoration: underline;">닫기</p>
		</div>
	</div>
</div>
</c:if>
<!-- 플레이오토 알림톡 팝업창 end -->
<script>
	var swiper = new Swiper('.slide_account', {
		touchRatio: 0,
		spaceBetween: 30,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			720: {
				spaceBetween: 0
			},
		}
	});

	var swiper = new Swiper('.slide_banner', {
		//touchRatio: 0,
		allowTouchMove: true,
		autoplay: {
			delay: 5000,
		},
		direction: 'vertical',
		mousewheelControl: true,
		//spaceBetween: 30,
		loop: true,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		breakpoints: {
			720: {
				spaceBetween: 0
			},
		},
	});

	/*main*/
	// 2019-07-09 디자인 변경 적용 
	// 판매몰 조회 실패 모달창
	$(".status_inquiry.again_chk").click(function () {
		var jqThis = $(this);
		var title = jqThis.data("title");
		var cust_mall_seq_no = jqThis.data("cust_mall_seq_no");
		var item_inquiry = jqThis.parent(".item_inquiry");
		$(".n_pop_chk").hide();
		var pop_chk = item_inquiry.siblings(".n_pop_chk");
		var $link = pop_chk.find(".n_pop_chk_link");
		$link.text(title);
		var url = $link.attr("href") + "?" + $.param({ "cust_mall_seq_no": cust_mall_seq_no });
		$link.attr("href", url);
		pop_chk.show();
	});

	// 판매몰 조회 현황의 에러 메시지 설명 모달 닫기
	$(".n_pop_chk_close").click(function () {
		$(".n_pop_chk").hide();
	});

	//상단 배너 삭제시 메인 패딩 css변경
	if (matchMedia("screen and (max-width: 1024px)").matches) {
		$(".close_banner").click(function () {
			$(".main_wra").css("padding", "80px 20px 3em");
		});
	}

	// 이벤트 모달 닫기
	$(".close_chk_modal").click(function () {
		if ($("#dayClose2").is(":checked")) {
			setCookie("eventPop", true, 1);
		};
		$(".modal_event").hide();
	});

	function setCookie(cookieName, value, exdays) {
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var cookieValue = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toGMTString());
		document.cookie = cookieName + "=" + cookieValue;
	}

	function getCookie(cookieName) {
		cookieName = cookieName + '=';
		var cookieData = document.cookie;
		var start = cookieData.indexOf(cookieName);
		var cookieValue = '';
		if (start != -1) {
			start += cookieName.length;
			var end = cookieData.indexOf(';', start);
			if (end == -1) end = cookieData.length;
			cookieValue = cookieData.substring(start, end);
		}
		return unescape(cookieValue);
	}

	if (!getCookie("eventPop")) {
		$(".modal_event").show();
	}
</script>