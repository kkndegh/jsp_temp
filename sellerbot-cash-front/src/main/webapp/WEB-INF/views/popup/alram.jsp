<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<head>
    <link rel="stylesheet" href="/assets/css/popup/alram.css">
</head>

<div class="popup popup_alarm">
    <div class="pop">
        <span class="close_pop" target="popup_alarm"><img src="/assets/images/member/close_gray.png" alt=""></span>
        <div class="pop_body">
            <p id="alertText" class="desc_pop"></p>
        </div>
        <div class="pop_footer">
            <button type="button" class="close_btn" target="popup_alarm">확인</button>
        </div>
    </div>
</div>