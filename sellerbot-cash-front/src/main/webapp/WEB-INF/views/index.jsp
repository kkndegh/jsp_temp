<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://onlyone.co.kr/functions" prefix="sf"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:useBean id="server_toDay" class="java.util.Date" />

<!DOCTYPE html>
<html lang="ko">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="index,follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="naver-site-verification" content="324e62a14d4c3a13a7f875040390999cc936dc83" />
    <meta property="og:title" content="셀러봇캐시" />
    <meta property="og:url" content="https://www.sellerbot.co.kr" />
    <meta property="og:image" content="https://www.sellerbot.co.kr/attachments/sellerbot_logo_meta_image.png" />
    <meta property="og:description" content="정산예정금 통합관리서비스! 셀러봇캐시" />
    <link rel="canonical" href="URL입력">
    <title>Sellerbot</title>
    <!--font-->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/moonspam/NanumSquare@1.0/nanumsquare.css">
    <!--swiper-->
    <link rel="stylesheet" href="/assets/js/swiper-master/dist/css/swiper.min.css">
    <!--fullcalendar-->
    <link rel="stylesheet" href="/assets/js/fullcalendar/daygrid/main.css">
    <link rel="stylesheet" href="/assets/js/fullcalendar/core/main.css">
    <link rel="stylesheet" href="/assets/js/monthpicker/monthPicker.css">
    <!--style-->
    <link rel="stylesheet" href="/assets/css/jquery-ui.css">
    <link rel="stylesheet" href="/assets/js/select2/select2.css">

    <link rel="stylesheet" href="/assets/css/style.css?ver=20230126_01">
    <link rel="stylesheet" href="/assets/css/intro.css?ver=20211108_01">
    <!--js-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/jquery-ui.js"></script>
    <script src="/assets/js/moment.min.js"></script>
    <script src="/assets/js/anime.min.js"></script>

    <!-- 개발 공통 스크립트 -->
    <script src="/assets/js/common/modal.js?ver=20190916_01"></script>
    <script src="/assets/js/common/validation.js?ver=20230830_01"></script>

    <c:if test="${pageContext.request.serverName eq 'www.sellerbot.co.kr'}">
        <!-- Google Tag Manager 20210819 추가 -->
        <script> (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WW7KKMG'); 
        </script> 
        <!-- End Google Tag Manager -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138220660-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-138220660-2');
        </script>

        <!-- [네이버 로그 설치] 공통 적용 스크립트 -->
        <script type="text/javascript" src="https://wcs.naver.net/wcslog.js"></script>
        <script type="text/javascript">
            if (!wcs_add) var wcs_add = {};
            wcs_add["wa"] = "s_33132b2535c4";
            if (!_nasa) var _nasa = {};
            wcs.inflow();
            wcs_do(_nasa);
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            ! function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '2404404909682543');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=2404404909682543&ev=PageView&noscript=1" />
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Ads: 691347026 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-691347026"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', 'AW-691347026');
        </script>
    </c:if>
    
    <script>
        $(window).load(function () {
            $("#interpark_temp_event_layer_body").click(function(){
                location.href="/login";
            });
            $("#interpark_temp_event_layer_close_btn").click(function(){
                $("#interpark_temp_event_layer").hide();
            });
        });
    </script>

    <!-- Channel Plugin Scripts -->
    <script>
        (function() {
            var w = window;
            if (w.ChannelIO) {
                return (window.console.error || window.console.log || function(){})('ChannelIO script included twice.');
            }
            var ch = function() {
                ch.c(arguments);
            };
            ch.q = [];
            ch.c = function(args) {
                ch.q.push(args);
            };
            w.ChannelIO = ch;
            function l() {
                if (w.ChannelIOInitialized) {
                return;
                }
                w.ChannelIOInitialized = true;
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://cdn.channel.io/plugin/ch-plugin-web.js';
                s.charset = 'UTF-8';
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }
            if (document.readyState === 'complete') {
                l();
            } else if (window.attachEvent) {
                window.attachEvent('onload', l);
            } else {
                window.addEventListener('DOMContentLoaded', l, false);
                window.addEventListener('load', l, false);
            }
        })();
        ChannelIO('boot', {
            "pluginKey": "fd2b7ab5-b960-4df0-908a-e4a1c5693828"
        });
    </script>
    <!-- End Channel Plugin -->
</head>

<body>
    <!-- Google Tag Manager (noscript) 2021.08.19 추가 -->
    <noscript>
        <iframe src="<a href="https://www.googletagmanager.com/ns.html?id=GTM-WW7KKMG">https://www.googletagmanager.com/ns.html?id=GTM-WW7KKMG" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrap">
        <div class="a_frame">
            <c:if test="${sessionScope.siteCd != null && sessionScope.siteCd eq 'IPK'}">
                <!--
                <div id="interpark_temp_event_layer" style="position: absolute;width: 500px;height: 520px;top: 120px;left: 850px;z-index: 999; background-color: white;">
                    <img id="interpark_temp_event_layer_body" src="/assets/images/intro/interpark_intro_image.jpg" style="width:500px;height: 500px;float: right;cursor: pointer;"/>
                    <img id="interpark_temp_event_layer_close_btn" src="/assets/images/intro/interpark_btn_close.jpg" style="width:38px;height: 14px;float: right;cursor: pointer;margin-right: 10px;" />
                </div>
                -->
            </c:if>
            <div class="swiper-container main_slider swiper-function">
                <div class="swiper-wrapper">
                    <div class="swiper-slide sw_bg_1">
                        <div class="intro_header">
                            <p class="intro_header_logo">
                                <a href="/"><img src="/assets/images/intro/intro_logo.png" alt="logo" /></a>
                            </p>
                            <div class="intro_header_login">
                                <ul class="intro_header_login_wrap">
                                    <li><a href="/login">Login</a></li>
                                    <li><a href="/pub/member/step1">Join</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="slider_text_area">
                            <h1>우리<br />정산예정금<br />얼마지?</h1>
                            <h2>
                                셀러봇캐시가<br />
                                <b class="intro_blue">우주 최강 53개 쇼핑몰수</b>의 <b class="intro_blue">정산예정금</b>을<br />
                                <b class="intro_blue">한 곳</b>에서<b class="intro_blue"> 한 눈</b>에 보여드려요.<br />
                            </h2>
                            <p class="intro_use">
                                이용하기
                                <!-- <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2" style="enable-background:new 0 0 163.6 119.2;" xml:space="preserve"> -->
                                <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2"
                                    width="163.6" height="119.2" xml:space="preserve">
                                    <g>
                                        <path class="st0" d="M152.7,101.1c0,0-11.5-39.4-54.3-54.3S42.6,63.2,41.9,76.6c-0.4,7.4,0,32,31.3,32S102.6,72,101.1,62
                    c-1.5-10-10.5-30.4-40.6-43.5c-18.3-8-41.7-5.2-51.3-3.3" id="pointer"></path>
                                        <polyline class="st1" points="157,66.5 154,103.7 120.8,87.4 	">
                                        </polyline>
                                    </g>
                                </svg>
                            </p>
                        </div>
                        <ul class="sns_icons">
                            <li>
                                <a href=""></a>
                            </li>
                            <li>
                                <a href="https://blog.naver.com/only1fs" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCO_EjaAI9_j776oek4G8Qgw" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/sellerbotcash" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/sellerbotcash" target="_blank"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="swiper-slide sw_bg_4">
                        <div class="intro_header">
                            <p class="intro_header_logo">
                                <a href="/"><img src="/assets/images/intro/intro_logo.png" alt="logo" /></a>
                            </p>
                            <div class="intro_header_login">
                                <ul class="intro_header_login_wrap">
                                    <li><a href="/login">Login</a></li>
                                    <li><a href="/pub/member/step1">Join</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="slider_text_area">
                            <h1>우리<br />정산예정금<br />언제 받지?</h1>
                            <h2>
                                셀러봇캐시가<br />
                                <b class="intro_blue">일자별/판매몰별/배송상태별</b> 정산예정금과<br /> 
                                <b class="intro_blue">판매몰별 관리팁</b>을 꼼꼼히 챙겨서<br />
                                매일 아침 리포트를 드려요.
                            </h2>
                            <p class="intro_use">
                                이용하기
                                <!-- <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2" style="enable-background:new 0 0 163.6 119.2;" xml:space="preserve"> -->
                                <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2"
                                    width="163.6" height="119.2" xml:space="preserve">
                                    <g>
                                        <path class="st0" d="M152.7,101.1c0,0-11.5-39.4-54.3-54.3S42.6,63.2,41.9,76.6c-0.4,7.4,0,32,31.3,32S102.6,72,101.1,62
                c-1.5-10-10.5-30.4-40.6-43.5c-18.3-8-41.7-5.2-51.3-3.3" id="pointer"></path>
                                        <polyline class="st1" points="157,66.5 154,103.7 120.8,87.4 	">
                                        </polyline>
                                    </g>
                                </svg>
                            </p>
                        </div>
                        <div class="intro_hand">
                            <div class="intro_pre">
                                알림톡<br />체험하기
                                <!-- <svg version="1.1" class="svg_pre_icon" id="레이어_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 115.8 81" style="enable-background:new 0 0 115.8 81;" xml:space="preserve"> -->
                                <svg version="1.1" class="svg_pre_icon" id="레이어_1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 115.8 81"
                                    width="115.8" height="81" xml:space="preserve">
                                    <g>
                                        <path class="st3" d="M7.6,70.2c0,0,8.3-28.4,39.1-39.1S87,42.9,87.5,52.5c0.3,5.4,0,23-22.5,23S43.8,49.3,44.9,42
                        c1.1-7.2,7.5-21.9,29.2-31.3c13.2-5.8,30-3.8,37-2.4" />
                                        <polyline class="st4" points="4.6,45.3 6.7,72.1 30.6,60.4 	" />
                                    </g>
                                </svg>
                            </div>
                            <img src="/assets/images/intro/intro_hand.png" alt="" />
                        </div>
                        <!--index[4]_popup-->
                        <div class="pre_use_popup" style="display:none; ">
                            <div class="popup_display_none"></div>
                            <div class="middle_box">
                                <div class="middle_box_con_wrap">
                                    <div class="pre_use_popup_close">
                                        <div></div>
                                        <div></div>
                                    </div>
                                    <div class="middle_box_con">
                                        <p class="middle_box_img"><img src="/assets/images/intro/intro_ph.png" alt="" />
                                        </p>
                                        <div class="middle_box_right_con">
                                            <div class="middle_box_text_1">
                                                <h1>매일아침,<br />원하는 시간에 받아볼 수 있는<br /><b class="middle_box_blue">셀러봇캐시
                                                        알림톡</b></h1>
                                            </div>
                                            <div class="middle_box_text_2">
                                                <h1>
                                                    핸드폰 번호를 입력하시면 셀러봇캐시 알림톡 샘플 메세지가 발송됩니다!<br /> 입력하신 번호는 샘플 발송 외에 다른
                                                    목적으로 사용되지 않습니다.
                                                </h1>
                                            </div>
                                            <div class="ph_input_wrap">
                                                <input type="text" maxlength="11" id="tgt_cust_no" class="middle_box_ph"
                                                    placeholder="핸드폰 번호를 입력해주세요." />
                                                <button class="submit_ph"></button>
                                                <p id="tgt_cust_no_msg">휴대폰 번호를 정확히 입력하세요.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--popup_end-->
                        <ul class="sns_icons">
                            <li>
                                <a href=""></a>
                            </li>
                            <li>
                                <a href="https://blog.naver.com/only1fs" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCO_EjaAI9_j776oek4G8Qgw" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/sellerbotcash" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/sellerbotcash" target="_blank"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="swiper-slide sw_bg_5">
                        <div class="intro_header">
                            <p class="intro_header_logo">
                                <a href="/"><img src="/assets/images/intro/intro_logo.png" alt="logo" /></a>
                            </p>
                            <div class="intro_header_login">
                                <ul class="intro_header_login_wrap">
                                    <li><a href="/login">Login</a></li>
                                    <li><a href="/pub/member/step1">Join</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="slider_text_area">
                            <h1>우리<br />매출현황은<br />어때?</h1>
                            <h2>
                                셀러봇캐시가<br />
                                <b class="intro_blue">매출, 정산금, 반품 통계</b><br />
                                <b class="intro_blue">동종업계 매출추이</b>를 분석해서 보고드려요.<br />
                            </h2>
                            <p class="intro_use">
                                이용하기
                                <!-- <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2" style="enable-background:new 0 0 163.6 119.2;" xml:space="preserve"> -->
                                <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2"
                                    width="163.6" height="119.2" xml:space="preserve">
                                    <g>
                                        <path class="st0" d="M152.7,101.1c0,0-11.5-39.4-54.3-54.3S42.6,63.2,41.9,76.6c-0.4,7.4,0,32,31.3,32S102.6,72,101.1,62
                       c-1.5-10-10.5-30.4-40.6-43.5c-18.3-8-41.7-5.2-51.3-3.3" id="pointer"></path>
                                        <polyline class="st1" points="157,66.5 154,103.7 120.8,87.4 	">
                                        </polyline>
                                    </g>
                                </svg>
                            </p>
                        </div>
                        <ul class="sns_icons">
                            <li>
                                <a href=""></a>
                            </li>
                            <li>
                                <a href="https://blog.naver.com/only1fs" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCO_EjaAI9_j776oek4G8Qgw" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/sellerbotcash" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/sellerbotcash" target="_blank"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="swiper-slide sw_bg_5 type_2">
                        <div class="intro_header">
                            <p class="intro_header_logo">
                                <a href="/"><img src="/assets/images/intro/intro_logo.png" alt="logo" /></a>
                            </p>
                            <div class="intro_header_login">
                                <ul class="intro_header_login_wrap">
                                    <li><a href="/login">Login</a></li>
                                    <li><a href="/pub/member/step1">Join</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="slider_text_area">
                            <h1>우리<br />정산금 제대로<br />받긴 받은거니?</h1>
                            <h2>
                                셀러봇캐시가<br />
                                <b class="intro_blue">정산계좌 잔액</b>과 <b class="intro_blue">입금된 정산금</b>을 한곳에서 정리하고<br />
                                주문과 입금을 <b class="intro_blue">자동으로 매칭</b>시켜드려요.<br />
                            </h2>
                            <p class="intro_use">
                                이용하기
                                <!-- <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2" style="enable-background:new 0 0 163.6 119.2;" xml:space="preserve"> -->
                                <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2"
                                    width="163.6" height="119.2" xml:space="preserve">
                                    <g>
                                        <path class="st0" d="M152.7,101.1c0,0-11.5-39.4-54.3-54.3S42.6,63.2,41.9,76.6c-0.4,7.4,0,32,31.3,32S102.6,72,101.1,62
                       c-1.5-10-10.5-30.4-40.6-43.5c-18.3-8-41.7-5.2-51.3-3.3" id="pointer"></path>
                                        <polyline class="st1" points="157,66.5 154,103.7 120.8,87.4 	">
                                        </polyline>
                                    </g>
                                </svg>
                            </p>
                        </div>
                        <ul class="sns_icons">
                            <li>
                                <a href=""></a>
                            </li>
                            <li>
                                <a href="https://blog.naver.com/only1fs" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCO_EjaAI9_j776oek4G8Qgw" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/sellerbotcash" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/sellerbotcash" target="_blank"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="swiper-slide sw_bg_6">
                        <div class="intro_header">
                            <p class="intro_header_logo">
                                <a href="/"><img src="/assets/images/intro/intro_logo.png" alt="logo" /></a>
                            </p>
                            <div class="intro_header_login">
                                <ul class="intro_header_login_wrap">
                                    <li><a href="/login">Login</a></li>
                                    <li><a href="/pub/member/step1">Join</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="slider_text_area">
                            <h1>우리<br />정산금<br />땡겨 받을 수는 없는거니?</h1>
                            <h2>
                                셀러봇캐시가<br />
                                정산예정금을 활용하는 <b class="intro_blue">미리 정산 TIP</b>을 드려요.<br />
                                이제 내게 맞는 <b class="intro_blue">금융 서비스</b>를 내가 <b class="intro_blue">직접 선택</b>해서 당당하게 활용하세요!<br />
                            </h2>
                            <p class="intro_use">
                                이용하기
                                <!-- <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2" style="enable-background:new 0 0 163.6 119.2;" xml:space="preserve"> -->
                                <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2"
                                    width="163.6" height="119.2" xml:space="preserve">
                                    <g>
                                        <path class="st0" d="M152.7,101.1c0,0-11.5-39.4-54.3-54.3S42.6,63.2,41.9,76.6c-0.4,7.4,0,32,31.3,32S102.6,72,101.1,62
                    c-1.5-10-10.5-30.4-40.6-43.5c-18.3-8-41.7-5.2-51.3-3.3" id="pointer"></path>
                                        <polyline class="st1" points="157,66.5 154,103.7 120.8,87.4 	">
                                        </polyline>
                                    </g>
                                </svg>
                            </p>
                        </div>
                        <ul class="sns_icons">
                            <li>
                                <a href=""></a>
                            </li>
                            <li>
                                <a href="https://blog.naver.com/only1fs" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCO_EjaAI9_j776oek4G8Qgw" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/sellerbotcash" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/sellerbotcash" target="_blank"></a>
                            </li>
                        </ul>
                    </div>

                    <!-- 
                        20210903 : 2번째 슬라이드에서 6번째 슬라이드로 변경 
                        sw_bg_2 클래스 변경 할려면 css 작업을 해야 하므로 기존 값으로 유지함.
                    -->
                    <div class="swiper-slide sw_bg_2">
                        <div class="intro_header">
                            <p class="intro_header_logo">
                                <a href="/"><img src="/assets/images/intro/intro_logo.png" alt="logo" /></a>
                            </p>
                            <div class="intro_header_login">
                                <ul class="intro_header_login_wrap">
                                    <li><a href="/login">Login</a></li>
                                    <li><a href="/pub/member/step1">Join</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="slider_text_area">
                            <h1>우리<br />쇼핑몰<br />어디가 있지?</h1>
                            <h2>
                                셀러봇캐시가<br />
                                <b class="intro_blue">우주에서 가장 많은 53개 쇼핑몰수</b>의<br /> 
                                정산예정금을 보여 드려요.
                            </h2>
                        </div>
                        <div class="intro_mall_pc">
                            <c:if test="${!empty bannerB4DDList }">
                                <c:forEach items="${bannerB4DDList}" var="data" varStatus="status">
                                    <img src="/imagefile/${data.stor_path}${data.stor_file_nm}" alt="" />
                                </c:forEach>
                            </c:if>
                        </div>
                        <div class="intro_mall_mo">
                            <c:if test="${!empty bannerB4MMList }">
                                `<c:forEach items="${bannerB4MMList}" var="data" varStatus="status">
                                    <img src="/imagefile/${data.stor_path}${data.stor_file_nm}" alt="" />
                                </c:forEach>
                            </c:if>
                        </div>
                        <!--popup_end-->
                        <ul class="sns_icons">
                            <li>
                                <a href=""></a>
                            </li>
                            <li>
                                <a href="https://blog.naver.com/only1fs" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCO_EjaAI9_j776oek4G8Qgw" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/sellerbotcash" target="_blank"></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/sellerbotcash" target="_blank"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="swiper-slide sw_bg_7">
                        <div class="intro_header">
                            <p class="intro_header_logo">
                                <a href="/"><img src="/assets/images/intro/intro_logo_c.png" alt="logo" /></a>
                            </p>
                            <div class="intro_header_login">
                                <ul class="intro_header_login_wrap intro_header_login_wrap_last_page">
                                    <li><a style="color:#333333" href="/login">Login</a></li>
                                    <li><a style="color:#333333" href="/pub/member/step1">Join</a></li>
                                </ul>
                            </div>
                        </div>
                        <link rel="stylesheet" href="../../assets/css/jquery.bxslider.css">
                        <script type="text/javascript" src="../../assets/js/jquery.bxslider.js"></script>
                        <div class="slider_text_area slider_text_area_margin">
                            <div class="new_vid_slider">
                                <div class="vid_slider_items">
                                    <iframe class="" width="100%" height="312"
                                        src="https://www.youtube.com/embed/gr1TWqaR3Rs?enablejsapi=1&version=3&playerapiid=ytplayer"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>
                                <div class="vid_slider_items">
                                    <iframe class="" width="100%" height="312"
                                        src="https://www.youtube.com/embed/5xWbULaRSMQ?enablejsapi=1&version=3&playerapiid=ytplayer"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>
                                <div class="vid_slider_items">
                                    <iframe class="" width="100%" height="312"
                                        src="https://www.youtube.com/embed/TKBJ0wrzXEA?enablejsapi=1&version=3&playerapiid=ytplayer"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>
                                <div class="vid_slider_items">
                                    <iframe class="" width="100%" height="312"
                                        src="https://www.youtube.com/embed/g3uF-juNnC8?enablejsapi=1&version=3&playerapiid=ytplayer"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>

                                </div>
                                <div class="vid_slider_items">
                                    <iframe class="" width="100%" height="312"
                                        src="https://www.youtube.com/embed/izSV58EBVf0?enablejsapi=1&version=3&playerapiid=ytplayer"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>
                                <div class="vid_slider_items">
                                    <iframe class="" width="100%" height="312"
                                        src="https://www.youtube.com/embed/jrHezsAzVj0?enablejsapi=1&version=3&playerapiid=ytplayer"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>
                            </div>
                            <script>
                                //polyfill
                                //ie11대응을 위한 polyfill입니다.

                                //1. foreach
                                if ('NodeList' in window && !NodeList.prototype.forEach) {
                                    NodeList.prototype.forEach = function (callback, thisArg) {
                                        thisArg = thisArg || window;
                                        for (var i = 0; i < this.length; i++) {
                                            callback.call(thisArg, this[i], i, this);
                                        }
                                    };
                                }
                                var slider,
                                    oBxSettings = {
                                        minSlides: 2,
                                        maxSlides: 2,
                                        controls: true,
                                        slideWidth: 1200,
                                        slideMargin: 20,
                                        onSlideBefore: function ($slideElement, oldIndex, newIndex) {
                                        	document.querySelectorAll("iframe").forEach(function (item, idx) {
                                                item.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                                            });
                                       	}
                                    };

                                function init() {
                                    // Set maxSlides depending on window width
                                    oBxSettings.maxSlides = window.outerWidth < 1200 ? 1 : 2;
                                    oBxSettings.minSlides = window.outerWidth < 1200 ? 1 : 2;
                                }

                                $(document).ready(function () {
                                    init();
                                    // Initial bxSlider setup
                                    slider = $('.new_vid_slider').bxSlider(oBxSettings);
                                });

                                $(window).resize(function () {
                                    // Update bxSlider when window crosses 430px breakpoint
                                    if ((window.outerWidth < 1200 && window.prevWidth >= 1200) ||
                                        (window.outerWidth >= 1200 && window.prevWidth < 1200)) {
                                        init();
                                        slider.reloadSlider(oBxSettings);
                                    }
                                    window.prevWidth = window.outerWidth;
                                });
                            </script>

                            <p class="intro_use">
                                이용하기
                                <!-- <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2" style="enable-background:new 0 0 163.6 119.2;" xml:space="preserve"> -->
                                <svg class="intro_pigtail_1" version="1.1" id="레이어_1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.6 119.2"
                                    width="163.6" height="119.2" xml:space="preserve">
                                    <g>
                                        <path class="st0" d="M152.7,101.1c0,0-11.5-39.4-54.3-54.3S42.6,63.2,41.9,76.6c-0.4,7.4,0,32,31.3,32S102.6,72,101.1,62
                c-1.5-10-10.5-30.4-40.6-43.5c-18.3-8-41.7-5.2-51.3-3.3" id="pointer"></path>
                                        <polyline class="st1" points="157,66.5 154,103.7 120.8,87.4 	">
                                        </polyline>
                                    </g>
                                </svg>
                            </p>
                            <ul class="intro_info_area">
                                <li>
                                    <h1>(주)온리원<br />대표자 : 최호식</h1>
                                </li>
                                <li>
                                    <h1>사업자등록번호 : 220-88-21645<br />특허번호 : 10-1089183</h1>
                                </li>
                                <li>
                                    <h1>사업자정보 : 소프트웨어 개발<br />소재지 : 경기도 하남시 미사대로 520<br />현대지식산업센터 한강미사2차 D동 5층 545호<!--<br />전화번호 : 1666-8216--></h1>
                                </li>
                            </ul>
                        </div>
                        <span class="intro_copy">
                            Copyright ⓒ Onlyone Corporation. Allrights reserved.
                        </span>
                    </div>
                    <div class="swiper-slide sw_bg_8">
                        <div class="intro_header">
                            <p class="intro_header_logo">
                                <a href="/index.html"><img src="/assets/images/intro/intro_logo_c.png" alt="logo" /></a>
                            </p>
                            <div class="intro_header_login">
                                <ul class="intro_header_login_wrap intro_header_login_wrap_last_page">
                                    <li><a style="color:#333333" href="/login">Login</a></li>
                                    <li><a style="color:#333333" href="/pub/member/step1">Join</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="slider_text_area slider_text_area_margin">
                            <ul class="intro_info_area">
                                <li>
                                    <h1>(주)온리원<br />대표자 : 최호식</h1>
                                </li>
                                <li>
                                    <h1>사업자등록번호 : 220-88-21645<br />특허번호 : 10-1089183</h1>
                                </li>
                                <li>
                                    <h1>사업자정보 : 소프트웨어 개발<br />소재지 : 경기도 하남시 미사대로 520<br />현대지식산업센터 한강미사2차 D동 5층 545호<!--<br />전화번호 : 1666-8216--></h1>
                                </li>
                            </ul>
                        </div>
                        <span class="intro_copy">
                            Copyright ⓒ Onlyone Corporation. Allrights reserved.
                        </span>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination swiper-pagination_intro"></div>
            </div>
        </div>
    </div>
    <c:set var="common_server_time">
        <fmt:formatDate value="${server_toDay}" pattern="yyyy-MM-dd HH:mm:ss" />
    </c:set>
    <input type="hidden" id="common_server_time" value="${common_server_time}">
    <!--swiper_script-->
    <script src="/assets/js/swiper-master/dist/js/swiper.min.js"></script>
    <script>
        $(document).ready(function () {
            if (screen.width > 769) {
                $(".sw_bg_8").hide();
            }

            var onoff = true;
            $(".pre_use_popup_close").click(function () {
                $(".pre_use_popup").hide();
                $('.a_frame.popup_').attr('style', 'z-index:0');
                $(".sw_bg_4").removeClass("swiper-no-swiping");
            });

            $(".intro_pre").click(function () {
                // 알림톡 체험하기
                $("#tgt_cust_no").val("");
                $("#tgt_cust_no_msg").hide();
                $(".pre_use_popup").show();
                $('.a_frame.popup_').attr('style', 'z-index:100');
                $(".sw_bg_4").addClass("swiper-no-swiping");
            });

            var swiper = new Swiper('.main_slider', {
                on: {
                    slideChangeTransitionEnd: function () {
                        console.log("swiper.activeIndex##" + swiper.activeIndex);
                        if (swiper.activeIndex == 1) {
                            //svg animation//
                            $(".st0").eq(1).attr("class", "st0 st0_animate");
                            $(".st1").eq(1).attr("class", "st1 st1_animate");
                            $(".st3").attr("class", "st3 st0_animate");
                            $(".st4").attr("class", "st4 st1_animate");
                        }
                        if (swiper.activeIndex == 2) {
                            //svg animation//
                            $(".st0").eq(2).attr("class", "st0 st0_animate");
                            $(".st1").eq(2).attr("class", "st1 st1_animate");
                        }
                        if (swiper.activeIndex == 3) {
                            //svg animation//
                            $(".st0").eq(3).attr("class", "st0 st0_animate");
                            $(".st1").eq(3).attr("class", "st1 st1_animate");
                        }
                        if (swiper.activeIndex == 4) {
                            //svg animation//
                            $(".st0").eq(4).attr("class", "st0 st0_animate");
                            $(".st1").eq(4).attr("class", "st1 st1_animate");
                        }
                        if (swiper.activeIndex == 5) {
                            //svg animation//
                            $(".st0").eq(5).attr("class", "st0 st0_animate");
                            $(".st1").eq(5).attr("class", "st1 st1_animate");
                        }
                        if (swiper.activeIndex == 6) {
                            //svg animation//
                            $(".st0").eq(6).attr("class", "st0 st0_animate");
                            $(".st1").eq(6).attr("class", "st1 st1_animate");
                        }

                        if (swiper.activeIndex == 6) {
                            $('.swiper-pagination_intro').addClass('last');
                        } else {
                            $('.swiper-pagination_intro').removeClass('last');
                        }

                    },
                },
                direction: 'vertical',
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                },
                paginationClickable: true,
                parallax: true,
                mousewheel: onoff,
                noSwipingClass: 'swiper-no-swiping',
                speed: 400,
                slidesPerView: 1,
                noSwiping: true
            });
        });
        var swiper_vid = new Swiper('.video_slider_wrapper', {
            slidesPerView: 2,
            navigation: {
                nextEl: '.button_edit1',
                prevEl: '.button_edit2',
            },
            spaceBetween: "30",
        });
                                // swiper_vid.update();
    </script>
    <script>
        function fnGetHours() {
            var time = $("#common_server_time").val();
            if (typeof time == "undefined" || time == "") {
                time = +new Date();
                return new Date(time).getHours();
            } else {
                return Number(moment(time, "YYYY-MM-DD HH:mm:ss").format("HH"));
            }
        }

        //시간대별 백그라운드 변경
        $(document).ready(function () {
            $(".intro_use").click(function () {
                location.href = '/login';
            });
            if ('${!empty bannerB4FFData }' == 'true'){
                var stor_path = '${bannerB4FFData.stor_path}';
                var stor_file_nm = '${bannerB4FFData.stor_file_nm}';
            }
            var x = fnGetHours();
            if (x >= 6 && x < 12) { //오전
                $(".sw_bg_1").addClass("sw_bg_1_time_3"); // 빵
                $(".sw_bg_1").removeClass("sw_bg_1_time_1");
                $(".sw_bg_1").removeClass("sw_bg_1_time_2");
            } else if (x >= 12 && x < 18) { //낮
                $(".sw_bg_1").addClass("sw_bg_1_time_2"); // 공장
                $(".sw_bg_1").removeClass("sw_bg_1_time_1");
                $(".sw_bg_1").removeClass("sw_bg_1_time_3");
            } else { //저녁과 새벽
                $(".sw_bg_1").addClass("sw_bg_1_time_1"); // 전동
                $(".sw_bg_1").removeClass("sw_bg_1_time_2");
                $(".sw_bg_1").removeClass("sw_bg_1_time_3");
            }

            if (stor_path != '' && stor_file_nm != '') {
                $(".sw_bg_1").attr('style', 'background: url(/imagefile/' + stor_path + stor_file_nm + ') center center / cover no-repeat !important; ');
            } else {
                $(".sw_bg_1").attr('style', 'background-color: #120E4A !important;');
            }

            $('.a_frame').css("height", window.innerHeight);

            $(".submit_ph").click(function () {
                var tgt_cust_no = $("#tgt_cust_no").val();
                $("#tgt_cust_no_msg").hide();

                if (!isMobile(tgt_cust_no)) {
                    showMsg("휴대폰 번호를 정확히 입력해주세요.");
                    return false;
                }

                $.ajax({
                    url: '<c:url value="/pub/trial"/>',
                    data: {
                        'tgt_cust_no': tgt_cust_no
                    },
                    type: 'POST',
                    success: function (data) {
                        if ("OK" == data) {
                            showMsg("알림톡 체험 신청 되었습니다.", "color-green");
                            return false;
                        } else {
                            showMsg("알림톡 체험을 이미 이용한 번호입니다.");
                            return false;
                        }
                    },
                    error: function (error) {
                        showMsg("알림톡 체험 신청에 실패하였습니다.");
                        return false;
                    }
                });
            });

            function showMsg(txt, color) {
                var msg = $("#tgt_cust_no_msg");
                msg.text(txt);

                if (nonNull(color)) {
                    if (color == "color-green") {
                        msg.addClass(color);
                    } else {
                        msg.removeClass(color);
                    }
                }
                msg.show();
                $("#tgt_cust_no").focus();
            }
        });
    </script>
    <script>
        $(window).load(function () {
            //svg animation//
            $(".st0").eq(0).attr("class", "st0 st0_animate");
            $(".st1").eq(0).attr("class", "st1 st1_animate");
        });
    </script>
    <style>
        .button_edit2 {
            right: -3rem;
            top: 66%;
            background-size: 90%;
            height: 52px;
            background-position: center;
        }

        .button_edit1 {
            left: -3rem;
            top: 66%;
            background-size: 90%;
            height: 52px;
            background-position: center;
        }

        .swiper_1_button_wrapper {
            position: absolute;
            max-width: 1200px;
            left: 50%;
            margin-left: -600px;
            margin-top: -154px;
            width: 1300px;
        }

        .a_frame {
            position: fixed;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
        }

        p#tgt_cust_no_msg {
            float: left;
            margin: 10px 30px;
            font-size: 0.8rem;
            display: none;
            color: red;
        }

        p.color-green {
            color: green !important;
        }

        .slider_text_area_margin .swiper-wrapper {
            height: 100%;
        }

        @media(max-width:720px) {
            .bx-wrapper .bx-pager.bx-default-pager a {
                background: #e4e4e4;
                text-indent: -9999px;
                display: block;
                width: 40px;
                height: 15px;
                margin: 0 5px;
                outline: 0;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border-radius: 10px;
            }
        }
    </style>
</body>

</html>