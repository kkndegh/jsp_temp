<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<head>
</head>
<body>
로그인 중...
</body>
<script type="text/javascript">
$(function() {

	var id = getParameterByName("id");
	var isMaster = getParameterByName("master");

	if(!id){
		errorBox("로그인 실패");
	}
	
	var deId = Base64.decode(id);
	
	if(!deId){
		 errorBox("로그인 실패");
	} 


	var master = false;
	
	if(isMaster == 1){
		master = true;
	}
	

	var obj =  {
		'id' : deId,
		'isMaster' : master
	}
	
	auto_login();
	
	function auto_login(){
 		$.ajax({
			type: "post"
			,data : JSON.stringify(obj)
			,url: '/user/auto_login'
			,dataType: 'json'
			,contentType: "application/json; charset=utf-8"
				,beforeSend : function(xhr){
					xhr.setRequestHeader("autoLogin", true); 
			}
			,success: function(data){
				location.href= "/dashboard";
			},
			error: function(data){
				errorBox("로그인 실패");
			}
		});
	}
	
});
</script>