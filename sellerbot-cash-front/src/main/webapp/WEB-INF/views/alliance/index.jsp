<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="container">
	<div class="member_wra">
<!-- 		<div class="left_title_area"> -->
<!-- 			<div class="left_title_item"> -->
<!-- 				<div class="title_txt">셀러봇캐시로<br>들어가볼까요?</div> -->
<!-- 				<p class="sub_title_txt">하나의 아이디로 셀러봇캐시가<br>준비한 특별한 서비스를 만나보세요.</p> -->
<!-- 			</div> -->
<!-- 			<div class="cs_center_info"> -->
<!-- 				<p class="cs-info-none">고객센터 : 1666-8216<br> -->
<!-- 					s-cash@only1fs.com</p> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 		<div class="right_contents_area"> -->
			
<input type="text" id="custId"><br>
<input type="button" id="tokenCreate" value="Token 최초발급"><br>
<input type="button" id="autoLogin" value="자동로그인"><br>
<input type="button" id="reissueAccess" value="AccessToken 재발급"><br>
<input type="button" id="reissueRefresh" value="RefreshToken 재발급"><br>


AccesToken <input type="text" id="curAccessToken" style="width:100%">
RefreshToken <input type="text" id="curRefreshToken" style="width:100%">




<!-- 2.1.1	고객 가입여부 확인	<br>			 -->
<!-- 3.1.1	서비스 약관	<br> -->
<!-- 4.1.1	고객 정보 생성 패키지	<br>	<button type="button" onclick="">회원</button> -->

<!-- 4.1.1	고객 정보 생성 패키지	<br>	<button type="button" onclick="">회원</button> -->
<!-- 2.1.2	자동로그인	<br> -->
<!-- 2.1.3	AccessToken 재발급요청	<br> -->
<!-- 2.1.4	Refresh Token 재발급요청	<br> -->

<!-- 4.2.1	고객 몰 목록	<br> -->
<!-- 4.2.2	고객 몰 생성	<br> -->
<!-- 4.2.3	고객 몰 상세	<br> -->
<!-- 4.2.4	고객 몰 수정	<br> -->
<!-- 4.2.5	고객 몰 삭제	<br> -->

<!-- 5.1	샐러봇 캐시 사용요청 API	<br> -->
<!-- 5.1.1	무료/유료사용 요청 API	<br> -->
<!-- 5.1.2	사용 취소 API	<br> -->


<!-- 		</div> -->
	</div>

</div>

<script>
	$('#tokenCreate').click(function(){
		var param={};
		param.cust_id=$("#custId").val();
		console.log(param);	
		$.ajax({
			type: "post"
			,data :JSON.stringify(param)
			,url: "/alliance/getAccessToken"
			,dataType: 'json'
			,contentType: "application/json; charset=utf-8"
			,beforeSend: function (xhr) {
			 	/* Authorization header */
			    xhr.setRequestHeader("svc_id", "playauto");
				xhr.setRequestHeader("svc_cert_key", "b119de2b-d090-4f48-a01a-2fe0707d44b6");
			}	
			,success: function(res){
				console.log('dddddddddddddddddd');
				$('#curAccessToken').val(res.access_token);
				$('#curRefreshToken').val(res.refresh_token);
			},error : function(res){
				console.log(res);
			}
		
		});//ajax					
    });
	$('#autoLogin').click(function(){
		$.ajax({
			type: "post"
			,url: "/alliance/login/auto"
			,dataType: 'html'
			,beforeSend: function (xhr) {
			 	/* Authorization header */
			    xhr.setRequestHeader("access_token", "Bearer " + $('#curAccessToken').val());
				xhr.setRequestHeader("svc_id", "playauto");
				xhr.setRequestHeader("svc_cert_key", "b119de2b-d090-4f48-a01a-2fe0707d44b6");
			}			
			,success: function(res){
				location.href = "http://localhost:8081/";
				//location.href= res;
			},error : function(res){
				console.log("error");
				console.log(res);
			}
				
		});//ajax					
    });

	$('#reissueAccess').click(function(){
		$.ajax({
			type: "post"
			,url: "/alliance/reissue/accessToken"
			,dataType: 'json'
			,beforeSend: function (xhr) {
			 	/* Authorization header */
			    xhr.setRequestHeader("refresh_token", "Bearer " + $('#curRefreshToken').val());
				xhr.setRequestHeader("svc_id", "playauto");
				xhr.setRequestHeader("svc_cert_key", "b119de2b-d090-4f48-a01a-2fe0707d44b6");
			}			
			,success: function(res){
				console.log('dddddddddddddddddd');
				$('#curAccessToken').val(res.access_token);

			},error : function(res){
				
				console.log(res.responseJSON.code);
				console.log(res.responseJSON.reason);
				console.log(res.responseJSON.comment);
				alert(res.responseJSON.code + ":" + res.responseJSON.reason);
			}
				
		});//ajax					
    });
	$('#reissueRefresh').click(function(){
		$.ajax({
			type: "post"
			,url: "/alliance/reissue/refreshToken"
			,dataType: 'json'
			,beforeSend: function (xhr) {
			 	/* Authorization header */
			    xhr.setRequestHeader("refresh_token", "Bearer " + $('#curRefreshToken').val());
				xhr.setRequestHeader("svc_id", "playauto");
				xhr.setRequestHeader("svc_cert_key", "b119de2b-d090-4f48-a01a-2fe0707d44b6");
			}			
			,success: function(res){
				console.log('dddddddddddddddddd');
				$('#curRefreshToken').val(res.refresh_token);

			},error : function(res){
				
				console.log(res.responseJSON.code);
				console.log(res.responseJSON.reason);
				console.log(res.responseJSON.comment);
				alert(res.responseJSON.code + ":" + res.responseJSON.reason);
			}
				
		});//ajax					
    });	
	

	
	
</script>