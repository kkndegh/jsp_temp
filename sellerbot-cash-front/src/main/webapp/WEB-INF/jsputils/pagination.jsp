<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:if test="${param.startPageNo > 0 }">
    <div class="pagination">
        <span class="arrow prev2" onclick="goPage(${param.firstPageNo}); return false;"><img
                src="/assets/images/notice/next2.png" alt=""></span>
        <span class="arrow prev1" onclick="goPage(${param.prevPageNo}); return false;"><img
                src="/assets/images/notice/next1.png" alt=""></span>
        <c:forEach var="i" begin="${param.startPageNo}" end="${param.endPageNo}" step="1">
            <c:choose>
                <c:when test="${i eq param.pageNo}">
                    <span class="page focus">${i}</span>
                </c:when>
                <c:otherwise>
                    <span class="page" onclick="goPage(${i}); return false;">${i}</span>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <span class="arrow next1" onclick="goPage(${param.nextPageNo}); return false;"><img
                src="/assets/images/notice/next1.png" alt=""></span>
        <span class="arrow next2" onclick="goPage(${param.finalPageNo}); return false;"><img
                src="/assets/images/notice/next2.png" alt=""></span>
    </div>
</c:if>