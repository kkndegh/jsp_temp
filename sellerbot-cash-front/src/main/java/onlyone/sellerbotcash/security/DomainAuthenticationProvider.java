package onlyone.sellerbotcash.security;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.service.SmpAuthService;

/**
 * Authenticate a user from the database.
 */
@Slf4j
@Component
public class DomainAuthenticationProvider implements AuthenticationProvider {

    @Autowired private final SmpAuthService smpService;
    
    public DomainAuthenticationProvider(SmpAuthService smpService) {
    	this.smpService = smpService;
    }
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    	// RequestContextHolder.currentRequestAttributes()
	    String name = authentication.getName();
	    String password = authentication.getCredentials().toString();
	    WebAuthenticationDetails details = (WebAuthenticationDetails) authentication.getDetails();
	    HashMap<String, Object> login = smpService.authenticationsLoginCust(name, password, details.getRemoteAddress());
	    boolean success = (boolean) login.get("result");
	    if( !success ) {
	    	throw new BadCredentialsException((String)login.get("errorCode"));
	    }
	    // use the credentials
	    // and authenticate against thnotee third-party system
	    log.info("called DomainAuthenticationProvider {}, {}", name, password );
	    User user = new User(name, "",  SellerbotConstant.GRANTEDAUTH);
    	return new UsernamePasswordAuthenticationToken(user, "", SellerbotConstant.GRANTEDAUTH);
	}

	@Override
    public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
