package onlyone.sellerbotcash.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 * Authenticate a user from the database.
 */
@Component
public class AccessFailureHandler implements AuthenticationFailureHandler {
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		// ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
   	 
        String accept = request.getHeader("accept");

        String error = "true";
        String message = exception.getMessage();
        
        if( StringUtils.indexOf(accept, "xml") > -1 ) {
             response.setContentType("application/xml");
             response.setCharacterEncoding("utf-8");
             String data = StringUtils.join(new String[] {
                  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                  "<response>",
                  "<error>" , error , "</error>",
                  "<message>" , message , "</message>",
                  "</response>"
             });

             PrintWriter out = response.getWriter();
             out.print(data);
             out.flush();
             out.close();

        } else if( StringUtils.indexOf(accept, "json") > -1 ) {
             response.setContentType("application/json");
             response.setCharacterEncoding("utf-8");

             String data = StringUtils.join(new String[] {
                  " { \"response\" : {",
                  " \"error\" : " , error , ", ",
                  " \"message\" : \"", message , "\" ",
                  "} } "
             });

             PrintWriter out = response.getWriter();
             out.print(data);
             out.flush();
             out.close();
        }
	}
	}		

//	@Override
//	public void handle(HttpServletRequest request, HttpServletResponse response,
//			AccessDeniedException accessDeniedException) throws IOException, ServletException {
//		
//		String accept = request.getHeader("accept"); 
//		String error = "true"; 
//		String message = accessDeniedException.getMessage(); 
//		response.setStatus(HttpServletResponse.SC_FORBIDDEN); 
//		response.setCharacterEncoding("UTF-8"); 
//		String data = StringUtils.join(new String[] {
//                " { \"response\" : {",
//                " \"error\" : " , error , ", ",
//                " \"message\" : \"", message , "\" ",
//                "} } "
//           });
//
//           PrintWriter out = response.getWriter();
//           out.print(data);
//           out.flush();
//           out.close();
//	}
