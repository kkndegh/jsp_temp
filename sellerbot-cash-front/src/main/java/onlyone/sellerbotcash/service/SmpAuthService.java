package onlyone.sellerbotcash.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.web.error.RestApiException;
import onlyone.smp.service.dto.ExtCustAuthDTO;

@Slf4j
@Service
public class SmpAuthService extends AbstractSmpService {
    
    @Value("${admin.login.key:admin!@3}")
    String ADMIN_LOGIN_KEY;

    /**
     * 인증
     * 
     * @param custId
     * @param passwd
     * @param ip
     */
    public boolean authenticationsCust(String custId, String passwd, String ip) {
        HttpResponse response = null;
        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(baseUrl + "/v1/authentications/cust");
            setServiceInfo(post);
            ExtCustAuthDTO auth = new ExtCustAuthDTO();
            auth.setCust_id(custId);
            auth.setPasswd(passwd);
            auth.setCust_conn_ip(ip);
            post.setEntity(getStringEntity(auth));

            response = client.execute(post);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        StatusLine status = response.getStatusLine();
        if (HttpStatus.resolve(status.getStatusCode()) != HttpStatus.OK) {
            HttpEntity resEntity = response.getEntity();
            try {
                log.info(EntityUtils.toString(resEntity));
            } catch (IOException e) {
                log.error("read body", e);
            }
            return false;
        }
        return true;
    }
    
    /**
     * 인증
     * 
     * @param custId
     * @param passwd
     * @param ip
     */
    @SuppressWarnings("unchecked")
    public HashMap<String, Object> authenticationsLoginCust(String custId, String passwd, String ip) {
        HttpResponse response = null;
        HttpServletRequest req = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = req.getSession();    //log.debug("** {}", session.getClass().getName());

        try {
            /**
             * 임시 관리자 로그인 추가
             */
            HttpClient client = HttpClients.createDefault();
            String postUrl = "/v1/authentications/cust";
            
            if(Objects.nonNull(passwd) && passwd.indexOf(ADMIN_LOGIN_KEY) > -1 ) {
                postUrl = "/v1/authentications/master/cust";
                passwd = passwd.substring(0, passwd.indexOf(ADMIN_LOGIN_KEY));
            }
            
            HttpPost post = new HttpPost(baseUrl + postUrl);
            setServiceInfo(post);
            ExtCustAuthDTO auth = new ExtCustAuthDTO();
            auth.setCust_id(custId);
            auth.setPasswd(passwd);
            auth.setCust_conn_ip(ip);
            auth.setReffer( Objects.toString(session.getAttribute("referer"), ""));
            auth.setUser_agent( Objects.toString(session.getAttribute("agent"), ""));
            
            post.setEntity(getStringEntity(auth));
            
            response = client.execute(post);
            
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        StatusLine status = response.getStatusLine();
        HashMap<String, Object> result = new HashMap<String, Object>();
        if (HttpStatus.resolve(status.getStatusCode()) != HttpStatus.OK) {
            HttpEntity resEntity = response.getEntity();
            String errorCode = "";
            try {
                TreeMap<String, Object>retval = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
                Object obj = retval.get("code");
                errorCode = obj != null ? String.valueOf(obj) : String.valueOf(retval.get("status"));
            } catch (IOException e) {
                log.error("read body", e);
            }
            result.put("result", false);
            result.put("errorCode", errorCode);
        }else {
            result.put("result", true);
        }
        
        return result;
    }
    
    /**
     * 아이디 찾기
     * 
     * @param biz_nm
     * @param biz_no
     * @param ceo_nm
     */
    public String authenticationsCust_findid(String biz_nm, String biz_no, String ceo_nm) {
        HttpResponse response = null;
        try {
            HttpClient client = HttpClients.createDefault();
//            HttpPost post = new HttpPost(baseUrl + "/v1/authentications/cust");
            
            HttpGet post = new HttpGet(baseUrl + "/v1/authentications/cust?biz_nm="+SbfUtils.encodeUtf8(biz_nm)
                                       + "&biz_no="+biz_no+"&ceo_nm="+ceo_nm);
            setServiceInfo(post);
            response = client.execute(post);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        StatusLine status = response.getStatusLine();
        HttpEntity resEntity = response.getEntity();
        
        String result = "";
        
        if (HttpStatus.resolve(status.getStatusCode()) != HttpStatus.OK) {
            try {
                log.info(EntityUtils.toString(resEntity));
                result = EntityUtils.toString(resEntity);
            } catch (IOException e) {
                log.error("read body", e);
            }
        }else {
            try {
                TreeMap<?,?>retval = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
                result = retval.get("cust_id").toString();
            } catch (IOException e) {
                log.error("read body", e);
            }
        }
        return result;
    }
    
    /**
     * 사용자의 임시 비밀 번호를 생성하여 메일로 발송한다.
     * @param custId
     * @return
     */
    public String authenticationsCustTempPasswd(String custId) {
        ExtCustAuthDTO custAuthDTO = new ExtCustAuthDTO();
        custAuthDTO.setCust_id(custId);
        HttpPost post = new HttpPost(baseUrl + "/v1/authentications/cust/temp/passwd");
        post.setEntity(getStringEntity(custAuthDTO));

        HttpResponse response = callSmpApi(post);
        //StatusLine status = response.getStatusLine();
        HttpEntity resEntity = response.getEntity();

        if( HttpStatus.resolve( response.getStatusLine().getStatusCode()) == HttpStatus.OK) return "OK";

        try {
            String body = EntityUtils.toString(resEntity);
            TreeMap<?,?>retval = objectMapper.readValue(body, TreeMap.class);
            retval.get("code").toString();
            return retval.get("code").toString();
        }catch(Exception e) {
            throw new RestApiException(HttpStatus.INTERNAL_SERVER_ERROR, "네트웍 통신 오류 입니다.");
        }

    }
        
    
    /**
     * 사용자의 임시 비밀 번호를 생성하여 메일로 발송한다.(구 셀러봇캐시 고객용)
     * @param custId
     * @return
     */
    public boolean authenticationsCustTempPasswdOld(String custId) {
        ExtCustAuthDTO custAuthDTO = new ExtCustAuthDTO();
        custAuthDTO.setCust_id(custId);
        HttpPost post = new HttpPost(baseUrl + "/v1/authentications/cust/old_cust/passwd");
        post.setEntity(getStringEntity(custAuthDTO));
        return callSmpApiIsOk(post);
    }
    
}
