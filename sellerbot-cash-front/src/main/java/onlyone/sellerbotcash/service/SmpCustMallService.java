package onlyone.sellerbotcash.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtCommMallDTO;
import onlyone.smp.service.dto.ExtCustMallDTO;
import onlyone.smp.service.dto.ExtCustMallListDTO;
import onlyone.smp.service.dto.ExtDealCustMallDTO;
import onlyone.smp.service.dto.ExtJoinCustMallListDTO;
import onlyone.smp.service.dto.ExtReCustMallDTO;
import onlyone.smp.service.dto.ExtReCustMallReqDTO;
import onlyone.smp.service.dto.ExtReCustMallResDTO;
import onlyone.smp.service.dto.ExtReMallDTO;

@Slf4j
@Service
public class SmpCustMallService extends AbstractSmpService {

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    private SmpJoinService smpJoinService;

    /**
     * 고객 몰 목록 조회
     * 
     * @param username
     * @return
     */
    public ExtCustMallListDTO getCustMall(String username) {
        HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/mall");
        ExtCustMallListDTO custMall = callSmpApi(get, ExtCustMallListDTO.class);
        return custMall;
    }

    /**
     * 고객 몰 목록 조회
     * 
     * @param username
     * @return
     */
    public List<ExtCustMallDTO> getCustErrorMall(String username) {
        HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/mall");
        ExtCustMallListDTO custMall = callSmpApi(get, ExtCustMallListDTO.class);
        List<ExtCustMallDTO> mallList = new ArrayList<ExtCustMallDTO>();

        for (int i = 0; i < custMall.getMall().size(); i++) {
            if ("ERR".equals(custMall.getMall().get(i).getCust_mall_sts_cd().toString())) {
                mallList.add(custMall.getMall().get(i));
            }
        }
        return mallList;
    }

    /**
     * 고객 몰 목록 조회
     * 
     * @param username
     * @return
     */
    public List<ExtCommMallDTO> getCustMallList(String username) {
        HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/mallList");

        List<ExtCommMallDTO> custMall = Arrays.asList(callSmpApi(get, ExtCommMallDTO[].class));
        return custMall;
    }

    public boolean deleteMall(String cust_id, String cust_mall_seq_no) {
        HttpDelete delete = new HttpDelete(baseUrl + "/v1/cust/" + cust_id + "/mall/" + cust_mall_seq_no);
        return callSmpApiIsOk(delete);
    }

    public ExtCustMallDTO getMallInfo(String username, String cust_mall_seq_no) {
        HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/mall/" + cust_mall_seq_no);
        ExtCustMallDTO custMall = callSmpApi(get, ExtCustMallDTO.class);
        return custMall;
    }

    /**
     * 삭제된 몰을 필터한 몰 정보
     * 
     * @param username
     * @return
     */
    public ExtCustMallListDTO getNonDelCustMall(String username) {
        ExtCustMallListDTO custMall = getCustMall(username);
        custMall.setMall(custMall.getMall().stream().filter(mall -> mall.getDel_yn().equals(EYesOrNo.N))
                .collect(Collectors.toList()));
        return custMall;
    }

    /**
     * 삭제된 몰을 제외한 데이터 조회
     * 
     * @param username
     * @return
     */
    public List<ExtCustMallDTO> getNonDelCustErrorMallList(String username) {
        List<ExtCustMallDTO> mallList = getCustErrorMall(username);
        return mallList.stream().filter(mall -> mall.getDel_yn().equals(EYesOrNo.N)).collect(Collectors.toList());
    }

    /**
     * 삭제되지 않은 몰을 필터한 몰 *정보 계정 구분 않함.
     * 
     * @param username
     * @return
     */
    public List<ExtCommMallDTO> getfilterCustMallList(String username) {

        List<ExtCommMallDTO> mallList = new ArrayList<ExtCommMallDTO>();
        ExtCustMallListDTO custMall = getCustMall(username);
        boolean isAddMall = true;
        for (ExtCustMallDTO mall : custMall.getMall()) {
            isAddMall = true;
            if (mall.getDel_yn() == EYesOrNo.N) {

                for (ExtCommMallDTO tempMall : mallList) {
                    if (tempMall.getMall_cd().equals(mall.getMall_cd())) {
                        isAddMall = false;
                    }
                }

                if (isAddMall) {
                    ExtCommMallDTO comMall = new ExtCommMallDTO();
                    comMall.setMall_cd(mall.getMall_cd());
                    comMall.setMall_cd_nm(mall.getMall_cd_nm());
                    mallList.add(comMall);
                }
            }
        }

        return mallList;
    }

    public ExtCustMallDTO getCustMallDetailInfo(String custMallSeqNo, String username) {
        HttpGet get = new HttpGet(
                baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/mall/" + custMallSeqNo + "/crypt");
        ExtCustMallDTO custMall = callSmpApi(get, ExtCustMallDTO.class);
        return custMall;
    }

    /**
     * 사용자 인증 이메일 가져오기
     * 
     * @param String
     * @return ExtDealCustMallDTO
     */
    public ExtDealCustMallDTO getInfoForCustAuthMail(String custMallSeqNo) {
        HttpGet get = new HttpGet(baseUrl + "/v1/cust/auth/mail/" + SbfUtils.encodeUtf8(custMallSeqNo));
        ExtDealCustMallDTO result = callSmpApi(get, ExtDealCustMallDTO.class);
        return result;
    }

    public ExtJoinCustMallListDTO getJoinCustMall(String username) {
        HttpGet get = new HttpGet(baseUrl + "/v1/cust/join/" + SbfUtils.encodeUtf8(username) + "/mall");
        ExtJoinCustMallListDTO custMall = callSmpApi(get, ExtJoinCustMallListDTO.class);
        return custMall;
    }

    public List<ExtReCustMallResDTO> saveJoinCustMall(List<ExtReCustMallReqDTO> mallData, String username) {
        HttpPost post = new HttpPost(baseUrl + "/v1/cust/join/" + username + "/mall/save");
        post.setEntity(getStringEntity(mallData));
        ArrayList<Header> headers = new ArrayList<>();
        List<ExtReCustMallResDTO> list = Arrays.asList(callSmpApi(post, ExtReCustMallResDTO[].class, headers));

        // 소드원 회원만 스크랩핑을 실행한다.
        List<ExtCustMallDTO> scrapList = list.stream().filter(o -> {
            if ("ok".equals(o.getResult_code())) {
                return true;
            } else {
                return false;
            }
        }).map(v -> {
            ExtCustMallDTO extCustMallDTO = new ExtCustMallDTO();
            extCustMallDTO.setCust_mall_seq_no(v.getCust_mall_seq_no());
            extCustMallDTO.setCust_mall_sts_cd(v.getCust_mall_sts_cd());
            extCustMallDTO.setCust_mall_sts_cd_nm(v.getCust_mall_sts_cd_nm());
            return extCustMallDTO;
        }).collect(Collectors.toList());

        if (scrapList.size() > 0) {
            try {
                smpJoinService.scrap(scrapList, username);
            } catch (Exception e) {
                log.error("첫 mall 등록 후 스크래핑 처리시 에러가 발생하였습니다.\n" + e);
            }
        }

        return list;
    }

    public ExtReCustMallDTO getJoinMallInfo(String cust_mall_seq_no, String username) {
        HttpGet get = new HttpGet(baseUrl + "/v1/cust/join/" + username + "/mall/" + cust_mall_seq_no);
        ExtReCustMallDTO custMall = callSmpApi(get, ExtReCustMallDTO.class);
        return custMall;
    }

    public List<ExtReMallDTO> getMallList(String mallOpenYn) {
        HttpGet get = new HttpGet(baseUrl + "/v1/cust/join/mall/" + mallOpenYn);
        List<ExtReMallDTO> custMall = Arrays.asList(callSmpApi(get, ExtReMallDTO[].class));
        return custMall;

    }

    public ExtReCustMallResDTO deleteJoinCustMall(String cust_mall_seq_no, String username) {
        HttpPut put = new HttpPut(baseUrl + "/v1/cust/join/" + username + "/mall/" + cust_mall_seq_no);
        ExtReCustMallResDTO result = callSmpApi(put, ExtReCustMallResDTO.class);
        return result;
    }

}
