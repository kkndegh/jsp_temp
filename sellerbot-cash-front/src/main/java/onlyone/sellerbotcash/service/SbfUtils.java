package onlyone.sellerbotcash.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import onlyone.sellerbotcash.config.SellerbotConstant;

public class SbfUtils {

	public static String encodeUtf8(String str) {
		try {
			return URLEncoder.encode(str, SellerbotConstant.UTF8);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String getJsonValue(String source, String key) {
		String retVal = source;
		try {
			TreeMap<?,?> map = new ObjectMapper().readValue(source, TreeMap.class);
			String reason = (String)map.get("reason");
			if (reason != null)	retVal = reason;
		} catch (Exception e) {
		}
		return retVal;
	}
}
