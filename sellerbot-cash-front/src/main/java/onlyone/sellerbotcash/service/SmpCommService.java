package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtTrGoodsCateDTO;


/**
 * 공통 코드 목록
 * @author bak
 *
 */
@Service
public class SmpCommService extends AbstractSmpService{
	
	/**
	 * 코드 목록
	 * @return
	 */
	public List<ExtCommCdDTO> getCodeList(String grpCode) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/comm/cd_grp/"+grpCode);

			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtCommCdDTO> list = Arrays.asList(callSmpApi(get, ExtCommCdDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getCodeList", e);
		}
	}

	/**
	 * 코드 목록
	 * @return
	 */
	public List<ExtCommCdDTO> getCodeList(String grpCode, EYesOrNo useYn) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/comm/cd_grp/"+grpCode);

			if(Objects.nonNull(useYn))
				builder.addParameter("use_yn", useYn.toString());

			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>();
			List<ExtCommCdDTO> list = Arrays.asList(callSmpApi(get, ExtCommCdDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getCodeList", e);
		}
	}
	
	/**
	 * 공통 카테고리 목록
	 * @return
	 */
	public List<ExtTrGoodsCateDTO> getCategoryList() {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/comm/cate");

			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtTrGoodsCateDTO> list = Arrays.asList(callSmpApi(get, ExtTrGoodsCateDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getCategoryList", e);
		}
	}
	
}
