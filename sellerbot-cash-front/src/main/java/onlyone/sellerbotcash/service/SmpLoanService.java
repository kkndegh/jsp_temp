package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.service.dto.ComboLoanDTO;
import onlyone.smp.service.dto.ExtLoanCustMallDTO;
import onlyone.smp.service.dto.ExtLoanDTO;

/**
 * 대출 처리
 * 
 * @author bak
 *
 */
@Slf4j
@Service
public class SmpLoanService extends AbstractSmpService{
    
    /**
     * TODO
     * 대출 상품 제휴사 판매몰 정보
     * @return
     */
    public ExtLoanDTO getLoanBatchProTgtInfo(String username) {
        URIBuilder builder;
        ExtLoanDTO result = null;
        try {
            builder = new URIBuilder(baseUrl + "/v1/loan/" + username + "/batch/pro/tgt/");

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtLoanDTO.class);
            
        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }

    /**
     * 대출 상품 제휴사 판매몰 정보
     * @return
     */
    public ExtLoanDTO getLoanProTgtInfo(String username, String loan_svc_id) {
        URIBuilder builder;
        ExtLoanDTO result = null;
        try {
            builder = new URIBuilder(baseUrl + "/v1/loan/" + username + "/" + loan_svc_id + "/batch/pro/tgt/");

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtLoanDTO.class);
            
        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }

    public boolean createMall(String username, List<ExtLoanCustMallDTO> extLoanCustMallDTO) {
        boolean result = false;
        HttpPost post = new HttpPost(baseUrl + "/v1/loan/"+username+"/req/mall");
        post.setEntity(getStringEntity(extLoanCustMallDTO));
        result = callSmpApiIsOk(post);

        return result;
    }

    /**
     * 금융 상품 사용중인 판매몰 리스트
     * 
     * @param username
     * @return
     */
    public List<ExtLoanCustMallDTO> getLoanCustUseMallList(String username) {
        URIBuilder builder;        
        try {
            builder = new URIBuilder(baseUrl + "/v1/loan/"+username+"/use/mall/list");
            HttpGet get = new HttpGet(builder.build() );
            
            return Arrays.asList(callSmpApi(get, ExtLoanCustMallDTO[].class));
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * 금융상품 서비스 콤보 리스트
     * 
     * @param username
     * @return
     */
    public List<ComboLoanDTO> getComboLoanInfo(String username) {
        URIBuilder builder;
        List<ComboLoanDTO> result = null;
        try {
            builder = new URIBuilder(baseUrl + "/v1/loan/" + username + "/combo/list");

            HttpGet httpGet = new HttpGet(builder.build());
            result = Arrays.asList(callSmpApi(httpGet, ComboLoanDTO[].class));
            
        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }
}
