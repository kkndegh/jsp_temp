package onlyone.sellerbotcash.service;

import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtSettAccScheFiveDTO;
import onlyone.smp.service.dto.ExtSettAccScheFourDTO;
import onlyone.smp.service.dto.ExtSettAccScheMallDayDTO;
import onlyone.smp.service.dto.ExtSettAccScheMallTipDTO;
import onlyone.smp.service.dto.ExtSettAccScheMonDTO;
import onlyone.smp.service.dto.ExtSettAccScheOneDTO;
import onlyone.smp.service.dto.ExtSettAccSchePreDTO;
import onlyone.smp.service.dto.ExtSettAccScheThreeDTO;
import onlyone.smp.service.dto.ExtSettAccScheTipDTO;
import onlyone.smp.service.dto.ExtSettAccScheTwoDTO;
import onlyone.smp.service.dto.ExtTodaySettAccMonthlyMallInfoDTO;
import onlyone.smp.service.dto.ExtTodaySettAccMonthlyMallListDTO;
import onlyone.smp.service.dto.ExtSettAccScheCalendarDTO;
import onlyone.smp.service.dto.ExtSettAccScheDetailDTO;
import onlyone.smp.service.dto.ExtSettAccScheSummaryDTO;

@Service
public class SmpSettAccScheService extends AbstractSmpService{
	
	/**
	 * 정산예정금 한 눈에 보기 페이지
	 * 
	 * @param username
	 * @param date
	 * @return
	 */		
	public ExtSettAccScheSummaryDTO all(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/page/all" );
		try {
			return callSmpApi(get, ExtSettAccScheSummaryDTO.class);
		} 
		catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 정산예정금 상세보기 페이지
	 * 
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccScheDetailDTO detail(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/page/detail" );
		try {
			return callSmpApi(get, ExtSettAccScheDetailDTO.class);
		}
		catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 정산예정금 달력 페이지
	 * 
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws 
	 */
	public ExtSettAccScheCalendarDTO calandar(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/page/calendar" );
		try {
			return callSmpApi(get, ExtSettAccScheCalendarDTO.class);
		} 
		catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 4.4.1 고객 정산 예정 정보 조회
	 * @param username id
	 * @param date - yyyy-MM-dd
	 * @return
	 */
	public ExtSettAccScheOneDTO getCustSettAccSche(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/sts/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) );			
		try {
			return callSmpApi(get, ExtSettAccScheOneDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 4.4.7	미리 정산받을 수 있는 금액(대출기이용자)
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccSchePreDTO getCustSettAccSchePre(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/pre");			
		try {
			return callSmpApi(get, ExtSettAccSchePreDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 4.4.7-1	미리 정산받을 수 있는 금액(대출미이용자)
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccSchePreDTO getCustSettAccSchePreCash(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/pre_cash");			
		try {
			return callSmpApi(get, ExtSettAccSchePreDTO.class);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 4.4.2	배송상태별 정산예정금
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccScheTwoDTO getCustSettAccScheDlv(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/dlv");
		try {
			return callSmpApi(get, ExtSettAccScheTwoDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	public Object getCustSettAccScheDetail(String custMallSeqNo, String date) {
		
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(date) +"/" + SbfUtils.encodeUtf8(custMallSeqNo) + "/detail");			
		try {
			return callSmpApi(get, Object.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	public Object getCustSettAccScheDetailCul(String mallCd) {
		
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/mall/" + SbfUtils.encodeUtf8(mallCd) + "/culmn");			
		try {
			return callSmpApi(get, Object.class);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 4.4.3 판매몰 배송상태별 정산예정금 - ExtSettAccScheThreeDTO
	 * http://192.168.111.250:8200/v1/sett_acc_sche/cust/10042579@naver.com/2019-06-19/dlv/mall
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccScheThreeDTO getCustSettAccScheDlvMall(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/dlv/mall");		
		try {
			return callSmpApi(get, ExtSettAccScheThreeDTO.class);
		}catch(Exception e) {
			return null;
		}
		
	}
	
	/**
	 * 4.4.3 판매몰 배송상태별 정산예정금 - ExtSettAccScheThreeDTO
	 * http://192.168.111.250:8200/v1/sett_acc_sche/cust/10042579@naver.com/2019-06-19/dlv/mall
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccScheThreeDTO getCustSettAccScheDlvMallNR(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/dlv/mall?type=NR");		
		try {
			return callSmpApi(get, ExtSettAccScheThreeDTO.class);
		}catch(Exception e) {
			return null;
		}
		
	}
	
	/** 
	 * 4.4.4    월별 정산에정금 
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccScheFourDTO getCustSettAccScheMon(String username, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/mon");			
		try {
			return callSmpApi(get, ExtSettAccScheFourDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 4.4.5 판매몰 일자별 정산 예정금 - ExtSettAccScheFiveDTO
	 * http://192.168.111.250:8200/v1/sett_acc_sche/cust/zoophono@naver.com/2019-05-24/day/mall
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccScheFiveDTO getCustSettAccScheDayMall(String username, String date) {
		
		String url = baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/day/mall";
		HttpGet get = new HttpGet(url);	
		try {
			return callSmpApi(get, ExtSettAccScheFiveDTO.class);
			
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 4.4.5 판매몰 일자별 정산 예정금 - ExtSettAccScheFiveDTO
	 * http://192.168.111.250:8200/v1/sett_acc_sche/cust/zoophono@naver.com/2019-05-24/day/mall
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccScheFiveDTO getCustSettAccScheDayMallNR(String username, String date) {
		
		String url = baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/day/mall?type=NR";
		HttpGet get = new HttpGet(url);	
		try {
			return callSmpApi(get, ExtSettAccScheFiveDTO.class);
			
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 판매몰 일자별 정산 예정금  달력 데이터 출력 
	 * @param params ( ExtSettAccScheFiveDTO ) 
	 * @return ( HashMap ) 
	 */
	public HashMap<String, Object> getCustSettAccScheDayMallCallendar(ExtSettAccScheFiveDTO params, ExtTodaySettAccMonthlyMallInfoDTO todaySettAccMonthlyMallInfo, String today) {
		
		HashMap<String, Object> result = new HashMap<String, Object>(); 
		NumberFormat nf = NumberFormat.getInstance();

		// 오늘 정산금 데이터 추가
		if(todaySettAccMonthlyMallInfo != null && todaySettAccMonthlyMallInfo.getDaily_mall_list() != null) {
			for( ExtTodaySettAccMonthlyMallListDTO dto : todaySettAccMonthlyMallInfo.getDaily_mall_list()) {

				if(dto.getToday_sett_acc_prc().compareTo(BigInteger.valueOf(0)) == 0) {
					continue;
				}

				if( Objects.nonNull(result.get(dto.getToday_sett_acc_dt())) ) {
					@SuppressWarnings("unchecked")
					HashMap<String, Object> accMap = (HashMap<String, Object>) result.get(dto.getToday_sett_acc_dt());
					Integer count = Integer.parseInt( accMap.get("count").toString() ) + 1;
					long payment = Long.parseLong( accMap.get("payment").toString() ) +  Long.parseLong(dto.getToday_sett_acc_prc().toString());
					
					accMap.put("desc", accMap.get("desc").toString() + "<p><span class='lb'>" + dto.getMall_cd_nm() + "<b class='id_info'>(" + dto.getMall_cert_1st_id() + ")</b></span> <span class='price'>" +  nf.format(dto.getToday_sett_acc_prc()) + "원</span></p>");
					accMap.put("payment", payment );
					accMap.put("count",count);
					
					result.put(dto.getToday_sett_acc_dt(), accMap);
				}
				else {
					HashMap< String, Object > accMap = new HashMap< String, Object >();
					accMap.put("payment", dto.getToday_sett_acc_prc());
					accMap.put("count", 1);

					// 오늘 정산금 툴팁에 타이틀 적용
					if(today.equals(dto.getToday_sett_acc_dt()))
						accMap.put("desc", "<h1>오늘 받을 정산금</h1><p><span class='lb'>" + dto.getMall_cd_nm() + "<b class='id_info'>(" + dto.getMall_cert_1st_id() + ")</b></span> <span class='price'>" +  nf.format(dto.getToday_sett_acc_prc()) + "원</span></p>");
					else 
						accMap.put("desc", "<p><span class='lb'>" + dto.getMall_cd_nm() + "<b class='id_info'>(" + dto.getMall_cert_1st_id() + ")</b></span> <span class='price'>" +  nf.format(dto.getToday_sett_acc_prc()) + "원</span></p>");
					
					result.put(dto.getToday_sett_acc_dt(), accMap);
				}
			}
		}	

		if( params != null && params.getSett_acc_pln_mall_day_l() != null ) {

			for( ExtSettAccScheMallDayDTO SettAccSechVo : params.getSett_acc_pln_mall_day_l() ) {
				// 기존의 API 4.4.5와 신규 API 4.11.3의 오늘 정산금이 중복됨
				if(today.equals(SettAccSechVo.getDay()))	
					continue;
				
				if(SettAccSechVo.getDay_sett_acc_pln() == 0)
					continue;

				if( Objects.nonNull(result.get(SettAccSechVo.getDay())) ) {
					@SuppressWarnings("unchecked")
					HashMap<String, Object> accMap = (HashMap<String, Object>) result.get( SettAccSechVo.getDay() );
					Integer count = Integer.parseInt( accMap.get("count").toString() ) + 1;
					long payment = Integer.parseInt( accMap.get("payment").toString() ) +  SettAccSechVo.getDay_sett_acc_pln() ;
					accMap.put("desc", accMap.get("desc").toString() + "<p><span class='lb'>" + SettAccSechVo.getMall_cd_nm() + "<b class='id_info'>(" + SettAccSechVo.getMall_cert_1st_id() + ")</b></span> <span class='price'>" +  nf.format(SettAccSechVo.getDay_sett_acc_pln()) + "원</span></p>");
					accMap.put("payment", payment );
					accMap.put("count",count);
					
					result.put(SettAccSechVo.getDay(), accMap);

				} else {
					
					HashMap< String, Object > accMap = new HashMap< String, Object >();
					accMap.put("payment", SettAccSechVo.getDay_sett_acc_pln());
					accMap.put("count", 1);
					accMap.put("desc", "<p><span class='lb'>" + SettAccSechVo.getMall_cd_nm() + "<b class='id_info'>(" + SettAccSechVo.getMall_cert_1st_id() + ")</b></span> <span class='price'>" +  nf.format(SettAccSechVo.getDay_sett_acc_pln()) + "원</span></p>");
					
					result.put(SettAccSechVo.getDay(), accMap);
					
				}

			}
		}
		
		return result;
	}

	// 상세 표시용 년간 데이터로 reverse 
	public HashMap<String, ArrayList<ExtSettAccScheMonDTO>> getCustSettAccScheDayMallMonth(ExtSettAccScheFourDTO paramsMonth) {
		
		HashMap<String, ArrayList<ExtSettAccScheMonDTO>> result = new HashMap<String, ArrayList<ExtSettAccScheMonDTO>>(); 
		
		if( paramsMonth.getMon_sett_acc_pln_l() != null ) {
			
			// 년  // 월  // 단위로 저장해야된다. 
			for(ExtSettAccScheMonDTO monthData : paramsMonth.getMon_sett_acc_pln_l()) {
				if (StringUtils.isNotBlank(monthData.getSett_acc_pln_dt_mon())) {
					String yearKey = monthData.getSett_acc_pln_dt_mon().substring(0, 4);
					
					if( result.get(yearKey) == null ) {
						
						ArrayList<ExtSettAccScheMonDTO> itemList = new ArrayList<ExtSettAccScheMonDTO>();
						monthData.setSett_acc_pln_dt_mon(monthData.getSett_acc_pln_dt_mon().substring(4,6));
						itemList.add(monthData);
						
						result.put(yearKey, itemList);
						
					} else {
						monthData.setSett_acc_pln_dt_mon(monthData.getSett_acc_pln_dt_mon().substring(4,6));
						result.get(yearKey).add(monthData);
					}
				}

			}
		}
	
		
		return result; 
	}

	// 상세 표시용 년간 데이터로 reverse 
	public HashMap<String, ArrayList<ExtSettAccScheMallDayDTO>> getCustSettAccScheDayMallDay(ExtSettAccScheFiveDTO paramsDay) {
		
		
		// 월 단위 리스트
		HashMap<String, ArrayList<ExtSettAccScheMallDayDTO>> result = new HashMap<String, ArrayList<ExtSettAccScheMallDayDTO>>();
		
		if( paramsDay.getSett_acc_pln_mall_day_l() != null ) {
			
			List<ExtSettAccScheMallDayDTO> mallList = paramsDay.getSett_acc_pln_mall_day_l();
			
			mallList = mallList.stream().sorted(Comparator.comparing(ExtSettAccScheMallDayDTO::getDay)).collect(Collectors.toList());
			
			// 년  // 월  // 단위로 저장해야된다. 
			for(ExtSettAccScheMallDayDTO dayData : mallList) {
				if (StringUtils.isNotBlank(dayData.getDay())) {
					// 년 월
					String monthKey = dayData.getDay().substring(0, 6);
					// 일자
					dayData.setDay(dayData.getDay().substring(6,8));
					
					if( result.get(monthKey) == null ) {
						ArrayList<ExtSettAccScheMallDayDTO> itemList = new ArrayList<ExtSettAccScheMallDayDTO>();
						itemList.add(dayData);
						result.put(monthKey, itemList);
					} else {
						
						result.get(monthKey).add(dayData);
					}
				}
			}
			
		}
		
		return result; 
	}
	
	// 4.4.6 정산예정금 판매몰별 상세 현황  ExtSettAccSchePreDTO 
	// http://192.168.111.250:8200/v1/sett_acc_sche/cust/zoophono@naver.com/2019-05-24/002

	/**
	 * 4.4.8 정산예정금 팁 - ExtSettAccScheTipDTO
	 * http://192.168.111.250:8200/v1/sett_acc_sche/cust/zoophono@naver.com/2019-05-24/tip
	 * @param username
	 * @param date
	 * @return
	 */
	public ExtSettAccScheTipDTO getCustSettAccScheTip(String username, String date) {
		
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/" + SbfUtils.encodeUtf8(username)+"/" + SbfUtils.encodeUtf8(date) + "/tip");			
		try {
			return callSmpApi(get, ExtSettAccScheTipDTO.class);
		} catch (Exception e) {
			return null;
		}
	}

	// public int getCustSettAccScheTipCount(ExtSettAccScheTipDTO extSettAccScheTipDTO) {
	// 	int count = 0;

	// 	for(ExtSettAccScheMallTipDTO dto : extSettAccScheTipDTO.getMall_tip()) {
	// 		// 기존 calendar.jsp의 조건 반영
	// 		int day = Integer.parseInt(dto.getD_day());
	// 		if(day < 90)
	// 			continue;
	// 		count += 1;
	// 	}

	// 	return count;
	// }

	public Map<String, List<ExtSettAccScheMallTipDTO>> getCustSettAccScheTipForMap(ExtSettAccScheTipDTO extSettAccScheTipDTO) {
		if (Objects.isNull(extSettAccScheTipDTO) || Objects.isNull(extSettAccScheTipDTO.getMall_tip()) || extSettAccScheTipDTO.getMall_tip().size() == 0)
			return null;
			
		Map<String, List<ExtSettAccScheMallTipDTO>> result = new LinkedHashMap<String, List<ExtSettAccScheMallTipDTO>>();

		for(ExtSettAccScheMallTipDTO dto : extSettAccScheTipDTO.getMall_tip()) {
			// 기존 calendar.jsp의 조건 반영
			int day = Integer.parseInt(dto.getD_day());
			if(day < 90)
				continue;

			if(result.containsKey(dto.getMall_cd())) {
				List<ExtSettAccScheMallTipDTO> list = result.get(dto.getMall_cd());
				list.add(dto);
				result.put(dto.getMall_cd(), list);
			}
			else {
				List<ExtSettAccScheMallTipDTO> list = new ArrayList<ExtSettAccScheMallTipDTO>();
				list.add(dto);
				result.put(dto.getMall_cd(), list);
			}
		}

		return result;
	}
	
	/**
	 * 일일 정산예정금
	 * @param sett_acc_pln_sum
	 * @param sett_acc_pln_sum_per_day(전일대비 증가 금액)
	 * @return
	 */
	// public Long calSettScheRtoDay(Long sett_acc_pln_sum, Long sett_acc_pln_sum_per_day) {
	// 	Long result = null;
	// 	if( Objects.nonNull(sett_acc_pln_sum) && Objects.nonNull(sett_acc_pln_sum_per_day) && sett_acc_pln_sum != 0L ) {
	// 		sett_acc_pln_sum_per_day = sett_acc_pln_sum - sett_acc_pln_sum_per_day;
	// 		Double temp = (sett_acc_pln_sum.doubleValue() - sett_acc_pln_sum_per_day.doubleValue()) / sett_acc_pln_sum_per_day.doubleValue() * 100; 
	// 		return temp.longValue();
	// 	}
	// 	return result;
	// }
}
