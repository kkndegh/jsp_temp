package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.persistent.domain.enums.EStorFileTyp;
import onlyone.smp.service.dto.ExtBannerDTO;

@Slf4j
@Service
public class SmpBannerService extends AbstractSmpService{
	
	/**
	 * 배너 조회
	 * @param username
	 * @return
	 */
	public List<ExtBannerDTO> getBannerData(EStorFileTyp banner_typ) {
		URIBuilder builder;
        List<ExtBannerDTO> result = null;

		try {
            builder = new URIBuilder(baseUrl + "/v1/banner");
            builder.addParameter("banner_typ", banner_typ.toString());
            HttpGet httpGet = new HttpGet(builder.build());
            result = Arrays.asList(callSmpApi(httpGet, ExtBannerDTO[].class));

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

		return result;
	}

	public boolean bannerViewCount(Long bannerItemSeqNo) {
		HttpResponse res = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/banner/view/" + bannerItemSeqNo);
            HttpPost httpPost = new HttpPost(builder.build());
            res = callSmpApi(httpPost);
        } catch (Exception e) {
            log.error("" + e);
        }
        return HttpStatus.resolve(res.getStatusLine().getStatusCode()) == HttpStatus.OK;
	}
}
