package onlyone.sellerbotcash.service;

import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO.ExtSmtAgreeTermsInfoDTO;


@Service
public class SmpTermsService extends AbstractSmpService{
	public String termsChk(String cust_id, String svc_id, long terms_id) {
		HttpGet get = new HttpGet(baseUrl + "/v1/service/terms/check?svc_id="+svc_id+"&terms_id="+terms_id+"&cust_id="+cust_id );
		try {
			return callSmpApi(get, String.class);
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ExtSmtAgreeTermsInfoDTO> getSmtAgreeItemList(Long cust_seq_no, String svc_id) {
		HttpGet get = new HttpGet(baseUrl + "/v1/service/smt/agree/item/list?svc_id="+svc_id+"&cust_seq_no="+cust_seq_no );

		try {
			return callSmpApi(get, List.class);
		} catch (Exception e) {
			return null;
		}
	}

	public boolean saveSmtAgreeItem(ExtSmtAgreeTermsDTO selectedSmtTerms) {

        HttpPost post = new HttpPost(baseUrl + "/v1/service/smt/agree/item");
        post.setEntity(getStringEntity(selectedSmtTerms));
        
        return callSmpApiIsOk(post);
	}
	
}
