package onlyone.sellerbotcash.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtFinGoodDTO;
import onlyone.smp.service.dto.ExtSalesMallDTO;
import onlyone.smp.service.dto.ExtSettAccScheFiveDTO;
import onlyone.smp.service.dto.ExtSettAccScheMallSvcPrc;
import onlyone.smp.service.dto.ExtSettAccSchePreDTO;
import onlyone.smp.service.dto.ExtSettAccScheThreeDTO;
import onlyone.smp.service.dto.ExtSettAccScheTipDTO;
import onlyone.smp.service.dto.ExtFinGoodPageListDTO;
import onlyone.smp.service.dto.ExtFinGoodPageDetailDTO;

@Service
public class SmpFinGoodService extends AbstractSmpService{
	
	@Autowired
	SmpSettAccScheService smpSettAccScheService;
	@Autowired
	SmpCustService smpCustService;
	
	
	/**
	 * 금융서비스 목록 데이터
	 * 
	 * @param model
	 * @param user
	 * @return
	 */
	public List<ExtFinGoodPageListDTO> getFinGoodPageList(String cust_id, String cust_type, String scra_dt) {
				
		try {
			URIBuilder builder = new URIBuilder(baseUrl + "/v1/fin/goods/page/"+ scra_dt +"/list");
			
			builder.addParameter("cust_id", cust_id );
			builder.addParameter("cust_type", cust_type);
			
			HttpGet get = new HttpGet(builder.build());						
			
			return Arrays.asList(callSmpApi(get, ExtFinGoodPageListDTO[].class));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 금융서비스 상세보기 데이터
	 * 
	 * @param model
	 * @param user
	 * @return
	 */
	public ExtFinGoodPageDetailDTO getFinGoodPageDetail(String finGoodSeqNo, String cust_id, String cust_type, String scra_dt) {
			
		try {
			URIBuilder builder = new URIBuilder(baseUrl + "/v1/fin/goods/page/"+ scra_dt + "/detail/" + finGoodSeqNo);
			
			builder.addParameter("cust_id", cust_id );
			builder.addParameter("cust_type", cust_type);
			
			HttpGet get = new HttpGet(builder.build());	
			
			return callSmpApi(get, ExtFinGoodPageDetailDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<ExtFinGoodDTO> getFinGoodList() {
		HttpGet get = new HttpGet(baseUrl + "/v1/fin/goods?sort=sort_ord" );			
		try {
			return Arrays.asList(callSmpApi(get, ExtFinGoodDTO[].class));
		} catch (Exception e) {
			return null;
		}
	}
	public ExtFinGoodDTO getFinGood(String finGoodSeqNo) {
		HttpGet get = new HttpGet(baseUrl + "/v1/fin/goods/"+finGoodSeqNo );			
		try {
			return callSmpApi(get, ExtFinGoodDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	public ExtSettAccScheThreeDTO getOpm(String id, String scraDt) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/"+id+"/"+scraDt+"/dlv/mall" );			
		try {
			return callSmpApi(get, ExtSettAccScheThreeDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	public ExtSettAccScheFiveDTO getNOpm(String id, String scraDt) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/"+id+"/"+scraDt+"/day/mall" );			
		try {
			return callSmpApi(get, ExtSettAccScheFiveDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	public ExtSettAccScheTipDTO getTip(String id, String scraDt) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sett_acc_sche/cust/"+id+"/"+scraDt+"/tip" );			
		try {
			return callSmpApi(get, ExtSettAccScheTipDTO.class);
		} catch (Exception e) {
			return null;
		}
	}

	public boolean requestCounsel(String biz_nm, String ceph_no, String fin_goods_seq_no) {
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("biz_nm", biz_nm);
		map.put("ceph_no", ceph_no);
		map.put("fin_goods_seq_no", fin_goods_seq_no);
		
		HttpPost post = new HttpPost(svcBaseUrl + "/restapi/svc/counsel.php");
		post.setEntity(getStringEntity(map));
		return callSmpApiIsOk(post);
	}
	
	public Long getBiggestPre(String username, String date) {
		long preSett = 0;
		List<ExtFinGoodDTO> finGoodList = getFinGoodList();
		ExtSettAccSchePreDTO settAccSchePre = null;
		String custTyp = smpCustService.getCustUseLoan(username);
		if(custTyp.equals("Y")) {
			settAccSchePre = smpSettAccScheService.getCustSettAccSchePre(username, date);
			
		}else {
			settAccSchePre = smpSettAccScheService.getCustSettAccSchePreCash(username, date);
		}
		
		if(settAccSchePre == null) return preSett;
		
		for (ExtFinGoodDTO extFinGoodDTO : finGoodList) {
			if(extFinGoodDTO.getTgt_sale_mall_all_yn()==EYesOrNo.Y) {
				if("sodeone".equals(extFinGoodDTO.getSvc_id())) {
					if(settAccSchePre.getTot_sett_acc_svc_prc_sum()<5000000) {
						
					}else {
						long preNow=settAccSchePre.getTot_sett_acc_svc_prc_sum()/1000000*1000000;
						if(preSett < preNow) {
							preSett = preNow;
						}
					}
				}else {
					long preNow=settAccSchePre.getTot_sett_acc_svc_prc_sum()/10000*10000;
					if(preSett < preNow) {
						preSett = preNow;
					}
				}
			}else {
				long sumPre=0;
				for (ExtSalesMallDTO salesMall : extFinGoodDTO.getSales_mall()) {
					for (ExtSettAccScheMallSvcPrc prc : settAccSchePre.getMall_svc_prc()) {
						if(salesMall.getMall_cd().equals(prc.getMall_cd())) {
							if(prc.getMall_b_noti_bef_sett_acc_svc_prc()>prc.getMall_a_noti_bef_sett_acc_svc_prc()) {
								sumPre += prc.getMall_b_noti_bef_sett_acc_svc_prc();
							}else {
								sumPre += prc.getMall_a_noti_bef_sett_acc_svc_prc();
							}
						}
					}
				}
				if("sodeone".equals(extFinGoodDTO.getSvc_id())) {
					if(sumPre<5000000) {
						
					}else {
						long preNow=sumPre/1000000*1000000;
						if(preSett < preNow) {
							preSett = preNow;
						}
					}
				}else {
					long preNow=sumPre/10000*10000;
					if(preSett < preNow) {
						preSett = preNow;
					}
				}
			}
		}
		return preSett;
	}
	public boolean setFinGoodStat(String fin_goods_seq_no, String fin_goods_req_typ_cd, String cust_id) {
		HttpPost post = new HttpPost(baseUrl + "/v1/fin/goods/"+fin_goods_seq_no+"/stat?fin_goods_req_typ_cd="+fin_goods_req_typ_cd+"&cust_id="+cust_id);
		return callSmpApiIsOk(post);
	}

}
