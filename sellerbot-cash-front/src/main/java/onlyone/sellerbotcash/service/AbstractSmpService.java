package onlyone.sellerbotcash.service;

import java.io.IOException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import onlyone.sellerbotcash.web.error.RestApiException;
import lombok.extern.slf4j.Slf4j;

/**
 * SMP에서 제공하는 API 호출용 Service 추상 클래스
 * 
 * @author oletree
 *
 */
@Slf4j
public abstract class AbstractSmpService {

	@Value("${smp.baseUrl:http://221.149.97.49:8200}")
	protected String baseUrl;
	@Value("${svc.baseUrl:http://only1.iptime.org:9025}")
	protected String svcBaseUrl;
	@Value("${svc.baseUrlSodeTwo:http://only1.iptime.org:9025}")
	protected String svcBaseUrlSodetwo;
	@Value("${smap.baseUrl:http://only1.iptime.org:10002}")
	protected String smapBaseUrl;
	@Value("${smp.svcKey:testkey}")
	private String svcKey;
	@Value("${smp.svcId:sellerbotcash}")
	protected String svcId;
	@Value("${svc.sodeBaseUrl:https://api15.sode.co.kr:443}")
	protected String sodeBaseUrl;
	@Autowired
	protected ObjectMapper objectMapper;

	protected void setServiceInfo(HttpMessage base) {
		base.addHeader("svc_id", svcId);
		base.addHeader("svc_cert_key", svcKey);
		base.addHeader("Accept", "application/json");
	}

	protected HttpResponse callSmpApi(HttpRequestBase httpReq) {
		try {
			HttpClient client = HttpClients.createDefault();
			setServiceInfo(httpReq);
			return client.execute(httpReq);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 성공인 200를 받은 경우 true 그외는 false
	 * 
	 * @param httpReq
	 * @return
	 */
	protected boolean callSmpApiIsOk(HttpRequestBase httpReq) {
		HttpResponse response = callSmpApi(httpReq);
		log.debug("------------- callSmpApi ------------------\n url : {}", httpReq.getURI());
		if (HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK) {
			return true;
		}
		HttpEntity resEntity = response.getEntity();
		try {
			log.error("{}", EntityUtils.toString(resEntity));
		} catch (Exception e) {
		}
		return false;
	}

	protected boolean callSmpApiBodyIsOk(HttpRequestBase httpReq) {
		boolean result = false;
		HttpResponse response = callSmpApi(httpReq);
		log.debug("------------- callSmpApi ------------------\n url : {}", httpReq.getURI());
		if (HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK) {
			try {
				HttpEntity resEntity = response.getEntity();
				result = Boolean.parseBoolean(EntityUtils.toString(resEntity));
			} catch (Exception e) {
				result = false;
			}
		}

		log.debug(result ? "Y" : "N");

		return result;
	}

	protected <T> T callSmpApi(HttpRequestBase httpReq, Class<T> clazz) {
		return callSmpApi(httpReq, clazz, null);
	}

	protected <T> T callSmpApi(HttpRequestBase httpReq, Class<T> clazz, List<Header> retHeader) {
		HttpResponse response = callSmpApi(httpReq);
		HttpEntity resEntity = response.getEntity();
		StatusLine status = response.getStatusLine();
		int statusCode = status.getStatusCode();
		String body = null;// "";
		// String comment = "";

		log.debug("------------- callSmpApi ------------------");
		log.info("{}[{}]", httpReq.getURI(), statusCode);
		if (HttpStatus.resolve(statusCode) == HttpStatus.OK) {
			try {
				Header[] headers = response.getHeaders("X-Total-Count");
				if (retHeader != null)
					retHeader.addAll(Arrays.asList(headers));

				if (clazz != null) {
					body = EntityUtils.toString(resEntity);
					log.debug("{}", body);
					return objectMapper.readValue(body, clazz);
				} else {
					return null;
				}
			} catch (Exception e) {
				statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
				body = e.getMessage();
			}
		}

		if (body == null) {
			try {
				body = EntityUtils.toString(resEntity);
			} catch (Exception e) {
			}
		}
		log.debug("body = {}", body);
		log.debug("------------- callSmpApi OK END ------------------");
		throw new RestApiException(HttpStatus.resolve(statusCode), body != null ? body : status.getReasonPhrase());
	}

	/**
	 * Object를 StringEntity로 변경하여 post, put등의 body 에 넣을 수 있게 함.
	 * 
	 * @param obj
	 * @return
	 */
	protected StringEntity getStringEntity(Object obj) {
		StringEntity entity;
		try {
			log.debug("---------------------------------------------");
			log.debug("{}", objectMapper.writeValueAsString(obj));
			log.debug("---------------------------------------------");
			entity = new StringEntity(objectMapper.writeValueAsString(obj), "UTF-8");
			entity.setContentType("application/json");
			return entity;
		} catch (UnsupportedCharsetException | JsonProcessingException e) {
			throw new RuntimeException("stringEntity", e);
		}
	}

	protected void callSmpNoReplyApi(HttpRequestBase httpReq) {
		HttpResponse response = null;
		try {
			HttpClient client = HttpClients.createDefault();
			setServiceInfo(httpReq);
			response = client.execute(httpReq);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		StatusLine status = response.getStatusLine();
		if (HttpStatus.resolve(status.getStatusCode()) != HttpStatus.OK) {
			HttpEntity resEntity = response.getEntity();
			try {
				log.info(EntityUtils.toString(resEntity));
			} catch (IOException e) {
				log.error("read body", e);
			}
			throw new UsernameNotFoundException(httpReq.getURI() + " has Error " + status.getReasonPhrase());
		}
	}

	protected ResponseEntity<String> getResponseEntity(HttpResponse res) {

		if (res.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
			return new ResponseEntity<String>(HttpStatus.OK);
		} else {
			try {
				return new ResponseEntity<String>(EntityUtils.toString(res.getEntity()),
						HttpStatus.valueOf(res.getStatusLine().getStatusCode()));
			} catch (Exception e) {
				log.error("" + e);
			}
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// 외부 svc_id, svc_cert_key를 받아서 API call 할 경우
	protected <T> T callSmpApiExternal(HttpRequestBase httpReq, Class<T> clazz, List<Header> retHeader, String psvcId,
			String psvcCertKey) {
		HttpResponse response = callSmpApiExternal(httpReq, psvcId, psvcCertKey);
		HttpEntity resEntity = response.getEntity();
		StatusLine status = response.getStatusLine();
		int statusCode = status.getStatusCode();
		String body = null;// "";
		// String comment = "";

		log.debug("------------- callSmpApiExternal ------------------");
		log.info("{}[{}]", httpReq.getURI(), statusCode);
		if (HttpStatus.resolve(statusCode) == HttpStatus.OK) {
			try {
				Header[] headers = response.getHeaders("X-Total-Count");
				if (retHeader != null)
					retHeader.addAll(Arrays.asList(headers));

				if (clazz != null) {
					body = EntityUtils.toString(resEntity);
					log.debug("{}", body);
					return objectMapper.readValue(body, clazz);
				} else {
					return null;
				}
			} catch (Exception e) {
				statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
				body = e.getMessage();
			}
		}

		if (body == null) {
			try {
				body = EntityUtils.toString(resEntity);
			} catch (Exception e) {
			}
		}
		log.debug("body = {}", body);
		log.debug("------------- callSmpApi OK END ------------------");
		throw new RestApiException(HttpStatus.resolve(statusCode), body != null ? body : status.getReasonPhrase());
	}

	protected HttpResponse callSmpApiExternal(HttpRequestBase httpReq, String psvcId, String psvcCertKey) {
		try {
			HttpClient client = HttpClients.createDefault();
			// setServiceInfo(httpReq);
			httpReq.addHeader("svc_id", psvcId);
			httpReq.addHeader("svc_cert_key", psvcCertKey);
			httpReq.addHeader("Accept", "application/json");
			return client.execute(httpReq);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected <T> T callSmpApiExternal(HttpRequestBase httpReq, Class<T> clazz, String psvcId, String psvcCertKey) {
		return callSmpApiExternal(httpReq, clazz, null, psvcId, psvcCertKey);
	}
}
