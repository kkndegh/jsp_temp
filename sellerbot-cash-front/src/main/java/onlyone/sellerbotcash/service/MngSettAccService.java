package onlyone.sellerbotcash.service;

import java.util.Objects;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtMngSettAccCalTipDTO;

/**
 * 2.2	정산예정금 통합관리 API용 Service
 * @author YunaJang
 */
@Service
public class MngSettAccService extends AbstractSmpService {
    
    /**
     * 2.2.3.1	정산예정금 관리팁
     * @param cust_id
     * @param scra_dt
     * @param mall_cd
     * @param page
     * @param size
     * @return
     */
    public ExtMngSettAccCalTipDTO getSettAccTipInfo(String cust_id, String scra_dt, String mall_cd, Integer page, Integer size) {
		try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/mngSettAcc/cal/tip");
            builder.setParameter("cust_id", cust_id);
            builder.setParameter("scra_dt", scra_dt);
			builder.setParameter("page", page.toString());
            builder.setParameter("size", size.toString());
            
            if(Objects.nonNull(mall_cd) && !mall_cd.equals(""))
			    builder.setParameter("mall_cd", mall_cd);
			
            HttpGet get = new HttpGet(builder.build());
			return callSmpApi(get, ExtMngSettAccCalTipDTO.class);
		} catch (Exception e) {
			return null;
		}
    }
}