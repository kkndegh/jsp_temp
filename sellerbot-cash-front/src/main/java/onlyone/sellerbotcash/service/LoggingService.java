package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 이력 저장용 Service
 * @author YunaJang
 */
@Slf4j
@Service
public class LoggingService extends AbstractSmpService {
    /**
     * 사이트 이용 이력 저장
     * @param conn_id
     * @param conn_ip
     * @param conn_scr_path
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> addSiteUsePath(String conn_id, String conn_ip, String conn_scr_path) throws JsonProcessingException {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("connId", conn_id);
            param.put("connIp", conn_ip);
            param.put("connScrPath", conn_scr_path);
  
            builder = new URIBuilder(baseUrl + "/v1/logging/site/path");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            res = callSmpApi(httpPost);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException(e.getMessage());
        }

        return getResponseEntity(res);
    }
}