package onlyone.sellerbotcash.service;

import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtTodaySettAccInfoDTO;
import onlyone.smp.service.dto.ExtTodaySettAccMonthlyInfoDTO;
import onlyone.smp.service.dto.ExtTodaySettAccMonthlyMallInfoDTO;

/**
 * 오늘 정산금/판매몰예치금 Service
 * @author YunaJang
 */
@Service
public class SmpTodaySettAccService extends AbstractSmpService {
    /**
	 * 4.11.1	오늘정산금/판매몰예치금 정보
	 * @param username id
	 * @param day [yyyyMMdd]
	 * @return
	 */
    public ExtTodaySettAccInfoDTO getTodaySettAccInfo(String custId, String day) {
        HttpGet httpGet = new HttpGet(baseUrl + "/v1/today_sett_acc/cust/" + SbfUtils.encodeUtf8(custId) + "/day/" + day);
        ExtTodaySettAccInfoDTO result = callSmpApi(httpGet, ExtTodaySettAccInfoDTO.class);
        return result;
    }

    /**
	 * 4.11.2	월별 오늘정산금 합계
	 * @param username id
	 * @param date [yyyyMMdd]
	 * @return
	 */
    public ExtTodaySettAccMonthlyInfoDTO getTodaySettAccMonthlyInfo(String custId, String date) {
        HttpGet httpGet = new HttpGet(baseUrl + "/v1/today_sett_acc/cust/" + SbfUtils.encodeUtf8(custId) + "/date/" + date);
        ExtTodaySettAccMonthlyInfoDTO result = callSmpApi(httpGet, ExtTodaySettAccMonthlyInfoDTO.class);
        return result;
    }

    /**
	 * 4.11.3	월별 오늘정산금 판매몰별
	 * @param username id
	 * @param date [yyyyMMdd]
	 * @return
	 */
    public ExtTodaySettAccMonthlyMallInfoDTO getTodaySettAccMonthlyMallInfo(String custId, String date) {
        HttpGet httpGet = new HttpGet(baseUrl + "/v1/today_sett_acc/cust/" + SbfUtils.encodeUtf8(custId) + "/date/" + date + "/mall");
        ExtTodaySettAccMonthlyMallInfoDTO result = callSmpApi(httpGet, ExtTodaySettAccMonthlyMallInfoDTO.class);
        return result;
    }
}