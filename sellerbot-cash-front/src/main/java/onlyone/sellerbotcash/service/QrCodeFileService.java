package onlyone.sellerbotcash.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.GlobalHistogramBinarizer;
import com.google.zxing.multi.GenericMultipleBarcodeReader;
import com.google.zxing.multi.MultipleBarcodeReader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class QrCodeFileService extends AbstractSmpService {
	
	private static Map<DecodeHintType,Object> HINTS;
	private static Map<DecodeHintType,Object> HINTS_PURE;

	@Value("${image.originalUploadDir}") private String originalUploadDir ;
	
	public String getQrCodeKeyExtraction(MultipartFile qrcode_image_file) throws IOException, NotFoundException, ChecksumException, FormatException {
		
		String qrCodeKey = "";
		File initialFile = null;

		HINTS = new EnumMap<>(DecodeHintType.class);
	    HINTS.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
	    HINTS.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.allOf(BarcodeFormat.class));
	    HINTS_PURE = new EnumMap<>(HINTS);
	    HINTS_PURE.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

		if (qrcode_image_file != null) {
			initialFile = new File(originalUploadDir + "/" + qrcode_image_file.getOriginalFilename());

			//동일한 파일이 있다면 삭제 한 후 다시 파일 생성
			if (initialFile.exists()) {
				initialFile.delete();
			}

			initialFile.createNewFile(); 
			FileOutputStream fos = new FileOutputStream(initialFile); 
			fos.write(qrcode_image_file.getBytes());
			fos.close();

			InputStream is = new FileInputStream(initialFile);
			BufferedImage image = ImageIO.read(is);
			LuminanceSource source = new BufferedImageLuminanceSource(image);
			BinaryBitmap bitmap = new BinaryBitmap(new GlobalHistogramBinarizer(source));
			Collection<Result> results = new ArrayList<>();
			Reader reader = new MultiFormatReader();
			MultipleBarcodeReader multiReader = new GenericMultipleBarcodeReader(reader);
			
			Result[] theResults = multiReader.decodeMultiple(bitmap, HINTS);
			if (theResults != null) {
				results.addAll(Arrays.asList(theResults));
			}
			
			if (results.isEmpty()) {
				Result theResult = reader.decode(bitmap, HINTS_PURE);
				if (theResult != null) {
					results.add(theResult);
				}
			}
			if (results.isEmpty()) {
				Result theResult = reader.decode(bitmap, HINTS);
				if (theResult != null) {
					results.add(theResult);
				}
			}
			if (results.isEmpty()) {
				Result theResult = reader.decode(bitmap, HINTS);
				if (theResult != null) {
					results.add(theResult);
				}
			}
			for (Result result : results) {
				qrCodeKey = result.getText();
			}

			//동일한 파일이 있다면 삭제 한 후 다시 파일 생성
			if (initialFile.exists()) {
				initialFile.delete();
			}
		}

		if (qrCodeKey.indexOf("secret=") > -1) {
			qrCodeKey = qrCodeKey.split("secret=")[1];

			if (qrCodeKey.indexOf("&") > -1) {
				qrCodeKey = qrCodeKey.split("&")[0];
			}
		} else {
			qrCodeKey = "";
		}

		return qrCodeKey;
	}

	public File convert(MultipartFile file) throws IOException
	{   
		File convFile = new File(originalUploadDir + "/" + file.getOriginalFilename());

		//동일한 파일이 있다면 삭제 한 후 다시 파일 생성
		if (convFile.exists()) {
			convFile.delete();
		}

	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(file.getBytes());
	    fos.close();
	    return convFile;
	}
}
