package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtBankResponseDTO;

@Service
public class SmpBankService extends AbstractSmpService{
	
	

	
	/**
	 * 5.3.1	은행 정보 전체 목록(변형)
	 * @param ExtBankRequstDTO param
	 * @return
	 * @throws URISyntaxException 
	 */
	public List<ExtBankResponseDTO> getBankList() throws URISyntaxException {		
		URIBuilder builder = new URIBuilder(baseUrl + "/v1/bank");
		builder.addParameter("page", "0" );
		builder.addParameter("size", "30");
		builder.addParameter("sort", "bankNm,asc");
		HttpGet get = new HttpGet(builder.build() );
		ArrayList<Header> headers = new ArrayList<>(); 
		return Arrays.asList(callSmpApi(get, ExtBankResponseDTO[].class, headers));	
	}
	
	
	
}
