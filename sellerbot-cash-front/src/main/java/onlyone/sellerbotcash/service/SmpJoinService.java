package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.databind.ObjectMapper;

import onlyone.sellerbotcash.web.error.RestApiException;
import onlyone.smp.persistent.domain.enums.EScraReqTyp;
import onlyone.smp.persistent.domain.enums.ETermsSts;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtAgreeTermsReqDTO;
import onlyone.smp.service.dto.ExtCustAuthTokenDTO;
import onlyone.smp.service.dto.ExtCustAuthTokenRtnDTO;
import onlyone.smp.service.dto.ExtCustMallDTO;
import onlyone.smp.service.dto.ExtCustPackageDTO;
import onlyone.smp.service.dto.ExtMallDTO;
import onlyone.smp.service.dto.ExtSMapUserMallResDTO;
import onlyone.smp.service.dto.ExtSMapUserResDTO;
import onlyone.smp.service.dto.ExtSvcTermsDTO;

@Slf4j
@Service
public class SmpJoinService extends AbstractSmpService {
    @Autowired
    SmpCustMallService smpCustMallService;
    @Autowired
    SmpSalesService smpSalesService;

    @Value("${smp.svcId.sodeone:sodeone}")
    String SVC_ID_SODEONE;

    private int compareYn(EYesOrNo f, EYesOrNo s) {
        if (f == null && s == null)
            return 0;
        if (f == null)
            return 1;
        if (s == null)
            return -1;
        return f.compareTo(s);
    }

    /**
     * 셀러봇용 약관 조회 및 필수 약관 순서를 상위로 이동 처리함.
     * 
     * @return
     */
    public List<ExtSvcTermsDTO> getSellerBotTerms(String svc_id) {
        URIBuilder builder;
        try {
            builder = new URIBuilder(baseUrl + "/v1/service/terms");
            builder.setParameter("svc_id", svc_id);
            // 약관이 20개를 넘지 않을 것으로 생각되어 20으로 설정함.
            builder.setParameter("size", "10");
            builder.setParameter("terms_sts_cd", ETermsSts.PO.name());
            HttpGet get = new HttpGet(builder.build());
            ExtSvcTermsDTO[] list = callSmpApi(get, ExtSvcTermsDTO[].class);

            // 필수 약관을 앞으로 이동함 (Enum의 위치가 Y가 앞에 있어 Y가 앞으로 이동됨.
            return Arrays.stream(list).sorted((f, s) -> {
                return compareYn(f.getTerms_esse_yn(), s.getTerms_esse_yn());
            }).collect(Collectors.toList());

        } catch (URISyntaxException e) {
            throw new RuntimeException("getSellerBotTerms", e);
        }
    }

    /**
     * 사업자 번호 사용 가능 확인
     * 
     * @param bizNo
     * @return true - 사용가능, false - 사용 불가
     * 
     */
    public void checkBizNo(String bizNo) {
        TreeMap<String, String> map = new TreeMap<>();
        map.put("biz_no", bizNo);
        HttpPost post = new HttpPost(baseUrl + "/v1/authentications/biz_no");
        post.setEntity(getStringEntity(map));
        HttpResponse response = callSmpApi(post);
        if (HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK)
            return;
        HttpEntity resEntity = response.getEntity();
        String message = "";
        try {
            TreeMap<?, ?> retval = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
            message = retval.get("code").toString();
        } catch (Exception e) {
            log.error("json Error", e);
            throw new RestApiException(HttpStatus.INTERNAL_SERVER_ERROR, "네트웍 통신 오류 입니다.");
        }
        throw new RestApiException(HttpStatus.CONFLICT, message);
    }

    /**
     * 사용가능한 사업자 번호 인가?
     * 
     * @param bizNo
     * @return
     */
    @SuppressWarnings("unchecked")
    public TreeMap<String, String> isExistsBizNo(String bizNo) {

        TreeMap<String, String> result = new TreeMap<>();
        TreeMap<String, String> map = new TreeMap<>();
        map.put("biz_no", bizNo);
        HttpPost post = new HttpPost(baseUrl + "/v1/authentications/biz_no");
        post.setEntity(getStringEntity(map));
        HttpResponse response = callSmpApi(post);

        if (HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK) {
            result.put("code", HttpStatus.OK.name());
            result.put("reason", "성공");
            result.put("time", LocalDate.now().toString());
        } else {
            try {
                HttpEntity resEntity = response.getEntity();
                result = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
            } catch (Exception e) {
                log.error("" + e);
                result.put("code", HttpStatus.INTERNAL_SERVER_ERROR.name());
                result.put("reason", HttpStatus.INTERNAL_SERVER_ERROR.name());
                result.put("time", LocalDate.now().toString());
            }
        }

        return result;
    }

    /**
     * 2.1.5고객 가입 여부 확인 및 인증 번호 발송
     * 
     * @param custId
     * @return
     */
    public String confirmCust(String custId) {

        TreeMap<String, String> map = new TreeMap<>();
        map.put("cust_id", custId);
        HttpPost post = new HttpPost(baseUrl + "/v1/authentications/cust/confirm");
        post.setEntity(getStringEntity(map));
        ExtCustAuthTokenRtnDTO retval = callSmpApi(post, ExtCustAuthTokenRtnDTO.class);

        return retval.getAuth_token();

    }

    /**
     * 2.1.6 고객 인증 번호 확인
     * 
     * @param custId
     * @param authNo
     * @param token
     * @return
     */
    public boolean confirmCustAuthtoken(String custId, String authNo, String token) {
        ExtCustAuthTokenDTO custAuthToken = new ExtCustAuthTokenDTO();
        custAuthToken.setCust_id(custId);
        custAuthToken.setAuth_no(authNo);
        custAuthToken.setAuth_token(token);
        HttpPost post = new HttpPost(baseUrl + "/v1/authentications/cust/authtoken/confirm");
        post.setEntity(getStringEntity(custAuthToken));
        return callSmpApiIsOk(post);
    }

    /**
     * 고객 추가
     * 
     * @param cust
     * @return
     */
    public boolean addCustPkg(ExtCustPackageDTO cust) {
        HttpPost post = new HttpPost(baseUrl + "/v1/cust/pkg");
        post.setEntity(getStringEntity(cust));
        return callSmpApiIsOk(post);

    }

    public List<ExtMallDTO> getMallList() {
        URIBuilder builder;
        try {
            builder = new URIBuilder(baseUrl + "/v1/mall");
            builder.addParameter("size", "500");

            HttpGet get = new HttpGet(builder.build());
            ArrayList<Header> headers = new ArrayList<>();
            List<ExtMallDTO> list = Arrays.asList(callSmpApi(get, ExtMallDTO[].class, headers));
            return list;
        } catch (URISyntaxException e) {
            throw new RuntimeException("mallList", e);
        }
    }

    /**
     * 모든 몰 정보
     * 
     * @return
     */
    public List<ExtMallDTO> getMallAllList() {
        URIBuilder builder;
        try {
            builder = new URIBuilder(baseUrl + "/v1/mall");
            builder.addParameter("size", "100");

            HttpGet get = new HttpGet(builder.build());
            ArrayList<Header> headers = new ArrayList<>();
            List<ExtMallDTO> list = Arrays.asList(callSmpApi(get, ExtMallDTO[].class, headers));
            return list;
        } catch (URISyntaxException e) {
            throw new RuntimeException("mallList", e);
        }

    }

    public List<ExtCustMallDTO> createMall(List<ExtCustMallDTO> extCustMallDTO, String username) {
        HttpPost post = new HttpPost(baseUrl + "/v1/cust/" + username + "/mall");
        post.setEntity(getStringEntity(extCustMallDTO));
        ArrayList<Header> headers = new ArrayList<>();
        List<ExtCustMallDTO> list = Arrays.asList(callSmpApi(post, ExtCustMallDTO[].class, headers));
        return list;
    }

    public boolean scrap(List<ExtCustMallDTO> dto, String username) {

        boolean isSodeOne = isExistSvcMember(SVC_ID_SODEONE, username);

        try {
            Calendar cal = new GregorianCalendar(Locale.KOREA);
            SimpleDateFormat fm = new SimpleDateFormat("yyyyMM");
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, -1);
            // 스크래핑 종료일자
            String scra_end_dt = fm.format(cal.getTime());
            cal.add(Calendar.YEAR, -3);
            // 스크래핑 시작일자(최초)
            String scra_fst_dt = fm.format(cal.getTime());
            cal.add(Calendar.YEAR, 3);
            cal.add(Calendar.MONTH, -2);
            // 스크래핑 시작일자(최초X)
            String scra_stt_dt = fm.format(cal.getTime());
            for (int i = 0; i < dto.size(); i++) {
                String custMallSeqNo = dto.get(i).getCust_mall_seq_no().toString();

                
                ExtCustMallDTO custMallRlt = smpCustMallService.getMallInfo(username, custMallSeqNo);
                HashMap<String, Object> map = new HashMap<String, Object>();
                if (custMallRlt.getFst_scra_yn() == EYesOrNo.Y) {
                    String salseRecentDate = smpSalesService.getMallSalseRecentDate(username, custMallSeqNo);

                    // 매출 데이터가 없을 경우 3년치 다시 조회
                    if (StringUtils.isEmpty(salseRecentDate)) {
                        map.put("scra_stt_dt", scra_fst_dt);
                    } else {
                        SimpleDateFormat compareFormet = new SimpleDateFormat("yyyymm");

                        Date recentDate = new Date(compareFormet.parse(salseRecentDate).getTime()); // 매출 최근 데이터
                        Date scraSttDate = new Date(compareFormet.parse(scra_stt_dt).getTime()); // 셋팅 날짜 데이터

                        int compareDate = scraSttDate.compareTo(recentDate);

                        if (compareDate > 0) {
                            map.put("scra_stt_dt", salseRecentDate); // 매출 최근 데이터 날짜로 셋팅
                        } else {
                            map.put("scra_stt_dt", scra_stt_dt);
                        }
                    }

                } else {
                    map.put("scra_stt_dt", scra_fst_dt);
                }
                map.put("cust_mall_seq_no", dto.get(i).getCust_mall_seq_no());
                map.put("scra_end_dt", scra_end_dt);
                map.put("scra_req_typ", EScraReqTyp.USR.name());
                // 스크랩타입 회원정보: 00, 매출: 01, 정산: 02, 정산예정금: 03, 반품율: 04, 상품: 05, 오늘 정산금: 06
                // map.put("scra_typ", "00,03");
                // 2019-11-29
                // 2020-03-03 오늘 정산금 추가 [SMP-231]
                map.put("scra_typ", "00,01,02,03,04,06");
                HttpPost post = new HttpPost(baseUrl + "/v1/scrap");
                post.setEntity(getStringEntity(map));

                // 스크래핑
                callSmpApiIsOk(post);
                if (isSodeOne) {
                    // 알람은 소드원 회원에게만 발송한다.
                    sendChangeMallInfo(dto.get(i).getCust_mall_seq_no().toString());
                }

            }
            return true;
        } catch (Exception e) {
            log.error("" + e);
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public TreeMap<String, String> updateMall(List<ExtCustMallDTO> extCustMallDTO, String username) {
        HttpResponse res = null;
        TreeMap<String, String> result = new TreeMap<>();

        for (int i = 0; i < extCustMallDTO.size(); i++) {
            HttpPut put = new HttpPut(baseUrl + "/v1/cust/" + username + "/mall");
            put.setEntity(getStringEntity(extCustMallDTO));

            res = callSmpApi(put);

            if (HttpStatus.resolve(res.getStatusLine().getStatusCode()) == HttpStatus.OK) {
                result.put("code", HttpStatus.OK.name());
                result.put("reason", "성공");
            } else {
                try {
                    HttpEntity resEntity = res.getEntity();
                    result = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
                } catch (Exception e) {
                    log.error("" + e);
                    result.put("code", HttpStatus.INTERNAL_SERVER_ERROR.name());
                    result.put("reason", HttpStatus.INTERNAL_SERVER_ERROR.name());
                    result.put("time", LocalDate.now().toString());
                }

                break;
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public List<HashMap<String, Object>> custAndBiz(String svc_cust_id, String token) {
        URIBuilder builder;
        List<HashMap<String, Object>> list = null;
        try {
            builder = new URIBuilder(svcBaseUrl + "/restapi/svc/cust_and_biz.php");
            builder.setParameter("svc_cust_id", svc_cust_id);
            builder.setParameter("auth_token", token);
            builder.setParameter("svc_id", svcId);

            HttpGet get = new HttpGet(builder.build());
            list = callSmpApi(get, List.class);
        } catch (RestApiException e) {
            throw e;
        } catch (Exception e) {
            log.error("Error", e);
            return null;
        }
        return list;
    }

    /**
     * 서비스(소드원 인증) 정보
     * 
     * @param passwd
     * @param svc_cust_id
     * @param token
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<HashMap<String, Object>> custAndBiz(String passwd, String svc_cust_id, String token) {
        URIBuilder builder;
        List<HashMap<String, Object>> list = null;
        try {
            builder = new URIBuilder(svcBaseUrl + "/restapi/svc/cust_auth.php");
            builder.setParameter("passwd", passwd);
            builder.setParameter("svc_cust_id", svc_cust_id);
            builder.setParameter("auth_token", token);
            builder.setParameter("svc_id", svcId);

            HttpGet get = new HttpGet(builder.build());
            list = callSmpApi(get, List.class);
        } catch (RestApiException e) {
            throw e;
        } catch (Exception e) {
            log.error("Error", e);
            throw new RuntimeException("getSellerBotTerms", e);
        }
        return list;

    }

    /**
     * 내용 변경 rest리턴 형식 명칭 앞에 "svc_"추가예정
     * 
     * @param svc_cust_id
     * @param auth_token
     * @return
     */
    @SuppressWarnings("unchecked")
    public HashMap<String, Object> getSodeCustInfo(String svc_cust_id, String auth_token) {
        URIBuilder builder;
        HashMap<String, Object> map = null;
        try {
            builder = new URIBuilder(svcBaseUrl + "/restapi/svc/cust/detail.php");
            builder.setParameter("cust_id", svc_cust_id);
            builder.setParameter("auth_token", auth_token);
            builder.setParameter("svc_id", svcId);

            HttpGet get = new HttpGet(builder.build());
            map = (HashMap<String, Object>) callSmpApi(get, Map.class);
            return map;
        } catch (RestApiException e) {
            throw e;
        } catch (Exception e) {
            log.error("Error", e);
            return null;
        }
    }

    /**
     * 셀러봇 캐시 약관추가
     * 
     * @param terms
     * @return
     */
    public boolean addTerms(List<ExtAgreeTermsReqDTO> terms) {

        // HashMap<String,Object> map = null;
        HttpPost post = new HttpPost(baseUrl + "/v1/service/terms/aggr");
        post.setEntity(getStringEntity(terms));

        return callSmpApiIsOk(post);

    }

    /**
     * 셀러봇 캐시 약관추가
     * 
     * @param terms
     * @return
     */
    public boolean delTerms(List<ExtAgreeTermsReqDTO> terms) {

        // HashMap<String,Object> map = null;
        HttpPost post = new HttpPost(baseUrl + "/v1/service/terms/del");
        post.setEntity(getStringEntity(terms));

        return callSmpApiIsOk(post);

    }

    /**
     * 고객 일번변호 등록
     * 
     * @param map
     */
    public void svcCustInsert(HashMap<String, Object> map) {
        HttpPost post = new HttpPost(svcBaseUrl + "/restapi/svc/cust.php");
        post.setEntity(getStringEntity(map));
        callSmpApi(post);
    }

    /**
     * 3.2.2. 서비스 유효 여부 서비스 아이디가 사용가능한지 판단한다.
     * 
     * @param svcid
     * @return true: 사용가능, false: 사용불가
     */
    public boolean isExistSvcId(String svcid) {
        String url = baseUrl + "/v1/service/" + svcid + "/available";
        HttpGet httpGet = new HttpGet(url);
        return callSmpApiIsOk(httpGet);
    }

    /**
     * 3.2.1 서비스 고객 가입 여부
     * 
     * @param svcid
     * @param custId
     * @return
     */
    public boolean isExistSvcMember(String svcid, String custId) {
        String url = baseUrl + "/v1/service/" + svcid + "/cust/" + custId + "/membership";
        HttpGet httpGet = new HttpGet(url);
        return callSmpApiBodyIsOk(httpGet);
    }

    public boolean sendChangeMallInfo(String cust_mall_seq_no) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cust_mall_seq_no", cust_mall_seq_no);
        HttpPost post = new HttpPost(svcBaseUrl + "/restapi/svc/cust_mall_alarm.php");
        post.setEntity(getStringEntity(map));
        return callSmpApiIsOk(post);
    }

    /**
     * S-MAP 회원 정보
     * 
     * @param dto
     * @return
     */
    public ExtSMapUserResDTO getSMapMemberInfo(String token, String bizNo) {
        URIBuilder builder;
        ExtSMapUserResDTO result = new ExtSMapUserResDTO();
        try {
            String url = smapBaseUrl + "/restapi/member/get_info.php";
            builder = new URIBuilder(url);
            builder.setParameter("token", token);
            builder.setParameter("biz_no", bizNo);

            HttpGet get = new HttpGet(builder.build());
            result = callSmpApi(get, ExtSMapUserResDTO.class);

        } catch (RestApiException e) {
            log.error("" + e);
            throw e;
        } catch (Exception e) {
            log.error("" + e);
            return null;
        }
        return result;
    }

    /**
     * 화면 표시용 사용자 몰정보
     * 
     * @param dto
     * @param bizNo
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, String>> getSMapMemberInfo(String token, String bizNo, boolean isReadOnly) {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        // 사용자 정보
        ExtSMapUserResDTO resDto = getSMapMemberInfo(token, bizNo);

        // 관리중인 전체 몰
        List<ExtMallDTO> mallList = getMallAllList();

        ObjectMapper oMapper = new ObjectMapper();

        for (ExtSMapUserMallResDTO mall : resDto.getMall()) {
            Map<String, String> map = oMapper.convertValue(mall, Map.class);

            if (isReadOnly) {
                map.remove("mall_cert_1st_passwd");
                map.remove("mall_cert_2nd_passwd");
            }

            for (ExtMallDTO extMall : mallList) {
                if (mall.getMall_cd().equals(extMall.getMall_cd())) {
                    map.put("mall_nm", extMall.getMall_nm());
                    map.put("sub_mall_cert_1st_nm", extMall.getSub_mall_cert_1st_nm());
                    map.put("sub_mall_cert_2nd_nm", extMall.getSub_mall_cert_2nd_nm());
                    map.put("cert_step", extMall.getSub_mall_cert_step().toString());
                    map.put("passwd_step", extMall.getPasswd_step().toString());

                    if (extMall.getMall_cert_typ().size() > 0) {
                        map.put("mall_cert_typ", extMall.getMall_cert_typ().get(0).getMall_cert_typ_cd().name());
                    }

                    break;
                }
            }

            result.add(map);
        }

        return result;
    }

}
