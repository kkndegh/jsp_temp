package onlyone.sellerbotcash.service;

import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtRtnGraphDTO;
import onlyone.smp.service.dto.ExtRtnStatusDTO;
import onlyone.smp.service.dto.ExtRtnSummaryLastDTO;
import onlyone.smp.service.dto.ExtRtnSummaryPeriodDTO;

/**
 * 반품 통계 Service
 * @author YunaJang
 */
@Service
public class SmpRtnService extends AbstractSmpService {

    /**
     * 4.7.1	반품 요약 – 최근반품 분석
     * @param custId
     * @return
     */
    public ExtRtnSummaryLastDTO getSummaryLast(String custId) {
        HttpGet getSummaryLast = new HttpGet(baseUrl + "/v1/rtn/cust/" + SbfUtils.encodeUtf8(custId) + "/summary/last");
        ExtRtnSummaryLastDTO summaryLastResult = callSmpApi(getSummaryLast, ExtRtnSummaryLastDTO.class);
        return summaryLastResult;
    }

    /**
     * 4.7.2	반품 요약 – 조회기간 분석
     * @param custId
     * @param startDate
     * @param endDate
     * @return
     */
    public ExtRtnSummaryPeriodDTO getSummaryPeriod(String custId, String startDate, String endDate) {
        StringBuffer apiUrl = new StringBuffer();
        apiUrl.append(baseUrl).append("/v1/rtn/cust/").append(SbfUtils.encodeUtf8(custId)).append("/summary/period");
        apiUrl.append("?rtn_find_stt_dt=").append(startDate).append("&rtn_find_end_dt=").append(endDate);

        HttpGet getSummaryPeriod = new HttpGet(apiUrl.toString());
        ExtRtnSummaryPeriodDTO summaryPeriodResult = callSmpApi(getSummaryPeriod, ExtRtnSummaryPeriodDTO.class);
        return summaryPeriodResult;
    }

    /**
     * 4.7.3	반품 현황
     * @param custId
     * @param startDate
     * @param endDate
     * @param mallCd
     * @return
     */
    public ExtRtnStatusDTO getMonthInfo(String custId, String startDate, String endDate, String mallCd) {
        StringBuffer apiUrl = new StringBuffer();
        apiUrl.append(baseUrl).append("/v1/rtn/cust/").append(SbfUtils.encodeUtf8(custId));
        apiUrl.append("?rtn_find_stt_dt=").append(startDate).append("&rtn_find_end_dt=").append(endDate);

        if(mallCd != null && !mallCd.equals(""))
            apiUrl.append("&mall_cd=").append(mallCd);

        HttpGet getMonthInfo = new HttpGet(apiUrl.toString());
        ExtRtnStatusDTO monthInfo = callSmpApi(getMonthInfo, ExtRtnStatusDTO.class);
        return monthInfo;
    }

    /**
     * 4.7.4	반품 분석 그래프
     * @param custId
     * @param startDate
     * @param endDate
     * @return
     */
    public ExtRtnGraphDTO getGraphInfo(String custId, String startDate, String endDate) {
        StringBuffer apiUrl = new StringBuffer();
        apiUrl.append(baseUrl).append("/v1/rtn/cust/").append(SbfUtils.encodeUtf8(custId)).append("/mall");
        apiUrl.append("?rtn_find_stt_dt=").append(startDate).append("&rtn_find_end_dt=").append(endDate);

        HttpGet getGraphInfo = new HttpGet(apiUrl.toString());
        ExtRtnGraphDTO graphInfo = callSmpApi(getGraphInfo, ExtRtnGraphDTO.class);
        return graphInfo;
    }
}