package onlyone.sellerbotcash.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.web.error.RestApiException;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.smp.persistent.domain.enums.EAdvPrgsSts;
import onlyone.smp.persistent.domain.enums.ECustPosi;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EUseSts;
import onlyone.smp.service.dto.ExtAdvReqDTO;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtCustDetailDTO;
import onlyone.smp.service.dto.ExtCustSvcDTO;
import onlyone.smp.service.dto.ExtCustTerminateDTO;
import onlyone.smp.service.dto.ExtKkontTgtDTO;

@Slf4j
@Service
public class SmpCustService extends AbstractSmpService {
	private Path fileStorageLocation = null;

	@Value("${image.originalUploadDir}")
	private String originalUploadDir;

	@PostConstruct
	public void init() {
		if (originalUploadDir.isEmpty())
			return;

		fileStorageLocation = Paths.get(originalUploadDir).toAbsolutePath().normalize();
		try {
			Files.createDirectories(fileStorageLocation);
		} catch (Exception ex) {
			throw new RuntimeException("Could not create the directory where the uploaded files will be stored.", ex);
		}
	}

	/**
	 * 고객 정보 조회
	 * 
	 * @param username
	 * @return
	 */
	public ExtCustDTO getCust(String username) {
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username));
		return callSmpApi(get, ExtCustDTO.class);
	}

	/**
	 * 고객 상세 정보
	 * 
	 * @param username
	 * @return
	 */
	public ExtCustDetailDTO getCustDetail(String username) {
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username));
		return callSmpApi(get, ExtCustDetailDTO.class);
	}

	public void editCust(String cust_id, String ceo_no, String chrg_nm, String chrg_no) {

		TreeMap<String, String> map = new TreeMap<>();
		map.put("ceo_no", ceo_no);
		map.put("chrg_nm", chrg_nm);
		map.put("chrg_no", chrg_no);

		HttpPut put = new HttpPut(baseUrl + "/v1/cust/" + cust_id);
		put.setEntity(getStringEntity(map));
		HttpResponse response = callSmpApi(put);
		if (HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK)
			return;
		HttpEntity resEntity = response.getEntity();
		String message = "";
		try {
			TreeMap<?, ?> retval = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
			message = retval.get("reason").toString();
		} catch (Exception e) {
			log.error("json Error", e);
			throw new RestApiException(HttpStatus.INTERNAL_SERVER_ERROR, "네트웍 통신 오류 입니다.");
		}
		throw new RestApiException(HttpStatus.CONFLICT, message);
	}

	public void editSmtCust(String cust_id, String ceo_nm, String ceo_no) {

		TreeMap<String, String> map = new TreeMap<>();
		map.put("ceo_nm", ceo_nm);
		map.put("ceo_no", ceo_no);

		HttpPut put = new HttpPut(baseUrl + "/v1/cust/" + cust_id);
		put.setEntity(getStringEntity(map));
		HttpResponse response = callSmpApi(put);
		if (HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK)
			return;
		HttpEntity resEntity = response.getEntity();
		String message = "";
		try {
			TreeMap<?, ?> retval = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
			message = retval.get("reason").toString();
		} catch (Exception e) {
			log.error("json Error", e);
			throw new RestApiException(HttpStatus.INTERNAL_SERVER_ERROR, "네트웍 통신 오류 입니다.");
		}
		throw new RestApiException(HttpStatus.CONFLICT, message);
	}

	public String change_pw(String username, String passwd, String new_passwd, String new_passwd_confirm) {
		TreeMap<String, String> map = new TreeMap<>();
		map.put("cust_id", username);
		map.put("passwd", passwd);
		map.put("new_passwd", new_passwd);
		map.put("new_passwd_confirm", new_passwd_confirm);

		HttpPut put = new HttpPut(baseUrl + "/v1/authentications/cust/password");
		put.setEntity(getStringEntity(map));
		HttpResponse response = callSmpApi(put);
		if (HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK)
			return "OK";
		HttpEntity resEntity = response.getEntity();
		try {

			String body = EntityUtils.toString(resEntity);

			// log.debug("body: "+body);
			// log.debug("-------------------------------------------------------");
			TreeMap<?, ?> retval = objectMapper.readValue(body, TreeMap.class);

			return retval.get("code").toString();
		} catch (Exception e) {
			log.error("json Error", e);
			throw new RestApiException(HttpStatus.INTERNAL_SERVER_ERROR, "네트웍 통신 오류 입니다.");
		}
	}

	public List<ExtCustSvcDTO> getSvcList(String username) {
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/svc");
		ArrayList<Header> headers = new ArrayList<>();
		try {
			return Arrays.asList(callSmpApi(get, ExtCustSvcDTO[].class, headers));
		} catch (Exception e) {
			return null;
		}
	}

	public ListData<ExtAdvReqDTO> getReqList(int page, String userName) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/adv/req");
			builder.addParameter("page", Integer.toString(page));
			builder.addParameter("size", "10");
			builder.addParameter("sort", "regTs,desc");
			builder.addParameter("adv_req_id", userName);
			HttpGet get = new HttpGet(builder.build());
			ArrayList<Header> headers = new ArrayList<>();
			List<ExtAdvReqDTO> list = Arrays.asList(callSmpApi(get, ExtAdvReqDTO[].class, headers));
			log.debug("headers {0}", headers);
			return new ListData<ExtAdvReqDTO>(Integer.parseInt(headers.get(0).getValue()), page, list);

		} catch (URISyntaxException e) {
			throw new RuntimeException("getReqList", e);
		}
	}

	public ExtAdvReqDTO getReq(String adv_req_seq_no, String userName) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/adv/req/" + adv_req_seq_no);
			// 문서에는 등록자 아이디를 기준으로 변경 가능
			builder.addParameter("req_id", userName);
			HttpGet get = new HttpGet(builder.build());
			ArrayList<Header> headers = new ArrayList<>();
			ExtAdvReqDTO extAdvReqDTO = callSmpApi(get, ExtAdvReqDTO.class, headers);
			return extAdvReqDTO;

		} catch (URISyntaxException e) {
			throw new RuntimeException("getReq", e);
		}
	}

	public boolean ask_save(String username, String adv_typ_cd, String adv_title, String adv_cont,
			MultipartFile adv_req_file) throws IOException {
		HttpPost post = new HttpPost(baseUrl + "/v1/adv/req");
		File file = null;

		if (adv_req_file != null) {
			file = convert(adv_req_file);
			HttpEntity entity = MultipartEntityBuilder.create()
					.addTextBody("cust_id", username, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_typ_cd", adv_typ_cd, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_req_id", username, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_req_tm",
							LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")),
							ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_title", adv_title, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_cont", adv_cont, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_prgs_sts_cd", EAdvPrgsSts.ADR.name(),
							ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addBinaryBody("adv_req_file", file, ContentType.create("application/octet-stream", Consts.UTF_8),
							file.getName())
					.build();
			post.setEntity(entity);
		} else {
			HttpEntity entity = MultipartEntityBuilder.create()
					.addTextBody("cust_id", username, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_typ_cd", adv_typ_cd, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_req_id", username, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_req_tm",
							LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")),
							ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_title", adv_title, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_cont", adv_cont, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_prgs_sts_cd", EAdvPrgsSts.ADR.name(),
							ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.build();
			post.setEntity(entity);
		}

		return callSmpApiIsOk(post);

	}

	public File convert(MultipartFile file) throws IOException {
		File convFile = new File(originalUploadDir + "/" + file.getOriginalFilename());

		// 동일한 파일이 있다면 삭제 한 후 다시 파일 생성
		if (convFile.exists()) {
			convFile.delete();
		}

		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public boolean ask_update(String username, String adv_typ_cd, String adv_title, String adv_cont,
			MultipartFile adv_req_file, String adv_req_seq_no, String adv_req_file_del_yn) throws IOException {

		String url = baseUrl + "/v1/adv/req/" + adv_req_seq_no;

		HttpPost post = new HttpPost(url);
		File file = null;

		log.debug("url: " + url);

		if (adv_req_file != null) {
			file = convert(adv_req_file);
			HttpEntity entity = MultipartEntityBuilder.create()
					.addTextBody("adv_req_seq_no", adv_req_seq_no, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("cust_id", username, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_typ_cd", adv_typ_cd, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_title", adv_title, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_cont", adv_cont, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_req_file_del_yn", adv_req_file_del_yn,
							ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addBinaryBody("adv_req_file", file, ContentType.create("application/octet-stream", Consts.UTF_8),
							file.getName())
					.build();
			post.setEntity(entity);
		} else {
			HttpEntity entity = MultipartEntityBuilder.create()
					.addTextBody("adv_req_seq_no", adv_req_seq_no, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("cust_id", username, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_typ_cd", adv_typ_cd, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_title", adv_title, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_cont", adv_cont, ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.addTextBody("adv_req_file_del_yn", adv_req_file_del_yn,
							ContentType.create("text/plain", MIME.UTF8_CHARSET))
					.build();
			post.setEntity(entity);
		}

		return callSmpApiIsOk(post);
	}

	public List<ExtKkontTgtDTO> getAlarm(String username) {

		String url = baseUrl + "/v1/kkont/cust/" + SbfUtils.encodeUtf8(username);
		HttpGet get = new HttpGet(url);

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> getData = callSmpApi(get, List.class);
		List<ExtKkontTgtDTO> rtnVal = new ArrayList<ExtKkontTgtDTO>();
		if (getData.size() == 0) {
			return null;
		}

		log.debug("{}", getData);

		for (HashMap<String, Object> item : getData) {
			ExtKkontTgtDTO dto = new ExtKkontTgtDTO();
			log.debug("{}", item);

			dto.setKkont_tgt_no(Long.parseLong(Objects.toString(item.get("kkont_tgt_no"), "0")));
			dto.setTgt_cust_nm(Objects.toString(item.get("tgt_cust_nm"), ""));
			dto.setTgt_cust_no(Objects.toString(item.get("tgt_cust_no"), ""));
			dto.setTrs_hop_hour(Integer.parseInt(Objects.toString(item.get("trs_hop_hour"), "0")));

			if (Objects.toString(item.get("use_sts_cd"), "").equals("AVT")) {
				dto.setUse_sts_cd(EUseSts.AVT);
			}

			if (Objects.toString(item.get("use_sts_cd"), "").equals("HLD")) {
				dto.setUse_sts_cd(EUseSts.HLD);
			}
			if (Objects.toString(item.get("use_sts_cd"), "").equals("TER")) {
				dto.setUse_sts_cd(EUseSts.TER);
			}
			if (Objects.toString(item.get("cust_posi_cd"), "").equals("CEO")) {
				dto.setCust_posi_cd(ECustPosi.CEO);
			}
			if (Objects.toString(item.get("cust_posi_cd"), "").equals("CRG")) {
				dto.setCust_posi_cd(ECustPosi.CRG);
			}

			rtnVal.add(dto);
		}

		return rtnVal;
	}

	public boolean alarm_update(String username, ExtKkontTgtDTO dto) {
		HttpPut put = new HttpPut(baseUrl + "/v1/kkont/" + dto.getKkont_tgt_no() + "/cust/" + username);
		put.setEntity(getStringEntity(dto));
		return callSmpApiIsOk(put);
	}

	public boolean alarm_insert(String username, ExtKkontTgtDTO dto) {
		HttpPost post = new HttpPost(baseUrl + "/v1/kkont/cust/" + username);
		List<ExtKkontTgtDTO> list = new ArrayList<ExtKkontTgtDTO>();
		list.add(dto);
		post.setEntity(getStringEntity(list));
		return callSmpApiIsOk(post);
	}

	public boolean custTrial(String tgt_cust_no) {
		HttpPost post = new HttpPost(baseUrl + "/v1/kkont/cust/trial");
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("tgt_cust_no", tgt_cust_no);
		post.setEntity(getStringEntity(param));

		return callSmpApiIsOk(post);
	}

	public boolean tokenCheck(String token) {
		if (null == token) {
			return false;
		} else {
			HttpPost post = new HttpPost(baseUrl + "/v1/authentications/authtoken/confirm");
			HashMap<String, String> param = new HashMap<String, String>();
			param.put("auth_token", token);
			post.setEntity(getStringEntity(param));

			return callSmpApiIsOk(post);
		}
	}

	public String getToken(String svc_cust_id) {

		HttpPost post = new HttpPost(baseUrl + "/v1/authentications/authtoken/");
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("auth_id", svc_cust_id);
		post.setEntity(getStringEntity(param));
		@SuppressWarnings("unchecked")
		Map<String, String> map = callSmpApi(post, Map.class);
		return map.get("auth_token");
	}

	public HashMap<String, Object> sellerConfirm(String cust_id, String password) {
		HttpPost post = new HttpPost(baseUrl + "/v1/authentications/cust/passwd/confirm");
		HttpResponse response = null;
		HashMap<String, String> param = new HashMap<String, String>();
		setServiceInfo(post);
		try {
			HttpClient client = HttpClients.createDefault();
			param.put("cust_id", cust_id);
			param.put("passwd", password);
			post.setEntity(getStringEntity(param));

			response = client.execute(post);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		StatusLine status = response.getStatusLine();
		HashMap<String, Object> result = new HashMap<String, Object>();
		if (HttpStatus.resolve(status.getStatusCode()) != HttpStatus.OK) {
			HttpEntity resEntity = response.getEntity();
			String errorCode = "";
			try {
				TreeMap<?, ?> retval = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
				errorCode = retval.get("code").toString();
			} catch (IOException e) {
				log.error("read body", e);
			}
			result.put("result", false);
			result.put("errorCode", errorCode);
		} else {
			result.put("result", true);
		}
		return result;
	}

	public boolean custTerminate(ExtCustTerminateDTO dto) {
		HttpPost post = new HttpPost(baseUrl + "/v1/cust/terminate");
		HashMap<String, String> param = new HashMap<String, String>();
		param.put("cust_id", dto.getCust_id());
		param.put("canc_reas_cd", dto.getCanc_reas_cd());
		param.put("canc_dti_reas", dto.getCanc_dti_reas());
		param.put("rlt_cd", dto.getRlt_cd());
		param.put("rlt_msg", dto.getRlt_msg());
		param.put("req_nm", dto.getReq_nm());
		param.put("di_val", dto.getDi_val());
		param.put("ci_val", dto.getCi_val());
		param.put("req_birthday", dto.getReq_birthday());
		param.put("req_gender", dto.getReq_gender());
		param.put("req_nation", dto.getReq_nation());
		param.put("req_telcmmcd", dto.getReq_telcmmcd());
		param.put("req_ceph_no", dto.getReq_ceph_no());
		param.put("retrunmsg", dto.getRetrunmsg());
		post.setEntity(getStringEntity(param));
		return callSmpApiIsOk(post);

	}

	/**
	 * 계좌 삭제 가능 여부 확인
	 * 
	 * @param biz_nm
	 * @param ceph_no
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String getCustAcctDeleteConfirm(String cust_id) {
		String result = "Y";
		String auth_token = getToken(cust_id);
		Map<String, String> map = new HashMap<String, String>();
		map.put("cust_id", cust_id);
		map.put("auth_token", auth_token);
		map.put("svc_id", svcId);

		// 2020-03-18 위의 getLoanMarketList()와 마찬가지로 소스원/소드투 고객을 구분할 수 없기 때문에 API를 각각 한번씩
		// 호출해야됨
		// 추후 GSM이 새로 개발되면 연관된 부분 수정 필요
		// sodeone
		HttpPost post = new HttpPost(svcBaseUrl + "/restapi/svc/cust_account_loan.php");
		post.setEntity(getStringEntity(map));
		List<Map<String, String>> sodeoneResult = (List<Map<String, String>>) callSmpApi(post, List.class);

		for (Map<String, String> data : sodeoneResult) {
			if (data.containsKey("del_yn") && data.get("del_yn").equals("N")) {
				result = "N";
				return result;
			}
		}

		// sodetwo
		HttpPost postForSodetwo = new HttpPost(svcBaseUrlSodetwo + "/restapi/svc/cust_account_loan.php");
		postForSodetwo.setEntity(getStringEntity(map));
		List<Map<String, String>> sodetwoResult = (List<Map<String, String>>) callSmpApi(postForSodetwo, List.class);

		for (Map<String, String> data : sodetwoResult) {
			if (data.containsKey("del_yn") && data.get("del_yn").equals("N")) {
				result = "N";
				return result;
			}
		}

		return result;
	}

	/**
	 * 대출 기사용고객인지 여부
	 * 
	 * @param biz_nm
	 * @param ceph_no
	 * @return
	 */
	// @SuppressWarnings("unchecked")
	public String getCustUseLoan(String cust_id) {
		// String result = "N";
		// String auth_token = getToken(cust_id);
		// Map<String, String> map = new HashMap<String, String>();
		// map.put("cust_id", cust_id);
		// map.put("auth_token", auth_token);
		// map.put("svc_id", svcId);

		// log.info("map.get(cust_id) = {} ", map.get("cust_id"));
		// // 2020-03-18 위의 getLoanMarketList()와 마찬가지로 소스원/소드투 고객을 구분할 수 없기 때문에 API를 각각
		// 한번씩
		// // 호출해야됨
		// // 추후 GSM이 새로 개발되면 연관된 부분 수정 필요
		// // sodeone

		// HttpPost post = new HttpPost(svcBaseUrl +
		// "/restapi/svc/cust/imnUseLoan.php");
		// post.setEntity(getStringEntity(map));
		// List<String> sodeoneResult = callSmpApi(post, List.class);

		// for (String data : sodeoneResult) {
		// if (cust_id.equals(data)) {
		// result = "Y";
		// return result;
		// }
		// }

		// HttpPost postForSodetwo = new HttpPost(svcBaseUrlSodetwo +
		// "/restapi/svc/cust/imnUseLoan.php");
		// postForSodetwo.setEntity(getStringEntity(map));
		// List<String> sodetwoResult = callSmpApi(postForSodetwo, List.class);

		// for (String data : sodetwoResult) {
		// if (cust_id.equals(data)) {
		// result = "Y";
		// return result;
		// }
		// }

		/**
		 * 2023.09.06 소드 회원 여부 확인(테이블 조회로 변경)
		 */
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/sodeCustChk/" + cust_id);
		String sodeCustChk = callSmpApiBodyIsOk(get) ? "Y" : "N";

		log.info(" 소드 회원 여부 확인 = {} ", sodeCustChk);

		return sodeCustChk;
	}

	public ExtCustDTO getCustOnSite(EInfPath siteCd, String siteId) {
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + siteCd + "/" + siteId);
		ExtCustDTO cust = null;
		try {
			cust = callSmpApi(get, ExtCustDTO.class);
		} catch (Exception e) {
		}
		return cust;
	}

	public ExtCustDTO getCustOnSiteByBizNo(String bizNo) {
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/bizNo" + "/" + bizNo);
		ExtCustDTO cust = null;
		try {
			cust = callSmpApi(get, ExtCustDTO.class);
		} catch (Exception e) {
		}
		return cust;
	}

	public void addCustEvent(String custId, Long eventNo) {
		HttpPost post = new HttpPost(baseUrl + "/v1/cust/event/add");
		Map<String, String> m = new HashMap<String, String>();
		m.put("cust_id", custId);
		m.put("event_no", String.valueOf(eventNo));

		post.setEntity(getStringEntity(m));

		callSmpApi(post);
	}

	public Long getValidCustEvent(String custId) {
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/event/" + custId);
		return callSmpApi(get, Long.class);
	}

	/**
	 * 2021.02.25 추가 external으로 연동 업체 알람 생성
	 * 
	 * @param username
	 * @param dto
	 * @return
	 */
	public boolean alarm_external_insert(String username, ExtKkontTgtDTO dto) {
		HttpPost post = new HttpPost(baseUrl + "/v1/kkont/cust/external/" + username);
		List<ExtKkontTgtDTO> list = new ArrayList<ExtKkontTgtDTO>();
		list.add(dto);
		post.setEntity(getStringEntity(list));
		return callSmpApiIsOk(post);
	}

	/**
	 * 20201.06.16 추가 회원가입 알람
	 * 
	 * @param cust_id
	 */
	public void alarm_send_join(String cust_id) {
		HttpPost post = new HttpPost(baseUrl + "/v1/kkont/cust/join/" + cust_id);
		callSmpApiIsOk(post);
	}

	public boolean getPaymentSkipCustChkFlag(String cust_id) {
		HttpPost post = new HttpPost(baseUrl + "/v1/cust/getPaymentSkipCustChkFlag/" + cust_id);
		post.setEntity(getStringEntity(cust_id));
		return callSmpApiIsOk(post);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getCustUseLoanMap(String cust_id) {

		String result = "N";
		String auth_token = getToken(cust_id);

		Map<String, String> resultMap = new HashMap<String, String>();
		resultMap.put("cust_typ", result);
		resultMap.put("loan_error_yn", "N");

		Map<String, String> map = new HashMap<String, String>();
		map.put("cust_id", cust_id);
		map.put("auth_token", auth_token);
		map.put("svc_id", svcId);

		// 2020-03-18 위의 getLoanMarketList()와 마찬가지로 소스원/소드투 고객을 구분할 수 없기 때문에 API를 각각 한번씩
		// 호출해야됨
		// 추후 GSM이 새로 개발되면 연관된 부분 수정 필요

		// sodeone
		try {
			HttpPost post = new HttpPost(svcBaseUrl + "/restapi/svc/cust/imnUseLoan.php");
			post.setEntity(getStringEntity(map));
			List<String> sodeoneResult = callSmpApi(post, List.class);

			for (String data : sodeoneResult) {
				if (cust_id.equals(data)) {
					result = "Y";
					resultMap.put("cust_typ", result);
					return resultMap;
				}
			}

		} catch (Exception e) {
			log.info("sodeone 에러!!");
			resultMap.put("loan_error_yn", "Y");
		}

		// sodetwo
		try {
			HttpPost postForSodetwo = new HttpPost(svcBaseUrlSodetwo + "/restapi/svc/cust/imnUseLoan.php");
			postForSodetwo.setEntity(getStringEntity(map));
			List<String> sodetwoResult = callSmpApi(postForSodetwo, List.class);

			for (String data : sodetwoResult) {
				if (cust_id.equals(data)) {
					result = "Y";
					resultMap.put("cust_typ", result);
					return resultMap;
				}
			}

		} catch (Exception e) {
			log.info("sodetwo 에러!!");
			resultMap.put("loan_error_yn", "Y");
		}

		return resultMap;

	}

	public boolean getCouponIssuanceChk(String custSeqNo) {
		String url = baseUrl + "/v1/cust/getCouponIssuanceChk/" + custSeqNo;
		HttpGet httpGet = new HttpGet(url);
		return callSmpApiIsOk(httpGet);
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> sodeCustTerminateReq(ExtCustDTO dto) throws URISyntaxException {
		// 소드원 회원 탈퇴 가능 여부 요청!
		Map<String, String> map = new HashMap<String, String>();
		Map<String, String> param = new HashMap<String, String>();
		try {
			log.info("sodeBaseUrl = {}", sodeBaseUrl);
			HttpDeleteWithBody deleteWithBody = new HttpDeleteWithBody(sodeBaseUrl + "/api/v1/member/info");
			param.put("cust_seq_no", String.valueOf(dto.getCust_seq_no()));
			param.put("biz_no", dto.getBiz_no());

			deleteWithBody.setEntity(getStringEntity(param));
			deleteWithBody.addHeader("Accept", "*/*");
			deleteWithBody.addHeader("Authorization",
					"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzZWxsZXJib3QuY28ua3IiLCJleHAiOjIwMzMxMTEwMTQxOTU5NTAwfQ==.L0iw6DkdIl_Ffb-Mj4-fiyUG12aEtnRFdpv1Rvz2mMI=");

			map = (Map<String, String>) callSmpApi(deleteWithBody, Map.class);

			return map;

		} catch (RestApiException e) {
			throw e;
		} catch (Exception e) {
			log.error("Error", e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getCustSodeTerminateChk(String userName) {
		Map<String, Object> map = new HashMap<String, Object>();
		String url = baseUrl + "/v1/cust/svc/sodeTerminateChk/" + userName;
		HttpGet httpGet = new HttpGet(url);
		map = (Map<String, Object>) callSmpApi(httpGet, Map.class);
		return map;
	}

	public boolean insertTsCustTerminateProStat(String userName) {
		String url = baseUrl + "/v1/cust/sode/terminateReq/save/" + userName;
		HttpGet httpGet = new HttpGet(url);
		return callSmpApiIsOk(httpGet);
	}
}
