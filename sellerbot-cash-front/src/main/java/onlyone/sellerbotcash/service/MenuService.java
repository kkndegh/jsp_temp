package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.service.dto.ExtMenuDTO;

/**
 * 메뉴 Service
 * @author YunaJang
 */
@Slf4j
@Service
public class MenuService extends AbstractSmpService {
    /**
     * 8.3.1	메뉴 목록 조회
     * @return
     */
    public List<ExtMenuDTO> getMenuListForSideBar(String custId) {
        URIBuilder builder;
        List<ExtMenuDTO> menuList = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/comm/menu");
            builder.addParameter("cust_id", custId);

            HttpGet httpGet = new HttpGet(builder.build());
            menuList = new ArrayList<ExtMenuDTO>();
            menuList.addAll(Arrays.asList(callSmpApi(httpGet, ExtMenuDTO[].class)));

        } catch (URISyntaxException e) {
            log.warn("{}", e);
            throw new RuntimeException();
        }

        return menuList;
    }

    /**
     * 8.3.2	메뉴별 권한 조회
     * @return
     */
    public List<ExtMenuDTO> getMenuListForViewAccess(String custId) {
        URIBuilder builder;
        List<ExtMenuDTO> menuList = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/comm/menuForAccess");
            builder.addParameter("cust_id", custId);

            HttpGet httpGet = new HttpGet(builder.build());
            menuList = Arrays.asList(callSmpApi(httpGet, ExtMenuDTO[].class));

        } catch (URISyntaxException e) {
            log.warn("{}", e);
            throw new RuntimeException();
        }

        return menuList;
    }
}