package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.service.dto.AcctTraContDepoHisDTO;
import onlyone.smp.service.dto.ShopByDepOrdRecoDTO;
import onlyone.smp.service.dto.ShopByOrdSearchDTO;
import onlyone.smp.service.dto.ShopByStoreDTO;
import onlyone.smp.service.dto.ShopByTokenDTO;
import onlyone.smp.service.shopby.dto.req.AuthMeReqDTO;
import onlyone.smp.service.shopby.dto.req.LongLivedReqDTO;
import onlyone.smp.service.shopby.dto.req.MallsReqDTO;
import onlyone.smp.service.shopby.dto.req.OrdersReqDTO;
import onlyone.smp.service.shopby.dto.res.AuthMeResDTO;
import onlyone.smp.service.shopby.dto.res.LongLivedResDTO;
import onlyone.smp.service.shopby.dto.res.MallsResDTO;
import onlyone.smp.service.shopby.dto.res.OrdersResDTO;

@Slf4j
@Service
public class ShopByService extends AbstractSmpService {

    //장기 토큰 발급
    public LongLivedResDTO longLived(LongLivedReqDTO longLivedReqDTO) {
        LongLivedResDTO result = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/shby/auth/token/long-lived");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(longLivedReqDTO));
            result = callSmpApi(httpPost, LongLivedResDTO.class);

        } catch (Exception e) {
            log.error("" + e);
        }
        return result;
    }

    //어드민/몰 정보 조회
    public AuthMeResDTO authMe(AuthMeReqDTO authMeReqDTO) {
        AuthMeResDTO result = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/shby/auth/me");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(authMeReqDTO));
            result = callSmpApi(httpPost, AuthMeResDTO.class);

        } catch (Exception e) {
            log.error("" + e);
        }
        return result;
    }

    //쇼핑몰 상세 조회
    public MallsResDTO malls(MallsReqDTO mallsReqDTO) {
        MallsResDTO result = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/shby/malls");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(mallsReqDTO));
            result = callSmpApi(httpPost, MallsResDTO.class);

        } catch (Exception e) {
            log.error("" + e);
        }
        return result;
    }

    //무통장 미입금 주문 리스트 조회
    public OrdersResDTO orders(OrdersReqDTO ordersReqDTO) {
        OrdersResDTO result = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/shby/accounts/orders");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(ordersReqDTO));
            result = callSmpApi(httpPost, OrdersResDTO.class);

        } catch (Exception e) {
            log.error("" + e);
        }
        return result;
    }

    //토큰 저장 / 갱신
    public boolean updateToken(ShopByTokenDTO shopByTokenDTO) {
        HttpResponse response = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/shby/updateToken");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(shopByTokenDTO));
            response = callSmpApi(httpPost);
        } catch (Exception e) {
            log.error("" + e);
        }
        return response != null ? HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK : false;
    }



    
    public ShopByStoreDTO getStoreInfo(String mallId) {
        URIBuilder builder;
        ShopByStoreDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/shby/getStoreInfo/" + mallId);

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ShopByStoreDTO.class);

        } catch (URISyntaxException e) {
            log.warn("{}", e);
            throw new RuntimeException();
        }

        return result;        
    }

    /**
     * 주문 내역 조회
     * @param ShopByOrderHistoryDTO::req (조회 기간), String::custSeqNo (고객 일련 번호)
     * @return ShopByOrderHistoryDTO::res
     * 
     */
    public ShopByOrdSearchDTO getOrderHistory(long custSeqNo, String startDate, String endDate, String keyword, int page, int size) {
        ShopByOrdSearchDTO res = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/shby/getOrderHistory/" + custSeqNo);
            HttpPost httpPost = new HttpPost(builder.build());

            ShopByOrdSearchDTO req = new ShopByOrdSearchDTO();
            req.setKeyword(keyword);
            req.setStartDate(startDate);
            req.setEndDate(endDate);
            req.setPageable(PageRequest.of(page, size, Sort.by("reg_dt").descending()/*.and(Sort.by(""))*/));

            httpPost.setEntity(getStringEntity(req));
            res = callSmpApi(httpPost, ShopByOrdSearchDTO.class);
        } catch (Exception e) {
            log.error("" + e);
        }
        return res;
    }

    /**
     * 입금 내역 조회
     * @param AcctTraContDepoHisDTO::req (조회 기간), String::custSeqNo (고객 일련 번호)
     * @return AcctTraContDepoHisDTO::res
     * 
     */
    public AcctTraContDepoHisDTO getDepositHistory(long custSeqNo, String startDate, String endDate, String keyword, int page, int size) {
        AcctTraContDepoHisDTO res = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/shby/getDepositHistory/" + custSeqNo);
            HttpPost httpPost = new HttpPost(builder.build());

            AcctTraContDepoHisDTO req = new AcctTraContDepoHisDTO();
            req.setStartDate(startDate);
            req.setEndDate(endDate);
            req.setKeyword(keyword);

            httpPost.setEntity(getStringEntity(req));
            res = callSmpApi(httpPost, AcctTraContDepoHisDTO.class);
        } catch (Exception e) {
            log.error("" + e);
        }
        return res;
    }

    /**
     * 대사 갱신
     * @param AcctTraContDepoHisDTO::req (조회 기간), String::custSeqNo (고객 일련 번호)
     * @return AcctTraContDepoHisDTO::res
     * 
     */
    public boolean updateDepOrdReco(long acctTraContSeqNo, long ordSeqNo) {
        HttpResponse res = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/shby/updateDepOrdReco");
            HttpPost httpPost = new HttpPost(builder.build());
            ShopByDepOrdRecoDTO req = new ShopByDepOrdRecoDTO();
            req.setAcctTraContSeqNo(acctTraContSeqNo);
            req.setOrdSeqNo(ordSeqNo);
            httpPost.setEntity(getStringEntity(req));
            res = callSmpApi(httpPost);
        } catch (Exception e) {
            log.error("" + e);
        }
        return HttpStatus.resolve(res.getStatusLine().getStatusCode()) == HttpStatus.OK;
    }
}
