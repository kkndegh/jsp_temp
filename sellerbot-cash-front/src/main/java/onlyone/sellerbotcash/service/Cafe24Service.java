/**
 * @author kenny
 */
package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.service.dto.AcctTraContDepoHisDTO;
import onlyone.smp.service.dto.Cafe24DepOrdRecoDTO;
import onlyone.smp.service.dto.Cafe24OrdSearchDTO;
import onlyone.smp.service.dto.Cafe24StoreDTO;
import onlyone.smp.service.dto.Cafe24TokenDTO;

@Slf4j
@Service
public class Cafe24Service extends AbstractSmpService {

    public boolean updateToken(String appName, String mallId, String code) {
        HttpResponse response = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/cafe24/updateToken/true");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(new Cafe24TokenDTO(appName, mallId, code)));
            response = callSmpApi(httpPost);
        } catch (Exception e) {
            log.error("" + e);
        }
        return response != null ? HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK : false;
    }

    // public boolean updateToken(Cafe24TokenDTO dto) {
    //     HttpResponse response = null;
    //     try {
    //         URIBuilder builder = new URIBuilder(baseUrl + "/v1/cafe24/updateToken/false");
    //         HttpPost httpPost = new HttpPost(builder.build());
    //         httpPost.setEntity(getStringEntity(dto));
    //         response = callSmpApi(httpPost);
    //     } catch (Exception e) {
    //         log.error("" + e);
    //     }
    //     return response != null ? HttpStatus.resolve(response.getStatusLine().getStatusCode()) == HttpStatus.OK : false;
    // }

    public Cafe24StoreDTO getStoreInfo(String mallId) {
        URIBuilder builder;
        Cafe24StoreDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/cafe24/getStoreInfo/" + mallId);

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, Cafe24StoreDTO.class);

        } catch (URISyntaxException e) {
            log.warn("{}", e);
            throw new RuntimeException();
        }

        return result;        
    }

    /**
     * 주문 내역 조회
     * @param Cafe24OrderHistoryDTO::req (조회 기간), String::custSeqNo (고객 일련 번호)
     * @return Cafe24OrderHistoryDTO::res
     * 
     */
    public Cafe24OrdSearchDTO getOrderHistory(long custSeqNo, String startDate, String endDate, String keyword, int page, int size) {
        Cafe24OrdSearchDTO res = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/cafe24/getOrderHistory/" + custSeqNo);
            HttpPost httpPost = new HttpPost(builder.build());

            Cafe24OrdSearchDTO req = new Cafe24OrdSearchDTO();
            req.setKeyword(keyword);
            req.setStartDate(startDate);
            req.setEndDate(endDate);
            req.setPageable(PageRequest.of(page, size, Sort.by("reg_dt").descending()/*.and(Sort.by(""))*/));

            httpPost.setEntity(getStringEntity(req));
            res = callSmpApi(httpPost, Cafe24OrdSearchDTO.class);
        } catch (Exception e) {
            log.error("" + e);
        }
        return res;
    }

    /**
     * 입금 내역 조회
     * @param AcctTraContDepoHisDTO::req (조회 기간), String::custSeqNo (고객 일련 번호)
     * @return AcctTraContDepoHisDTO::res
     * 
     */
    public AcctTraContDepoHisDTO getDepositHistory(long custSeqNo, String startDate, String endDate, String keyword, int page, int size) {
        AcctTraContDepoHisDTO res = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/cafe24/getDepositHistory/" + custSeqNo);
            HttpPost httpPost = new HttpPost(builder.build());

            AcctTraContDepoHisDTO req = new AcctTraContDepoHisDTO();
            req.setStartDate(startDate);
            req.setEndDate(endDate);
            req.setKeyword(keyword);

            httpPost.setEntity(getStringEntity(req));
            res = callSmpApi(httpPost, AcctTraContDepoHisDTO.class);
        } catch (Exception e) {
            log.error("" + e);
        }
        return res;
    }

    /**
     * 대사 갱신
     * @param AcctTraContDepoHisDTO::req (조회 기간), String::custSeqNo (고객 일련 번호)
     * @return AcctTraContDepoHisDTO::res
     * 
     */
    public boolean updateDepOrdReco(long acctTraContSeqNo, long ordSeqNo) {
        HttpResponse res = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/cafe24/updateDepOrdReco");
            HttpPost httpPost = new HttpPost(builder.build());
            Cafe24DepOrdRecoDTO req = new Cafe24DepOrdRecoDTO();
            req.setAcctTraContSeqNo(acctTraContSeqNo);
            req.setOrdSeqNo(ordSeqNo);
            httpPost.setEntity(getStringEntity(req));
            res = callSmpApi(httpPost);
        } catch (Exception e) {
            log.error("" + e);
        }
        return HttpStatus.resolve(res.getStatusLine().getStatusCode()) == HttpStatus.OK;
    }
}
