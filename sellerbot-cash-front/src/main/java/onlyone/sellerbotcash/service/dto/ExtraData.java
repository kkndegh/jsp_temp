/**
 * @author kenny
 */
package onlyone.sellerbotcash.service.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExtraData {
    private String mode;
    private List<Goods> goodsList;
	private String freeTrialYn;
	private Long eventNo;
    private String buyerName;
    private String buyerTel;
    private String buyerEmail;

	@Getter @Setter @ToString
	public static class Goods {
		private Long r;		//goodsReqSeqNo
		private Long o;		//goodsOptSeqNo
		private Long s; 	//goodsSeqNo
		private Long p;		//price
		private String t;	//typeCd
		private String n;	//name
		private int d;		//duration
	}
}
