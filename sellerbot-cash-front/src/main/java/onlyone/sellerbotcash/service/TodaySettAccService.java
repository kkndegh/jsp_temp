package onlyone.sellerbotcash.service;

import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.service.dto.ExtTodaySettAccMonthlyInfoDTO;
import onlyone.smp.service.dto.ExtTodaySettAccMonthlyMallInfoDTO;

@Slf4j
@Service
public class TodaySettAccService extends AbstractSmpService {
	
	/**
	 * 4.11.2 월별 오늘정산금 합계
	 * @param username id
	 * @param date - yyyyMM
	 * @return
	 */
	
	public ExtTodaySettAccMonthlyInfoDTO getTodaySettAccMonthlyInfo(String custId, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/today_sett_acc/cust/" + SbfUtils.encodeUtf8(custId)+"/date/" + SbfUtils.encodeUtf8(date) );			
		try {
			return callSmpApi(get, ExtTodaySettAccMonthlyInfoDTO.class);
		} catch (Exception e) {
			log.error("{}", e);
             throw new RuntimeException(e.getMessage());
		}
    }
    
    /**
	 * 4.11.3 월별 오늘정산금 판매몰별
	 * @param username id
	 * @param date - yyyyMM
	 * @return
	 */
	
	public ExtTodaySettAccMonthlyMallInfoDTO getTodaySettAccMonthlyMallInfo(String custId, String date) {
		HttpGet get = new HttpGet(baseUrl + "/v1/today_sett_acc/cust/" + SbfUtils.encodeUtf8(custId)+"/date/" + SbfUtils.encodeUtf8(date) + "/mall");
		try {
			return callSmpApi(get, ExtTodaySettAccMonthlyMallInfoDTO.class);
		} catch (Exception e) {
			log.error("{}", e);
            throw new RuntimeException(e.getMessage());
		}
	}
}