package onlyone.sellerbotcash.service;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Objects;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtComSalesMallReqDTO;
import onlyone.smp.service.dto.ExtSalesMallGraphDTO;
import onlyone.smp.service.dto.ExtSalesMonthMarketDTO;
import onlyone.smp.service.dto.ExtSalesSummaryLastDTO;
import onlyone.smp.service.dto.ExtSalesYearMarketDTO;

/**
 * 매출용 서비스
 * @author bak
 *
 */
@Service
public class SmpSalesService extends AbstractSmpService{
	
	
	
	/**
	 * 4.5.1	매출 요약 – 최근매출 분석
	 * @param username id
	 * @return
	 */
	public ExtSalesSummaryLastDTO getSalesSummaryLast(String username) {
		HttpGet get = new HttpGet(baseUrl + "/v1/sales/cust/" + SbfUtils.encodeUtf8(username)+"/summary/last");			
		try {
			return callSmpApi(get, ExtSalesSummaryLastDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
     * 4.5.2 매출 요약 – 조회기간 분석
     * @param cust_id
     * @param sales_find_stt_dt
     * @param sales_find_end_dt
     * @return
     */
	public ExtSalesYearMarketDTO getYearSalesMarketInfo(String username, ExtComSalesMallReqDTO param ) {

		try {
			URIBuilder builder = new URIBuilder(baseUrl + "/v1/sales/cust/" + SbfUtils.encodeUtf8(username)+"/summary/period");
			
			builder.setParameter("sales_find_stt_dt", param.getSales_find_stt_dt());	
			builder.setParameter("sales_find_end_dt", param.getSales_find_end_dt());
			HttpGet get = new HttpGet(builder.build());
			
			return callSmpApi(get, ExtSalesYearMarketDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	
	
	/**
     * 4.5.3 매출 요약 – 조회기간 분석
     * @param cust_id
     * @param sales_find_stt_dt
     * @param sales_find_end_dt
     * @return
     */
	public ExtSalesMonthMarketDTO getMonthSalesMarketInfo(String username, ExtComSalesMallReqDTO param ) {

		try {
			URIBuilder builder = new URIBuilder(baseUrl + "/v1/sales/cust/" + SbfUtils.encodeUtf8(username));
			
			builder.setParameter("sales_find_stt_dt", param.getSales_find_stt_dt());	
			builder.setParameter("sales_find_end_dt", param.getSales_find_end_dt());
			if(Objects.nonNull(param.getMall_cd())) {
				builder.setParameter("mall_cd", param.getMall_cd());	
			}
			if(Objects.nonNull(param.getUnit())) {
				builder.setParameter("unit", param.getUnit().toString());
			}
			HttpGet get = new HttpGet(builder.build());
			
			return callSmpApi(get, ExtSalesMonthMarketDTO.class);
		} catch (Exception e) {
			return null;
		}
	}

	/**
     * 4.5.4	매출 분석 그래프
     * @param cust_id
     * @param sales_find_stt_dt
     * @param sales_find_end_dt
     * @return
     */
	public ExtSalesMallGraphDTO getMallSalesGraphList(String username, ExtComSalesMallReqDTO param ) {

		try {
			URIBuilder builder = new URIBuilder(baseUrl + "/v1/sales/cust/" + SbfUtils.encodeUtf8(username)+"/mall");
			
			builder.setParameter("sales_find_stt_dt", param.getSales_find_stt_dt());	
			builder.setParameter("sales_find_end_dt", param.getSales_find_end_dt());
			if(Objects.nonNull(param.getMall_cd())) {
				builder.setParameter("mall_cd", param.getMall_cd());	
			}
			HttpGet get = new HttpGet(builder.build());
			
			return callSmpApi(get, ExtSalesMallGraphDTO.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 일별 매출 비율
	 * @return
	 */
	// public Long calSalesRtoDay(String userName) {
		
	// 	Long result = null;
		
	// 	ExtComSalesMallReqDTO param = new ExtComSalesMallReqDTO();
	// 	LocalDateTime time = LocalDateTime.now();
	// 	param.setSales_find_stt_dt(DateUtil.getDateTimeString(time.minusMonths(11), DateUtil.FORMAT_MONTH_NO_HI));
	// 	param.setSales_find_end_dt(DateUtil.getDateTimeString(time, DateUtil.FORMAT_MONTH_NO_HI));
	// 	ExtSalesMonthMarketDTO cSales = getMonthSalesMarketInfo(userName, param);
		
		
	// 	int today = time.getDayOfMonth(); // 오늘 일자
	// 	int lastToday = time.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth(); // 전달 마지막 일자

	// 	Long sales_prc = cSales.getSales_month().get(11).getSales_prc(); // 해당 월 매출
	// 	Long lastSales_prc = cSales.getSales_month().get(10).getSales_prc(); // 전달 월 매출
		
	// 	if(Objects.nonNull(sales_prc) && Objects.nonNull(lastSales_prc) && sales_prc > 0L && lastSales_prc > 0L ) {
	// 		double lastDayAvg = lastSales_prc.doubleValue() / lastToday;
	// 		double dayAvg = sales_prc.doubleValue() / today;
	// 		Double temp = (dayAvg -lastDayAvg) / lastDayAvg * 100;
	// 		result = temp.longValue();	
	// 	}
		
	// 	return result;
	// }
	
	/**
	 * 전달 기준 매출 비율 취득
	 * @param sales_prc
	 * @param lastSales_prc
	 * @return
	 */
	public Long calLastSalesRtoDay(Long sales_prc, Long lastSales_prc) {

		Long result = null;
		
		LocalDateTime time = LocalDateTime.now();
		
		LocalDateTime monthDay = time.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
		LocalDateTime lastMonthDay = time.minusMonths(2).with(TemporalAdjusters.lastDayOfMonth());

		int today = monthDay.getDayOfMonth(); // 오늘 일자
		int lastToday = lastMonthDay.getDayOfMonth(); // 전달 마지막 일자
		
		if(Objects.nonNull(sales_prc) && Objects.nonNull(lastSales_prc) && sales_prc > 0L && lastSales_prc > 0L ) {
			double lastDayAvg = lastSales_prc.doubleValue() / lastToday;
			double dayAvg = sales_prc.doubleValue() / today;
			Double temp = (dayAvg -lastDayAvg) / lastDayAvg * 100;
			result = temp.longValue();
		}
		
		return result;
	}
	
	public String getMallSalseRecentDate(String custId, String custMallSeqNo) {
		try {
			URIBuilder builder = new URIBuilder(baseUrl + "/v1/salse/scarp/mall/" + SbfUtils.encodeUtf8(custId)+"/RecentDate");
			
			builder.setParameter("cust_mall_seq_no", custMallSeqNo);
			HttpGet get = new HttpGet(builder.build());
			
			return callSmpApi(get, String.class);
		} catch (Exception e) {
			return null;
		}
	}
}
