package onlyone.sellerbotcash.service;


import org.apache.http.client.methods.HttpPost;
import org.springframework.stereotype.Service;

// @Slf4j
@Service
public class ProductTerminationService extends AbstractSmpService{
	
	public boolean withdraw(String cust_id){
		HttpPost post = new HttpPost(baseUrl + "/v1/termination/withdraw/" + SbfUtils.encodeUtf8(cust_id) );			
		post.setEntity(getStringEntity(cust_id));
	    return callSmpApiIsOk(post);
	        
	}

	
}
