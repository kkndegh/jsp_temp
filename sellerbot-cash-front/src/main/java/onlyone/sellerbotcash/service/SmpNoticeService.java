package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtEventDTO;
import onlyone.smp.service.dto.ExtFaqDTO;
import onlyone.smp.service.dto.ExtNotiDTO;
import onlyone.smp.service.dto.ExtNotiForDashBoardDTO;

@Slf4j
@Service
public class SmpNoticeService extends AbstractSmpService {

	/**
	 * 공지 사항 목록 조회
	 * @param title
	 * @return
	 */
	public ListData<ExtNotiDTO> getNotiList(String title, int page) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/noti");
			if(StringUtils.isNotBlank(title)) {
				builder.setParameter("title", title);
			}
			builder.addParameter("post_yn", "Y");
			builder.addParameter("page",Integer.toString(page) );
			builder.addParameter("size", "10");
			builder.addParameter("sort", "notiMthCd,desc");
			builder.addParameter("sort", "notiSeqNo,desc");
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtNotiDTO> list = Arrays.asList(callSmpApi(get, ExtNotiDTO[].class, headers));
			log.debug("headers {0}", headers);
			return new ListData<ExtNotiDTO>(Integer.parseInt(headers.get(0).getValue()), page , list);
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getNoticeList", e);
		}
	}
	/**
	 * 공지 사항 목록 조회
	 * @param title
	 * @return
	 */
	public List<ExtNotiDTO> getNotiList() {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/noti");
			builder.addParameter("pop_noti_yn", "Y");
			builder.addParameter("sort", "notiMthCd,desc");
			builder.addParameter("sort", "notiSeqNo,desc");
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtNotiDTO> list = Arrays.asList(callSmpApi(get, ExtNotiDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getNoticeList", e);
		}
	}

	/**
	 * 공지 사항 목록 조회 - 메인화면
	 * @return
	 */
	public List<ExtNotiForDashBoardDTO> getNotiListForDashBoard() {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/noti/main");
			builder.addParameter("sort", "noti_mth_cd,desc");
			builder.addParameter("sort", "noti_seq_no,desc");
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtNotiForDashBoardDTO> list = Arrays.asList(callSmpApi(get, ExtNotiForDashBoardDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getNoticeList", e);
		}
	}

	/**
	 * 6.1.4	공지사항 조회 수 갱신
	 * @return
	 */
	public void updateViewCountForNoti(String seqNo) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/noti/viewCount");
			builder.addParameter("noti_seq_no", seqNo);

            HttpGet httpGet = new HttpGet(builder.build());
			callSmpApi(httpGet);
		} catch (URISyntaxException e) {
			throw new RuntimeException("{}", e);
		}
	}
	
	/**
	 * 이벤트  목록 조회
	 * @param title
	 * @return
	 */
	public ListData<ExtEventDTO> getEventList(String event_sts_cd, int page) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/event");

			builder.addParameter("page",Integer.toString(page) );
			builder.addParameter("size", "10");
			if( !event_sts_cd.equals("ALL") ) {
				builder.addParameter("event_sts_cd", event_sts_cd);	
			}
			builder.addParameter("sort", "eventStsCd,desc");
			builder.addParameter("sort", "eventSeqNo,desc");
			
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtEventDTO> list = Arrays.asList(callSmpApi(get, ExtEventDTO[].class, headers));
			return new ListData<ExtEventDTO>(Integer.parseInt(headers.get(0).getValue()), page , list);
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getEventList", e);
		}
	}
	
	/**
	 * 이벤트  목록 조회
	 * @param title
	 * @return
	 */
	public List<ExtEventDTO> getEventList() {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/event");
			builder.addParameter("event_pop_yn", "Y");
			
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>();
			List<ExtEventDTO> list = Arrays.asList(callSmpApi(get, ExtEventDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getEventList", e);
		}
	}

	public ExtEventDTO getEventDetail(int event_seq_no) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/event/"+event_seq_no);

			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			ExtEventDTO detail = callSmpApi(get, ExtEventDTO.class, headers);
			
			return detail;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getEventDetail", e);
		}
		
	}

	public ListData<ExtFaqDTO> getFaqList(String title, String faq_typ_cd,int page) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/faq");

			builder.addParameter("page",Integer.toString(page) );
			builder.addParameter("size", "10");
			builder.addParameter("size", "10");
			if(StringUtils.isNotBlank(title)) {
				builder.setParameter("title", title);
			}
			if(StringUtils.isNotBlank(faq_typ_cd)) {
				builder.setParameter("faq_typ_cd", faq_typ_cd);
			}
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtFaqDTO> list = Arrays.asList(callSmpApi(get, ExtFaqDTO[].class, headers));
			return new ListData<ExtFaqDTO>(Integer.parseInt(headers.get(0).getValue()), page , list);
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getEventList", e);
		}
	}

	public List<ExtCommCdDTO> getFaqCodeList() {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/comm/cd_grp/A039");

			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtCommCdDTO> list = Arrays.asList(callSmpApi(get, ExtCommCdDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getEventList", e);
		}
	}
	
}
