package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtCateCustReqDTO;
import onlyone.smp.service.dto.ExtCateCustResDTO;
import onlyone.smp.service.dto.ExtCateSalesRankAreaReqDTO;
import onlyone.smp.service.dto.ExtCateSalesRankAreaResDTO;
import onlyone.smp.service.dto.ExtCateSalesRankReqDTO;
import onlyone.smp.service.dto.ExtCateSalesRankResDTO;
import onlyone.smp.service.dto.ExtCustCateRankResDTO;
import onlyone.sellerbotcash.web.util.DateUtil;


/**
 * 4.10 동종업계 매출 추이
 * @author bak
 *
 */
@Service
public class SmpPeersaleService extends AbstractSmpService{
	
	/**
	 * 4.10.1 월별 카테고리별 매출 순위 목록 요청
	 * @return
	 */
	public List<ExtCateCustResDTO> getCateCustDateList(ExtCateCustReqDTO dto) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/cust/"+ SbfUtils.encodeUtf8(dto.getCust_id()) +"/cate/date");
			
			if(Objects.nonNull(dto.getYear_month())) {
				builder.addParameter("year_month", dto.getYear_month());	
			}
			
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtCateCustResDTO> list = Arrays.asList(callSmpApi(get, ExtCateCustResDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getCateCustDateList", e);
		}
	}
	
	
	
	/**
	 * 4.10.2 월별 카테고리별 매출 순위 목록 요청
	 * @return
	 */
	public List<ExtCateSalesRankResDTO> getCateSalesRankList(ExtCateSalesRankReqDTO dto) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/cust/cate/date/top");
			
			if(Objects.nonNull(dto.getCust_id())) {
				builder.addParameter("cust_id", dto.getCust_id());	
			}
			
			if(Objects.nonNull(dto.getSales_dt())) {
				builder.addParameter("sales_dt", dto.getSales_dt());	
			}
			
			if(Objects.nonNull(dto.getGoods_cate_seq_no()) && dto.getGoods_cate_seq_no().size() > 0) {
				List<String> strGoodsCateSeqNo = new ArrayList<String>();
				for(Long goods_cate_seq_no : dto.getGoods_cate_seq_no()) {
					strGoodsCateSeqNo.add(goods_cate_seq_no.toString());
				}
				builder.addParameter("goods_cate_seq_no", String.join(",", strGoodsCateSeqNo) );	
			}
			
			if(Objects.nonNull(dto.getTop_count())) {
				builder.addParameter("top_count", dto.getTop_count().toString());	
			}
			
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtCateSalesRankResDTO> list = Arrays.asList(callSmpApi(get, ExtCateSalesRankResDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getCateSalesRankList", e);
		}
	}
	
	
	/**
	 * 4.10.4 월별 카테고리 매출 분포 목록 요청
	 * @return
	 */
	public ExtCateSalesRankAreaResDTO getCategoryAreaInfo(ExtCateSalesRankAreaReqDTO dto) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/cust/cate/area");
			
			if( Objects.nonNull(dto.getGoods_cate_seq_no()) ) {
				builder.addParameter("goods_cate_seq_no", dto.getGoods_cate_seq_no().toString());
			}
			
			if( Objects.nonNull(dto.getSta_dt()) ) {
				builder.addParameter("sta_dt", dto.getSta_dt());
			}
			
			if( Objects.nonNull(dto.getEnd_dt()) ) {
				builder.addParameter("end_dt", dto.getEnd_dt());
			}
			
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			ExtCateSalesRankAreaResDTO res = callSmpApi(get, ExtCateSalesRankAreaResDTO.class, headers);
			return res;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getCateSalesAreaRankList", e);
		}
	}
	
	
	/**
	 * 4.10.5	고객매출 랭킹 변동 추이 조회
	 * @return
	 */
	public List<ExtCustCateRankResDTO> getCustCateRankList(String cust_id, ExtCateSalesRankAreaReqDTO dto) {
		URIBuilder builder;
		try {
			builder = new URIBuilder(baseUrl + "/v1/cust/"+SbfUtils.encodeUtf8(cust_id)+"/cate/rank");
			
			if( Objects.nonNull(dto.getGoods_cate_seq_no()) ) {
				builder.addParameter("goods_cate_seq_no", dto.getGoods_cate_seq_no().toString());
			}
			
			if( Objects.nonNull(dto.getSta_dt()) ) {
				builder.addParameter("sta_dt", dto.getSta_dt());
			}
			
			if( Objects.nonNull(dto.getEnd_dt()) ) {
				builder.addParameter("end_dt", dto.getEnd_dt());
			}
			
			HttpGet get = new HttpGet(builder.build() );
			ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtCustCateRankResDTO> list = Arrays.asList(callSmpApi(get, ExtCustCateRankResDTO[].class, headers));
			return list;
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("getCustCateRankList", e);
		}
	}
	
	/**
	 * 4.10.5	고객매출 랭킹 변동 추이 조회 화면 변환 데이터
	 * @return 행<열<상세정보>>
	 */
	public List<List<ExtCustCateRankResDTO>> getParseMyRankMonthList(String cust_id, ExtCateSalesRankAreaReqDTO dto) {
		

		List<List<ExtCustCateRankResDTO>> result = new ArrayList<List<ExtCustCateRankResDTO>>();
		
		// 4.10.5	고객매출 랭킹 변동 추이 조회
		List<ExtCustCateRankResDTO> custCateRankList = getCustCateRankList(cust_id, dto);
		List<String> monthList = DateUtil.getDateMonthList(LocalDateTime.now().minusMonths(12), 11, DateUtil.FORMAT_MONTH_NO_HI);
		
		List<Long> cateSeqNoList = new ArrayList<Long>();
		for(ExtCustCateRankResDTO extCustCateRankResDTO : custCateRankList) {
			cateSeqNoList.add(extCustCateRankResDTO.getGoods_cate_seq_no().longValue());
		}
		
		// 중복 제거
		TreeSet<Long> tempCateSeqNoList = new TreeSet<Long>(cateSeqNoList);
		cateSeqNoList = new ArrayList<Long>(tempCateSeqNoList);
		
		// 카테고리별 행 생성
		for(Long cateSeqNo : cateSeqNoList) {
			List<ExtCustCateRankResDTO> list = new ArrayList<ExtCustCateRankResDTO>();
			
			// 공통 정보 취득
			ExtCustCateRankResDTO rowInfoDTO = custCateRankList.stream().filter(x -> (
					cateSeqNo == x.getGoods_cate_seq_no().longValue())
			).findAny().orElse(new ExtCustCateRankResDTO());
			
			// 날짜별 열 생성
			for(String month : monthList) {
				
				ExtCustCateRankResDTO resDTO = custCateRankList.stream().filter(x -> (
						cateSeqNo == x.getGoods_cate_seq_no().longValue()
						&& month.equals(x.getYear_month())
				)).findAny().orElse(new ExtCustCateRankResDTO());
				
				// 매칭 정보가 없는 경우 대응
				resDTO.setGoods_cate_seq_no(rowInfoDTO.getGoods_cate_seq_no());
				resDTO.setCate_nm(rowInfoDTO.getCate_nm());
				resDTO.setYear_month(month);
				
				list.add(resDTO);
			}
			
			result.add(list);
		}
		
		return result;
	}
	
}
