package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtGoodsDTO;
import onlyone.smp.service.dto.ExtGoodsInfoDTO;
import onlyone.smp.service.dto.ExtJoinDTO;
import onlyone.smp.service.dto.ExtSelectGoodsDTO;

/**
 * 상품 Service
 * 
 * @author YunaJang
 */
@Service
public class GoodsService extends AbstractSmpService {
    /**
     * 9.1.1 상품 목록/상세
     * 
     * @param custId
     * @return
     */
    public List<ExtGoodsDTO> getGoodsList() {
        URIBuilder builder;
        List<ExtGoodsDTO> goodsList = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/default_goods");

            HttpGet httpGet = new HttpGet(builder.build());
            goodsList = Arrays.asList(callSmpApi(httpGet, ExtGoodsDTO[].class));

        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return goodsList;
    }

    /**
     * 상품목록 조회(사용상품그룹코드를 이용하여 제휴사 조회 후 해당되는 상품만 조회)
     * 
     * @since 2023.10.19
     * @param goodsDistCd
     * @return List<ExtGoodsDTO>
     */
    public List<ExtGoodsDTO> getGoodsList(String goodsDistCd, String custSeqNo) {
        URIBuilder builder;
        List<ExtGoodsDTO> goodsList = null;

        try {
            builder = new URIBuilder(
                    baseUrl + "/v1/get/goodsInfo?goodsDistCd=" + goodsDistCd + "&" + "custSeqNo=" + custSeqNo);

            HttpGet httpGet = new HttpGet(builder.build());
            goodsList = Arrays.asList(callSmpApi(httpGet, ExtGoodsDTO[].class));

        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return goodsList;
    }

    public List<ExtGoodsDTO> getGoodsList(Long custSeqNo) {

        URIBuilder builder;
        List<ExtGoodsDTO> goodsList = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/goods/cust/" + custSeqNo);

            HttpGet httpGet = new HttpGet(builder.build());
            goodsList = Arrays.asList(callSmpApi(httpGet, ExtGoodsDTO[].class));

        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return goodsList;
    }

    /**
     * 2.8.1 이용권 소개
     * 
     * @param custSeqNo
     * @return
     */
    public ExtGoodsInfoDTO getGoodsListForCust(Long custSeqNo) {
        URIBuilder builder;
        ExtGoodsInfoDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/goods/goodsInfo/" + custSeqNo);
            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtGoodsInfoDTO.class);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return result;
    }

    /**
     * 상품목록 조회(제휴 상품별 상품)
     * 
     * @param goodsDistCd
     * @param custSeqNo
     * @return ExtGoodsInfoDTO
     */
    public ExtGoodsInfoDTO getGoodsListForCust(String goodsDistCd, Long custSeqNo) {
        URIBuilder builder;
        ExtGoodsInfoDTO result = null;

        try {
            builder = new URIBuilder(
                    baseUrl + "/v1/get/goodsInfo/goods?goodsDistCd=" + goodsDistCd + "&" + "custSeqNo=" + custSeqNo);

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtGoodsInfoDTO.class);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return result;
    }

    public ExtJoinDTO getStepOneInfo() {
        URIBuilder builder;
        ExtJoinDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/join/stepOneInfo");
            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtJoinDTO.class);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return result;
    }

    public ExtSelectGoodsDTO getGoodsHis(String cust_id) {
        URIBuilder builder;
        ExtSelectGoodsDTO goodsHis = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/goodsHis/" + cust_id);
            HttpGet httpGet = new HttpGet(builder.build());
            goodsHis = callSmpApi(httpGet, ExtSelectGoodsDTO.class);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return goodsHis;
    }

    public ExtGoodsDTO getGoodsInfo(Long goodsSeqNo, Long goodsOptSeqNo) {
        URIBuilder builder;
        ExtGoodsDTO goodsInfo = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/goodsInfo/" + goodsSeqNo + "/" + goodsOptSeqNo);
            HttpGet httpGet = new HttpGet(builder.build());
            goodsInfo = callSmpApi(httpGet, ExtGoodsDTO.class);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return goodsInfo;
    }

    public boolean getUsingGoodsPromotionChk(Long custSeqNo) {
        String url = baseUrl + "/v1/goodsInfo/getUsingGoodsPromotionChk/" + custSeqNo;
        HttpGet httpGet = new HttpGet(url);
        return callSmpApiIsOk(httpGet);
    }

}