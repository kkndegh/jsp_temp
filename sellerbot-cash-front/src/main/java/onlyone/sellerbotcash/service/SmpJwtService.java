package onlyone.sellerbotcash.service;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
//import java.net.URLDecoder;
import java.util.HashMap;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SmpJwtService extends AbstractSmpService {

    @SuppressWarnings("unchecked")
	public ResponseEntity<HashMap<String, String>> tokenCheck(String token) throws UnsupportedEncodingException {		
        
        URIBuilder builder;
        HashMap<String, String> resCustId = null;
         
        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("token", token);
  
            builder = new URIBuilder(baseUrl + "/v1/master/token/check");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            resCustId = callSmpApi(httpPost, HashMap.class);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }

        return new ResponseEntity<HashMap<String, String>>(resCustId, HttpStatus.OK);
	}
}
