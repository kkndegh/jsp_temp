package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtCustJumpAddRequestDTO;
import onlyone.smp.service.dto.ExtCustJumpAddResponseDTO;
import onlyone.smp.service.dto.ExtCustJumpListDTO;
import onlyone.smp.service.dto.ExtJumpResponseDTO;

/**
 * 점프 서비스용 Service
 * @author YunaJang
 */
@Slf4j
@Service
public class JumpService extends AbstractSmpService {
    /**
     * 4.12.1	고객 점프 서비스 목록
     * @param custId
     * @return
     */
    public List<ExtCustJumpListDTO> getCustJumpList(String custId) {
        URIBuilder builder;
        List<ExtCustJumpListDTO> jumpList = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(custId) + "/jump");

            HttpGet httpGet = new HttpGet(builder.build());
            jumpList = Arrays.asList(callSmpApi(httpGet, ExtCustJumpListDTO[].class));

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException(e.getMessage());
        }

        return jumpList;
    }

    /**
     * 고객 점프 서비스 목록을 Map 형태로 변환
     * @param data
     * @return
     */
    public Map<String, List<ExtCustJumpListDTO>> getCustJumpMap(List<ExtCustJumpListDTO> data) {
        if (Objects.isNull(data) || data.size() == 0)
            return null;

        Map<String, List<ExtCustJumpListDTO>> result = new LinkedHashMap<String, List<ExtCustJumpListDTO>>();

        for (ExtCustJumpListDTO dto : data) {
            List<ExtCustJumpListDTO> jumpList = null;

            if (result.containsKey(dto.getJump_cate_cd()))
                jumpList = result.get(dto.getJump_cate_cd());
            else
                jumpList = new ArrayList<ExtCustJumpListDTO>();

            jumpList.add(dto);
            result.put(dto.getJump_cate_cd(), jumpList);
        }

        return result;
    }

    /**
     * 5.4.1	점프정보 목록
     * @return
     */
    public List<ExtJumpResponseDTO> getJumpList() {
        URIBuilder builder;
        List<ExtJumpResponseDTO> jumpList = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/jump");
            builder.addParameter("size", "1000");
            builder.addParameter("sort", "jump_cate_cd");
            builder.addParameter("sort", "ord");

            HttpGet httpGet = new HttpGet(builder.build());
            jumpList = Arrays.asList(callSmpApi(httpGet, ExtJumpResponseDTO[].class));

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException(e.getMessage());
        }

        return jumpList;
    }

    /**
     * 점프정보 목록을 Map 형태로 변환
     * @param data
     * @return
     */
    public Map<String, List<ExtJumpResponseDTO>> getJumpMap(List<ExtJumpResponseDTO> data) {
        if (Objects.isNull(data) || data.size() == 0)
            return null;

        Map<String, List<ExtJumpResponseDTO>> result = new LinkedHashMap<String, List<ExtJumpResponseDTO>>();

        for (ExtJumpResponseDTO dto : data) {
            List<ExtJumpResponseDTO> jumpList = null;

            if (result.containsKey(dto.getJump_cate_cd()))
                jumpList = result.get(dto.getJump_cate_cd());
            else
                jumpList = new ArrayList<ExtJumpResponseDTO>();

            jumpList.add(dto);
            result.put(dto.getJump_cate_cd(), jumpList);
        }

        return result;
    }

    /**
     * 4.12.2	고객 점프 서비스 추가
     * @param custId
     * @param jumpSeqNoList
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> addJumpList(String custId, String jumpSeqNoList) throws JsonProcessingException {
        URIBuilder builder;
        List<ExtCustJumpAddResponseDTO> addedList = null;
        List<ExtCustJumpAddRequestDTO> custJumpList = new ArrayList<ExtCustJumpAddRequestDTO>();

        for(String jumpSeqNo : jumpSeqNoList.split(",")) {
            ExtCustJumpAddRequestDTO extCustJumpAddRequestDTO = new ExtCustJumpAddRequestDTO();
            extCustJumpAddRequestDTO.setJump_seq_no(Long.parseLong(jumpSeqNo));
            extCustJumpAddRequestDTO.setAuto_set_yn(EYesOrNo.N);
            custJumpList.add(extCustJumpAddRequestDTO);
        }

        try {
            builder = new URIBuilder(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(custId) + "/jump");

            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(custJumpList));
            addedList = Arrays.asList(callSmpApi(httpPost, ExtCustJumpAddResponseDTO[].class));
			
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage());
        }

        if(Objects.isNull(addedList) || addedList.size() == 0)
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    /**
     * 4.12.3	고객 점프 서비스 삭제
     * @param custId
     * @param custJumpSeqNoList
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> deleteJumpList(String custId, String custJumpSeqNoList) throws JsonProcessingException {
        HttpDelete delete = new HttpDelete(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(custId) + "/jump?cust_jump_seq_no=" + custJumpSeqNoList);
		HttpResponse res = callSmpApi(delete);
		
        return getResponseEntity(res);
    }

    /**
     * 4.12.4	고객 점프 다운로드 이력 저장
     * @param custId
     * @param dwdl_nm
     * @param dwdl_ver
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> addCustJumpDownHistory(String custId, String dwdl_nm, String dwdl_ver) throws JsonProcessingException {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("cust_id", custId);
            param.put("dwdl_nm", dwdl_nm);
            param.put("dwdl_ver", dwdl_ver);
  
            builder = new URIBuilder(baseUrl + "/v1/cust/jump/down");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            res = callSmpApi(httpPost);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException(e.getMessage());
        }

        return getResponseEntity(res);
    }
}