package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Future;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.smp.service.dto.AcctTraContDepoHisDTO;
import onlyone.smp.service.dto.VerifyAcctDTO;
import onlyone.smp.service.dto.Cafe24OrdSearchDTO;
import onlyone.smp.service.dto.ExtAutoMatchRecoInfoDTO;
import onlyone.smp.service.dto.ExtCustAcctDTO;
import onlyone.smp.service.dto.ExtCustAcctDtlInfoDTO;
import onlyone.smp.service.dto.ExtCustAcctInfoDTO;
import onlyone.smp.service.dto.ExtCustAcctTratRequstDTO;
import onlyone.smp.service.dto.ExtTraContRecoListDTO;

/**
 * 정산계좌 Service
 * @author YunaJang
 */
@Slf4j
@Service
public class AccountService extends AbstractSmpService {
    /**
     * 2.3.1	정산계좌
     * @param custId
     * @return
     */
    public ExtCustAcctInfoDTO getCustAcctInfo(String custId, String custTyp) {
        ExtCustAcctInfoDTO res = null;

        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/mngAcct/acct/list");
            builder.addParameter("cust_id", custId);
            builder.addParameter("cust_typ", custTyp);

            HttpGet httpGet = new HttpGet(builder.build());
            res = callSmpApi(httpGet, ExtCustAcctInfoDTO.class);
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException(e.getMessage());
        }

        return res;
    }

    /**
     * 2.3.2	거래내역 조회
     * @param cust_id
     * @param cust_acct_seq_no
     * @param startDT
     * @param endDT
     * @param page
     * @param size
     * @return
     */
    public ExtCustAcctDtlInfoDTO getCustAcctDtlInfo(ExtCustAcctTratRequstDTO param, Integer page, Integer size) {
		try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/mngAcct/tra/list");
            builder.setParameter("cust_acct_seq_no", param.getCust_acct_seq_no().toString());
            builder.setParameter("startDT", param.getSta_dt());
            builder.setParameter("endDT", param.getEnd_dt());
			builder.setParameter("page", page.toString());
            builder.setParameter("size", size.toString());
            builder.setParameter("sort", "TRA_DT,DESC");
			
            HttpGet get = new HttpGet(builder.build());
			return callSmpApi(get, ExtCustAcctDtlInfoDTO.class);
		} catch (Exception e) {
			return null;
		}
    }

    /**
     * 2.3.2.1	거래내역 메모 등록
     * @param acct_tra_cont_seq_no
     * @param memo
     * @return
     */
    public ResponseEntity<String> regMemoForTraCont(Long acct_tra_cont_seq_no, String memo) {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("acct_tra_cont_seq_no", acct_tra_cont_seq_no.toString());
            param.put("memo", memo);
  
            builder = new URIBuilder(baseUrl + "/v1/mngAcct/tra/regMemo");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            res = callSmpApi(httpPost);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }

        return getResponseEntity(res);
    }

    /**
     * 2.3.2.2	정산 계좌 목록
     * @param cust_id
     * @param reco_yn
     * @return
     */
    public List<ExtCustAcctDTO> getCustAcctList(String cust_id, String reco_yn) {
        List<ExtCustAcctDTO> resultList = null;
		try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/mngAcct/acct/custAcctList");
            builder.setParameter("cust_id", cust_id);
            if(reco_yn != null && reco_yn != "")
                builder.setParameter("reco_yn", reco_yn);
			
            HttpGet httpGet = new HttpGet(builder.build());
            resultList = Arrays.asList(callSmpApi(httpGet, ExtCustAcctDTO[].class));

		} catch (Exception e) {
			return null;
        }
        
        return resultList;
    }

    /**
     * 2.3.3	대사서비스
     * @param cust_id
     * @param cust_acct_seq_no
     * @param acct_tra_cont
     * @param reco_sts_cd
     * @param startDT
     * @param endDT
     * @param page
     * @param size
     * @return
     */
    public ListData<ExtTraContRecoListDTO> getRecoList(String cust_id, Long cust_acct_seq_no, String acct_tra_cont, String reco_sts_cd, String startDT, String endDT, Integer page, Integer size) {
		try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/mngAcct/reco/list");
            builder.setParameter("cust_id", cust_id);
            builder.setParameter("cust_acct_seq_no", cust_acct_seq_no.toString());
            builder.setParameter("startDT", startDT);
            builder.setParameter("endDT", endDT);
			builder.setParameter("page", page.toString());
            builder.setParameter("size", size.toString());
            builder.setParameter("sort", "ACCT_TRA_CONT_SEQ_NO,DESC");

            if(acct_tra_cont != null && acct_tra_cont != "")
                builder.setParameter("acct_tra_cont", acct_tra_cont);

            if(reco_sts_cd != null && reco_sts_cd != "")
                builder.setParameter("reco_sts_cd_list_str", reco_sts_cd);
			
            HttpGet httpGet = new HttpGet(builder.build());
            ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtTraContRecoListDTO> list = Arrays.asList(callSmpApi(httpGet, ExtTraContRecoListDTO[].class, headers));
			return new ListData<ExtTraContRecoListDTO>(Integer.parseInt(headers.get(0).getValue()), page , list);
		} catch (Exception e) {
			return null;
        }
    }

    /**
     * 2.3.3.1	자동대사 매칭정보 조회
     * @param acct_tra_cont_seq_no
     * @return
     */
    public ExtAutoMatchRecoInfoDTO getAutoMatRecoInfo(Long acct_tra_cont_seq_no, Long ord_seq_no) {
		try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/mngAcct/reco/relInfo");
            builder.setParameter("acct_tra_cont_seq_no", acct_tra_cont_seq_no.toString());
            builder.setParameter("ord_seq_no", ord_seq_no.toString());
			
            HttpGet get = new HttpGet(builder.build());
			return callSmpApi(get, ExtAutoMatchRecoInfoDTO.class);
		} catch (Exception e) {
			return null;
		}
    }

    /**
     * 2.3.3.2	자동대사 변경
     * @param acct_tra_cont_seq_no
     * @param ord_seq_no
     * @return
     */
    public ResponseEntity<String> updateRecoSts(Long acct_tra_cont_seq_no) {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("acct_tra_cont_seq_no", acct_tra_cont_seq_no.toString());
  
            builder = new URIBuilder(baseUrl + "/v1/mngAcct/reco/chgRecoSts");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            res = callSmpApi(httpPost);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }

        return getResponseEntity(res);
    }

    /**
     * 2.3.3.3	대사제외
     * @param acct_tra_cont_seq_no
     * @param ord_seq_no
     * @return
     */
    public ResponseEntity<String> exclRecoSts(Long acct_tra_cont_seq_no) {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("acct_tra_cont_seq_no", acct_tra_cont_seq_no.toString());
  
            builder = new URIBuilder(baseUrl + "/v1/mngAcct/reco/exclReco");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            res = callSmpApi(httpPost);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }

        return getResponseEntity(res);
    }

    /**
     * 거래내역 조회
     * @param cust_seq_no
     * @param acct_tra_cont_seq_no
     * @return
     */
    public AcctTraContDepoHisDTO getDepositHistory(Long cust_seq_no, String startDate, String endDate, 
    String keyword, String column, Long acct_tra_cont_seq_no, Integer pageNumber, Integer pageSize) {
        URIBuilder builder;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            if(Objects.nonNull(acct_tra_cont_seq_no)) {
                param.put("acctTraContSeqNo", acct_tra_cont_seq_no.toString());
            }
            else {
                param.put("startDate", startDate);
                param.put("endDate", endDate);
                param.put("pageNumber", pageNumber.toString());
                param.put("pageSize", pageSize.toString());

                if((Objects.nonNull(keyword) && !"".equals(keyword)) 
                && (Objects.nonNull(column) && !"".equals(column))) {
                    param.put("keyword", keyword);
                    param.put("column", column);
                }
            }
  
            builder = new URIBuilder(baseUrl + "/v1/cafe24/getDepositHistory/" + cust_seq_no);
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));
            return callSmpApi(httpPost, AcctTraContDepoHisDTO.class);
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }
    }

    /**
     * 주문내역 조회
     * @param cust_seq_no
     * @param startDate
     * @param endDate
     * @param keyword
     * @param column
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Cafe24OrdSearchDTO getOrderHistory(Long cust_seq_no, String startDate, String endDate, 
    String keyword, String column, Integer pageNumber, Integer pageSize) {
        URIBuilder builder;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("pageNumber", pageNumber.toString());
            param.put("pageSize", pageSize.toString());
            param.put("sort", "ord_id DESC");

            if((Objects.nonNull(startDate) && !"".equals(startDate))
            && (Objects.nonNull(endDate) && !"".equals(endDate))) {
                param.put("startDate", startDate);
                param.put("endDate", endDate);
            }

            if((Objects.nonNull(keyword) && !"".equals(keyword)) 
            && (Objects.nonNull(column) && !"".equals(column))) {
                param.put("keyword", keyword);
                param.put("column", column);
            }
  
            builder = new URIBuilder(baseUrl + "/v1/cafe24/getOrderHistory/" + cust_seq_no);
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));
            return callSmpApi(httpPost, Cafe24OrdSearchDTO.class);
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }
    }

    /**
     * 수동입금 확인
     * @param cust_seq_no
     * @param depList
     * @param ordList
     * @return
     */
    public ResponseEntity<Integer> matchingDepNOrdInfo(Long cust_seq_no, String depListStr, String ordListStr) {
        Integer result = null;

        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/cafe24/updateDepOrdReco/" + cust_seq_no);
            builder.setParameter("depListStr", depListStr);
            builder.setParameter("ordListStr", ordListStr);

            HttpPost httpPost = new HttpPost(builder.build());
            result = callSmpApi(httpPost, Integer.class);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }

        return new ResponseEntity<Integer>(result, HttpStatus.OK);
    }

    /**
     * 계좌테스트
     */
    @Async("taskExecutor")
    public Future<VerifyAcctDTO> verifyAccount(VerifyAcctDTO dto) {
        VerifyAcctDTO rdto = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/mngAcct/acct/verify");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(dto));
            rdto = callSmpApi(httpPost, VerifyAcctDTO.class);
		} catch (URISyntaxException e) {
            log.error("*** {}", e);
			throw new RuntimeException();
        }
        return new AsyncResult<VerifyAcctDTO>(rdto);
    }

    /**
     * 계좌 중복 체크
     * @param acct_no
     * @return
     */
    public ResponseEntity<Integer> countByCustNo(String acct_no) {
        Integer result = null;
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/mngAcct/acct/checkDupl");
            builder.setParameter("acct_no", acct_no);

            HttpPost httpPost = new HttpPost(builder.build());
            result = callSmpApi(httpPost, Integer.class);

        }catch(URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }
        return new ResponseEntity<Integer>(result, HttpStatus.OK);
    }

}
