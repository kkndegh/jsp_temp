/**
 * kenny rewrite
 */
package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.web.error.RestApiException;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.smp.service.dto.ExtMaxRegCountResDTO;
import onlyone.smp.service.dto.ExtPaidInfoDTO;
import onlyone.smp.service.dto.ExtPayAppRrDTO;
import onlyone.smp.service.dto.ExtPayCancelReqDTO;
import onlyone.smp.service.dto.ExtPayInicisReqDTO;
import onlyone.smp.service.dto.ExtPayInicisRespDTO;
import onlyone.smp.service.dto.ExtPayStatusDTO;

/**
 * 결제 관련 Service
 * 
 * @author YunaJang
 */
@Slf4j
@Service
public class PaymentService extends AbstractSmpService {
    /**
     * 9.3.1 결제 승인 요청/결과 생성
     * 
     * @param custId
     * @return
     * @throws JsonProcessingException
     */
    public ExtPayInicisRespDTO postPaymentByInicis(ExtPayInicisReqDTO dto) {
        URIBuilder builder;
        ExtPayInicisRespDTO res = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/pay/inicis");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(dto));

            res = callSmpApi(httpPost, ExtPayInicisRespDTO.class);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return res;
    }

    /**
     * 9.3.2 이용 정보 요약 조회
     * 
     * @param custId
     * @return
     */
    public ExtPayStatusDTO getPaymentStatus(String custId) {
        URIBuilder builder;
        ExtPayStatusDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/pay/status");
            builder.addParameter("cust_id", custId);

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtPayStatusDTO.class);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return result;
    }

    /**
     * 9.3.3 결제 내역 조회
     * 
     * @param custId
     * @return
     */
    public ListData<ExtPayAppRrDTO> getPaymentHistory(String custId, String start_dt, String end_dt, int page,
            int size) {
        try {
            URIBuilder builder;
            builder = new URIBuilder(baseUrl + "/v1/pay/hist");
            builder.addParameter("cust_id", custId);
            builder.addParameter("start_dt", start_dt);
            builder.addParameter("end_dt", end_dt);
            builder.addParameter("page", Integer.toString(page));
            builder.addParameter("size", Integer.toString(size));
            builder.addParameter("sort", "applDate,DESC");
            builder.addParameter("sort", "applTime,DESC");

            HttpGet httpGet = new HttpGet(builder.build());
            ArrayList<Header> headers = new ArrayList<>();
            List<ExtPayAppRrDTO> list = Arrays.asList(callSmpApi(httpGet, ExtPayAppRrDTO[].class, headers));
            return new ListData<ExtPayAppRrDTO>(Integer.parseInt(headers.get(0).getValue()), page, list);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }
    }

    /**
     * 9.3.4 이용권 정보 조회
     * 
     * @param custId
     * @return
     */
    public ExtPaidInfoDTO getPaidInfo(String custId) {
        URIBuilder builder;
        ExtPaidInfoDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/pay/paidInfo");
            builder.addParameter("cust_id", custId);

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtPaidInfoDTO.class);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }

    /**
     * 9.3.5 이용권 해지
     * 
     * @param params
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> postPaymentCancelForInicis(ExtPayCancelReqDTO params) throws JsonProcessingException {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/pay/cancel");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(params));

            res = callSmpApi(httpPost);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }
        return getResponseEntity(res);
    }

    /**
     * 9.3.6 이용권 등록 가능 수량 조회
     * 
     * @param custId
     * @return
     */
    public ExtMaxRegCountResDTO getMaxRegCountInfo(String custId) {
        URIBuilder builder;
        ExtMaxRegCountResDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/pay/maxRegCnt");
            builder.addParameter("cust_id", custId);

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtMaxRegCountResDTO.class);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return result;
    }

    /**
     * 9.3.7 이용권 변경
     * 
     * @param params
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> postChangeTicket(String custId, String goods_req_seq_no, String new_goods_opt_seq_no,
            boolean refund) throws JsonProcessingException {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("cust_id", custId);
            param.put("goods_req_seq_no", goods_req_seq_no);
            param.put("new_goods_opt_seq_no", new_goods_opt_seq_no);

            builder = new URIBuilder(baseUrl + (refund ? "/v1/pay/changeProduct/refund" : "/v1/pay/changeProduct"));
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            res = callSmpApi(httpPost);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }

        return getResponseEntity(res);
    }

    /**
     * 9.3.8 결제수단 변경
     * 
     * @param params
     * @return
     * @throws Exception
     */
    public void postChangeCard(ExtPayInicisReqDTO dto) throws Exception {
        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/pay/changeMethod");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(dto));
            callSmpApi(httpPost);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public ResponseEntity<String> paymentPromotion(String custId, String goodsTyp, String sttDt, String endDt,
            String promotionCd) {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            HashMap<String, String> param = new HashMap<String, String>();

            param.put("goods_typ", goodsTyp);
            param.put("stt_dt", sttDt);
            param.put("end_dt", endDt);
            param.put("promotion_cd", promotionCd);

            builder = new URIBuilder(baseUrl + "/v1/pay/promotion/" + custId + "/product");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            res = callSmpApi(httpPost);

            if( HttpStatus.resolve( res.getStatusLine().getStatusCode()) == HttpStatus.OK) 
                return new ResponseEntity<String>(HttpStatus.OK);

            HttpEntity resEntity = res.getEntity();
            String code = "";
            try {
                TreeMap<?,?>retval = objectMapper.readValue(EntityUtils.toString(resEntity), TreeMap.class);
                code = retval.get("code").toString();
            } catch(Exception e) {
                log.error("json Error",e);
                throw new RestApiException(HttpStatus.INTERNAL_SERVER_ERROR, "네트웍 통신 오류 입니다.");
            }
            throw new RestApiException(HttpStatus.CONFLICT, code);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 발행 쿠폰 상태코드 해지
     * 
     * @param dto
     */
	public void postCouponStateExpire(ExtPayInicisReqDTO dto) {
        URIBuilder builder;

        try {
            builder = new URIBuilder(baseUrl + "/v1/pay/couponStateExpire");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(dto));

            callSmpApi(httpPost);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage());
        }
	}
}