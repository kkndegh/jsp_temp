package onlyone.sellerbotcash.service;

import java.util.HashMap;

import org.apache.http.client.methods.HttpPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import lombok.extern.slf4j.Slf4j;
import onlyone.smp.service.dto.ExtCustDTO;

//@Slf4j
@Service
public class SmpAllianceService extends AbstractSmpService {
	@Autowired
	SmpCustMallService smpCustMallService;
	
		
	/**
	 * 고객 추가
	 * @param cust
	 * @return
	 */
	public ExtCustDTO getAccessToken(ExtCustDTO cust, String psvcId, String psvcCertKey) {
		HttpPost post = new HttpPost(baseUrl + "/v1/alliance/getAccessToken");
		post.setEntity(getStringEntity(cust));
		return callSmpApiExternal(post, ExtCustDTO.class, psvcId, psvcCertKey);		
	}
	public ExtCustDTO tokenCheck(String token, String psvcId, String psvcCertKey) {
	    token = (token.startsWith("Bearer ")) ? token.substring(7) : token;
		if(null == token) {
			return null;
		}else {
			HttpPost post = new HttpPost(baseUrl + "/v1/alliance/token/check");
			HashMap<String, String> param = new HashMap<String, String>();
			param.put("access_token", token);
			post.setEntity(getStringEntity(param));
			
			return callSmpApiExternal(post,ExtCustDTO.class, psvcId, psvcCertKey);	
		}
	}
	public ExtCustDTO reissueAccessToken(String token, String psvcId, String psvcCertKey) {
		if(null == token) {
			return null;
		}else {
			HttpPost post = new HttpPost(baseUrl + "/v1/alliance/reissue/accessToken");
			HashMap<String, String> param = new HashMap<String, String>();
			param.put("refresh_token", token);
			post.setEntity(getStringEntity(param));
			
			return callSmpApiExternal(post,ExtCustDTO.class, psvcId, psvcCertKey);	
		}
	}
	public ExtCustDTO reissueRefreshToken(String token, String psvcId, String psvcCertKey) {
		if(null == token) {
			return null;
		}else {
			HttpPost post = new HttpPost(baseUrl + "/v1/alliance/reissue/refreshToken");
			HashMap<String, String> param = new HashMap<String, String>();
			param.put("refresh_token", token);
			post.setEntity(getStringEntity(param));
			
			return callSmpApiExternal(post,ExtCustDTO.class, psvcId, psvcCertKey);	
		}
	}
	
}
