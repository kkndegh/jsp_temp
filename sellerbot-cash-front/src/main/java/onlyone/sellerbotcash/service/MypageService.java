package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.smp.service.dto.ExtPayHistListDTO;
import onlyone.smp.service.dto.ExtPayInfoDTO;
import onlyone.smp.service.dto.ExtPayInfoForCanDTO;
import onlyone.smp.service.dto.ExtUsingGoodsInfoDTO;

/**
 * 마이페이지 관련 Service
 * @author
 */
@Slf4j
@Service
public class MypageService extends AbstractSmpService {
    /**
     * 2.7.2	결제정보
     * @param custId
     * @return
     */
    public ExtPayInfoDTO getPayInfo(String custId) {
        ExtPayInfoDTO res = null;

        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/cust/payInfo");
            builder.addParameter("cust_id", custId);
            
            HttpGet httpGet = new HttpGet(builder.build());
            res = callSmpApi(httpGet, ExtPayInfoDTO.class);
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException(e.getMessage());
        }

        return res;
    }

    /**
     * 2.7.2.1	이용중인 서비스
     * @param custId
     * @return
     */
    public ExtUsingGoodsInfoDTO getPayInfoForUsingGoods(String custId) {
        ExtUsingGoodsInfoDTO res = null;

        try {
            URIBuilder builder = new URIBuilder(baseUrl + "/v1/cust/payInfo/usingGoods");
            builder.addParameter("cust_id", custId);
            HttpGet httpGet = new HttpGet(builder.build());
            
            res = callSmpApi(httpGet, ExtUsingGoodsInfoDTO.class);
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException(e.getMessage());
        }

        return res;
    }

    /**
     * 2.7.2.2	결제내역
     * @param custId
     * @param start_dt
     * @param end_dt
     * @param page
     * @param size
     * @return
     */
    public ListData<ExtPayHistListDTO> getPaymentHistory(String custId, String start_dt, String end_dt, int page, int size) {
        try {
            URIBuilder builder;
            builder = new URIBuilder(baseUrl + "/v1/cust/payHist");
            builder.addParameter("cust_id", custId);
            builder.addParameter("start_dt", start_dt);
            builder.addParameter("end_dt", end_dt);
            builder.addParameter("page", Integer.toString(page));
            builder.addParameter("size", Integer.toString(size));

            HttpGet httpGet = new HttpGet(builder.build());
            ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtPayHistListDTO> list = Arrays.asList(callSmpApi(httpGet, ExtPayHistListDTO[].class, headers));
			return new ListData<ExtPayHistListDTO>(Integer.parseInt(headers.get(0).getValue()), page , list);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }
    }

    /**
     * 2.7.2.4	해지정보
     * @param custId
     * @param goods_req_seq_no
     * @return
     */
    public ExtPayInfoForCanDTO getPayInfoForCan(String custId, Long goodsReqSeqNo) {
        // if (goodsReqSeqNos.size() < 1) return null;
        
        ExtPayInfoForCanDTO res = null;
        try {
            URIBuilder builder;
            builder = new URIBuilder(baseUrl + "/v1/cust/payInfoForCan");
            builder.addParameter("cust_id", custId);
            if (Objects.nonNull(goodsReqSeqNo))
                builder.addParameter("goodsReqSeqNo", String.valueOf(goodsReqSeqNo));
            // String seqs = "";
            // Iterator<Long> it = goodsReqSeqNos.iterator();
            // while (it.hasNext()) {
            //     seqs += it.next();
            //     if (it.hasNext()) seqs += ",";
            // }
            // builder.addParameter("seqs", seqs);

            HttpGet httpGet = new HttpGet(builder.build());
            res = callSmpApi(httpGet, ExtPayInfoForCanDTO.class);
        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return res;
    }
}