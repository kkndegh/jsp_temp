package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.smp.service.dto.ExtDashboardDTO;
import onlyone.smp.service.dto.ExtDashboardRecoDTO;

/**
 * 메인용 Service
 * @author YunaJang
 */
@Slf4j
@Service
public class MainService extends AbstractSmpService {
    /**
     * 메인화면용 데이터 조회
     * @param custId
     * @param custTyp
     * @return
     */
    public ExtDashboardDTO  getDashboardInfo(String custId, String custTyp, String loanSvcId) {
    	//custTyp : 기대출고객인지여부(Y/N)
        URIBuilder builder;
        ExtDashboardDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/dashboard");
            builder.addParameter("cust_id", custId);
            builder.addParameter("cust_typ", custTyp);
            builder.addParameter("loan_svc_id", loanSvcId);
            
            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtDashboardDTO.class);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }

    /**
     * 2.1.2	자동 대사 현황
     * @param custId
     * @return
     */
    public ExtDashboardRecoDTO getRecoInfo(String custId) {
        URIBuilder builder;
        ExtDashboardRecoDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/dashboard/traContSts");
            builder.addParameter("cust_id", custId);
            
            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtDashboardRecoDTO.class);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }
}