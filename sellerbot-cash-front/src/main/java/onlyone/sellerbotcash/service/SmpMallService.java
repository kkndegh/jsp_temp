package onlyone.sellerbotcash.service;

import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtMallDTO;

@Service
public class SmpMallService extends AbstractSmpService {
	// public List<ExtMallDTO> getMallList(){
	// 	HttpGet get = new HttpGet(baseUrl + "/v1/mall?size=100" );			
	// 	try {
	// 		return Arrays.asList(callSmpApi(get, ExtMallDTO[].class));
	// 	} catch (Exception e) {
	// 		return null;
	// 	}
	// }

	public ExtMallDTO getMallInfo(String mall_cd) {
		HttpGet get = new HttpGet(baseUrl + "/v1/mall/" + mall_cd);
		ExtMallDTO mallInfo = callSmpApi(get, ExtMallDTO.class);
		return mallInfo;
	}
}
