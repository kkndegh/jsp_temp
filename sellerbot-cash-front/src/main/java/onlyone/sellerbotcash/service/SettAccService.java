package onlyone.sellerbotcash.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import onlyone.sellerbotcash.web.util.DateUtil;
import onlyone.sellerbotcash.web.util.ExcelUtil;
import onlyone.smp.service.dto.ExtCustMallDTO;
import onlyone.smp.service.dto.ExtCustMallListDTO;
import onlyone.smp.service.dto.ExtSettAccScheDetailDTO;
import onlyone.smp.service.dto.ExtSettAccScheDlvDTO;
import onlyone.smp.service.dto.ExtSettAccScheFiveDTO;
import onlyone.smp.service.dto.ExtSettAccScheMallDayDTO;
import onlyone.smp.service.dto.ExtSettAccScheMallInfoDTO;
import onlyone.smp.service.dto.ExtSettAccScheOneDTO;
import onlyone.smp.service.dto.ExtSettAccScheThreeDTO;


/**
 * 정산예정금 서비스
 * @author bak
 *
 */
@Service
public class SettAccService {
	
	@Autowired
	private SmpSettAccScheService smpSettAccScheService;
	
	@Autowired
	private SmpCustMallService smpCustMallService;
	
	private final String[][] OPM_ROW_CODES =  {
			{"합계", "SUM"}, {"발송대상", "RDY"}, {"배송 중", "DOI"}, {"배송완료", "DON"}, {"구매확정", "CON"}
	};
	
	public void excelDownload(User user, HttpServletResponse res) {
		
		ExtSettAccScheDetailDTO ret = smpSettAccScheService.detail(user.getUsername(), DateUtil.getDateTimeNoFormatDay(LocalDate.now()));
		
		String filename = DateUtil.getDateTimeString(LocalDateTime.now(), DateUtil.NO_FORMAT_FULL) + "_정산예정금";
		String sheetname = "정산예정금";
		String title = user.getUsername()+"님의 정산예정금 현황";
		ExcelUtil excel = new ExcelUtil();
		int rowIndex = 2;
		int colIndex = 0;
		
		XSSFWorkbook wb = excel.createWorkBook();
		XSSFSheet sheet = excel.createSheet(wb, sheetname);
		
		Map<String, CellStyle> styles = excel.createStyles(wb);
		CellStyle tableTitleStyle = styles.get(excel.STYLE_TABLE_TITLE);
		CellStyle headerStyle = styles.get(excel.STYLE_HEADER);
		CellStyle moneyStyle = styles.get(excel.STYLE_MONEY);
		
		// 제목
		excel.setCellValue(sheet, rowIndex, colIndex, title, styles.get(excel.STYLE_TITLE));
		
		// 1. 판매몰 현황
		rowIndex = 6;
		excel.setCellValue(sheet, rowIndex, colIndex, "* 판매몰 현황", tableTitleStyle);
		ExtCustMallListDTO mallInfo = smpCustMallService.getNonDelCustMall(user.getUsername());
		List<ExtCustMallDTO> mallList = mallInfo.getMall();
		String lastUpdateDate = "";
		
		// 정산 예정금 정보
		ExtSettAccScheOneDTO settAccScheOne = ret.getSettAccScheOne();
		
		if(mallList.size() > 0) {
			mallList = mallList.stream().sorted(Comparator.comparing(ExtCustMallDTO::getMod_ts).reversed()).collect(Collectors.toList());
			lastUpdateDate = settAccScheOne.getScra_ts(); 
		}
		
		// 헤더 설정
		CellStyle cheaderStyle = excel.createStyle(wb, excel.STYLE_HEADER);
		excel.setCellValue(sheet, ++rowIndex, colIndex, "마지막 업데이트일자: "+ lastUpdateDate, cheaderStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, "마지막 업데이트일자: "+ lastUpdateDate, cheaderStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, "마지막 업데이트일자: "+ lastUpdateDate, cheaderStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, "마지막 업데이트일자: "+ lastUpdateDate, cheaderStyle);
		excel.setColMerged(sheet, rowIndex);
		
		colIndex = 0;
		excel.setCellValue(sheet, ++rowIndex, colIndex, "전체", headerStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, "정상", headerStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, "점검", headerStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, "오류", excel.createStyle(wb, excel.STYLE_HEADER_ERR));
		 
		// body 설정
		colIndex = 0;
		excel.setCellValue(sheet, ++rowIndex, colIndex, mallInfo.getAll_mall_cnt() - mallInfo.getDel_mall_cnt(), moneyStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, mallInfo.getNor_mall_cnt(), moneyStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, mallInfo.getIns_mall_cnt(), moneyStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, mallInfo.getErr_mall_cnt(), excel.createStyle(wb, excel.STYLE_MONEY_ERR));
		
		// 2.정산예정금 요약		
		rowIndex++;
		colIndex = 0;
		excel.setCellValue(sheet, ++rowIndex, colIndex, "* 정산예정금 요약", tableTitleStyle);
		// 헤더 설정
		excel.setCellValue(sheet, ++rowIndex, colIndex, "오픈마켓(배송상태 기준)", headerStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, "비오픈마켓 (정산일자 기준)", headerStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, "정산예정금 합계", headerStyle);
		// 바디 설정
		colIndex = 0;
		excel.setCellValue(sheet, ++rowIndex, colIndex, settAccScheOne.getSett_acc_pln_open_sum(), moneyStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, settAccScheOne.getSett_acc_pln_nopen_sum(), moneyStyle);
		excel.setCellValue(sheet, rowIndex, ++colIndex, settAccScheOne.getSett_acc_pln_sum(), moneyStyle);
		
		// 3.오픈마켓
		ExtSettAccScheThreeDTO opmDataList =  ret.getSettAccScheDlvMall();
		if(Objects.nonNull(opmDataList) && Objects.nonNull(opmDataList.getMall()) && opmDataList.getMall().size() > 0 ) {
			rowIndex++;
			colIndex = 0;
			List<ExtSettAccScheMallInfoDTO> sortOpmMallList = getSortOpmMallSumList(opmDataList.getMall());
			
			// 제목 설정
			excel.setCellValue(sheet, ++rowIndex, colIndex, "* 오픈마켓 정산예정금 요약", tableTitleStyle);
			String hearder = "";
			// 헤더 설정 2줄
			for(int i = 0; i < 2; i++) {
				excel.setCellValue(sheet, ++rowIndex, colIndex, "구분", headerStyle);
				excel.setCellValue(sheet, rowIndex, ++colIndex, "계", headerStyle);
				for(ExtSettAccScheMallInfoDTO scheMallInfo : sortOpmMallList) {
					hearder = i == 0 ? scheMallInfo.getMall_nm() : scheMallInfo.getMall_cert_1st_id();
					excel.setCellValue(sheet, rowIndex, ++colIndex, hearder, headerStyle);
				}
				colIndex = 0;
			}
			// 동일 몰이 있는 경우 셀 병합
			excel.setColMerged(sheet, rowIndex-1);
			sheet.addMergedRegion(new CellRangeAddress(rowIndex-1, rowIndex, 0, 0));
			sheet.addMergedRegion(new CellRangeAddress(rowIndex-1, rowIndex, 1, 1));
			
			CellStyle opmTextSyle = excel.createStyle(wb, excel.STYLE_BODY);
			CellStyle opmSumSyle = excel.createStyle(wb, excel.STYLE_MONEY);
			
			opmTextSyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
			opmTextSyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			
			opmSumSyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
			opmSumSyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			excel.addBorderStyle(opmSumSyle);
			
			// 바디 설정(5줄 고정)
			// 관리하는 항목이 공통코드와 내용이 다르기 때문에 수정 설정
			long code_sum = 0;
			for(String[] codeinfo : OPM_ROW_CODES) {
				code_sum = 0;
				excel.setCellValue(sheet, ++rowIndex, colIndex, codeinfo[0], opmTextSyle);
				++colIndex; // 계 위치
				for(ExtSettAccScheMallInfoDTO scheMallInfo : sortOpmMallList) {
					++colIndex;
					List<ExtSettAccScheDlvDTO> schelist =  scheMallInfo.getSett_acc_pln_open_dlv_l();
					if( codeinfo[1].equals(OPM_ROW_CODES[0][1]) ) {
						// 합계
						excel.setCellValue(sheet, rowIndex, colIndex, scheMallInfo.getMall_dvl_sett_acc_pln_sum(), moneyStyle);
						code_sum += scheMallInfo.getMall_dvl_sett_acc_pln_sum();
					} else {
						// 그외
						Long opmValue = getOpmValue(codeinfo[1], schelist);
						excel.setCellValue(sheet, rowIndex, colIndex, opmValue, moneyStyle);
						code_sum += opmValue;
					}
				}
				// 계 설정
				excel.setCellValue(sheet, rowIndex, 1, code_sum, opmSumSyle);
				colIndex = 0;
			}
		}
		
		// 4. 비오픈마켓 요약 
		ExtSettAccScheFiveDTO nOpmDataList = ret.getSettAccScheDayMall();
		if(Objects.nonNull(nOpmDataList)) { 
			rowIndex++; 
			colIndex = 0;
			
			List<ExtSettAccScheMallDayDTO> dayList = nOpmDataList.getSett_acc_pln_mall_day_l();
			//몰, 계정 별 합계 정보 (열)
			List<Map<String, Object>> mallSumList = getSortNopmMallSumList(dayList);
			List<Map<String, Object>> rowList = getNopmRowList(dayList);
			
			CellStyle textSyle = excel.createStyle(wb, excel.STYLE_BODY);
			CellStyle sumSyle = excel.createStyle(wb, excel.STYLE_MONEY);
			
			textSyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
			textSyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			
			sumSyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
			sumSyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			excel.addBorderStyle(sumSyle);
			
			// 테이블 제목
			excel.setCellValue(sheet, ++rowIndex, colIndex, "* 비오픈마켓 정산예정금 요약", tableTitleStyle);
			
			String hearder = "";
			
			// body 설정을 위한 계정 일련번호 목록
			List<Long> custMallSeqList = new ArrayList<Long>(); 
			// 테이블 헤더 
			for(int i = 0; i < 2; i++) {
				excel.setCellValue(sheet, ++rowIndex, colIndex, "구분", headerStyle);
				excel.setCellValue(sheet, rowIndex, ++colIndex, "계", headerStyle);
				for(Map<String, Object> mall : mallSumList) {
					@SuppressWarnings("unchecked")
					ArrayList<Map<String, Object>> custMallList = (ArrayList<Map<String, Object>>)mall.get("cust_mall");
					for(Map<String, Object> custMall : custMallList) {
						hearder = custMall.get("cust_mall_id").toString();
						if(i == 0) {
							hearder = mall.get("mall_nm").toString();
							custMallSeqList.add( (Long)custMall.get("cust_mall_seq_no") );	
						} 
						excel.setCellValue(sheet, rowIndex, ++colIndex, hearder, headerStyle);
					}
				}
				colIndex = 0;
			}
			
			// 헤더 병합
			excel.setColMerged(sheet, rowIndex-1);
			sheet.addMergedRegion(new CellRangeAddress(rowIndex-1, rowIndex, 0, 0));
			sheet.addMergedRegion(new CellRangeAddress(rowIndex-1, rowIndex, 1, 1));
			
			// body 설정
			// 1줄 합계 설정
			excel.setCellValue(sheet, ++rowIndex, colIndex, "합계", textSyle);
			Long cust_mall_total_sum = 0L;
			++colIndex;
			for(Map<String, Object> mall : mallSumList) {
				@SuppressWarnings("unchecked")
				ArrayList<Map<String, Object>> custMallList = (ArrayList<Map<String, Object>>)mall.get("cust_mall");
				for(Map<String, Object> custMall : custMallList) {
					cust_mall_total_sum += (Long)custMall.get("cust_mall_sum");
					excel.setCellValue(sheet, rowIndex, ++colIndex, (Long)custMall.get("cust_mall_sum"), moneyStyle);
				}
			}
			excel.setCellValue(sheet, rowIndex, 1, cust_mall_total_sum, sumSyle);
			colIndex = 0;
			
			// 일자별 행 설정 
			String strRowDate = null;
			Long strRowDateSum = 0L;
			for(Map<String, Object> row :  rowList) {
				strRowDate = row.get("date").toString();
				strRowDateSum = (Long)row.get("sum");
				excel.setCellValue(sheet, ++rowIndex, colIndex, strRowDate, textSyle);
				excel.setCellValue(sheet, rowIndex, ++colIndex, strRowDateSum, sumSyle);
				// 열
				for(Long custMallSeq : custMallSeqList) {
					++colIndex;
					excel.setCellValue(sheet, rowIndex, colIndex, 0, moneyStyle);
					long rowSum = 0l;
					for(ExtSettAccScheMallDayDTO dayInfo : dayList) {
						// 대상 정보 취득
						if( strRowDate.equals(dayInfo.getDay()) && custMallSeq == dayInfo.getCust_mall_seq_no() ) {
							rowSum += dayInfo.getDay_sett_acc_pln();
							excel.setCellValue(sheet, rowIndex, colIndex, rowSum, moneyStyle);
						}
					}
				}
				colIndex = 0;
			}
		}
		
		excel.setWidth(sheet, 0, 1, 6000);
		excel.setWidth(sheet, 2, excel.getLastCellNum(sheet), 4000);
		excel.download(res, wb, filename);
	}
	
	
	/**
	 * 오픈 마켓 [정렬 조건 Desc] 
	 * 1. 몰별 합계 금액, 
	 * 2. 동일몰의 다른 계정인경우 계정별 금액 
	 * @param opmMallList
	 * @return
	 */
	public List<ExtSettAccScheMallInfoDTO> getSortOpmMallSumList(List<ExtSettAccScheMallInfoDTO> opmMallList){
		List<ExtSettAccScheMallInfoDTO> result = new ArrayList<ExtSettAccScheMallInfoDTO>(); 
		List<ExtSettAccScheMallInfoDTO> mallList = opmMallList.stream().sorted(Comparator.comparing(ExtSettAccScheMallInfoDTO::getMall_cd)).collect(Collectors.toList());
		
		String tempMallCd = "";
		List<Map<String, Object>> mallSumList = new ArrayList<Map<String,Object>>();
		for(ExtSettAccScheMallInfoDTO mallInfo : mallList) {
			if(!mallInfo.getMall_cd().equals(tempMallCd)) {
				tempMallCd = mallInfo.getMall_cd();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("mall_cd", tempMallCd);
				map.put("mall_nm", mallInfo.getMall_nm());
				mallSumList.add(map);
			}
		}
		
		long mall_dvl_sett_acc_pln_sum = 0;
		
		for(Map<String, Object> mapSum : mallSumList) {
			mall_dvl_sett_acc_pln_sum = 0;
			for(ExtSettAccScheMallInfoDTO mallInfo : mallList) {
				if( Objects.toString(mapSum.get("mall_cd")).equals(mallInfo.getMall_cd()) ) {
					mall_dvl_sett_acc_pln_sum += mallInfo.getMall_dvl_sett_acc_pln_sum();  
				}
			}
			mapSum.put("mall_dvl_sett_acc_pln_sum", mall_dvl_sett_acc_pln_sum);
		}
		
		// 몰 순서 정렬
		Collections.sort(mallSumList, new Comparator<Map<String, Object>>() {
			@Override
		       public int compare(Map<String, Object> first, Map<String, Object> second) {
                return ((Long) second.get("mall_dvl_sett_acc_pln_sum")).compareTo((Long) first.get("mall_dvl_sett_acc_pln_sum"));
            }
		});
		
		// 금액 정렬
		opmMallList = opmMallList.stream().sorted(Comparator.comparing(ExtSettAccScheMallInfoDTO::getMall_dvl_sett_acc_pln_sum).reversed()).collect(Collectors.toList());
		
		for(Map<String, Object> mall : mallSumList) {
			for(ExtSettAccScheMallInfoDTO opmInfoDto : opmMallList ) {
				if( Objects.toString(mall.get("mall_cd")).equals(opmInfoDto.getMall_cd())) {
					result.add(opmInfoDto);
				}
			}
		}
		return result;
	}
	
	/**
	 * 비오픈 마켓 [정렬 조건 Desc] 
	 * 1. 몰별 합계 금액, 
	 * 2. 동일몰의 다른 계정인경우 계정별 금액 
	 * @param opmMallList
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Map<String, Object>> getSortNopmMallSumList(List<ExtSettAccScheMallDayDTO> nopmMallList){
		List<Map<String, Object>> mallInfoList = new ArrayList<Map<String, Object>>();
		
		// 계정별 정렬
		List<ExtSettAccScheMallDayDTO> mallList = nopmMallList.stream().sorted(Comparator.comparing(ExtSettAccScheMallDayDTO::getMall_cd).thenComparing(ExtSettAccScheMallDayDTO::getCust_mall_seq_no)).collect(Collectors.toList());
		
		for(ExtSettAccScheMallDayDTO dayDto : mallList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("mall_cd", dayDto.getMall_cd());
			map.put("mall_nm", dayDto.getMall_cd_nm());
			mallInfoList.add(map);
		}
		HashSet<Map<String, Object>> hsMallCds = new HashSet<>(mallInfoList);
		mallInfoList = new ArrayList<Map<String, Object>>(hsMallCds);
		
		List<Map<String, Object>> custMallCdList = new ArrayList<Map<String, Object>>();
		for(ExtSettAccScheMallDayDTO dayDto : mallList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("mall_cd", dayDto.getMall_cd());
			map.put("mall_nm", dayDto.getMall_cd_nm());
			map.put("cust_mall_seq_no", dayDto.getCust_mall_seq_no());
			map.put("cust_mall_id", dayDto.getMall_cert_1st_id());
			custMallCdList.add(map);
		}
		hsMallCds = new HashSet<>(custMallCdList);
		custMallCdList = new ArrayList<Map<String, Object>>(hsMallCds);
		
		long mall_sum = 0;
		long cust_mall_sum = 0;
		String mall_cd = null;
		Long cust_mall_seq_no;
		for(Map<String, Object> mMall : custMallCdList) {
			mall_sum = 0;
			cust_mall_sum = 0;
			mall_cd =  Objects.toString(mMall.get("mall_cd"));
			cust_mall_seq_no = (Long)mMall.get("cust_mall_seq_no");
			
			for(ExtSettAccScheMallDayDTO dayDto : mallList ) {
				if( mall_cd.equals(dayDto.getMall_cd()) ) {
					mall_sum += dayDto.getDay_sett_acc_pln();
				}
				if(cust_mall_seq_no == dayDto.getCust_mall_seq_no()) {
					cust_mall_sum += dayDto.getDay_sett_acc_pln();
				}
			}
			mMall.put("mall_sum", mall_sum);
			mMall.put("cust_mall_sum", cust_mall_sum);	
		}
		
		for(Map<String, Object> mallInfo : mallInfoList ) {
			for(Map<String, Object> mMall : custMallCdList) {
				if( Objects.toString(mallInfo.get("mall_cd")).equals(Objects.toString(mMall.get("mall_cd")))) {
					mallInfo.put("mall_sum", (Long)mMall.get("mall_sum"));
					List<Map> cust_mall = new ArrayList<Map>();
					if(Objects.nonNull(mallInfo.get("cust_mall"))) {
						cust_mall = (List<Map>) mallInfo.get("cust_mall");
					}
					
					Map custInfo = new HashMap<String, Object>();
					custInfo.put("cust_mall_seq_no", (Long)mMall.get("cust_mall_seq_no"));
					custInfo.put("cust_mall_sum", (Long)mMall.get("cust_mall_sum"));
					custInfo.put("cust_mall_id", mMall.get("cust_mall_id").toString());
					cust_mall.add(custInfo);
					mallInfo.put("cust_mall", cust_mall);
				}
			}	
		}
		
		// 몰 순서 정렬
		Collections.sort(mallInfoList, new Comparator<Map<String, Object>>() {
			@Override
		       public int compare(Map<String, Object> first, Map<String, Object> second) {
                return ((Long) second.get("mall_sum")).compareTo((Long) first.get("mall_sum"));
            }
		});
		
		// 계정 순서 정렬
		for(Map mallInfo : mallInfoList) {
			Collections.sort((List)mallInfo.get("cust_mall"), new Comparator<Map<String, Object>>() {
				@Override
			       public int compare(Map<String, Object> first, Map<String, Object> second) {
	                return ((Long) second.get("cust_mall_sum")).compareTo((Long) first.get("cust_mall_sum"));
	            }
			});
		}
			
		return mallInfoList;
	}
	
	/**
	 * 오픈 마켓 데이터 셋팅
	 * @param code
	 * @param dataList
	 * @return
	 */
	public Long getOpmValue(String code, List<ExtSettAccScheDlvDTO> dataList) {
		Long result = null;
		for(ExtSettAccScheDlvDTO data : dataList ) {
			if( code.equals(data.getDvl_sts_cd().name()) ) {
				result = data.getDvl_sett_acc_pln();
				break;
			}
		}
		result = Objects.isNull(result) ? 0L : result;
		return result;
	}
	
	/**
	 * 비오픈 마켓 행 (날짜, 합계)
	 * @param nopmMallList
	 * @return
	 */
	public List<Map<String, Object>> getNopmRowList(List<ExtSettAccScheMallDayDTO> nopmMallList) {
		// 날짜, 합계
		List<Map<String, Object>> rows = new ArrayList<Map<String,Object>>();
		for(ExtSettAccScheMallDayDTO dayDto : nopmMallList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("date", dayDto.getDay());
			rows.add(map);
		}
		HashSet<Map<String, Object>> hsRows = new HashSet<>(rows);
		rows = new ArrayList<Map<String, Object>>(hsRows);
		
		Collections.sort(rows, new Comparator<Map<String, Object>>() {
			@Override
		       public int compare(Map<String, Object> first, Map<String, Object> second) {
                return ((String) first.get("date")).compareTo((String) second.get("date"));
            }
		});
		
		Long rowSum = 0l;
		for(Map<String, Object> mRow : rows) {
			rowSum = 0l;
			String date = Objects.toString(mRow.get("date"));
			for(ExtSettAccScheMallDayDTO dayDto : nopmMallList) {
				if(date.equals(dayDto.getDay())) {
					rowSum += dayDto.getDay_sett_acc_pln();
				}
			}
			mRow.put("sum", rowSum);
		}
		
		return rows;
	}
}
