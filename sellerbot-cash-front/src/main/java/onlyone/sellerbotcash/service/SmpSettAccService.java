package onlyone.sellerbotcash.service;

import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Service;

import onlyone.smp.service.dto.ExtSettAccDetailInfoDTO;
import onlyone.smp.service.dto.ExtSettAccStatusDTO;
import onlyone.smp.service.dto.ExtSettAccSummaryLastDTO;
import onlyone.smp.service.dto.ExtSettAccSummaryPeriodDTO;

/**
 * 과거 정산금 통계 Service
 * @author YunaJang
 */
@Service
public class SmpSettAccService extends AbstractSmpService {

    /**
     * 4.6.1	정산금 요약 – 최근정산금 분석
     * @param custId
     * @return
     */
    public ExtSettAccSummaryLastDTO getSummaryLast(String custId) {
        HttpGet getSummaryLast = new HttpGet(baseUrl + "/v1/sett_acc/cust/" + SbfUtils.encodeUtf8(custId) + "/summary/last");
        ExtSettAccSummaryLastDTO summaryLastResult = callSmpApi(getSummaryLast, ExtSettAccSummaryLastDTO.class);
        return summaryLastResult;
    }

    /**
     * 4.6.2	정산금 요약 – 조회기간 분석
     * @param custId
     * @param startDate
     * @param endDate
     * @return
     */
    public ExtSettAccSummaryPeriodDTO getSummaryPeriod(String custId, String startDate, String endDate) {
        StringBuffer apiUrl = new StringBuffer();
        apiUrl.append(baseUrl).append("/v1/sett_acc/cust/").append(SbfUtils.encodeUtf8(custId)).append("/summary/period");
        apiUrl.append("?sales_find_stt_dt=").append(startDate).append("&sales_find_end_dt=").append(endDate);

        HttpGet getSummaryPeriod = new HttpGet(apiUrl.toString());
        ExtSettAccSummaryPeriodDTO summaryPeriodResult = callSmpApi(getSummaryPeriod, ExtSettAccSummaryPeriodDTO.class);
        return summaryPeriodResult;
    }

    /**
     * 4.6.3	정산금 현황
     * @param custId
     * @param startDate
     * @param endDate
     * @param mallCd
     * @param unit
     * @return
     */
    public ExtSettAccStatusDTO getMonthInfo(String custId, String startDate, String endDate, String mallCd, Integer unit) {
        StringBuffer apiUrl = new StringBuffer();
        apiUrl.append(baseUrl).append("/v1/sett_acc/cust/").append(SbfUtils.encodeUtf8(custId));
        apiUrl.append("?sales_find_stt_dt=").append(startDate).append("&sales_find_end_dt=").append(endDate);

        if(mallCd != null && !mallCd.equals(""))
            apiUrl.append("&mall_cd=").append(mallCd);

        if(unit != null && unit > 0)    
            apiUrl.append("&unit=").append(unit);    

        HttpGet getMonthInfo = new HttpGet(apiUrl.toString());
        ExtSettAccStatusDTO monthInfo = callSmpApi(getMonthInfo, ExtSettAccStatusDTO.class);
        return monthInfo;
    }

    /**
     * 4.6.4	정산금 판매몰별 상세 현황
     * @param custId
     * @param startDate
     * @param endDate
     * @param mallCd
     * @return
     */
    public ExtSettAccDetailInfoDTO getMonthDetailInfo(String custId, String startDate, String endDate, String mallCd) {
        StringBuffer apiUrl = new StringBuffer();
        apiUrl.append(baseUrl).append("/v1/sett_acc/cust/").append(SbfUtils.encodeUtf8(custId)).append("/detail");
        apiUrl.append("?sales_find_stt_dt=").append(startDate).append("&sales_find_end_dt=").append(endDate);
        apiUrl.append("&mall_cd=").append(mallCd);

        HttpGet getMonthInfo = new HttpGet(apiUrl.toString());
        ExtSettAccDetailInfoDTO monthInfo = callSmpApi(getMonthInfo, ExtSettAccDetailInfoDTO.class);
        return monthInfo;
    }
}