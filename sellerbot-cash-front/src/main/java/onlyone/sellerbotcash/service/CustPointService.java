package onlyone.sellerbotcash.service;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.smp.service.dto.ExtCustPointDTO;
import onlyone.smp.service.dto.ExtCustPointHistResDTO;
import onlyone.smp.service.dto.ExtCustPointHistResListDTO;
import onlyone.smp.service.dto.ExtCustUseReserveDTO;

/**
 * 포인트 Service
 * @author YunaJang
 */
@Slf4j
@Service
public class CustPointService extends AbstractSmpService {
    /**
     * 9.2.1	포인트 요약 조회
     * @param custId
     * @return
     */
    public ExtCustPointDTO getCustPointSummary(String custId) {
        URIBuilder builder;
        ExtCustPointDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/point/summary");

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtCustPointDTO.class);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }

    /**
     * 9.2.2	소멸 예정 포인트 목록 조회
     * @param custId
     * @param startExpireDt
     * @param endExpireDt
     * @return
     * 
     */
    public ExtCustPointHistResDTO getExpiryPointList(String custId, String start_expire_dt, String end_expirre_dt) {
        URIBuilder builder;
        ExtCustPointHistResDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/point/expiryWithinPeriod");
            builder.addParameter("cust_id", custId);
            builder.addParameter("start_expire_dt", start_expire_dt);
            builder.addParameter("end_expirre_dt", end_expirre_dt);

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtCustPointHistResDTO.class);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }

    /**
     * 9.2.3	포인트 사용 예약 조회
     * @param custId
     * @param reservStsCd
     * @return
     * 
     */
    public ExtCustUseReserveDTO getCustPointReserv(String custId) {
        URIBuilder builder;
        ExtCustUseReserveDTO result = null;

        try {
            builder = new URIBuilder(baseUrl + "/v1/point/reserv");
            builder.addParameter("cust_id", custId);

            HttpGet httpGet = new HttpGet(builder.build());
            result = callSmpApi(httpGet, ExtCustUseReserveDTO.class);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException();
        }

        return result;
    }

    /**
     * 9.2.4	포인트 사용 예약 생성
     * @param custId
     * @param reserve_poi
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> addCustPointReserv(String custId, String reserv_poi) throws JsonProcessingException {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            HashMap<String, String> param = new HashMap<String, String>();
            param.put("cust_id", custId);
            param.put("reserv_poi", reserv_poi);
  
            builder = new URIBuilder(baseUrl + "/v1/point/reserv");
            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(getStringEntity(param));

            res = callSmpApi(httpPost);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }

        return getResponseEntity(res);
    }

    /**
     * 9.2.5	포인트 사용 예약 취소
     * @param use_reserv_seq_no
     * @return
     * @throws JsonProcessingException
     */
    public ResponseEntity<String> cancelCustPointReserv(Long use_reserv_seq_no) throws JsonProcessingException {
        URIBuilder builder;
        HttpResponse res = null;

        try {
            ExtCustUseReserveDTO dto = new ExtCustUseReserveDTO();
            dto.setUSE_RESERV_SEQ_NO(use_reserv_seq_no);

            builder = new URIBuilder(baseUrl + "/v1/point/reserv");
            HttpPut httpPut = new HttpPut(builder.build());
            httpPut.setEntity(getStringEntity(dto));

            res = callSmpApi(httpPut);
			
		} catch (URISyntaxException e) {
            log.error("{}", e);
			throw new RuntimeException();
        }

        return getResponseEntity(res);
    }

    /**
     * 9.2.6	S포인트 내역 조회
     * @param custId
     * @param start_dt
     * @param end_dt
     * @param page
     * @param size
     * @return
     */ 
    public ListData<ExtCustPointHistResListDTO> getCustPointHistList(String custId, String start_dt, String end_dt, int page, int size) {
        try {
            URIBuilder builder;
            builder = new URIBuilder(baseUrl + "/v1/point/hist");
            builder.addParameter("cust_id", custId);
            builder.addParameter("start_dt", start_dt);
            builder.addParameter("end_dt", end_dt);
            builder.addParameter("page", Integer.toString(page));
            builder.addParameter("size", Integer.toString(size));
            builder.addParameter("sort", "OCCUR_TS,DESC");

            HttpGet httpGet = new HttpGet(builder.build());
            ArrayList<Header> headers = new ArrayList<>(); 
			List<ExtCustPointHistResListDTO> list = Arrays.asList(callSmpApi(httpGet, ExtCustPointHistResListDTO[].class, headers));
			return new ListData<ExtCustPointHistResListDTO>(Integer.parseInt(headers.get(0).getValue()), page , list);

        } catch (URISyntaxException e) {
            log.error("{}", e);
            throw new RuntimeException(e.getMessage());
        }
    }
}