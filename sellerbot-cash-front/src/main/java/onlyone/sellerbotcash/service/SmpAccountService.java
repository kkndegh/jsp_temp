package onlyone.sellerbotcash.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.web.util.ExcelUtil;
import onlyone.smp.service.dto.ExtCusAcctDTO;
import onlyone.smp.service.dto.ExtCusAcctDetailDTO;
import onlyone.smp.service.dto.ExtCusAcctEditDTO;
import onlyone.smp.service.dto.ExtCustAcctAepositRequstDTO;
import onlyone.smp.service.dto.ExtCustAcctAepositResponseDTO;
import onlyone.smp.service.dto.ExtCustAcctTratRequstDTO;
import onlyone.smp.service.dto.ExtCustAcctTratResponseDTO;
import onlyone.smp.service.dto.ExtCustAcctTratResultDTO;
import onlyone.sellerbotcash.web.util.DateUtil;


/**
 * 계좌 관리 서비스
 * @author bak
 *
 */
@Slf4j
@Service
public class SmpAccountService extends AbstractSmpService{
	
	@Autowired private SmpCustService smpCustService;
	
	/**
	 * 4.9.1 고객 계좌 목록
	 * @param username
	 * @return
	 */
	public ExtCusAcctDTO getCustAcctList(String username) {
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/acct" );			
		return callSmpApi(get, ExtCusAcctDTO.class);
	}
	
	/**
	 * 4.9.2	고객 계좌 등록
	 * @param username
	 * @return
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public ResponseEntity<String> addCustAcct(String username, List<ExtCusAcctEditDTO> dtoList) {
		HttpPost post = new HttpPost(baseUrl + "/v1/cust/"+username+"/acct");
		post.setEntity(getStringEntity(dtoList));
		HttpResponse res = callSmpApi(post);
		
		return getResponseEntity(res);
	}
	
	/**
	 * 4.9.3 고객 계좌 상세
	 * @param username
	 * @return
	 */
	public ExtCusAcctDetailDTO getCustAcctInfo(String username, Long cust_acct_seq_no) {
		HttpGet get = new HttpGet(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/acct/"+cust_acct_seq_no );			
		return callSmpApi(get, ExtCusAcctDetailDTO.class);
	}
	
	/**
	 * 4.9.4	고객 계좌 수정
	 * @param username
	 * @return 
	 * @return
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public ResponseEntity<String> editCustAcct(String username, Long cust_acct_seq_no, List<ExtCusAcctEditDTO> dtoList ) {
		
		if(dtoList.size() > 0) {
			for(ExtCusAcctEditDTO dto : dtoList) { dto.setCust_acct_seq_no(cust_acct_seq_no);}
			HttpPut put = new HttpPut(baseUrl + "/v1/cust/"+username+"/acct");
			put.setEntity(getStringEntity(dtoList));
			HttpResponse res = callSmpApi(put);
			
			return getResponseEntity(res);
		} else {
			// log.debug("수정 데이터가 없습니다.");
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		
		
	}
	
	/**
	 * 4.9.5	고객 계좌 삭제
	 * @param username
	 * @return
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public ResponseEntity<String> deleteCustAcct(String username, Long cust_acct_seq_no) {
		String delConfirm = smpCustService.getCustAcctDeleteConfirm(username);
		if( delConfirm.equals("N") ) {
			return ResponseEntity.badRequest().body("can_not_delete");
		}

		HttpDelete delete = new HttpDelete(baseUrl + "/v1/cust/"+username+"/acct/"+cust_acct_seq_no);
		HttpResponse res = callSmpApi(delete);
		
		return getResponseEntity(res);
	}
	
	/**
	 * 4.9.6	고객 계좌 거래내역 조회
	 * @param username
	 * @return
	 */
	public ExtCustAcctTratResultDTO getAcctDetailList(String username, ExtCustAcctTratRequstDTO param, int page, int size, String sort) {
		StringBuffer apiUrl = new StringBuffer();
		apiUrl.append(baseUrl).append("/v1/cust/").append(SbfUtils.encodeUtf8(username)).append("/acct/tra");
		apiUrl.append("?sta_dt=").append(param.getSta_dt()).append("&end_dt=").append(param.getEnd_dt());
		apiUrl.append("&page=").append(page).append("&size=").append(size).append("&sort=").append(sort);

		if(param.getCust_acct_seq_no() != null && param.getCust_acct_seq_no() > 0)
			apiUrl.append("&cust_acct_seq_no=").append(param.getCust_acct_seq_no());

		if(param.getFind_wd() != null && !param.getFind_wd().equals(""))
			apiUrl.append("&find_wd=").append(param.getFind_wd());	

		HttpGet get = new HttpGet(apiUrl.toString());
		return callSmpApi(get, ExtCustAcctTratResultDTO.class);
	}	
	
	/**
	 * 4.9.7	고객 계좌 판매몰 입금 현황 조회
	 * @param username
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ExtCustAcctAepositResponseDTO> getCustAcctList(String username, ExtCustAcctAepositRequstDTO param) {
		URIBuilder builder;
		
		try {
			builder = new URIBuilder(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/acct/stat");
			builder.addParameter("keywd_typ_cd", param.getKeywd_typ_cd().name() );
			builder.addParameter("sta_dt", param.getSta_dt() );
			builder.addParameter("end_dt", param.getEnd_dt() );
			HttpGet get = new HttpGet(builder.build() );
			return callSmpApi(get, List.class);
			
		} catch (URISyntaxException e) {
			log.error("{}", e);
			throw new RuntimeException(e.getMessage());
		}
		
	}
	
	/**
	 * 4.9.7	고객 계좌 판매몰 입금 현황 조회
	 * @param username
	 * @return
	 */
	public void reqCustAcctTra(String username) {
		
		HttpPost post = new HttpPost(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/acct/tra/req");
		try {
			callSmpApi(post);
		} catch (Exception e) {
			log.error(" + e");
			throw new RuntimeException();
		}
		
	}

	/**
	 * 4.9.8 고객 계좌 거래내역 조회 요청
	 * @param username
	 * @param custAcctSeqNo
	 */
	public void reqCustAcctTra(String username, String acct_no) {
		try {
			URIBuilder builder;
			builder = new URIBuilder(baseUrl + "/v1/cust/" + SbfUtils.encodeUtf8(username) + "/acct/tra/req");
			if(Objects.nonNull(acct_no))
				builder.addParameter("acct_no", acct_no);

			HttpPost post = new HttpPost(builder.build());
			callSmpApi(post);
		} catch (Exception e) {
			log.error(" + e");
			throw new RuntimeException();
		}
		
	}
	
	/**
	 * 정산계좌 상세조회 엑셀다운로드 처리
	 * @param dto
	 */
	public void accountExcelDownload(HttpServletResponse res, ExtCustAcctTratRequstDTO param, ExtCustAcctTratResultDTO dto) {
		String filename = DateUtil.getDateTimeString(LocalDateTime.now(), DateUtil.NO_FORMAT_FULL) + "_정산계좌상세";
		ExcelUtil excelUtil = new ExcelUtil();
		XSSFWorkbook wb = excelUtil.createWorkBook();
		XSSFSheet sheet = excelUtil.createSheet(wb, "정산계좌상세");
		Map<String, CellStyle> styles = excelUtil.createStyles(wb);
		final String[] columns = {"은행", "계좌번호", "이름", "거래일자", "거래내용", "거래형태", "거래은행지점", "입금액", "출금액", "통장잔액"};
		int rowIndex = 2;
		int colIndex = 0;
		
		CellStyle titleStyle = styles.get(excelUtil.STYLE_TITLE);
		CellStyle tableTitleStyle = styles.get(excelUtil.STYLE_TABLE_TITLE);
		CellStyle headerStyle = styles.get(excelUtil.STYLE_HEADER);
		CellStyle moneyStyle = styles.get(excelUtil.STYLE_MONEY);
		CellStyle bodyStyle = styles.get(excelUtil.STYLE_BODY);
		
		CellStyle cheaderStyle = excelUtil.createStyle(wb, excelUtil.STYLE_HEADER);
		
		
		excelUtil.setCellValue(sheet, rowIndex++, colIndex, "정산계좌 상세조회", titleStyle);
		excelUtil.setCellValue(sheet, ++rowIndex, colIndex, "* 정산계좌 계좌 합계", tableTitleStyle);
		
		excelUtil.setCellValue(sheet, ++rowIndex, 0, "입급 합계", cheaderStyle);
		excelUtil.setCellValue(sheet, rowIndex, 1, dto.getDepo_prc_total(), moneyStyle);
		
		excelUtil.setCellValue(sheet, ++rowIndex, 0, "출금 합계", cheaderStyle);
		excelUtil.setCellValue(sheet, rowIndex, 1, dto.getWdr_prc_total(), moneyStyle);
		
		rowIndex++;
		excelUtil.setCellValue(sheet, ++rowIndex, 0, "* 정산계좌 상세조회 내역", tableTitleStyle);
		excelUtil.setCellValue(sheet, ++rowIndex, 0, "기간", tableTitleStyle);
		
		excelUtil.setCellValue(sheet, rowIndex, 1, DateUtil.parseKrDateString(param.getSta_dt()) + " ~ " + DateUtil.parseKrDateString(param.getEnd_dt()), tableTitleStyle);

		rowIndex++;
		for(String column : columns) {
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, column, headerStyle);
		}
		rowIndex++;
		String traDate = "";
		for(ExtCustAcctTratResponseDTO traDto : dto.getAcct_tra()) {
			colIndex = 0;
			traDate = DateUtil.makeNonFormatToFormat(traDto.getTra_dt() + traDto.getTra_times());
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getBankNm(), bodyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getAcct_no(), bodyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getDpsiNm(), bodyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDate, bodyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getAcct_tra_cont(), bodyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getSumm(), bodyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getTra_branch_nm(), bodyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getDepo_prc(), moneyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getWdr_prc(), moneyStyle);
			excelUtil.setCellValue(sheet, rowIndex, colIndex++, traDto.getTra_remai(), moneyStyle);
			rowIndex++;
		}
		
		excelUtil.setWidth(sheet, 0, excelUtil.getLastCellNum(sheet), 5000);
		excelUtil.download(res, wb, filename);

	}
	
}
