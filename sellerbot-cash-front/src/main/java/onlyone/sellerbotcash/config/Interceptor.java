package onlyone.sellerbotcash.config;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import onlyone.sellerbotcash.service.SmpNoticeService;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtNotiForDashBoardDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class Interceptor implements HandlerInterceptor {
	
    @Value("${internal.redirect.base:}")
    private String redirectBase;

    @Autowired
    private SmpNoticeService smpNoticeService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        ExtCustDTO cust = (ExtCustDTO) request.getSession().getAttribute("cust");
        String requestURI = request.getRequestURI();
        
        // 2021.09.27 공지사항 모든 화면 나오게 처리. 인트로페이지 제외
        if (!StringUtils.equals("/", requestURI) && !StringUtils.equals("/pub/member_only1/step1", requestURI) && requestURI.indexOf("imagefile") == -1) {
            List<ExtNotiForDashBoardDTO> notiListData = smpNoticeService.getNotiListForDashBoard();
            request.getSession().setAttribute("notiListData", notiListData);
        }
        
        if (cust != null && (cust.getReg_mall_cnt_al() == 0 || cust.getCust_uses_goods_total_count() == 0)) {
            if (requestURI.indexOf("imagefile") > -1) {
                return true;
            } else if (requestURI.indexOf("extraction") > -1) {
                return true;
            } else if (requestURI.indexOf("/sub/my/join/") > -1){
                return true;
            } else if (requestURI.indexOf("/pub/member_only1") > -1) {
                return true;
            } else if (requestURI.indexOf("/sub/my/leave_frm") > -1) { // 회원 탈퇴 페이지
                return true;
            } else if (requestURI.indexOf("/sub/my/leave") > -1) { // 회원 탈퇴 로직
                return true;
            } else if (requestURI.indexOf("authPopup") > -1) { // 본인인증 제외
                return true;
            }
            
            try {
                if (cust.getReg_mall_cnt_al() == 0) {
                    response.sendRedirect(redirectBase + "/sub/member/step5");
                } else {
                    // 결제 정보가 없을 경우(뱅크봇 카운트 제외)
                    if (cust.getCust_uses_goods_total_count() == 0) {
                        // 2023.05.17 SMP 보안로그인으로 통해 접근 값 체크
                        String adminSkip = "";
                        if (request.getSession().getAttribute("adminSkip") != null) {
                            adminSkip = (String) request.getSession().getAttribute("adminSkip");
                        }
                        if ("Y".equals(adminSkip)) {
                            return true;
                        }

                        if (requestURI.indexOf("payment") > -1) { // 결제 관련 제외
                            return true;
                        } else {
                            String stepUrl = "/sub/member/step6";
                            
                            HttpSession session = request.getSession();
                            Object prevUrl = session.getAttribute("prevUrl");

                            if (Objects.nonNull(prevUrl)) {

                                if (prevUrl.toString().indexOf("?") > 0) {
                                    stepUrl = stepUrl + prevUrl.toString().substring(prevUrl.toString().indexOf("?"), prevUrl.toString().length());
                                }
                            }

                            response.sendRedirect(redirectBase + stepUrl);
                        }
                    }
                }
            } catch (IOException e) {
                log.error("" + e);
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }
}
