/**
 * @author: kenny
 */
package onlyone.sellerbotcash.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.config.ClasspathXmlConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import org.springframework.session.hazelcast.config.annotation.web.http.EnableHazelcastHttpSession;

@EnableHazelcastHttpSession 
@EnableCaching
@Configuration
public class SessionConfiguration {
	@Value("${spring.hazelcast.config}") private String hazelconfig;
    
	@Bean
	public HazelcastInstance hazelcastInstance() {
        Config config = new ClasspathXmlConfig(hazelconfig);

        // MapAttributeConfig attributeConfig = new MapAttributeConfig()
        //                                     .setName(HazelcastSessionRepository.PRINCIPAL_NAME_ATTRIBUTE)
        //                                     .setExtractor(PrincipalNameExtractor.class.getName());

        // config.getMapConfig(HazelcastSessionRepository.DEFAULT_SESSION_MAP_NAME)
        //     .addMapAttributeConfig(attributeConfig)
        //     .addMapIndexConfig(new MapIndexConfig(HazelcastSessionRepository.PRINCIPAL_NAME_ATTRIBUTE, false));
        
        config.setInstanceName("sellerbot-front");

		return Hazelcast.newHazelcastInstance(config); 
    }
    
    @Bean(name="cafe24-mall-id")
    public IMap<String, String> cafe24MallId(HazelcastInstance instaince){
    	return instaince.getMap("cafe24-mall-id");
    }

    @Bean(name="nhnc-mall-id")
    public IMap<String, String> nhncMallId(HazelcastInstance instaince){
    	return instaince.getMap("nhnc-mall-id");
    }
}
