/**
 * @author kenny
 */
package onlyone.sellerbotcash.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@ConfigurationProperties(ignoreUnknownFields = true)
@Getter
@Setter
public class ApplicationProperties {
    private Map<String, Cafe24Credential> cafe24;
    private Map<String, ShbyCredential> shby;

    @Getter
    @Setter
    public static class Cafe24Credential {
        private String redirectUri;
        private String clientId;
    }

    @Getter
    @Setter
    public static class ShbyCredential {
        private String redirectUri;
        private String clientId;
        private String clientSecret;
    }

    private Map<String, String> fromWhere;

    

}
