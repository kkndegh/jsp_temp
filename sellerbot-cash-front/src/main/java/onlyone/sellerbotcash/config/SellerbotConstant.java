package onlyone.sellerbotcash.config;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import com.inicis.std.util.SignatureUtil;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class SellerbotConstant {

	public final static String UTF8 = "UTF-8";
	public final static List<GrantedAuthority> GRANTEDAUTH = Arrays.asList(new SimpleGrantedAuthority("USER"));
	//SMP에 등록된 sellerbot용 service id 값
	public final static String SERVICE_ID = "selerbotcash";
	//가입에서 사용할 session 이름
	public static final String JOIN_SESSON_NAME = "sellerbot_join";
	public static final String INICIS_CHARSET = "UTF-8";
	public static final String INICIS_FORMAT = "JSON";
	public static String INICIS_MKEY = "";
	public static String SITE_DOMAIN = "";
    public static String INICIS_MID;
	public static String INICIS_SIGN_KEY;
	public static String INICIS_API_BASE_URL;
	public static String INICIS_API_KEY;
	public static String INILITE_KEY;
	// public static String CAFE24_MALLID_KEY = "CAFE24-MALLID";
	// public static String CAFE24_CODE_KEY = "CAFE24-CODE";

	@PostConstruct
	void init() {
		try {
			INICIS_MKEY = SignatureUtil.hash(INICIS_SIGN_KEY, "SHA-256");
		} catch (Exception e) {}
	}

	@Value("${inicis.mid}")
	public void setInicisMid(String inicisMid) {
		SellerbotConstant.INICIS_MID = inicisMid;
	}

	@Value("${inicis.sign-key}")
	public void setInicisSignKey(String inicisSignKey) {
		SellerbotConstant.INICIS_SIGN_KEY = inicisSignKey;
	}

	@Value("${inicis.api-base-url}")
	public void setINICIS_API_BASE_URL(String inicisApiBaseUrl) {
		SellerbotConstant.INICIS_API_BASE_URL = inicisApiBaseUrl;
	}

	@Value("${inicis.api-key}")
	public void setINICIS_API_KEY(String inicisApiKey) {
		SellerbotConstant.INICIS_API_KEY = inicisApiKey;
	}

	@Value("${inicis.inilite-key}")
	public void setINILITE_KEY(String iniLiteKey) {
		SellerbotConstant.INILITE_KEY = iniLiteKey;
	}

}
