package onlyone.sellerbotcash.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

import onlyone.sellerbotcash.web.util.DownloadView;

/**
 * Apache tiles Configuration for spring
 * 
 * https://tiles.apache.org/
 * 
 * @author oletree
 *
 */
@Configuration
public class TilesConfiguration {

	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setViewClass(JstlView.class);
		resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        //kenny: prevent from redirecting https to http
        resolver.setRedirectHttp10Compatible(false);
        resolver.setOrder(2);
		return resolver;
    }

    @Bean
    public DownloadView downloadView() {
        return new DownloadView();
    }
    
    @Bean
    public BeanNameViewResolver beanNameViewResolver() {
        BeanNameViewResolver resolver = new BeanNameViewResolver();
        resolver.setOrder(0);
        return resolver;
    }
	
	@Bean
	public UrlBasedViewResolver tilesViewResolver() {
        UrlBasedViewResolver tilesViewResolver = new UrlBasedViewResolver();
        tilesViewResolver.setViewClass(TilesView.class);
        //kenny: prevent from redirecting https to http
        tilesViewResolver.setRedirectHttp10Compatible(false);
        tilesViewResolver.setOrder(1);
        return tilesViewResolver;
	}
	
    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tiles = new TilesConfigurer();
        tiles.setDefinitions(new String[] { "WEB-INF/tiles/tiles.xml" });
        tiles.setCheckRefresh(true);
        tiles.setDefinitionsFactoryClass(CustomLocaleDefinitionsFactory.class);
        return tiles;
    }
}
