package onlyone.sellerbotcash.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import onlyone.sellerbotcash.security.AccessFailureHandler;
import onlyone.sellerbotcash.security.AccessSuccessHandler;
import onlyone.sellerbotcash.security.DomainAuthenticationProvider;
import onlyone.sellerbotcash.security.ServiceSiteAuthenticationProvider;

import onlyone.sellerbotcash.web.filter.BaseInfoFilter;
import onlyone.sellerbotcash.web.filter.ViewAccessFilter;
import onlyone.sellerbotcash.web.filter.ViewLoggingFilter;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired private BaseInfoFilter baseInfoFilter;
	@Autowired private ViewAccessFilter viewAccessFilter;
	@Autowired private ViewLoggingFilter viewLoggingFilter;
	@Autowired private DomainAuthenticationProvider domainAuthenticationProvider;
	
	@Autowired private ServiceSiteAuthenticationProvider serviceSiteAuthenticationProvider;
	@Autowired private AccessFailureHandler authenticationFailureHandler;
	@Autowired private AccessSuccessHandler successHandler;
	@Value("${internal.redirect.base:}") private String redirectBase;
	
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
	
	/*
	 * Security 적용을 받지 않는 페턴을 추가한다.
	 * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.web.builders.WebSecurity)
	 */
	@Override
    public void configure(WebSecurity web) throws Exception {
        web
        .ignoring()
            .antMatchers("/assets/**")
			// .antMatchers("service/**")
            .antMatchers("/.well-known/**")
			.antMatchers("/attachments/**")
			.antMatchers("/api/LATEST_RELEASE_**")
			// .antMatchers("/sub/payment/renewal_getSignature")
			.antMatchers("/alliance/**")
			.antMatchers("/external/**")
        ;
    }
	
    @Override
    public void configure(HttpSecurity http) throws Exception { 
		http.cors().and().csrf().disable()
		.addFilterAfter(baseInfoFilter, BasicAuthenticationFilter.class)
		.addFilterAfter(viewAccessFilter, BaseInfoFilter.class)
		.addFilterAfter(viewLoggingFilter, BaseInfoFilter.class)
        .formLogin()
    		.loginPage("/master/login") // 관리자 로그인
        	.loginPage("/login") // 일반 로그인
			.loginProcessingUrl("/login")
        	.usernameParameter("usr")
        	.passwordParameter("pwd")
        	.defaultSuccessUrl(redirectBase + "/")
        	.failureHandler(authenticationFailureHandler)
        	.successHandler(successHandler)
        .and()
        .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        	.invalidateHttpSession(true)
        	.deleteCookies("JSESSIONID")
			.logoutSuccessUrl(redirectBase + "/")
        .and()
        .authorizeRequests()
			.antMatchers("/sub/**","/member").authenticated()
		;
		http.headers().frameOptions().disable(); //for inipay
		http.headers().httpStrictTransportSecurity().disable(); // for okname
    }

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(domainAuthenticationProvider);		
		auth.authenticationProvider(serviceSiteAuthenticationProvider);
	}
}
