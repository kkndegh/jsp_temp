package onlyone.sellerbotcash.config.xss;

import com.navercorp.lucy.security.xss.servletfilter.XssEscapeServletFilter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.AllArgsConstructor;

@Configuration
@AllArgsConstructor
public class XssConfig implements WebMvcConfigurer {

    // private final ObjectMapper objectMapper;

    @Bean
    public FilterRegistrationBean<XssEscapeServletFilter> filterRegistrationBean() {
        FilterRegistrationBean<XssEscapeServletFilter> filterRegistration = new FilterRegistrationBean<>();
        filterRegistration.setFilter(new XssEscapeServletFilter());
        filterRegistration.setOrder(1);
        filterRegistration.addUrlPatterns("/*");
        return filterRegistration;
    }

    // @Bean
    // public MappingJackson2HttpMessageConverter jsonEscapeConverter() {
    //     ObjectMapper copy = objectMapper.copy();
    //     copy.getFactory().setCharacterEscapes(new HTMLCharacterEscapes());
    //     return new MappingJackson2HttpMessageConverter(copy);
    // }
}