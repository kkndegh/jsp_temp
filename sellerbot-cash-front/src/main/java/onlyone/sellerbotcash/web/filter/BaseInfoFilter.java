/**
 * @author kenny
 */
package onlyone.sellerbotcash.web.filter;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.service.SmpCustService;

@Slf4j
@Component
public class BaseInfoFilter extends OncePerRequestFilter {
    @Autowired private SmpCustService smpCustService;
    @Value("${inicis.siteDomain:}") private String siteDomain;
    @Value("${session.timeout}") private Integer SESSION_TIMEOUT_IN_SECONDS;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        
        String requestURI = request.getRequestURI(); 
        HttpSession session = request.getSession();        
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();

        setRefererUrl(request, session);
        
        if (auth != null 
            && !(auth instanceof AnonymousAuthenticationToken) 
            && auth.isAuthenticated()) {
            if (session.getAttribute("cust") == null) {
                if (auth.getPrincipal() instanceof User) {
                    User user = (User) auth.getPrincipal();
                    
                    session.setMaxInactiveInterval(SESSION_TIMEOUT_IN_SECONDS);
                    session.setAttribute("cust", smpCustService.getCust(user.getUsername()));

                    if (StringUtils.isEmpty(siteDomain)) {
                        siteDomain = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
                    }
                    session.setAttribute("siteDomain", siteDomain);
                }
            }
        } else {
            
            boolean prevUrlFlag = false;

            if ("/sub/pre_calculate/pre_calculate".equals(requestURI)   // 금융 서비스 페이지
                || "/sub/my/paidCancel".equals(requestURI)              // 이용권 해지 페이지
                || "/sub/payment/step1".equals(requestURI)              // 결제 페이지
                || "/sub/settAcc/calendar".equals(requestURI)           // 정산 예정금
                || "/sub/my/join/market".equals(requestURI)             // 판매몰 관리 페이지
            ) {
                prevUrlFlag = true;
            }

            if (prevUrlFlag) {
                StringBuilder prevUrlBuilder = new StringBuilder(requestURI);
                String queryString = request.getQueryString();

                if (queryString != null) {
                    requestURI = prevUrlBuilder.append('?').append(queryString).toString();
                }
                session.setAttribute("prevUrl", requestURI);
            }
        }

        chain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return request.getRequestURI().contains("/assets");
    }

    private void setRefererUrl(HttpServletRequest req, HttpSession session) {
        if (session.getAttribute("referer") != null)
            return;

        String referer = Objects.toString(req.getHeader("referer"), "");
        String agent = Objects.toString(req.getHeader("user-agent"), "");
        String serverName = req.getServerName();

        if (referer.indexOf(serverName) > -1)
            referer = "";

        session.setAttribute("referer", referer);
        session.setAttribute("agent", agent);
        log.debug("referer: {}, agent: {}", referer, agent);
    }
}
