package onlyone.sellerbotcash.web.filter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

// import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.service.MenuService;
import onlyone.sellerbotcash.web.util.CustPayInfo;
import onlyone.smp.persistent.domain.enums.ECustSts;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtMenuDTO;

/**
 * 결제 여부에 따른 화면 접근 제어
 * 
 * @author YunaJang
 */
// @Slf4j
@Component
public class ViewAccessFilter extends OncePerRequestFilter {

    @Autowired
    private MenuService menuService;
    @Autowired
    private CustPayInfo custPayInfo;
    @Value("${internal.redirect.base:}")
    private String redirectBase;

    @Override
    @SuppressWarnings("unchecked")
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        String requestURI = request.getRequestURI();

        if (Objects.nonNull(session.getAttribute("cust"))) {
            // In
            ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");

            // 회원유입사이트코드가 PLA, EZA인지 여부 체크
            boolean infSiteCheck = false;
            if (cust.getInf_site_cd() == EInfPath.PLA) {
                infSiteCheck = true;
            }

            // 사이드바용 메뉴 리스트 갱신
            if (Objects.isNull(session.getAttribute("menuForAccess"))) {
                List<ExtMenuDTO> menuList = menuService.getMenuListForSideBar(cust.getCust_id());

                // 회원유입사이트코드가 PLA, EZA인 경우 메뉴에서 제거
                if (infSiteCheck) {
                    for (int i = 0; i < menuList.size(); i++) {
                        if (menuList.get(i).getMENU_NM().equals("마이페이지")) {
                            menuList.get(i).getSUB_MENU_LIST().removeIf(obj -> obj.getSUB_MENU_NM().equals("대사 서비스")
                                    || obj.getSUB_MENU_NM().equals("결제정보") || obj.getSUB_MENU_NM().equals("회원탈퇴")
                                    || obj.getSUB_MENU_NM().equals("이용권 소개") || obj.getSUB_MENU_NM().equals("결제하기"));
                        }
                    }

                    menuList.removeIf(obj -> obj.getMENU_NM().equals("이용요금 안내"));
                }

                // 20230228 소드3 테스트 계정 점프 서비스 메뉴 제거
                if (cust.getCust_id().equals("sode3@only1fs") || cust.getCust_id().equals("krossdatatest@gmail.com")) {
                    menuList.removeIf(obj -> obj.getMENU_NM().equals("점프 서비스"));
                }

                session.setAttribute("menu", menuList);
            }

            // 메뉴별 권한 조회
            List<ExtMenuDTO> menuList = null;
            if (Objects.isNull(session.getAttribute("menuForAccess"))) {
                menuList = menuService.getMenuListForViewAccess(cust.getCust_id());
                session.setAttribute("menuForAccess", menuList);
            } else {
                menuList = (List<ExtMenuDTO>) session.getAttribute("menuForAccess");
            }

            HttpServletResponse res = (HttpServletResponse) response;

            if (cust.getCust_sts_cd() == ECustSts.EM) {
                boolean loginCk = false;

                if (!ObjectUtils.isEmpty(cust.getCust_lock_ts())) {
                    LocalDateTime localDateTime = LocalDateTime.now();
                    Date earlierDate = java.sql.Timestamp.valueOf(localDateTime);

                    Date laterDate = java.sql.Timestamp.valueOf(cust.getCust_lock_ts() + ":00");

                    int daysDiff = (int) ((laterDate.getTime() / 1000) - (earlierDate.getTime() / 1000));

                    if (daysDiff > 0) {
                        loginCk = true;
                    }
                }

                if (!loginCk) {
                    session.removeAttribute("cust");
                    session.removeAttribute("menuForAccess");
                    session.setMaxInactiveInterval(0);

                    res.sendRedirect(redirectBase + "/");
                    return;
                }
            }

            // 진입 URL 확인
            ExtMenuDTO menuInfo = menuList.stream().filter(x -> x.getMENU_PATH().equals(requestURI)).findFirst()
                    .orElse(null);
            if (Objects.nonNull(menuInfo)) {
                if (menuInfo.getAUTH_YN().equals(EYesOrNo.Y)) {
                    // 유료 화면을 무료 사용자가 진입 시 이용안내 페이지로 redirect
                    if (!custPayInfo.isPayingUser(session, cust.getCust_id())) {
                        if (infSiteCheck) {
                            res.sendRedirect(redirectBase + "/redirect/alert");
                            return;
                        } else {
                            res.sendRedirect(redirectBase + "/sub/payment/step1");
                            return;
                        }
                    }

                    // 기본 상품만 이용 중인 경우 대사서비스 화면 접근 불가
                    if (custPayInfo.isOnlyBasicUser(session, cust.getCust_id())
                            && requestURI.indexOf("reconcile") > -1) {
                        res.sendRedirect(redirectBase + "/sub/payment/step1");
                        return;
                    }

                    // 20230228 소드3 테스트 계정 점프 서비스 메뉴 막기
                    if ((cust.getCust_id().equals("sode3@only1fs") || cust.getCust_id().equals("krossdatatest@gmail.com"))
                            && requestURI.indexOf("jump") > -1) {
                        res.sendRedirect(redirectBase + "/");
                        return;
                    }
                }

                // 뱅크봇만 이용 시 특정 화면만 접근 가능
                if (requestURI.indexOf("reconcile") > -1 && !custPayInfo.isBNKUser(session, cust.getCust_id())) {
                    res.sendRedirect(redirectBase + "/sub/payment/step1");
                    return;
                }
            } else if (requestURI.indexOf("reconcile") > -1) {
                res.sendRedirect(redirectBase + "/");
                return;
            }
        }

        chain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return request.getRequestURI().contains("/assets");
    }
}