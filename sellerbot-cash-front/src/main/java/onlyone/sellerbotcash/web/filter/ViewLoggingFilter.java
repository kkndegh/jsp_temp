package onlyone.sellerbotcash.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

// import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.service.LoggingService;
import onlyone.smp.service.dto.ExtCustDTO;

/**
 * 화면 접근 이력 저장용 Filter
 * @author YunaJang
 */
// @Slf4j
@Component
public class ViewLoggingFilter extends OncePerRequestFilter {
    @Autowired private LoggingService loggingService;

    /**
     * 화면이 모두 그려진 후에 이력을 저장, 로그인 필수
     */
    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        chain.doFilter(request, response);
        //kenny: 마지막 filter에서 리소스 처리 및 전달하고 이곳으로

        String requestURI = request.getRequestURI();
        boolean isView = false;
        if (requestURI.equals("/") || requestURI.startsWith("/sub/") || requestURI.startsWith("/pub/sc/")) {
            isView = true;
        }

        HttpSession session = request.getSession();

        if (isView && session.getAttribute("cust") != null) {
            ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");
            loggingService.addSiteUsePath(cust.getCust_id(), request.getRemoteAddr(), requestURI);
        }
    } 

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		return request.getRequestURI().contains("/assets/");
	}
}