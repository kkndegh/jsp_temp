
package onlyone.sellerbotcash.web.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.cert.X509Certificate;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ExternalDriverApiResource {
    @Value("${repository.iedriver}")
    String iedriverPath;

    /**
     * IE 드라이버 버전 체크 API
     * @param version
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @GetMapping("/api/LATEST_RELEASE_{version}")
    public ResponseEntity<String> getLatestVersion(@PathVariable String version)
            throws JsonParseException, JsonMappingException, IOException {
        String jsonStr = getJsonString(iedriverPath + "latest.json");
        ObjectMapper objectMapper = new ObjectMapper();

        TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
        Map<String, String> map = objectMapper.readValue(jsonStr, typeRef);
        String latestVersion = map.get(version);
        if(Objects.isNull(latestVersion) || latestVersion.equals(""))
            latestVersion = map.get("3100");

        return new ResponseEntity<String>(latestVersion, HttpStatus.OK);
    }

    private String getJsonString(String urlStr) {
        String rtnStr = "";

        try {
            URL url = new URL(urlStr);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("CONTENT-TYPE", "Application/json");

            TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            } };
    
            SSLContext sc;
            try {
                sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));

            String inputLine;
            StringBuffer stringBuffer = new StringBuffer();

            while ((inputLine = bufferedReader.readLine()) != null) {
                stringBuffer.append(inputLine.trim());
            }
            
            bufferedReader.close();
            rtnStr = stringBuffer.toString();
            
        } catch (IOException e) {
            log.error("" + e);
        }
        
        return rtnStr;
    }
}