package onlyone.sellerbotcash.web.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.sellerbotcash.web.vm.JoinSessionInfo;
import onlyone.sellerbotcash.web.vm.SimpleJoinReqDTO;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EMallCertTyp;
import onlyone.smp.service.dto.ExtAgreeTermsReqDTO;
import onlyone.smp.service.dto.ExtCustMallDTO;
import onlyone.smp.service.dto.ExtCustPackageDTO;

@Slf4j
@RestController
public class RestJoinController {
	
	@Value("${join.skip:false}") private boolean skipCustAuthId;
	@Value("${smp.svcId:sellerbotcash}") private String svcId;
	@Autowired private SmpJoinService smpJoinService;
	@Autowired private SmpCustService smpCustService;
	
	/**
	 * 사업자 등록 번호 확인
	 * @param bizNo
	 * @return
	 */
	@PostMapping("/pub/api/member/join/checkBizNo")
	public ResponseEntity<String> checkBizNo(String bizNo, HttpSession session) {
		JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);
		if(joinSessionInfo == null) {
			return ResponseEntity.status(HttpStatus.GONE).body("flow error");
		}
		ExtCustPackageDTO custDto = joinSessionInfo.getCustDto();
		if(StringUtils.isNotEmpty(custDto.getBiz_no()) ){
			//사업자 번호가 있을 경우 biz_no값을 초기화 한다.
			custDto.setBiz_no(null);
			session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);
		}
		smpJoinService.checkBizNo(bizNo);
		
		custDto.setBiz_no(bizNo);
		session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);
		
		return ResponseEntity.ok("");

	}
	
	/**
	 * cust_id에 대한 인증 메일 발송
	 * @param custId
	 * @param session
	 * @return
	 */
	@PostMapping("/pub/api/member/join/confirmCust")
	public ResponseEntity<String> confirmCust(String custId, HttpSession session){
		JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);
	
		if(joinSessionInfo == null) {
			return ResponseEntity.status(HttpStatus.GONE).body("flow error");
		}
		// 이메일 재발송 허용
//		if(StringUtils.isNotEmpty(joinSessionInfo.getCustIdToken())) {
//			return ResponseEntity.status(4490).body("already");
//		}
		String token = null;
		if (skipCustAuthId) {
			token = "nocheck";
		} else {
			token = smpJoinService.confirmCust(custId);
		}
		joinSessionInfo.setCustIdToken(token);
		ExtCustPackageDTO custDto = new ExtCustPackageDTO();
		custDto.setCust_id(custId);
		joinSessionInfo.setCustDto(custDto );
		session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);
		return ResponseEntity.ok("");
	}
	
	/**
	 * 인증 번호 확인
	 * @param authNo
	 * @param modelMap
	 * @param session
	 * @return
	 */
	@PostMapping("/pub/api/member/join/confirmAuthNo")
	public ResponseEntity<String> postJoinStep2(String authNo, ModelMap modelMap, HttpSession session) {
		JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);
			
		if(joinSessionInfo == null) {
			return ResponseEntity.status(HttpStatus.GONE).body("flow error");
		}
		if(StringUtils.isEmpty(joinSessionInfo.getCustIdToken())) {
			return ResponseEntity.status(4490).body("not called");
		}
		// 테스트를 위하여 인증 번호 확인 절차 제거
		boolean result =false;
		if(skipCustAuthId) {
			result = true;
		}else {
			result = smpJoinService.confirmCustAuthtoken(joinSessionInfo.getCustDto().getCust_id(), authNo, joinSessionInfo.getCustIdToken());
		}
		if(result) {
			joinSessionInfo.setCompleteAuthNo(true);
			session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);
			return ResponseEntity.ok("");
		}else {
			return ResponseEntity.badRequest().body("not same");
		}
		
	}
	
	/**
	 * 간편 가입 회원 가입
	 * @param authNo
	 * @param modelMap
	 * @param session
	 * @return
	 */
	@Transactional
	@PostMapping("/pub/api/simple_Join")
	public ResponseEntity<String> simpleJoin(HttpSession session, @RequestBody SimpleJoinReqDTO joinDto) {
		JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);
		
		// 세션 확인
		if(joinSessionInfo == null) {
			return ResponseEntity.status(HttpStatus.GONE).body("flow error");
		}
		
		// 토큰이 없는경우
		if(StringUtils.isEmpty( joinDto.getToken())) {
			joinDto.setToken(smpCustService.getToken(joinDto.getCust_id()));
		}
		
		ExtCustPackageDTO custDto = joinSessionInfo.getCustDto();
		
		// 기본 정보 설정
		custDto.setPasswd(joinDto.getPasswd());
		custDto.setPasswd_same(joinDto.getPasswd_same());
		custDto.setBiz_nm(joinDto.getBiz_nm());
		custDto.setBiz_no(joinDto.getBiz_no());
		custDto.setCeo_nm(joinDto.getCeo_nm());
		custDto.setCeo_no(joinDto.getCeo_no());
		if (StringUtils.isNotEmpty(joinDto.getChrg_nm())) {
			custDto.setChrg_nm(joinDto.getChrg_nm());
		}
		if (StringUtils.isNotEmpty(joinDto.getChrg_no())) {
			custDto.setChrg_no(joinDto.getChrg_no());
		}
		
		// 현시점에서 유입 경로는 SMAP만 존재한다.[고정]
		custDto.setInf_path_cd(joinDto.getInf_path_cd());
		custDto.setInf_site_cd(joinSessionInfo.getSiteCd());
		
		// 동의 약관
		List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();
		for(Long term : joinDto.getTerms()) {
			ExtAgreeTermsReqDTO  extAgreeTermsReqDTO = new ExtAgreeTermsReqDTO();
			extAgreeTermsReqDTO.setCust_id(joinDto.getCust_id());
			extAgreeTermsReqDTO.setSvc_id(svcId);
			extAgreeTermsReqDTO.setTerms_id(term);
			terms.add(extAgreeTermsReqDTO);
		}
		custDto.setTerms(terms);

		custDto.setEventNo(joinSessionInfo.getEventNo());
		//custDto.setIpkPartnerNo(joinDto.getIpkPartnerNo());
		//custDto.setIpkSupplySeq(joinDto.getIpkSupplySeq());
		
		// 회원가입
		if (smpJoinService.addCustPkg(custDto)) {
			// 1. 회원정보 등록 성공
			// 2. 몰 정보 등록

			if (joinDto.getInf_path_cd() == EInfPath.SMA) {
				// SMAP 회원의 몰정보 
				List<Map<String, String>> mallList = smpJoinService.getSMapMemberInfo(joinDto.getToken(), joinDto.getBiz_no(), false);
				
				List<ExtCustMallDTO> extCustMallDTOList = new ArrayList<ExtCustMallDTO>();
				for (Map<String, String> map : mallList) {
					ExtCustMallDTO extCustMallDTO = new ExtCustMallDTO();
					extCustMallDTO.setMall_cd( map.get("mall_cd") );
					extCustMallDTO.setCert_step( Integer.valueOf(map.get("cert_step")) );
					extCustMallDTO.setMall_cert_1st_id( map.get("mall_cert_1st_id"));
					extCustMallDTO.setMall_cert_1st_passwd(map.get("mall_cert_1st_passwd") );
					extCustMallDTO.setPasswd_step( Integer.valueOf(map.get("passwd_step")) );
					
					if (!StringUtils.isEmpty(map.get("sub_mall_cert_1st"))) {
						extCustMallDTO.setSub_mall_cert_1st(map.get("sub_mall_cert_1st"));	
					}
					
					if (!StringUtils.isEmpty(map.get("sub_mall_cert_2nd"))) {
						extCustMallDTO.setSub_mall_cert_2nd(map.get("sub_mall_cert_2nd"));	
					}
					
					if (Objects.nonNull(map.get("mall_cert_typ"))) {
						extCustMallDTO.setMall_cert_typ_cd( EMallCertTyp.valueOf(map.get("mall_cert_typ")) );
					} else {
						extCustMallDTO.setMall_cert_typ_cd( EMallCertTyp.A00 );
					}
					
					extCustMallDTOList.add(extCustMallDTO);
				}
				
				extCustMallDTOList = smpJoinService.createMall(extCustMallDTOList, joinDto.getCust_id());
				
				
				// 소드원 회원만 스크랩핑을 실행한다.
				try {
					smpJoinService.scrap(extCustMallDTOList, joinDto.getCust_id());	
				} catch (Exception e) {
					log.error("첫 mall 등록 후 스크래핑 처리시 에러가 발생하였습니다.\n" + e);
				}
				
			}
			
		} else {
			return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);	
		}
		
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
}
