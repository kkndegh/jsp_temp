package onlyone.sellerbotcash.web.page;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AuthenticationController {

	@GetMapping(value="/auth/SearchId")
	public String viewSearchId(ModelMap modelMap) {
		return "auth/getSearchId";
	}
	
	@PostMapping(value="/auth/SearchId")
	public String login(ModelMap modelMap) {
		return "auth/postSearchId";
	}
	
}
