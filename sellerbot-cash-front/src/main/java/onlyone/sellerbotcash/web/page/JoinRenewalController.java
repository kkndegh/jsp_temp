package onlyone.sellerbotcash.web.page;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import onlyone.sellerbotcash.service.dto.ExtraData.Goods;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inicis.std.util.HttpUtil;
import com.inicis.std.util.ParseUtil;
import com.inicis.std.util.SignatureUtil;

import static onlyone.sellerbotcash.config.SellerbotConstant.GRANTEDAUTH;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.security.ServiceSiteAuthenticationToken;
import onlyone.sellerbotcash.service.AccountService;
import onlyone.sellerbotcash.service.GoodsService;
import onlyone.sellerbotcash.service.MypageService;
import onlyone.sellerbotcash.service.PaymentService;
import onlyone.sellerbotcash.service.SbfUtils;
import onlyone.sellerbotcash.service.SmpAccountService;
import onlyone.sellerbotcash.service.SmpBankService;
import onlyone.sellerbotcash.service.SmpBannerService;
import onlyone.sellerbotcash.service.SmpCommService;
import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.sellerbotcash.service.SmpLoanService;
import onlyone.sellerbotcash.service.dto.ExtraData;
import onlyone.sellerbotcash.web.util.CustPayInfo;
import onlyone.sellerbotcash.web.util.NumberToKorUtil;
import onlyone.sellerbotcash.web.util.SecurityAES256;
import onlyone.sellerbotcash.web.vm.JoinCustInfo;
import onlyone.sellerbotcash.web.vm.JoinSessionInfo;
import onlyone.sellerbotcash.web.vm.JoinTerms;
import onlyone.smp.persistent.domain.enums.ECustPosi;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EStorFileTyp;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ComboLoanDTO;
import onlyone.smp.service.dto.ExtAgreeTermsReqDTO;
import onlyone.smp.service.dto.ExtBankResponseDTO;
import onlyone.smp.service.dto.ExtBannerDTO;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtCusAcctDTO;
import onlyone.smp.service.dto.ExtCustAcctInfoDTO;
import onlyone.smp.service.dto.ExtCustAcctListDTO;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtCustMallListDTO;
import onlyone.smp.service.dto.ExtCustPackageDTO;
import onlyone.smp.service.dto.ExtGoodsDTO;
import onlyone.smp.service.dto.ExtJoinCustMallListDTO;
import onlyone.smp.service.dto.ExtSelectGoodsDTO;
import onlyone.smp.service.dto.ExtJoinDTO;
import onlyone.smp.service.dto.ExtKkontTgtDTO;
import onlyone.smp.service.dto.ExtLoanDTO;
import onlyone.smp.service.dto.ExtMaxRegCountResDTO;
import onlyone.smp.service.dto.ExtPayInicisReqDTO;
import onlyone.smp.service.dto.ExtPayInicisRespDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO;
import onlyone.smp.service.dto.ExtSvcTermsDTO;
import onlyone.smp.service.dto.ExtUsingGoodsInfoDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO.ExtSmtAgreeTermsInfoDTO;

@Slf4j
@Controller
public class JoinRenewalController {

    @Value("${smp.svcId:sellerbotcash}")
    private String svcId;
    @Value("${internal.redirect.base:}")
    private String redirectBase;

    @Autowired
    private SmpJoinService smpJoinService;

    @Autowired
    private SmpCommService smpCommService;

    @Autowired
    private SmpCustMallService smpCustMallService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private SmpCustService smpCustService;

    // @Autowired
    // private Cafe24Service cafe24Service;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private SmpLoanService smpLoanService;

    @Autowired
    private SmpBannerService smpBannerService;

    @Autowired
    private CustPayInfo custPayInfo;

    @Autowired
    private AccountService accountService;

    @Autowired
    private MypageService mypageService;

    @Autowired
    private SmpBankService smpBankService;

    @Autowired
    private SmpAccountService smpAccountService;

    @Autowired
    private Environment env;

    /**
     * 멤버십 선택(step1)
     * 
     * @return
     */
    @GetMapping("/pub/member/step1")
    public String getJoinRenewalStep1(ModelMap modelMap, HttpSession session, @AuthenticationPrincipal User user) {
        JoinSessionInfo joinSessionInfo = new JoinSessionInfo();

        // 로그인 상태에서 join_step에 접속시 메인 화면으로 이동 시킨다.
        if (Objects.nonNull(user)) {
            return "redirect:" + redirectBase + "/";
        }

        String siteCd = (String) session.getAttribute("siteCd");
        if (siteCd != null) {
            joinSessionInfo.setSiteCd(EInfPath.valueOf(siteCd));
            if (EInfPath.CF24.name().equals(siteCd) || EInfPath.SHBY.name().equals(siteCd)) {
                joinSessionInfo.setMallId((String) session.getAttribute("siteId"));
            }
        }

        Long eventNo = (Long) session.getAttribute("pe");
        if (eventNo != null) {
            joinSessionInfo.setEventNo(eventNo);
        }

        // String infPath = (String) session.getAttribute("infPath");
        // if (infPath != null) {
        //     joinSessionInfo.setInfPath(infPath);
        //     if (EInfPath.SCB.name().equals(infPath.toUpperCase())) {
        //         joinSessionInfo.setSiteCd(EInfPath.valueOf(infPath.toUpperCase()));
        //     }
        // }

        // 상품 정보
        // 기존 로니봇, 파이봇은 리스트에 안뜨게 하려고 코드 분기처리
        List<ExtGoodsDTO> goodsList = goodsService.getGoodsList("", ""); // 디폴트 상품 조회시는 파라미터에 빈값("제휴 구분코드",
                                                                         // "cust_seq_no") 넘김
        goodsList = goodsList.stream().filter(good -> {
            if (good.getGOODS_TYP_CD().equals("RNB") || good.getGOODS_TYP_CD().equals("PYB")) {
                return false;
            } else {
                return true;
            }
        }).collect(Collectors.toList());

        modelMap.addAttribute("goodsList", goodsList);

        // 누적 판매량 , 사용 유저 , 지원 판매몰
        ExtJoinDTO step_one_info = goodsService.getStepOneInfo();

        String salsePrc = step_one_info.getSumSalsePrc();
        String numberString = NumberToKorUtil.NumberToKor(salsePrc);

        modelMap.addAttribute("siteInfo", step_one_info);
        modelMap.addAttribute("numberString", numberString);
        modelMap.addAttribute("activeProfiles", env.getActiveProfiles()[0]);

        session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);

        return "sub/memberRenewal/join_renewal_step1";
    }

    @PostMapping("/pub/member/step1")
    public String postJoinRenewalStep1(ModelMap modelMap, HttpSession session, String goodsSeqNo, String goodsOptSeqNo,
            String goodsTypCd, String loanSvc) {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);

        if (joinSessionInfo == null) {
            return "redirect:" + redirectBase + "/pub/member/step1";
        }

        joinSessionInfo.setGoodsSeqNo(goodsSeqNo);
        joinSessionInfo.setGoodsOptSeqNo(goodsOptSeqNo);
        joinSessionInfo.setGoodsTypCd(goodsTypCd);

        // SC제일은행 URL 사용중
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(loanSvc)) {
            session.setAttribute("infPath", loanSvc);
        } else {
            session.removeAttribute("infPath");
        }

        session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);

        return "redirect:" + redirectBase + "/pub/member/step2";
    }

    /**
     * 이메일 인증 및 약관 동의(step2)
     * 
     * @param modelMap
     * @param session
     * @return
     */
    @GetMapping("/pub/member/step2")
    public String getJoinRenewalStep2(ModelMap modelMap, HttpSession session, @AuthenticationPrincipal User user) {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);

        // 로그인 상태에서 join_step에 접속시 메인 화면으로 이동 시킨다.
        if (Objects.nonNull(user)) {
            return "redirect:" + redirectBase + "/";
        }

        // session이 생성되지 않은 상태이면 step1으로 이동 시킨다.
        if (joinSessionInfo == null
                || (joinSessionInfo.getGoodsSeqNo() == null && joinSessionInfo.getGoodsOptSeqNo() == null)) {
            return "redirect:" + redirectBase + "/pub/member/step1";
        }

        List<ExtSvcTermsDTO> sellerbotTermsList = smpJoinService.getSellerBotTerms(svcId);
        // 모든 약관 아이디 저장
        joinSessionInfo.setAllTermsIds(sellerbotTermsList.stream().collect(Collectors.toMap(k -> k.getTerms_id(),
                v -> new JoinTerms(v.getTerms_esse_yn() == EYesOrNo.Y, v.getSvc_id(), v.getTerms_id()))));
        // 필수 약관 아이디 저장
        joinSessionInfo.setEssentialTermsIds(sellerbotTermsList.stream().filter(it -> {
            return it.getTerms_esse_yn() == EYesOrNo.Y;
        }).map(it -> {
            return it.getTerms_id();
        }).sorted().collect(Collectors.toList()));

        // 20221117 셀러봇캐시 마켓팅 활용 세부 항목 가져오기 추가
        List<ExtCommCdDTO> smtTermsList = smpCommService.getCodeList("SMT");
        smtTermsList = smtTermsList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord).reversed())
                .collect(Collectors.toList());

        session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);

        System.out.println(session.getAttribute("infPath"));

        modelMap.addAttribute("sellerbotTerms", sellerbotTermsList);
        modelMap.addAttribute("smtTermsList", smtTermsList); // 셀러봇캐시 마켓팅 활용 세부 항목

        return "sub/memberRenewal/join_renewal_step2";
    }

    /**
     * 이메일 , 약관동의 세션에 저장
     */
    @PostMapping("/pub/member/step2")
    public String postJoinRenewalStep2(ModelMap modelMap, HttpSession session, JoinCustInfo JoinCustInfo,
            String[] sellerbotTerm, String[] smtTerm) throws Exception {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);

        if (joinSessionInfo == null
                || (joinSessionInfo.getGoodsSeqNo() == null && joinSessionInfo.getGoodsOptSeqNo() == null)) {
            return "redirect:" + redirectBase + "/pub/member/step1";
        }

        if (JoinCustInfo.getCust_id().isEmpty() || JoinCustInfo.getPasswd().isEmpty()) {
            return "redirect:" + redirectBase + "/pub/member/step2";
        }

        joinSessionInfo.getCustDto().setCust_id(JoinCustInfo.getCust_id());

        // 패스워드 암호화
        joinSessionInfo.getCustDto().setPasswd(SecurityAES256.AES_Encode(JoinCustInfo.getPasswd()));
        joinSessionInfo.getCustDto().setPasswd_same(SecurityAES256.AES_Encode(JoinCustInfo.getPasswd_same()));

        log.info("passwd AES256 == {} ", joinSessionInfo.getCustDto().getPasswd());

        Map<Long, JoinTerms> allTerm = joinSessionInfo.getAllTermsIds();
        Map<Long, JoinTerms> selectedTerm = new TreeMap<>();
        int essentialSize = 0;
        // 약관 선택 오류 확인 및 잘못 전달된 id 값 확인
        for (String item : sellerbotTerm) {
            Long sTerm = Long.parseLong(item);
            if (!allTerm.containsKey(sTerm))
                throw new RuntimeException("Not Found TermId " + sTerm);
            if (selectedTerm.containsKey(sTerm))
                throw new RuntimeException("Same TermId Get " + sellerbotTerm);
            JoinTerms terms = allTerm.get(sTerm);
            Boolean isEssentialTerm = terms.isEssTerm();
            selectedTerm.put(sTerm, terms);
            if (isEssentialTerm)
                essentialSize++;
        }

        if (essentialSize != joinSessionInfo.getEssentialTermsIds().size()) {
            log.error("필수 약관 선택 수 오류 " + selectedTerm);
            return "redirect:" + redirectBase + "/pub/member/step2";
        }
        joinSessionInfo.setSelectedTerms(selectedTerm);

        // 셀러봇캐시 마케팅 활용 동희 항목
        List<String> selectedSmtTerms = new ArrayList<String>();
        if (!org.springframework.util.StringUtils.isEmpty(smtTerm)) {
            for (String smtTermItem : smtTerm) {
                selectedSmtTerms.add(smtTermItem);
            }
        }
        joinSessionInfo.setSelectedSmtTerms(selectedSmtTerms);

        // if (joinSessionInfo.getMallId() != null) {
        // Cafe24StoreDTO tmp = cafe24Service.getStoreInfo(joinSessionInfo.getMallId());
        // // 카페24 연동API에 데이터가 존재할 경우 session 에 저장
        // if (tmp != null && tmp.getStore() != null) {
        // joinSessionInfo.setCafeStore(tmp.getStore());
        // }
        // }

        session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);
        return "redirect:" + redirectBase + "/pub/member/step3";
    }

    /**
     * 사업자 정보 입력(step3)
     * 
     * @param modelMap
     * @param session
     * @return
     */
    @GetMapping("/pub/member/step3")
    public String getJoinRenewalStep3(ModelMap modelMap, HttpSession session, JoinCustInfo custInfo,
            @AuthenticationPrincipal User user) {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);

        // 로그인 상태에서 join_step에 접속시 메인 화면으로 이동 시킨다.
        if (Objects.nonNull(user)) {
            return "redirect:" + redirectBase + "/";
        }

        // 세션 , 상품 번호 , 상품 옵션 번호
        if (joinSessionInfo == null
                || (joinSessionInfo.getGoodsSeqNo() == null && joinSessionInfo.getGoodsOptSeqNo() == null)) {
            return "redirect:" + redirectBase + "/pub/member/step1";
        }

        // user_id , pass_word
        if (joinSessionInfo.getCustDto().getCust_id().isEmpty() || joinSessionInfo.getCustDto().getPasswd().isEmpty()) {
            return "redirect:" + redirectBase + "/pub/member/step2";
        }

        // 약관 동의 값
        if (joinSessionInfo.getSelectedTerms() == null || joinSessionInfo.getSelectedTerms().size() == 0) {
            return "redirect:" + redirectBase + "/pub/member/step2";
        }

        System.out.println(session.getAttribute("infPath"));

        List<ExtCommCdDTO> codeList = smpCommService.getCodeList("A020");
        List<ExtCommCdDTO> sortedCodeList = codeList.stream()
                .sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord).reversed()).collect(Collectors.toList());

        modelMap.addAttribute("codeList", sortedCodeList);
        modelMap.addAttribute("cust_id", joinSessionInfo.getCustDto().getCust_id());

        EInfPath siteCd = joinSessionInfo.getSiteCd();
        if (siteCd != null) {
            modelMap.addAttribute("spCode", siteCd.name());
        }

        System.out.println(session.getAttribute("infPath"));

        return "sub/memberRenewal/join_renewal_step3";
    }

    @PostMapping("/pub/member/step3")
    public String postJoinRenewalStep2(JoinCustInfo custInfo, ModelMap modelMap, HttpServletRequest request,
            HttpSession session) throws Exception {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);

        // 세션 , 상품 번호 , 상품 옵션 번호
        if (joinSessionInfo == null
                || (joinSessionInfo.getGoodsSeqNo() == null && joinSessionInfo.getGoodsOptSeqNo() == null)) {
            return "redirect:" + redirectBase + "/pub/member/step1";
        }

        // user_id , pass_word
        if (joinSessionInfo.getCustDto().getCust_id().isEmpty() || joinSessionInfo.getCustDto().getPasswd().isEmpty()) {
            return "redirect:" + redirectBase + "/pub/member/step2";
        }

        // 약관 동의 값
        if (joinSessionInfo.getSelectedTerms() == null || joinSessionInfo.getSelectedTerms().size() == 0) {
            return "redirect:" + redirectBase + "/pub/member/step2";
        }

        // 패스워드 복호화
        joinSessionInfo.getCustDto().setPasswd(SecurityAES256.AES_Decode(joinSessionInfo.getCustDto().getPasswd()));
        joinSessionInfo.getCustDto()
                .setPasswd_same(SecurityAES256.AES_Decode(joinSessionInfo.getCustDto().getPasswd_same()));

        // 사업자정보 저장
        ExtCustPackageDTO custDto = joinSessionInfo.getCustDto();
        // 사업자 번호 중복 확인
        if (StringUtils.isEmpty(custDto.getBiz_no())) {
            return "redirect:" + redirectBase + "/pub/member/step3";
        }

        // 기본 정보 설정
        custDto.setBiz_nm(custInfo.getBiz_nm().replaceAll(" ", ""));
        custDto.setCeo_nm(custInfo.getCeo_nm().replaceAll(" ", ""));
        custDto.setCeo_no(custInfo.getCeo_no());
        if (StringUtils.isNotEmpty(custInfo.getChrg_nm())) {
            custDto.setChrg_nm(custInfo.getChrg_nm().replaceAll(" ", ""));
        }
        if (StringUtils.isNotEmpty(custInfo.getChrg_no())) {
            custDto.setChrg_no(custInfo.getChrg_no());
        }
        custDto.setInf_path_cd(custInfo.getInf_path_cd());

        // 선택된 약관을 list로 변환
        List<ExtAgreeTermsReqDTO> terms = joinSessionInfo.getSelectedTerms().values().stream().map(it -> {
            ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
            t.setCust_id(custDto.getCust_id());
            t.setSvc_id(it.getSvcId());
            t.setTerms_id(it.getTermId());
            return t;
        }).collect(Collectors.toList());

        // 약관 정보 설정
        custDto.setTerms(terms);

        // 20221118 셀러봇 캐시 마켓팅 동의 선택된 항목을 list로 변환
        ExtSmtAgreeTermsDTO d = new ExtSmtAgreeTermsDTO();
        d.setCust_id(custInfo.getCust_id());
        d.setSvc_id(svcId);
        // d.setCust_seq_no는 SMP에서 신규 회원정보 생성 후 처리함.

        List<ExtSmtAgreeTermsInfoDTO> smtTerms = joinSessionInfo.getSelectedSmtTerms().stream().map(s -> {
            ExtSmtAgreeTermsInfoDTO e = new ExtSmtAgreeTermsInfoDTO();
            e.setSmt_terms_cd(s);
            e.setSmt_terms_aggr_yn(EYesOrNo.Y);

            return e;
        }).collect(Collectors.toList());
        d.setSmt_aggr_item_list(smtTerms);
        // 20221118 셀러봇 캐시 마켓팅 설정
        custDto.setSmtTerm(d);

        custDto.setInf_site_cd(joinSessionInfo.getSiteCd());
        custDto.setMallId(joinSessionInfo.getMallId());
        custDto.setEventNo(joinSessionInfo.getEventNo());

        custDto.setGoodsSeqNo(joinSessionInfo.getGoodsSeqNo());
        custDto.setGoodsOptSeqNo(joinSessionInfo.getGoodsOptSeqNo());
        custDto.setGoodsTypCd(joinSessionInfo.getGoodsTypCd());

        if (smpJoinService.addCustPkg(custDto)) {

            session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, null);
            // 회원가입 완료 시 로그인 처리 프로세스
            User user = new User(custDto.getCust_id(), "", new ArrayList<>());
            ServiceSiteAuthenticationToken token = new ServiceSiteAuthenticationToken(user, "", GRANTEDAUTH);
            request.getSession();
            token.setDetails(new WebAuthenticationDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(token));

            new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(30000);
                        /**
                         * 1. 알림톡 미신청 일 경우 발생 => 회원가입 완료 알림톡 발송만 2. 알림톡 신청 일 경우 발생 => 알림톡 신청 발송만
                         */

                        // 알림톡 대상자 등록
                        ExtKkontTgtDTO extKkontTgtDTO = new ExtKkontTgtDTO();
                        boolean rtn = false;

                        // 대표자 정보가 있을 경우
                        if (StringUtils.isNotEmpty(custInfo.getCeo_nm())
                                && StringUtils.isNotEmpty(custInfo.getCeo_no())) {
                            // 알림톡 신청
                            if (StringUtils.isNotEmpty(custInfo.getTrs_hop_hour1())) {
                                extKkontTgtDTO.setCust_posi_cd(ECustPosi.CEO);
                                extKkontTgtDTO.setTgt_cust_nm(custInfo.getCeo_nm());
                                extKkontTgtDTO.setTgt_cust_no(custInfo.getCeo_no());
                                extKkontTgtDTO.setTrs_hop_hour(Integer.valueOf(custInfo.getTrs_hop_hour1()));
                                rtn = smpCustService.alarm_insert(custDto.getCust_id(), extKkontTgtDTO);

                                // 대표자 정보 저장이 정상이면서 담당자 정보가 있을 경우
                                if (rtn && StringUtils.isNotEmpty(custInfo.getChrg_nm())
                                        && StringUtils.isNotEmpty(custInfo.getChrg_no())
                                        && StringUtils.isNotEmpty(custInfo.getTrs_hop_hour2())) {
                                    extKkontTgtDTO.setCust_posi_cd(ECustPosi.CRG);
                                    extKkontTgtDTO.setTgt_cust_nm(custInfo.getChrg_nm());
                                    extKkontTgtDTO.setTgt_cust_no(custInfo.getChrg_no());
                                    extKkontTgtDTO.setTrs_hop_hour(Integer.valueOf(custInfo.getTrs_hop_hour2()));
                                    rtn = smpCustService.alarm_insert(custDto.getCust_id(), extKkontTgtDTO);
                                }
                            } else {
                                // 알림톡 미신청
                                // 회원가입 알림톡 발송
                                smpCustService.alarm_send_join(custDto.getCust_id());
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            // 20211020 가입프로모션
            try {
                log.info("####################JOINFREE paymentPromotion Start###################"
                        + custDto.getCust_id());
                paymentService.paymentPromotion(custDto.getCust_id(), "FREE", "", "", "JOINFREE");
            } catch (Exception e) {
                log.error("JOINFREEPromotion Exception####" + e);
            }

            return "redirect:" + redirectBase + "/sub/member/step4";
        } else {
            return "redirect:" + redirectBase + "/pub/member/step3?error=add";
        }
    }

    /**
     * 회원가입이 완료 안내(step4)
     * 
     * @param modelMap
     * @param session
     * @return
     */
    @SuppressWarnings("unused")
    @GetMapping("/sub/member/step4")
    public String getJoinRenewalStep4(ModelMap modelMap, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        return "sub/memberRenewal/join_renewal_step4";
    }

    /**
     * 판매몰 등록(step5)
     * 
     * @param modelMap
     * @param session
     * @return
     */

    @GetMapping("/sub/member/step5")
    public String getJoinRenewalStep5(ModelMap modelMap, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        /**
         * 2020-06-23 고객 몰 정보가 존재하면 화면 이동
         */
        ExtCustMallListDTO custMall = smpCustMallService.getCustMall(user.getUsername());
        if (Objects.nonNull(custMall) && custMall.getAll_mall_cnt() > 0) {
            return "redirect:" + redirectBase + "/";
        }

        // 금융상품 서비스 콤보 리스트
        List<ComboLoanDTO> selectList = smpLoanService.getComboLoanInfo(user.getUsername());
        modelMap.put("selectList", selectList);

        ExtLoanDTO loanBatchProTgtInfo = smpLoanService.getLoanBatchProTgtInfo(user.getUsername());
        modelMap.addAttribute("loanBatchProTgtInfo", loanBatchProTgtInfo);

        // 현재 이용권으로 등록 가능한 수량 조회
        String userName = user.getUsername();
        ExtMaxRegCountResDTO maxRegCountInfo = paymentService.getMaxRegCountInfo(userName);
        modelMap.addAttribute("maxRegCountInfo", maxRegCountInfo);

        // 배너 정보(판매몰관리우측)
        List<ExtBannerDTO> bannerB2RRDataList = smpBannerService.getBannerData(EStorFileTyp.B2RR);
        modelMap.put("bannerB2RRDataList", bannerB2RRDataList);

        return "sub/memberRenewal/join_renewal_step5";
    }

    /**
     * 결제수단 등록(step6)
     * 
     * @param modelMap
     * @param session
     * @return
     */
    @GetMapping("/sub/member/step6")
    public String getJoinRenewalStep6(HttpServletRequest request, ModelMap modelMap, HttpSession session,
            Device device) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        User user = (User) auth.getPrincipal();

        String userName = user.getUsername();
        ExtCustDTO cust = smpCustService.getCust(userName);

        if (cust == null) {
            throw new RuntimeException(user.getUsername() + "not found!");
        }

        if (cust.getReg_mall_cnt_al() == 0) {
            return "redirect:" + redirectBase + "/sub/member/step5";
        }

        // 이용중 상품 여부 확인
        if (cust.getCust_uses_goods_total_count() > 0) {
            return "redirect:" + redirectBase;
        }

        if (map != null) {
            modelMap.put("successYN", map.get("successYN"));
            modelMap.put("errMsg", map.get("errMsg"));
        }

        // 건너띄기 가능 boolean
        boolean skipResult = false;
        skipResult = smpCustService.getPaymentSkipCustChkFlag(cust.getCust_id());

        // 2023.11.24 회원 가입시 유입경로(제휴사 코드 상관없이)가 KBB 일 경우 건너띄기 가능
        if (EInfPath.KBB.equals(cust.getInf_path_cd())) {
            skipResult = true;
        }

        modelMap.addAttribute("skipResult", skipResult);

        modelMap.addAttribute("siteDomain", session.getAttribute("siteDomain"));
        modelMap.addAttribute("mid", SellerbotConstant.INICIS_MID);
        modelMap.addAttribute("mKey", SellerbotConstant.INICIS_MKEY);

        modelMap.addAttribute("buyername", cust.getCeo_nm());
        modelMap.addAttribute("buyertel", cust.getCeo_no());
        modelMap.addAttribute("buyeremail", cust.getCust_id());

        // 등록 몰 정보 - 등록 카운트
        ExtJoinCustMallListDTO extJoinCustMallListDTO = smpCustMallService.getJoinCustMall(user.getUsername());
        int cnt = extJoinCustMallListDTO.getExtReCustMallCntDTO().getAll_cnt()
                - extJoinCustMallListDTO.getExtReCustMallCntDTO().getDel_cnt();
        modelMap.addAttribute("mallCnt", cnt);

        // 제휴 별 상품을 구분하기 위해 파라미터 셋팅
        String infSiteCd = cust.getInf_site_cd() != null ? cust.getInf_site_cd().name() : "";
        String custSeqNo = String.valueOf(cust.getCust_seq_no());

        boolean promoEligible = smpCustService.getCouponIssuanceChk(custSeqNo);
        // 2023.10.30 프로모션 랜딩 페이지 통해서 들어오는 파라미터 값 정의(mgmtCd, goodTypCd)
        String mgmtCd = request.getParameter("mgmtCd");
        if (mgmtCd != null) {
            session.setAttribute("mgmtCd", mgmtCd);
        }
        String sessionMgmtCd = (String) session.getAttribute("mgmtCd");
        String goodTypCd = request.getParameter("goodTypCd");

        String promAlertYn = "N";
        // 팝업 노출
        if (!promoEligible && sessionMgmtCd != null) {
            promAlertYn = "Y";
        }

        modelMap.addAttribute("promAlertYn", promAlertYn);
        modelMap.addAttribute("promoEligible", promoEligible);

        // 상품 정보
        // 기존 로니봇, 파이봇은 리스트에 안뜨게 하려고 코드 분기처리
        List<ExtGoodsDTO> goodsList = goodsService.getGoodsList(infSiteCd, custSeqNo); // 파라미터에 ("제휴구분코드",
                                                                                       // "cust_seq_no") 값을 넣어 넘김
        goodsList = goodsList.stream().filter(good -> {
            if (good.getGOODS_TYP_CD().equals("RNB") || good.getGOODS_TYP_CD().equals("PYB")) {
                return false;
            } else {
                if (good.getSALE_MALL_REG_ID_PSB_CNT() == 0) {
                    return true;
                } else {
                    if (good.getSALE_MALL_REG_ID_PSB_CNT() < cnt) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }).collect(Collectors.toList());

        modelMap.addAttribute("goodsList", goodsList);

        // 회원가입시 선택한 상품 정보(선택 정보 기간 : 2주일로 정의)
        ExtSelectGoodsDTO goodsHis = goodsService.getGoodsHis(cust.getCust_id());

        // 2023.10.30 프로모션 랜딩 페이지에서 선택 한 상품이 체크가 되도록 처리
        if (!StringUtils.isEmpty(goodTypCd)) {
            for (ExtGoodsDTO good : goodsList) {
                if (good.getGOODS_TYP_CD().equals(goodTypCd)) {
                    goodsHis.setGoodsSeqNo(good.getGOODS_SEQ_NO().toString());
                    goodsHis.setGoodsOptSeqNo(good.getGOODS_OPT_SEQ_NO().toString());
                    goodsHis.setGoodsTypCd(good.getGOODS_TYP_CD());
                }
            }
        }
        modelMap.addAttribute("goodsHis", goodsHis);

        if (device.isNormal()) {
            modelMap.addAttribute("DeviceType", "PC");
        } else {
            modelMap.addAttribute("DeviceType", "Other");
        }
        return "sub/memberRenewal/join_renewal_step6";
    }

    /**
     * 결제수단 등록 건너띄기
     * 
     * @param request
     * @return
     */
    @PostMapping("/sub/member/step6/skipStep")
    public ResponseEntity<String> skipStep(HttpServletRequest request, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        
        User user = (User) auth.getPrincipal();

        String userName = user.getUsername();
        ExtCustDTO cust = smpCustService.getCust(userName);
        
        paymentService.paymentPromotion(cust.getCust_id(), "FREE", "", "", "LOANJOFR");
        session.setAttribute("cust", smpCustService.getCust(cust.getCust_id()));

        return ResponseEntity.ok("");
    }

    /**
     * 정산계좌 등록(step7)
     * 
     * @param modelMap
     * @param session
     * @return
     */
    @GetMapping("/sub/member/step7")
    public String getJoinRenewalStep7(ModelMap modelMap, HttpSession session, HttpServletRequest request)
            throws Exception {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        User user = (User) auth.getPrincipal();

        ExtCusAcctDTO extCusAcctDTO = smpAccountService.getCustAcctList(user.getUsername());
        if (Objects.nonNull(extCusAcctDTO) && extCusAcctDTO.getAll_acct_cnt() > 0) {
            return "redirect:" + redirectBase + "/";
        }

        // 결제 정보 리턴값
        if (map != null) {
            modelMap.put("goodsInfo", map.get("goodsInfo"));
            modelMap.put("cardNum", map.get("cardNum"));
            modelMap.put("successYN", map.get("successYN"));
            modelMap.put("goodName", map.get("goodName"));
        }

        // Cafe24 고객 여부
        ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");
        String cafe24_usr_yn = "N";
        if (Objects.nonNull(cust.getInf_site_cd()) && cust.getInf_site_cd().equals(EInfPath.CF24)) {
            cafe24_usr_yn = "Y";
        }
        modelMap.addAttribute("cafe24_usr_yn", cafe24_usr_yn);

        // 이용중인 서비스 조회
        ExtUsingGoodsInfoDTO usingGoods = mypageService.getPayInfoForUsingGoods(user.getUsername());
        modelMap.addAttribute("addn_goods_req_seq_no", usingGoods.getAddn_goods_req_seq_no());

        // 현재 이용권으로 등록 가능한 수량 조회
        String userName = user.getUsername();
        ExtMaxRegCountResDTO maxRegCountInfo = paymentService.getMaxRegCountInfo(userName);
        modelMap.addAttribute("maxRegCountInfo", maxRegCountInfo);

        // 은행 정보
        List<ExtBankResponseDTO> bankList = smpBankService.getBankList();
        modelMap.addAttribute("bankList", bankList);

        // 통화 코드
        List<ExtCommCdDTO> codeList = smpCommService.getCodeList("A056");
        modelMap.addAttribute("codeList", codeList);

        return "sub/memberRenewal/join_renewal_step7";
    }

    /**
     * 정산계좌 리스트
     * 
     * @param extJoinCustMallListDTO
     * @return
     */
    @GetMapping("/pub/membe/accountList")
    @ResponseBody
    public List<ExtCustAcctListDTO> custMallList(ExtJoinCustMallListDTO extJoinCustMallListDTO) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        String userName = user.getUsername();
        String custTyp = smpCustService.getCustUseLoan(userName);
        ExtCustAcctInfoDTO custAcctInfo = accountService.getCustAcctInfo(userName, custTyp);

        return custAcctInfo.getAcct_list();
    }

    /**
     * 결제 결과 수신 - PC
     * 
     * @param request
     * @param redirectAttr
     * @param session
     * @return
     */
    @PostMapping(value = "/sub/join/payment/payresponse")
    public String paymentResponse(HttpServletRequest request, RedirectAttributes redirectAttr, HttpSession session) {
        boolean success = false;
        String errMsg = "";
        String mode = null;
        ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");

        try {
            if (cust == null)
                throw new Exception("Customer Not Found.");
            request.setCharacterEncoding("UTF-8");

            Map<String, String> paramMap = new Hashtable<String, String>();
            Enumeration<String> elems = request.getParameterNames();
            while (elems.hasMoreElements()) {
                String key = elems.nextElement();
                paramMap.put(key, request.getParameter(key));
            }
            log.info(
                    "(+)=========================================================\n" + "	paramMap : {} - {}\n"
                            + "(-)=========================================================\n",
                    cust.getCust_id(), paramMap);

            if ("0000".equals(paramMap.get("resultCode"))) { // 인증 성공
                String mid = paramMap.get("mid");
                String timestamp = SignatureUtil.getTimestamp();
                String charset = SellerbotConstant.INICIS_CHARSET;
                String format = SellerbotConstant.INICIS_FORMAT;
                String authToken = paramMap.get("authToken");
                String authUrl = paramMap.get("authUrl");
                String netCancelUrl = paramMap.get("netCancelUrl");

                String merchantData = paramMap.get("merchantData").toString(); // 가맹점 관리데이터 수신
                merchantData = StringEscapeUtils.unescapeHtml(merchantData).replace("&quot", "\"");
                log.info("merchantData = {}", merchantData);
                ExtraData extras = new ObjectMapper().readValue(merchantData, ExtraData.class);
                mode = extras.getMode();
                List<Goods> goodsList = extras.getGoodsList();

                Map<String, String> signParam = new HashMap<String, String>();
                signParam.put("authToken", authToken);
                signParam.put("timestamp", timestamp);

                String signature = SignatureUtil.makeSignature(signParam);
                Map<String, String> authMap = new Hashtable<String, String>();
                authMap.put("mid", mid); // 필수
                authMap.put("authToken", authToken); // 필수
                authMap.put("signature", signature); // 필수
                authMap.put("timestamp", timestamp); // 필수
                authMap.put("charset", charset); // default=UTF-8
                authMap.put("format", format); // default=XML

                HttpUtil httpUtil = new HttpUtil();
                try { // 승인 요청
                    String authResultString = httpUtil.processHTTP(authMap, authUrl);
                    String replaced = authResultString.replace(",", "&").replace(":", "=").replace("\"", "")
                            .replace(" ", "").replace("\n", "").replace("}", "").replace("{", "");
                    Map<String, String> resultMap = ParseUtil.parseStringToMap(replaced);

                    log.info(
                            "(+)=========================================================\n"
                                    + "	resultMap : {} - {}\n"
                                    + "(-)=========================================================\n",
                            cust.getCust_id(), resultMap);

                    Map<String, String> secureMap = new HashMap<String, String>();
                    secureMap.put("mid", mid);
                    secureMap.put("tstamp", timestamp);
                    secureMap.put("MOID", resultMap.get("MOID"));
                    secureMap.put("TotPrice", resultMap.get("TotPrice"));

                    String secureSignature = SignatureUtil.makeSignatureAuth(secureMap);
                    String resultCode = resultMap.get("resultCode");
                    String authSignature = resultMap.get("authSignature");

                    if ("0000".equals(resultCode)) { // 승인 성공
                        if (secureSignature.equals(authSignature)) {
                            String tid = resultMap.get("tid");
                            ExtPayInicisReqDTO c = new ExtPayInicisReqDTO();

                            c.setResultCode(resultMap.get("resultCode"));
                            c.setResultMsg(resultMap.get("resultMsg"));
                            c.setPayMethod(resultMap.get("payMethod"));
                            c.setGoodNm(resultMap.get("goodName"));
                            c.setBuyerName(resultMap.get("buyerName"));
                            c.setBuyerTel(resultMap.get("buyerTel"));
                            c.setBuyerEmail(resultMap.get("buyerEmail"));
                            c.setApplTime(resultMap.get("applTime"));
                            c.setCurrency(resultMap.get("currency"));
                            c.setPayDevice(resultMap.get("payDevice"));
                            c.setApplDate(resultMap.get("applDate"));
                            c.setMoid(resultMap.get("MOID"));
                            c.setTid(tid);
                            c.setCardBillKey(resultMap.get("CARD_BillKey"));
                            c.setCardBankCode(resultMap.get("CARD_BankCode"));
                            c.setCardQuota(resultMap.get("CARD_Quota"));
                            c.setCardCode(resultMap.get("CARD_Code"));
                            c.setCardNum(resultMap.get("CARD_Num"));
                            c.setCardInterest(resultMap.get("CARD_Interest"));
                            c.setCustSeqNo(cust.getCust_seq_no());
                            c.setEventNo(extras.getEventNo());

                            redirectAttr.addFlashAttribute("cardNum", resultMap.get("CARD_Num"));
                            redirectAttr.addFlashAttribute("goodName", resultMap.get("goodName"));

                            List<ExtPayInicisReqDTO.Goods> _l = new ArrayList<ExtPayInicisReqDTO.Goods>();
                            if ("register".equals(mode)) {
                                for (Goods g : goodsList) {
                                    ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                                    goods.setGoodsOptSeqNo(g.getO());
                                    goods.setUsePoi(0L);
                                    goods.setGoodNm(URLDecoder.decode(g.getN(), "UTF-8"));
                                    goods.setPrice(g.getP());
                                    // 에러 때문에 임시커밋
                                    // goods.setPeriod(g.getD());
                                    _l.add(goods);
                                }
                                c.setGoodsList(_l);
                                ExtPayInicisRespDTO p = paymentService.postPaymentByInicis(c);
                                success = true;
                                redirectAttr.addFlashAttribute("freeYN", p.getFreeYN());
                                redirectAttr.addFlashAttribute("goodsInfo", p.getGoodsList().get(0));
                                redirectAttr.addFlashAttribute("products", p.getGoodsList());

                            } else if ("change".equals(mode)) {
                                for (Goods g : goodsList) {
                                    ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                                    goods.setGoodsReqSeqNo(g.getR());
                                    goods.setGoodsSeqNo(g.getS());
                                    _l.add(goods);
                                }
                                c.setGoodsList(_l);
                                paymentService.postChangeCard(c);
                                success = true;
                            }

                            //발행 쿠폰 상태코드 해지로 변경
                            paymentService.postCouponStateExpire(c);
                        } else {
                            throw new Exception("데이터 위변조 체크 실패");
                        }
                    } else {
                        errMsg = resultMap.get("resultMsg");
                    }

                } catch (Exception ex) {
                    errMsg = SbfUtils.getJsonValue(ex.getMessage(), "reason");
                    // log.info("===> {}", errMsg);
                    String result = httpUtil.processHTTP(authMap, netCancelUrl);
                    log.info(
                            "(+)=========================================================\n" + "	result : {} - {}\n"
                                    + "(-)=========================================================\n",
                            cust.getCust_id(), result);
                }
            } else {
                log.info("{} - Authentication Failed", cust.getCust_id());
                errMsg = paramMap.get("resultMsg");
            }
        } catch (Exception e) {
            errMsg = SbfUtils.getJsonValue(e.getMessage(), "reason");
        }

        redirectAttr.addFlashAttribute("successYN", success ? "Y" : "N");
        if (!success) {
            redirectAttr.addFlashAttribute("errMsg", errMsg);
            return "redirect:" + redirectBase + "/sub/member/step6";
        }

        session.setAttribute("cust", smpCustService.getCust(cust.getCust_id()));
        custPayInfo.updateSessionInfoAboutPay(session, cust.getCust_id());
        return "redirect:" + redirectBase + "/sub/member/step7";
    }

    /**
     * 결제 결과 수신 - MOBILE
     * 
     * @param request
     * @param redirectAttr
     * @param session
     * @return
     */
    @PostMapping(value = "/sub/join/payment/mobilepayresponse")
    public String mobilePaymentResponse(HttpServletRequest request, RedirectAttributes redirectAttr,
            HttpSession session) {
        boolean success = false;
        String errMsg = "";
        String mode = "register";
        ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");

        try {
            if (cust == null)
                throw new Exception("Customer Not Found.");
            request.setCharacterEncoding("UTF-8");

            Map<String, String> resultMap = new Hashtable<String, String>();
            Enumeration<String> elems = request.getParameterNames();

            while (elems.hasMoreElements()) {
                String key = elems.nextElement();
                resultMap.put(key, request.getParameter(key));
            }

            log.debug(
                    "(+)=========================================================\n" + "	resultMap : {} - {}\n"
                            + "(-)=========================================================\n",
                    cust.getCust_id(), resultMap);

            String p_noti = resultMap.get("p_noti").toString();
            p_noti = StringEscapeUtils.unescapeHtml(p_noti).replace("&quot", "\"");
            ExtraData extras = new ObjectMapper().readValue(p_noti, ExtraData.class);
            mode = extras.getMode();
            List<Goods> goodsList = extras.getGoodsList();

            String resultCode = resultMap.get("resultcode");
            if ("00".equals(resultCode)) {
                String tid = resultMap.get("tid");
                ExtPayInicisReqDTO c = new ExtPayInicisReqDTO();

                c.setResultCode(resultCode + "00");
                c.setResultMsg(resultMap.get("resultmsg"));
                c.setMoid(resultMap.get("orderid"));
                c.setBuyerName(extras.getBuyerName());
                c.setBuyerTel(extras.getBuyerTel());
                c.setBuyerEmail(extras.getBuyerEmail());
                c.setApplTime(resultMap.get("pgauthtime"));
                c.setApplDate(resultMap.get("pgauthdate"));
                c.setTid(tid);
                c.setCardBillKey(resultMap.get("billkey"));
                c.setCardCode(resultMap.get("cardcd"));
                c.setCardNum(resultMap.get("cardno"));
                c.setCardQuota("0");
                c.setCardInterest("0");
                c.setPayDevice("MOBILE");
                c.setCustSeqNo(cust.getCust_seq_no());

                redirectAttr.addFlashAttribute("cardNum", resultMap.get("cardno"));

                List<ExtPayInicisReqDTO.Goods> _l = new ArrayList<ExtPayInicisReqDTO.Goods>();
                if ("register".equals(mode)) {
                    for (Goods g : goodsList) {
                        ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                        goods.setGoodsOptSeqNo(g.getO());
                        goods.setUsePoi(0L);
                        goods.setGoodNm(URLDecoder.decode(g.getN(), "UTF-8"));
                        goods.setPrice(g.getP());
                        // 에러 때문에 임시커밋
                        // goods.setPeriod(g.getD());
                        _l.add(goods);
                    }
                    c.setGoodsList(_l);
                    ExtPayInicisRespDTO p = paymentService.postPaymentByInicis(c);
                    success = true;
                    redirectAttr.addFlashAttribute("freeYN", p.getFreeYN());
                    redirectAttr.addFlashAttribute("goodsInfo", p.getGoodsList().get(0));
                    redirectAttr.addFlashAttribute("products", p.getGoodsList());
                } else if ("change".equals(mode)) {
                    for (Goods g : goodsList) {
                        ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                        goods.setGoodsReqSeqNo(g.getR());
                        _l.add(goods);
                    }
                    c.setGoodsList(_l);
                    paymentService.postChangeCard(c);
                    success = true;
                }

                //발행 쿠폰 상태코드 해지로 변경
                paymentService.postCouponStateExpire(c);
            } else {
                errMsg = resultMap.get("resultMsg");
            }
        } catch (Exception e) {
            errMsg = e.getMessage();
        }

        redirectAttr.addFlashAttribute("successYN", success ? "Y" : "N");
        if (!success) {
            redirectAttr.addFlashAttribute("errMsg", errMsg);
            return "redirect:" + redirectBase + "/sub/member/step6";
        }

        session.setAttribute("cust", smpCustService.getCust(cust.getCust_id()));
        custPayInfo.updateSessionInfoAboutPay(session, cust.getCust_id());
        return "redirect:" + redirectBase + "/sub/member/step7";
    }
}
