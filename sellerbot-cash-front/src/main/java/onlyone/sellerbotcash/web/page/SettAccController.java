package onlyone.sellerbotcash.web.page;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import onlyone.sellerbotcash.service.MngSettAccService;
import onlyone.sellerbotcash.service.SettAccService;
import onlyone.sellerbotcash.service.SmpBannerService;
import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpFinGoodService;
import onlyone.sellerbotcash.service.SmpSettAccScheService;
import onlyone.sellerbotcash.service.TodaySettAccService;
import onlyone.smp.persistent.domain.enums.EStorFileTyp;
import onlyone.smp.service.dto.ExtBannerDTO;
import onlyone.sellerbotcash.web.util.DateUtil;
import onlyone.smp.service.dto.ExtCustMallListDTO;
import onlyone.smp.service.dto.ExtMngSettAccCalTipDTO;
import onlyone.smp.service.dto.ExtSettAccScheDlvDTO;
import onlyone.smp.service.dto.ExtSettAccScheFiveDTO;
import onlyone.smp.service.dto.ExtSettAccScheFourDTO;
import onlyone.smp.service.dto.ExtSettAccScheMallDayDTO;
import onlyone.smp.service.dto.ExtSettAccScheMallInfoDTO;
import onlyone.smp.service.dto.ExtSettAccScheMallTipDTO;
import onlyone.smp.service.dto.ExtSettAccScheMonDTO;
import onlyone.smp.service.dto.ExtSettAccScheOneDTO;
import onlyone.smp.service.dto.ExtSettAccScheThreeDTO;
import onlyone.smp.service.dto.ExtSettAccScheTipDTO;
import onlyone.smp.service.dto.ExtSettAccScheTwoDTO;
import onlyone.smp.service.dto.ExtTodaySettAccMonthlyMallInfoDTO;
import onlyone.smp.service.dto.ExtSettAccScheCalendarDTO;
import onlyone.smp.service.dto.ExtSettAccScheDetailDTO;
import onlyone.smp.service.dto.ExtSettAccScheSummaryDTO;


/**
 * 정산예정금 컨트롤러
 * @author bak
 *
 */
@Controller
public class SettAccController {
	@Autowired private SmpFinGoodService smpFinGoodService;
	@Autowired private SmpSettAccScheService smpSettAccScheService;
	@Autowired private SmpCustMallService smpCustMallService;
	@Autowired private SettAccService settAccService; 
	@Autowired private TodaySettAccService todaySettAccService;
	@Autowired private MngSettAccService mngSettAccService;	
	@Autowired private SmpBannerService smpBannerService;
	
	/**
	 * 정산예정금 한 눈에 보기
	 * 
	 * @param model
	 * @param user
	 * @return
	 */
	@GetMapping("/sub/settAcc/all")
	public String all(ModelMap model, @AuthenticationPrincipal User user) {
		
		ExtSettAccScheSummaryDTO ret = smpSettAccScheService.all (user.getUsername(), DateUtil.getDateTimeNoFormatDay(LocalDate.now()) );

		model.put("mallInfo", ret.getMall());
		model.put("settAccSche", ret.getSettAccScheOne());
		model.put("settAccScheDlv", ret.getSettAccScheDlv());
		model.put("settAccScheMon", ret.getSettAccScheMon());		
		model.put("settAccScheDlvMall", ret.getSettAccScheDlvMall());
		
		List<ExtSettAccScheMallDayDTO> mallList = ret.getSettAccScheDayMall().getSett_acc_pln_mall_day_l().stream().sorted(Comparator.comparing(ExtSettAccScheMallDayDTO::getDay)).collect(Collectors.toList());;
		ret.getSettAccScheDayMall().setSett_acc_pln_mall_day_l(mallList);
		model.put("settAccScheDayMall", ret.getSettAccScheDayMall());		
	
		model.addAttribute("custSettAccScheTipVo" , ret.getCustSettAccScheTipVo());
		model.addAttribute("tipCount" , ret.getTipCount()); 

		if(ret.getCustSettAccScheTipVo().getMall_tip().size()>0) {			
			model.addAttribute("tipFirstVo" , ret.getCustSettAccScheTipVo().getMall_tip().get(0));
			model.addAttribute("tipLastVo" , ret.getCustSettAccScheTipVo().getMall_tip().get(ret.getCustSettAccScheTipVo().getMall_tip().size() - 1));
		}

		// 배너 정보
		List<ExtBannerDTO> bannerDataList = smpBannerService.getBannerData(EStorFileTyp.B1DD);
		model.put("bannerDataList", bannerDataList);
		
		return "sub/settAcc/all";
	}
	
	/**
	 * 정산예정금 상세보기
	 * 
	 * @param model
	 * @param user
	 * @return
	 */
	@GetMapping("/sub/settAcc/detail")
	public String detail(ModelMap model, @AuthenticationPrincipal User user)  throws JsonProcessingException {
		
		ExtSettAccScheDetailDTO ret = smpSettAccScheService.detail(user.getUsername(), DateUtil.getDateTimeNoFormatDay(LocalDate.now()));
		
		model.put("mallInfo", ret.getMall());
		model.put("settAccSche", ret.getSettAccScheOne());
		
		model.addAttribute("opmDataMalList", ret.getSettAccScheDlvMall().getMall());
		model.addAttribute("opmDataList", new ObjectMapper().writeValueAsString(ret.getSettAccScheDlvMall()));
		
		model.addAttribute("nOpMallList", ret.getSettAccScheDayMall().getNOpMallList());
		model.addAttribute("nOpmDataList", new ObjectMapper().writeValueAsString(ret.getSettAccScheDayMall()));
		
		model.addAttribute("custSettAccScheTipVo" , ret.getCustSettAccScheTipVo());
		model.addAttribute("tipCount" , ret.getTipCount()); 
	
		if(ret.getCustSettAccScheTipVo().getMall_tip().size()>0) {			
			model.addAttribute("tipFirstVo" , ret.getCustSettAccScheTipVo().getMall_tip().get(0));
			model.addAttribute("tipLastVo" , ret.getCustSettAccScheTipVo().getMall_tip().get(ret.getCustSettAccScheTipVo().getMall_tip().size() - 1));
		}

		// 배너 정보
		List<ExtBannerDTO> bannerDataList = smpBannerService.getBannerData(EStorFileTyp.B1DD);
		model.put("bannerDataList", bannerDataList);
		
		return "sub/settAcc/detail";
	}
	
	/**
	 * 정산예정금 달력
	 * 
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping("/sub/settAcc/calendar")
	public String calendar(ModelMap model, HttpSession session, Device device, @AuthenticationPrincipal User user) 
			 throws JsonProcessingException {

		ExtSettAccScheCalendarDTO ret = smpSettAccScheService.calandar(user.getUsername(), DateUtil.getDateTimeNoFormatDay(LocalDate.now()));

		ExtSettAccScheFiveDTO custSettAccScheDayMallVo = ret.getSettAccScheDayMall();
		ExtSettAccScheFourDTO custSettAccScheMonVal = ret.getSettAccScheMon();
		ExtTodaySettAccMonthlyMallInfoDTO todaySettAccMonthlyMallInfo = ret.getTodaySettAccMonthlyMallInfo();
		
		if (device.isNormal()) {
			model.addAttribute("DeviceType" , "PC");
        } else {
			model.addAttribute("DeviceType" , "Other");
        }
		
		// 달력 출력용 데이터 산출
		if( custSettAccScheDayMallVo != null || todaySettAccMonthlyMallInfo != null) {
			HashMap<String, Object> custSettAccScheDayMallCalendar = smpSettAccScheService.getCustSettAccScheDayMallCallendar(custSettAccScheDayMallVo, todaySettAccMonthlyMallInfo, DateUtil.getDateTimeNoFormatDay(LocalDate.now()));
			String jsonStrCustSettAccScheDayMallCalendar = new ObjectMapper().writeValueAsString(custSettAccScheDayMallCalendar);
			model.addAttribute("custSettAccScheDayMallCalendar" , jsonStrCustSettAccScheDayMallCalendar);	
		}
		
		// 상세 정보 collection용 데이터 수집 (월별 데이터)
		if( custSettAccScheMonVal != null) {
			HashMap<String, ArrayList<ExtSettAccScheMonDTO>> custSettAccScheDayMallMonth = 
				smpSettAccScheService.getCustSettAccScheDayMallMonth(custSettAccScheMonVal );

			model.addAttribute("custSettAccScheDayMallMonth" , custSettAccScheDayMallMonth);
			model.addAttribute("yearList" , custSettAccScheDayMallMonth.keySet().stream().sorted().collect(Collectors.toList()));
		}
		
		// 상세정보 collection용 데이터 수집 (일별 데이터) 
		if( custSettAccScheDayMallVo != null ) {
			HashMap<String, ArrayList<ExtSettAccScheMallDayDTO>> custSettAccScheDayMallDay = 
				smpSettAccScheService.getCustSettAccScheDayMallDay(custSettAccScheDayMallVo );
						
			model.addAttribute("yyyymmList" , custSettAccScheDayMallDay.keySet().stream().sorted().collect(Collectors.toList()));
			model.addAttribute("custSettAccScheDayMallDay" , custSettAccScheDayMallDay);
		}
		
		model.put("custSettAccScheVo", ret.getSettAccScheOne());
		model.put("mallInfo", ret.getMall());
		
		model.addAttribute("custSettAccScheTipVo" , ret.getCustSettAccScheTipVo());
		model.addAttribute("tipCount" , ret.getTipCount()); 

		if(ret.getCustSettAccScheTipVo().getMall_tip().size()>0) {			
			model.addAttribute("tipFirstVo" , ret.getCustSettAccScheTipVo().getMall_tip().get(0));
			model.addAttribute("tipLastVo" , ret.getCustSettAccScheTipVo().getMall_tip().get(ret.getCustSettAccScheTipVo().getMall_tip().size() - 1));
		}

		// 배너 정보
		List<ExtBannerDTO> bannerDataList = smpBannerService.getBannerData(EStorFileTyp.B1DD);
		model.put("bannerDataList", bannerDataList);
		
		return "sub/settAcc/calendar";
	}	
	
	/**
	 * 정산예정금 > 한 눈에 보기
	 * @param modelMap
	 * @param user
	 * @return
	 */
	@Deprecated
	//@GetMapping("/sub/settAcc/all")
	public String getEventDetail(ModelMap modelMap, @AuthenticationPrincipal User user) {
		// 정산예정금 현황 및 오픈/비오픈 마켓 그래프
		ExtSettAccScheOneDTO settAccScheOne = smpSettAccScheService.getCustSettAccSche(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		// 배송상태별 정산예정금
		ExtSettAccScheTwoDTO settAccScheDlv = smpSettAccScheService.getCustSettAccScheDlv(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		// 월별 정산예정금
		ExtSettAccScheFourDTO settAccScheMon = smpSettAccScheService.getCustSettAccScheMon(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		// 배송상태별 정산예정금 - 모든 몰 표시
		ExtSettAccScheThreeDTO settAccScheDlvMall = smpSettAccScheService.getCustSettAccScheDlvMall(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		// 월별 정산예정금 - 몰별 일자별 데이터
		ExtSettAccScheFiveDTO settAccScheDayMall = smpSettAccScheService.getCustSettAccScheDayMall(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		// 몰 정보
		ExtCustMallListDTO mallInfo = smpCustMallService.getNonDelCustMall(user.getUsername());
		// 배너 정보
		List<ExtBannerDTO> bannerDataList = smpBannerService.getBannerData(EStorFileTyp.B1DD);
		
		List<String> dayMallNameList = new ArrayList<String>();
		List<String> dayMallCdList = new ArrayList<String>();

		if (settAccScheDayMall!=null) {
			List<ExtSettAccScheMallDayDTO> mallList = settAccScheDayMall.getSett_acc_pln_mall_day_l();
			
			for (int i = 0, n = mallList.size(); i < n; i++) {
				ExtSettAccScheMallDayDTO dto = mallList.get(i);
			    if (!dayMallNameList.contains(dto.getMall_cd_nm())) {
			    	dayMallNameList.add(dto.getMall_cd_nm());
			    	dayMallCdList.add(dto.getMall_cd());
			    }
			}
			
			// List<ExtSettAccScheMallDayDTO> mallList = settAccScheDayMall.getSett_acc_pln_mall_day_l();
			// mallList = mallList.stream().sorted(Comparator.comparing(ExtSettAccScheMallDayDTO::getDay)).collect(Collectors.toList());
			Collections.sort(mallList);
			settAccScheDayMall.setSett_acc_pln_mall_day_l(mallList);
		}
		
		// 판매팁 
		ExtSettAccScheTipDTO custSettAccScheTipVo =	smpSettAccScheService.getCustSettAccScheTip(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		modelMap.addAttribute("custSettAccScheTipVo" , custSettAccScheTipVo);

		//미리정산가능금액
		long preSett = smpFinGoodService.getBiggestPre(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		modelMap.addAttribute("preSett", preSett);
		
		// 요약 정보 확인 
		modelMap.addAttribute("tipFirstVo" , null);
		modelMap.addAttribute("tipLastVo" , null);

		if( custSettAccScheTipVo.getMall_tip().size() > 0 ) {
			modelMap.addAttribute("tipFirstVo" , custSettAccScheTipVo.getMall_tip().get(0));
			modelMap.addAttribute("tipLastVo" , custSettAccScheTipVo.getMall_tip().get(custSettAccScheTipVo.getMall_tip().size() - 1));
			
			List<ExtSettAccScheMallTipDTO> list = custSettAccScheTipVo.getMall_tip().stream().filter(dto -> Integer.parseInt(dto.getD_day()) > 89).collect(Collectors.toList());
			modelMap.addAttribute("tipCount" , list.size()); // 3개월 내의 tip 갯수
		}
		
		String[] orders = {"RDY", "DOI", "DON", "CON"};
		// 정렬 변경
		List<ExtSettAccScheDlvDTO> list = new ArrayList<ExtSettAccScheDlvDTO>();
		
		for(String order : orders) {
			for( ExtSettAccScheDlvDTO dto : settAccScheDlv.getDlv() ) {
				if(order.equals(dto.getDvl_sts_cd().name()) ) {
					list.add(dto);
				}
			}
		}
		
		settAccScheDlv.setDlv(list);
		
		modelMap.put("mallInfo",mallInfo);
		modelMap.put("settAccSche", settAccScheOne);
		modelMap.put("settAccScheDlv", settAccScheDlv);
		modelMap.put("settAccScheMon", settAccScheMon);
		modelMap.put("settAccScheDlvMall", settAccScheDlvMall);
		modelMap.put("settAccScheDayMall", settAccScheDayMall);
		modelMap.put("dayMallNameList", dayMallNameList);
		modelMap.put("dayMallCdList", dayMallCdList);
		modelMap.put("bannerDataList", bannerDataList);
		
		return "sub/settAcc/all";
	}

	/**
	 * 정산예정금 > 상세
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws JsonProcessingException
	 */
	@Deprecated
	//@GetMapping("/sub/settAcc/detail")
	public String getPaymentDetail(ModelMap modelMap, HttpSession session) throws JsonProcessingException {
		//log.debug("** {}", session.getClass().getName());

		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication auth = securityContext.getAuthentication();
		User user = (User)auth.getPrincipal();
		
		// 오픈 마켓
		ExtSettAccScheThreeDTO opmDataList = smpSettAccScheService.getCustSettAccScheDlvMallNR(user.getUsername(),DateUtil.getDateString(LocalDate.now()));
		
		if(opmDataList!=null) {
			List<ExtSettAccScheMallInfoDTO> mallList = opmDataList.getMall();
			mallList = mallList.stream().sorted(Comparator.comparing(ExtSettAccScheMallInfoDTO::getMall_cd)).collect(Collectors.toList());
			opmDataList.setMall(mallList);
			
			modelMap.addAttribute("opmDataMalList", opmDataList.getMall());
			modelMap.addAttribute("opmDataList", new ObjectMapper().writeValueAsString(opmDataList));
		} 
		
		// 비오픈 마켓
		ExtSettAccScheFiveDTO nOpmDataList = smpSettAccScheService.getCustSettAccScheDayMallNR(user.getUsername(), DateUtil.getDateString(LocalDate.now()));

		if(nOpmDataList!=null) {
			List<ExtSettAccScheMallDayDTO> mallList = nOpmDataList.getSett_acc_pln_mall_day_l();
			mallList = mallList.stream().sorted(Comparator.comparing(ExtSettAccScheMallDayDTO::getMall_cd)).collect(Collectors.toList());
			nOpmDataList.setSett_acc_pln_mall_day_l(mallList);
			
			// 중복 제거
			ArrayList<ExtSettAccScheMallDayDTO> nOpMallList = new ArrayList<ExtSettAccScheMallDayDTO>();
			long beforeMallSeq = 99999999L;
			for(ExtSettAccScheMallDayDTO mallDto : mallList) {
				if(beforeMallSeq != mallDto.getCust_mall_seq_no()) {
					nOpMallList.add(mallDto);
				}
				beforeMallSeq = mallDto.getCust_mall_seq_no();
			}
			
			modelMap.addAttribute("nOpMallList", nOpMallList);
			modelMap.addAttribute("nOpmDataList", new ObjectMapper().writeValueAsString(nOpmDataList));
		}
		
		// 정산예정금 현황
		ExtSettAccScheOneDTO settAccScheOne = smpSettAccScheService.getCustSettAccSche(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		modelMap.put("settAccSche", settAccScheOne);
		
		//미리정산가능금액
		long preSett = smpFinGoodService.getBiggestPre(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		modelMap.addAttribute("preSett", preSett);
		
		// 판매팁 
		ExtSettAccScheTipDTO custSettAccScheTipVo =	smpSettAccScheService.getCustSettAccScheTip(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		modelMap.addAttribute("custSettAccScheTipVo" , custSettAccScheTipVo);
		
		// 요약 정보 확인 
		modelMap.addAttribute("tipFirstVo" , null);
		modelMap.addAttribute("tipLastVo" , null);

		if( custSettAccScheTipVo.getMall_tip().size() > 0 ) {
			modelMap.addAttribute("tipFirstVo" , custSettAccScheTipVo.getMall_tip().get(0));
			modelMap.addAttribute("tipLastVo" , custSettAccScheTipVo.getMall_tip().get(custSettAccScheTipVo.getMall_tip().size() - 1));
		
			List<ExtSettAccScheMallTipDTO> list = custSettAccScheTipVo.getMall_tip().stream().filter(dto -> Integer.parseInt(dto.getD_day()) > 89).collect(Collectors.toList());
			modelMap.addAttribute("tipCount" , list.size()); // 3개월 내의 tip 갯수
		}
		
		ExtCustMallListDTO mallInfo = smpCustMallService.getNonDelCustMall(user.getUsername());
		modelMap.put("mallInfo", mallInfo);

		// 배너 정보
		List<ExtBannerDTO> bannerDataList = smpBannerService.getBannerData(EStorFileTyp.B1DD);
		modelMap.put("bannerDataList", bannerDataList);
		
		return "sub/settAcc/detail";
	}
	
	/**
	 * 정산예정금 > 상세 > 몰 상세 정보
	 * @param custMallSeqNo
	 * @return
	 */
	@GetMapping("/sub/settAcc/detail/ajax")
	@ResponseBody
	public Object detailPageAjaxReturner(@RequestParam String custMallSeqNo) {
		return smpSettAccScheService.getCustSettAccScheDetail(custMallSeqNo,DateUtil.getDateString(LocalDate.now()));
	}
	
	/**
	 * 정산예정금 > 상세 > 몰 상세 컬럼 정보
	 * @param mallCd
	 * @return
	 */
	@GetMapping("/sub/settAcc/detail/ajax/cul")
	@ResponseBody
	public Object detailPageAjaxCulReturner(@RequestParam String mallCd) {
		return smpSettAccScheService.getCustSettAccScheDetailCul(mallCd);
	}
	
	/**
	 * 정산예정금 > 상세 > 엑셀 다운로드
	 * @param mallCd
	 * @return
	 */
	@GetMapping("/sub/settAcc/detail/excel")
	public void excelDownload( HttpServletResponse res, @AuthenticationPrincipal User user) {
		settAccService.excelDownload(user, res);
	}
	
	/**
	 * 정산예정금 > 달력
	 * @param modelMap
	 * @param session
	 * @return
	 * @throws JsonProcessingException
	 */
	@Deprecated
	//@GetMapping("/sub/settAcc/calendar")
	public String getPaymentCalendar(ModelMap modelMap, HttpSession session, Device device) throws JsonProcessingException {
		//log.debug("** {}", session.getClass().getName());

		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication auth = securityContext.getAuthentication();
		User user= (User)auth.getPrincipal();

		if (device.isNormal()) {
			modelMap.addAttribute("DeviceType" , "PC");
        } else {
			modelMap.addAttribute("DeviceType" , "Other");
        }
		
		//미리정산가능금액
		long preSett = smpFinGoodService.getBiggestPre(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		modelMap.addAttribute("preSett", preSett);
		// 현재 일자 yyyy-mm-dd 로 데이터 산출
		SimpleDateFormat  formatter_now = new SimpleDateFormat("yyyy-MM-dd");
		String now_date = formatter_now.format(new Date());

		// 고객 정산 예정 정보 조회
		ExtSettAccScheOneDTO custSettAccScheVo =	smpSettAccScheService.getCustSettAccSche(user.getUsername(), now_date);
		modelMap.addAttribute("custSettAccScheVo" , custSettAccScheVo);

		// 월별 정산 예정금액 
		ExtSettAccScheFourDTO custSettAccScheMonVo =	smpSettAccScheService.getCustSettAccScheMon(user.getUsername(), now_date);
		ExtSettAccScheFourDTO custSettAccScheMonVal = custSettAccScheMonVo;

		//  판매몰 일자별 정산 예정금
		ExtSettAccScheFiveDTO custSettAccScheDayMallVo =	smpSettAccScheService.getCustSettAccScheDayMall(user.getUsername(), now_date);

		// 오늘 정산금 조회
		ExtTodaySettAccMonthlyMallInfoDTO todaySettAccMonthlyMallInfo = todaySettAccService.getTodaySettAccMonthlyMallInfo(user.getUsername(), DateUtil.getYearAndMonth(LocalDate.now()));

		if( custSettAccScheDayMallVo != null || todaySettAccMonthlyMallInfo != null) {
			// 달력 출력용 데이터 산출
			HashMap<String, Object> custSettAccScheDayMallCalendar = smpSettAccScheService.getCustSettAccScheDayMallCallendar(custSettAccScheDayMallVo, todaySettAccMonthlyMallInfo, DateUtil.getDateTimeNoFormatDay(LocalDate.now()));
			// modelMap.addAttribute("custSettAccScheDayMallCalendar" , custSettAccScheDayMallCalendar);
			String jsonStrCustSettAccScheDayMallCalendar = new ObjectMapper().writeValueAsString(custSettAccScheDayMallCalendar);
			modelMap.addAttribute("custSettAccScheDayMallCalendar" , jsonStrCustSettAccScheDayMallCalendar);	
		}
		
		if( custSettAccScheMonVal != null) {
			// 상세 정보 collection용 데이터 수집 (월별 데이터)
			HashMap<String, ArrayList<ExtSettAccScheMonDTO>> custSettAccScheDayMallMonth = smpSettAccScheService.getCustSettAccScheDayMallMonth(custSettAccScheMonVal );
			modelMap.addAttribute("custSettAccScheDayMallMonth" , custSettAccScheDayMallMonth);
			ArrayList<String> yearList = new ArrayList<String>();
			Set<String> set = custSettAccScheDayMallMonth.keySet();
			Iterator<String> iterator = set.iterator();
			while (iterator.hasNext()) {
				String key = (String)iterator.next();
				yearList.add(key);
			}
			
			Collections.sort(yearList);
			modelMap.addAttribute("yearList" , yearList);
		}
		
		if( custSettAccScheDayMallVo != null ) {
			// 상세정보 collection용 데이터 수집 (일별 데이터) 
			HashMap<String, ArrayList<ExtSettAccScheMallDayDTO>> custSettAccScheDayMallDay = smpSettAccScheService.getCustSettAccScheDayMallDay(custSettAccScheDayMallVo );
			ArrayList<String> yyyymmList = new ArrayList<String>();
			Set<String> set = custSettAccScheDayMallDay.keySet();
			Iterator<String> iterator = set.iterator();
			while (iterator.hasNext()) {
				String key = (String)iterator.next();
				yyyymmList.add(key);
			}
			
			Collections.sort(yyyymmList);
			modelMap.addAttribute("yyyymmList" , yyyymmList);
			modelMap.addAttribute("custSettAccScheDayMallDay" , custSettAccScheDayMallDay);
		}
		
		// 판매팁
		ExtSettAccScheTipDTO custSettAccScheTipVo =	smpSettAccScheService.getCustSettAccScheTip(user.getUsername(), DateUtil.getDateString(LocalDate.now()));

		// 요약 정보 확인 
		modelMap.addAttribute("tipFirstVo" , null);
		modelMap.addAttribute("tipLastVo" , null);

		if( custSettAccScheTipVo.getMall_tip().size() > 0 ) {
			modelMap.addAttribute("tipFirstVo" , custSettAccScheTipVo.getMall_tip().get(0));
			modelMap.addAttribute("tipLastVo" , custSettAccScheTipVo.getMall_tip().get(custSettAccScheTipVo.getMall_tip().size() - 1));
		}
		
		// 전체 / 정상 / 오류 표시
		ExtCustMallListDTO mallInfo = smpCustMallService.getNonDelCustMall(user.getUsername());
		modelMap.put("mallInfo", mallInfo);

		// 배너 정보
		List<ExtBannerDTO> bannerDataList = smpBannerService.getBannerData(EStorFileTyp.B1DD);
		modelMap.put("bannerDataList", bannerDataList);
		 
		return "sub/settAcc/calendar";
	}
	
	/**
	 * 정산예정금 달력 > 정산예정금 관리팁
	 * @param mall_cd
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping("/sub/settAcc/calendar/tip")
	@ResponseBody
	public ExtMngSettAccCalTipDTO getSettAccTipInfo(String mall_cd, int page, int size, @AuthenticationPrincipal User user) throws JsonProcessingException {
		String userName = user.getUsername();
		String today = DateUtil.getDateString(LocalDate.now());
		return mngSettAccService.getSettAccTipInfo(userName, today, mall_cd, page, size);
	}
	
}