package onlyone.sellerbotcash.web.page;

import java.util.ArrayList;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hazelcast.core.IMap;

import org.apache.commons.lang.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.config.ApplicationProperties;
import onlyone.sellerbotcash.security.ServiceSiteAuthenticationToken;
import onlyone.sellerbotcash.service.ShopByService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ShopByTokenDTO;
import onlyone.smp.service.shopby.dto.req.AuthMeReqDTO;
import onlyone.smp.service.shopby.dto.req.LongLivedReqDTO;
import onlyone.smp.service.shopby.dto.res.AuthMeResDTO;
import onlyone.smp.service.shopby.dto.res.LongLivedResDTO;

import static onlyone.sellerbotcash.config.SellerbotConstant.GRANTEDAUTH;

@Slf4j
@Controller
@RequestMapping("/shby")
public class ShopByController {

    @Autowired
    private ApplicationProperties appProperties;

    @Value("${internal.redirect.base:}")
    private String redirectBase;

    @Autowired
    private SmpCustService smpCustService;

    @Autowired
    private ShopByService shopByService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Resource(name = "nhnc-mall-id")
    private IMap<String, String> nhncMallIdStore;

    /**
     * SHOP BY 앱에서 첫 진입
     * 
     * @param httpResponse
     * @param request
     * @param app
     */
    @RequestMapping("/{appName}")
    @ResponseBody
    public void entry(HttpServletResponse httpResponse, HttpServletRequest request,
            @PathVariable("appName") String app) {
        HttpSession session = request.getSession();
        String state = UUID.randomUUID().toString().replace("-", "");
        String code = request.getParameter("code");
        String token = "";

        log.info("app={}, state={}, code={}, ", app, state, code);

        /**
         * 1. 장기 토큰 발급
         */
        LongLivedReqDTO longLivedReqDTO = new LongLivedReqDTO();
        //헤더
        longLivedReqDTO.setAccept("application/json");
        longLivedReqDTO.setVersion("1.0");
        longLivedReqDTO.setContentType("application/json");
        //바디
        longLivedReqDTO.setClientId(appProperties.getShby().get(app).getClientId());
        longLivedReqDTO.setClientSecret(appProperties.getShby().get(app).getClientSecret());
        longLivedReqDTO.setCode(code);
        longLivedReqDTO.setGrantType("authorization_code");
        longLivedReqDTO.setRedirectUri(appProperties.getShby().get(app).getRedirectUri());

        try {
            LongLivedResDTO longLivedResDTO = shopByService.longLived(longLivedReqDTO);

            if(StringUtils.isNotEmpty(longLivedResDTO.getAccessToken())){
                token = longLivedResDTO.getAccessToken();
            }else{
                log.info(longLivedResDTO.getMessage());
            }
        } catch (Exception e) {
            log.error("토큰 발급 에러발생 :: " + e.toString());
            e.printStackTrace();
        }

        /**
         * 2. 샵 정보 조회
         */
        if (StringUtils.isNotEmpty(token)) {
            AuthMeReqDTO authMeReqDTO = new AuthMeReqDTO();
            //헤더
            authMeReqDTO.setAccept("application/json");
            authMeReqDTO.setSystemkey(appProperties.getShby().get(app).getClientId());
            authMeReqDTO.setVersion("1.0");
            authMeReqDTO.setAuthorization(token);

            try {
                AuthMeResDTO authMeResDTO = shopByService.authMe(authMeReqDTO);

                log.info("샵 정보 조회 response :: " + authMeResDTO);

                String uMallId = authMeResDTO.getMall().getMallId();
                String uClientId = authMeResDTO.getMall().getClientId();

                if (StringUtils.isNotEmpty(uMallId)) {
                    ShopByTokenDTO shopByTokenDTO = new ShopByTokenDTO();
                    shopByTokenDTO.setAppName(app);
                    shopByTokenDTO.setMallId(uMallId);
                    shopByTokenDTO.setLongToken(token);
                    shopByTokenDTO.setClientId(uClientId);

                    boolean updated = shopByService.updateToken(shopByTokenDTO);
                    log.info("shopby launch updated={}", updated);
                    if (updated) {
                        session.setAttribute("siteId", uMallId);

                        httpResponse.sendRedirect("/shby/oauth/" + app);
                    } else {
                        log.info("shopby Failed to update token!!!");
                    }
                }
            } catch (Exception e) {
                log.error("스토어정보 조회 에러발생 :: " + e.toString());
                e.printStackTrace();
            }
        } else {
            log.info("토큰 없음.");
        }
    }

    /**
     * SHOP BY 브릿지 페이지
     * 
     * @param request
     * @param response
     * @param modelMap
     * @param appName
     * @return
     */
    @GetMapping("/oauth/{appName}")
    public String oAuth(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap,
            @PathVariable("appName") String appName) {
        int status = response.getStatus();
        if (status == HttpStatus.OK.value()) {
            log.info("app={} ", appName);

            modelMap.put("app", appName);
        }
        return "shby/entry";
    }

    /**
     * SHOP BY 회원가입/로그인
     * 
     * @param request
     * @return
     */
    @RequestMapping("/launch")
    public String service(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String appName = request.getParameter("app");
        String mallId = "";

        if (session.getAttribute("siteId") != null) {
            mallId = session.getAttribute("siteId").toString();
        } else {
            return "shby/alert";
        }

        log.info("app={}, mallId={}", appName, mallId);

        ExtCustDTO cust = smpCustService.getCustOnSite(EInfPath.SHBY, mallId);
        if (cust != null) {
            User user = new User(cust.getCust_id(), "", new ArrayList<>());
            ServiceSiteAuthenticationToken token = new ServiceSiteAuthenticationToken(user, "", GRANTEDAUTH);
            request.getSession();
            token.setDetails(new WebAuthenticationDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(token));
        } else {
            session.setAttribute("siteCd", EInfPath.SHBY.name());
            return "redirect:" + redirectBase + "/pub/member/step1";
        }

        return "redirect:" + redirectBase + "/";
    }
}
