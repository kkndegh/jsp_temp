package onlyone.sellerbotcash.web.page;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import onlyone.sellerbotcash.service.SmpBannerService;
import onlyone.sellerbotcash.service.SmpNoticeService;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.sellerbotcash.web.vm.Pagination;
import onlyone.smp.persistent.domain.enums.EStorFileTyp;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtBannerDTO;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtEventDTO;
import onlyone.smp.service.dto.ExtFaqDTO;
import onlyone.smp.service.dto.ExtNotiDTO;

@Controller
public class NoticeController {

	@Autowired
	SmpNoticeService smpNoticeService;

	@Autowired
	SmpBannerService smpBannerService;
	
	@GetMapping("/pub/cs/notice")
	public String getNoticeList(ModelMap modelMap, @RequestParam(required=false) String noti_seq_no, @RequestParam(required=false) String title, @RequestParam(defaultValue="0") int page) {
		int queryPage = page > 0 ? page - 1 : 0; 
		ListData<ExtNotiDTO> noticeList = smpNoticeService.getNotiList(title, queryPage);

		Pagination pagingDto = new Pagination();
		pagingDto.setBlockSize(5);
		pagingDto.setPageNo(noticeList.getPage() + 1);
		pagingDto.setTotalCount(noticeList.getTotalCount());
		modelMap.addAttribute("title", title);
		modelMap.addAttribute("paging" , pagingDto);
		modelMap.addAttribute("noticeList" , noticeList);
		modelMap.addAttribute("noti_seq_no" , noti_seq_no);

		// 배너 정보(판매몰관리우측)
		List<ExtBannerDTO> bannerB3DDDataList = smpBannerService.getBannerData(EStorFileTyp.B3DD);
		modelMap.put("bannerB3DDDataList", bannerB3DDDataList);

		return "sub/notice/notice";
	}

	@PostMapping("/pub/cs/viewCountingNoti")
	@ResponseBody
	public void updateViewCount(@RequestParam("notiSeqNo") String notiSeqNo) {
		smpNoticeService.updateViewCountForNoti(notiSeqNo);
	}
	
	@GetMapping("/pub/cs/event")
	public String getEventList(ModelMap modelMap, @RequestParam(required=false, defaultValue = "ALL") String event_sts_cd, @RequestParam(defaultValue="0") int page) {
		int queryPage = page > 0 ? page - 1 : 0; 
		ListData<ExtEventDTO> eventList = smpNoticeService.getEventList(event_sts_cd, queryPage);
		Pagination pagingDto = new Pagination();
		pagingDto.setBlockSize(5);
		pagingDto.setPageNo(eventList.getPage() + 1);
		pagingDto.setTotalCount(eventList.getTotalCount());
		modelMap.addAttribute("paging" , pagingDto);
		modelMap.addAttribute("eventList" , eventList);
		modelMap.addAttribute("event_sts_cd" , event_sts_cd);
		
		return "sub/notice/event";
	}
	
	@GetMapping("/pub/cs/event_detail")
	public String getEventDetail(ModelMap modelMap, @RequestParam("event_seq_no")int event_seq_no) {
		
		ExtEventDTO eventDetail = smpNoticeService.getEventDetail(event_seq_no);
		eventDetail.setEvent_cont( StringEscapeUtils.unescapeHtml(eventDetail.getEvent_cont()) );
		// eventDetail.setEvent_cont( StringEscapeUtils.unescapeHtml(eventDetail.getEvent_cont().replaceAll("\"", "\\\"").replaceAll("\n", "</br>") ));
		modelMap.addAttribute("eventDetail" , eventDetail);
		
		return "sub/notice/event_detail";
	}
	
	@GetMapping("/pub/cs/faq")
	public String getFaqList(ModelMap modelMap, @RequestParam(required=false) String title, @RequestParam(required=false) String faq_typ_cd, @RequestParam(defaultValue="0") int page) {
		int queryPage = page > 0 ? page - 1 : 0; 
		ListData<ExtFaqDTO> faqList = smpNoticeService.getFaqList(title, faq_typ_cd, queryPage);
		List<ExtCommCdDTO> codeList = smpNoticeService.getFaqCodeList()
											.stream()
											.filter(item -> EYesOrNo.Y == item.getUse_yn())
											.sorted(Comparator.comparing(ExtCommCdDTO::getCd_descp))
											.collect(Collectors.toList());
		
		Pagination pagingDto = new Pagination();
		pagingDto.setBlockSize(5);
		pagingDto.setPageNo(faqList.getPage() + 1);
		pagingDto.setTotalCount(faqList.getTotalCount());
		modelMap.addAttribute("title", title);
		modelMap.addAttribute("paging" , pagingDto);
		modelMap.addAttribute("faqList" , faqList);
		modelMap.addAttribute("codeList" , codeList);
		modelMap.addAttribute("faq_typ_cd" , faq_typ_cd);
		return "sub/cs/faq";
	}
	
	@GetMapping("/pub/cs/ask")
	public String ask(ModelMap modelMap) {
		return "sub/cs/ask";
	}
}
