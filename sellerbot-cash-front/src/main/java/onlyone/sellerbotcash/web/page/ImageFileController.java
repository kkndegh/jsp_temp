package onlyone.sellerbotcash.web.page;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.HandlerMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ImageFileController {
	@Value("${image.uploadDir}") private String uploadPath ;
    @Autowired private ServletContext servletContext;
	
	@GetMapping("/imagefile/**")
	public ResponseEntity<InputStreamResource> getImageFile(HttpServletRequest request) throws Exception {
		InputStreamResource resource;
		String restOfTheUrl = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		restOfTheUrl = restOfTheUrl.substring(10);
		log.debug("imagefile {}", restOfTheUrl);
		MediaType mediaType;
        try {
        	String mineType = servletContext.getMimeType(restOfTheUrl);
            mediaType = MediaType.parseMediaType(mineType);
        } catch (Exception e) {
            mediaType= MediaType.APPLICATION_OCTET_STREAM;
        }
        
		File file = new File(uploadPath + restOfTheUrl);
		String filename = file.getName();
		if(!file.exists()) {
			filename = "image-not-found.png";
			URL url = servletContext.getResource("WEB-INF/defaultImage/image-not-found.png");
			log.info(url.getFile());
			file = new File(url.getFile());
		}
		
		resource = new InputStreamResource(new FileInputStream(file));
		return ResponseEntity.ok()
                // Content-Disposition
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + filename)
                // Content-Type
                .contentType(mediaType)
                // Contet-Length
                .contentLength(file.length()) //
                .body(resource);
	}
}
