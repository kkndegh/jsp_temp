package onlyone.sellerbotcash.web.page;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import onlyone.sellerbotcash.service.QrCodeFileService;

// import lombok.extern.slf4j.Slf4j;

// @Slf4j
@Controller
public class QrCodeFileController {

	@Autowired
	private QrCodeFileService qrCodeFileService;

	@RequestMapping(value="/qrcode/image/key/extraction", method=RequestMethod.POST)
	public ResponseEntity<String> getQrCodeKeyExtraction(MultipartFile qrcode_image_file) {

		String qrCodeKey = "";
		try {
			qrCodeKey = qrCodeFileService.getQrCodeKeyExtraction(qrcode_image_file);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("ERROR");
		}
		
		if (StringUtils.isBlank(qrCodeKey)) {
			return ResponseEntity.badRequest().body("ERROR");
		}
		
		return ResponseEntity.ok(qrCodeKey);
	}

}
