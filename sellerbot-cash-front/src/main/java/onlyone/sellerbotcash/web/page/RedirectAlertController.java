package onlyone.sellerbotcash.web.page;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RedirectAlertController {
	 
	@RequestMapping("/redirect/alert")
    public String handleError(HttpServletRequest request) {
	    
	    return "redirect/alert";
    }
}
