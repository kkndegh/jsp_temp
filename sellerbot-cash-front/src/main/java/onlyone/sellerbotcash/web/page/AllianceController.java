package onlyone.sellerbotcash.web.page;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.service.SmpAllianceService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.web.util.DateUtil;
import onlyone.smp.service.dto.ExtCustDTO;

@Controller
public class AllianceController {
	
	@Autowired private SmpAllianceService smpAllianceService;
	
	// @RequestMapping(value="/alliance/index", method=RequestMethod.GET)
	// public String index(ModelMap modelMap,HttpServletRequest request) {		
	// 	return "alliance/index";	
	// }
	
	@PostMapping(value="/alliance/getAccessToken")
	public ResponseEntity<ExtCustDTO> getAccessToken(@RequestBody ExtCustDTO custDto,HttpServletRequest request, HttpSession session) {
		String psvcId = request.getHeader("svc_id");
		String psvcCertKey = request.getHeader("svc_cert_key");

		return ResponseEntity.ok(smpAllianceService.getAccessToken(custDto, psvcId, psvcCertKey));
	}

	@Autowired private SmpCustService smpCustService;
	@PostMapping(value="/alliance/login/auto")
	public String loginAuto(ModelMap modelMap, HttpServletRequest request,HttpServletResponse response,Device device) throws IOException, ServletException {
		String token = "";
		String rtnUrl = "main";
		
		String bearerToken = request.getHeader("access_token");
		String psvcId = request.getHeader("svc_id");
		String psvcCertKey = request.getHeader("svc_cert_key");
	    if (bearerToken != null && bearerToken.startsWith("Bearer ")) {	  
	    	token = bearerToken.substring(7);
	    }
		
	    ExtCustDTO rstDto = smpAllianceService.tokenCheck(token, psvcId, psvcCertKey);
	    User user = new User(rstDto.getCust_id(), "",  SellerbotConstant.GRANTEDAUTH);

	    Authentication authentication = new UsernamePasswordAuthenticationToken(user, "", SellerbotConstant.GRANTEDAUTH);
    	
        SecurityContext securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(authentication);
        SecurityContextHolder.setContext(securityContext);
        request.getSession().setAttribute (HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
		request.getSession().setAttribute ("cust", smpCustService.getCust(rstDto.getCust_id()));
		
		boolean isExternalPopUp = true;
		if (!device.isNormal()) { // MOBILE
			isExternalPopUp = false;
		}

		try {
			if (DateUtil.getCompreBeforeNowDateToEndDate("2021-02-27")) {
				isExternalPopUp = false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		modelMap.put("isExternalPopUp", isExternalPopUp);
        
		return rtnUrl;
	}

	@PostMapping(value="/alliance/reissue/accessToken")
	public ResponseEntity<ExtCustDTO> reissueAccessToken(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		String token = "";
	    String bearerToken = request.getHeader("refresh_token");
	    if (bearerToken != null && bearerToken.startsWith("Bearer ")) {	  
	    	token = bearerToken.substring(7);
		}		
		String psvcId = request.getHeader("svc_id");
		String psvcCertKey = request.getHeader("svc_cert_key");

	    ExtCustDTO rstDto = smpAllianceService.reissueAccessToken(token, psvcId, psvcCertKey);		
		return ResponseEntity.ok(rstDto);
	}

	@PostMapping(value="/alliance/reissue/refreshToken")
	public ResponseEntity<ExtCustDTO> reissueRefreshToken(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		String token = "";
	    String bearerToken = request.getHeader("refresh_token");
	    if (bearerToken != null && bearerToken.startsWith("Bearer ")) {	  
	    	token = bearerToken.substring(7);
		}
		String psvcId = request.getHeader("svc_id");
		String psvcCertKey = request.getHeader("svc_cert_key");
	    ExtCustDTO rstDto = smpAllianceService.reissueRefreshToken(token, psvcId, psvcCertKey);
		return ResponseEntity.ok(rstDto);
	}	
	
}
