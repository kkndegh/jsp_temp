/**
 * @author: kenny
 */
package onlyone.sellerbotcash.web.page;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hazelcast.core.IMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import onlyone.sellerbotcash.config.ApplicationProperties;
import onlyone.sellerbotcash.security.ServiceSiteAuthenticationToken;
import onlyone.sellerbotcash.service.Cafe24Service;
import onlyone.sellerbotcash.service.SbfUtils;
import onlyone.sellerbotcash.service.SmpAuthService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.sellerbotcash.web.util.SecurityAES256;
import onlyone.sellerbotcash.web.vm.JoinCustInfo;
import onlyone.sellerbotcash.web.vm.JoinTerms;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.persistent.domain.enums.StatusEnum;
import onlyone.smp.service.dto.Cafe24MessageDTO;
import onlyone.smp.service.dto.Cafe24StoreDTO;
import onlyone.smp.service.dto.ExtAgreeTermsReqDTO;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtCustPackageDTO;
import onlyone.smp.service.dto.ExtSvcTermsDTO;

import static onlyone.sellerbotcash.config.SellerbotConstant.GRANTEDAUTH;

@Slf4j
@Controller
@RequestMapping("/cafe24")
public class Cafe24Controller {
    @Autowired
    private ApplicationProperties appProperties;
    @Value("${internal.redirect.base:}") 
    private String redirectBase;
    @Autowired 
    private SmpCustService smpCustService;
    @Autowired 
    private Cafe24Service cafe24Service;
    @Autowired 
    private AuthenticationManager authenticationManager;
    @Resource(name="cafe24-mall-id") 
    private IMap<String, String> cafe24MallIdStore;
    @Autowired
    private SmpJoinService smpJoinService;
    @Autowired
    private SmpAuthService smpAuthService;

    @RequestMapping("/{appName}")
    public String entry(HttpServletRequest request, @PathVariable("appName") String app) {
        String state = UUID.randomUUID().toString().replace("-", "");
        String mallId = request.getParameter("mall_id");

        log.info("app={}, state={}, mallId={}, ", app, state, mallId);

        if (state != null && mallId != null)
             cafe24MallIdStore.put(state, mallId);

        StringBuffer url = new StringBuffer();
        url.append("https://").append(mallId).append(".cafe24api.com/api/v2/oauth/authorize");
        url.append("?response_type=code");
        url.append("&client_id=").append(appProperties.getCafe24().get(app).getClientId());
        url.append("&state=").append(state);
        url.append("&redirect_uri=").append(SbfUtils.encodeUtf8(appProperties.getCafe24().get(app).getRedirectUri()));
        url.append("&scope=").append(SbfUtils.encodeUtf8("mall.read_application,mall.write_application,mall.read_customer,mall.read_order,mall.write_order,mall.read_store"));
        log.info("url = {}", url.toString());

        return "redirect:" + url.toString();
    }

    @GetMapping("/oauth/{appName}")
    public String oAuth(HttpServletRequest request, HttpServletResponse response, @PathVariable("appName") String appName, ModelMap modelMap) {
        int status = response.getStatus();
        if (status == HttpStatus.OK.value()) {
            HttpSession session = request.getSession();
            String code = request.getParameter("code");
            String state = request.getParameter("state");
            String mallId = cafe24MallIdStore.get(state);
            
            log.info("app={}, code={}, state={}, mallId={}, ", appName, code, state, mallId);

            if (StringUtils.isNotEmpty(mallId)) {
                boolean updated = cafe24Service.updateToken(appName, mallId, code);
                log.info("cafe24 launch updated={}", updated);
                if (updated) {
                    session.setAttribute("siteId", mallId);

                    //상점정보 조회 API
                    Cafe24StoreDTO storeInfo = cafe24Service.getStoreInfo(mallId);
                    String biz_no = storeInfo.getStore().getCompany_registration_no();

                    //회원정보 조회
                    ExtCustDTO cust = smpCustService.getCustOnSiteByBizNo(biz_no);
                    
                    if (cust != null) {
                        User user = new User(cust.getCust_id(), "", new ArrayList<>());
                        ServiceSiteAuthenticationToken token = new ServiceSiteAuthenticationToken(user, "", GRANTEDAUTH);
                        request.getSession();
                        token.setDetails(new WebAuthenticationDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(token));

                        return "redirect:" + redirectBase + "/";
                    } else {
                        try {
                            List<ExtSvcTermsDTO> bankbotTermsList = smpJoinService.getSellerBotTerms("cf24");
                            modelMap.addAttribute("bankbotTerms", bankbotTermsList);

                            //데이터 변조 방지위해 JSON OBJECT로 데이터 묶어서 URI인코딩
                            JSONObject jso = new JSONObject();
                            jso.put("company_name", storeInfo.getStore().getCompany_name());
                            jso.put("president_name", storeInfo.getStore().getPresident_name());
                            jso.put("company_registration_no", storeInfo.getStore().getCompany_registration_no());
                            jso.put("email", storeInfo.getStore().getEmail());
                            jso.put("mallId", mallId);
                            String encodeVal = URLEncoder.encode(jso.toString(), "UTF-8");

                            modelMap.put("company_name", storeInfo.getStore().getCompany_name());
                            modelMap.put("president_name", storeInfo.getStore().getPresident_name());
                            modelMap.put("company_registration_no", storeInfo.getStore().getCompany_registration_no());
                            modelMap.put("email", storeInfo.getStore().getEmail());
                            modelMap.put("mallId", mallId);
                            modelMap.put("encodeVal", encodeVal);
                        } catch (Exception e) {
                            log.error("cafe24 data setting error!!!");
                        }
                    }
                } else {
                    log.info("cafe24 Failed to update token!!!");
                }
            }
        }
        return "cafe24/new_entry";
    }

    @PostMapping(value = "/launch")
    @ResponseBody
    public ResponseEntity<Cafe24MessageDTO> launch(HttpServletRequest request, JoinCustInfo custInfo, String[] bankbotTerm, String encodeVal) {
        Cafe24MessageDTO message = new Cafe24MessageDTO();

        try {
            //인코딩 된 데이터 URI디코딩 후 JSON OBJECT 파싱
            String decodeVal = URLDecoder.decode(encodeVal, "UTF-8");
            JSONObject jso = new JSONObject(decodeVal);

            ExtCustPackageDTO custDto = new ExtCustPackageDTO();
            custDto.setCust_id(jso.get("email").toString());
            custDto.setPasswd(SecurityAES256.AES_Encode("1234"));
            custDto.setPasswd_same(SecurityAES256.AES_Encode("1234"));
            custDto.setBiz_no(jso.get("company_registration_no").toString());
            custDto.setBiz_nm(jso.get("company_name").toString());
            custDto.setCeo_nm(jso.get("president_name").toString());
            custDto.setCeo_no(custInfo.getCeo_no());
            custDto.setInf_path_cd(EInfPath.CF24);
            custDto.setInf_site_cd(EInfPath.CF24);
            custDto.setMallId(jso.get("mallId").toString());

            /**
             * 약관처리
             */
            //뱅크봇 약관 리스트
            List<ExtSvcTermsDTO> bankbotTermsList = smpJoinService.getSellerBotTerms("cf24");

            // 모든 약관 아이디 저장
            Map<Long, JoinTerms> allTermsIds = bankbotTermsList.stream().collect(Collectors.toMap(k -> k.getTerms_id(),
            v -> new JoinTerms(v.getTerms_esse_yn() == EYesOrNo.Y, v.getSvc_id(), v.getTerms_id())));
            
            // 필수 약관 아이디 저장
            List<Long> essentialTermsIds = bankbotTermsList.stream().filter(it -> {
                return it.getTerms_esse_yn() == EYesOrNo.Y;
            }).map(it -> {
                return it.getTerms_id();
            }).sorted().collect(Collectors.toList());

            Map<Long, JoinTerms> selectedTerm = new TreeMap<>();
            int essentialSize = 0;
            // 약관 선택 오류 확인 및 잘못 전달된 id 값 확인
            for (String item : bankbotTerm) {
                Long sTerm = Long.parseLong(item);
                if (!allTermsIds.containsKey(sTerm))
                    throw new RuntimeException("Not Found TermId " + sTerm);
                if (selectedTerm.containsKey(sTerm))
                    throw new RuntimeException("Same TermId Get " + bankbotTerm);
                JoinTerms terms = allTermsIds.get(sTerm);
                Boolean isEssentialTerm = terms.isEssTerm();
                selectedTerm.put(sTerm, terms);
                if (isEssentialTerm)
                    essentialSize++;
            }

            if (essentialSize != essentialTermsIds.size()) {
                message.setStatus(StatusEnum.INTERNAL_SERER_ERROR);
                message.setMessage("필수 약관 선택 수 오류 " + selectedTerm);
                
                return new ResponseEntity<Cafe24MessageDTO>(message, HttpStatus.OK);
            }

            // 선택된 약관을 list로 변환
            List<ExtAgreeTermsReqDTO> terms = selectedTerm.values().stream().map(it -> {
                ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                t.setCust_id(custDto.getCust_id());
                t.setSvc_id(it.getSvcId());
                t.setTerms_id(it.getTermId());
                return t;
            }).collect(Collectors.toList());

            // 약관 정보 설정
            custDto.setTerms(terms);

            //회원가입
            if (smpJoinService.addCustPkg(custDto)) {
                // 회원가입 완료 시 로그인 처리 프로세스
                User user = new User(custDto.getCust_id(), "", new ArrayList<>());
                ServiceSiteAuthenticationToken token = new ServiceSiteAuthenticationToken(user, "", GRANTEDAUTH);
                request.getSession();
                token.setDetails(new WebAuthenticationDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(token));
            
                //email 발송
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(30000);
                            smpAuthService.authenticationsCustTempPasswd(custInfo.getCust_id());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();

                //알림톡 발송
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(30000);
                            smpCustService.alarm_send_join(custDto.getCust_id());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();

                message.setStatus(StatusEnum.OK);
                message.setMessage("회원가입 완료.");
            }else{
                message.setStatus(StatusEnum.NOT_FOUND);
                message.setMessage("회원가입 실패.");
            }
        } catch (Exception e) {
            message.setStatus(StatusEnum.INTERNAL_SERER_ERROR);
            message.setMessage("회원가입 에러발생 :: " + e.toString());
        }
        return new ResponseEntity<Cafe24MessageDTO>(message, HttpStatus.OK);
    }

    //TODO 테스트용
    @GetMapping("/test/bankbot")
    public String test(ModelMap modelMap) {
        try {
            //데이터 변조 방지위해 JSON OBJECT로 데이터 묶어서 URI인코딩
            JSONObject jso = new JSONObject();
            jso.put("company_name", "(주)온리원");
            jso.put("president_name", "홍길동");
            jso.put("company_registration_no", "2208821666");
            jso.put("email", "kkndegh@onlyoneloan.co.kr");
            jso.put("mallId", "only10fs");
            String encodeVal = URLEncoder.encode(jso.toString(), "UTF-8");

            modelMap.put("company_name", "(주)온리원");
            modelMap.put("president_name", "홍길동");
            modelMap.put("company_registration_no", "2208821666");
            modelMap.put("email", "kkndegh@onlyoneloan.co.kr");
            modelMap.put("mallId", "only10fs");
            modelMap.put("encodeVal", encodeVal);
            
            List<ExtSvcTermsDTO> bankbotTermsList = smpJoinService.getSellerBotTerms("cf24");
            modelMap.addAttribute("bankbotTerms", bankbotTermsList);
        } catch (Exception e) {
            log.error("에러발생 :: " + e.toString());
        }
        return "cafe24/new_entry";
    }
}
