/**
 * @author: mailsung
 */
package onlyone.sellerbotcash.web.page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import onlyone.sellerbotcash.service.ProductTerminationService;

// @Slf4j
@Controller
@RequestMapping("/termination")
public class ProductTerminationController {
    @Autowired
    ProductTerminationService productTerminationService;
    

    @RequestMapping("/withdraw/{encust_id}")
    public String withdraw(ModelMap model, @PathVariable("encust_id") String encust_id) throws Exception {

        boolean result;
        String msg;

        // encust_id = "1kgAYZWlaQOu%2BOnnJCdOW02AzZpnSIWzp7sS%2FcBmDQE%3D";
        result = productTerminationService.withdraw(encust_id);

		if(result) {
            msg = "성공";
			model.addAttribute("msg", msg);
		}else {
			msg = "실패";
			model.addAttribute("msg", msg);
		}
        return "redirect/msgAlert";
    }
}
