package onlyone.sellerbotcash.web.page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

// import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.service.SmpBannerService;

// @Slf4j
@Controller
public class BannerController {

    @Autowired private SmpBannerService smpBannerService;

    @PostMapping(value="/sub/banner/view/count")
    @SuppressWarnings("unused")
    public ResponseEntity<String> bannerViewCount(Long bannerItemSeqNo) {
        boolean result =false;

        result = smpBannerService.bannerViewCount(bannerItemSeqNo);

        return ResponseEntity.ok("");
    }
}
