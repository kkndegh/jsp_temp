package onlyone.sellerbotcash.web.page;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.smp.service.dto.ExtSvcTermsDTO;

@Controller
public class CommonController {
	@Autowired private SmpJoinService smpJoinService;
	
	/*
	 * 20190715회의
	 * svcId는 파라미터로 받게 처리
	 */
	@Value("${smp.svcId:sellerbotcash}") private  String svcId;
	
	/**
	 * 약관 정보 조회
	 * @param modelMap
	 * @param user
	 * @param session
	 * @return
	 */
	@GetMapping("/pub/common/terms")
	public ResponseEntity<List<ExtSvcTermsDTO>> getTerms() {
		List<ExtSvcTermsDTO> list = smpJoinService.getSellerBotTerms(svcId);
		return ResponseEntity.ok(list);
	}
}
