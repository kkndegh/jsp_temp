package onlyone.sellerbotcash.web.page;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpRtnService;
import onlyone.smp.service.dto.ExtCommMallDTO;
import onlyone.smp.service.dto.ExtRtnGraphDTO;
import onlyone.smp.service.dto.ExtRtnStatusDTO;
import onlyone.smp.service.dto.ExtRtnSummaryLastDTO;
import onlyone.smp.service.dto.ExtRtnSummaryPeriodDTO;
import onlyone.sellerbotcash.web.util.DateUtil;

/**
 * 반품 통계 Controller
 * @author YunaJang
 */
@Controller
public class ReturnController {

	@Autowired
	SmpRtnService smpRtnService;

	@Autowired
	SmpCustMallService smpCustMallService;

	/**
	 * 반품 통계 -> 반품 현황
	 * @param modelMap
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping("/sub/return/return")
	public String getReturnView(ModelMap modelMap, @AuthenticationPrincipal User user)
			throws JsonProcessingException {
		String userName = user.getUsername();
		
		// 데이터 조회기간 및 각 포맷에 맞는 날짜 정보 설정
		getDateInfo(modelMap);
		String startDate = modelMap.get("startDate").toString();
		String endDate = modelMap.get("endDate").toString();

		// 반품 현황 조회
		ExtRtnSummaryLastDTO rtnSummaryLast = smpRtnService.getSummaryLast(userName);

		// 몰별 반품 현황 조회
		// 조회 기간(전월기준) : 전년 동월 ~ 전월
		ExtRtnSummaryPeriodDTO rtnSummaryPeriod = smpRtnService.getSummaryPeriod(userName, startDate, endDate);
		
		// 쇼핑몰 목록 조회
		List<ExtCommMallDTO> custMallList = smpCustMallService.getCustMallList(userName);

		// 월별 반품 현황 조회
		ExtRtnStatusDTO monthInfo = smpRtnService.getMonthInfo(userName, startDate, endDate, null);
		 
		ObjectMapper mapper = new ObjectMapper();
		modelMap.addAttribute("rtnSummaryLast", rtnSummaryLast);
		modelMap.addAttribute("rtnSummaryPeriod", mapper.writeValueAsString(rtnSummaryPeriod));
		modelMap.addAttribute("custMallList", custMallList);
		modelMap.addAttribute("monthInfo", mapper.writeValueAsString(monthInfo));
		modelMap.addAttribute("monthList", mapper.writeValueAsString(monthInfo.getRtn_month()));

		return "sub/return/return";
	}

	/**
	 * 반품 통계 -> 반품분석 그래프
	 * @param modelMap
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping("/sub/return/compare")
	public String getReturnCompareView(ModelMap modelMap, @AuthenticationPrincipal User user)
			throws JsonProcessingException {
		String userName = user.getUsername();

		// 데이터 조회기간 및 각 포맷에 맞는 날짜 정보 설정
		getDateInfo(modelMap);
		String startDate = modelMap.get("startDate").toString();
		String endDate = modelMap.get("endDate").toString();

		// 반품 현황 조회
		ExtRtnSummaryLastDTO rtnSummaryLast = smpRtnService.getSummaryLast(userName);

		// 몰별 반품 현황 조회
		// 조회 기간(전월기준) : 전년 동월 ~ 전월
		ExtRtnSummaryPeriodDTO rtnSummaryPeriod = smpRtnService.getSummaryPeriod(userName, startDate, endDate);
		
		// 유형별 반품 현황 조회
		ExtRtnGraphDTO graphInfo = smpRtnService.getGraphInfo(userName, startDate, endDate);

		ObjectMapper mapper = new ObjectMapper();
		modelMap.addAttribute("rtnSummaryLast", rtnSummaryLast);
		modelMap.addAttribute("rtnSummaryPeriod", mapper.writeValueAsString(rtnSummaryPeriod));
		modelMap.addAttribute("graphInfo", graphInfo);
		modelMap.addAttribute("mallGraphList", mapper.writeValueAsString(graphInfo.getRtn_mall()));
		modelMap.addAttribute("marketGraphList", mapper.writeValueAsString(graphInfo.getRtn_market_typ()));

		return "sub/return/compare";
	}

	/**
	 * 기간별 반품 현황 조회
	 * @param rtn_find_stt_dt
	 * @param rtn_find_end_dt
	 * @param mall_cd
	 * @param user
	 * @return
	 */
	@GetMapping("/sub/return/rtnSummaryPeriod")
	@ResponseBody
	public ExtRtnSummaryPeriodDTO getSummaryPeriod(@ModelAttribute("rtn_find_stt_dt") String rtn_find_stt_dt, @ModelAttribute("rtn_find_end_dt") String rtn_find_end_dt, 
			@ModelAttribute("mall_cd") String mall_cd, @AuthenticationPrincipal User user) {
		ExtRtnSummaryPeriodDTO rtnSummaryPeriod = smpRtnService.getSummaryPeriod(user.getUsername(), rtn_find_stt_dt, rtn_find_end_dt);
		return rtnSummaryPeriod;
	}

	/**
	 * 월별 반품 현황 조회
	 * @param rtn_find_stt_dt
	 * @param rtn_find_end_dt
	 * @param mall_cd
	 * @param user
	 * @return
	 */
	@GetMapping("/sub/return/rtnMonthInfo")
	@ResponseBody
	public ExtRtnStatusDTO getRtnMonthInfo(@ModelAttribute("rtn_find_stt_dt") String rtn_find_stt_dt, @ModelAttribute("rtn_find_end_dt") String rtn_find_end_dt, 
			@ModelAttribute("mall_cd") String mall_cd, @AuthenticationPrincipal User user) {
		ExtRtnStatusDTO monthInfo = smpRtnService.getMonthInfo(user.getUsername(), rtn_find_stt_dt, rtn_find_end_dt, mall_cd);
		return monthInfo;
	}

	/**
	 * 유형별 반품 현황 조회
	 * @param rtn_find_stt_dt
	 * @param rtn_find_end_dt
	 * @param user
	 * @return
	 */
	@GetMapping("/sub/return/rtnGraphInfo")
	@ResponseBody
	public ExtRtnGraphDTO getRtnGraphInfo(@ModelAttribute("rtn_find_stt_dt") String rtn_find_stt_dt, @ModelAttribute("rtn_find_end_dt") String rtn_find_end_dt, @AuthenticationPrincipal User user) {
		ExtRtnGraphDTO graphInfo = smpRtnService.getGraphInfo(user.getUsername(), rtn_find_stt_dt, rtn_find_end_dt);
		return graphInfo;
	}

	/**
	 * 데이터 조회기간 및 각 포맷에 맞는 날짜 정보 설정
	 * @param modelMap
	 * @TODO: 과거 정산금 통계와 동일, 추후 한곳에서 처리하도록 수정
	 */
	private void getDateInfo(ModelMap modelMap) {
		// 지난 달 텍스트
		// ex) 19년 9월
		LocalDate prevMonthDate = LocalDate.now().minusMonths(1);
		StringBuffer prevMonthStr = new StringBuffer();
		prevMonthStr.append(prevMonthDate.getYear() % 100).append("년 ").append(prevMonthDate.getMonthValue()).append("월");
		modelMap.addAttribute("prevMonth", prevMonthStr.toString());
		// ex) 2019-09
		prevMonthStr = null;
		prevMonthStr = new StringBuffer();
		prevMonthStr.append(prevMonthDate.getYear()).append("-");

		if(prevMonthDate.getMonthValue() < 10)
			prevMonthStr.append("0");

		prevMonthStr.append(prevMonthDate.getMonthValue());
		modelMap.addAttribute("prevMonth2", prevMonthStr.toString());


		// 조회기간 기준 텍스트
		// ex) 2018-04~2019-03
		LocalDate prevYearSameMonthDate = LocalDate.now().minusYears(1);
		StringBuffer searchDateRangeStr = new StringBuffer();
		searchDateRangeStr.append(prevYearSameMonthDate.getYear()).append("-");
		
		if(prevYearSameMonthDate.getMonthValue() < 10)
			searchDateRangeStr.append("0");
		
		searchDateRangeStr.append(prevYearSameMonthDate.getMonthValue());
		searchDateRangeStr.append("~");
		searchDateRangeStr.append(prevMonthDate.getYear()).append("-");
		
		if(prevMonthDate.getMonthValue() < 10)
			searchDateRangeStr.append("0");

		searchDateRangeStr.append(prevMonthDate.getMonthValue());
		modelMap.addAttribute("searchDateRangeStr", searchDateRangeStr.toString());


		// 조회 기간
		String startDate = DateUtil.getYearAndMonth(prevYearSameMonthDate);
		String endDate = DateUtil.getYearAndMonth(prevMonthDate);
		modelMap.addAttribute("startDate", startDate);
		modelMap.addAttribute("endDate", endDate);
	}
}
