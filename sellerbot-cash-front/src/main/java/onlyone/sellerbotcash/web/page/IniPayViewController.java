package onlyone.sellerbotcash.web.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/inipay/")
public class IniPayViewController {
    @RequestMapping("/payrequest")
    public String initStdPayRequest() {
        return "inipay/INIStdPayRequest";
    }

    @RequestMapping("/paybill")
    public String initStdPayBill() {
        return "inipay/INIStdPayBill";
    }

    @RequestMapping(value="/payreturn")
    public String initStdPayReturn() {

        return "inipay/INIStdPayReturn";
    }

    @RequestMapping(value="/popup")
    public String popup() {
        return "inipay/popup";
    }

    @RequestMapping(value="/close")
    public String close() {
        return "inipay/close";
    }    
}
