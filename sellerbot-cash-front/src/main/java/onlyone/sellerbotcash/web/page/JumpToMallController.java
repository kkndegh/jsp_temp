package onlyone.sellerbotcash.web.page;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import onlyone.sellerbotcash.service.JumpService;
import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpMallService;
import onlyone.sellerbotcash.web.util.CryptHelper;
import onlyone.smp.service.dto.ExtCustJumpListDTO;
import onlyone.smp.service.dto.ExtCustMallDTO;
import onlyone.smp.service.dto.ExtJumpResponseDTO;
import onlyone.smp.service.dto.ExtMallDTO;

/**
 * 점프 서비스 Controller
 * @author YunaJang
 */
@Controller
public class JumpToMallController {
	@Autowired
	private SmpCustMallService smpCustMallService;

	@Autowired
	private SmpMallService smpMallService;

	@Autowired
	private JumpService jumpService;

	/**
	 * 점프 서비스 -> 점프 서비스
	 * @param modelMap
	 * @param device
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping("/sub/jump/service")
	public String getJumpService(ModelMap modelMap, Device device, @AuthenticationPrincipal User user) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String userName = user.getUsername();

		if (device.isNormal()) {
			modelMap.addAttribute("DeviceType" , "PC");
        } else {
			modelMap.addAttribute("DeviceType" , "Other");
        }

		// 사용자의 몰 리스트
		List<ExtCustMallDTO> custMallList = smpCustMallService.getNonDelCustMall(userName).getMall();
		String mallListString = convertListToStringForMall(custMallList);
		modelMap.addAttribute("mallListString" , mallListString);

		// 사용자의 점프 리스트
		List<ExtCustJumpListDTO> custJumpList = jumpService.getCustJumpList(userName);
		Map<String, List<ExtCustJumpListDTO>> custJumpMap = jumpService.getCustJumpMap(custJumpList);
		modelMap.addAttribute("custJumpMapString" , mapper.writeValueAsString(custJumpMap));

		// 점프 리스트
		List<ExtJumpResponseDTO> jumpList = jumpService.getJumpList();
		Map<String, List<ExtJumpResponseDTO>> jumpMap = jumpService.getJumpMap(jumpList);
		modelMap.addAttribute("jumpMapString" , mapper.writeValueAsString(jumpMap));

		return "sub/jump/service";
	}

	/**
	 * 점프 서비스 -> 프로그램 설치안내
	 * @param modelMap
	 * @return
	 * @TODO: 추후 IE11 브라우저 설정 방법 안내 필요
	 */
	@GetMapping("/sub/jump/download")
	public String getDownloadService(ModelMap modelMap) {
		return "sub/jump/download";
	}

	/**
	 * 사용자의 점프 리스트 조회
	 * @param user
	 * @return
	 */
	@PostMapping("/sub/jump/custJumpList")
	@ResponseBody
	public Map<String, List<ExtCustJumpListDTO>> getCustJumpList(@AuthenticationPrincipal User user) {
		String userName = user.getUsername();
		List<ExtCustJumpListDTO> custJumpList = jumpService.getCustJumpList(userName);
		Map<String, List<ExtCustJumpListDTO>> custJumpMap = jumpService.getCustJumpMap(custJumpList);

		return custJumpMap;
	}

	/**
	 * 점프 서비스를 위한 몰 정보 조회
	 * @param cust_mall_seq_no
	 * @param user
	 * @return
	 */
	@PostMapping("/sub/jump/mallInfo")
	@ResponseBody
	public ExtCustMallDTO getMallInfo(@ModelAttribute("cust_mall_seq_no") String cust_mall_seq_no,
			@AuthenticationPrincipal User user) {
		String userName = user.getUsername();
		ExtCustMallDTO mallInfo = smpCustMallService.getCustMallDetailInfo(cust_mall_seq_no, userName);
		mallInfo.setMall_cert_1st_id(CryptHelper.aes_decrypt(mallInfo.getMall_cert_1st_id()));
		mallInfo.setMall_cert_1st_passwd(CryptHelper.aes_decrypt(mallInfo.getMall_cert_1st_passwd()));

		if (mallInfo.getPasswd_step() >= 1)
			mallInfo.setMall_cert_2nd_passwd(CryptHelper.aes_decrypt(mallInfo.getMall_cert_2nd_passwd()));

		if (mallInfo.getCert_step() >= 1)
			mallInfo.setSub_mall_cert_1st(CryptHelper.aes_decrypt(mallInfo.getSub_mall_cert_1st()));

		if (mallInfo.getPasswd_step() >= 2)
			mallInfo.setSub_mall_cert_2nd(CryptHelper.aes_decrypt(mallInfo.getSub_mall_cert_2nd()));

		return mallInfo;
	}

	/**
	 * 몰 정보 조회
	 * @param mall_cd
	 * @return
	 */
	@PostMapping("/sub/jump/basicMallInfo")
	@ResponseBody
	public ExtMallDTO getMallInfo(@ModelAttribute("mall_cd") String mall_cd) {
		ExtMallDTO mallInfo = smpMallService.getMallInfo(mall_cd);
		return mallInfo;
	}

	/**
	 * 점프 서비스를 위한 사이트들을 등록
	 * @param jumpSeqNoList
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@PostMapping("/sub/jump/addJumpItems")
	@ResponseBody
	public ResponseEntity<String> addJumpItems(@ModelAttribute("jumpSeqNoList") String jumpSeqNoList,
			@AuthenticationPrincipal User user) throws JsonProcessingException {
		String userName = user.getUsername();
		return jumpService.addJumpList(userName, jumpSeqNoList);
	}

	/**
	 * 점프 서비스를 위하여 등록된 사이트들을 제거
	 * @param custJumpSeqNoList
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@PostMapping("/sub/jump/deleteJumpItems")
	@ResponseBody
	public ResponseEntity<String> deleteJumpItems(@ModelAttribute("custJumpSeqNoList") String custJumpSeqNoList,
			@AuthenticationPrincipal User user) throws JsonProcessingException {
		String userName = user.getUsername();
		return jumpService.deleteJumpList(userName, custJumpSeqNoList);
	}

	/**
	 * 사용자의 몰 정보를 Json 형태로 변환
	 * @param list
	 * @return
	 * @throws JsonProcessingException
	 */
	private String convertListToStringForMall(List<ExtCustMallDTO> list) throws JsonProcessingException {
		// API 추가 없이 기존의 API를 호출하다보니 데이터 가공이 필요
		List<ExtCustMallDTO> newList = new ArrayList<ExtCustMallDTO>();
		for(ExtCustMallDTO dto : list) {
			ExtCustMallDTO newDto = new ExtCustMallDTO();
			newDto.setCust_id(dto.getCust_id());
			newDto.setCust_mall_seq_no(dto.getCust_mall_seq_no());
			newDto.setCust_mall_sts_cd(dto.getCust_mall_sts_cd());
			newDto.setCust_mall_sts_cd_nm(dto.getCust_mall_sts_cd_nm());
			newDto.setMall_cd(dto.getMall_cd());
			newDto.setMall_cd_nm(dto.getMall_cd_nm());
			newDto.setMall_cert_1st_id(dto.getMall_cert_1st_id());
			newDto.setMall_typ_cd(dto.getMall_typ_cd());
			newDto.setMall_typ_cd_nm(dto.getMall_typ_cd_nm());
			newDto.setStor_path(dto.getStor_path());
			newDto.setStor_file_nm(dto.getStor_file_nm());

			newList.add(newDto);
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(newList);
	}
}