package onlyone.sellerbotcash.web.page;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import onlyone.smp.service.dto.ExtCustDTO;

@Controller
public class AuthPopupController {
	@Value("${okname.licenseDir:}") private String licenseDir;
	@Value("${okname.returnUrl:}") private String returnUrl;
	@Value("${okname.siteUrl:}") private String siteUrl;	
	
	/**
	 * 고정 코드
	 */
	static final String CP_CD = "V34990000000";
	
	@GetMapping("/authPopup/phone_popup1")
	public String callPopup1(HttpSession session, ModelMap modelMap) {

		modelMap.addAttribute("CP_CD", CP_CD);
		modelMap.addAttribute("licenseDir", licenseDir);
		modelMap.addAttribute("returnUrl", returnUrl);
		modelMap.addAttribute("SITE_URL", siteUrl);
		
		//20211026 본인인증 휴대폰번호 + 성명 디폴트 지정
		ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");
		String name = "";
		String telNo = "";
		if (cust != null) {
			name = cust.getCeo_nm();
			telNo = cust.getCeo_no().replaceAll("-", "");
		}
		modelMap.addAttribute("NAME", name);
		modelMap.addAttribute("TEL_NO", telNo);
		
		return "authPopupCall";
	}
	
	@PostMapping("/authPopup/phone_popup3")
	public String callPopup2(HttpSession session, ModelMap modelMap) {
		modelMap.addAttribute("licenseDir", licenseDir);
		modelMap.addAttribute("cpCd", CP_CD);
		return "authPopupReturn";
	}

	@GetMapping("/authPopup/phone_popup3")
	public String getCallPopup3(HttpSession session, ModelMap modelMap) {
		modelMap.addAttribute("licenseDir", licenseDir);
		modelMap.addAttribute("cpCd", CP_CD);
		return "authPopupReturn";
	}
}
