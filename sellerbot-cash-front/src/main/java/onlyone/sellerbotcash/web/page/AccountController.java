package onlyone.sellerbotcash.web.page;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Future;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;
import onlyone.sellerbotcash.service.AccountService;
import onlyone.sellerbotcash.service.MypageService;
import onlyone.sellerbotcash.service.PaymentService;
import onlyone.sellerbotcash.service.SmpAccountService;
import onlyone.sellerbotcash.service.SmpBankService;
import onlyone.sellerbotcash.service.SmpCommService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpFinGoodService;
import onlyone.sellerbotcash.web.util.CryptHelper;
import onlyone.sellerbotcash.web.util.DateUtil;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.AcctTraContDepoHisDTO;
import onlyone.smp.service.dto.Cafe24OrdSearchDTO;
import onlyone.smp.service.dto.ExtAutoMatchRecoInfoDTO;
import onlyone.smp.service.dto.ExtBankResponseDTO;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtCusAcctDTO;
import onlyone.smp.service.dto.ExtCusAcctDetailDTO;
import onlyone.smp.service.dto.ExtCusAcctEditDTO;
import onlyone.smp.service.dto.ExtCustAcctAepositRequstDTO;
import onlyone.smp.service.dto.ExtCustAcctAepositResponseDTO;
import onlyone.smp.service.dto.ExtCustAcctDTO;
import onlyone.smp.service.dto.ExtCustAcctDtlInfoDTO;
import onlyone.smp.service.dto.ExtCustAcctInfoDTO;
import onlyone.smp.service.dto.ExtCustAcctTratRequstDTO;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtMaxRegCountResDTO;
import onlyone.smp.service.dto.ExtTraContRecoListDTO;
import onlyone.smp.service.dto.ExtUsingGoodsInfoDTO;
import onlyone.smp.service.dto.VerifyAcctDTO;

/**
 * 정산계좌 통합관리
 * 
 * @author bak
 *
 */
// @Slf4j
@Controller
public class AccountController {
    @Autowired
    private SmpFinGoodService smpFinGoodService;
    @Autowired
    private SmpAccountService smpAccountService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private SmpCustService smpCustService;
    @Autowired
    private SmpBankService smpBankService;
    @Autowired
    private SmpCommService smpCommService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private MypageService mypageService;
    // @Resource(name="taskExecutor")
    // private ExceptionHandlingExecutor taskExecutor;

    /**
     * 정산계좌
     * 
     * @param modelMap
     * @param user
     * @param session
     * @return
     * @throws JsonProcessingException
     * @throws URISyntaxException
     */
    @ExceptionHandler
    @RequestMapping(value = "/sub/account/account")
    public String account(ModelMap modelMap, @AuthenticationPrincipal User user, HttpSession session)
            throws JsonProcessingException, URISyntaxException {
        ObjectMapper mapper = new ObjectMapper();
        String username = user.getUsername();
        String custTyp = smpCustService.getCustUseLoan(username);

        ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");

        // Cafe24 고객 여부
        EYesOrNo cafe24_usr_yn = EInfPath.CF24 == cust.getInf_site_cd() ? EYesOrNo.Y : EYesOrNo.N;
        modelMap.addAttribute("cafe24_usr_yn", cafe24_usr_yn.name());

        // 샵바이 고객 여부
        EYesOrNo shopby_usr_yn = EInfPath.SHBY == cust.getInf_site_cd() ? EYesOrNo.Y : EYesOrNo.N;
        modelMap.addAttribute("shopby_usr_yn", shopby_usr_yn.name());

        // 이용중인 서비스 조회
        ExtUsingGoodsInfoDTO usingGoods = mypageService.getPayInfoForUsingGoods(username);
        modelMap.addAttribute("addn_goods_req_seq_no", usingGoods.getAddn_goods_req_seq_no());

        // 정산계좌 목록
        ExtCustAcctInfoDTO custAcctInfo = accountService.getCustAcctInfo(username, custTyp);
        modelMap.addAttribute("custAcctList", mapper.writeValueAsString(custAcctInfo.getAcct_list()));

        // 10만 단위만 표시
        Long preSett = custAcctInfo.getPre_sett() / 100000 * 100000;
        custAcctInfo.setPre_sett(preSett);
        modelMap.addAttribute("custAcctInfo", custAcctInfo);

        // 은행 정보
        List<ExtBankResponseDTO> bankList = smpBankService.getBankList();
        modelMap.addAttribute("bankList", bankList);

        // 통화 코드
        List<ExtCommCdDTO> codeList = smpCommService.getCodeList("A056");
        modelMap.addAttribute("codeList", codeList);

        // 현재 이용권으로 등록 가능한 수량 조회
        String userName = user.getUsername();
        ExtMaxRegCountResDTO maxRegCountInfo = paymentService.getMaxRegCountInfo(userName);
        modelMap.addAttribute("maxRegCountInfo", maxRegCountInfo);

        // 2020-03-18 고객의 계좌 삭제 가능 여부 확인 [CASH-266]
        String delConfirm = smpCustService.getCustAcctDeleteConfirm(user.getUsername());
        modelMap.addAttribute("delConfirm", delConfirm);

        return "sub/account/account";
    }

    /**
     * 계좌 등록
     * 
     * @param user
     * @param dtoList
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/sub/account/regAccount", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> accountRegister(@AuthenticationPrincipal User user,
            @Valid @RequestBody List<ExtCusAcctEditDTO> dtoList) {
        return smpAccountService.addCustAcct(user.getUsername(), dtoList);
    }

    /**
     * 계좌 정보 조회
     * 
     * @param user
     * @return
     */
    @GetMapping("/sub/account/accountDetail")
    @ResponseBody
    public ResponseEntity<ExtCusAcctDetailDTO> accountDetail(Long cust_acct_seq_no,
            @AuthenticationPrincipal User user) {
        ExtCusAcctDetailDTO detailInfo = smpAccountService.getCustAcctInfo(user.getUsername(), cust_acct_seq_no);
        return new ResponseEntity<ExtCusAcctDetailDTO>(detailInfo, HttpStatus.OK);
    }

    /**
     * 계좌 수정
     * 
     * @param user
     * @param cust_acct_seq_no
     * @param dtoList
     * @return
     */
    @ResponseBody
    @PutMapping(value = "/sub/account/account/{cust_acct_seq_no}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> accountEdit(@PathVariable("cust_acct_seq_no") Long cust_acct_seq_no,
            @Valid @RequestBody List<ExtCusAcctEditDTO> dtoList, @AuthenticationPrincipal User user) {
        return smpAccountService.editCustAcct(user.getUsername(), cust_acct_seq_no, dtoList);
    }

    /**
     * 계좌 삭제
     * 
     * @param user
     * @param cust_acct_seq_no
     * @return
     */
    @ResponseBody
    @DeleteMapping(value = "/sub/account/account/{cust_acct_seq_no}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> accountDelete(@AuthenticationPrincipal User user,
            @PathVariable("cust_acct_seq_no") Long cust_acct_seq_no) {
        return smpAccountService.deleteCustAcct(user.getUsername(), cust_acct_seq_no);
    }

    /**
     * 4.9.1 고객 계좌 판매몰 입금 현황 조회
     * 
     * @param user
     * @param param
     * @return
     */
    @GetMapping("/sub/account/acct")
    @ResponseBody
    public ExtCusAcctDTO getCustAcctList(@AuthenticationPrincipal User user) {
        return smpAccountService.getCustAcctList(user.getUsername());
    }

    /**
     * 4.9.7 고객 계좌 판매몰 입금 현황 조회
     * 
     * @param user
     * @param param
     * @return
     */
    @GetMapping("/sub/account/stat")
    @ResponseBody
    public List<ExtCustAcctAepositResponseDTO> getCustAcctAepositList(@AuthenticationPrincipal User user,
            @ModelAttribute ExtCustAcctAepositRequstDTO param) {
        // 4.9.7 고객 계좌 판매몰 입금 현황 조회
        return smpAccountService.getCustAcctList(user.getUsername(), param);
    }

    /**
     * 4.9.8 고객 계좌 거래내용 요청
     * 
     * @param user
     * @param param
     * @return
     */
    @PostMapping(value = "/sub/account/tra/req")
    @ResponseBody
    public ResponseEntity<String> reqCustAcctTra(@AuthenticationPrincipal User user, String acct_no) {
        smpAccountService.reqCustAcctTra(user.getUsername(), acct_no);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 거래내역 조회
     * 
     * @param modelMap
     * @param user
     * @param session
     * @return
     */
    @RequestMapping(value = "/sub/account/detail")
    public String getAccountDetailView(@RequestParam(required = false) String cust_acct_seq_no, ModelMap modelMap,
            @AuthenticationPrincipal User user, HttpSession session) {
        String username = user.getUsername();

        ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");

        // Cafe24 고객 여부
        EYesOrNo cafe24_usr_yn = EInfPath.CF24 == cust.getInf_site_cd() ? EYesOrNo.Y : EYesOrNo.N;
        modelMap.addAttribute("cafe24_usr_yn", cafe24_usr_yn.name());

        // 샵바이 고객 여부
        EYesOrNo shopby_usr_yn = EInfPath.SHBY == cust.getInf_site_cd() ? EYesOrNo.Y : EYesOrNo.N;
        modelMap.addAttribute("shopby_usr_yn", shopby_usr_yn.name());

        // 계좌 목록
        List<ExtCustAcctDTO> custAcctList = accountService.getCustAcctList(username, null);
        modelMap.addAttribute("custAcctList", custAcctList);

        // 미리정산가능금액
        long preSett = smpFinGoodService.getBiggestPre(username, DateUtil.getDateString(LocalDate.now()));
        modelMap.addAttribute("preSett", preSett);

        modelMap.addAttribute("cust_acct_seq_no", cust_acct_seq_no);
        return "sub/account/detail";
    }

    /**
     * 고객 계좌 거래내역 조회
     * 
     * @param param
     * @param page
     * @param size
     * @param sort
     * @param user
     * @return
     */
    @GetMapping("/sub/account/detailInfo")
    @ResponseBody
    public ExtCustAcctDtlInfoDTO getAcctTraDetailInfo(ExtCustAcctTratRequstDTO param, int page, int size,
            @AuthenticationPrincipal User user) {
        // 거래내역
        ExtCustAcctDtlInfoDTO custAcctDtlInfo = accountService.getCustAcctDtlInfo(param, page, size);
        return custAcctDtlInfo;
    }

    /**
     * 거래내역 메모 등록
     * 
     * @param acct_tra_cont_seq_no
     * @param memo
     * @return
     */
    @PostMapping(value = "/sub/account/tra/regMemo")
    @ResponseBody
    public ResponseEntity<String> regMemoForTraCont(Long acct_tra_cont_seq_no, String memo) {
        return accountService.regMemoForTraCont(acct_tra_cont_seq_no, memo);
    }

    @RequestMapping("/sub/account/reconcile")
    public String reconcile(String cust_acct_seq_no, String startDT, String endDT, String acctTraCont,
            ModelMap modelMap, @AuthenticationPrincipal User user, HttpSession session) {
        String username = user.getUsername();

        // 계좌 목록
        List<ExtCustAcctDTO> custAcctList = accountService.getCustAcctList(username, EYesOrNo.Y.toString());
        modelMap.addAttribute("custAcctList", custAcctList);

        // 미리정산가능금액
        long preSett = smpFinGoodService.getBiggestPre(username, DateUtil.getDateString(LocalDate.now()));
        modelMap.addAttribute("preSett", preSett);

        modelMap.addAttribute("cust_acct_seq_no", cust_acct_seq_no);
        modelMap.addAttribute("startDT", startDT);
        modelMap.addAttribute("endDT", endDT);
        modelMap.addAttribute("acctTraCont", acctTraCont);

        return "sub/account/reconcile";
    }

    /**
     * 대사 서비스 조회
     * 
     * @param cust_acct_seq_no
     * @param acct_tra_cont
     * @param reco_sts_cd
     * @param startDT
     * @param endDT
     * @param page
     * @param size
     * @param user
     * @return
     */
    @GetMapping("/sub/account/recoList")
    @ResponseBody
    public ListData<ExtTraContRecoListDTO> getRecoList(Long cust_acct_seq_no, String acct_tra_cont, String reco_sts_cd,
            String startDT, String endDT, Integer page, Integer size, @AuthenticationPrincipal User user) {
        reco_sts_cd = StringEscapeUtils.unescapeHtml(reco_sts_cd);
        return accountService.getRecoList(user.getUsername(), cust_acct_seq_no, acct_tra_cont, reco_sts_cd, startDT,
                endDT, page, size);
    }

    /**
     * 자동대사 정보
     * 
     * @param acct_tra_cont_seq_no
     * @return
     */
    @GetMapping("/sub/account/recoInfo")
    @ResponseBody
    public ExtAutoMatchRecoInfoDTO getRecoInfo(Long acct_tra_cont_seq_no, Long ord_seq_no) {
        return accountService.getAutoMatRecoInfo(acct_tra_cont_seq_no, ord_seq_no);
    }

    /**
     * 대사 상태 변경(입금확인 -> 미확인)
     * 
     * @param acct_tra_cont_seq_no
     * @param ord_seq_no
     * @return
     */
    @PostMapping(value = "/sub/account/chgRecoSts")
    @ResponseBody
    public ResponseEntity<String> chgRecoSts(Long acct_tra_cont_seq_no) {
        return accountService.updateRecoSts(acct_tra_cont_seq_no);
    }

    /**
     * 대사 상태 변경(입금확인 -> 대사제외)
     * 
     * @param acct_tra_cont_seq_no
     * @return
     */
    @PostMapping(value = "/sub/account/exclRecoSts")
    @ResponseBody
    public ResponseEntity<String> regMemoForTraCont(Long acct_tra_cont_seq_no) {
        return accountService.exclRecoSts(acct_tra_cont_seq_no);
    }

    /**
     * 거래내역 조회
     * 
     * @param startDate
     * @param endDate
     * @param keyword
     * @param column
     * @param acct_tra_cont_seq_no
     * @param pageNumber
     * @param pageSize
     * @param user
     * @return
     */
    @PostMapping(value = "/sub/account/depoHist")
    @ResponseBody
    public AcctTraContDepoHisDTO getDepositHistory(String startDate, String endDate, String keyword, String column,
            Long acct_tra_cont_seq_no, Integer pageNumber, Integer pageSize, @AuthenticationPrincipal User user) {
        String userName = user.getUsername();
        ExtCustDTO cust = smpCustService.getCust(userName);
        return accountService.getDepositHistory(cust.getCust_seq_no(), startDate, endDate, keyword, column,
                acct_tra_cont_seq_no, pageNumber, pageSize);
    }

    /**
     * 주문내역 조회
     * 
     * @param startDate
     * @param endDate
     * @param keyword
     * @param column
     * @param pageNumber
     * @param pageSize
     * @param user
     * @return
     */
    @PostMapping(value = "/sub/account/ordHist")
    @ResponseBody
    public Cafe24OrdSearchDTO getOrderHistory(String startDate, String endDate, String keyword, String column,
            Integer pageNumber, Integer pageSize, @AuthenticationPrincipal User user) {
        String userName = user.getUsername();
        ExtCustDTO cust = smpCustService.getCust(userName);
        return accountService.getOrderHistory(cust.getCust_seq_no(), startDate, endDate, keyword, column, pageNumber,
                pageSize);
    }

    /**
     * 수동입금 확인
     * 
     * @param depList
     * @param ordList
     * @param user
     * @return
     */
    @PostMapping(value = "/sub/account/matchDepNOrd")
    @ResponseBody
    public ResponseEntity<Integer> matchingDepNOrdInfo(String depListStr, String ordListStr,
            @AuthenticationPrincipal User user) {
        String userName = user.getUsername();
        ExtCustDTO cust = smpCustService.getCust(userName);
        return accountService.matchingDepNOrdInfo(cust.getCust_seq_no(), depListStr, ordListStr);
    }

    @RequestMapping("/sub/account/verifyAccount")
    public ResponseEntity<VerifyAcctDTO> verifyAccount(String bankCd, String bank_id, String bank_passwd,
            String acct_no, String acct_passwd, String ctz_biz_no) throws Exception {
        VerifyAcctDTO dto = new VerifyAcctDTO();
        dto.setBankCd(bankCd);
        dto.setAcctNo(acct_no);
        dto.setAcctPw(CryptHelper.aes_encrypt(acct_passwd));
        dto.setCtzBizNo(CryptHelper.aes_encrypt(ctz_biz_no));

        if (Objects.nonNull(bank_id) && !"".equals(bank_id))
            dto.setBankId(CryptHelper.aes_encrypt(bank_id));

        if (Objects.nonNull(bank_passwd) && !"".equals(bank_passwd))
            dto.setBankPw(CryptHelper.aes_encrypt(bank_passwd));

        Future<VerifyAcctDTO> task = accountService.verifyAccount(dto);
        return new ResponseEntity<VerifyAcctDTO>(task.get(), HttpStatus.OK);
    }

    /**
     * 중복 계좌 확인
     * 
     * @param acct_no
     */
    @RequestMapping("/sub/account/checkDupl")
    public ResponseEntity<Integer> countByCustNo(@Param("acct_no") String acct_no) throws Exception {
        return accountService.countByCustNo(acct_no);
    }

}