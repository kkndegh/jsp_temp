package onlyone.sellerbotcash.web.page;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.config.ApplicationProperties;
import onlyone.sellerbotcash.service.MainService;
import onlyone.sellerbotcash.service.MypageService;
import onlyone.sellerbotcash.service.SmpBannerService;
import onlyone.sellerbotcash.service.SmpCommService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.sellerbotcash.service.SmpLoanService;
import onlyone.sellerbotcash.service.SmpTermsService;
import onlyone.sellerbotcash.web.util.DateUtil;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EStorFileTyp;
import onlyone.smp.persistent.domain.enums.ETermsTyp;
import onlyone.smp.service.dto.ExtBannerDTO;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtDashboardDTO;
import onlyone.smp.service.dto.ExtDashboardRecoDTO;
import onlyone.smp.service.dto.ExtLoanCustMallDTO;
import onlyone.smp.service.dto.ExtSvcTermsDTO;
import onlyone.smp.service.dto.ExtUsingGoodsInfoDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO.ExtSmtAgreeTermsInfoDTO;

@Slf4j
@Controller
public class MainController {

    @Value("${smp.svcId:sellerbotcash}")
    private String svcId;
    @Value("${internal.redirect.base:}")
    private String redirectBase;
    @Autowired
    private SmpCustService smpCustService;
    @Autowired
    private MainService mainService;
    @Autowired
    private MypageService mypageService;
    @Autowired
    private ApplicationProperties props;
    @Autowired
    private SmpJoinService smpJoinService;
    @Autowired
    private SmpBannerService smpBannerService;
    @Autowired
    private SmpCommService smpCommService;
    @Autowired
    private SmpTermsService smpTermsService;
    @Autowired
    private SmpLoanService smpLoanService;

    @GetMapping("/session-info")
    public String sessionInfo(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session == null) {
            return "세션이 없습니다";
        }

        log.info("sessionId = {}", session.getId());
        log.info("getMaxInactiveInterval={}", session.getMaxInactiveInterval());
        log.info("creationTime={}", new Date(session.getCreationTime()));
        log.info("lastAccessTime={}", new Date(session.getLastAccessedTime()));

        return "세션출력";

    }

    @RequestMapping(value = "/")
    @ExceptionHandler(Exception.class)
    public String newHome(ModelMap modelMap, @AuthenticationPrincipal User user, HttpServletRequest request,
            Device device) {
        HttpSession session = request.getSession();

        // return "underConstrn/underConstrn";

        if (user != null) {
            Object prevUrl = session.getAttribute("prevUrl");
            if (Objects.nonNull(prevUrl)) {
                session.removeAttribute("prevUrl");
                return "redirect:" + prevUrl.toString();
            }
            String custId = user.getUsername();
            log.info("userName: {}", custId);

            Long eventNo = (Long) session.getAttribute("pe");
            if (eventNo != null) // 이벤트 참여
                smpCustService.addCustEvent(custId, eventNo);

            // if (smpCustService.getValidCustEvent(custId) != 0L) {
            // return "redirect:" + redirectBase + "/sub/payment/product";
            // }

            // 메인화면 데이터 조회
            // Map<String, String> custTypResultMap = new HashMap<String, String>();
            // custTypResultMap = smpCustService.getCustUseLoanMap(custId);
            // String custTyp = custTypResultMap.get("cust_typ");

            String custTyp = smpCustService.getCustUseLoan(custId);

            String loanSvcId = "";
            if (session.getAttribute("infPath") != null) {
                if (EInfPath.SCB.name().equals(session.getAttribute("infPath").toString().toUpperCase())) {
                    loanSvcId = "scbank";
                }
            }

            ExtDashboardDTO dashboardInfo = mainService.getDashboardInfo(custId, custTyp, loanSvcId);
            ExtDashboardRecoDTO recoInfo = null;

            // ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");
            // if (cust != null) {

            // 모든 사용자에게 설문조사 이벤트 노출 2020-11-27
            // if (custPayInfo.isPayingUser(session, custId/*cust.getCust_id()*/)) {
            // dashboardInfo.setEvent_list(null); // pop-up 비노출
            // }

            // 이용중인 서비스 조회
            ExtUsingGoodsInfoDTO usingGoods = mypageService.getPayInfoForUsingGoods(custId);
            if (Objects.nonNull(usingGoods) && Objects.nonNull(usingGoods.getAddn_goods_req_seq_no())) {
                recoInfo = mainService.getRecoInfo(custId);
            }
            // }

            // 금융 상품 사용중인 판매몰 리스트
            List<ExtLoanCustMallDTO> loanCustUseMallList = smpLoanService.getLoanCustUseMallList(user.getUsername());
            boolean loanCustUse = false;
            if (loanCustUseMallList.size() > 0) {
                loanCustUse = true;
            }
            modelMap.addAttribute("loanCustUse", loanCustUse);

            boolean isExternalPopUp = true;
            if (!device.isNormal()) { // MOBILE
                dashboardInfo.setEvent_list(null); // pop-up 비노출
                isExternalPopUp = false;
            }

            try {
                if (DateUtil.getCompreBeforeNowDateToEndDate("2021-09-01")) {
                    isExternalPopUp = false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            List<ExtSvcTermsDTO> list = smpJoinService.getSellerBotTerms(svcId);
            Long smtTermsId = 0L;

            for (ExtSvcTermsDTO extSvcTermsDTO : list) {
                if (extSvcTermsDTO.getTerms_typ_cd() == ETermsTyp.IT3) {
                    modelMap.put("IT3TermsId", extSvcTermsDTO.getTerms_id());
                    modelMap.put("IT3TermsCont", extSvcTermsDTO.getTerms_cont());
                    modelMap.put("IT3TermsTitle", extSvcTermsDTO.getTerms_title());
                } else if (extSvcTermsDTO.getTerms_typ_cd() == ETermsTyp.SMT) {
                    modelMap.put("SMTTermsEsseYn", extSvcTermsDTO.getTerms_esse_yn());
                    modelMap.put("SMTTermsId", extSvcTermsDTO.getTerms_id());
                    modelMap.put("SMTTermsCont", extSvcTermsDTO.getTerms_cont());
                    modelMap.put("SMTTermsTitle", extSvcTermsDTO.getTerms_title());
                    smtTermsId = extSvcTermsDTO.getTerms_id();
                }

            }

            ExtCustDTO custData = smpCustService.getCust(user.getUsername());
            modelMap.addAttribute("cust", custData);

            // 20221117 셀러봇캐시 마케팅 활용 세부 항목 가져오기 추가
            List<ExtCommCdDTO> smtTermsList = smpCommService.getCodeList("SMT");
            smtTermsList = smtTermsList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord).reversed())
                    .collect(Collectors.toList());
            modelMap.addAttribute("smtTermsList", smtTermsList); // 셀러봇캐시 마케팅 활용 세부 항목

            String smtAgreeYN = smpTermsService.termsChk(custId, svcId, smtTermsId);
            modelMap.addAttribute("smtAgreeYN", smtAgreeYN);

            // 20221117
            List<ExtSmtAgreeTermsInfoDTO> smtAgreeItemList = smpTermsService
                    .getSmtAgreeItemList(custData.getCust_seq_no(), svcId);
            modelMap.addAttribute("smtAgreeItemList", smtAgreeItemList); // 셀러봇캐시 마케팅 동의 항목 리스트 가져오기

            modelMap.put("dashboardInfo", dashboardInfo);
            modelMap.put("recoInfo", recoInfo);
            modelMap.put("isExternalPopUp", isExternalPopUp);

            // 배너 정보
            List<ExtBannerDTO> bannerDataList = smpBannerService.getBannerData(EStorFileTyp.B0RR);
            List<ExtBannerDTO> bannerMainList = smpBannerService.getBannerData(EStorFileTyp.B0FF);
            modelMap.put("bannerDataList", bannerDataList);

            ExtBannerDTO bannerMainData = new ExtBannerDTO();
            for (ExtBannerDTO bannerMain : bannerMainList) {
                bannerMainData = bannerMain;
            }
            modelMap.put("bannerMainData", bannerMainData);

            return "new_main";
        } // user 정보 있을 경우

        // site 식별
        String where = request.getParameter("e");
        if (where != null) {
            String param = props.getFromWhere().get(where.toUpperCase());

            if (StringUtils.isNotBlank(param)) {
                session.setAttribute("siteCd", param);
                // promotion 식별
                // session.setAttribute("pe", request.getParameter("pe"));
                if ("001".equals(where)) { // IPK 배너(메인) 임시.
                    // TODO 운용배포 시 event_no 확인할 것.
                    session.setAttribute("pe", 52L);
                }
            }
        }

        // SC제일은행 URL 사용중
        if (request.getParameter("infPath") != null) {
            session.setAttribute("infPath", request.getParameter("infPath"));
        }
        List<ExtBannerDTO> bannerB4DDList = smpBannerService.getBannerData(EStorFileTyp.B4DD);
        List<ExtBannerDTO> bannerB4MMList = smpBannerService.getBannerData(EStorFileTyp.B4MM);
        modelMap.put("bannerB4DDList", bannerB4DDList);
        modelMap.put("bannerB4MMList", bannerB4MMList);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime msDate = LocalDateTime.of(LocalDate.now(), LocalTime.of(06, 00, 00)); // 06:00:00
        LocalDateTime meDate = LocalDateTime.of(LocalDate.now(), LocalTime.of(11, 59, 59)); // 11:59:59
        LocalDateTime asDate = LocalDateTime.of(LocalDate.now(), LocalTime.of(12, 00, 00)); // 12:00:00
        LocalDateTime aeDate = LocalDateTime.of(LocalDate.now(), LocalTime.of(17, 59, 59)); // 17:59:59

        List<ExtBannerDTO> bannerB4FFList = smpBannerService.getBannerData(EStorFileTyp.B4FF);
        ExtBannerDTO bannerB4FFData = new ExtBannerDTO();
        for (ExtBannerDTO bannerB4FF : bannerB4FFList) {
            if (now.isAfter(msDate) && now.isBefore(meDate)) { // 오전
                if (bannerB4FF.getBanner_ord() == 1) {
                    bannerB4FFData = bannerB4FF;
                }
            } else if (now.isAfter(asDate) && now.isBefore(aeDate)) { // 낮
                if ((bannerB4FF.getBanner_ord() == 2)) {
                    bannerB4FFData = bannerB4FF;
                }
            } else { // 저녁
                if ((bannerB4FF.getBanner_ord() == 3)) {
                    bannerB4FFData = bannerB4FF;
                }
            }
        }
        modelMap.put("bannerB4FFData", bannerB4FFData);

        return "index";
    }

    @PostMapping("/pub/trial")
    @ResponseBody
    public ResponseEntity<String> trial(String tgt_cust_no, HttpSession session) {
        boolean result = smpCustService.custTrial(tgt_cust_no);
        if (result) {
            return ResponseEntity.ok("OK");
        } else {
            return ResponseEntity.ok("USED");
        }
    }
}
