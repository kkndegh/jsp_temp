package onlyone.sellerbotcash.web.page;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.RequestContextUtils;

import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;

import com.fasterxml.jackson.core.JsonProcessingException;

import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.service.CustPointService;
import onlyone.sellerbotcash.service.GoodsService;
import onlyone.sellerbotcash.service.MypageService;
import onlyone.sellerbotcash.service.PaymentService;
import onlyone.sellerbotcash.service.SmpAccountService;
import onlyone.sellerbotcash.service.SmpAllianceService;
import onlyone.sellerbotcash.service.SmpBannerService;
import onlyone.sellerbotcash.service.SmpCommService;
import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.sellerbotcash.service.SmpLoanService;
import onlyone.sellerbotcash.service.SmpTermsService;
import onlyone.sellerbotcash.web.util.CustPayInfo;
import onlyone.sellerbotcash.web.util.DateUtil;
import onlyone.sellerbotcash.web.vm.ListData;
import onlyone.sellerbotcash.web.vm.Pagination;
import onlyone.smp.persistent.domain.enums.ECustPosi;
import onlyone.smp.persistent.domain.enums.EStorFileTyp;
import onlyone.smp.persistent.domain.enums.ETermsTyp;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ComboLoanDTO;
import onlyone.smp.service.dto.ExtAdvReqDTO;
import onlyone.smp.service.dto.ExtAgreeTermsReqDTO;
import onlyone.smp.service.dto.ExtBannerDTO;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtCusAcctDTO;
import onlyone.smp.service.dto.ExtCusAcctEditDTO;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtCustMallDTO;
import onlyone.smp.service.dto.ExtCustMallListDTO;
import onlyone.smp.service.dto.ExtCustPointHistResDTO;
import onlyone.smp.service.dto.ExtCustPointHistResListDTO;
import onlyone.smp.service.dto.ExtCustSvcDTO;
import onlyone.smp.service.dto.ExtCustTerminateDTO;
import onlyone.smp.service.dto.ExtGoodsInfoDTO;
import onlyone.smp.service.dto.ExtJoinCustMallListDTO;
import onlyone.smp.service.dto.ExtKkontTgtDTO;
import onlyone.smp.service.dto.ExtLoanCustMallDTO;
import onlyone.smp.service.dto.ExtLoanDTO;
import onlyone.smp.service.dto.ExtMallDTO;
import onlyone.smp.service.dto.ExtMaxRegCountResDTO;
import onlyone.smp.service.dto.ExtPayCancelReqDTO;
import onlyone.smp.service.dto.ExtPayHistListDTO;
import onlyone.smp.service.dto.ExtPayInfoDTO;
import onlyone.smp.service.dto.ExtPayInfoForCanDTO;
import onlyone.smp.service.dto.ExtReCustMallDTO;
import onlyone.smp.service.dto.ExtReCustMallReqDTO;
import onlyone.smp.service.dto.ExtReCustMallResDTO;
import onlyone.smp.service.dto.ExtReMallDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO;
import onlyone.smp.service.dto.ExtSvcTermsDTO;
import onlyone.smp.service.dto.ExtTrGoodsCateDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO.ExtSmtAgreeTermsInfoDTO;

@Slf4j
@Controller
public class MyPageController {

    @Value("${smp.svcId:sellerbotcash}")
    private String svcId;
    @Value("${internal.redirect.base:}")
    private String redirectBase;

    @Autowired
    private SmpCustService smpCustService;

    @Autowired
    private SmpCustMallService smpCustMallService;

    @Autowired
    private SmpJoinService smpJoinService;

    @Autowired
    private SmpAccountService smpAccountService;

    @Autowired
    private SmpCommService smpCommService;

    @Autowired
    private CustPointService custPointService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private MypageService mypageService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private CustPayInfo custPayInfo;

    @Autowired
    private SmpLoanService smpLoanService;

    @Autowired
    private SmpBannerService smpBannerService;

    @Autowired
    private SmpTermsService smpTermsService;

    @Autowired
    private SmpAllianceService smpAllianceService;

    @GetMapping("/sub/my/edit_info")
    public String getMypageCustInfo(HttpSession session, ModelMap modelMap) {
        // log.debug("** {}", session.getClass().getName());

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        ExtCustDTO custData = smpCustService.getCust(user.getUsername());
        modelMap.addAttribute("cust", custData);

        if (session.getAttribute("confirmPw") == null) {
            return "redirect:/";
        } else if (!session.getAttribute("confirmPw").equals(true)) {
            return "redirect:/";
        }

        List<ExtSvcTermsDTO> list = smpJoinService.getSellerBotTerms(svcId);

        boolean smtUseChk = false; // 셀러봇 마케팅 동의 활성화 여부 체크
        for (ExtSvcTermsDTO extSvcTermsDTO : list) {
            if (extSvcTermsDTO.getTerms_typ_cd() == ETermsTyp.MT3) {
                modelMap.put("MT3TermsEsseYn", extSvcTermsDTO.getTerms_esse_yn());
                modelMap.put("MT3TermsId", extSvcTermsDTO.getTerms_id());
                modelMap.put("MT3TermsCont", extSvcTermsDTO.getTerms_cont());
                modelMap.put("MT3TermsTitle", extSvcTermsDTO.getTerms_title());
            }

            if (extSvcTermsDTO.getTerms_typ_cd() == ETermsTyp.SMT) {
                modelMap.put("SMTTermsEsseYn", extSvcTermsDTO.getTerms_esse_yn());
                modelMap.put("SMTTermsId", extSvcTermsDTO.getTerms_id());
                modelMap.put("SMTTermsCont", extSvcTermsDTO.getTerms_cont());
                modelMap.put("SMTTermsTitle", extSvcTermsDTO.getTerms_title());

                smtUseChk = true;
            }
        }

        // 20221117 셀러봇캐시 마케팅 활용 세부 항목 가져오기 추가
        List<ExtCommCdDTO> smtTermsList = new ArrayList<ExtCommCdDTO>();
        if (smtUseChk) {
            smtTermsList = smpCommService.getCodeList("SMT");
            smtTermsList = smtTermsList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord).reversed())
                    .collect(Collectors.toList());
        }
        modelMap.addAttribute("smtTermsList", smtTermsList); // 셀러봇캐시 마케팅 활용 세부 항목

        // 20221117
        List<ExtSmtAgreeTermsInfoDTO> smtAgreeItemList = smpTermsService.getSmtAgreeItemList(custData.getCust_seq_no(),
                svcId);
        modelMap.addAttribute("smtAgreeItemList", smtAgreeItemList); // 셀러봇캐시 마케팅 동의 항목 리스트 가져오기

        session.removeAttribute("confirmPw");
        return "mypage/edit_info";
    }

    @PostMapping("/sub/my/edit")
    public ResponseEntity<String> getMypageEdit(String ceo_no, String chrg_nm, String chrg_no, Long MT3TermsId,
            String chkVal, Long SMTTermsId, String smtChkFlag, String[] smtChkArr, HttpSession session) {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        List<ExtKkontTgtDTO> list = smpCustService.getAlarm(user.getUsername());
        ExtKkontTgtDTO extKkontTgtDTO = new ExtKkontTgtDTO();

        if (list != null && list.size() > 0) {
            for (ExtKkontTgtDTO dto : list) {
                if (dto.getCust_posi_cd().equals(ECustPosi.CEO)) {
                    extKkontTgtDTO = dto;
                    extKkontTgtDTO.setTgt_cust_no(ceo_no);
                    break;
                }
            }
            if (smpCustService.alarm_update(user.getUsername(), extKkontTgtDTO)) {
                smpCustService.editCust(user.getUsername(), ceo_no, chrg_nm, chrg_no);

            }
        } else {
            smpCustService.editCust(user.getUsername(), ceo_no, chrg_nm, chrg_no);
        }

        List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();
        ExtAgreeTermsReqDTO termsdto = new ExtAgreeTermsReqDTO();

        termsdto.setCust_id(user.getUsername());
        termsdto.setSvc_id(svcId);
        termsdto.setTerms_id(MT3TermsId);
        terms.add(termsdto);

        boolean termschk = false;
        if (chkVal.equals("true")) { // 미체크된 정보에서 채크로 변경
            termschk = smpJoinService.addTerms(terms);
        } else if (chkVal.equals("false")) { // 체크된 정보에서 미체크로 변경
            termschk = smpJoinService.delTerms(terms);
        } else if (chkVal.equals("fixed")) { // 아무 변경이 없을 경우
            termschk = true;
        }

        if (!termschk) {
            return ResponseEntity.badRequest().body("TermsErrors");
        }

        ExtCustDTO custData = smpCustService.getCust(user.getUsername());

        // 2022118 셀러봇캐시 마케팅 활용 동의 항목
        ExtSmtAgreeTermsDTO d = new ExtSmtAgreeTermsDTO();
        d.setCust_id(custData.getCust_id());
        d.setCust_seq_no(custData.getCust_seq_no());
        d.setSvc_id(svcId);

        List<ExtSmtAgreeTermsInfoDTO> selectedSmtTerms = new ArrayList<ExtSmtAgreeTermsInfoDTO>();

        if (!StringUtils.isEmpty(smtChkArr)) {
            for (String smtTermsCd : smtChkArr) {
                ExtSmtAgreeTermsInfoDTO selectedSmtTerm = new ExtSmtAgreeTermsInfoDTO();
                selectedSmtTerm.setTerms_id(SMTTermsId);
                selectedSmtTerm.setSmt_terms_cd(smtTermsCd);
                selectedSmtTerm.setSmt_terms_aggr_yn(EYesOrNo.Y);
                selectedSmtTerm.setReg_id(user.getUsername());
                selectedSmtTerm.setMod_id(user.getUsername());

                selectedSmtTerms.add(selectedSmtTerm);
            }
        }
        d.setSmt_aggr_item_list(selectedSmtTerms);

        List<ExtAgreeTermsReqDTO> smtTerms = new ArrayList<ExtAgreeTermsReqDTO>();
        ExtAgreeTermsReqDTO smtTermsdto = new ExtAgreeTermsReqDTO();
        smtTermsdto.setCust_id(user.getUsername());
        smtTermsdto.setSvc_id(svcId);
        smtTermsdto.setTerms_id(SMTTermsId);

        smtTerms.add(smtTermsdto);

        if (smtChkFlag.equals("true") && selectedSmtTerms.size() < 1) {
            // 셀러봇 캐시 마케팅 활용 동의를 하였지만 모든 항목을 해지 한 경우 삭제
            termschk = smpJoinService.delTerms(smtTerms); // 셀러 이용약관 테이블 삭제
        } else if (smtChkFlag.equals("false") && selectedSmtTerms.size() > 0) {
            // 셀러봇 캐시 마케팅 활용 미동의를 하였지만 항목을 선택 한 경우 등록
            termschk = smpJoinService.addTerms(smtTerms); // 셀러 이용약관 테이블 추가
        }
        termschk = smpTermsService.saveSmtAgreeItem(d); // 셀러봇캐시 마케팅 동의 항목 관리

        if (!termschk) {
            return ResponseEntity.badRequest().body("TermsErrors");
        }

        return ResponseEntity.ok("");
    }

    @GetMapping("/sub/my/confirm_pw")
    public String confirm_pw(HttpSession session) {
        return "mypage/confirm_pw";
    }

    @PostMapping("/sub/my/check_pw")
    public @ResponseBody HashMap<String, Object> check_pw(String password, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        password = StringEscapeUtils.unescapeHtml(password);
        HashMap<String, Object> map = smpCustService.sellerConfirm(user.getUsername(), password);
        if (map.get("result").equals(true)) {
            boolean confirmPw = true;
            session.setAttribute("confirmPw", confirmPw);
        }
        return map;
    }

    @PostMapping("/sub/my/confirm/pw")
    public @ResponseBody HashMap<String, Object> confirmPw(String passwd, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        HashMap<String, Object> map = smpCustService.sellerConfirm(user.getUsername(), passwd);
        return map;
    }

    @GetMapping("/sub/my/change_pw")
    public String change_pw(HttpSession session, ModelMap modelMap) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        session.setAttribute("cust", smpCustService.getCust(user.getUsername()));

        return "mypage/change_pw";
    }

    @PostMapping("/sub/my/password")
    public ResponseEntity<String> password(String passwd, String new_passwd, String new_passwd_confirm,
            HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        String result = smpCustService.change_pw(user.getUsername(), passwd, new_passwd, new_passwd_confirm);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/sub/my/use_id")
    public String use_id(HttpSession session, ModelMap modelMap) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        List<ExtCustSvcDTO> svcList = smpCustService.getSvcList(user.getUsername());
        modelMap.addAttribute("svcList", svcList);
        return "mypage/use_id";
    }

    @GetMapping("/sub/my/ask")
    public String getAskList(ModelMap modelMap, HttpSession session, @RequestParam(defaultValue = "0") int page) {
        int queryPage = page > 0 ? page - 1 : 0;
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        ListData<ExtAdvReqDTO> reqList = smpCustService.getReqList(queryPage, user.getUsername());
        Pagination pagingDto = new Pagination();
        pagingDto.setBlockSize(5);
        pagingDto.setPageNo(reqList.getPage() + 1);
        pagingDto.setTotalCount(reqList.getTotalCount());
        modelMap.addAttribute("paging", pagingDto);
        modelMap.addAttribute("reqList", reqList);
        return "mypage/ask";
    }

    @GetMapping("/sub/my/register_ask")
    public String register_ask(HttpSession session, ModelMap modelMap) {
        return "mypage/register_ask";
    }

    @GetMapping("/sub/my/ask_edit")
    public String ask_edit(HttpSession session, ModelMap modelMap, String adv_req_seq_no) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        ExtAdvReqDTO req = smpCustService.getReq(adv_req_seq_no, user.getUsername());
        modelMap.addAttribute("req", req);
        return "mypage/ask_edit";
    }

    @PostMapping("/sub/my/ask_save")
    public ResponseEntity<String> save(String adv_typ_cd, String adv_title, String adv_cont, MultipartFile adv_req_file,
            HttpSession session) {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        boolean result;
        try {
            result = smpCustService.ask_save(user.getUsername(), adv_typ_cd, adv_title, adv_cont, adv_req_file);
            if (result) {
                return ResponseEntity.ok("OK");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Save Error");
        }
        return ResponseEntity.badRequest().body("Save Error");
    }

    @PostMapping("/sub/my/ask_update")
    public ResponseEntity<String> update(String adv_typ_cd, String adv_title, String adv_cont,
            MultipartFile adv_req_file, String adv_req_seq_no, String adv_req_file_del_yn, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        boolean result;
        try {
            result = smpCustService.ask_update(user.getUsername(), adv_typ_cd, adv_title, adv_cont, adv_req_file,
                    adv_req_seq_no, adv_req_file_del_yn);
            if (result) {
                return ResponseEntity.ok("OK");
            }
        } catch (IOException e) {
            return ResponseEntity.badRequest().body("Save Error");
        }
        return ResponseEntity.badRequest().body("Save Error");
    }

    @GetMapping("/sub/my/alarm")
    public String alarm(HttpSession session, ModelMap modelMap) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        modelMap.addAttribute("cust", smpCustService.getCust(user.getUsername()));
        List<ExtKkontTgtDTO> dtoList = smpCustService.getAlarm(user.getUsername());
        if (dtoList != null) {
            for (ExtKkontTgtDTO dto : dtoList) {
                if (dto.getCust_posi_cd() == ECustPosi.CEO) {
                    modelMap.addAttribute("ceoData", dto);
                } else if (dto.getCust_posi_cd() == ECustPosi.CRG) {
                    modelMap.addAttribute("crgData", dto);
                }
            }
        }

        return "mypage/alarm";
    }

    @PostMapping("/sub/my/alarm_input")
    @ResponseBody
    public boolean alarmInput(@RequestBody List<ExtKkontTgtDTO> params) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        boolean rtn = true;
        for (ExtKkontTgtDTO extKkontTgtDTO : params) {
            if (extKkontTgtDTO.getKkont_tgt_no() != null) {
                rtn = smpCustService.alarm_update(user.getUsername(), extKkontTgtDTO);
            } else {
                rtn = smpCustService.alarm_insert(user.getUsername(), extKkontTgtDTO);
            }
        }
        return rtn;
    }

    @GetMapping("/sub/my/market")
    @ExceptionHandler(Exception.class)
    public String market(HttpSession session, ModelMap modelMap, String cust_mall_seq_no)
            throws JsonProcessingException {

        return "redirect:" + redirectBase + "/sub/my/join/market";

    }

    /**
     * 20211007 판매몰 현황 추가
     * 
     * @param session
     * @param modelMap
     * @param cust_mall_seq_no
     * @return
     * @throws JsonProcessingException
     */
    @GetMapping("/sub/my/marketRegList")
    @ExceptionHandler(Exception.class)
    public String marketRegList(HttpSession session, ModelMap modelMap, String cust_mall_seq_no)
            throws JsonProcessingException {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        ExtCustMallListDTO custMall = smpCustMallService.getCustMall(user.getUsername());

        if (custMall.getMall().size() == 0) {
            System.out.println(custMall.getMall().size());
        }

        custMall.setMall(custMall.getMall().stream().filter(mall -> mall.getDel_yn().equals(EYesOrNo.N))
                .collect(Collectors.toList()));

        modelMap.addAttribute("custMall", custMall);

        List<ExtMallDTO> extMallDTOList = smpJoinService.getMallList();
        modelMap.addAttribute("mallList", extMallDTOList);
        modelMap.addAttribute("categoryList", smpCommService.getCategoryList());

        ExtLoanDTO loanBatchProTgtInfo = smpLoanService.getLoanBatchProTgtInfo(user.getUsername());
        modelMap.addAttribute("loanBatchProTgtInfo", loanBatchProTgtInfo);

        // 금융 상품 사용중인 판매몰 리스트
        List<ExtLoanCustMallDTO> loanCustUseMallList = smpLoanService.getLoanCustUseMallList(user.getUsername());
        modelMap.addAttribute("loanCustUseMallList", loanCustUseMallList);

        return "mypage/marketRegList";

    }

    @ResponseBody
    @PostMapping(value = "/sub/loan/getLoanInfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getLoanInfo(@AuthenticationPrincipal User user,
            @RequestBody List<ExtLoanCustMallDTO> extLoanCustMallDTO, HttpSession session) {
        boolean result = smpLoanService.createMall(user.getUsername(), extLoanCustMallDTO);

        if (result) {
            return ResponseEntity.ok("{\"result\":\"OK\"}");

        } else {
            return ResponseEntity.badRequest().body("ERROR");
        }
    }

    @PostMapping("/sub/member/delete_mall")
    public ResponseEntity<String> delete_mall(String cust_mall_seq_no, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        // 삭제 불가 마켓 정보
        List<ExtLoanCustMallDTO> loanCustUseMallList = smpLoanService.getLoanCustUseMallList(user.getUsername());
        for (ExtLoanCustMallDTO loanCustUseMall : loanCustUseMallList) {
            if (Objects.toString(loanCustUseMall.getCust_mall_seq_no(), "").equals(cust_mall_seq_no)) {
                return ResponseEntity.badRequest().body("can_not_delete");
            }
        }

        boolean result;
        result = smpCustMallService.deleteMall(user.getUsername(), cust_mall_seq_no);
        if (result) {
            return ResponseEntity.ok("OK");
        } else {
            return ResponseEntity.badRequest().body("Save Error");
        }
    }

    @PostMapping("/sub/member/getMallInfo")
    public @ResponseBody ExtCustMallDTO getMallInfo(String cust_mall_seq_no, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        ExtCustMallDTO extCustMallDTO = smpCustMallService.getMallInfo(user.getUsername(), cust_mall_seq_no);

        return extCustMallDTO;
    }

    @GetMapping("/sub/my/leave_frm")
    public String leave_frm(HttpSession session, ModelMap modelMap) {
        boolean svcChk = false;
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        String userName = user.getUsername();

        List<ExtCustSvcDTO> svcList = smpCustService.getSvcList(userName);
        for (ExtCustSvcDTO extCustSvcDTO : svcList) {
            if (extCustSvcDTO.getSvc_join_dt() != null) {
                svcChk = true;
            }
        }

        String isPayingUser = EYesOrNo.N.toString();
        if (custPayInfo.isPayingUser(session, user.getUsername()))
            isPayingUser = EYesOrNo.Y.toString();

        ExtLoanDTO etlDto = smpLoanService.getLoanBatchProTgtInfo(user.getUsername());
        // 탈퇴신청여부 , 소드 서비스 사용 여부 확인
        Map<String, Object> chkMap = new HashMap<String, Object>();
        chkMap = smpCustService.getCustSodeTerminateChk(userName);

        modelMap.addAttribute("sodeChk", (String) chkMap.get("sodeChk"));
        modelMap.addAttribute("reqChk", (String) chkMap.get("reqChk"));

        modelMap.addAttribute("batchProTgtList", etlDto.getBatchProTgt_list());
        modelMap.addAttribute("scbLoanCnt", etlDto.getScb_loan_cnt());

        modelMap.addAttribute("isPayingUser", isPayingUser);
        modelMap.addAttribute("svcChk", svcChk);
        modelMap.addAttribute("cust", smpCustService.getCust(user.getUsername()));
        return "mypage/leave_frm";
    }

    @PostMapping("/sub/my/leave")
    @ResponseBody
    public ResponseEntity<String> leave(ExtCustTerminateDTO dto, HttpSession session, HttpServletRequest request)
            throws JsonProcessingException {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        // 2022118 셀러봇캐시 마케팅 활용 동의 항목 해지
        ExtCustDTO custData = smpCustService.getCust(user.getUsername());

        ExtSmtAgreeTermsDTO d = new ExtSmtAgreeTermsDTO();
        d.setCust_id(custData.getCust_id());
        d.setCust_seq_no(custData.getCust_seq_no());
        d.setSvc_id(svcId);
        d.setSmt_aggr_item_list(new ArrayList<ExtSmtAgreeTermsInfoDTO>());

        if (!smpTermsService.saveSmtAgreeItem(d)) { // 셀러봇캐시 마케팅 동의 항목 관리
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

        if (custPayInfo.isPayingUser(session, user.getUsername())) {
            ExtPayCancelReqDTO params = new ExtPayCancelReqDTO();
            params.setCanc_reas_cd(dto.getCanc_reas_cd());
            params.setCanc_dti_reas(dto.getCanc_dti_reas());
            params.setCust_id(user.getUsername());

            ResponseEntity<String> result = paymentService.postPaymentCancelForInicis(params);
            if (result.getStatusCode().equals(HttpStatus.OK))
                custPayInfo.updateSessionInfoAboutPay(session, user.getUsername());
            else
                return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

        dto.setCust_id(user.getUsername());
        boolean result = smpCustService.custTerminate(dto);

        if (result) {
            return new ResponseEntity<String>(HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/sub/my/sodeCust/terminateReq")
    @ResponseBody
    public ResponseEntity<String> terminateReq(ExtCustTerminateDTO dto, HttpSession session, HttpServletRequest request)
            throws JsonProcessingException, URISyntaxException {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        ExtCustDTO cust = smpCustService.getCust(user.getUsername());
        Map<String, String> resultMap = new HashMap<String, String>();

        // 소드원 탈퇴 요청
        resultMap = smpCustService.sodeCustTerminateReq(cust);

        if (resultMap.get("result_code").equals("D000")) {
            boolean save = smpCustService.insertTsCustTerminateProStat(user.getUsername());
            log.info("save ={}", save);
            return new ResponseEntity<String>(HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }


    }

    @GetMapping("/sub/my/leaveSuccess")
    public String leaveSuccess(HttpServletRequest request) {
        try {
            request.logout();
            HttpSession session = request.getSession();
            session.invalidate();
            SecurityContextHolder.clearContext();
        } catch (ServletException e) {
            log.error("" + e);
        }

        return "mypage/leave";
    }

    /**
     * 정산계좌 관리 > 등록
     * 
     * @param session
     * @param modelMap
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/sub/my/account", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> accountRegister(@AuthenticationPrincipal User user,
            @Valid @RequestBody List<ExtCusAcctEditDTO> dtoList) {
        return smpAccountService.addCustAcct(user.getUsername(), dtoList);
    }

    /**
     * 정산계좌 관리 > 삭제
     * 
     * @param session
     * @param modelMap
     * @return
     */
    @ResponseBody
    @DeleteMapping(value = "/sub/my/account/{cust_acct_seq_no}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> frmAccountDelete(@AuthenticationPrincipal User user,
            @PathVariable("cust_acct_seq_no") Long cust_acct_seq_no) {
        return smpAccountService.deleteCustAcct(user.getUsername(), cust_acct_seq_no);
    }

    /**
     * 마이페이지 -> 결제정보의 이용 정보요약 탭
     * 
     * @param modelMap
     * @param user
     * @return
     */
    @GetMapping("/sub/my/paidInfo")
    public String getPaidInfoView(HttpServletRequest request, ModelMap modelMap, @AuthenticationPrincipal User user,
            HttpSession session, Device device) {
        // 이용권 변경 결과 값 반환
        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        if (map != null) {
            modelMap.put("successYN", map.get("successYN"));
            modelMap.put("errMsg", map.get("errMsg"));
        }

        String userName = user.getUsername();

        ExtCustDTO cust = smpCustService.getCust(userName);
        if (cust == null) {
            throw new RuntimeException(user.getUsername() + " not found!");
        }

        // 이용 정보 요약
        ExtPayInfoDTO newPaidInfo = mypageService.getPayInfo(userName);
        modelMap.addAttribute("paidInfo", newPaidInfo);

        // 소멸 예정 포인트 (60일 이내)
        final int period = 60;
        LocalDate today = LocalDate.now();
        String startDt = DateUtil.getDateString(today);
        String endDt = DateUtil.getDateString(today.plusDays(period - 1));

        ExtCustPointHistResDTO poiHistInfo = custPointService.getExpiryPointList(userName, startDt, endDt);
        modelMap.addAttribute("poiHistInfo", poiHistInfo);

        // 제휴 별 상품을 구분하기 위해 파라미터 셋팅
        String infSiteCd = cust.getInf_site_cd() != null ? cust.getInf_site_cd().name() : "";
        Long custSeqNo = cust.getCust_seq_no();

        // 상품 정보 조회
        // ExtGoodsInfoDTO goodsInfo =
        // goodsService.getGoodsListForCust(cust.getCust_seq_no());
        ExtGoodsInfoDTO goodsInfo = goodsService.getGoodsListForCust(infSiteCd, custSeqNo);

        boolean promoEligible = smpCustService.getCouponIssuanceChk(String.valueOf(custSeqNo));
        modelMap.addAttribute("promoEligible", promoEligible);
        // 사용중인 상품이 프로모션 상품인지 확인
        boolean isPromoChk = goodsService.getUsingGoodsPromotionChk(custSeqNo);
        modelMap.addAttribute("isPromoChk", isPromoChk);

        // modelMap.addAttribute("goodsInfo", goodsInfo);

        if (goodsInfo.getBasic_goods_info() != null) {
            modelMap.addAttribute("RNB", goodsInfo.getBasic_goods_info().get("RNB"));
            modelMap.addAttribute("PYB", goodsInfo.getBasic_goods_info().get("PYB"));
            modelMap.addAttribute("PASB", goodsInfo.getBasic_goods_info().get("PASB"));
            modelMap.addAttribute("PASP", goodsInfo.getBasic_goods_info().get("PASP"));
            modelMap.addAttribute("PAFP", goodsInfo.getBasic_goods_info().get("PAFP"));
        } else {
            modelMap.addAttribute("RNB", null);
            modelMap.addAttribute("PYB", null);
            modelMap.addAttribute("PASB", null);
            modelMap.addAttribute("PASP", null);
            modelMap.addAttribute("PAFP", null);
        }

        // try {
        // modelMap.addAttribute("basic_goods",
        // StringEscapeUtils.escapeHtml(objectMapper.writeValueAsString(goodsInfo.getBasic_goods())));
        // } catch (Exception e) {}
        modelMap.addAttribute("basic_goods", goodsInfo.getBasic_goods());
        // modelMap.addAttribute("isEvent", goodsInfo.getEventNo() > 0 ? "Y" : "N");
        modelMap.addAttribute("eventNo", goodsInfo.getEventNo());

        modelMap.addAttribute("mid", SellerbotConstant.INICIS_MID);
        modelMap.addAttribute("mKey", SellerbotConstant.INICIS_MKEY);
        modelMap.addAttribute("buyername", cust.getCeo_nm());
        modelMap.addAttribute("buyertel", cust.getCeo_no());
        modelMap.addAttribute("buyeremail", cust.getCust_id());
        modelMap.addAttribute("siteDomain", session.getAttribute("siteDomain"));

        // 등록된 몰의 개수 조회 (전체 몰 - 삭제된 몰)
        ExtCustMallListDTO custMallInfo = smpCustMallService.getCustMall(userName);
        int notDelMallCount = 0;
        if (Objects.nonNull(custMallInfo))
            notDelMallCount = custMallInfo.getAll_mall_cnt() - custMallInfo.getDel_mall_cnt();

        // 몰 등록 가능 여부 확인
        String isRegMall = EYesOrNo.Y.toString();
        if (goodsInfo.getBasic_goods_info() != null) {
            if (notDelMallCount > goodsInfo.getBasic_goods_info().get("PASB").getSALE_MALL_REG_ID_PSB_CNT())
                isRegMall = EYesOrNo.N.toString();
        }

        // 등록된 계좌의 개수 조회 (전체 계좌 - 삭제된 계좌)
        ExtCusAcctDTO extCusAcctDTO = smpAccountService.getCustAcctList(userName);
        int notDelAcctCount = 0;
        if (Objects.nonNull(extCusAcctDTO))
            notDelAcctCount = extCusAcctDTO.getAll_acct_cnt() - extCusAcctDTO.getDel_acct_cnt();

        // 계좌 등록 가능 여부 확인
        String isRegAcct = EYesOrNo.Y.name();
        if (goodsInfo.getBasic_goods_info() != null) {
            if (notDelAcctCount > goodsInfo.getBasic_goods_info().get("PASB").getSETT_ACC_ACCT_REG_PSB_CNT())
                isRegAcct = EYesOrNo.N.toString();
        }

        modelMap.addAttribute("isRegMall", isRegMall);
        modelMap.addAttribute("isRegAcct", isRegAcct);

        if (device.isNormal()) {
            modelMap.addAttribute("DeviceType", "PC");
        } else {
            modelMap.addAttribute("DeviceType", "Other");
        }

        return "mypage/paidInfo";
    }

    /**
     * 이용권 변경 신청
     * 
     * @param reserv_poi
     * @param user
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping({ "/sub/my/changeTicket", "/sub/my/changeTicket/{refund}" })
    @ResponseBody
    public ResponseEntity<String> changeTicket(HttpSession session, @AuthenticationPrincipal User user,
            @ModelAttribute("goods_req_seq_no") String goods_req_seq_no,
            @ModelAttribute("new_goods_opt_seq_no") String new_goods_opt_seq_no, @PathVariable Optional<String> refund)
            throws JsonProcessingException {
        return paymentService.postChangeTicket(user.getUsername(), goods_req_seq_no, new_goods_opt_seq_no,
                "refund".equals(refund.orElse(null)));
    }

    /**
     * 포인트 사용예약 등록
     * 
     * @param reserv_poi
     * @param user
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/sub/my/reservPoi")
    @ResponseBody
    public ResponseEntity<String> addCustPointReserv(@ModelAttribute("reserv_poi") String reserv_poi,
            @AuthenticationPrincipal User user) throws JsonProcessingException {
        String userName = user.getUsername();
        return custPointService.addCustPointReserv(userName, reserv_poi);
    }

    /**
     * 포인트 사용예약 취소
     * 
     * @param reserv_poi
     * @param user
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/sub/my/cancelReservPoi")
    @ResponseBody
    public ResponseEntity<String> cancelCustPointReserv(@ModelAttribute("use_reserv_seq_no") Long use_reserv_seq_no)
            throws JsonProcessingException {
        return custPointService.cancelCustPointReserv(use_reserv_seq_no);
    }

    /**
     * 마이페이지 -> 결제정보의 결제 내역 탭
     * 
     * @param modelMap
     * @param user
     * @return
     */
    @GetMapping("/sub/my/payHist")
    public String getPaymentHistoryView(ModelMap modelMap, @AuthenticationPrincipal User user) {
        return "mypage/payHist";
    }

    /**
     * 마이페이지 -> 결제정보의 결제 내역 리스트 조회
     * 
     * @param start_dt
     * @param end_dt
     * @param page
     * @param size
     * @param user
     * @return
     */
    @GetMapping("/sub/my/payHistList")
    @ResponseBody
    public ListData<ExtPayHistListDTO> getPaymentHistList(String start_dt, String end_dt, int page, int size,
            @AuthenticationPrincipal User user) {
        String userName = user.getUsername();
        ListData<ExtPayHistListDTO> histList = mypageService.getPaymentHistory(userName, start_dt, end_dt, page, size);
        return histList;
    }

    /**
     * 마이페이지 -> 결제정보의 S포인트 내역 탭
     * 
     * @param modelMap
     * @param user
     * @return
     */
    // @GetMapping("/sub/my/poiHist")
    // public String getPointHistoryView(ModelMap modelMap, @AuthenticationPrincipal
    // User user) {
    // return "mypage/poiHist";
    // }

    /**
     * 마이페이지 -> 결제정보의 S포인트 리스트 조회
     * 
     * @param start_dt
     * @param end_dt
     * @param page
     * @param size
     * @param user
     * @return
     */
    @GetMapping("/sub/my/poiHistList")
    @ResponseBody
    public ListData<ExtCustPointHistResListDTO> getPointHistList(String start_dt, String end_dt, int page, int size,
            @AuthenticationPrincipal User user) {
        String userName = user.getUsername();
        ListData<ExtCustPointHistResListDTO> histList = custPointService.getCustPointHistList(userName, start_dt,
                end_dt, page, size);
        return histList;
    }

    /**
     * 이용권 해지 신청 화면
     * 
     * @param modelMap
     * @param user
     * @return
     */
    @GetMapping("/sub/my/paidCancel")
    public String getPaidCancelView(ModelMap modelMap,
            @RequestParam(value = "goodsReqSeqNo", required = false) Long goodsReqSeqNo,
            @AuthenticationPrincipal User user) {
        ExtPayInfoForCanDTO paidInfo = mypageService.getPayInfoForCan(user.getUsername(), goodsReqSeqNo);

        modelMap.addAttribute("paidInfo", paidInfo);
        modelMap.addAttribute("goods_req_seq_no", goodsReqSeqNo);
        // TODO: 테스트용 코드. 운용 배포 시 반드시 ("cert", "N") 으로 변경 할 것
        modelMap.addAttribute("cert", "N");

        return "mypage/paidCancel";
    }

    /**
     * 이용권 해지 신청
     * 
     * @param reserv_poi
     * @param user
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/sub/my/paidCancel")
    @ResponseBody
    public ResponseEntity<String> cancelPaidInfo(@ModelAttribute("params") ExtPayCancelReqDTO params,
            @AuthenticationPrincipal User user, HttpSession session) throws JsonProcessingException {
        String userName = user.getUsername();

        params.setCust_id(userName);

        ResponseEntity<String> result = paymentService.postPaymentCancelForInicis(params);

        if (result.getStatusCode().equals(HttpStatus.OK))
            custPayInfo.updateSessionInfoAboutPay(session, userName);

        return result;
    }

    @GetMapping("/sub/my/join/market")
    @ExceptionHandler(Exception.class)
    public String testPubMarket(HttpSession session, ModelMap modelMap, String cust_mall_seq_no)
            throws JsonProcessingException {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        // 금융상품 서비스 콤보 리스트
        List<ComboLoanDTO> selectList = smpLoanService.getComboLoanInfo(user.getUsername());
        modelMap.put("selectList", selectList);

        ExtLoanDTO loanBatchProTgtInfo = smpLoanService.getLoanBatchProTgtInfo(user.getUsername());
        modelMap.addAttribute("loanBatchProTgtInfo", loanBatchProTgtInfo);

        // 현재 이용권으로 등록 가능한 수량 조회
        String userName = user.getUsername();
        ExtMaxRegCountResDTO maxRegCountInfo = paymentService.getMaxRegCountInfo(userName);
        modelMap.addAttribute("maxRegCountInfo", maxRegCountInfo);

        // 배너 정보(판매몰관리우측)
        List<ExtBannerDTO> bannerB2RRDataList = smpBannerService.getBannerData(EStorFileTyp.B2RR);
        modelMap.put("bannerB2RRDataList", bannerB2RRDataList);

        return "mypage/joinMarket";

    }

    @GetMapping("/sub/my/join/custMallList")
    @ResponseBody
    public ExtJoinCustMallListDTO custMallList(ExtJoinCustMallListDTO extJoinCustMallListDTO) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        extJoinCustMallListDTO = smpCustMallService.getJoinCustMall(user.getUsername());

        return extJoinCustMallListDTO;
    }

    /**
     * 몰 저장 / 수정
     * 
     * @param mallData
     * @return
     */
    @PostMapping("/sub/my/join/custMallSave")
    @ResponseBody
    public List<ExtReCustMallResDTO> custMallSaveAndUpdate(@RequestBody List<ExtReCustMallReqDTO> mallData,
            HttpSession session) {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        List<ExtReCustMallResDTO> saveList = new ArrayList<>();
        log.info("mallData.toString() = {} ", mallData.toString());
        saveList = smpCustMallService.saveJoinCustMall(mallData, user.getUsername());

        session.setAttribute("cust", smpCustService.getCust(user.getUsername()));

        return saveList;
    }

    @PostMapping("/sub/my/join/custMallDelete")
    @ResponseBody
    public ExtReCustMallResDTO custMallDelete(String cust_mall_seq_no) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        ExtReCustMallResDTO extReCustMallResDTO = new ExtReCustMallResDTO();
        extReCustMallResDTO = smpCustMallService.deleteJoinCustMall(cust_mall_seq_no, user.getUsername());
        return extReCustMallResDTO;
    }

    @GetMapping("/sub/my/join/mallList")
    @ResponseBody
    public List<ExtReMallDTO> mallList(String mallOpenYn) {
        List<ExtReMallDTO> mallDTOs = smpCustMallService.getMallList(mallOpenYn);
        return mallDTOs;
    }

    @GetMapping("/sub/my/join/categoryList")
    @ResponseBody
    public List<ExtTrGoodsCateDTO> categoryList() {
        return smpCommService.getCategoryList();
    }

    @GetMapping("/sub/my/join/getMallInfo")
    @ResponseBody
    public ExtReCustMallDTO mallInfo(String cust_mall_seq_no) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        ExtReCustMallDTO mallInfo = smpCustMallService.getJoinMallInfo(cust_mall_seq_no, user.getUsername());
        return mallInfo;
    }

    @ResponseBody
    @PostMapping(value = "/sub/loan/getLoanCustMallList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ExtLoanDTO getLoanCustMallList(@AuthenticationPrincipal User user, @RequestBody String loan_svc_id,
            HttpSession session) {
        ExtLoanDTO loanBatchProTgtInfo = smpLoanService.getLoanProTgtInfo(user.getUsername(), loan_svc_id);

        return loanBatchProTgtInfo;
    }

    @PostMapping("/sub/my/smtTerm")
    public ResponseEntity<String> getSMTtermsEdit(String ceo_nm, String ceo_no, Long SMTTermsId, String smtChkFlag,
            String[] smtChkArr, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();
        ExtAgreeTermsReqDTO termsdto = new ExtAgreeTermsReqDTO();

        termsdto.setCust_id(user.getUsername());
        termsdto.setSvc_id(svcId);
        terms.add(termsdto);

        boolean termschk = false;

        ExtCustDTO custData = smpCustService.getCust(user.getUsername());

        List<ExtKkontTgtDTO> list = smpCustService.getAlarm(user.getUsername());
        ExtKkontTgtDTO extKkontTgtDTO = new ExtKkontTgtDTO();

        if (list != null && list.size() > 0) {
            for (ExtKkontTgtDTO dto : list) {
                if (dto.getCust_posi_cd().equals(ECustPosi.CEO)) {
                    extKkontTgtDTO = dto;
                    extKkontTgtDTO.setTgt_cust_no(ceo_no);
                    break;
                }
            }
            if (smpCustService.alarm_update(user.getUsername(), extKkontTgtDTO)) {
                smpCustService.editSmtCust(user.getUsername(), ceo_nm, ceo_no);

            }
        } else if (custData.getCeo_nm() != ceo_nm || custData.getCeo_no() != ceo_no) {
            smpCustService.editSmtCust(user.getUsername(), ceo_nm, ceo_no);
        }

        // 2022118 셀러봇캐시 마케팅 활용 동의 항목
        ExtSmtAgreeTermsDTO d = new ExtSmtAgreeTermsDTO();
        d.setCust_id(custData.getCust_id());
        d.setCust_seq_no(custData.getCust_seq_no());
        d.setSvc_id(svcId);

        List<ExtSmtAgreeTermsInfoDTO> selectedSmtTerms = new ArrayList<ExtSmtAgreeTermsInfoDTO>();

        if (!StringUtils.isEmpty(smtChkArr)) {
            for (String smtTermsCd : smtChkArr) {
                ExtSmtAgreeTermsInfoDTO selectedSmtTerm = new ExtSmtAgreeTermsInfoDTO();
                selectedSmtTerm.setTerms_id(SMTTermsId);
                selectedSmtTerm.setSmt_terms_cd(smtTermsCd);
                selectedSmtTerm.setSmt_terms_aggr_yn(EYesOrNo.Y);
                selectedSmtTerm.setReg_id(user.getUsername());
                selectedSmtTerm.setMod_id(user.getUsername());

                selectedSmtTerms.add(selectedSmtTerm);
            }
        }
        d.setSmt_aggr_item_list(selectedSmtTerms);

        List<ExtAgreeTermsReqDTO> smtTerms = new ArrayList<ExtAgreeTermsReqDTO>();
        ExtAgreeTermsReqDTO smtTermsdto = new ExtAgreeTermsReqDTO();
        smtTermsdto.setCust_id(user.getUsername());
        smtTermsdto.setSvc_id(svcId);
        smtTermsdto.setTerms_id(SMTTermsId);

        smtTerms.add(smtTermsdto);

        if (smtChkFlag.equals("true") && selectedSmtTerms.size() < 1) {
            // 셀러봇 캐시 마케팅 활용 동의를 하였지만 모든 항목을 해지 한 경우 삭제
            termschk = smpJoinService.delTerms(smtTerms); // 셀러 이용약관 테이블 삭제
        } else if (smtChkFlag.equals("false") && selectedSmtTerms.size() > 0) {
            // 셀러봇 캐시 마케팅 활용 미동의를 하였지만 항목을 선택 한 경우 등록
            termschk = smpJoinService.addTerms(smtTerms); // 셀러 이용약관 테이블 추가
        }
        termschk = smpTermsService.saveSmtAgreeItem(d); // 셀러봇캐시 마케팅 동의 항목 관리

        if (!termschk) {
            return ResponseEntity.badRequest().body("TermsErrors");
        }

        return ResponseEntity.ok("");
    }
}