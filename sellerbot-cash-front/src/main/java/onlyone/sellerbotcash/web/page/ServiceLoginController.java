package onlyone.sellerbotcash.web.page;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.security.ServiceSiteAuthenticationToken;
import onlyone.sellerbotcash.service.SmpAuthService;

@Slf4j
@Controller
public class ServiceLoginController {
	@Autowired private SmpAuthService smpService;
    @Autowired protected AuthenticationManager authenticationManager;
    
    @RequestMapping(value = "/service/login", method = RequestMethod.GET)
	public String loginServiceSite(@AuthenticationPrincipal User user, HttpServletRequest request, HttpServletResponse response) {
		if(user == null) {
			log.debug("user not found");
		}
		
		return "service/svclogin";
	}
    
	@PostMapping(value = "/service/signup")
	public String signupServiceSite(@RequestParam String username,@RequestParam String password, HttpServletRequest request, HttpServletResponse response) {
		
		log.debug("start login");
	
		boolean success = smpService.authenticationsCust(username, password, request.getRemoteAddr());
		if( success ) {
			authenticateUserAndSetSessionSite(username, "", request);
			return "redirect:/";
		}else {
			log.info("login Fail " + username + " " + password);
			return "redirect:/service/svclogin";
		}
		
	}
	
    private void authenticateUserAndSetSessionSite( String username,String password, HttpServletRequest request) {
    	User user = new User(username, password, new ArrayList<>());
    	ServiceSiteAuthenticationToken token = new ServiceSiteAuthenticationToken(user, "SVCLOGIN", SellerbotConstant.GRANTEDAUTH ) ;

        // generate session if one doesn't exist
        request.getSession();

        token.setDetails(new WebAuthenticationDetails(request));
        Authentication authenticatedUser = authenticationManager.authenticate(token);

        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
    }
	

}
