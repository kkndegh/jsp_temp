package onlyone.sellerbotcash.web.page;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inicis.std.util.HttpUtil;
import com.inicis.std.util.ParseUtil;
import com.inicis.std.util.SignatureUtil;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.service.PaymentService;
import onlyone.sellerbotcash.service.SbfUtils;
import onlyone.sellerbotcash.service.SmpCommService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.sellerbotcash.web.vm.JoinCustInfo;
import onlyone.smp.persistent.domain.enums.ECustPosi;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtAgreeTermsReqDTO;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtCustDetailDTO;
import onlyone.smp.service.dto.ExtCustPackageDTO;
import onlyone.smp.service.dto.ExtGoodsDTO;
import onlyone.smp.service.dto.ExtKkontTgtDTO;
import onlyone.smp.service.dto.ExtNotiForDashBoardDTO;
import onlyone.smp.service.dto.ExtPayInicisReqDTO;
import onlyone.smp.service.dto.ExtPayInicisRespDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO;
import onlyone.smp.service.dto.ExtSvcTermsDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO.ExtSmtAgreeTermsInfoDTO;
import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.service.GoodsService;
import onlyone.sellerbotcash.service.SmpNoticeService;
import onlyone.sellerbotcash.service.dto.ExtraData;
import onlyone.sellerbotcash.service.dto.ExtraData.Goods;
import onlyone.sellerbotcash.web.error.RestApiException;
import onlyone.sellerbotcash.web.util.CustPayInfo;

@Slf4j
@Controller
public class JoinSodeOneController {

    // 소드원 회원가입 계정
    private final static String KEY_SODEONE_MEMBER = "sodeone_member";
    // 셀러봇 캐시 회원 가입 계정
    private final static String KEY_SELERBOTCASH_MEMBER = "selerbotcash_member";
    // 셀러봇 캐시 회원 가입 계정
    private final static String KEY_INTEGRATION_MEMBER = "integration_member";

    @Value("${smp.svcId:sellerbotcash}")
    private String svcId;
    @Value("${internal.redirect.base:}")
    private String redirectBase;

    @Autowired
    private SmpJoinService smpJoinService;

    @Autowired
    private SmpCommService smpCommService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private SmpCustService smpCustService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private CustPayInfo custPayInfo;

    @Autowired
    private SmpNoticeService smpNoticeService;

    /**
     * 소드원 통합 가입 - 약관동의
     * 
     * @param modelMap
     * @param session
     * @param request
     * @return
     */
    @GetMapping("/pub/member_only1/step1")
    public String getOnly1JoinRenewalStep1(ModelMap modelMap, HttpSession session, HttpServletRequest request) {

        // 로그인한 브라우저로 신규 회원 가입을 페이지에 접근하는 경우 세션 초기화
        log.debug("session : {}", session);
        session.invalidate();

        // 세션 초기화 하기떄문에 공지사항 리스트 다시 세션에 담아야 함
        List<ExtNotiForDashBoardDTO> notiListData = smpNoticeService.getNotiListForDashBoard();
        request.getSession().setAttribute("notiListData", notiListData);

        String token = request.getParameter("token");
        String sodeone_svc_id = request.getParameter("svc_id"); // 파라미터 서비스 아이디

        if (Objects.isNull(token)) {
            token = request.getParameter("TOKEN");
        }

        if (Objects.isNull(token) || Objects.isNull(sodeone_svc_id)) {
            log.debug("parameter not null value [token: {}, svc_id: {}]", token, sodeone_svc_id);
            return "errors/403";
        }

        if (!smpJoinService.isExistSvcId(sodeone_svc_id)) {
            log.debug("not allow [svc_id: {}]", sodeone_svc_id);
            return "errors/403";
        }

        boolean check = smpCustService.tokenCheck(token);
        // check = true;
        // sodeone_svc_id = "sodeone";
        // 허용 하는 svc id 추가
        if (check) {
            List<ExtSvcTermsDTO> sellerbotTerms = smpJoinService.getSellerBotTerms(svcId); // 프로퍼티 서비스아이디
            List<ExtSvcTermsDTO> sodeOneTerms = smpJoinService.getSellerBotTerms(sodeone_svc_id);

            // 20221206 셀러봇캐시 마켓팅 활용 세부 항목 가져오기 추가
            List<ExtCommCdDTO> smtTermsList = smpCommService.getCodeList("SMT");
            smtTermsList = smtTermsList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord).reversed())
                    .collect(Collectors.toList());

            modelMap.addAttribute("svc_cust_id", request.getParameter("svc_cust_id"));
            modelMap.addAttribute("svc_biz_no", request.getParameter("svc_biz_no"));
            modelMap.addAttribute("sellerbotTerms", sellerbotTerms);
            modelMap.addAttribute("sodeOneTerms", sodeOneTerms);
            modelMap.addAttribute("svc_id", sodeone_svc_id);
            modelMap.addAttribute("smtTermsList", smtTermsList);

            return "sub/memberOnly1Renweal/only1_renewal_step1";
        } else {
            log.debug("not allow [token: {}]", token);
            return "errors/403";
        }
    }

    /**
     * 서비스 가입여부 조회 (step2)
     * 
     * @param modelMap
     * @param sellerbotTerm
     * @param smtTerm
     * @param sodeOneTerm
     * @param svc_id
     * @param req
     * @return
     */
    @PostMapping("/pub/member_only1/step2")
    public String member_only1_step2(ModelMap modelMap, String sellerbotTerm, String smtTerm, String sodeOneTerm,
            String svc_id, HttpServletRequest req) {

        String svc_cust_id = req.getParameter("svc_cust_id");
        String svc_biz_no = req.getParameter("svc_biz_no");
        String step = req.getParameter("step"); // 화면정보

        modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
        modelMap.addAttribute("smtTerm", smtTerm);
        modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
        modelMap.addAttribute("svc_cust_id", svc_cust_id);
        modelMap.addAttribute("svc_biz_no", svc_biz_no);
        modelMap.addAttribute("svc_id", svc_id);

        // 소드원 가입여부 조회 건너뛰기
        if (Objects.nonNull(step) && "pre_join_step2".equals(step)) {
            return "sub/memberOnly1Renweal/only1_renewal_step3";
        }

        if (StringUtils.isEmpty(svc_cust_id) && StringUtils.isEmpty(svc_biz_no)) {
            // 통합외원 가입
            return "sub/memberOnly1Renweal/only1_renewal_step2";
        } else {
            //
            return "sub/memberOnly1Renweal/only1_renewal_step3";
        }
    }

    /**
     * 소드원 회원가입 조회
     * 
     * @param password
     * @param svc_cust_id
     * @param session
     * @return
     */
    @PostMapping("/pub/member_only1/join_check")
    public @ResponseBody HashMap<String, Boolean> service_join_check(String password, String svc_cust_id,
            HttpSession session) {

        HashMap<String, Boolean> result = new HashMap<String, Boolean>();

        String token = smpCustService.getToken(svc_cust_id);
        // 1. 소드원 회원정보 조회
        List<HashMap<String, Object>> list = smpJoinService.custAndBiz(password, svc_cust_id, token);
        result.put(KEY_SODEONE_MEMBER, Objects.isNull(list) ? false : true);

        try {

            // 2. 셀러봇 캐시 회원 정보
            ExtCustDetailDTO extCustDetailDTO = smpCustService.getCustDetail(svc_cust_id);
            if (Objects.nonNull(extCustDetailDTO)) {
                result.put(KEY_SELERBOTCASH_MEMBER, true);
            } else {
                result.put(KEY_SELERBOTCASH_MEMBER, false);
            }
        } catch (RestApiException e) {
            result.put(KEY_SELERBOTCASH_MEMBER, false);
        }

        return result;
    }

    /**
     * 
     * 
     * @param modelMap
     * @param sellerbotTerm
     * @param smtTerm
     * @param sodeOneTerm
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/join_cust_confirm")
    public String member_only1_step2_cust_confirm(ModelMap modelMap, String sellerbotTerm, String smtTerm,
            String sodeOneTerm, HttpSession session, HttpServletRequest request) {
        // log.debug("** {}", session.getClass().getName());
        String password = request.getParameter("password");
        String req_cust_id = request.getParameter("req_cust_id");
        String token = smpCustService.getToken(req_cust_id);

        List<HashMap<String, Object>> list = smpJoinService.custAndBiz(password, req_cust_id, token);
        if (null == list) {
            list = new ArrayList<HashMap<String, Object>>();
        }
        if (list.size() >= 2) {
            modelMap.addAttribute("list", list);
            return "sub/member_only1/only1_renewal_step7";
        } else if (list.size() == 1) {
            modelMap.addAttribute("svc_biz_no", list.get(0).get("svc_biz_no"));
            modelMap.addAttribute("svc_cust_id", list.get(0).get("svc_cust_id"));
            modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
            modelMap.addAttribute("smtTerm", smtTerm);
            modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
            return "sub/memberOnly1Renweal/only1_renewal_step3";
        } else {
            modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
            modelMap.addAttribute("smtTerm", smtTerm);
            modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
            return "sub/memberOnly1Renweal/only1_renewal_step3";
        }
    }

    /**
     * 회원 정보 입력 화면
     * 
     * @param modelMap
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/step3")
    public String member_only1_step3(ModelMap modelMap, HttpSession session, HttpServletRequest request) {
        // log.debug("** {}", session.getClass().getName());

        String svc_biz_no = request.getParameter("svc_biz_no");
        String svc_cust_id = request.getParameter("svc_cust_id");
        String cust_id = request.getParameter("cust_id");
        String sellerbotTerm = request.getParameter("sellerbotTerm");
        String smtTerm = request.getParameter("smtTerm");
        String sodeOneTerm = request.getParameter("sodeOneTerm");

        if (StringUtils.isNotEmpty(svc_cust_id)) {
            // 1. 소드원 계정이 있는 경우
            String auth_token = smpJoinService.confirmCust(cust_id);
            modelMap.addAttribute("cust_info", smpJoinService.getSodeCustInfo(svc_cust_id, auth_token));
        } else {
            modelMap.addAttribute("cust_info", new JoinCustInfo());
        }

        // 유입 경로 조회
        List<ExtCommCdDTO> codeList = smpCommService.getCodeList("A020");
        List<ExtCommCdDTO> sortedCodeList = codeList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord))
                .collect(Collectors.toList());
        modelMap.addAttribute("codeList", sortedCodeList);

        modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
        modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
        modelMap.addAttribute("smtTerm", smtTerm);
        modelMap.addAttribute("svc_biz_no", svc_biz_no);
        modelMap.addAttribute("svc_cust_id", svc_cust_id);
        modelMap.addAttribute("cust_id", cust_id);

        return "sub/memberOnly1Renweal/only1_renewal_step4";
    }

    /**
     * 
     * 
     * @param modelMap
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/cash_confirm")
    public String pre_join_step3(ModelMap modelMap, String sellerbotTerm, String sodeOneTerm, HttpSession session,
            HttpServletRequest request) {
        // log.debug("** {}", session.getClass().getName());
        String svc_biz_no = request.getParameter("svc_biz_no");
        String cust_id = request.getParameter("cust_id");

        modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
        modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
        modelMap.addAttribute("svc_biz_no", svc_biz_no);
        modelMap.addAttribute("cust_id", cust_id);

        return "sub/memberOnly1Renweal/only1_renewal_step6";
    }

    /**
     * 회원 가입 성공 처리
     * 
     * @param modelMap
     * @param sodeOneTerm
     * @param sellerbotTerm
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/step4")
    public String join_step4(ModelMap modelMap, String sodeOneTerm, String sellerbotTerm, HttpSession session,
            HttpServletRequest request) {
        // log.debug("** {}", session.getClass().getName());
        log.debug("join_step4 start");
        String svc_biz_no = request.getParameter("svc_biz_no"); // 서비스
        String cust_id = request.getParameter("cust_id");
        String svc_cust_id = request.getParameter("svc_cust_id");
        // 고객 정보 상세 최득
        log.debug(cust_id);

        ExtCustDetailDTO extCustDTO = smpCustService.getCustDetail(cust_id);

        modelMap.addAttribute("svc_biz_no", svc_biz_no);
        modelMap.addAttribute("cust_id", cust_id);
        modelMap.addAttribute("svc_cust_id", svc_cust_id);
        modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
        modelMap.addAttribute("sellerbotTerm", sellerbotTerm);

        modelMap.addAttribute("extCustDTO", extCustDTO);

        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();

        if (Objects.nonNull(svc_cust_id) && Objects.nonNull(svc_biz_no)) {
            String token = smpCustService.getToken(svc_cust_id);
            list = smpJoinService.custAndBiz(svc_cust_id, token);
            modelMap.addAttribute("list", list);
        }

        // 소드원 사업자 번호와 셀러봇 사업자 번호가 같은 경우 || 셀러봇 계정은 있고, 소드원은 없는경우
        if (svc_biz_no.equals(extCustDTO.getBiz_no())
                || (StringUtils.isEmpty(svc_biz_no) && !StringUtils.isEmpty(extCustDTO.getBiz_no()))) {

            if (list.size() == 0) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("cust_id", cust_id);
                map.put("biz_no", extCustDTO.getBiz_no());
                list.add(map);
                modelMap.addAttribute("list", list);
            }

            return "sub/memberOnly1Renweal/only1_renewal_step7"; // 셀러봇 계정 있는 경우 결제수단 등록X
        } else {
            // 소드원 정보가 있는 경우
            // 통합 불가
            return "sub/memberOnly1Renweal/only1_renewal_end";
        }
    }

    /**
     * 아이디 통합하기
     * 
     * @param modelMap
     * @param sodeOneTerm
     * @param sellerbotTerm
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/integrate")
    public String join_integrate(ModelMap modelMap, String sodeOneTerm[], String sellerbotTerm[], HttpSession session,
            HttpServletRequest request) {
        String svc_biz_no = request.getParameter("svc_biz_no"); // 서비스
        String cust_id = request.getParameter("cust_id");
        String svc_cust_id = request.getParameter("svc_cust_id");
        String token = smpCustService.getToken(svc_cust_id);
        // 고객 정보 상세 최득
        log.debug(cust_id);

        ExtCustDetailDTO extCustDTO = smpCustService.getCustDetail(cust_id);

        // 소드원 사업자 번호와 셀러봇 사업자 번호가 같은 경우
        if (svc_biz_no.equals(extCustDTO.getBiz_no())) {

            // 통합 가입 회원
            List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();

            // 셀러봇 약관 저장
            for (int i = 0; i < sellerbotTerm.length; i++) {
                ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                t.setCust_id(cust_id);
                t.setSvc_id(svcId);
                t.setTerms_id(Long.parseLong(sellerbotTerm[i]));
                terms.add(t);
            }

            // 소드원 약관 저장
            List<ExtAgreeTermsReqDTO> sodeTerms = new ArrayList<ExtAgreeTermsReqDTO>();

            for (int j = 0; j < sodeOneTerm.length; j++) {
                ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                t.setCust_id(cust_id);
                t.setSvc_id("sodeone");
                t.setTerms_id(Long.parseLong(sodeOneTerm[j]));
                sodeTerms.add(t);
            }

            // 약관 등록
            smpJoinService.addTerms(terms);
            smpJoinService.addTerms(sodeTerms);

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("svc_id", svcId);
            map.put("cust_id", cust_id);
            map.put("cust_seq_no", extCustDTO.getCust_seq_no());
            map.put("auth_token", token);

            // 고객 일번변호 등록
            smpJoinService.svcCustInsert(map);

        } else {

            // 셀러봇 계정은 있고, 소드원은 없는경우
            if (StringUtils.isEmpty(svc_biz_no) && !StringUtils.isEmpty(extCustDTO.getBiz_no())) {

                // 통합 가입 회원
                List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();

                // 셀러봇 약관 저장
                for (int i = 0; i < sellerbotTerm.length; i++) {
                    ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                    t.setCust_id(cust_id);
                    t.setSvc_id(svcId);
                    t.setTerms_id(Long.parseLong(sellerbotTerm[i]));
                    terms.add(t);
                }

                // 소드원 약관 저장
                List<ExtAgreeTermsReqDTO> sodeTerms = new ArrayList<ExtAgreeTermsReqDTO>();

                for (int j = 0; j < sodeOneTerm.length; j++) {
                    ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                    t.setCust_id(cust_id);
                    t.setSvc_id("sodeone");
                    t.setTerms_id(Long.parseLong(sodeOneTerm[j]));
                    sodeTerms.add(t);
                }

                // 약관 등록
                // 셀러봇 약관은 추가 등록하지 않는다.
                smpJoinService.addTerms(terms);
                smpJoinService.addTerms(sodeTerms);

                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("svc_id", svcId);
                map.put("cust_id", cust_id);
                map.put("cust_seq_no", extCustDTO.getCust_seq_no());
                map.put("auth_token", token);

                // 고객 일번변호 등록
                smpJoinService.svcCustInsert(map);

            } else {
                log.debug("svc_biz_no : {0}", svc_biz_no);
                log.debug("extCustDTO : {0}", extCustDTO);
                return "errors/500";

            }
        }
        return "sub/memberOnly1Renweal/only1_renewal_complete"; // 완료 페이지로
    }

    /**
     * 인증 번호 전송 및 계정정보 확인
     * 
     * @param cust_id
     * @param session
     * @return
     */
    @PostMapping("/pub/member_only1/id_check")
    public ResponseEntity<String> join_id_check(String svc_id, String cust_id, HttpSession session) {
        // log.debug("** {}", session.getClass().getName());

        // 3.2.1 서비스 유형 여부
        boolean isExistSvcId = smpJoinService.isExistSvcMember(svc_id, cust_id);

        //
        if (isExistSvcId) {
            return ResponseEntity.status(400).body(KEY_INTEGRATION_MEMBER);
        }

        // 셀로봇 캐시 회원가입 여부 체크
        if (smpJoinService.isExistSvcMember(svcId, cust_id)) {
            return ResponseEntity.status(409).body(KEY_SELERBOTCASH_MEMBER);
        }

        String auth_token = smpJoinService.confirmCust(cust_id);
        return ResponseEntity.ok(auth_token);
    }

    /**
     * 
     * 
     * @param cust_id
     * @param password
     * @param session
     * @return
     */
    @PostMapping("/pub/member_only1/seller_check")
    public @ResponseBody HashMap<String, Object> seller_join_check(String cust_id, String password,
            HttpSession session) {
        // log.debug("** {}", session.getClass().getName());
        HashMap<String, Object> map = smpCustService.sellerConfirm(cust_id, password);
        return map;
    }

    /**
     * 2.1.6 고객 인증 번호 확인 & 2.1.7 고객 비밀 번호 확인
     * 
     * @param cust_id
     * @param authNo
     * @param auth_token
     * @param session
     * @return
     */
    @PostMapping("/pub/member_only1/confirm_cust")
    public ResponseEntity<String> confirmCust(String cust_id, String authNo, String auth_token, HttpSession session) {

        if (StringUtils.isEmpty(auth_token)) {
            return ResponseEntity.status(4490).body("not called");
        }

        // 2.1.6 고객 인증 번호 확인
        boolean result = smpJoinService.confirmCustAuthtoken(cust_id, authNo, auth_token);

        if (result) {
            return ResponseEntity.ok("");
        } else {
            return ResponseEntity.badRequest().body("not same");
        }
    }

    /**
     * 사업자번호 확인
     * 
     * @param bizNo
     * @param session
     * @return
     */
    @PostMapping("/pub/member_only1/check_bizNo")
    public ResponseEntity<String> checkBizNo(String bizNo, HttpSession session) {
        smpJoinService.checkBizNo(bizNo);
        return ResponseEntity.ok("");
    }

    /**
     * 회원가입 가입 처리
     * 
     * @param custInfo
     * @param sodeOneTerm
     * @param sellerbotTerm
     * @param smtTerm
     * @param modelMap
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/step_end")
    public String join_step_end(JoinCustInfo custInfo, String[] sodeOneTerm, String[] sellerbotTerm, String[] smtTerm,
            ModelMap modelMap, HttpSession session, HttpServletRequest request) {

        ExtCustPackageDTO custDto = new ExtCustPackageDTO();

        custDto.setPasswd(custInfo.getPasswd());
        custDto.setPasswd_same(custInfo.getPasswd_same());
        custDto.setBiz_nm(custInfo.getBiz_nm());
        custDto.setCeo_nm(custInfo.getCeo_nm());
        custDto.setCeo_no(custInfo.getCeo_no_svc() + custInfo.getCeo_no());
        custDto.setCust_id(custInfo.getCust_id());
        custDto.setBiz_no(custInfo.getBiz_no());

        if (StringUtils.isNotEmpty(custInfo.getChrg_nm())) {
            custDto.setChrg_nm(custInfo.getChrg_nm());
        }
        if (StringUtils.isNotEmpty(custInfo.getChrg_no())) {
            custDto.setChrg_no(custInfo.getCharg_no_svc() + custInfo.getChrg_no());
        }
        custDto.setInf_path_cd(custInfo.getInf_path_cd());

        List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();
        List<ExtAgreeTermsReqDTO> sodeTerms = new ArrayList<ExtAgreeTermsReqDTO>();

        for (int i = 0; i < sellerbotTerm.length; i++) {
            ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
            t.setCust_id(custDto.getCust_id());
            t.setSvc_id(svcId);
            t.setTerms_id(Long.parseLong(sellerbotTerm[i]));
            terms.add(t);
        }

        custDto.setTerms(terms);

        // 20221118 셀러봇 캐시 마켓팅 동의 선택된 항목을 list로 변환
        // 셀러봇캐시 마케팅 활용 동희 항목
        List<String> selectedSmtTerms = new ArrayList<String>();
        if (!org.springframework.util.StringUtils.isEmpty(smtTerm)) {
            for (String smtTermItem : smtTerm) {
                selectedSmtTerms.add(smtTermItem);
            }
        }

        ExtSmtAgreeTermsDTO d = new ExtSmtAgreeTermsDTO();
        d.setCust_id(custInfo.getCust_id());
        d.setSvc_id(svcId);
        // d.setCust_seq_no는 SMP에서 신규 회원정보 생성 후 처리함.

        List<ExtSmtAgreeTermsInfoDTO> smtTerms = selectedSmtTerms.stream().map(s -> {
            ExtSmtAgreeTermsInfoDTO e = new ExtSmtAgreeTermsInfoDTO();
            e.setSmt_terms_cd(s);
            e.setSmt_terms_aggr_yn(EYesOrNo.Y);

            return e;
        }).collect(Collectors.toList());
        d.setSmt_aggr_item_list(smtTerms);
        // 20221118 셀러봇 캐시 마켓팅 설정
        custDto.setSmtTerm(d);

        // 세러봇캐시 회원정보 / 약관동의 저장 /
        if (smpJoinService.addCustPkg(custDto)) {

            // 아이디 세션에 저장
            request.getSession().setAttribute("sodeCust", smpCustService.getCust(custDto.getCust_id()));

            new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(30000);
                        /**
                         * 1. 알림톡 미신청 일 경우 발생 => 회원가입 완료 알림톡 발송만 2. 알림톡 신청 일 경우 발생 => 알림톡 신청 발송만
                         */

                        // 알림톡 대상자 등록
                        ExtKkontTgtDTO extKkontTgtDTO = new ExtKkontTgtDTO();
                        boolean rtn = false;

                        // 대표자 정보가 있을 경우
                        if (StringUtils.isNotEmpty(custInfo.getCeo_nm())
                                && StringUtils.isNotEmpty(custInfo.getCeo_no())) {
                            // 알림톡 신청
                            if (StringUtils.isNotEmpty(custInfo.getTrs_hop_hour1())) {
                                extKkontTgtDTO.setCust_posi_cd(ECustPosi.CEO);
                                extKkontTgtDTO.setTgt_cust_nm(custInfo.getCeo_nm());
                                extKkontTgtDTO.setTgt_cust_no(custInfo.getCeo_no());
                                extKkontTgtDTO.setTrs_hop_hour(Integer.valueOf(custInfo.getTrs_hop_hour1()));
                                rtn = smpCustService.alarm_insert(custDto.getCust_id(), extKkontTgtDTO);

                                // 대표자 정보 저장이 정상이면서 담당자 정보가 있을 경우
                                if (rtn && StringUtils.isNotEmpty(custInfo.getChrg_nm())
                                        && StringUtils.isNotEmpty(custInfo.getChrg_no())
                                        && StringUtils.isNotEmpty(custInfo.getTrs_hop_hour2())) {
                                    extKkontTgtDTO.setCust_posi_cd(ECustPosi.CRG);
                                    extKkontTgtDTO.setTgt_cust_nm(custInfo.getChrg_nm());
                                    extKkontTgtDTO.setTgt_cust_no(custInfo.getChrg_no());
                                    extKkontTgtDTO.setTrs_hop_hour(Integer.valueOf(custInfo.getTrs_hop_hour2()));
                                    rtn = smpCustService.alarm_insert(custDto.getCust_id(), extKkontTgtDTO);
                                }
                            } else {
                                // 알림톡 미신청
                                // 회원가입 알림톡 발송
                                smpCustService.alarm_send_join(custDto.getCust_id());
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            // 셀러봇 캐시 회원가입 성공시 소드원도 가입처리 , 약관동의 사항 저장
            for (int j = 0; j < sodeOneTerm.length; j++) {
                ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                t.setCust_id(custDto.getCust_id());
                t.setSvc_id("sodeone");
                t.setTerms_id(Long.parseLong(sodeOneTerm[j]));
                sodeTerms.add(t);
            }
            smpJoinService.addTerms(sodeTerms);
        }

        // 고객 상세 정보 취득
        log.debug(custDto.getCust_id());
        ExtCustDetailDTO extCustDTO = smpCustService.getCustDetail(custDto.getCust_id());
        // 토큰 정보 생성
        String token = smpCustService.getToken(custDto.getCust_id());

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("svc_id", svcId);
        map.put("cust_id", custDto.getCust_id());
        map.put("cust_seq_no", extCustDTO.getCust_seq_no());
        map.put("auth_token", token);

        // 소드원 고객정보 저장&업데이트
        smpJoinService.svcCustInsert(map);

        modelMap.addAttribute("cust_id", custDto.getCust_id());

        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("cust_id", custDto.getCust_id());

        // return data;
        // return ResponseEntity.ok(custDto.getCust_id());
        return "redirect:" + redirectBase + "/pub/member_only1/step5";
    }

    /**
     * 결제수단 등록
     * 
     * @param request
     * @param modelMap
     * @param session
     * @param device
     * @param cust_id
     * @return
     */
    @GetMapping("/pub/member_only1/step5")
    public String join_renewal_step6(HttpServletRequest request, ModelMap modelMap, HttpSession session, Device device,
            String cust_id) {
        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        ExtCustDTO cust = new ExtCustDTO();
        // ExtCustDTO cust_session = (ExtCustDTO) session.getAttribute("sodeCust");
        cust = (ExtCustDTO) request.getSession().getAttribute("sodeCust");

        if (map != null) {
            modelMap.put("successYN", map.get("successYN"));
            modelMap.put("errMsg", map.get("errMsg"));
        }

        if (cust == null) {
            return "redirect:" + redirectBase + "/login";
        }

        // Cafe24 고객 여부
        // EYesOrNo cafe24_usr_yn = EYesOrNo.N;
        // if (cust.getInf_site_cd() != null &&
        // cust.getInf_site_cd().equals(EInfPath.CF24)) {
        // cafe24_usr_yn = EYesOrNo.Y;
        // }

        modelMap.addAttribute("siteDomain", redirectBase);
        modelMap.addAttribute("mid", SellerbotConstant.INICIS_MID);
        modelMap.addAttribute("mKey", SellerbotConstant.INICIS_MKEY);

        modelMap.addAttribute("buyername", cust.getCeo_nm());
        modelMap.addAttribute("buyertel", cust.getCeo_no());
        modelMap.addAttribute("buyeremail", cust.getCust_id());

        // List<ExtGoodsDTO> goodsList = goodsService.getGoodsList();
        List<ExtGoodsDTO> goodsList = goodsService.getGoodsList("", "");   //디폴트 상품 조회시는 파라미터에 빈값("제휴사구분코드", "cust_seq_no") 넘김
        ExtGoodsDTO goodsInfo = new ExtGoodsDTO();
        goodsList = goodsList.stream().filter(e -> {
            if ("PAFP".equals(e.getGOODS_TYP_CD())) {
                return true;
            } else {
                return false;
            }
        }).collect(Collectors.toList());

        if (goodsList.size() > 0) {
            goodsInfo = goodsList.get(0);
        }

        // 상품정보
        modelMap.addAttribute("goodsInfo", goodsInfo);

        if (device.isNormal()) {
            modelMap.addAttribute("DeviceType", "PC");
        } else {
            modelMap.addAttribute("DeviceType", "Other");
        }

        return "sub/memberOnly1Renweal/only1_renewal_step5";
    }

    /**
     * 결제 결과 수신 - PC
     * 
     * @param request
     * @param redirectAttr
     * @param session
     * @return
     */
    @PostMapping(value = "/pub/member_only1/payment/payresponse")
    public String paymentResponse(HttpServletRequest request, RedirectAttributes redirectAttr, HttpSession session) {
        boolean success = false;
        String errMsg = "";
        String mode = null;
        String cust_id = "";
        ExtCustDTO cust = new ExtCustDTO();

        try {

            request.setCharacterEncoding("UTF-8");
            Map<String, String> paramMap = new Hashtable<String, String>();
            Enumeration<String> elems = request.getParameterNames();
            while (elems.hasMoreElements()) {
                String key = elems.nextElement();
                paramMap.put(key, request.getParameter(key));
            }
            log.info(
                    "(+)=========================================================\n" + "	paramMap : {} - {}\n"
                            + "(-)=========================================================\n",
                    cust.getCust_id(), paramMap);

            if ("0000".equals(paramMap.get("resultCode"))) { // 인증 성공
                String mid = paramMap.get("mid");
                String timestamp = SignatureUtil.getTimestamp();
                String charset = SellerbotConstant.INICIS_CHARSET;
                String format = SellerbotConstant.INICIS_FORMAT;
                String authToken = paramMap.get("authToken");
                String authUrl = paramMap.get("authUrl");
                String netCancelUrl = paramMap.get("netCancelUrl");

                String merchantData = paramMap.get("merchantData").toString(); // 가맹점 관리데이터 수신
                merchantData = StringEscapeUtils.unescapeHtml(merchantData).replace("&quot", "\"");
                ExtraData extras = new ObjectMapper().readValue(merchantData, ExtraData.class);

                mode = extras.getMode();
                cust_id = extras.getBuyerEmail();

                List<Goods> goodsList = extras.getGoodsList();
                request.getSession().setAttribute("sodeCust", smpCustService.getCust(cust_id));
                cust = smpCustService.getCust(cust_id);

                if (cust == null)
                    throw new Exception("Customer Not Found.");

                Map<String, String> signParam = new HashMap<String, String>();
                signParam.put("authToken", authToken);
                signParam.put("timestamp", timestamp);

                String signature = SignatureUtil.makeSignature(signParam);
                Map<String, String> authMap = new Hashtable<String, String>();
                authMap.put("mid", mid); // 필수
                authMap.put("authToken", authToken); // 필수
                authMap.put("signature", signature); // 필수
                authMap.put("timestamp", timestamp); // 필수
                authMap.put("charset", charset); // default=UTF-8
                authMap.put("format", format); // default=XML

                HttpUtil httpUtil = new HttpUtil();
                try { // 승인 요청
                    String authResultString = httpUtil.processHTTP(authMap, authUrl);
                    String replaced = authResultString.replace(",", "&").replace(":", "=").replace("\"", "")
                            .replace(" ", "").replace("\n", "").replace("}", "").replace("{", "");
                    Map<String, String> resultMap = ParseUtil.parseStringToMap(replaced);

                    log.info(
                            "(+)=========================================================\n"
                                    + "	resultMap : {} - {}\n"
                                    + "(-)=========================================================\n",
                            cust.getCust_id(), resultMap);

                    Map<String, String> secureMap = new HashMap<String, String>();
                    secureMap.put("mid", mid);
                    secureMap.put("tstamp", timestamp);
                    secureMap.put("MOID", resultMap.get("MOID"));
                    secureMap.put("TotPrice", resultMap.get("TotPrice"));

                    String secureSignature = SignatureUtil.makeSignatureAuth(secureMap);
                    String resultCode = resultMap.get("resultCode");
                    String authSignature = resultMap.get("authSignature");

                    if ("0000".equals(resultCode)) { // 승인 성공
                        if (secureSignature.equals(authSignature)) {
                            String tid = resultMap.get("tid");
                            ExtPayInicisReqDTO c = new ExtPayInicisReqDTO();

                            c.setResultCode(resultMap.get("resultCode"));
                            c.setResultMsg(resultMap.get("resultMsg"));
                            c.setPayMethod(resultMap.get("payMethod"));
                            c.setGoodNm(resultMap.get("goodName"));
                            c.setBuyerName(resultMap.get("buyerName"));
                            c.setBuyerTel(resultMap.get("buyerTel"));
                            c.setBuyerEmail(resultMap.get("buyerEmail"));
                            c.setApplTime(resultMap.get("applTime"));
                            c.setCurrency(resultMap.get("currency"));
                            c.setPayDevice(resultMap.get("payDevice"));
                            c.setApplDate(resultMap.get("applDate"));
                            c.setMoid(resultMap.get("MOID"));
                            c.setTid(tid);
                            c.setCardBillKey(resultMap.get("CARD_BillKey"));
                            c.setCardBankCode(resultMap.get("CARD_BankCode"));
                            c.setCardQuota(resultMap.get("CARD_Quota"));
                            c.setCardCode(resultMap.get("CARD_Code"));
                            c.setCardNum(resultMap.get("CARD_Num"));
                            c.setCardInterest(resultMap.get("CARD_Interest"));
                            c.setCustSeqNo(cust.getCust_seq_no());
                            c.setEventNo(extras.getEventNo());

                            redirectAttr.addFlashAttribute("cardNum", resultMap.get("CARD_Num"));
                            redirectAttr.addFlashAttribute("goodName", resultMap.get("goodName"));

                            List<ExtPayInicisReqDTO.Goods> _l = new ArrayList<ExtPayInicisReqDTO.Goods>();
                            if ("register".equals(mode)) {
                                for (Goods g : goodsList) {
                                    ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                                    goods.setGoodsOptSeqNo(g.getO());
                                    goods.setUsePoi(0L);
                                    goods.setGoodNm(URLDecoder.decode(g.getN(), "UTF-8"));
                                    goods.setPrice(g.getP());
                                    // 에러 때문에 임시커밋
                                    // goods.setPeriod(g.getD());
                                    _l.add(goods);
                                }
                                c.setGoodsList(_l);
                                ExtPayInicisRespDTO p = paymentService.postPaymentByInicis(c);
                                log.info("paymentService.postPaymentByInicis = {}", p);
                                success = true;
                                redirectAttr.addFlashAttribute("freeYN", p.getFreeYN());
                                redirectAttr.addFlashAttribute("goodsInfo", p.getGoodsList().get(0));
                                redirectAttr.addFlashAttribute("products", p.getGoodsList());

                            } else if ("change".equals(mode)) {
                                for (Goods g : goodsList) {
                                    ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                                    goods.setGoodsReqSeqNo(g.getR());
                                    goods.setGoodsSeqNo(g.getS());
                                    _l.add(goods);
                                }
                                c.setGoodsList(_l);
                                paymentService.postChangeCard(c);
                                success = true;
                            }
                        } else {
                            throw new Exception("데이터 위변조 체크 실패");
                        }
                    } else {
                        errMsg = resultMap.get("resultMsg");
                    }

                } catch (Exception ex) {
                    errMsg = SbfUtils.getJsonValue(ex.getMessage(), "reason");
                    // log.info("===> {}", errMsg);
                    String result = httpUtil.processHTTP(authMap, netCancelUrl);
                    log.info(
                            "(+)=========================================================\n" + "	result : {} - {}\n"
                                    + "(-)=========================================================\n",
                            cust.getCust_id(), result);
                }
            } else {
                log.info("{} - Authentication Failed", cust.getCust_id());
                errMsg = paramMap.get("resultMsg");
            }
        } catch (Exception e) {
            errMsg = SbfUtils.getJsonValue(e.getMessage(), "reason");
        }

        redirectAttr.addFlashAttribute("successYN", success ? "Y" : "N");

        log.info("successYN ={}", success);
        if (!success) {
            redirectAttr.addFlashAttribute("errMsg", errMsg);
            return "redirect:" + redirectBase + "/pub/member_only1/step5";
        }
        // 결제 완료후 세션 삭제
        request.getSession().setAttribute("sodeCust", null);
        custPayInfo.updateSessionInfoAboutPay(session, cust.getCust_id());
        return "sub/memberOnly1Renweal/only1_renewal_complete";
    }

    /**
     * 결제 결과 수신 - MOBILE
     * 
     * @param request
     * @param redirectAttr
     * @param session
     * @return
     */
    @PostMapping(value = "/pub/member_only1/payment/mobilepayresponse")
    public String mobilePaymentResponse(HttpServletRequest request, RedirectAttributes redirectAttr,
            HttpSession session) {
        boolean success = false;
        String errMsg = "";
        String mode = "register";
        ExtCustDTO cust = new ExtCustDTO();
        String cust_id = "";
        try {

            request.setCharacterEncoding("UTF-8");

            Map<String, String> resultMap = new Hashtable<String, String>();
            Enumeration<String> elems = request.getParameterNames();

            while (elems.hasMoreElements()) {
                String key = elems.nextElement();
                resultMap.put(key, request.getParameter(key));
            }

            log.debug(
                    "(+)=========================================================\n" + "	resultMap : {} - {}\n"
                            + "(-)=========================================================\n",
                    cust.getCust_id(), resultMap);

            String p_noti = resultMap.get("p_noti").toString();
            p_noti = StringEscapeUtils.unescapeHtml(p_noti).replace("&quot", "\"");
            ExtraData extras = new ObjectMapper().readValue(p_noti, ExtraData.class);
            mode = extras.getMode();
            cust_id = extras.getBuyerEmail();
            List<Goods> goodsList = extras.getGoodsList();
            request.getSession().setAttribute("sodeCust", smpCustService.getCust(cust_id));
            cust = smpCustService.getCust(cust_id);

            if (cust == null)
                throw new Exception("Customer Not Found.");

            String resultCode = resultMap.get("resultcode");
            if ("00".equals(resultCode)) {
                String tid = resultMap.get("tid");
                ExtPayInicisReqDTO c = new ExtPayInicisReqDTO();

                c.setResultCode(resultCode + "00");
                c.setResultMsg(resultMap.get("resultmsg"));
                c.setMoid(resultMap.get("orderid"));
                c.setBuyerName(extras.getBuyerName());
                c.setBuyerTel(extras.getBuyerTel());
                c.setBuyerEmail(extras.getBuyerEmail());
                c.setApplTime(resultMap.get("pgauthtime"));
                c.setApplDate(resultMap.get("pgauthdate"));
                c.setTid(tid);
                c.setCardBillKey(resultMap.get("billkey"));
                c.setCardCode(resultMap.get("cardcd"));
                c.setCardNum(resultMap.get("cardno"));
                c.setCardQuota("0");
                c.setCardInterest("0");
                c.setPayDevice("MOBILE");
                c.setCustSeqNo(cust.getCust_seq_no());

                redirectAttr.addFlashAttribute("cardNum", resultMap.get("cardno"));

                List<ExtPayInicisReqDTO.Goods> _l = new ArrayList<ExtPayInicisReqDTO.Goods>();
                if ("register".equals(mode)) {
                    for (Goods g : goodsList) {
                        ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                        goods.setGoodsOptSeqNo(g.getO());
                        goods.setUsePoi(0L);
                        goods.setGoodNm(URLDecoder.decode(g.getN(), "UTF-8"));
                        goods.setPrice(g.getP());
                        // 에러 때문에 임시커밋
                        // goods.setPeriod(g.getD());
                        _l.add(goods);
                    }
                    c.setGoodsList(_l);
                    ExtPayInicisRespDTO p = paymentService.postPaymentByInicis(c);
                    success = true;
                    redirectAttr.addFlashAttribute("freeYN", p.getFreeYN());
                    redirectAttr.addFlashAttribute("products", p.getGoodsList());
                } else if ("change".equals(mode)) {
                    for (Goods g : goodsList) {
                        ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                        goods.setGoodsReqSeqNo(g.getR());
                        _l.add(goods);
                    }
                    c.setGoodsList(_l);
                    paymentService.postChangeCard(c);
                    success = true;
                }
            } else {
                errMsg = resultMap.get("resultMsg");
            }
        } catch (Exception e) {
            errMsg = e.getMessage();
        }

        redirectAttr.addFlashAttribute("successYN", success ? "Y" : "N");
        if (!success) {
            redirectAttr.addFlashAttribute("errMsg", errMsg);
            return "redirect:" + redirectBase + "/pub/member_only1/step5";
        }

        request.getSession().setAttribute("sodeCust", null);
        custPayInfo.updateSessionInfoAboutPay(session, cust.getCust_id());
        return "sub/memberOnly1Renweal/only1_renewal_complete";
    }
}