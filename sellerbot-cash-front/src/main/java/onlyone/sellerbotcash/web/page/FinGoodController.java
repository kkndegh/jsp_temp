package onlyone.sellerbotcash.web.page;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import onlyone.sellerbotcash.service.SmpFinGoodService;
import onlyone.sellerbotcash.web.util.DateUtil;

@Controller
public class FinGoodController {
	
	@Autowired
	private SmpFinGoodService smpFinGoodService;
	
	/**
	 * 미리정산가능금액
	 * 
	 * @param user
	 * @return
	 * @return
	 */
	@GetMapping("/sub/finGood/preSett")
	public ResponseEntity<String> finGoodPreSett(@AuthenticationPrincipal User user) {
		
		//미리정산가능금액
		long preSett = 0;

		try {
			preSett = smpFinGoodService.getBiggestPre(user.getUsername(), DateUtil.getDateString(LocalDate.now()));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("ERROR");
		}
		
		return ResponseEntity.ok(String.valueOf(preSett));
	}
}
