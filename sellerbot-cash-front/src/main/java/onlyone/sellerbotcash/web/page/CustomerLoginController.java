package onlyone.sellerbotcash.web.page;

import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.service.SmpAllianceService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.web.error.RestApiException;

@Controller
public class CustomerLoginController {

	@Autowired 
	private SmpAllianceService smpAllianceService;
	
	@Autowired
	private SmpCustService smpCustService;
	
	@ExceptionHandler(ServletRequestBindingException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void requestBindingException(ServletRequestBindingException e) {
	}
			
	/**
	 * 토큰 유효성 체크
	 * 
	 * @header access_token : 엑세스 토큰  
	 * @header svc_id : 서비스 ID 
	 * @header svc_cert_key : 서비스 인증 키
	 * @return 정상응답 또는 에러 코드
	 */
	@RequestMapping(value="/alliance/token/valid", method=RequestMethod.POST)
	public ResponseEntity<?> validToken(
			@RequestHeader(value = "access_token") String access_token,
			@RequestHeader(value = "svc_id") String svc_id,
			@RequestHeader(value = "svc_cert_key") String svc_cert_key) {
		
			smpAllianceService.tokenCheck(access_token, svc_id, svc_cert_key);

	    return ResponseEntity.ok().build();
	}
	
	/**
	 * 자동 로그인 리다이렉트
	 * 
	 * 1)쿼리 파라메터 필수 체크
	 * 2)엑세스 토큰으로 사용자 ID 요청
	 * 3)사용자 ID 정보 조회 요청 
	 * 4)존재하지 않는 사용자 경우 로그인 화면 이동 
	 * 5)그 외 예외 발생 시 로그인 화면 이동 
	 * 
	 * @param access_token : 엑세스 토큰
	 * @param svc_id : 서비스 ID
	 * @param svc_cert_key: 서비스 인증 키
	 * @return string : 화면 이동 주소
	 * 
	 */
	@RequestMapping(value="/alliance/login/auto", method=RequestMethod.GET)
	public String redirect(HttpServletRequest request,
			@RequestParam(name = "access_token", required = false) String access_token,
			@RequestParam(name = "svc_id", required = false) String svc_id,
			@RequestParam(name = "svc_cert_key", required = false) String svc_cert_key) {		
			
		HttpSession session = request.getSession();
		
		if(Stream.of(svc_id, svc_cert_key, access_token).anyMatch(StringUtils::isBlank)) {
			session.invalidate();
		}
		else {
			String cust_id = null;
			try {
				cust_id = smpAllianceService.tokenCheck(access_token, svc_id, svc_cert_key).getCust_id();
				smpCustService.getCust(cust_id);
			} 
			catch(RestApiException e) {
				 session.invalidate(); 	return "redirect:/";
			}
			User user = new User(cust_id, "",  SellerbotConstant.GRANTEDAUTH);
			
		    SecurityContext sc = SecurityContextHolder.getContext();
	        sc.setAuthentication(new UsernamePasswordAuthenticationToken(user, null, SellerbotConstant.GRANTEDAUTH));
	        session.setAttribute (HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc); 
		}
		return "redirect:/";

	}	
	

}
