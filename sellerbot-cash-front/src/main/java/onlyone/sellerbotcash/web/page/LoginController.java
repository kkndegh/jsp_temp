package onlyone.sellerbotcash.web.page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

// import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.security.ServiceSiteAuthenticationToken;
import onlyone.sellerbotcash.service.SmpAuthService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpJwtService;
import onlyone.sellerbotcash.web.error.RestApiException;
import onlyone.smp.service.dto.ExtCustPackageDTO;
import static onlyone.sellerbotcash.config.SellerbotConstant.GRANTEDAUTH;

// @Slf4j
@Controller
public class LoginController {

	@Value("${admin.login.key:admin!@3}")
	private String ADMIN_LOGIN_KEY;
	@Value("${internal.redirect.base:}")
	private String redirectBase;
	@Autowired
	private SmpAuthService smpService;
	@Autowired
	private SmpCustService smpCustService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap modelMap, @AuthenticationPrincipal User user, HttpServletRequest request,
			HttpSession session) {

		// 로그인 한 유저 정보가 있는 경우 index 페이지로 이동한다.
		if (Objects.isNull(user)) {
			// 신한 고객 API(00003,00004 코드)로 통해서들어온 고객
			if (request.getParameter("inf") != null) {
				session.setAttribute("inf", request.getParameter("inf"));
			}

			return "login/login";
		}

		return "redirect:" + redirectBase + "/";
	}

	@RequestMapping(value = "/pub/member/find_id", method = RequestMethod.GET)
	public String find_id(ModelMap modelMap) {
		return "login/find_id";
	}

	@RequestMapping(value = "/pub/member/find_id", method = RequestMethod.POST)
	@ResponseBody
	public String find_id(ExtCustPackageDTO custDto) {

		String result = smpService.authenticationsCust_findid(custDto.getBiz_nm(), custDto.getBiz_no(),
				custDto.getCeo_nm());
		if ("".equals(result)) {
			result = "ID 없음";
		}

		return result;
	}

	@RequestMapping(value = "/pub/member/result_id", method = RequestMethod.GET)
	public String result_id(ModelMap modelMap, HttpServletRequest request) {
		ExtCustPackageDTO custDto = new ExtCustPackageDTO();
		String cust_id = "";
		String ceo_nm = "";
		cust_id = (String) request.getParameter("cust_id");
		ceo_nm = (String) request.getParameter("ceo_nm");

		custDto.setCust_id(cust_id);
		custDto.setCeo_nm(ceo_nm);

		modelMap.addAttribute("custDto", custDto);

		return "login/result_id";
	}

	@RequestMapping(value = "/pub/member/find_pw", method = RequestMethod.GET)
	public String find_pw(ModelMap modelMap, String cust_id) {
		modelMap.addAttribute("cust_id", cust_id);
		return "login/find_pw";
	}

	@RequestMapping(value = "/pub/member/result_pw", method = RequestMethod.GET)
	public String result_pw(ModelMap modelMap, String custId) {
		modelMap.put("custId", custId);
		return "login/result_pw";
	}

	@PostMapping(value = "/pub/member/result_pw")
	public ResponseEntity<String> find_pw(String custId) {
		String result = smpService.authenticationsCustTempPasswd(custId);
		return ResponseEntity.ok(result);
	}

	@PostMapping(value = "/pub/member/result_pw_old")
	public ResponseEntity<String> find_pw_old(String custId) {
		boolean result = false;

		result = smpService.authenticationsCustTempPasswdOld(custId);

		if (result) {
			return ResponseEntity.ok("");
		} else {
			return ResponseEntity.badRequest().body("not same");
		}
	}

	/**
	 * 관리자 로그인
	 * 
	 * @param modelMap
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/master/login", method = RequestMethod.GET)
	public String masterLogin(ModelMap modelMap, @AuthenticationPrincipal User user, HttpSession session,
			@RequestParam(value = "token", defaultValue = "") String token) {
		// log.debug("** {}", session.getClass().getName());

		boolean isToken = smpCustService.tokenCheck(token);
		// 로그인 한 유저 정보가 있는 경우 index 페이지로 이동한다.
		if (Objects.isNull(user) && isToken) {
			modelMap.addAttribute("token", ADMIN_LOGIN_KEY);
			return "login/masterlogin";
		}

		return "redirect:" + redirectBase + "/";
	}

	@Autowired
	private SmpJwtService smpJwtService;

	@Autowired
	private AuthenticationManager authenticationManager;

	/**
	 * 관리자 로그인 by JWT
	 * 
	 * @param modelMap
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/jwt/master/login/{token}", method = RequestMethod.GET)
	public String jwtMasterLogin(ModelMap modelMap, @PathVariable("token") String token, HttpServletRequest request)
			throws IOException, ServletException {
		ResponseEntity<HashMap<String, String>> mapCustInfo = null;
		String adminSkip = request.getParameter("adminSkip");

		try {
			mapCustInfo = smpJwtService.tokenCheck(token);

		} catch (Exception e) {
			throw new RestApiException(HttpStatus.INTERNAL_SERVER_ERROR,
					"마스터로그인 JWT Token 오류입니다 [" + e.getMessage() + "]");
		}

		request.logout();
		User user = new User(mapCustInfo.getBody().get("custId"), "", new ArrayList<>());
		ServiceSiteAuthenticationToken tokenAuth = new ServiceSiteAuthenticationToken(user, "", GRANTEDAUTH);

		tokenAuth.setDetails(new WebAuthenticationDetails(request));
		SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(tokenAuth));

		// 2023.05.17 SMP 보안로그인으로 통해 접근 값 체크
		request.getSession().setAttribute("adminSkip", adminSkip);

		return "redirect:" + redirectBase + "/";
	}
}
