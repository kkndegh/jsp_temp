package onlyone.sellerbotcash.web.page;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpSalesService;
import onlyone.smp.service.dto.ExtComSalesMallReqDTO;
import onlyone.smp.service.dto.ExtCommMallDTO;
import onlyone.smp.service.dto.ExtSalesMallGraphDTO;
import onlyone.smp.service.dto.ExtSalesMonthMarketDTO;
import onlyone.smp.service.dto.ExtSalesSummaryLastDTO;
import onlyone.smp.service.dto.ExtSalesYearMarketDTO;
import onlyone.sellerbotcash.web.util.DateUtil;


/**
 * 정산 예정금 > 매출통계
 * @author bak
 *
 */
@Controller
public class SalesController {
	
	@Autowired
	private SmpSalesService smpSalesService; 
	
	@Autowired
	private SmpCustMallService smpCustMallService;
	
	/**
	 * 매출 통계
	 * @param modelMap
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@ExceptionHandler
	@GetMapping("/sub/sales/sales")
	public String formPaymentSales(ModelMap modelMap, @AuthenticationPrincipal User user) throws JsonProcessingException {
		String userName = user.getUsername();
		ExtComSalesMallReqDTO param = new ExtComSalesMallReqDTO();
		
		LocalDateTime time = LocalDateTime.now();
		LocalDateTime lastMonth = time.minusMonths(1); // 저번달 
		LocalDateTime lastYear = time.minusMonths(12); // 12개월 전
		
		param.setSales_find_stt_dt(DateUtil.getDateTimeMonthString(lastYear));
		param.setSales_find_end_dt(DateUtil.getDateTimeMonthString(lastMonth));
		
		// 정상 등록 된 판매몰(마켓) 목록
		List<ExtCommMallDTO> extCommMallDTOList = smpCustMallService.getCustMallList(userName);
		
		modelMap.addAttribute("mallList", extCommMallDTOList);		
		
		return "sub/sales/sales";
	}
	/**
	 * 매출 통계 중 최근 매출 
	 * @param modelMap
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	
	@GetMapping("/sub/sales/summaryLast")
	@ResponseBody
	public ExtSalesSummaryLastDTO getSummaryLast(@AuthenticationPrincipal User user) {
		String userName = user.getUsername();
		// 최근 매출
		ExtSalesSummaryLastDTO extSalesSummaryLastDTO = smpSalesService.getSalesSummaryLast(userName);
		
		return extSalesSummaryLastDTO;
	}
	
	/**
	 * 매출 통계
	 * @param modelMap
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@ExceptionHandler
	@GetMapping("/sub/sales/analysis")
	public String formPaymentSalesAnalysis(ModelMap modelMap, @AuthenticationPrincipal User user) throws JsonProcessingException {
		String userName = user.getUsername();
		ExtComSalesMallReqDTO param = new ExtComSalesMallReqDTO();
		
		LocalDateTime time = LocalDateTime.now();
		LocalDateTime lastMonth = time.minusMonths(1); // 저번달 
		LocalDateTime lastYear = time.minusMonths(12); // 12개월 전
		
		param.setSales_find_stt_dt(DateUtil.getDateTimeMonthString(lastYear));
		param.setSales_find_end_dt(DateUtil.getDateTimeMonthString(lastMonth));
		
		// 정상 등록 된 판매몰(마켓) 목록
		List<ExtCommMallDTO> extCommMallDTOList = smpCustMallService.getCustMallList(userName);

		// 최근 매출
		ExtSalesSummaryLastDTO extSalesSummaryLastDTO = smpSalesService.getSalesSummaryLast(userName);
		
		ObjectMapper mapper = new ObjectMapper();
		modelMap.addAttribute("mallList", extCommMallDTOList);		
		modelMap.addAttribute("summaryLast", mapper.writeValueAsString(extSalesSummaryLastDTO));

		return "sub/sales/graph";				
	}
	
	/**
	 * 매출 요약 연간
	 * @param user
	 * @param param
	 * @return
	 */
	@GetMapping("/sub/sales/year")
	@ResponseBody
	public ExtSalesYearMarketDTO getYearSalesMarketInfo(@AuthenticationPrincipal User user, @ModelAttribute ExtComSalesMallReqDTO param) {
		return smpSalesService.getYearSalesMarketInfo(user.getUsername(), param);
	}
	
	/**
	 * 매출 요약
	 * @param user
	 * @param param
	 * @return
	 */
	@GetMapping("/sub/sales/month")
	@ResponseBody
	public ExtSalesMonthMarketDTO getMonthSalesMarketInfo(@AuthenticationPrincipal User user, @ModelAttribute ExtComSalesMallReqDTO param) {
		return smpSalesService.getMonthSalesMarketInfo(user.getUsername(), param);
	}
	
	/**
	 * 매출 요약
	 * @param user
	 * @param param
	 * @return
	 */
	@GetMapping("/sub/sales/graph")
	@ResponseBody
	public ExtSalesMallGraphDTO getMallSalesGraphList(@AuthenticationPrincipal User user, @ModelAttribute ExtComSalesMallReqDTO param) {
		return smpSalesService.getMallSalesGraphList(user.getUsername(), param);
	}
}
