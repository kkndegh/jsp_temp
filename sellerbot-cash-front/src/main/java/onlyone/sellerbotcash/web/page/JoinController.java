package onlyone.sellerbotcash.web.page;

import static onlyone.sellerbotcash.config.SellerbotConstant.GRANTEDAUTH;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kcb.org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.security.ServiceSiteAuthenticationToken;
import onlyone.sellerbotcash.service.Cafe24Service;
import onlyone.sellerbotcash.service.MypageService;
import onlyone.sellerbotcash.service.PaymentService;
import onlyone.sellerbotcash.service.SmpAccountService;
import onlyone.sellerbotcash.service.SmpBankService;
import onlyone.sellerbotcash.service.SmpCommService;
import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.sellerbotcash.web.error.RestApiException;
import onlyone.sellerbotcash.web.util.SecurityAES256;
import onlyone.sellerbotcash.web.util.XmlParseUtil;
import onlyone.sellerbotcash.web.vm.JoinCustInfo;
import onlyone.sellerbotcash.web.vm.JoinSessionInfo;
import onlyone.sellerbotcash.web.vm.JoinTerms;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.Cafe24StoreDTO;
import onlyone.smp.service.dto.ExtAgreeTermsReqDTO;
import onlyone.smp.service.dto.ExtBankResponseDTO;
import onlyone.smp.service.dto.ExtCommCdDTO;
import onlyone.smp.service.dto.ExtCusAcctDTO;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtCustDetailDTO;
import onlyone.smp.service.dto.ExtCustMallDTO;
import onlyone.smp.service.dto.ExtCustMallListDTO;
import onlyone.smp.service.dto.ExtCustPackageDTO;
import onlyone.smp.service.dto.ExtDealCustMallDTO;
import onlyone.smp.service.dto.ExtSimpleJoinReqDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO;
import onlyone.smp.service.dto.ExtSvcTermsDTO;
import onlyone.smp.service.dto.ExtUsingGoodsInfoDTO;
import onlyone.smp.service.dto.ExtSmtAgreeTermsDTO.ExtSmtAgreeTermsInfoDTO;

@Slf4j
@Controller
public class JoinController {
    // 소드원 회원가입 계정
    private final static String KEY_SODEONE_MEMBER = "sodeone_member";
    // 셀러봇 캐시 회원 가입 계정
    private final static String KEY_SELERBOTCASH_MEMBER = "selerbotcash_member";
    // 셀러봇 캐시 회원 가입 계정
    private final static String KEY_INTEGRATION_MEMBER = "integration_member";

    @Value("${smp.svcId:sellerbotcash}")
    private String svcId;
    @Value("${internal.redirect.base:}")
    private String redirectBase;

    @Autowired
    private SmpCustService smpCustService;
    @Autowired
    private SmpJoinService smpJoinService;
    @Autowired
    private SmpCommService smpCommService;
    @Autowired
    private SmpAccountService smpAccountService;
    @Autowired
    private SmpCustMallService smpCustMallService;
    @Autowired
    private SmpBankService smpBankService;
    @Autowired
    private Cafe24Service cafe24Service;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private MypageService mypageService;

    /**
     * 스드원 > 셀러봇캐시 약관 전체 동의
     * 
     * @param modelMap
     * @param user
     * @param session
     * @return
     */
    @GetMapping("/pub/member/join_step1")
    public String getJoinStep1(ModelMap modelMap, @AuthenticationPrincipal User user, HttpSession session,
            HttpServletRequest request) {

        // 로그인 상태에서 join_step에 접속시 메인 화면으로 이동 시킨다.
        if (Objects.nonNull(user)) {
            return "redirect:" + redirectBase + "/";
        }

        JoinSessionInfo joinSessionInfo = new JoinSessionInfo();
        List<ExtSvcTermsDTO> list = smpJoinService.getSellerBotTerms(svcId);
        // 모든 약관 아이디 저장
        joinSessionInfo.setAllTermsIds(list.stream().collect(Collectors.toMap(k -> k.getTerms_id(),
                v -> new JoinTerms(v.getTerms_esse_yn() == EYesOrNo.Y, v.getSvc_id(), v.getTerms_id()))));
        // 필수 약관 아이디 저장
        joinSessionInfo.setEssentialTermsIds(list.stream().filter(it -> {
            return it.getTerms_esse_yn() == EYesOrNo.Y;
        }).map(it -> {
            return it.getTerms_id();
        }).sorted().collect(Collectors.toList()));

        // 20221117 셀러봇캐시 마켓팅 활용 세부 항목 가져오기 추가
        List<ExtCommCdDTO> smtTermsList = smpCommService.getCodeList("SMT");
        smtTermsList = smtTermsList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord).reversed()).collect(Collectors.toList());

        String siteCd = (String) session.getAttribute("siteCd");
        if (siteCd != null) {
            joinSessionInfo.setSiteCd(EInfPath.valueOf(siteCd));
            if (EInfPath.CF24.name().equals(siteCd))
                joinSessionInfo.setMallId((String) session.getAttribute("siteId"));
        }

        Long eventNo = (Long) session.getAttribute("pe");
        if (eventNo != null) {
            joinSessionInfo.setEventNo(eventNo);
        }

        String infPath = (String) session.getAttribute("infPath");
        if (infPath != null) {
            joinSessionInfo.setInfPath(infPath);
        }

        session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);
        modelMap.addAttribute("sellerbotTerms", list);
        modelMap.addAttribute("smtTermsList", smtTermsList); // 셀러봇캐시 마켓팅 활용 세부 항목

        return "sub/member/join_step1";
    }

    @PostMapping("/pub/member/join_step1")
    public String postJoinStep1(String[] sellerbotTerm, String[] smtTerm, HttpSession session) {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);
        // session이 생성되지 않은 상태이면 메인으로 이동 시킨다.
        if (joinSessionInfo == null || sellerbotTerm == null) {
            return "redirect:" + redirectBase + "/pub/member/join_step1";
        }

        Map<Long, JoinTerms> allTerm = joinSessionInfo.getAllTermsIds();
        Map<Long, JoinTerms> selectedTerm = new TreeMap<>();
        int essentialSize = 0;
        // 약관 선택 오류 확인 및 잘못 전달된 id 값 확인
        for (String item : sellerbotTerm) {
            Long sTerm = Long.parseLong(item);
            if (!allTerm.containsKey(sTerm))
                throw new RuntimeException("Not Found TermId " + sTerm);
            if (selectedTerm.containsKey(sTerm))
                throw new RuntimeException("Same TermId Get " + sellerbotTerm);
            JoinTerms terms = allTerm.get(sTerm);
            Boolean isEssentialTerm = terms.isEssTerm();
            selectedTerm.put(sTerm, terms);
            if (isEssentialTerm)
                essentialSize++;
        }

        if (essentialSize != joinSessionInfo.getEssentialTermsIds().size()) {
            log.error("필수 약관 선택 수 오류 " + selectedTerm);
            return "redirect:" + redirectBase + "/pub/member/join_step1";
        }
        joinSessionInfo.setSelectedTerms(selectedTerm);

        // 셀러봇캐시 마케팅 활용 동희 항목
        List<String> selectedSmtTerms = new ArrayList<String>();
        if (!org.springframework.util.StringUtils.isEmpty(smtTerm)) {
            for (String smtTermItem : smtTerm) {
                selectedSmtTerms.add(smtTermItem);
            }
        }
        joinSessionInfo.setSelectedSmtTerms(selectedSmtTerms);

        if (joinSessionInfo.getMallId() != null) {
            Cafe24StoreDTO tmp = cafe24Service.getStoreInfo(joinSessionInfo.getMallId());
            // 카페24 연동API에 데이터가 존재할 경우 session 에 저장
            if (tmp != null && tmp.getStore() != null) {
                joinSessionInfo.setCafeStore(tmp.getStore());
            }
        }

        session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);

        return "redirect:" + redirectBase + "/pub/member/join_step2";
    }

    @GetMapping("/pub/member/join_step2")
    public String getJoinStep2(ModelMap modelMap, HttpSession session) {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);

        // session이 생성되지 않은 상태이면 step1으로 이동 시킨다.
        if (joinSessionInfo == null) {
            return "redirect:" + redirectBase + "/pub/member/join_step1";
        }

        // 약관 선택이 잘되어 있는지 확인 한다.
        if (joinSessionInfo.getSelectedTerms() == null || joinSessionInfo.getSelectedTerms().size() == 0) {
            return "redirect:" + redirectBase + "/pub/member/join_step1";
        }

        modelMap.addAttribute("custDto", joinSessionInfo.getCustDto());
        modelMap.addAttribute("cafeStore", joinSessionInfo.getCafeStore());

        return "sub/member/join_step2";
    }

    @GetMapping("/pub/member/join_step3")
    public String getJoinStep3(ModelMap modelMap, @RequestParam(required = false) String error, HttpSession session) {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);

        // session이 생성되지 않은 상태이면 step1으로 이동 시킨다.
        if (joinSessionInfo == null) {
            return "redirect:" + redirectBase + "/pub/member/join_step1";
        }
        // 약관 선택 정보가 없을 경우 step1으로 이동 한다.
        if (joinSessionInfo.getSelectedTerms() == null || joinSessionInfo.getSelectedTerms().size() == 0) {
            return "redirect:" + redirectBase + "/pub/member/join_step1";
        }
        // 계정 정보가 있는지 확인 한다.
        if (joinSessionInfo.getCustDto() == null || StringUtils.isEmpty(joinSessionInfo.getCustDto().getCust_id())) {
            return "redirect:" + redirectBase + "/pub/member/join_step2";
        }
        // ID 인증 처리 확인
        if (!joinSessionInfo.isCompleteAuthNo()) {
            return "redirect:" + redirectBase + "/pub/member/join_step2";
        }

        // 유입 경로 조회
        List<ExtCommCdDTO> codeList = smpCommService.getCodeList("A020");
        List<ExtCommCdDTO> sortedCodeList = codeList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord).reversed())
                .collect(Collectors.toList());

        modelMap.addAttribute("codeList", sortedCodeList);
        modelMap.addAttribute("custDto", joinSessionInfo.getCustDto());
        modelMap.addAttribute("error", error);

        modelMap.addAttribute("cafeStore", joinSessionInfo.getCafeStore());

        EInfPath siteCd = joinSessionInfo.getSiteCd();
        if (siteCd != null)
            modelMap.addAttribute("spCode", siteCd.name());

        return "sub/member/join_step3";
    }

    @GetMapping("/sub/member/join_step4")
    public String getJoinStep4(ModelMap modelMap, @RequestParam(required = false) String error, HttpSession session) {
        // log.debug("** {}", session.getClass().getName());
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        /**
         * 2020-06-23 고객 몰 정보가 존재하면 화면 이동
         */
        ExtCustMallListDTO custMall = smpCustMallService.getCustMall(user.getUsername());
        if (Objects.nonNull(custMall) && custMall.getAll_mall_cnt() > 0) {
            return "redirect:" + redirectBase + "/";
        }
        
        return "nomenu/sub/member/join_pub_step4";
    }

    @PostMapping("/pub/member/join_step3")
    public String postJoinStep3(JoinCustInfo custInfo, ModelMap modelMap, HttpServletRequest request,
            HttpSession session) {
        JoinSessionInfo joinSessionInfo = (JoinSessionInfo) session.getAttribute(SellerbotConstant.JOIN_SESSON_NAME);

        // session이 생성되지 않은 상태이면 step1으로 이동 시킨다.
        if (joinSessionInfo == null) {
            return "redirect:" + redirectBase + "/pub/member/join_step1";
        }

        // 약관 선택 정보가 없을 경우 step1으로 이동 한다.
        if (joinSessionInfo.getSelectedTerms() == null || joinSessionInfo.getSelectedTerms().size() == 0) {
            return "redirect:" + redirectBase + "/pub/member/join_step1";
        }

        // 계정 정보가 있는지 확인 한다.
        if (joinSessionInfo.getCustDto() == null || StringUtils.isEmpty(joinSessionInfo.getCustDto().getCust_id())) {
            return "redirect:" + redirectBase + "/pub/member/join_step2";
        }

        // ID 인증 처리 확인
        if (!joinSessionInfo.isCompleteAuthNo()) {
            return "redirect:" + redirectBase + "/pub/member/join_step2";
        }

        ExtCustPackageDTO custDto = joinSessionInfo.getCustDto();
        // 사업자 번호 중복 확인
        if (StringUtils.isEmpty(custDto.getBiz_no())) {
            return "redirect:" + redirectBase + "/pub/member/join_step3";
        }

        // 기본 정보 설정
        custDto.setPasswd(custInfo.getPasswd());
        custDto.setPasswd_same(custInfo.getPasswd_same());
        custDto.setBiz_nm(custInfo.getBiz_nm());
        custDto.setCeo_nm(custInfo.getCeo_nm());
        custDto.setCeo_no(custInfo.getCeo_no_svc() + custInfo.getCeo_no());
        if (StringUtils.isNotEmpty(custInfo.getChrg_nm())) {
            custDto.setChrg_nm(custInfo.getChrg_nm());
        }
        if (StringUtils.isNotEmpty(custInfo.getChrg_no())) {
            custDto.setChrg_no(custInfo.getCharg_no_svc() + custInfo.getChrg_no());
        }
        custDto.setInf_path_cd(custInfo.getInf_path_cd());

        // 선택된 약관을 list로 변환
        List<ExtAgreeTermsReqDTO> terms = joinSessionInfo.getSelectedTerms().values().stream().map(it -> {
            ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
            t.setCust_id(custDto.getCust_id());
            t.setSvc_id(it.getSvcId());
            t.setTerms_id(it.getTermId());
            return t;
        }).collect(Collectors.toList());

        // 약관 정보 설정
        custDto.setTerms(terms);

        // 20221118 셀러봇 캐시 마켓팅 동의 선택된 항목을 list로 변환
        ExtSmtAgreeTermsDTO d = new ExtSmtAgreeTermsDTO();
        d.setCust_id(custInfo.getCust_id());
        d.setSvc_id(svcId);
        // d.setCust_seq_no는 SMP에서 신규 회원정보 생성 후 처리함.

        List<ExtSmtAgreeTermsInfoDTO> smtTerms = joinSessionInfo.getSelectedSmtTerms().stream().map(s -> {
            ExtSmtAgreeTermsInfoDTO e = new ExtSmtAgreeTermsInfoDTO();
            e.setSmt_terms_cd(s);
            e.setSmt_terms_aggr_yn(EYesOrNo.Y);

            return e;
        }).collect(Collectors.toList());
        d.setSmt_aggr_item_list(smtTerms);
        // 20221118 셀러봇 캐시 마켓팅 설정
        custDto.setSmtTerm(d);

        custDto.setInf_site_cd(joinSessionInfo.getSiteCd());
        custDto.setMallId(joinSessionInfo.getMallId());
        custDto.setEventNo(joinSessionInfo.getEventNo());

        if (smpJoinService.addCustPkg(custDto)) {

            session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, null);
            // 회원가입 완료 시 로그인 처리 프로세스
            User user = new User(custDto.getCust_id(), "", new ArrayList<>());
            ServiceSiteAuthenticationToken token = new ServiceSiteAuthenticationToken(user, "", GRANTEDAUTH);
            request.getSession();
            token.setDetails(new WebAuthenticationDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(token));

            // 20210616 회원가입 알림톡 발송
            smpCustService.alarm_send_join(custDto.getCust_id());

            // 20211020 가입프로모션
            try {
                log.info("####################JOINFREE paymentPromotion Start###################" + custDto.getCust_id());
                paymentService.paymentPromotion(custDto.getCust_id(), "FREE", "", "", "JOINFREE");
            } catch (Exception e) {
                log.error("JOINFREEPromotion Exception####" + e);
            }

            return "redirect:" + redirectBase + "/pub/member/join_complete";
        } else {
            return "redirect:" + redirectBase + "/pub/member/join_step3?error=add";
        }
    }

    @GetMapping("/pub/member/join_complete")
    public String getJoinComplete(ModelMap modelMap) {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        String userName = user.getUsername();
        
        // 이용중인 프로모션 서비스 조회
        ExtUsingGoodsInfoDTO usingGoods = mypageService.getPayInfoForUsingGoods(userName);
        if (usingGoods != null) {
            if (usingGoods.getAddn_goods_req_seq_no() != null) {
                modelMap.addAttribute("usingGoods", usingGoods);
            }
        }
        
        modelMap.addAttribute("goUrl", redirectBase + "/");
        return "sub/member/join_complete";
    }

    /**
     * 첫로그인시 몰등록
     * 
     * @param user
     * @param extCustMallDTO
     * @param session
     * @return
     */
    @PostMapping("/sub/member/create_mall")
    public ResponseEntity<String> create_mall(@AuthenticationPrincipal User user,
            @RequestBody List<ExtCustMallDTO> extCustMallDTO, HttpSession session) {
        // log.debug("** {}", session.getClass().getName());

        List<ExtCustMallDTO> list = new ArrayList<ExtCustMallDTO>();
        try {
            list = smpJoinService.createMall(extCustMallDTO, user.getUsername());
        } catch (Exception e) {
            // 20210420 SMP에서 오류 메세지가 있을 경우 오류 메세지로 알럿트 나오게 처리
            JSONObject jObject = new JSONObject(e.getMessage());
            if (jObject.has("comment")) {
                String comment = jObject.getString("comment");
                
                return ResponseEntity.badRequest().body(comment);
            } else {
                log.error("첫 mall 등록 중 에러가 발생하였습니다.\n" + e);
                return ResponseEntity.badRequest().body("ERROR");
            }

        }

        // 소드원 회원만 스크랩핑을 실행한다.
        try {
            smpJoinService.scrap(list, user.getUsername());
        } catch (Exception e) {
            log.error("첫 mall 등록 후 스크래핑 처리시 에러가 발생하였습니다.\n" + e);
        }
        session.setAttribute("cust", smpCustService.getCust(user.getUsername()));
        return ResponseEntity.ok("{\"result\":\"OK\"}");
    }

    @PostMapping("/sub/member/update_mall")
    public ResponseEntity<String> update_mall(@AuthenticationPrincipal User user,
            @RequestBody List<ExtCustMallDTO> extCustMallDTO, HttpSession session) {
        TreeMap<String, String> map = smpJoinService.updateMall(extCustMallDTO, user.getUsername());
        if (true == map.get("code").equals(HttpStatus.OK.name())) {
            /* boolean result2 = */smpJoinService.scrap(extCustMallDTO, user.getUsername());
            session.setAttribute("cust", smpCustService.getCust(user.getUsername()));
            return ResponseEntity.ok("{\"result\":\"OK\"}");

        } else {
            String errMsg = "ERROR";

            // 20210420 SMP에서 오류 메세지가 있을 경우 오류 메세지로 알럿트 나오게 처리
            if (map.containsKey("comment")) {
                errMsg = map.get("comment");
            }

            return ResponseEntity.badRequest().body(errMsg);
        }
    }

    @GetMapping("/sub/member/join_step5")
    public String getJoinStep5(ModelMap modelMap, HttpSession session) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();

        ExtCustDTO ecd = smpCustService.getCust(user.getUsername());

        if(ecd.getKkont_req_yn() == EYesOrNo.Y) {
            return "redirect:" + redirectBase + "/sub/member/join_step6";
        }

        modelMap.addAttribute("cust", ecd);
        return "nomenu/sub/member/join_step5";
    }

    @GetMapping("/sub/member/join_step6")
    public String getJoinStep6(ModelMap modelMap, HttpSession session)
            throws URISyntaxException, JsonProcessingException {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication auth = securityContext.getAuthentication();
        User user = (User) auth.getPrincipal();
        String userName = user.getUsername();

        ExtCusAcctDTO extCusAcctDTO = smpAccountService.getCustAcctList(userName);
        if (Objects.nonNull(extCusAcctDTO) && extCusAcctDTO.getAll_acct_cnt() > 0) {
            return "redirect:" + redirectBase + "/";
        }

        ObjectMapper mapper = new ObjectMapper();

        // 은행 정보
        List<ExtBankResponseDTO> bankList = smpBankService.getBankList();
        modelMap.addAttribute("bankList", mapper.writeValueAsString(bankList));

        // 통화 코드
        List<ExtCommCdDTO> codeList = smpCommService.getCodeList("A056");
        modelMap.addAttribute("codeList", codeList);

        // 2020-03-18 고객의 계좌 삭제 가능 여부 확인 [CASH-266]
        String delConfirm = smpCustService.getCustAcctDeleteConfirm(user.getUsername());
        modelMap.addAttribute("delConfirm", delConfirm);

        // 이용중인 프로모션 서비스 조회
        ExtUsingGoodsInfoDTO usingGoods = mypageService.getPayInfoForUsingGoods(userName);
        boolean usingGoodFlag = false;
        if (usingGoods != null) {
            if (usingGoods.getAddn_goods_req_seq_no() != null) {
                usingGoodFlag = true;
            }
        }
        modelMap.addAttribute("usingGoodFlag", usingGoodFlag);

        return "nomenu/sub/member/join_step6";
    }

    /**
     * 정산계좌 리스트 조회
     * 
     * @param user
     * @return
     */
    @ResponseBody
    @GetMapping(value = "/sub/member/join_step6/acct", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ExtCusAcctDTO> getAcctList(@AuthenticationPrincipal User user) {
        ExtCusAcctDTO extCusAcctDTO = smpAccountService.getCustAcctList(user.getUsername());
        return new ResponseEntity<ExtCusAcctDTO>(extCusAcctDTO, HttpStatus.OK);
    }

    /**
     * 약관동의
     * 
     * @param modelMap
     * @param session
     * @param request
     * @return
     */
    @GetMapping("/pub/member_only1/join_step1")
    public String member_only1_step1(ModelMap modelMap, HttpSession session, HttpServletRequest request) {

        // 로그인한 브라우저로 신규 회원 가입을 페이지에 접근하는 경우 세션 초기화
        log.debug("session : {}", session);
        session.invalidate();

        String token = request.getParameter("token");
        String svc_id = request.getParameter("svc_id"); // 파라미터 서비스 아이디
        if (Objects.isNull(token)) {
            token = request.getParameter("TOKEN");
        }

        if (Objects.isNull(token) || Objects.isNull(svc_id)) {
            log.debug("parameter not null value [token: {}, svc_id: {}]", token, svc_id);
            return "errors/403";
        }

        if (!smpJoinService.isExistSvcId(svc_id)) {
            log.debug("not allow [svc_id: {}]", svc_id);
            return "errors/403";
        }

        boolean check = smpCustService.tokenCheck(token);

        // 허용 하는 svc id 추가
        if (check) {
            List<ExtSvcTermsDTO> sellerbotTerms = smpJoinService.getSellerBotTerms(svcId); // 프로퍼티 서비스아이디
            List<ExtSvcTermsDTO> sodeOneTerms = smpJoinService.getSellerBotTerms(svc_id);

            // 20221206 셀러봇캐시 마켓팅 활용 세부 항목 가져오기 추가
            List<ExtCommCdDTO> smtTermsList = smpCommService.getCodeList("SMT");
            smtTermsList = smtTermsList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord).reversed()).collect(Collectors.toList());

            modelMap.addAttribute("svc_cust_id", request.getParameter("svc_cust_id"));
            modelMap.addAttribute("svc_biz_no", request.getParameter("svc_biz_no"));
            modelMap.addAttribute("sellerbotTerms", sellerbotTerms);
            modelMap.addAttribute("sodeOneTerms", sodeOneTerms);
            modelMap.addAttribute("svc_id", svc_id);
            modelMap.addAttribute("smtTermsList", smtTermsList);

            return "sub/member_only1/join_step1";
        } else {
            log.debug("not allow [token: {}]", token);
            return "errors/403";
        }

    }

    /**
     * 서비스 가입여부 조회
     * 
     * @param modelMap
     * @param sodeOneTerm
     * @param sellerbotTerm
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/join_step2")
    public String member_only1_step2(ModelMap modelMap, String sellerbotTerm, String smtTerm, String sodeOneTerm, String svc_id,
            HttpServletRequest req) {

        String svc_cust_id = req.getParameter("svc_cust_id");
        String svc_biz_no = req.getParameter("svc_biz_no");
        String step = req.getParameter("step"); // 화면정보

        modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
        modelMap.addAttribute("smtTerm", smtTerm);
        modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
        modelMap.addAttribute("svc_cust_id", svc_cust_id);
        modelMap.addAttribute("svc_biz_no", svc_biz_no);
        modelMap.addAttribute("svc_id", svc_id);

        // 소드원 가입여부 조회 건너뛰기
        if (Objects.nonNull(step) && "pre_join_step2".equals(step)) {
            return "sub/member_only1/join_step2";
        }

        if (StringUtils.isEmpty(svc_cust_id) && StringUtils.isEmpty(svc_biz_no)) {
            // 통합외원 가입
            return "sub/member_only1/pre_join_step2";
        } else {
            //
            return "sub/member_only1/join_step2";
        }
    }

    /**
     * pre_join_step2 전용 > 소드원 회원가입 조회
     * 
     * @param password
     * @param svc_cust_id
     * @param session
     * @return
     */
    @PostMapping("/pub/member_only1/service_join_check")
    public @ResponseBody HashMap<String, Boolean> service_join_check(String password, String svc_cust_id,
            HttpSession session) {

        HashMap<String, Boolean> result = new HashMap<String, Boolean>();

        String token = smpCustService.getToken(svc_cust_id);
        // 1. 소드원 회원정보 조회
        List<HashMap<String, Object>> list = smpJoinService.custAndBiz(password, svc_cust_id, token);
        result.put(KEY_SODEONE_MEMBER, Objects.isNull(list) ? false : true);

        try {

            // 2. 셀러봇 캐시 회원 정보
            ExtCustDetailDTO extCustDetailDTO = smpCustService.getCustDetail(svc_cust_id);
            if (Objects.nonNull(extCustDetailDTO)) {
                result.put(KEY_SELERBOTCASH_MEMBER, true);
            } else {
                result.put(KEY_SELERBOTCASH_MEMBER, false);
            }
        } catch (RestApiException e) {
            result.put(KEY_SELERBOTCASH_MEMBER, false);
        }

        return result;
    }

    @PostMapping("/pub/member_only1/svc_cust_confirm")
    public String join_step2_svc_cust_confirm(ModelMap modelMap, String sellerbotTerm, String smtTerm, String sodeOneTerm,
            HttpSession session, HttpServletRequest request) {
        // log.debug("** {}", session.getClass().getName());
        String password = request.getParameter("password");
        String req_cust_id = request.getParameter("req_cust_id");
        String token = smpCustService.getToken(req_cust_id);

        List<HashMap<String, Object>> list = smpJoinService.custAndBiz(password, req_cust_id, token);
        if (null == list) {
            list = new ArrayList<HashMap<String, Object>>();
        }
        if (list.size() >= 2) {
            modelMap.addAttribute("list", list);
            return "sub/member_only1/id_merge";
        } else if (list.size() == 1) {
            modelMap.addAttribute("svc_biz_no", list.get(0).get("svc_biz_no"));
            modelMap.addAttribute("svc_cust_id", list.get(0).get("svc_cust_id"));
            modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
            modelMap.addAttribute("smtTerm", smtTerm);
            modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
            return "sub/member_only1/join_step2";
        } else {
            modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
            modelMap.addAttribute("smtTerm", smtTerm);
            modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
            return "sub/member_only1/join_step2";
        }
    }

    /**
     * 회원 정보 입력 화면
     * 
     * @param modelMap
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/join_step3")
    public String join_step3(ModelMap modelMap, HttpSession session, HttpServletRequest request) {
        // log.debug("** {}", session.getClass().getName());

        String svc_biz_no = request.getParameter("svc_biz_no");
        String svc_cust_id = request.getParameter("svc_cust_id");
        String cust_id = request.getParameter("cust_id");
        String sellerbotTerm = request.getParameter("sellerbotTerm");
        String smtTerm = request.getParameter("smtTerm");
        String sodeOneTerm = request.getParameter("sodeOneTerm");

        if (StringUtils.isNotEmpty(svc_cust_id)) {
            // 1. 소드원 계정이 있는 경우
            String auth_token = smpJoinService.confirmCust(cust_id);
            modelMap.addAttribute("cust_info", smpJoinService.getSodeCustInfo(svc_cust_id, auth_token));
        } else {
            modelMap.addAttribute("cust_info", new JoinCustInfo());
        }

        // 유입 경로 조회
        List<ExtCommCdDTO> codeList = smpCommService.getCodeList("A020");
        List<ExtCommCdDTO> sortedCodeList = codeList.stream().sorted(Comparator.comparing(ExtCommCdDTO::getSort_ord))
                .collect(Collectors.toList());
        modelMap.addAttribute("codeList", sortedCodeList);

        modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
        modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
        modelMap.addAttribute("smtTerm", smtTerm);
        modelMap.addAttribute("svc_biz_no", svc_biz_no);
        modelMap.addAttribute("svc_cust_id", svc_cust_id);
        modelMap.addAttribute("cust_id", cust_id);

        return "sub/member_only1/join_step3";
    }

    /**
     * pre_join_step3 을 cash_cust_confirm으로 변경
     * 
     * @param modelMap
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/cash_cust_confirm")
    public String pre_join_step3(ModelMap modelMap, String sellerbotTerm, String sodeOneTerm, HttpSession session,
            HttpServletRequest request) {
        // log.debug("** {}", session.getClass().getName());
        String svc_biz_no = request.getParameter("svc_biz_no");
        String cust_id = request.getParameter("cust_id");

        modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
        modelMap.addAttribute("sellerbotTerm", sellerbotTerm);
        modelMap.addAttribute("svc_biz_no", svc_biz_no);
        modelMap.addAttribute("cust_id", cust_id);

        return "sub/member_only1/pre_join_step3";
    }

    /**
     * 회원 가입 성공 처리
     * 
     * @param modelMap
     * @param sodeOneTerm
     * @param sellerbotTerm
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/join_step4")
    public String join_step4(ModelMap modelMap, String sodeOneTerm, String sellerbotTerm, HttpSession session,
            HttpServletRequest request) {
        // log.debug("** {}", session.getClass().getName());
        log.debug("join_step4 start");
        String svc_biz_no = request.getParameter("svc_biz_no"); // 서비스
        String cust_id = request.getParameter("cust_id");
        String svc_cust_id = request.getParameter("svc_cust_id");
        // 고객 정보 상세 최득
        log.debug(cust_id);

        ExtCustDetailDTO extCustDTO = smpCustService.getCustDetail(cust_id);

        modelMap.addAttribute("svc_biz_no", svc_biz_no);
        modelMap.addAttribute("cust_id", cust_id);
        modelMap.addAttribute("svc_cust_id", svc_cust_id);
        modelMap.addAttribute("sodeOneTerm", sodeOneTerm);
        modelMap.addAttribute("sellerbotTerm", sellerbotTerm);

        modelMap.addAttribute("extCustDTO", extCustDTO);

        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();

        if (Objects.nonNull(svc_cust_id) && Objects.nonNull(svc_biz_no)) {
            String token = smpCustService.getToken(svc_cust_id);
            list = smpJoinService.custAndBiz(svc_cust_id, token);
            modelMap.addAttribute("list", list);
        }

        // 소드원 사업자 번호와 셀러봇 사업자 번호가 같은 경우 || 셀러봇 계정은 있고, 소드원은 없는경우
        if (svc_biz_no.equals(extCustDTO.getBiz_no())
                || (StringUtils.isEmpty(svc_biz_no) && !StringUtils.isEmpty(extCustDTO.getBiz_no()))) {

            if (list.size() == 0) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("cust_id", cust_id);
                map.put("biz_no", extCustDTO.getBiz_no());
                list.add(map);
                modelMap.addAttribute("list", list);
            }

            return "sub/member_only1/id_merge";
        } else {
            // 소드원 정보가 있는 경우
            // 통합 불가
            return "sub/member_only1/join_end";
        }

    }

    /**
     * 아이디 통합하기
     */

    @PostMapping("/pub/member_only1/join_integrate")
    public String join_integrate(ModelMap modelMap, String sodeOneTerm[], String sellerbotTerm[], HttpSession session,
            HttpServletRequest request) {
        String svc_biz_no = request.getParameter("svc_biz_no"); // 서비스
        String cust_id = request.getParameter("cust_id");
        String svc_cust_id = request.getParameter("svc_cust_id");
        String token = smpCustService.getToken(svc_cust_id);
        // 고객 정보 상세 최득
        log.debug(cust_id);

        ExtCustDetailDTO extCustDTO = smpCustService.getCustDetail(cust_id);

        // 소드원 사업자 번호와 셀러봇 사업자 번호가 같은 경우
        if (svc_biz_no.equals(extCustDTO.getBiz_no())) {

            // 통합 가입 회원
            List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();

            // 셀러봇 약관 저장
            for (int i = 0; i < sellerbotTerm.length; i++) {
                ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                t.setCust_id(cust_id);
                t.setSvc_id(svcId);
                t.setTerms_id(Long.parseLong(sellerbotTerm[i]));
                terms.add(t);
            }

            // 소드원 약관 저장
            List<ExtAgreeTermsReqDTO> sodeTerms = new ArrayList<ExtAgreeTermsReqDTO>();

            for (int j = 0; j < sodeOneTerm.length; j++) {
                ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                t.setCust_id(cust_id);
                t.setSvc_id("sodeone");
                t.setTerms_id(Long.parseLong(sodeOneTerm[j]));
                sodeTerms.add(t);
            }

            // 약관 등록
            smpJoinService.addTerms(terms);
            smpJoinService.addTerms(sodeTerms);

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("svc_id", svcId);
            map.put("cust_id", cust_id);
            map.put("cust_seq_no", extCustDTO.getCust_seq_no());
            map.put("auth_token", token);

            // 고객 일번변호 등록
            smpJoinService.svcCustInsert(map);

        } else {

            // 셀러봇 계정은 있고, 소드원은 없는경우
            if (StringUtils.isEmpty(svc_biz_no) && !StringUtils.isEmpty(extCustDTO.getBiz_no())) {

                // 통합 가입 회원
                List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();

                // 셀러봇 약관 저장
                for (int i = 0; i < sellerbotTerm.length; i++) {
                    ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                    t.setCust_id(cust_id);
                    t.setSvc_id(svcId);
                    t.setTerms_id(Long.parseLong(sellerbotTerm[i]));
                    terms.add(t);
                }

                // 소드원 약관 저장
                List<ExtAgreeTermsReqDTO> sodeTerms = new ArrayList<ExtAgreeTermsReqDTO>();

                for (int j = 0; j < sodeOneTerm.length; j++) {
                    ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                    t.setCust_id(cust_id);
                    t.setSvc_id("sodeone");
                    t.setTerms_id(Long.parseLong(sodeOneTerm[j]));
                    sodeTerms.add(t);
                }

                // 약관 등록
                // 셀러봇 약관은 추가 등록하지 않는다.
                smpJoinService.addTerms(terms);
                smpJoinService.addTerms(sodeTerms);

                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("svc_id", svcId);
                map.put("cust_id", cust_id);
                map.put("cust_seq_no", extCustDTO.getCust_seq_no());
                map.put("auth_token", token);

                // 고객 일번변호 등록
                smpJoinService.svcCustInsert(map);

            } else {
                log.debug("svc_biz_no : {0}", svc_biz_no);
                log.debug("extCustDTO : {0}", extCustDTO);
                return "errors/500";

            }
        }

        return "sub/member_only1/join_complete";
    }

    /**
     * 인증 번호 전송 및 계정정보 확인
     * 
     * @param cust_id
     * @param session
     * @return
     */
    @PostMapping("/pub/member_only1/join_id_check")
    public ResponseEntity<String> join_id_check(String svc_id, String cust_id, HttpSession session) {
        // log.debug("** {}", session.getClass().getName());

        // 3.2.1 서비스 유형 여부
        boolean isExistSvcId = smpJoinService.isExistSvcMember(svc_id, cust_id);

        //
        if (isExistSvcId) {
            return ResponseEntity.status(400).body(KEY_INTEGRATION_MEMBER);
        }

        // 셀로봇 캐시 회원가입 여부 체크
        if (smpJoinService.isExistSvcMember(svcId, cust_id)) {
            return ResponseEntity.status(409).body(KEY_SELERBOTCASH_MEMBER);
        }

        String auth_token = smpJoinService.confirmCust(cust_id);
        return ResponseEntity.ok(auth_token);
    }

    @PostMapping("/pub/member_only1/seller_join_check")
    public @ResponseBody HashMap<String, Object> seller_join_check(String cust_id, String password,
            HttpSession session) {
        // log.debug("** {}", session.getClass().getName());
        HashMap<String, Object> map = smpCustService.sellerConfirm(cust_id, password);
        return map;
    }

    /**
     * 2.1.6 고객 인증 번호 확인 & 2.1.7 고객 비밀 번호 확인
     * 
     * @param cust_id
     * @param authNo
     * @param auth_token
     * @param session
     * @return
     */
    @PostMapping("/pub/member_only1/confirmCust")
    public ResponseEntity<String> confirmCust(String cust_id, String authNo, String auth_token, HttpSession session) {

        if (StringUtils.isEmpty(auth_token)) {
            return ResponseEntity.status(4490).body("not called");
        }

        // 2.1.6 고객 인증 번호 확인
        boolean result = smpJoinService.confirmCustAuthtoken(cust_id, authNo, auth_token);

        if (result) {
            return ResponseEntity.ok("");
        } else {
            return ResponseEntity.badRequest().body("not same");
        }

    }

    @PostMapping("/pub/member_only1/checkBizNo")
    public ResponseEntity<String> checkBizNo(String bizNo, HttpSession session) {
        smpJoinService.checkBizNo(bizNo);
        return ResponseEntity.ok("");

    }

    /**
     * 회원가입 > 처
     * 
     * @param custInfo
     * @param sodeOneTerm
     * @param sellerbotTerm
     * @param modelMap
     * @param session
     * @param request
     * @return
     */
    @PostMapping("/pub/member_only1/join_step_end")
    public String join_step_end(JoinCustInfo custInfo, String[] sodeOneTerm, String[] sellerbotTerm, String[] smtTerm, 
            ModelMap modelMap, HttpSession session, HttpServletRequest request) {

        ExtCustPackageDTO custDto = new ExtCustPackageDTO();

        custDto.setPasswd(custInfo.getPasswd());
        custDto.setPasswd_same(custInfo.getPasswd_same());
        custDto.setBiz_nm(custInfo.getBiz_nm());
        custDto.setCeo_nm(custInfo.getCeo_nm());
        custDto.setCeo_no(custInfo.getCeo_no_svc() + custInfo.getCeo_no());
        custDto.setCust_id(custInfo.getCust_id());
        custDto.setBiz_no(custInfo.getBiz_no());

        if (StringUtils.isNotEmpty(custInfo.getChrg_nm())) {
            custDto.setChrg_nm(custInfo.getChrg_nm());
        }
        if (StringUtils.isNotEmpty(custInfo.getChrg_no())) {
            custDto.setChrg_no(custInfo.getCharg_no_svc() + custInfo.getChrg_no());
        }
        custDto.setInf_path_cd(custInfo.getInf_path_cd());

        List<ExtAgreeTermsReqDTO> terms = new ArrayList<ExtAgreeTermsReqDTO>();
        List<ExtAgreeTermsReqDTO> sodeTerms = new ArrayList<ExtAgreeTermsReqDTO>();

        for (int i = 0; i < sellerbotTerm.length; i++) {
            ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
            t.setCust_id(custDto.getCust_id());
            t.setSvc_id(svcId);
            t.setTerms_id(Long.parseLong(sellerbotTerm[i]));
            terms.add(t);
        }

        custDto.setTerms(terms);

        // 20221118 셀러봇 캐시 마켓팅 동의 선택된 항목을 list로 변환
        // 셀러봇캐시 마케팅 활용 동희 항목
        List<String> selectedSmtTerms = new ArrayList<String>();
        if (!org.springframework.util.StringUtils.isEmpty(smtTerm)) {
            for (String smtTermItem : smtTerm) {
                selectedSmtTerms.add(smtTermItem);
            }
        }

        ExtSmtAgreeTermsDTO d = new ExtSmtAgreeTermsDTO();
        d.setCust_id(custInfo.getCust_id());
        d.setSvc_id(svcId);
        // d.setCust_seq_no는 SMP에서 신규 회원정보 생성 후 처리함.

        List<ExtSmtAgreeTermsInfoDTO> smtTerms = selectedSmtTerms.stream().map(s -> {
            ExtSmtAgreeTermsInfoDTO e = new ExtSmtAgreeTermsInfoDTO();
            e.setSmt_terms_cd(s);
            e.setSmt_terms_aggr_yn(EYesOrNo.Y);

            return e;
        }).collect(Collectors.toList());
        d.setSmt_aggr_item_list(smtTerms);
        // 20221118 셀러봇 캐시 마켓팅 설정
        custDto.setSmtTerm(d);

        // 세러봇캐시 회원정보 / 약관동의 저장 /
        if (smpJoinService.addCustPkg(custDto)) {

            // 셀러봇 캐시 회원가입 성공시 소드원도 가입처리 , 약관동의 사항 저장
            for (int j = 0; j < sodeOneTerm.length; j++) {
                ExtAgreeTermsReqDTO t = new ExtAgreeTermsReqDTO();
                t.setCust_id(custDto.getCust_id());
                t.setSvc_id("sodeone");
                t.setTerms_id(Long.parseLong(sodeOneTerm[j]));
                sodeTerms.add(t);
            }
            smpJoinService.addTerms(sodeTerms);
        }

        // 고객 상세 정보 취득
        log.debug(custDto.getCust_id());
        ExtCustDetailDTO extCustDTO = smpCustService.getCustDetail(custDto.getCust_id());
        // 토큰 정보 생성
        String token = smpCustService.getToken(custDto.getCust_id());

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("svc_id", svcId);
        map.put("cust_id", custDto.getCust_id());
        map.put("cust_seq_no", extCustDTO.getCust_seq_no());
        map.put("auth_token", token);

        // 소드원 고객정보 저장&업데이트
        smpJoinService.svcCustInsert(map);

        return "sub/member_only1/join_complete";
    }

    /**
     * S-MAP(인터파크) > 간편 가입
     * 
     * @param modelMap
     * @return
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws NoSuchProviderException
     * @throws DecoderException
     */

    // @ExceptionHandler
    @GetMapping("/pub/simple_join")
    public String formSimpleJoin(ModelMap modelMap, ExtSimpleJoinReqDTO dto, HttpSession session)
            throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException,
            DecoderException {
        // 1. 파라미터 파싱
        String data = SecurityAES256.AES_Decode(dto.getData());
        if (StringUtils.isEmpty(data)) {
            log.warn("Parameter[data] does not exist");
            return "redirect:" + redirectBase + "/";
        }

        XmlParseUtil xmlParseUtil = new XmlParseUtil(data);
        Map<String, Object> mUserInfo = xmlParseUtil.getMapValue();
        String bizNo = (String) mUserInfo.get("BIZNO");

        if (StringUtils.isEmpty(bizNo)) {
            log.warn("Biz No. does not exist");
            return "redirect:" + redirectBase + "/";
        }

        // 1.사업자 번호 체크
        // TODO: 테스트용 코드. 운영 배포시 반드시 주석 제거 할 것
        TreeMap<String, String> map = smpJoinService.isExistsBizNo(Objects.toString(bizNo));
        if (false == map.get("code").equals(HttpStatus.OK.name())) {
            String message = "사업자 번호가 유효하지 않습니다.";
            if (map.get("reason").equals("휴업/폐업/정보없음")) {
                message = "사업자 번호가 유효하지 않습니다.";
            } else if (map.get("reason").equals("중복 사업자번호")) {
                message = "이미 가입한 고객입니다. 로그인 후 이용해주세요.";

                session.setAttribute("siteCd", "IPK");
                session.setAttribute("pe", 52L);
            }
            modelMap.addAttribute("message", message);
            return "errors/400";
        }

        // 2. 화면 세션 저장[기존 회원가입과 동일하게 하기위해]
        JoinSessionInfo joinSessionInfo = new JoinSessionInfo();
        // 3. 이용 약관 조회
        List<ExtSvcTermsDTO> terms = smpJoinService.getSellerBotTerms(svcId);
        // 4. S-MAP 회원 정보
        EInfPath inf_path_cd = EInfPath.IPK;
        List<Map<String, String>> userMallList = new ArrayList<>();
        // token이 없는경우 인터파크 고정
        String token = dto.getToken();
        if (StringUtils.isNotEmpty(token)) {
            userMallList = smpJoinService.getSMapMemberInfo(token, bizNo, true);
            inf_path_cd = EInfPath.SMA;
        }

        joinSessionInfo.setSiteCd(inf_path_cd);
        // TODO: 운용 배포 시 eventNo 확인 할 것.
        Long eventNo = 52L; //dto.getPe(); 
        joinSessionInfo.setEventNo(eventNo);

        session.setAttribute(SellerbotConstant.JOIN_SESSON_NAME, joinSessionInfo);

        modelMap.addAttribute("inf_path_cd", inf_path_cd);
        modelMap.addAttribute("token", dto.getToken());
        modelMap.addAttribute("terms", terms);
        modelMap.addAttribute("userInfo", mUserInfo);
        modelMap.addAttribute("userMallList", userMallList);

        return "pub/simple_join";
    }

    /**
     * 사용자 sub 인증 이메일 가져오기
     * 
     * @param extCustMallDTO
     * @return ResponseEntity<String>
     */
    @PostMapping("/sub/member/get_cust_sub_auth_mail")
    @ResponseBody
    public ResponseEntity<String> getCustSubAuthMail(@RequestBody ExtCustMallDTO extCustMallDTO) {
        ExtDealCustMallDTO extLoanCustMall = smpCustMallService.getInfoForCustAuthMail(String.valueOf(extCustMallDTO.getCust_mall_seq_no()));

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("result", "OK");
        result.put("data", extLoanCustMall.getDeal_nm());
        
        JSONObject obj = new JSONObject(result);

        return ResponseEntity.ok(obj.toString());
    }

    /**
     * 네이버 인증 메일 발송
     * 
     * @param extCustMallDTO extCustMallDTO
     * @param User user
     * @return ResponseEntity<String>
     */
    @PostMapping("/sub/member/send_sub_auth_mail")
    @ResponseBody
    public ResponseEntity<String> sendSubAuthMail(@RequestBody ExtCustMallDTO extCustMallDTO, @AuthenticationPrincipal User user) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        
        try {
            List<ExtCustMallDTO> list = new ArrayList<ExtCustMallDTO>();
            list.add(extCustMallDTO);

            smpJoinService.scrap(list, user.getUsername());	

            result.put("result", "OK");
        } catch (Exception e) {
            log.error("인증메일 발송 에러발생 :: " + e.toString());
            result.put("result", "FAIL");
        }
        
        JSONObject obj = new JSONObject(result);
        return ResponseEntity.ok(obj.toString());
    }
}
