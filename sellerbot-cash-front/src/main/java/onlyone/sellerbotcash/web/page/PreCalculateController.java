package onlyone.sellerbotcash.web.page;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

// import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.SmpFinGoodService;
import onlyone.sellerbotcash.service.SmpJoinService;
import onlyone.sellerbotcash.service.SmpTermsService;
import onlyone.sellerbotcash.web.util.DateUtil;
import onlyone.smp.service.dto.ExtAgreeTermsReqDTO;
import onlyone.smp.service.dto.ExtFinGoodPageListDTO;
import onlyone.smp.service.dto.ExtFinGoodPageDetailDTO;


/**
 * 금융 서비스
 * @author bak
 *
 */
// @Slf4j
@Controller
public class PreCalculateController {
	
	@Autowired private SmpFinGoodService smpFinGoodService;
	@Autowired private SmpCustService smpCustService; 
	@Autowired private SmpJoinService smpJoinService;
	@Autowired private SmpTermsService smpTermsService;
	
	@Value("${smp.svcId:sellerbotcash}") private String svcId;
	@Value("${sodeone.url}") private String SODEONE_HOME;
	
	/**
	 * 금융 서비스 목록 페이지
	 * 
	 * @param modelMap
	 * @param user
	 * @param session
	 * @return
	 * 
	 */
	@RequestMapping(value="/sub/pre_calculate/pre_calculate")
	public String getFinGoodList(ModelMap modelMap, @AuthenticationPrincipal User user, HttpSession session) {
		
		String cust_type =  smpCustService.getCustUseLoan(user.getUsername());
		String scra_dt = DateUtil.getDateString(LocalDate.now());
		
		List<ExtFinGoodPageListDTO> list = 
			smpFinGoodService.getFinGoodPageList(user.getUsername(), cust_type, scra_dt);

		modelMap.addAttribute("finGoodList", list);
		modelMap.addAttribute("totalInq", list.stream().mapToLong(i->i.getList().getInq_cnt()).sum());

		return "sub/pre_calculate/pre_calculate";
	}
		
	/**
	 * 금융 서비스 > 금융 서비스 상품 상세보기
	 * 
	 * @param modelMap
	 * @param user
	 * @param session
	 * @return
	 *  
	 */
	@RequestMapping(value="/sub/pre_calculate/detail")
	public String getFinGoodDetail(String finGoodSeqNo, ModelMap modelMap, @AuthenticationPrincipal User user, HttpSession session, Device device) {
		
		String cust_type = smpCustService.getCustUseLoan(user.getUsername());
		String scra_dt = DateUtil.getDateString(LocalDate.now());
		
		ExtFinGoodPageDetailDTO detail = 
			smpFinGoodService.getFinGoodPageDetail(finGoodSeqNo, user.getUsername(), cust_type, scra_dt);
		
		modelMap.addAttribute("terms", detail.getExtSvcTermsDTO());
		modelMap.addAttribute("totalInterest", detail.getTotalInterest());
		modelMap.addAttribute("maxUseDate", detail.getMaxUseDate());
		modelMap.addAttribute("rcm", detail.getRcm());
		modelMap.addAttribute("b_noti_bef_sett_acc_svc_prc_sum", detail.getPre());
		modelMap.addAttribute("loanBatchProTgtInfo", detail.getExtLoanDTO());
		modelMap.addAttribute("finGood", detail.getList());
		modelMap.addAttribute("cust", smpCustService.getCust(user.getUsername()));
		modelMap.addAttribute("finGoodSeqNo", finGoodSeqNo);
		modelMap.put("sodeone_home", SODEONE_HOME);
		
		if (device.isNormal())
			modelMap.addAttribute("DeviceType", "PC");
		else
			modelMap.addAttribute("DeviceType", "Other");
		
		return "sub/pre_calculate/detail";
	}
	
	@GetMapping("/sub/pre_calculate/detail/term")
	@ResponseBody
	public String detailPageTermAjax(@RequestParam long termsId, @AuthenticationPrincipal User user) {
		return smpTermsService.termsChk(user.getUsername(), svcId, termsId);
	}
	
	@GetMapping("/sub/pre_calculate/detail/term/aggr")
	@ResponseBody
	public boolean detailPageTermAggrAjax(@RequestParam long termsId, @AuthenticationPrincipal User user) {
		List<ExtAgreeTermsReqDTO> terms=new ArrayList<ExtAgreeTermsReqDTO>();
		ExtAgreeTermsReqDTO dto=new ExtAgreeTermsReqDTO();
		dto.setCust_id(user.getUsername());
		dto.setSvc_id(svcId);
		dto.setTerms_id(termsId);
		terms.add(dto);
		return smpJoinService.addTerms(terms);
	}
	
	@PostMapping("/sub/pre_calculate/detail/consel")
	@ResponseBody
	public boolean requestCounsel(@RequestParam String biz_nm, @RequestParam String ceph_no, @RequestParam String fin_goods_seq_no) {
		return smpFinGoodService.requestCounsel(biz_nm, ceph_no, fin_goods_seq_no);
	}
	
	@PostMapping("/sub/pre_calculate/detail/setFinGoodStat")
	@ResponseBody
	public boolean setFinGoodStat(@RequestParam String fin_goods_seq_no,@RequestParam String fin_goods_req_typ_cd, @AuthenticationPrincipal User user) {
		return smpFinGoodService.setFinGoodStat(fin_goods_seq_no, fin_goods_req_typ_cd, user.getUsername());
	}
	
}
