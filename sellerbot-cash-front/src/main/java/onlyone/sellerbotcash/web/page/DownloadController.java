package onlyone.sellerbotcash.web.page;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;

import onlyone.sellerbotcash.service.JumpService;
import onlyone.sellerbotcash.web.util.XmlParseUtil;

@RestController
public class DownloadController {
    @Autowired
    private JumpService jumpService;
    
    @Value("${repository.installer:http://192.168.111.250/repository/installer/}")
    String installerPath;

    @RequestMapping("/download/installer")
    public ModelAndView downloadInstaller(@AuthenticationPrincipal User user) throws IOException {
        String userName = user.getUsername();

		// version 확인
        String versionUrlStr = installerPath + "version.xml";
        URL url = new URL(versionUrlStr);
        URLConnection connection = url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestProperty("CONTENT-TYPE", "Application/xml");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));

        String inputLine;
        StringBuffer stringBuffer = new StringBuffer();

        while ((inputLine = bufferedReader.readLine()) != null) {
            stringBuffer.append(inputLine.trim());
        }
        
        bufferedReader.close();

        XmlParseUtil xmlParseUtil = new XmlParseUtil(stringBuffer.toString());
        Map<String, String> versionInfo = xmlParseUtil.getParseMap();
        
        if(Objects.isNull(versionInfo)) {
            throw new RuntimeException("Not found version file");
        }

        String versionStr = null;
        if(versionInfo.containsKey("version")) {
            versionStr = versionInfo.get("version").toString();
        }
        
        String installFileStr = null;
        if(versionInfo.containsKey("install-file")) {
            installFileStr = versionInfo.get("install-file").toString();
        }

        if(Objects.isNull(installFileStr) || installFileStr.equals("")) {
            throw new RuntimeException("Not found name of download file");
        }

        jumpService.addCustJumpDownHistory(userName, installFileStr, versionStr);

        String filePath = installerPath + installFileStr;

        return new ModelAndView("downloadView")
        .addObject("filePath", filePath)
        .addObject("fileName", installFileStr);
	}
    
    @RequestMapping("/smp/download/installer")
    public ModelAndView smpDownloadInstaller() throws IOException {
//        String userName = user.getUsername();

		// version 확인
        String versionUrlStr = installerPath + "version.xml";
        URL url = new URL(versionUrlStr);
        URLConnection connection = url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestProperty("CONTENT-TYPE", "Application/xml");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));

        String inputLine;
        StringBuffer stringBuffer = new StringBuffer();

        while ((inputLine = bufferedReader.readLine()) != null) {
            stringBuffer.append(inputLine.trim());
        }
        
        bufferedReader.close();

        XmlParseUtil xmlParseUtil = new XmlParseUtil(stringBuffer.toString());
        Map<String, String> versionInfo = xmlParseUtil.getParseMap();
        
        if(Objects.isNull(versionInfo)) {
            throw new RuntimeException("Not found version file");
        }

        //String versionStr = null;
        if(versionInfo.containsKey("version")) {
            /*versionStr = */versionInfo.get("version").toString();
        }
        
        String installFileStr = null;
        if(versionInfo.containsKey("install-file")) {
            installFileStr = versionInfo.get("install-file").toString();
        }

        if(Objects.isNull(installFileStr) || installFileStr.equals("")) {
            throw new RuntimeException("Not found name of download file");
        }

//        jumpService.addCustJumpDownHistory(userName, installFileStr, versionStr);

        String filePath = installerPath + installFileStr;

        return new ModelAndView("downloadView")
        .addObject("filePath", filePath)
        .addObject("fileName", installFileStr);
	}
    
    
}