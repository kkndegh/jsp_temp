package onlyone.sellerbotcash.web.page;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import lombok.extern.slf4j.Slf4j;
import onlyone.sellerbotcash.config.SellerbotConstant;
import onlyone.sellerbotcash.service.GoodsService;
import onlyone.sellerbotcash.service.MypageService;
import onlyone.sellerbotcash.service.PaymentService;
import onlyone.sellerbotcash.service.SbfUtils;
import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpCustService;
import onlyone.sellerbotcash.service.dto.ExtraData;
import onlyone.sellerbotcash.service.dto.ExtraData.Goods;
import onlyone.sellerbotcash.web.util.CustPayInfo;
import onlyone.sellerbotcash.web.util.NumberToKorUtil;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.persistent.domain.enums.EYesOrNo;
import onlyone.smp.service.dto.ExtCustDTO;
import onlyone.smp.service.dto.ExtGoodsDTO;
import onlyone.smp.service.dto.ExtJoinCustMallListDTO;
import onlyone.smp.service.dto.ExtJoinDTO;
import onlyone.smp.service.dto.ExtPayInicisReqDTO;
import onlyone.smp.service.dto.ExtPayInicisRespDTO;
import onlyone.smp.service.dto.ExtSelectGoodsDTO;
import onlyone.smp.service.dto.ExtUsingGoodsInfoDTO;

import com.inicis.std.util.ParseUtil;
import com.inicis.std.util.SignatureUtil;
import com.inicis.std.util.StdSignatureUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inicis.std.util.HttpUtil;

/**
 * 결제 관련 Controller
 */
@Slf4j
@Controller
public class PaymentRenewalController {

    @Value("${internal.redirect.base:}")
    private String redirectBase;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private SmpCustService smpCustService;
    @Autowired
    private SmpCustMallService smpCustMallService;
    @Autowired
    private MypageService mypageService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private CustPayInfo custPayInfo;

    /**
     * 이용요금 안내 -> 이용권 소개
     * 
     * @param modelMap
     * @param device
     * @param user
     * @param session
     * @return String
     */
    @GetMapping("/sub/payment/step1")
    public String getRenewalRegPay01(ModelMap modelMap, Device device, @AuthenticationPrincipal User user,
            HttpSession session, HttpServletRequest request) {
        // 고객 정보 조회
        String userName = user.getUsername();
        ExtCustDTO cust = smpCustService.getCust(userName);
        if (cust == null)
            throw new RuntimeException(user.getUsername() + "not found!");

        modelMap.addAttribute("freeYn", cust.getFree_trial_yn());
        // Cafe24 고객 여부
        EYesOrNo cafe24_usr_yn = EInfPath.CF24 == cust.getInf_site_cd() ? EYesOrNo.Y : EYesOrNo.N;
        modelMap.addAttribute("cafe24_usr_yn", cafe24_usr_yn.name());

        // 샵바이 고객 여부
        EYesOrNo shopby_usr_yn = EInfPath.SHBY == cust.getInf_site_cd() ? EYesOrNo.Y : EYesOrNo.N;
        modelMap.addAttribute("shopby_usr_yn", shopby_usr_yn.name());

        // 이용중인 서비스 조회
        ExtUsingGoodsInfoDTO usingGoods = mypageService.getPayInfoForUsingGoods(userName);
        modelMap.addAttribute("usingGoods", usingGoods);

        // 등록 몰 정보 - 등록 카운트
        ExtJoinCustMallListDTO extJoinCustMallListDTO = smpCustMallService.getJoinCustMall(user.getUsername());
        int cnt = extJoinCustMallListDTO.getExtReCustMallCntDTO().getAll_cnt()
                - extJoinCustMallListDTO.getExtReCustMallCntDTO().getDel_cnt();
        modelMap.addAttribute("mallCnt", cnt);

        // INF_SITE_CD를 상품조회 코드로 선언
        String infSiteCd = cust.getInf_site_cd() != null ? cust.getInf_site_cd().name() : "";
        String custSeqNo = String.valueOf(cust.getCust_seq_no());

        boolean promoEligible = smpCustService.getCouponIssuanceChk(custSeqNo);
        String mgmtCd = request.getParameter("mgmtCd");

        if (mgmtCd != null) {
            session.setAttribute("mgmtCd", mgmtCd);
        }

        String sessionMgmtCd = (String) session.getAttribute("mgmtCd");
        String promAlertYn = "N";
        // 팝업 노출
        if (!promoEligible && sessionMgmtCd != null) {
            promAlertYn = "Y";
        }
        modelMap.addAttribute("promoEligible", promoEligible);
        modelMap.addAttribute("promAlertYn", promAlertYn);
        List<ExtGoodsDTO> goodsList = goodsService.getGoodsList(infSiteCd, custSeqNo); // 파라미터에 ("제휴구분코드",
                                                                                       // "cust_seq_no") 값을 넣어 넘김

        // 상품 정보
        // 기존 로니봇, 파이봇은 리스트에 안뜨게 하려고 코드 분기처리
        // goodsList = goodsList.stream().filter(good -> {
        // if (good.getGOODS_TYP_CD().equals("RNB") ||
        // good.getGOODS_TYP_CD().equals("PYB")) {
        // return false;
        // } else {
        // return true;
        // }
        // }).collect(Collectors.toList());

        modelMap.addAttribute("goodsList", goodsList);

        // 누적 판매량 , 사용 유저 , 지원 판매몰
        ExtJoinDTO step_one_info = goodsService.getStepOneInfo();

        String salsePrc = step_one_info.getSumSalsePrc();
        String numberString = NumberToKorUtil.NumberToKor(salsePrc);

        modelMap.addAttribute("siteInfo", step_one_info);
        modelMap.addAttribute("numberString", numberString);

        if (device.isNormal())
            modelMap.addAttribute("DeviceType", "PC");
        else
            modelMap.addAttribute("DeviceType", "Other");

        return "sub/paymentRenewal/renewal_regPay_01";
    }

    @PostMapping("/sub/payment/step1")
    public String postRenewalRegPay01(ModelMap modelMap, HttpSession session, Long goodsSeqNo, Long goodsOptSeqNo) {
        return "redirect:" + redirectBase + "/sub/payment/step2?seq=" + goodsSeqNo + "&" + "opt=" + goodsOptSeqNo;
    }

    /**
     * 이용요금 안내 -> 결제하기
     *
     * @param request
     * @param modelMap
     * @param user
     * @param session
     * @param device
     * @return
     */
    @GetMapping("/sub/payment/step2")
    public String getRenewalRegPay02(HttpServletRequest request, ModelMap modelMap, @AuthenticationPrincipal User user,
            HttpSession session, Device device) {

        String userName = user.getUsername();
        ExtCustDTO cust = smpCustService.getCust(userName);
        if (cust == null) {
            throw new RuntimeException(user.getUsername() + "not found!");
        }

        // 등록 몰 정보 - 등록 카운트
        ExtJoinCustMallListDTO extJoinCustMallListDTO = smpCustMallService.getJoinCustMall(user.getUsername());
        int cnt = extJoinCustMallListDTO.getExtReCustMallCntDTO().getAll_cnt()
                - extJoinCustMallListDTO.getExtReCustMallCntDTO().getDel_cnt();

        // 제휴 별 상품을 구분하기 위해 파라미터 셋팅
        String infSiteCd = cust.getInf_site_cd() != null ? cust.getInf_site_cd().name() : "";
        String custSeqNo = String.valueOf(cust.getCust_seq_no());
        List<ExtGoodsDTO> goodsList = goodsService.getGoodsList(infSiteCd, custSeqNo); // 파라미터에 ("제휴구분코드",
                                                                                       // "cust_seq_no") 값을 넣어 넘김
        boolean promoEligible = smpCustService.getCouponIssuanceChk(custSeqNo);
        modelMap.addAttribute("promoEligible", promoEligible);
        // 상품 정보
        // 기존 로니봇, 파이봇은 리스트에 안뜨게 하려고 코드 분기처리
        goodsList = goodsList.stream().filter(good -> {
            if (good.getGOODS_TYP_CD().equals("RNB") || good.getGOODS_TYP_CD().equals("PYB")) {
                return false;
            } else {
                if (good.getSALE_MALL_REG_ID_PSB_CNT() == 0) {
                    return true;
                } else {
                    if (good.getSALE_MALL_REG_ID_PSB_CNT() < cnt) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }).collect(Collectors.toList());

        modelMap.addAttribute("goodsList", goodsList);

        ExtSelectGoodsDTO goodsHis = new ExtSelectGoodsDTO();
        goodsHis.setGoodsOptSeqNo(request.getParameter("opt"));
        goodsHis.setGoodsSeqNo(request.getParameter("seq"));
        modelMap.addAttribute("goodsHis", goodsHis);

        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        if (map != null) {
            modelMap.put("successYN", map.get("successYN"));
            modelMap.put("errMsg", map.get("errMsg"));
        }

        // Cafe24 고객 여부
        // EYesOrNo cafe24_usr_yn = EYesOrNo.N;
        // if (cust.getInf_site_cd() != null &&
        // cust.getInf_site_cd().equals(EInfPath.CF24)) {
        // cafe24_usr_yn = EYesOrNo.Y;
        // }

        modelMap.addAttribute("freeTrialYn", cust.getFree_trial_yn());
        // modelMap.addAttribute("cafe24_usr_yn", cafe24_usr_yn.name());

        modelMap.addAttribute("mid", SellerbotConstant.INICIS_MID);
        modelMap.addAttribute("mKey", SellerbotConstant.INICIS_MKEY);
        modelMap.addAttribute("buyername", cust.getCeo_nm());
        modelMap.addAttribute("buyertel", cust.getCeo_no());
        modelMap.addAttribute("buyeremail", cust.getCust_id());
        modelMap.addAttribute("siteDomain", session.getAttribute("siteDomain"));

        if (device.isNormal()) {
            modelMap.addAttribute("DeviceType", "PC");
        } else {
            modelMap.addAttribute("DeviceType", "Other");
        }
        return "sub/paymentRenewal/renewal_regPay_02";
    }

    /**
     * 결제 완료 화면
     *
     * @param request
     * @param modelMap
     * @return
     */
    @GetMapping("/sub/payment/step3")
    public String getRenewalRegPay03(HttpServletRequest request, ModelMap modelMap) {
        Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
        if (map != null) {
            modelMap.put("goodsTypCd", map.get("goodsTypCd"));
            modelMap.put("price", map.get("price"));
            modelMap.put("cardNum", map.get("cardNum"));
            modelMap.put("nextBillYMD", map.get("nextBillYMD"));
            modelMap.put("nextBillDate", map.get("nextBillDate"));
        }

        return "sub/paymentRenewal/renewal_regPay_03";
    }

    @GetMapping("/pub/payment/pay_close")
    public String paymentClose(ModelMap modelMap) {
        modelMap.addAttribute("mid", SellerbotConstant.INICIS_MID);
        return "sub/paymentRenewal/renewal_close";
    }

    /**
     * 이니시스 시그니처
     * 
     * @param mid
     * @param price
     * @return
     */
    // @PostMapping("/sub/payment/renewal_getSignature")
    // public @ResponseBody Map<String, String> getSignature(String mid, String
    // price) {
    // Map<String, String> map = new HashMap<String, String>();
    // String ts = SignatureUtil.getTimestamp();
    // String oid = mid + "_" + ts;

    // Map<String, String> signParam = new HashMap<String, String>();
    // signParam.put("oid", oid);
    // signParam.put("price", price);
    // signParam.put("timestamp", ts);

    // try {
    // String signature = SignatureUtil.makeSignature(signParam);
    // map.put("timestamp", ts);
    // map.put("oid", oid);
    // map.put("signature", signature);

    // String hash = mid + oid + ts + SellerbotConstant.INILITE_KEY;
    // hash = StdSignatureUtil.hash(hash, "SHA-256");
    // map.put("hash", hash);

    // } catch (Exception e) {
    // log.error("{}", e);
    // }

    // log.debug("map = {}", map);
    // return map;
    // }

    /**
     * 이니시스 시그니처
     * 
     * @param mid
     * @param price
     * @return
     */
    @PostMapping("/pub/payment/pay_getSignature")
    public @ResponseBody Map<String, String> getSignature2(String mid, String price) {
        Map<String, String> map = new HashMap<String, String>();
        String ts = SignatureUtil.getTimestamp();
        String oid = mid + "_" + ts;

        Map<String, String> signParam = new HashMap<String, String>();
        signParam.put("oid", oid);
        signParam.put("price", price);
        signParam.put("timestamp", ts);

        try {
            String signature = SignatureUtil.makeSignature(signParam);
            map.put("timestamp", ts);
            map.put("oid", oid);
            map.put("signature", signature);

            String hash = mid + oid + ts + SellerbotConstant.INILITE_KEY;
            hash = StdSignatureUtil.hash(hash, "SHA-256");
            map.put("hash", hash);

        } catch (Exception e) {
            log.error("{}", e);
        }

        log.debug("map = {}", map);
        return map;
    }

    /**
     * 결제 결과 수신 - PC
     * 
     * @param request
     * @param redirectAttr
     * @param session
     * @return
     */
    @PostMapping(value = "/sub/payment/pay_payresponse")
    public String paymentResponse(HttpServletRequest request, RedirectAttributes redirectAttr, HttpSession session) {
        boolean success = false;
        String errMsg = "";
        String mode = null;
        ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");
        Long goodsSeqNo = null;
        Long goodsOptSeqNo = null;
        try {
            if (cust == null)
                throw new Exception("Customer Not Found.");
            request.setCharacterEncoding("UTF-8");

            Map<String, String> paramMap = new Hashtable<String, String>();
            Enumeration<String> elems = request.getParameterNames();

            while (elems.hasMoreElements()) {
                String key = elems.nextElement();
                paramMap.put(key, request.getParameter(key));
            }
            log.info(
                    "(+)=========================================================\n" + "    paramMap : {} - {}\n"
                            + "(-)=========================================================\n",
                    cust.getCust_id(), paramMap);

            String selectMerchantData = paramMap.get("merchantData").toString(); // 가맹점 관리데이터 수신
            selectMerchantData = StringEscapeUtils.unescapeHtml(selectMerchantData).replace("&quot", "\"");
            ExtraData selectGoodsInfo = new ObjectMapper().readValue(selectMerchantData, ExtraData.class);
            List<Goods> selectGoodsListInfo = selectGoodsInfo.getGoodsList();

            goodsSeqNo = selectGoodsListInfo.get(0).getS();
            goodsOptSeqNo = selectGoodsListInfo.get(0).getO();

            if ("0000".equals(paramMap.get("resultCode"))) { // 인증 성공
                String mid = paramMap.get("mid");
                String timestamp = SignatureUtil.getTimestamp();
                String charset = SellerbotConstant.INICIS_CHARSET;
                String format = SellerbotConstant.INICIS_FORMAT;
                String authToken = paramMap.get("authToken");
                String authUrl = paramMap.get("authUrl");
                String netCancelUrl = paramMap.get("netCancelUrl");
                String merchantData = paramMap.get("merchantData").toString(); // 가맹점 관리데이터 수신
                merchantData = StringEscapeUtils.unescapeHtml(merchantData).replace("&quot", "\"");
                log.debug("merchantData = {}", merchantData);
                ExtraData extras = new ObjectMapper().readValue(merchantData, ExtraData.class);
                mode = extras.getMode();
                List<Goods> goodsList = extras.getGoodsList();

                Map<String, String> signParam = new HashMap<String, String>();
                signParam.put("authToken", authToken);
                signParam.put("timestamp", timestamp);

                String signature = SignatureUtil.makeSignature(signParam);
                Map<String, String> authMap = new Hashtable<String, String>();
                authMap.put("mid", mid); // 필수
                authMap.put("authToken", authToken); // 필수
                authMap.put("signature", signature); // 필수
                authMap.put("timestamp", timestamp); // 필수
                authMap.put("charset", charset); // default=UTF-8
                authMap.put("format", format); // default=XML

                HttpUtil httpUtil = new HttpUtil();
                try { // 승인 요청
                    String authResultString = httpUtil.processHTTP(authMap, authUrl);
                    String replaced = authResultString.replace(",", "&").replace(":", "=").replace("\"", "")
                            .replace(" ", "").replace("\n", "").replace("}", "").replace("{", "");
                    Map<String, String> resultMap = ParseUtil.parseStringToMap(replaced);

                    log.info(
                            "(+)=========================================================\n"
                                    + "    resultMap : {} - {}\n"
                                    + "(-)=========================================================\n",
                            cust.getCust_id(), resultMap);

                    Map<String, String> secureMap = new HashMap<String, String>();
                    secureMap.put("mid", mid);
                    secureMap.put("tstamp", timestamp);
                    secureMap.put("MOID", resultMap.get("MOID"));
                    secureMap.put("TotPrice", resultMap.get("TotPrice"));

                    String secureSignature = SignatureUtil.makeSignatureAuth(secureMap);
                    String resultCode = resultMap.get("resultCode");
                    String authSignature = resultMap.get("authSignature");

                    if ("0000".equals(resultCode)) { // 승인 성공
                        if (secureSignature.equals(authSignature)) {
                            String tid = resultMap.get("tid");
                            ExtPayInicisReqDTO c = new ExtPayInicisReqDTO();

                            c.setResultCode(resultMap.get("resultCode"));
                            c.setResultMsg(resultMap.get("resultMsg"));
                            c.setPayMethod(resultMap.get("payMethod"));
                            c.setGoodNm(resultMap.get("goodName"));
                            c.setBuyerName(resultMap.get("buyerName"));
                            c.setBuyerTel(resultMap.get("buyerTel"));
                            c.setBuyerEmail(resultMap.get("buyerEmail"));
                            c.setApplTime(resultMap.get("applTime"));
                            c.setCurrency(resultMap.get("currency"));
                            c.setPayDevice(resultMap.get("payDevice"));
                            c.setApplDate(resultMap.get("applDate"));
                            c.setMoid(resultMap.get("MOID"));
                            c.setTid(tid);
                            c.setCardBillKey(resultMap.get("CARD_BillKey"));
                            c.setCardBankCode(resultMap.get("CARD_BankCode"));
                            c.setCardQuota(resultMap.get("CARD_Quota"));
                            c.setCardCode(resultMap.get("CARD_Code"));
                            c.setCardNum(resultMap.get("CARD_Num"));
                            c.setCardInterest(resultMap.get("CARD_Interest"));
                            c.setCustSeqNo(cust.getCust_seq_no());
                            c.setEventNo(extras.getEventNo());

                            redirectAttr.addFlashAttribute("cardNum", resultMap.get("CARD_Num"));

                            List<ExtPayInicisReqDTO.Goods> _l = new ArrayList<ExtPayInicisReqDTO.Goods>();
                            if ("register".equals(mode)) {
                                for (Goods g : goodsList) {
                                    ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                                    goods.setGoodsOptSeqNo(g.getO());
                                    goods.setUsePoi(0L);
                                    goods.setGoodNm(URLDecoder.decode(g.getN(), "UTF-8"));
                                    goods.setPrice(g.getP());
                                    // 에러 때문에 임시커밋
                                    // goods.setPeriod(g.getD());
                                    _l.add(goods);
                                }
                                c.setGoodsList(_l);
                                ExtPayInicisRespDTO p = paymentService.postPaymentByInicis(c);
                                success = true;
                                redirectAttr.addFlashAttribute("freeYN", p.getFreeYN());
                                redirectAttr.addFlashAttribute("products", p.getGoodsList());
                            } else if ("change".equals(mode)) {
                                for (Goods g : goodsList) {
                                    ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                                    goods.setGoodsReqSeqNo(g.getR());
                                    goods.setGoodsSeqNo(g.getS());
                                    _l.add(goods);
                                }
                                c.setGoodsList(_l);
                                paymentService.postChangeCard(c);
                                success = true;
                            }
                            //발행 쿠폰 상태코드 해지로 변경
                            paymentService.postCouponStateExpire(c);
                        } else {
                            throw new Exception("데이터 위변조 체크 실패");
                        }
                    } else {
                        errMsg = resultMap.get("resultMsg");
                    }

                } catch (Exception ex) {
                    errMsg = SbfUtils.getJsonValue(ex.getMessage(), "reason");
                    // log.info("===> {}", errMsg);
                    String result = httpUtil.processHTTP(authMap, netCancelUrl);
                    log.info(
                            "(+)=========================================================\n" + "    result : {} - {}\n"
                                    + "(-)=========================================================\n",
                            cust.getCust_id(), result);
                }
            } else {
                log.info("{} - Authentication Failed", cust.getCust_id());
                errMsg = paramMap.get("resultMsg");
            }
        } catch (Exception e) {
            errMsg = SbfUtils.getJsonValue(e.getMessage(), "reason");
        }

        redirectAttr.addFlashAttribute("successYN", success ? "Y" : "N");
        if (!success) {
            redirectAttr.addFlashAttribute("errMsg", errMsg);

            return "register".equals(mode)
                    ? "redirect:" + redirectBase + "/sub/payment/step2?seq=" + goodsSeqNo + "&" + "opt=" + goodsOptSeqNo
                    : "redirect:" + redirectBase + "/sub/my/paidInfo";
        }
        custPayInfo.updateSessionInfoAboutPay(session, cust.getCust_id());
        return "register".equals(mode) ? "redirect:" + redirectBase + "/sub/payment/step3"
                : "redirect:" + redirectBase + "/sub/my/paidInfo";
    }

    /**
     * 결제 결과 수신 - MOBILE
     * 
     * @param request
     * @param redirectAttr
     * @param session
     * @return
     */
    @PostMapping(value = "/sub/payment/pay_mobilepayresponse")
    public String mobilePaymentResponse(HttpServletRequest request, RedirectAttributes redirectAttr,
            HttpSession session) {
        boolean success = false;
        String errMsg = "";
        String mode = "register";
        ExtCustDTO cust = (ExtCustDTO) session.getAttribute("cust");
        Long goodsSeqNo = null;
        Long goodsOptSeqNo = null;

        try {
            if (cust == null)
                throw new Exception("Customer Not Found.");
            request.setCharacterEncoding("UTF-8");

            Map<String, String> resultMap = new Hashtable<String, String>();
            Enumeration<String> elems = request.getParameterNames();

            while (elems.hasMoreElements()) {
                String key = elems.nextElement();
                resultMap.put(key, request.getParameter(key));
            }

            log.info(
                    "(+)=========================================================\n" + "    resultMap : {} - {}\n"
                            + "(-)=========================================================\n",
                    cust.getCust_id(), resultMap);

            String p_noti = resultMap.get("p_noti").toString();
            p_noti = StringEscapeUtils.unescapeHtml(p_noti).replace("&quot", "\"");
            ExtraData extras = new ObjectMapper().readValue(p_noti, ExtraData.class);
            mode = extras.getMode();
            List<Goods> goodsList = extras.getGoodsList();

            goodsSeqNo = goodsList.get(0).getS();
            goodsOptSeqNo = goodsList.get(0).getO();

            String resultCode = resultMap.get("resultcode");
            if ("00".equals(resultCode)) {
                String tid = resultMap.get("tid");
                ExtPayInicisReqDTO c = new ExtPayInicisReqDTO();

                c.setResultCode(resultCode + "00");
                c.setResultMsg(resultMap.get("resultmsg"));
                c.setMoid(resultMap.get("orderid"));
                c.setBuyerName(extras.getBuyerName());
                c.setBuyerTel(extras.getBuyerTel());
                c.setBuyerEmail(extras.getBuyerEmail());
                c.setApplTime(resultMap.get("pgauthtime"));
                c.setApplDate(resultMap.get("pgauthdate"));
                c.setTid(tid);
                c.setCardBillKey(resultMap.get("billkey"));
                c.setCardCode(resultMap.get("cardcd"));
                c.setCardNum(resultMap.get("cardno"));
                c.setCardQuota("0");
                c.setCardInterest("0");
                c.setPayDevice("MOBILE");
                c.setCustSeqNo(cust.getCust_seq_no());

                redirectAttr.addFlashAttribute("cardNum", resultMap.get("cardno"));

                List<ExtPayInicisReqDTO.Goods> _l = new ArrayList<ExtPayInicisReqDTO.Goods>();
                if ("register".equals(mode)) {
                    for (Goods g : goodsList) {
                        ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                        goods.setGoodsOptSeqNo(g.getO());
                        goods.setUsePoi(0L);
                        goods.setGoodNm(URLDecoder.decode(g.getN(), "UTF-8"));
                        goods.setPrice(g.getP());
                        // 에러 때문에 임시커밋
                        // goods.setPeriod(g.getD());
                        _l.add(goods);
                    }
                    c.setGoodsList(_l);
                    ExtPayInicisRespDTO p = paymentService.postPaymentByInicis(c);
                    success = true;
                    redirectAttr.addFlashAttribute("freeYN", p.getFreeYN());
                    redirectAttr.addFlashAttribute("products", p.getGoodsList());
                } else if ("change".equals(mode)) {
                    for (Goods g : goodsList) {
                        ExtPayInicisReqDTO.Goods goods = new ExtPayInicisReqDTO.Goods();
                        goods.setGoodsReqSeqNo(g.getR());
                        goods.setGoodsSeqNo(g.getS());
                        _l.add(goods);
                    }
                    c.setGoodsList(_l);
                    paymentService.postChangeCard(c);
                    success = true;
                }

                //발행 쿠폰 상태코드 해지로 변경
                paymentService.postCouponStateExpire(c);
            } else {
                errMsg = resultMap.get("resultMsg");
            }
        } catch (Exception e) {
            errMsg = e.getMessage();
        }

        redirectAttr.addFlashAttribute("successYN", success ? "Y" : "N");
        if (!success) {
            redirectAttr.addFlashAttribute("errMsg", errMsg);
            return mode.equals("register")
                    ? "redirect:" + redirectBase + "/sub/payment/step2?seq=" + goodsSeqNo + "&" + "opt=" + goodsOptSeqNo
                    : "redirect:" + redirectBase + "/sub/my/paidInfo";
        }
        custPayInfo.updateSessionInfoAboutPay(session, cust.getCust_id());
        return mode.equals("register") ? "redirect:" + redirectBase + "/sub/payment/step3"
                : "redirect:" + redirectBase + "/sub/my/paidInfo";
    }

    @PostMapping(value = "/sub/payment/pay_promotion")
    @ResponseBody
    public ResponseEntity<String> paymentPromotion(String goodsTyp, String sttDt, String endDt, String promotionCd,
            HttpSession session, @AuthenticationPrincipal User user) throws JsonProcessingException {
        String userName = user.getUsername();
        ResponseEntity<String> result = paymentService.paymentPromotion(userName, goodsTyp, sttDt, endDt, promotionCd);
        if (result.getStatusCode().equals(HttpStatus.OK)) {
            session.removeAttribute("custPaidInfo"); // 상품 신청 세션 정보 삭제
            return new ResponseEntity<String>(HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }
}
