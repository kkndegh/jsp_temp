package onlyone.sellerbotcash.web.page;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import onlyone.sellerbotcash.service.SmpCommService;
import onlyone.sellerbotcash.service.SmpPeersaleService;
import onlyone.smp.service.dto.ExtCateCustReqDTO;
import onlyone.smp.service.dto.ExtCateCustResDTO;
import onlyone.smp.service.dto.ExtCateSalesRankAreaReqDTO;
import onlyone.smp.service.dto.ExtCateSalesRankAreaResDTO;
import onlyone.smp.service.dto.ExtCateSalesRankReqDTO;
import onlyone.smp.service.dto.ExtCateSalesRankResDTO;
import onlyone.smp.service.dto.ExtCustCateRankResDTO;
import onlyone.smp.service.dto.ExtTrGoodsCateDTO;
import onlyone.sellerbotcash.web.util.DateUtil;


/**
 * 동종업계 매출 추이
 * @author bak
 *
 */
@Controller
public class PeersaleController {

	@Autowired
	private SmpCommService smpCommService;
	
	@Autowired
	private SmpPeersaleService smpPeersaleService;
	

	/**
	 * 동종업계 매출 추이 > 나의 매출
	 * @param modelMap
	 * @param user
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler
	@GetMapping("/sub/peersale/state")
	public String formState(ModelMap modelMap,  @AuthenticationPrincipal User user) {
		List<Map> result = new ArrayList<Map>();
		
		// 기준 날짜
		LocalDateTime time = LocalDateTime.now();
		String lastMonth = DateUtil.getDateTimeMonthString(time.minusMonths(1)); // 저번달
				
		// 4.10.1 월별 카테고리별 매출 순위 목록 요청
		ExtCateCustReqDTO extCateCustReqDTO = new ExtCateCustReqDTO();
		extCateCustReqDTO.setCust_id(user.getUsername());
		extCateCustReqDTO.setYear_month(lastMonth);
		List<ExtCateCustResDTO> extCateCustResList = smpPeersaleService.getCateCustDateList(extCateCustReqDTO);
		
		// 4.10.2 월별 카테고리별 매출 순위 목록 파라미터  요청 
		ExtCateSalesRankReqDTO extCateSalesRankReqDTO = new ExtCateSalesRankReqDTO();
		extCateSalesRankReqDTO.setCust_id(user.getUsername());
		extCateSalesRankReqDTO.setSales_dt(lastMonth);
		
		ObjectMapper oMapper = new ObjectMapper();
		extCateSalesRankReqDTO.setTop_count(10);
		
		for(ExtCateCustResDTO resDto : extCateCustResList) {
			List<Long> goodsCateSeqNoList = new ArrayList<Long>();
			goodsCateSeqNoList.add(resDto.getGoods_cate_seq_no().longValue());
			extCateSalesRankReqDTO.setGoods_cate_seq_no(goodsCateSeqNoList);
			Map<String, Object> map = oMapper.convertValue(resDto, Map.class);
			
			// 카테고리 이름 취득
			map.put("cate_nm", resDto.getCust_nm());
			
			// 카테고리별 순위 & 범위 정보 취득
			map.put("rankList", smpPeersaleService.getCateSalesRankList(extCateSalesRankReqDTO));
			
			result.add(map);
		}
		
		modelMap.addAttribute("lastMonth", lastMonth); // 검색기준 저번달
		modelMap.addAttribute("categoryList", result); // 사용자의 카테고리 정보 조회
		
		return "sub/peersale/state";
	}
	
	
	/**
	 * 동종업계 매출 추이 > 매출 랭킹 변동 추이
	 * @param modelMap
	 * @param user
	 * @return
	 * @throws JsonProcessingException 
	 */
	@ExceptionHandler
	@GetMapping("/sub/peersale/state_change")
	public String formStateChange(ModelMap modelMap,  @AuthenticationPrincipal User user) throws JsonProcessingException {
		
		LocalDateTime time = LocalDateTime.now();
		String lastYear = DateUtil.getDateTimeMonthString(time.minusMonths(12)); // 12개월전
		String lastMonth = DateUtil.getDateTimeMonthString(time.minusMonths(1)); // 저번달
		
		ExtCateSalesRankAreaReqDTO dto = new ExtCateSalesRankAreaReqDTO();
		dto.setSta_dt(lastYear);
		dto.setEnd_dt(lastMonth);
		
		
		List<String> monthList = DateUtil.getDateMonthList(time.minusMonths(12), 11, DateUtil.FORMAT_MONTH);
		List<List<ExtCustCateRankResDTO>> list = smpPeersaleService.getParseMyRankMonthList(user.getUsername(), dto);
		
		modelMap.put("monthList", monthList);
		modelMap.put("myRankList", list);
		modelMap.put("jsonMyRankList", new ObjectMapper().writeValueAsString(list));
		
		return "sub/peersale/state_change";
	}
	
	
	
	
	/**
	 * 동종업계 매출 추이 > 분야별 랭킹 Top10
	 * @param modelMap
	 * @param user
	 * @return
	 */
	@ExceptionHandler
	@GetMapping("/sub/peersale/rank_top")
	public String formRankTop(ModelMap modelMap,  @AuthenticationPrincipal User user) {
		
		// 공통 전체 카테고리 목록
		List<ExtTrGoodsCateDTO> CommGoodsCategoryList = smpCommService.getCategoryList();
		
		// 기준 날짜
		LocalDateTime time = LocalDateTime.now();
		String lastMonth = DateUtil.getDateTimeMonthString(time.minusMonths(1)); // 저번달
		// 4.10.2 월별 카테고리별 매출 순위 목록 요청 
		ExtCateSalesRankReqDTO extCateSalesRankReqDTO = new ExtCateSalesRankReqDTO();
		extCateSalesRankReqDTO.setCust_id(user.getUsername());
		extCateSalesRankReqDTO.setSales_dt(lastMonth);
		extCateSalesRankReqDTO.setTop_count(10);
		List<ExtCateSalesRankResDTO> rankList = smpPeersaleService.getCateSalesRankList(extCateSalesRankReqDTO);
		
		modelMap.addAttribute("lastMonth", lastMonth); // 검색기준 저번달
		modelMap.addAttribute("categoryList", CommGoodsCategoryList);
		modelMap.addAttribute("rankList", rankList);
		
		return "sub/peersale/rank_top";
	}
	
	
	/**
	 * 4.10.2 월별 카테고리별 매출 순위 목록 요청
	 * @param user
	 * @param extCateSalesRankReqDTO
	 * @return
	 */
	@GetMapping(value = "/sub/peersale/category/rank", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public List<ExtCateSalesRankResDTO> getCategoryRankList(@AuthenticationPrincipal User user, @ModelAttribute ExtCateSalesRankReqDTO extCateSalesRankReqDTO) {
		extCateSalesRankReqDTO.setCust_id(user.getUsername());
		return smpPeersaleService.getCateSalesRankList(extCateSalesRankReqDTO);
	}
	
	/**
	 * 4.10.4 월별 카테고리 매출 분포 목록 요청
	 * @param user
	 * @param extCateSalesRankAreaReqDTO
	 * @return
	 */
	@GetMapping(value = "/sub/peersale/category/area", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public ExtCateSalesRankAreaResDTO getCategoryAreaInfo(@AuthenticationPrincipal User user, @ModelAttribute ExtCateSalesRankAreaReqDTO extCateSalesRankAreaReqDTO) {
		return smpPeersaleService.getCategoryAreaInfo(extCateSalesRankAreaReqDTO);
	}
}
