package onlyone.sellerbotcash.web.page;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import onlyone.sellerbotcash.service.SmpCustMallService;
import onlyone.sellerbotcash.service.SmpSalesService;
import onlyone.sellerbotcash.service.SmpSettAccService;
import onlyone.smp.service.dto.ExtComSalesMallReqDTO;
import onlyone.smp.service.dto.ExtCommMallDTO;
import onlyone.smp.service.dto.ExtSalesMonthMarketDTO;
import onlyone.smp.service.dto.ExtSettAccDetailInfoDTO;
import onlyone.smp.service.dto.ExtSettAccStatusDTO;
import onlyone.smp.service.dto.ExtSettAccSummaryLastDTO;
import onlyone.smp.service.dto.ExtSettAccSummaryPeriodDTO;
import onlyone.sellerbotcash.web.util.DateUtil;

/**
 * 과거 정산금 통계 Controller
 * @author YunaJang
 */
@Controller
public class PastController {

	@Autowired
	SmpSettAccService smpSettAccService;

	@Autowired
	SmpCustMallService smpCustMallService;

	@Autowired
	private SmpSalesService smpSalesService; 

	final Integer unit = 1000;

	/**
	 * 과거 정산금 통계 -> 정산금 현황
	 * @param modelMap
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping("/sub/past/past")
	public String getPastView(ModelMap modelMap, @AuthenticationPrincipal User user)
			throws JsonProcessingException {
		String userName = user.getUsername();

		// 데이터 조회기간 및 각 포맷에 맞는 날짜 정보 설정
		getDateInfo(modelMap);
		String startDate = modelMap.get("startDate").toString();
		String endDate = modelMap.get("endDate").toString();

		// 정산금 현황 조회
		ExtSettAccSummaryLastDTO settAccSummaryLast = smpSettAccService.getSummaryLast(userName);
		
		// 몰별 정산금 현황 조회
		// 조회 기간(전월기준) : 전년 동월 ~ 전월
		ExtSettAccSummaryPeriodDTO settAccSummaryPeriod = smpSettAccService.getSummaryPeriod(userName, startDate, endDate);

		// 쇼핑몰 목록 조회
		List<ExtCommMallDTO> custMallList = smpCustMallService.getCustMallList(userName);
		
		// 월별 정산금 현황 조회
		ExtSettAccStatusDTO monthInfo = smpSettAccService.getMonthInfo(userName, startDate, endDate, null, unit);
		 
		ObjectMapper mapper = new ObjectMapper();
		modelMap.addAttribute("settAccSummaryLast", settAccSummaryLast);
		modelMap.addAttribute("settAccSummaryPeriod", mapper.writeValueAsString(settAccSummaryPeriod));
		modelMap.addAttribute("custMallList", custMallList);
		modelMap.addAttribute("monthInfo", mapper.writeValueAsString(monthInfo));
		modelMap.addAttribute("monthList", mapper.writeValueAsString(monthInfo.getSett_acc_month()));
 
		return "sub/past/past";
	}
	
	/**
	 * 과거 정산금 통계 -> 매출/정산 비교
	 * @param modelMap
	 * @param user
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping("/sub/past/compare")
	public String getPastCompareView(ModelMap modelMap, @AuthenticationPrincipal User user)
			throws JsonProcessingException {
		String userName = user.getUsername();

		// 데이터 조회기간 및 각 포맷에 맞는 날짜 정보 설정
		getDateInfo(modelMap);
		String startDate = modelMap.get("startDate").toString();
		String endDate = modelMap.get("endDate").toString();

		// 정산금 현황 조회
		ExtSettAccSummaryLastDTO settAccSummaryLast = smpSettAccService.getSummaryLast(userName);
		
		// 몰별 정산금 현황 조회
		// 조회 기간(전월기준) : 전년 동월 ~ 전월
		ExtSettAccSummaryPeriodDTO settAccSummaryPeriod = smpSettAccService.getSummaryPeriod(userName, startDate, endDate);

		// 쇼핑몰 목록 조회
		List<ExtCommMallDTO> custMallList = smpCustMallService.getCustMallList(userName);
		
		// 월별 정산금 현황 조회
		ExtSettAccStatusDTO monthInfo = smpSettAccService.getMonthInfo(userName, startDate, endDate, null, unit);

		ExtComSalesMallReqDTO param = new ExtComSalesMallReqDTO();
		param.setSales_find_stt_dt(startDate);
		param.setSales_find_end_dt(endDate);

		// 월별 매출 내역(테이블용)
		ExtSalesMonthMarketDTO extSalesMonthMarketDTO = smpSalesService.getMonthSalesMarketInfo(userName, param);
		 
		ObjectMapper mapper = new ObjectMapper();
		modelMap.addAttribute("settAccSummaryLast", settAccSummaryLast);
		modelMap.addAttribute("settAccSummaryPeriod", mapper.writeValueAsString(settAccSummaryPeriod));
		modelMap.addAttribute("custMallList", custMallList);
		modelMap.addAttribute("monthInfo", monthInfo);
		modelMap.addAttribute("monthList", mapper.writeValueAsString(monthInfo.getSett_acc_month()));
		modelMap.addAttribute("salesInfo", extSalesMonthMarketDTO);
		modelMap.addAttribute("monthMarket", mapper.writeValueAsString(extSalesMonthMarketDTO.getSales_month()));
 
		return "sub/past/compare";
	}

	/**
	 * 기간별 정산금 현황 조회
	 * @param sales_find_stt_dt
	 * @param sales_find_end_dt
	 * @param user
	 * @return
	 */
	@GetMapping("/sub/past/pastSummaryPeriod")
	@ResponseBody
	public ExtSettAccSummaryPeriodDTO getSummaryPeriod(@ModelAttribute("sales_find_stt_dt") String sales_find_stt_dt, @ModelAttribute("sales_find_end_dt") String sales_find_end_dt, @AuthenticationPrincipal User user) {
		ExtSettAccSummaryPeriodDTO settAccSummaryPeriod = smpSettAccService.getSummaryPeriod(user.getUsername(), sales_find_stt_dt, sales_find_end_dt);
		return settAccSummaryPeriod;
	}

	/**
	 * 월별 정산금 현황 조회
	 * @param sales_find_stt_dt
	 * @param sales_find_end_dt
	 * @param mall_cd
	 * @param user
	 * @return
	 */
	@GetMapping("/sub/past/pastMonthInfo")
	@ResponseBody
	public ExtSettAccStatusDTO getSettAccMonthInfo(@ModelAttribute("sales_find_stt_dt") String sales_find_stt_dt, @ModelAttribute("sales_find_end_dt") String sales_find_end_dt, 
			@ModelAttribute("mall_cd") String mall_cd, @AuthenticationPrincipal User user) {
		ExtSettAccStatusDTO monthInfo = smpSettAccService.getMonthInfo(user.getUsername(), sales_find_stt_dt, sales_find_end_dt, mall_cd, unit);
		return monthInfo;
	}

	/**
	 * 월별 정산금 상세 현황 조회
	 * @param sales_find_stt_dt
	 * @param sales_find_end_dt
	 * @param mall_cd
	 * @param user
	 * @return
	 */
	@GetMapping("/sub/past/pastMonthDetailInfo")
	@ResponseBody
	public ExtSettAccDetailInfoDTO getSettAccMonthDetailInfo(@ModelAttribute("sales_find_stt_dt") String sales_find_stt_dt, @ModelAttribute("sales_find_end_dt") String sales_find_end_dt, 
			@ModelAttribute("mall_cd") String mall_cd, @AuthenticationPrincipal User user) {
		ExtSettAccDetailInfoDTO monthInfo = smpSettAccService.getMonthDetailInfo(user.getUsername(), sales_find_stt_dt, sales_find_end_dt, mall_cd);
		return monthInfo;
	}

	/**
	 * 데이터 조회기간 및 각 포맷에 맞는 날짜 정보 설정
	 * @param modelMap
	 */
	private void getDateInfo(ModelMap modelMap) {
		// 지난 달 텍스트
		// ex) 19년 9월
		LocalDate prevMonthDate = LocalDate.now().minusMonths(1);
		StringBuffer prevMonthStr = new StringBuffer();
		prevMonthStr.append(prevMonthDate.getYear() % 100).append("년 ").append(prevMonthDate.getMonthValue()).append("월");
		modelMap.addAttribute("prevMonth", prevMonthStr.toString());
		// ex) 2019-09
		prevMonthStr = null;
		prevMonthStr = new StringBuffer();
		prevMonthStr.append(prevMonthDate.getYear()).append("-");

		if(prevMonthDate.getMonthValue() < 10)
			prevMonthStr.append("0");

		prevMonthStr.append(prevMonthDate.getMonthValue());
		modelMap.addAttribute("prevMonth2", prevMonthStr.toString());


		// 조회기간 기준 텍스트
		// ex) 2018-04~2019-03
		LocalDate prevYearSameMonthDate = LocalDate.now().minusYears(1);
		StringBuffer searchDateRangeStr = new StringBuffer();
		searchDateRangeStr.append(prevYearSameMonthDate.getYear()).append("-");
		
		if(prevYearSameMonthDate.getMonthValue() < 10)
			searchDateRangeStr.append("0");
		
		searchDateRangeStr.append(prevYearSameMonthDate.getMonthValue());
		searchDateRangeStr.append("~");
		searchDateRangeStr.append(prevMonthDate.getYear()).append("-");
		
		if(prevMonthDate.getMonthValue() < 10)
			searchDateRangeStr.append("0");

		searchDateRangeStr.append(prevMonthDate.getMonthValue());
		modelMap.addAttribute("searchDateRangeStr", searchDateRangeStr.toString());


		// 조회 기간
		String startDate = DateUtil.getYearAndMonth(prevYearSameMonthDate);
		String endDate = DateUtil.getYearAndMonth(prevMonthDate);
		modelMap.addAttribute("startDate", startDate);
		modelMap.addAttribute("endDate", endDate);
	}
}
