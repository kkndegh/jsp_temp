package onlyone.sellerbotcash.web.error;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;


/**
 * Exception에 따른 전달 방식 변경
 * @author oletree
 *
 */
@ControllerAdvice
public class ExceptionTranslator {


	@ExceptionHandler
    public ResponseEntity<String> handleExternalRequestFailure(RestApiException ex, NativeWebRequest request, HttpServletResponse res){
    	return new ResponseEntity<String>(ex.getMessage(), ex.getStatusCode());
    }
}
