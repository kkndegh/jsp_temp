/**
 * @author kenny
 */
package onlyone.sellerbotcash.web.error;

import lombok.Getter;

@Getter
public class RestfulRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private int code;

    public RestfulRuntimeException(int code, String message) {
        super(message);
        this.code = code;
    }
}