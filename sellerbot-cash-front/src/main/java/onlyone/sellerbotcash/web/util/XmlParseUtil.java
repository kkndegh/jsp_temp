package onlyone.sellerbotcash.web.util;

import java.io.StringReader;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XmlParseUtil {

	private String XML;
	private Document DOCUMENT_XML;
	
	public XmlParseUtil(String xmlStr) {
		XML = xmlStr;
		convertStringToDocument();
	}

	private void convertStringToDocument() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			DOCUMENT_XML = builder.parse(new InputSource(new StringReader(XML)));
		} catch (Exception e) {
			log.error("" + e);
		}
	}
	
	/**
	 * 인터 파크 간편 가입 전용 파싱
	 * @return
	 */
	public HashMap<String, Object> getMapValue() {
		HashMap<String, Object> values = new HashMap<String, Object>();
		Node root = DOCUMENT_XML.getFirstChild();
		
		// head
		NodeList childs = root.getChildNodes().item(0).getChildNodes();
		
		Node child;
		for (int i = 0; i < childs.getLength(); i++) {
			child = childs.item(i);
			values.put(child.getNodeName(), child.getTextContent());
		}
		return values;

	}

	public HashMap<String, String> getParseMap() {
		HashMap<String, String> values = new HashMap<String, String>();
		Node root = DOCUMENT_XML.getFirstChild();
		Node child = null;
		
		for(int i = 0; i < root.getChildNodes().getLength(); i++) {
			NodeList childs = root.getChildNodes().item(i).getChildNodes();
			
			if(childs.getLength() == 1) {
				child = root.getChildNodes().item(i);
				values.put(child.getNodeName(), child.getTextContent());
			}
			else {
				for(int j = 0; j < childs.getLength(); j++) {
					child = childs.item(j);
					values.put(child.getNodeName(), child.getTextContent());
				}
			}
		}

		return values;
	}
	
	public HashMap<String, Object> getTargetValue(String targetName) {
		HashMap<String, Object> values = new HashMap<String, Object>();
		// head
		NodeList childs = DOCUMENT_XML.getElementsByTagName(targetName);
		Node child;
		for (int i = 0; i < childs.getLength(); i++) {
			child = childs.item(i);
			values.put(child.getNodeName(), child.getTextContent());
		}
		return values;

	}
	
	public void clear() {
		XML = null;
		DOCUMENT_XML = null;
	}
	
}
