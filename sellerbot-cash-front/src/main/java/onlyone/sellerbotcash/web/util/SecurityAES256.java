package onlyone.sellerbotcash.web.util;

import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

/**
 * S-MAP 통신 복호화 파리미터
 * @author bak
 *
 */
public class SecurityAES256 {
	private static volatile SecurityAES256 INSTANCE;

	final static String SECRETKEY = "0x0AB6B56890DBDE99D70F69EA2D4EA7"; // 32byte
	static String IV = "0x0AB6B56890DBDE";
	final static String MODE_PADDING = "AES/CBC/PKCS5Padding";

	public static SecurityAES256 getInstance() {
		if (INSTANCE == null) {
			synchronized (SecurityAES256.class) {
				if (INSTANCE == null)
					INSTANCE = new SecurityAES256();
			}
		}
		return INSTANCE;
	}

	// 암호화
	public static String AES_Encode(String str)
			throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		byte[] keyData = SECRETKEY.getBytes();

		SecretKey secureKey = new SecretKeySpec(keyData, "AES");

		Cipher c = Cipher.getInstance(MODE_PADDING);
		c.init(Cipher.ENCRYPT_MODE, secureKey, new IvParameterSpec(IV.getBytes(Charset.forName("UTF-8"))));

		byte[] encrypted = c.doFinal(str.getBytes("EUC-KR"));
		return Hex.encodeHexString(encrypted);
	}

	// 복호화
	public static String AES_Decode(String str)
			throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, DecoderException {
		// log.error("******** {}" + Hex.class.getProtectionDomain().getCodeSource().getLocation());
		// log.error("***** str : {}", str);
		byte[] keyData = SECRETKEY.getBytes();
		SecretKey secureKey = new SecretKeySpec(keyData, "AES");
		Cipher c = Cipher.getInstance(MODE_PADDING);
				
		byte[] bytesIv = IV.getBytes(Charset.forName("EUC-KR"));
				
		c.init(Cipher.DECRYPT_MODE, secureKey, new IvParameterSpec(bytesIv));
		byte[] decodedHex;
		if(str.indexOf("0x") > -1) {
			decodedHex = Hex.decodeHex(str.substring(2));	
		} else {
			decodedHex = Hex.decodeHex(str);
		}
		
		return new String(c.doFinal(decodedHex), "EUC-KR");
	}
}
