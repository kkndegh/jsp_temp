package onlyone.sellerbotcash.web.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestApiUtil {
	public String executePost(String URLAddress, Map<String, String> headerParams, List<NameValuePair> postParams)
			throws MalformedURLException {

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost(URLAddress);

		if (headerParams != null) {
			for (String header : headerParams.keySet()) {
				httpPost.addHeader(header, headerParams.get(header));
			}
		}

		if (postParams != null)
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(postParams, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				log.error("" + e);
			}

		ResponseHandler<String> handler = new ResponseHandler<String>() {
			@Override
			public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				log.info("Status: " + status);
				HttpEntity entity = response.getEntity();
				return entity != null ? EntityUtils.toString(entity) : null;
			}
		};

		String responseBody = null;

		try {
			responseBody = client.execute(httpPost, handler);
		} catch (Exception e) {
			log.error("" + e);
		}
		return responseBody;
	}

	public String executeSSLGet(String URLAddress, Map<String, String> headerParams) throws MalformedURLException,
			IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

		HttpClient client = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(URLAddress);
		if (headerParams != null) {
			for (String header : headerParams.keySet()) {
				httpget.addHeader(header, headerParams.get(header));
			}
		}

		log.info("Executing request " + httpget.getRequestLine());

		ResponseHandler<String> handler = new ResponseHandler<String>() {
			@Override
			public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				log.info("Status: " + status);
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				} else {
					throw new ClientProtocolException("Unexpected response status: " + status);
				}
			}
		};

		String responseBody = client.execute(httpget, handler);
		return responseBody;

	}

	@SuppressWarnings("deprecation")
	public String executeSSLPost(String URLAddress,

			Map<String, String> headerParams,

			List<NameValuePair> postParams) throws MalformedURLException,

			IOException, NoSuchAlgorithmException, KeyManagementException,

			KeyStoreException {

		HttpClientBuilder builder = HttpClientBuilder.create();

		SSLContext sslContext = new SSLContextBuilder()
				.loadTrustMaterial(null, new TrustStrategy() {
					public boolean isTrusted(X509Certificate[] arg0,
							String arg1) throws CertificateException {
						return true;
					}
				}).build();

		HostnameVerifier hostnameVerifier = SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
		SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("http",PlainConnectionSocketFactory.getSocketFactory())
				.register("https", sslSocketFactory).build();

		PoolingHttpClientConnectionManager connectionMgr =
				new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		builder.setConnectionManager(connectionMgr);
		builder.setSslcontext(sslContext);
		HttpClient client = builder.build();
		HttpPost httpPost = new HttpPost(URLAddress);
		if (headerParams != null) {
			for (String header : headerParams.keySet()) {
				httpPost.addHeader(header, headerParams.get(header));
			}
		}

		if (postParams != null)
			httpPost.setEntity(new UrlEncodedFormEntity(postParams, "UTF-8"));

		ResponseHandler<String> handler = new ResponseHandler<String>() {
			@Override
			public String handleResponse(final HttpResponse response)
					throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				log.info("Status: " + status);
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				} else {
					throw new ClientProtocolException(
							"Unexpected response status: " + status);
				}

			}

		};

		return client.execute(httpPost, handler);
	}

}
