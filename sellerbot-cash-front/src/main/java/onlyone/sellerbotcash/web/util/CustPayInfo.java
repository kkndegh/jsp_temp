package onlyone.sellerbotcash.web.util;

import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import onlyone.sellerbotcash.service.MypageService;
import onlyone.smp.service.dto.ExtUsingGoodsInfoDTO;

@Component
public class CustPayInfo {
    public final static String CUST_PAID_INFO = "custPaidInfo";

    @Autowired private MypageService mypageService;

    public boolean isPayingUser(HttpSession session, String custId) {
        boolean result = false;
        ExtUsingGoodsInfoDTO custPaidInfo = getCustPayInfoFromSession(session);
        if(Objects.isNull(custPaidInfo)) {
            saveCustPayInfoForSession(session, custId);
            custPaidInfo = getCustPayInfoFromSession(session);
        }

        if(Objects.nonNull(custPaidInfo.getBasic_goods_req_seq_no()) || Objects.nonNull(custPaidInfo.getAddn_goods_req_seq_no()))
            result = true;

        return result; 
    }

    public boolean isOnlyBasicUser(HttpSession session, String custId) {
        boolean result = false;
        ExtUsingGoodsInfoDTO custPaidInfo = getCustPayInfoFromSession(session);
        if(Objects.isNull(custPaidInfo)) {
            saveCustPayInfoForSession(session, custId);
            custPaidInfo = getCustPayInfoFromSession(session);
        }

        if(Objects.nonNull(custPaidInfo.getBasic_goods_req_seq_no()) && Objects.isNull(custPaidInfo.getAddn_goods_req_seq_no()))
            result = true;

        return result;
    }

    public boolean isOnlyBNKUser(HttpSession session, String custId) {
        boolean result = false;
        ExtUsingGoodsInfoDTO custPaidInfo = getCustPayInfoFromSession(session);
        if(Objects.isNull(custPaidInfo)) {
            saveCustPayInfoForSession(session, custId);
            custPaidInfo = getCustPayInfoFromSession(session);
        }

        if(Objects.isNull(custPaidInfo.getBasic_goods_req_seq_no()) && Objects.nonNull(custPaidInfo.getAddn_goods_req_seq_no()))
            result = true;

        return result; 
    }

    public boolean isBNKUser(HttpSession session, String custId) {
        boolean result = false;
        ExtUsingGoodsInfoDTO custPaidInfo = getCustPayInfoFromSession(session);
        if(Objects.isNull(custPaidInfo)) {
            saveCustPayInfoForSession(session, custId);
            custPaidInfo = getCustPayInfoFromSession(session);
        }

        if(Objects.nonNull(custPaidInfo.getAddn_goods_req_seq_no()))
            result = true;

        return result; 
    }

    private void saveCustPayInfoForSession(HttpSession session, String custId) {
        ExtUsingGoodsInfoDTO custPaidInfo = mypageService.getPayInfoForUsingGoods(custId);
        session.setAttribute(CUST_PAID_INFO, custPaidInfo);
    }

    private ExtUsingGoodsInfoDTO getCustPayInfoFromSession(HttpSession session) {
        ExtUsingGoodsInfoDTO custPaidInfo = (ExtUsingGoodsInfoDTO) session.getAttribute(CUST_PAID_INFO);
        return custPaidInfo;
    }

    /**
     * 사용자의 이용권 여부에 따른 기능 제한을 위해 session 정보 갱신
     */
    public void updateSessionInfoAboutPay(HttpSession session, String custId) {
        // 사용자가 이용중인 서비스 갱신
		saveCustPayInfoForSession(session, custId);
    }

    
}