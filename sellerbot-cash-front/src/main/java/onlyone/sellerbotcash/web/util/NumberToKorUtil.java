package onlyone.sellerbotcash.web.util;

public class NumberToKorUtil {
    public static String NumberToKor(String amt){
            
        String amt_msg = "";
        // 중복 글자 제거하면 안됨. 자릿수에 맞게 표현
        String[] arrayUnit = {"","십","백","천","만","만","만","만","억","억","억","억","조","조","조","조","경","경","경","경","해","해","해","해"};
   
        if(amt.length() > 0){
            int len = amt.length();
            amt_msg = arrayUnit[len - 1];
        }else{
            amt_msg = amt;
        }
     
        return amt_msg;
    }
}