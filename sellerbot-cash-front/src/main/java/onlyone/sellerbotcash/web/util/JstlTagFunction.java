package onlyone.sellerbotcash.web.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * JSTL에서 사용할 tag library 추가.
 * @author oletree
 *
 */
public class JstlTagFunction {

	static public String formatLocalDateTime(LocalDateTime time, String pattern) {
		
		return time.format(DateTimeFormatter.ofPattern(pattern));
	}
	
	static public String toStringNum(Object num) {
		if(num == null) return "";
		if(num instanceof Long)
			return Long.toString(((Long)num));
		
		return num.toString();
	}
	
	static public String textToHtml(String content) {
		return content.replaceAll("\n", "<br>");
	}
}
