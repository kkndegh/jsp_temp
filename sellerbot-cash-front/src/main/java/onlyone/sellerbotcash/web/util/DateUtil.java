package onlyone.sellerbotcash.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateUtil {
	
	public static final String timezoneName = TimeZone.getDefault().getID();
	/** yyyy-MM */
    public static final DateTimeFormatter FORMAT_MONTH = DateTimeFormatter.ofPattern("yyyy-MM");
    /** yyyyMM */
    public static final DateTimeFormatter FORMAT_MONTH_NO_HI = DateTimeFormatter.ofPattern("yyyyMM");
    /** yyyyMMdd */
    public static final DateTimeFormatter NO_FORMAT_DAY   = DateTimeFormatter.ofPattern("yyyyMMdd");
    /** yyyy-MM-dd */
    public static final DateTimeFormatter FORMAT_DAY   = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    /** yyyy-MM-dd HH:mm */
    public static final DateTimeFormatter FORMAT_HOUR  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    /** yyyy-MM-dd HH:mm:ss */
    public static final DateTimeFormatter FORMAT_FULL  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    /** yyyyMMddHHmmss */
	public static final DateTimeFormatter NO_FORMAT_FULL  = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	
	private static SimpleDateFormat yyyymm = new SimpleDateFormat("yyyyMM");

	/** yyyy년 MM월 dd일 */
	public static final DateTimeFormatter KR_FORMAT_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy년 MM월 dd일");

	/** MM월 dd일 */
	public static final DateTimeFormatter KR_FORMAT_MM_DD = DateTimeFormatter.ofPattern("MM월 dd일");

	/** yyyy/MM/dd */
    public static final DateTimeFormatter FORMAT_YYYY_MM_DD   = DateTimeFormatter.ofPattern("yyyy/MM/dd");

	/** yyyy.MM.dd */
    public static final DateTimeFormatter FORMAT_DOT_YYYY_MM_DD   = DateTimeFormatter.ofPattern("yyyy.MM.dd");
	
	public static Date getYearMonth(String str) throws ParseException {
		return yyyymm.parse(str);
	}
	
	public static String getYearMonthString(Date date) throws ParseException {
		return yyyymm.format(date);
	}
	
	/**
	 * yyyy-MM-dd HH:mm:ss 형태의 String을 LocalDateTime으로 변경 한다.
	 * @param datetime
	 * @return
	 */
	public static LocalDateTime getDateTime(String datetime) {
		return LocalDateTime.parse(datetime, FORMAT_FULL);
	}
	
	/**
	 * yyyy-MM-dd HH:mm:ss 형태의 String으로 변경 한다.
	 * @param datetime
	 * @return
	 */
	public static String getDateTime(LocalDateTime datetime) {
		return datetime.format(FORMAT_FULL);
	}	
	

	/**
	 * 오늘 날짜의 시작 시간을 리턴 한다.
	 * @return
	 */
	public static LocalDateTime getDateTimeFormStartOfDay() {
		LocalDate ld = LocalDate.now();
		return ld.atStartOfDay();
		
	}
	
	/**
	 * yyyy-MM-dd 형태로된 날짜 스트링을 사용하여 시작 시간을 설정하여 LocalDateTime을 리턴 한다.
	 * @param day
	 * @return
	 */
	public static LocalDateTime getDateTimeFormStartOfDay(String day) {
		LocalDate ld = LocalDate.parse(day, FORMAT_DAY);
		return ld.atStartOfDay();
	}

	/**
	 * 오늘 날짜의 마지막 시간을 리턴한다.
	 * @return
	 */
	public static LocalDateTime getDateTimeFormEndOfDay() {
		LocalDate ld = LocalDate.now();
		return ld.atTime(LocalTime.MAX);
	}
	
	/**
	 * yyyy-MM-dd 형태로된 날짜 스트링을 사용하여 마지막 시간을 설정하여 LocalDateTime을 리턴 한다. 
	 * @param day
	 * @return
	 */
	public static LocalDateTime getDateTimeFormEndOfDay(String day) {
		if(day == null) return null;
		LocalDate ld = LocalDate.parse(day, FORMAT_DAY);
		return ld.atTime(LocalTime.MAX);
	}


	/**
	 * yyyy-MM-dd HH:mm 형태
	 * @param ldt 
	 * @return
	 */
	public static String getDateTimeMinuteString(LocalDateTime ldt) {
		if(ldt == null) return null;
		return ldt.format(FORMAT_HOUR);
	}
	
	/**
	 * yyyy-MM 형태
	 * @param ldt 
	 * @return
	 */
	public static String getDateTimeMonthString(LocalDateTime ldt) {
		if(ldt == null) return null;
		return ldt.format(FORMAT_MONTH);
	}
	
	public static String getYearAndMonth(LocalDateTime ldt) {
		if(ldt == null) return null;
		return ldt.format(FORMAT_MONTH_NO_HI);
	}
	
	/**
	 * format 형식에 따라 리턴
	 * @param ldt 
	 * @return
	 */
	public static String getDateTimeString(LocalDateTime ldt, DateTimeFormatter format) {
		if(ldt == null) return null;
		return ldt.format(format);
	}
	
	/**
	 * yyyy-MM-dd 형태
	 * @param ld
	 * @return
	 */
	public static String getDateString(LocalDate ld) {
		if(ld == null) return null;
		return ld.format(FORMAT_DAY);
	}
	/**
	 * yyyyMM 형태
	 * @param dt
	 * @return
	 */
	public static String getYearAndMonth(LocalDate dt) {
		if(dt == null) return null;
		return dt.format(FORMAT_MONTH_NO_HI);
	}

	/**
	 * yyyyMMdd 형태
	 * @param dt
	 * @return
	 */
	public static String getDateTimeNoFormatDay(LocalDate dt) {
		return dt.format(NO_FORMAT_DAY);
	}

	/**
	 * Format되지 않은 Date String을 Format 된 String으로 변경 함.
	 * @param date
	 * @return
	 */
	public static String makeNonFormatToFormat(String date) {
		int size = date.length();
		StringBuilder sb = new StringBuilder();
		sb.append( date.substring(0, 4)).append("-");
		sb.append(date.substring(4, 6));
		if( size >= 8) {
			//일까지
			sb.append("-").append(date.substring(6, 8));
		}
		if( size >= 10) {
			//시까지
			sb.append(" ").append(date.substring(8, 10));
		}
		if (size >= 12) {
			//분까지
			sb.append(":").append(date.substring(10, 12));
		}
		if(size >= 14) {
			//초까지
			sb.append(":").append(date.substring(12,14));
		}
		
		return sb.toString();
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public static String parseKrDateString(String date) {
		date = date.replaceAll("-", "");
		int size = date.length();
		StringBuilder sb = new StringBuilder();
		sb.append( date.substring(0, 4)).append("년 ");
		sb.append(date.substring(4, 6)).append("월 ");;
		if( size >= 8) {
			//일까지
			sb.append(date.substring(6, 8)).append("일");
		}
		if( size >= 10) {
			//시까지
			sb.append(" ").append(date.substring(8, 10)).append("시");
		}
		if (size >= 12) {
			//분까지
			sb.append(":").append(date.substring(10, 12)).append("분");;
		}
		if(size >= 14) {
			//초까지
			sb.append(":").append(date.substring(12,14)).append("초");;;
		}
		
		return sb.toString();
	}
	
	/**
	 * 날짜 정보 취득
	 * @param startDate
	 * @param lastMinusMonths
	 * @return
	 */
	public static List<String> getDateMonthList(LocalDateTime startDate, int lastMinusMonths, DateTimeFormatter format) {
		
		List<String> list = new ArrayList<String>();
		  
		list.add(DateUtil.getDateTimeString(startDate, format));
		lastMinusMonths++;
		
		for(int i = 1; i < lastMinusMonths; i++) {
			list.add(DateUtil.getDateTimeString(startDate.plusMonths(i), format));
		}
		
		return list;
		
	}


	//-----------------------------------------------------------------------------------------------
	// 2020-04-28 신규 추가
	//-----------------------------------------------------------------------------------------------

	/**
	 * 문자열 형식의 날짜 정보 포맷을 변경
	 * @param date
	 * @param from
	 * @param to
	 * @return
	 */
	public static String changeDateFormat(String date, DateTimeFormatter from, DateTimeFormatter to) {
		String dateStr = date.replaceAll("-", "").replaceAll("/", "");
		int size = dateStr.length();
		String rtnDate = "";
		
		if(size > 8) {
			LocalDateTime localDateTime = LocalDateTime.parse(date, from);
			rtnDate = localDateTime.format(to);
		}
		else {
			LocalDate localDate = LocalDate.parse(date, from);
			rtnDate = localDate.format(to);
		}

		return rtnDate;
	}

	/**
	 * 문자열 형식의 날짜 정보 포맷을 변경
	 * @param date
	 * @return
	 */
	public static String changeDateFormatForIndex(String date, int type) {
		switch (type) {
			case 1: // yyyyMMdd -> yyyy-MM-dd
				return changeDateFormat(date, NO_FORMAT_DAY, FORMAT_DAY);
			case 2: // yyyy-MM-dd -> yyyy/MM/dd
				return changeDateFormat(date, FORMAT_DAY, FORMAT_YYYY_MM_DD);	
			case 3: // yyyyMMdd -> yyyy/MM/dd
				return changeDateFormat(date, NO_FORMAT_DAY, FORMAT_YYYY_MM_DD);
			case 4: // yyyyMMdd -> yyyy.MM.dd일
				return changeDateFormat(date, NO_FORMAT_DAY, FORMAT_DOT_YYYY_MM_DD);
			default: // yyyyMMdd -> yyyy년 MM월 dd일
				return changeDateFormat(date, NO_FORMAT_DAY, KR_FORMAT_YYYY_MM_DD);
		}
	}

	public static String getDateTimeKrFormatYMD(LocalDate dt) {
		return dt.format(KR_FORMAT_YYYY_MM_DD);
	}

	public static String getDateTimeKrFormatMD(LocalDate dt) {
		return dt.format(KR_FORMAT_MM_DD);
	}	

	public static String getDateTimeNoFormatFull(LocalDateTime dt) {
		return dt.format(NO_FORMAT_FULL);
	}

	/**
	 * 현재 날짜와 비교
	 * 
	 * @param endDate
	 * @return
	 * @throws ParseException
	 */
	public static boolean getCompreBeforeNowDateToEndDate(String endDate) throws ParseException {
		boolean flag = false;
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");

		Date nowDate = new Date();
		Date compreDate = sdformat.parse(endDate);

		if (compreDate.before(nowDate)) {
			flag = true;
		}

		return flag;
	}
}

