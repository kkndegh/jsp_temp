package onlyone.sellerbotcash.web.util;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

public class DownloadView extends AbstractView {
    private static final String ENCODING = StandardCharsets.UTF_8.name();

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        
        String filePath = model.get("filePath").toString();
        String fileName = model.get("fileName").toString();

        HttpURLConnection connection = null;

        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileName, ENCODING) + "\";");

            connection = (HttpURLConnection) new URL(filePath).openConnection();

            response.setContentLengthLong(connection.getContentLengthLong());
            response.setContentType(connection.getContentType());

            FileCopyUtils.copy(connection.getInputStream(), response.getOutputStream());

        } catch (Exception e) {
            throw new IllegalStateException(e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

}