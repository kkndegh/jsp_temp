package onlyone.sellerbotcash.web.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import lombok.extern.slf4j.Slf4j;

import com.google.common.net.MediaType;

import onlyone.sellerbotcash.service.SbfUtils;

@Slf4j
public class ExcelUtil {
	
	private String FILE_EXT = ".xlsx";
	public final String STYLE_TITLE = "title";
	public final String STYLE_TABLE_TITLE = "table_title";
	public final String STYLE_HEADER = "header";
	public final String STYLE_HEADER_ERR = "header_err";
	public final String STYLE_BODY = "body";
	public final String STYLE_MONEY = "money";
	public final String STYLE_MONEY_ERR = "money_err";
	public final String STYLE_CELL = "cell";
	public final String STYLE_FORMULA = "formula";
	public final String STYLE_FORMULA_2 = "formula_2";
	

	public XSSFWorkbook createWorkBook() {
		return new XSSFWorkbook();
	}
	
	public XSSFSheet createSheet(Workbook wb, String sheetName) {
		XSSFSheet sheet = (XSSFSheet) wb.createSheet(sheetName);
		return sheet;
	}
	
	public XSSFCellStyle borderStyle(XSSFWorkbook wb) {
		XSSFCellStyle style = (XSSFCellStyle) wb.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(HSSFColorPredefined.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(HSSFColorPredefined.BLACK.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(HSSFColorPredefined.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(HSSFColorPredefined.BLACK.getIndex());
        return style;
	}
	
	public CellStyle addBorderStyle(CellStyle style) {
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(HSSFColorPredefined.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(HSSFColorPredefined.BLACK.getIndex());
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(HSSFColorPredefined.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(HSSFColorPredefined.BLACK.getIndex());
        return style;
	}
	
	
	
	public XSSFCell setCellValue(XSSFSheet sheet, int rowIndex, int colIndex, String value) {
		XSSFRow row = sheet.createRow(rowIndex);
		XSSFCell cell = row.createCell(colIndex);
		cell.setCellValue(value);
		return cell;
	}
	
	public XSSFCell setCellValue(XSSFSheet sheet, XSSFRow row, int colIndex, String value, CellStyle style) {
		XSSFCell cell = row.createCell(colIndex);
		cell.setCellValue(value);
		cell.setCellStyle(style);
		return cell;
	}
	
	public XSSFCell setCellValue(XSSFSheet sheet, int rowIndex, int colIndex, String value, CellStyle style) {
		XSSFRow row = Objects.isNull(sheet.getRow(rowIndex)) ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);
		XSSFCell cell = Objects.isNull(row.getCell(colIndex)) ?  row.createCell(colIndex) : row.getCell(colIndex);
		cell.setCellValue(value);
		cell.setCellStyle(style);
		return cell;
	}
	
	public XSSFCell setCellValue(XSSFSheet sheet, int rowIndex, int colIndex, int value, CellStyle style) {
		XSSFRow row = Objects.isNull(sheet.getRow(rowIndex)) ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);
		XSSFCell cell = Objects.isNull(row.getCell(colIndex)) ?  row.createCell(colIndex) : row.getCell(colIndex);
		cell.setCellValue(value);
		cell.setCellStyle(style);
		return cell;
	}
	
	public XSSFCell setCellValue(XSSFSheet sheet, int rowIndex, int colIndex, Long value, CellStyle style) {
		XSSFRow row = Objects.isNull(sheet.getRow(rowIndex)) ? sheet.createRow(rowIndex) : sheet.getRow(rowIndex);
		XSSFCell cell = Objects.isNull(row.getCell(colIndex)) ?  row.createCell(colIndex) : row.getCell(colIndex);
		value = Objects.isNull(value) ? 0L : value;
		cell.setCellValue(value);
		cell.setCellStyle(style);
		return cell;
	}
	
	public void download(HttpServletResponse res, XSSFWorkbook wb, String filename) {
		try {

			res.setCharacterEncoding("UTF-8");
			res.setHeader("Content-Disposition", "attachment; filename="+SbfUtils.encodeUtf8(filename+FILE_EXT));
			res.setContentType(MediaType.MICROSOFT_EXCEL.toString()+";charset=euc-kr");
			wb.write(res.getOutputStream());
		} catch (IOException e) {
			log.error("" + e);
		}
	}
	
    public Map<String, CellStyle> createStyles(XSSFWorkbook wb){
        Map<String, CellStyle> styles = new HashMap<>();
        CellStyle style;
        Font titleFont = wb.createFont();
        titleFont.setFontHeightInPoints((short)28);
        titleFont.setBold(true);
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFont(titleFont);
        styles.put(STYLE_TITLE, style);
        
        Font titleFont_2 = wb.createFont();
        titleFont_2.setFontHeightInPoints((short)12);
        titleFont_2.setBold(true);
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFont(titleFont_2);
        styles.put(STYLE_TABLE_TITLE, style);

        Font headerFont = wb.createFont();
        headerFont.setFontHeightInPoints((short)10);
        headerFont.setBold(true);
        // headerFont.setColor(IndexedColors.WHITE.getIndex());
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(headerFont);
        style = addBorderStyle(style);
        //style.setWrapText(true);
        styles.put(STYLE_HEADER, style);
        
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.LEFT);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font bodyFont = wb.createFont();
        bodyFont.setFontHeightInPoints((short)10);
        style.setFont(bodyFont);
        styles.put(STYLE_BODY, addBorderStyle(style));
        
        style = wb.createCellStyle();
        Font moneyFont = wb.createFont();
        moneyFont.setFontHeightInPoints((short)10);
        style.setAlignment(HorizontalAlignment.RIGHT);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setDataFormat(wb.createDataFormat().getFormat("#,##0"));
        style.setFont(moneyFont);
        styles.put(STYLE_MONEY, addBorderStyle(style));
        
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setWrapText(true);
        styles.put(STYLE_CELL, style);

        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
        styles.put(STYLE_FORMULA, style);

        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
        styles.put("formula_2", style);

        return styles;
    }
    
    public CellStyle createStyle(XSSFWorkbook wb, String type) {
    	CellStyle style = wb.createCellStyle();
    	Font font;
    	if( STYLE_HEADER.equals(type) ) {
    		font = wb.createFont();
			font.setColor(IndexedColors.BLACK.getIndex());
			font.setFontHeightInPoints((short)10);
			font.setBold(true);
	        style.setAlignment(HorizontalAlignment.LEFT);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	        style.setFont(font);
	        addBorderStyle(style);
    	} else if( STYLE_HEADER_ERR.equals(type) ) {
    		font = wb.createFont();
			font.setFontHeightInPoints((short)10);
			font.setColor(IndexedColors.RED.getIndex());
			font.setBold(true);
	        style.setAlignment(HorizontalAlignment.CENTER);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	        style.setFont(font);
	        addBorderStyle(style);
    	}else if( STYLE_MONEY.equals(type) ) {
    		font = wb.createFont();
			font.setFontHeightInPoints((short)10);
			font.setColor(IndexedColors.BLACK.getIndex());
	        style.setAlignment(HorizontalAlignment.RIGHT);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setDataFormat(wb.createDataFormat().getFormat("#,##0"));
	        style.setFont(font);
	        addBorderStyle(style);
    	} else if( STYLE_MONEY_ERR.equals(type) ) {
    		font = wb.createFont();
			font.setFontHeightInPoints((short)10);
			font.setColor(IndexedColors.RED.getIndex());
	        style.setAlignment(HorizontalAlignment.RIGHT);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setDataFormat(wb.createDataFormat().getFormat("#,##0"));
	        style.setFont(font);
	        addBorderStyle(style);
    	} else if( STYLE_BODY.equals(type) ) {
    		font = wb.createFont();
			font.setFontHeightInPoints((short)10);
			font.setColor(IndexedColors.BLACK.getIndex());
	        style.setAlignment(HorizontalAlignment.LEFT);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setFont(font);
	        addBorderStyle(style);
    	}
			
    	return style;
	}
    
    public void setColMerged(XSSFSheet sheet, int rowIndex) {
    	XSSFRow row = sheet.getRow(rowIndex);
    	int lastCol = row.getLastCellNum();
    	int mergeStartCol = 0;
    	int mergeEndCol = 0;
    	String temp = lastCol > 0 ? row.getCell(0).getStringCellValue() : "";
    	for(int i = 0; i < lastCol; i++) {
    		
    		if( temp.equals(row.getCell(i).getStringCellValue())) {
    			mergeEndCol = i;
    		} else {
    			if( mergeStartCol != mergeEndCol ) {
    				sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, mergeStartCol, mergeEndCol));
    			}
    			mergeStartCol = i;
    			mergeEndCol = i;
    			temp = row.getCell(i).getStringCellValue();
    		}
    	}
    	
    	if( mergeStartCol != mergeEndCol ) {
			sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, mergeStartCol, mergeEndCol));
		}
    	
    }
    
    public void setWidth(XSSFSheet sheet, int startCol, int lastCol, int width) {
    	for(int i = startCol; i <= lastCol; i++) {
    		sheet.setColumnWidth(i, width);	
    	}
	}
    
    public int getLastCellNum(XSSFSheet sheet) {
    	int result = 0;
    	int lastRowNum = sheet.getLastRowNum();
    	XSSFRow row;
    	
    	for(int i = 0; i <= lastRowNum; i++ ) {
    		row = sheet.getRow(i);	
    		if(Objects.nonNull(row)) {
    			if( sheet.getRow(i).getLastCellNum() > result) {
        			result = sheet.getRow(i).getLastCellNum();
        		}
    		}
    	}
    	return result;
	}
    
    
    
 
	
}
