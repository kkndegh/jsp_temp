package onlyone.sellerbotcash.web.vm;

import java.io.Serializable;
import java.util.List;

import onlyone.smp.service.dto.ExtKkontTgtDTO;


public class AlarmDTO implements Serializable { 

	private static final long serialVersionUID = 3778467504337106133L;

	private List<ExtKkontTgtDTO> list;

	public List<ExtKkontTgtDTO> getList() {
		return list;
	}

	public void setList(List<ExtKkontTgtDTO> list) {
		this.list = list;
	}
	
	
}
