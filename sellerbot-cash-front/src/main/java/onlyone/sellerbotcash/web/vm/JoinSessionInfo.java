package onlyone.sellerbotcash.web.vm;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import onlyone.smp.persistent.domain.enums.EInfPath;
import onlyone.smp.service.dto.Cafe24StoreDetailDTO;
import onlyone.smp.service.dto.ExtCustPackageDTO;

/**
 * sellerbot cash 신규 가입에서만 사용 할 것. 가입 절차 진행에 따른 정보를 저장 하여 최종 가입 처리를 진행 한다.
 * 
 * @author oletree
 *
 */
@Getter
@Setter
public class JoinSessionInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	// Long- termid, Boolean - true이면 필수 약관
	private Map<Long, JoinTerms> allTermsIds;
	// 필수 약관 목록
	private List<Long> essentialTermsIds;
	// 사용자가 선택한 약관 정보
	private Map<Long, JoinTerms> selectedTerms;

	// 20221118 사용자가 선택한 셀러봇캐시 마케팅 활용 동의 항목 추가
	private List<String> selectedSmtTerms;

	private ExtCustPackageDTO custDto;
	// cust_id용 토큰 정보
	private String custIdToken;
	// 인증 번호 확인 여부
	private boolean completeAuthNo = false;
	private EInfPath siteCd = null;
	// Cafe24 MallId
	private String mallId = null;
	// Cafe24 StoreInfo
	private Cafe24StoreDetailDTO cafeStore = null;
	// 가입 이벤트(프로모션) No.
	private Long eventNo;
	// 금융사 경로정보
	private String infPath;

	// 유료 상품 번호
	private String goodsSeqNo;
	// 유료 상품 옵션 번호
	private String goodsOptSeqNo;
	// 유료 상품 코드
	private String goodsTypCd;
	// 20230213 유료 상품 정보(TODO 네임밍 및 타입 변경 예정)
	private List<Map<Long, String>> paidProductInfos;

}
