package onlyone.sellerbotcash.web.vm;

import java.util.List;

/**
 * Api로 부터 전달 받은 페이지 정보와 목록 자료 
 * @author oletree
 *
 * @param <T>
 */
public class ListData<T> {

	private int page;
	private int totalCount;
	private List<T> dataList;

	public ListData(int totalCount, int page, List<T> dataList) {
		this.totalCount = totalCount;
		this.page = page;
		this.dataList = dataList;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public List<T> getDataList() {
		return dataList;
	}

	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}

}
