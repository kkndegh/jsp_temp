/**
 * @author kenny
 */
package onlyone.sellerbotcash.web.vm;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import onlyone.smp.service.dto.ExtCustAddDTO;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SimpleJoinReqDTO extends ExtCustAddDTO {
	private static final long serialVersionUID = 1L;
	private String token;
	private List<Long> terms = new ArrayList<Long>();

	private String ipkPartnerNo;
	private String ipkSupplySeq;
}
