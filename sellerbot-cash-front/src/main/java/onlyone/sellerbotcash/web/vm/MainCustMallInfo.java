package onlyone.sellerbotcash.web.vm;

/**
 * 메인 화면에서 사용할 고객 몰 상태별 개수
 * @author oletree
 *
 */
public class MainCustMallInfo {

	private int total;
	private int normal;
	private int error;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getNormal() {
		return normal;
	}

	public void setNormal(int normal) {
		this.normal = normal;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

}
