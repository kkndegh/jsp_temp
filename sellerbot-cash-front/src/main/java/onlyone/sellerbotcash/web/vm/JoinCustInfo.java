package onlyone.sellerbotcash.web.vm;

import onlyone.smp.service.dto.ExtCustAddDTO;

/**
 * sellerbot cash 가입 중 join_step3에서 전달되는 사용자 가입 정보
 * 
 * @author oletree
 *
 */
public class JoinCustInfo extends ExtCustAddDTO {
	private static final long serialVersionUID = 1L;

	private String ceo_no_svc;
	private String charg_no_svc;
	private String trs_hop_hour1;
	private String trs_hop_hour2;
	private String req_dt;

	public String getReq_dt() {
		return this.req_dt;
	}

	public void setReq_dt(String req_dt) {
		this.req_dt = req_dt;
	}

	public String getTrs_hop_hour1() {
		return this.trs_hop_hour1;
	}

	public void setTrs_hop_hour1(String trs_hop_hour1) {
		this.trs_hop_hour1 = trs_hop_hour1;
	}

	public String getTrs_hop_hour2() {
		return this.trs_hop_hour2;
	}

	public void setTrs_hop_hour2(String trs_hop_hour2) {
		this.trs_hop_hour2 = trs_hop_hour2;
	}

	public String getCeo_no_svc() {
		return ceo_no_svc;
	}

	public void setCeo_no_svc(String ceo_no_svc) {
		this.ceo_no_svc = ceo_no_svc;
	}

	public String getCharg_no_svc() {
		return charg_no_svc;
	}

	public void setCharg_no_svc(String charg_no_svc) {
		this.charg_no_svc = charg_no_svc;
	}

}
