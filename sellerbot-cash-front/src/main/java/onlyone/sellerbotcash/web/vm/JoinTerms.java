package onlyone.sellerbotcash.web.vm;

import java.io.Serializable;

/**
 * SMP로 부터 전달 받은 약관의 기본 정보   
 * @author oletree
 *
 */
public class JoinTerms implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean essTerm;
	private String svcId;
	private Long TermId;

	public JoinTerms(boolean essTerm, String svcId, Long termId) {
		super();
		this.essTerm = essTerm;
		this.svcId = svcId;
		TermId = termId;
	}

	public boolean isEssTerm() {
		return essTerm;
	}

	public void setEssTerm(boolean essTerm) {
		this.essTerm = essTerm;
	}

	public String getSvcId() {
		return svcId;
	}

	public void setSvcId(String svcId) {
		this.svcId = svcId;
	}

	public Long getTermId() {
		return TermId;
	}

	public void setTermId(Long termId) {
		TermId = termId;
	}

}
