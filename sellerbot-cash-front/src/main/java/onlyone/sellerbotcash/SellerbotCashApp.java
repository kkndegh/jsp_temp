package onlyone.sellerbotcash;

import java.net.InetAddress;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.core.env.Environment;

import lombok.extern.slf4j.Slf4j;

/**
 * Sellerbot cash front
 * @author oletree
 *
 */
@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class SellerbotCashApp {
    private static String HOST_ADDRESS;

    @Value("${server.address:}")
    public void setHostAddress(String hostAddress) {
        SellerbotCashApp.HOST_ADDRESS = hostAddress;
    }

	public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SellerbotCashApp.class);
        app.setBannerMode(Banner.Mode.OFF);
		Environment env = app.run(args).getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }

        if (StringUtils.isNotEmpty(SellerbotCashApp.HOST_ADDRESS))
            hostAddress = SellerbotCashApp.HOST_ADDRESS;

        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}\n\t" +
                "External: \t{}://{}:{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            protocol,
            env.getProperty("server.port"),
            protocol,
            hostAddress,
            env.getProperty("server.port"),
            env.getActiveProfiles());
	}
}
