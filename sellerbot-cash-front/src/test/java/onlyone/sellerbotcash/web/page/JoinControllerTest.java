/**
 * kenny
 */
package onlyone.sellerbotcash.web.page;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import onlyone.sellerbotcash.web.util.SecurityAES256;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class JoinControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void simpleJoin() throws Exception {
        String data = "<?xml version=\"1.0\" encoding=\"euc-kr\"?><ROOT><HEAD><BIZNO>999128131</BIZNO><COMPANY>인마이홈컴퍼니</COMPANY><CEO>온리원</CEO></HEAD></ROOT>";
        String encrypted = SecurityAES256.AES_Encode(data);

        mockMvc.perform(get("http://localhost:8081/pub/simple_join?data=0x" + encrypted.toUpperCase()))
        .andExpect(status().isOk());
    }


    public static void main(String[] args) throws Exception {

        String data = "<?xml version=\"1.0\" encoding=\"euc-kr\"?><ROOT><HEAD><BIZNO>3052818622</BIZNO><COMPANY>인마이홈컴퍼니</COMPANY><CEO>온리원</CEO></HEAD></ROOT>";
        String encrypted = SecurityAES256.AES_Encode(data);

        System.out.println("http://localhost:8081/pub/simple_join?data=0x" + encrypted.toUpperCase());
    }


}