/**
 * mailsung
 */
package onlyone.sellerbotcash.web.page;

import java.net.URISyntaxException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.junit.Test;
// import org.springframework.beans.factory.annotation.Autowired;

// import onlyone.sellerbotcash.config.ApplicationProperties;
import onlyone.sellerbotcash.service.AbstractSmpService;
import onlyone.smp.service.shopby.dto.req.AuthMeReqDTO;
import onlyone.smp.service.shopby.dto.req.LongLivedReqDTO;
import onlyone.smp.service.shopby.dto.req.MallsReqDTO;
import onlyone.smp.service.shopby.dto.req.OrdersReqDTO;
import onlyone.smp.service.shopby.dto.res.AuthMeResDTO;
import onlyone.smp.service.shopby.dto.res.LongLivedResDTO;
import onlyone.smp.service.shopby.dto.res.MallsResDTO;
import onlyone.smp.service.shopby.dto.res.OrdersResDTO;

public class ShopBy_API_TEST extends AbstractSmpService {

    // @Autowired
    // private ApplicationProperties appProperties;


    @Test
    public void test1() {
        URIBuilder builder;
        LongLivedResDTO result = null;

        LongLivedReqDTO longLivedReqDTO = new LongLivedReqDTO();
        // longLivedReqDTO.setAccept("application/json");
        // longLivedReqDTO.setContentType("application/json");
        // longLivedReqDTO.setVersion("1.0");

        longLivedReqDTO.setClientId("b1hLbVFoS1lUeUdJdTc4Z1B1NzJYdz09");
        longLivedReqDTO.setClientSecret("O1y7c1J4lM3Sn9KmXrU1zWPEs2Fv4U3U");
        longLivedReqDTO.setCode("Lr6oHTfEz4WW");
        longLivedReqDTO.setGrantType("authorization_code");
        longLivedReqDTO.setRedirectUri("https://devcash.sellerbot.co.kr/shby/oauth/bankbot");
        

        try {
            builder = new URIBuilder("http://localhost:8080" + "/v1/shby/auth/token/long-lived");

            StringEntity entity;
            try {
                ObjectMapper objectMapper = new ObjectMapper();

                entity = new StringEntity(objectMapper.writeValueAsString(longLivedReqDTO),"UTF-8");
                entity.setContentType("application/json");
            } catch (UnsupportedCharsetException | JsonProcessingException e) {
                throw new RuntimeException("stringEntity", e);
            }

            HttpPost httpPost = new HttpPost(builder.build());
            httpPost.setEntity(entity);
            result = callSmpApi(httpPost, LongLivedResDTO.class);
            System.out.println(result);

            // result = new LongLivedResDTO();
            // result.setAccessToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXJ0bmVyTm8iOjk2ODE1LCJhZG1pbk5vIjoxMDg1NjEsImFjY2Vzc2libGVJcHMiOltdLCJ1c2FnZSI6IlNFUlZFUiIsImlzcyI6Ik5ITiBDb21tZXJjZSIsImFwcE5vIjo3MTcsIm1hbGxObyI6NjM5NjcsImV4cCI6NDgzOTIwMTUzMiwiaWF0IjoxNjg1NjAxNTMyfQ.YhHqomweHiUrJiEDr37VFscLhm3RNQCKDcA2hroyAKo");
            String option = "4";

            if(StringUtils.isNotEmpty(result.getAccessToken())){

                if(option.equals("2")){
                    //2
                    URIBuilder builder2;
                    AuthMeResDTO result2 = null;
                    
                    AuthMeReqDTO authMeReqDTO = new AuthMeReqDTO();
                    authMeReqDTO.setAuthorization(result.getAccessToken());
                    authMeReqDTO.setSystemkey("b1hLbVFoS1lUeUdJdTc4Z1B1NzJYdz09");

                    builder2 = new URIBuilder("http://localhost:8080" + "/v1/shby/auth/me");

                    StringEntity entity2;
                    try {
                        ObjectMapper objectMapper = new ObjectMapper();

                        entity2 = new StringEntity(objectMapper.writeValueAsString(authMeReqDTO),"UTF-8");
                        entity2.setContentType("application/json");
                    } catch (UnsupportedCharsetException | JsonProcessingException e) {
                        throw new RuntimeException("stringEntity", e);
                    }

                    HttpPost httpPost2 = new HttpPost(builder2.build());
                    httpPost2.setEntity(entity2);
                    result2 = callSmpApi(httpPost2, AuthMeResDTO.class);
                    System.out.println(result2);
                }else if(option.equals("3")){
                    //3
                    URIBuilder builder2;
                    MallsResDTO result2 = null;
                    
                    MallsReqDTO mallsReqDTO = new MallsReqDTO();
                    mallsReqDTO.setAuthorization(result.getAccessToken());
                    mallsReqDTO.setSystemkey("b1hLbVFoS1lUeUdJdTc4Z1B1NzJYdz09");

                    builder2 = new URIBuilder("http://localhost:8080" + "/v1/shby/malls");

                    StringEntity entity2;
                    try {
                        ObjectMapper objectMapper = new ObjectMapper();

                        entity2 = new StringEntity(objectMapper.writeValueAsString(mallsReqDTO),"UTF-8");
                        entity2.setContentType("application/json");
                    } catch (UnsupportedCharsetException | JsonProcessingException e) {
                        throw new RuntimeException("stringEntity", e);
                    }

                    HttpPost httpPost2 = new HttpPost(builder2.build());
                    httpPost2.setEntity(entity2);
                    result2 = callSmpApi(httpPost2, MallsResDTO.class);
                    System.out.println(result2);
                } else if(option.equals("4")){
                    //3
                    URIBuilder builder2;
                    OrdersResDTO result2 = null;
                    
                    OrdersReqDTO ordersReqDTO = new OrdersReqDTO();
                    ordersReqDTO.setAuthorization(result.getAccessToken());
                    ordersReqDTO.setSystemkey("b1hLbVFoS1lUeUdJdTc4Z1B1NzJYdz09");

                    builder2 = new URIBuilder("http://localhost:8080" + "/v1/shby/accounts/orders");

                    StringEntity entity2;
                    try {
                        ObjectMapper objectMapper = new ObjectMapper();

                        entity2 = new StringEntity(objectMapper.writeValueAsString(ordersReqDTO),"UTF-8");
                        entity2.setContentType("application/json");
                    } catch (UnsupportedCharsetException | JsonProcessingException e) {
                        throw new RuntimeException("stringEntity", e);
                    }

                    HttpPost httpPost2 = new HttpPost(builder2.build());
                    httpPost2.setEntity(entity2);
                    result2 = callSmpApi(httpPost2, OrdersResDTO.class);
                    System.out.println(result2);
                }
            }else{
                System.out.println("토큰없다");
            }
        } catch (URISyntaxException e) {
            throw new RuntimeException();
        }
    }

    @Test
    public void testShopBy() throws Exception {

        // String app = "bankbot";
        // String clientId = appProperties.getShopBy().get(app).getClientId();
        String clientId = "b1hLbVFoS1lUeUdJdTc4Z1B1NzJYdz09";

        // 계약된 파트너 정보 조회하기
        String URLAddress = "https://server-api.e-ncp.com/malls/contracts/partners";

        Map<String, String> headerParams = new HashMap<String, String>();
        
        // 장기 토큰 발급
        URLAddress = "https://server-api.e-ncp.com/auth/token/long-lived";
        headerParams = new HashMap<String, String>();
        headerParams.put("accept", "application/json");
        headerParams.put("Version", "1.0");
        headerParams.put("Content-Type", "application/json");

        JSONObject postParams = new JSONObject();
        postParams.put("grant_type", "authorization_code");
        postParams.put("client_id", clientId);
        postParams.put("redirect_uri", "https://devcash.sellerbot.co.kr/shby/oauth/bankbot");
        postParams.put("code", "1OGO4UydK6oV");
        postParams.put("client_secret", "O1y7c1J4lM3Sn9KmXrU1zWPEs2Fv4U3U");

        String token = "";
        try {
            HttpResponse<String> response = Unirest.post(URLAddress).headers(headerParams).body(postParams.toString()).asString();
            JSONObject longLivedRes = JSONObject.fromObject(response.getBody());

            System.out.println("longLivedRes :: " + longLivedRes);

            Object resSelector = longLivedRes.get("access_token");
            if(resSelector == null){
                System.out.println(longLivedRes.get("message"));
            }else {
                token = longLivedRes.get("access_token").toString();
            }
            
            System.out.println("Access Token Info response :: " + token);
        } catch (UnirestException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        
        if(StringUtils.isNotEmpty(token)){
            try {
                HttpResponse<String> response = Unirest.get("https://server-api.e-ncp.com/auth/me")
                                                .header("accept", "application/json")
                                                .header("systemkey", clientId)
                                                .header("version", "1.0")
                                                .header("Authorization", "Bearer "+token)
                                                .asString();
                
                JSONObject mallsRes = JSONObject.fromObject(response.getBody());
                System.out.println("auth me response :: " + mallsRes);

                JSONObject mallObj = JSONObject.fromObject(mallsRes.get("mall"));
                
                String uMallId = mallObj.get("mallId").toString();
                String uClientId = mallObj.get("clientId").toString();

                System.out.println("uMallId :: " + uMallId);
                System.out.println("uClientId :: " + uClientId);
            } catch (Exception e) {
                System.out.println("몰정보 조회 에러발생 :: " + e.toString());
                e.printStackTrace();
            }
            
        }else{
            System.out.println("토큰 없음.");
        }

        // headerParams.put("systemKey", clientId);
        // headerParams.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXJ0bmVyTm8iOjk2ODE1LCJhZG1pbk5vIjoxMDg1NjEsImFjY2Vzc2libGVJcHMiOltdLCJ1c2FnZSI6IlNFUlZFUiIsImlzcyI6Ik5ITiBDb21tZXJjZSIsImFwcE5vIjo3MTcsIm1hbGxObyI6NjM5NjcsImV4cCI6NDgzNTMxMzU1MSwiaWF0IjoxNjgxNzEzNTUxfQ.cEF8-QQuOZMdEvk_m_5avyzElgcw0KRqfh-Uaj8VryA");
        // headerParams.put("Version", "1.0");

        // HttpResponse<String> partnersResult = Unirest.get(URLAddress)
        //                                         .headers(headerParams)
        //                                         .asString();
        // System.out.println("response::" + partnersResult.getBody());

        // authorization code로 장기 토큰발급하기
        // URLAddress = "https://server-api.e-ncp.com/auth/token/long-lived";
        // headerParams = new HashMap<String, String>();
        // headerParams.put("accept", "application/json");
        // headerParams.put("Version", "1.0");
        // headerParams.put("Content-Type", "application/json");

        // Map<String, Object> postParams = new HashMap<String, Object>();
        // postParams.put("grant_type", "authorization_code");
        // postParams.put("client_id", clientId);
        // postParams.put("redirect_uri", "https://devcash.sellerbot.co.kr/shby/oauth/bankbot");
        // postParams.put("code", "aIYTZtViMFx5");
        // postParams.put("client_secret", "O1y7c1J4lM3Sn9KmXrU1zWPEs2Fv4U3U");

        // JSONObject jsonObject = new JSONObject(postParams);

        // HttpResponse<String> response = Unirest.post(URLAddress)
        //                                         .headers(headerParams)
        //                                         .body(jsonObject)
        //                                         .asString();
        // System.out.println("response::" + response.getBody());

        // malls 쇼핑몰 상세 조회
        // URLAddress = "https://server-api.e-ncp.com/malls";
        // headerParams = new HashMap<String, String>();
        // headerParams.put("systemKey", clientId);
        // headerParams.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXJ0bmVyTm8iOjk2ODE1LCJhZG1pbk5vIjoxMDg1NjEsImFjY2Vzc2libGVJcHMiOltdLCJ1c2FnZSI6IlNFUlZFUiIsImlzcyI6Ik5ITiBDb21tZXJjZSIsImFwcE5vIjo3MTcsIm1hbGxObyI6NjM5NjcsImV4cCI6NDgzNTM4NjYwMywiaWF0IjoxNjgxNzg2NjAzfQ.B01g7KO2nNZUK4FrSDnII63vc3AdRecjbhUAGg9uOy4");
        // headerParams.put("Version", "1.0");

        // HttpResponse<String> partnersResult = Unirest.get(URLAddress)
        //                                         .headers(headerParams)
        //                                         .asString();
        // System.out.println("response::" + partnersResult.getBody());

        //무통장 미입금 주문 리스트 조회
        // URLAddress = "https://server-api.e-ncp.com/accounts/orders";
        // headerParams = new HashMap<String, String>();
        // headerParams.put("accept", "application/json");
        // headerParams.put("Version", "1.0");
        // headerParams.put("Content-Type", "application/json");
        // headerParams.put("systemKey", clientId);
        // headerParams.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXJ0bmVyTm8iOjk2ODE1LCJhZG1pbk5vIjoxMDg1NjEsImFjY2Vzc2libGVJcHMiOltdLCJ1c2FnZSI6IlNFUlZFUiIsImlzcyI6Ik5ITiBDb21tZXJjZSIsImFwcE5vIjo3MTcsIm1hbGxObyI6NjM5NjcsImV4cCI6NDgzNTY0MzgwNywiaWF0IjoxNjgyMDQzODA3fQ.3zinAQqgc20aFDVPRWwG1CHTII0qiE2VPQgsgFRlom0");

        // HttpResponse<String> response = Unirest.get(URLAddress)
        //                                         .headers(headerParams)
        //                                         // .body(jsonObject)
        //                                         .asString();

        // System.out.println("response::" + response.getBody());

        //몰정보
        // HttpResponse<String> response = Unirest.get("https://server-api.e-ncp.com/auth/me")
        //                                         .header("accept", "application/json")
        //                                         .header("systemkey", "b1hLbVFoS1lUeUdJdTc4Z1B1NzJYdz09")
        //                                         .header("version", "1.0")
        //                                         .header("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXJ0bmVyTm8iOjk2ODE1LCJhZG1pbk5vIjoxMDg1NjEsImFjY2Vzc2libGVJcHMiOltdLCJ1c2FnZSI6IlNFUlZFUiIsImlzcyI6Ik5ITiBDb21tZXJjZSIsImFwcE5vIjo3MTcsIm1hbGxObyI6NjM5NjcsImV4cCI6NDgzNTY1NDE0OSwiaWF0IjoxNjgyMDU0MTQ5fQ.j_pxgh8KkS-2x-uixjjOM6jBWWzpc7Jsmki48YrTIHE")
        //                                         .asString();
        // System.out.println("response::" + response.getBody());

        // Map<String, String> headerParams = new HashMap<String, String>();
    }


}